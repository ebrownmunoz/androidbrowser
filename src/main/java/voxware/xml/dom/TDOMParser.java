package voxware.xml.dom;

import java.util.*;
import java.net.*;
import java.io.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.InputSource;

import org.apache.xerces.dom.DOMImplementationImpl;

import voxware.net.URLLoader;
import voxware.util.SysLog;

/** A Trivial DOM Parser **/

public class TDOMParser {

  protected org.xml.sax.InputSource  source;
  protected org.xml.sax.ErrorHandler errorHandler;

  /**-------------------------------------------------------------------------*
   * ErrorHandler access methods
   *-------------------------------------------------------------------------**/
  public void setErrorHandler(ErrorHandler errorHandler) { this.errorHandler = errorHandler; }
  public ErrorHandler getErrorHandler()                  { return this.errorHandler; }

  /**-------------------------------------------------------------------------*
   * Document access methods
   *-------------------------------------------------------------------------**/
  public Document getDocument() { return document; }


  /**-------------------------------------------------------------------------*
   * Default constructor
   *-------------------------------------------------------------------------**/
  public TDOMParser() {};


  /**-------------------------------------------------------------------------*
   * Parse the InputSource specified by the given system identifier
   *-------------------------------------------------------------------------**/
  public void parse(java.lang.String systemId) throws DOMException, IOException, MalformedURLException, SAXException {
    parse(new org.xml.sax.InputSource(systemId));   // Throws DOMException, IOException, MalformedURLException, SAXException
    if (source.getCharacterStream() != null) source.getCharacterStream().close();
    else if (source.getByteStream() != null) source.getByteStream().close();
  }

  /**-------------------------------------------------------------------------*
   * Parse the specified InputSource
   *-------------------------------------------------------------------------**/
  public void parse(InputSource source) throws DOMException, IOException, MalformedURLException, SAXException {

    this.source = source;

    java.io.Reader reader = source.getCharacterStream();
    if (reader == null) {
       // No Reader is available; try to get an InputStream
      java.io.InputStream stream = source.getByteStream();
      if (stream == null) {
         // No InputStream is available; try to get a systemId string
        String systemId = source.getSystemId();
        if (systemId == null) {
           // No systemId is available
          fatalError(new SAXParseException("No XML source available", source.getPublicId(), null, 0, 0));
        } else try {
           // Try to interpret the systemId as a URL and GET from there
          URLLoader url = new URLLoader(systemId); // Throws MalformedURLException
          byte[] bytes = url.byteArray();          // Throws IOException
          if (bytes == null)
            throw new IOException("TDOMParser.parse: URLLoader.byteArray returns null");
          else
            stream = new ByteArrayInputStream(bytes);
        } catch (IOException e) {
          fatalError(new SAXParseException("I/O Error", source.getPublicId(), systemId, 0, 0, e));
        }
      }
      reader = (source.getEncoding() != null) ? new InputStreamReader(stream, source.getEncoding()) : new InputStreamReader(stream);
    }

     // Now we should have a functional Reader giving access to the XML source
    parse(reader);  // Throws DOMException, IOException, SAXException
  }

  /**-------------------------------------------------------------------------*
   * A relevant sample of XML:
   *
   *  <?xml version="1.0"?>
   *  <!DOCTYPE vxml SYSTEM "http://192.168.200.8:8080/vxml/vwxml_100.dtd">
   *  <vxml version="1.0" application="vxml/root.vxml">
   *    <property name="speakervolume" expr="speakervolume"/>
   *    <script><![CDATA[
   *      userName = 'dcv';
   *      pickList = 'Order 12-527';
   *    ]]></script>
   *    <form id="print">
   *      <block>
   *        <audio><c:out value="${printerForm.message}"/></audio>
   *        <goto next="vxml/special.vxml"/>" caching="fast" fetchhint="prefetch"/>
   *      </block>
   *    </form>
   *  </vxml>
   *
   *-------------------------------------------------------------------------**/

  private static final int    InitialBufferLength = 88;
  private static final char   EOF = (char)-1;
  private static final char   Bra = '<';
  private static final char   Ket = '>';
  private static final String CommentTerminator = "-->";
  private static final String CDATAStarter = "CDATA[";
  private static final String CDATATerminator = "]]>";
  private static final String DocTypeFieldName = "DOCTYPE";
  private static final String DocSysIdFieldName = "SYSTEM";

   // Private parser state variables
  private Reader       reader;
  private DocumentType documentType;
  private Document     document;
  private StringBuffer buffer = null;
  private char         character = ' ';
  private boolean      barren;

  /**-------------------------------------------------------------------------*
   * Parse the specified Reader
   *-------------------------------------------------------------------------**/
  public void parse(Reader reader) throws DOMException, IOException, SAXException {

    long start = 0L;
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.XMLPARSER_MASK)) start = System.currentTimeMillis();

    this.reader = reader;

    DOMImplementation dom = DOMImplementationImpl.getDOMImplementation();

     // Parse the XML version header
    buffer = new StringBuffer(InitialBufferLength);
    skipTo(Bra);                        // Throws SAXException
    if (buffer.length() > 0 && SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.XMLPARSER_MASK))
      SysLog.println("TDOMParser.parse: WARNING - skipped over spurious text \"" + buffer.toString() + "\" before initial \'" + Bra + "\'");
    buffer = new StringBuffer(InitialBufferLength);
    accumulateTo(Ket);
    if (!buffer.toString().equals("?xml version=\"1.0\"?"))
      fatalError("Invalid XML version header");

     // Parse the document type header
    skipWhitespace(Bra);                // Throws SAXException
    buffer = new StringBuffer(InitialBufferLength);
    accumulateTo(Ket);
    String docType = buffer.toString();
    String docTypeName = null;;
    int dtStart = docType.indexOf(DocTypeFieldName, 1);
    String docSysIdField = null;;
    if (dtStart < 0) {
      fatalError("No DOCTYPE in document type specifier");
    } else {
      dtStart += DocTypeFieldName.length();
      int dtEnd;
      int idStart = docType.indexOf(DocSysIdFieldName, dtStart);
      if (idStart < 0) {
        dtEnd = docType.length();
      } else {
        dtEnd = idStart;
        idStart += DocSysIdFieldName.length();
        docSysIdField = docType.substring(idStart).trim();
      }
      docTypeName = docType.substring(dtStart, dtEnd).trim();
    }
    documentType = dom.createDocumentType(docTypeName, null, docSysIdField); // Throws DOMException
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.XMLPARSER_MASK))
      SysLog.println("TDOMParser.parse: created DocumentType with name \"" + documentType.getName() + "\" and systemId \"" + documentType.getSystemId() + "\"");
    document = dom.createDocument(null, docTypeName, documentType); // Throws DOMException
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.XMLPARSER_MASK))
      SysLog.println("TDOMParser.parse: created Document named \"" + document.getNodeName() + "\" of type \"" + document.getDoctype().getName() + "\"");
    
     // Parse the document tag
    skipWhitespace(Bra);
    Node node = parseNode(null);
    if (skipWhitespace() != EOF) warning("Document \"" + docTypeName + "\" has extraneous characters at the end");
    else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.XMLPARSER_MASK))
      SysLog.println("TDOMParser.parse: parsed Document \"" + document.getDocumentElement().getTagName() + "\" successfully in " + (System.currentTimeMillis() - start) + " milliseconds");
  }
  

  /**-------------------------------------------------------------------------*
   * Skip over whitespace
   * Returns the first non-whitespace character encountered, which may be EOF
   *-------------------------------------------------------------------------**/
  private char skipWhitespace() throws IOException {
    while (Character.isWhitespace(character = (char)reader.read()));
    return character;
  }

  /**-------------------------------------------------------------------------*
   * Skip over whitespace until reaching the given terminator
   * Throws SAXException if terminator is not reached via whitespace
   *-------------------------------------------------------------------------**/
  private void skipWhitespace(char terminator) throws IOException, SAXException {
    while (Character.isWhitespace(character = (char)reader.read()));
    if (character == terminator)     return;
    else if (character == EOF) eofError();
    else                       fatalError("Unexpected \'" + character + "\' encountered"); // Throws SAXException
  }

  /**-------------------------------------------------------------------------*
   * Skip over everything until reaching the given terminator
   * Throws SAXException on EOF
   *-------------------------------------------------------------------------**/
  private void skipTo(char terminator) throws IOException, SAXException {
    while ((character = (char)reader.read()) != terminator) {
      if (character == EOF) eofError();    // Throws SAXException
      else buffer.append(character);
    }
  }

  /**-------------------------------------------------------------------------*
   * Skip over everything until reaching the end of the terminator string
   * Throws SAXException on EOF
   *-------------------------------------------------------------------------**/
  private void skipTo(String terminator) throws IOException, SAXException {
    int index = 0;
    int terminatorLength = terminator.length();
    char terminatorChar = terminator.charAt(index);
    while (true) {
      if ((character = (char)reader.read()) == terminatorChar) {
        if (++index < terminatorLength) terminatorChar = terminator.charAt(index);
        else                            break;
      } else if (character == EOF) {
        eofError();                               // Throws SAXException
      } else {
        index = 0;
      }
    }
  }

  /**-------------------------------------------------------------------------*
   * Accumulate raw text until reaching the given terminator
   *-------------------------------------------------------------------------**/
  private void accumulateTo(char terminator) throws IOException, SAXException {
    while ((character = (char)reader.read()) != terminator) {
      if (character == EOF) eofError();           // Throws SAXException
      else                  buffer.append(character);
    }
  }

  /**-------------------------------------------------------------------------*
   * Accumulate raw text until reaching the END of the terminator string
   * Throws SAXException on EOF
   *-------------------------------------------------------------------------**/
  private void accumulateTo(String terminator) throws IOException, SAXException {
    int index = 0;
    int terminatorLength = terminator.length();
    char terminatorChar = terminator.charAt(index);
    while (true) {
      if ((character = (char)reader.read()) == terminatorChar) {
        if (++index < terminatorLength) terminatorChar = terminator.charAt(index);
        else                            break;
      } else if (character == EOF) {
        eofError();                               // Throws SAXException
      } else {
        index = 0;
      }
      buffer.append(character);
    }
  }

  /**-------------------------------------------------------------------------*
   * Skip over the given pattern String, returning true iff there is a match
   *-------------------------------------------------------------------------**/
  private boolean match(String pattern) throws IOException, SAXException {
    int patternLength = pattern.length();
    for (int index = 0; index < patternLength; index++) {
      if ((character = (char)reader.read()) == pattern.charAt(index));
      else if (character == EOF) fatalError("Premature end of file");   // Throws SAXException
      else                       return false;
    }
    return true;
  }

  /**-------------------------------------------------------------------------*
   * Accumulate decoded text until reaching the given terminator
   *-------------------------------------------------------------------------**/
  private void decodeTo(char terminator) throws IOException, SAXException {
    while (true) {
      if ((character = (char)reader.read()) == terminator) {
        return;
      } else if (character == '&') {
         // This is the start of a character element (only "&gt;", "&lt;" or "&amp;" are allowed)
        if ((character = (char)reader.read()) == 'g') {
          if (match("t;"))  { buffer.append(">"); continue; }
        } else if (character == 'l') {
          if (match("t;"))  { buffer.append("<"); continue; }
        } else if (character == 'a') {
          if (match("mp;")) { buffer.append("&"); continue; }
        } else if (character == EOF) {
          break;
        }
        fatalError("Unrecognized character entity"); // Throws SAXException
      } else if (character == EOF) {
        break;
      } else {
        buffer.append(character);
      }
    }
    eofError();      // Throws SAXException
  }

  /**-------------------------------------------------------------------------*
   * Parse the stream into a DOM Node and return it
   *-------------------------------------------------------------------------**/
  private Node parseNode(Node parent) throws IOException, SAXException, DOMException {
    buffer = new StringBuffer(InitialBufferLength);
    if (character != Bra) {
       // This must be a Text section; decode text up to the start of the next tag and house it in a Text node
      decodeTo(Bra);  // Throws SAXException
      Text text = document.createTextNode(buffer.toString());
      if (text != null) {
        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.XMLPARSER_MASK)) {
          if (buffer.length() > 0) SysLog.println("TDOMParser.parseNode: constructed text node \"" + text.getData() + "\""); // Throws DOMException
          else                     SysLog.println("TDOMParser.parseNode: constructed empty text node");                      // Throws DOMException
        }
        return text;
      } else {
        fatalError("TDOMParser.parseNode: cannot create a text node for text \"" + buffer.toString() + "\"");   // Throws SAXException
      }
    } else if ((character = (char)reader.read()) == '!') {
       // This can be either a comment or a CDATA section
      if ((character = (char)reader.read()) == '-') {
        if ((character = (char)reader.read()) == '-') {
           // This is a comment; accumulate everything between here and the CommentTerminator
          accumulateTo(CommentTerminator);
           // Create a Comment from the text and return it
          buffer.setLength(buffer.length() - CommentTerminator.length());
          return document.createComment(buffer.toString());
        } else {
          fatalError("TDOMParser.parseNode: section starting with \"<!-\" is not a comment"); // Throws SAXException
        }
      } else if (character == '[' && match(CDATAStarter)) {
         // This is a CDATA section
        accumulateTo(CDATATerminator);
         // Create a CDATA section from the text and return it
        buffer.setLength(buffer.length() - CDATATerminator.length());
        return document.createCDATASection(buffer.toString());  // Throws DOMException
      } else {
        fatalError("TDOMParser.parseNode: section starting with \"<!\" is neither a comment nor CDATA"); // Throws SAXException
      }
    } else if (character == '/') {
        // This is a ket tag ("</...>"); its name must match the name of the bra tag (i.e., parent.getNodeName())
      if (match(parent.getNodeName())) {
         // The names match; skip to the end of the tag and return null to signal closure
        skipTo(Ket);
        return null;
      } else {
        fatalError("TDOMParser.parseNode: bra-ket mismatch: ket != \"" + parent.getNodeName() + "\""); // Throws SAXException
      }
    } else {
       // Parse the bra tag ("<...>") and construct an Element from it
      Element element = null;
       // Extract the tag name
      buffer.append(character);
      while (!Character.isWhitespace((character = (char)reader.read())) && character != '/' && character != Ket) {
        if (character == EOF) eofError();                   // Throws SAXException
        else                  buffer.append(character);
      }
      if (buffer.length() == 0) fatalError("TDOMParser.parseNode: missing tag name"); // Throws SAXException
      String tagName = buffer.toString();
      if (parent == null) {
         // This is the document tag (e.g., "<vxml>"); get the Element from the Document rather than constructing it
        if (!tagName.equals(documentType.getName())) {
          fatalError("Misnamed document tag: <" + tagName + "> != <" + documentType.getName() + ">"); // Throws SAXException
        } else {
          element = document.getDocumentElement();
        }
      } else {
         // Construct an Element from this tag
        element = document.createElement(tagName);
      }
      if (element == null) fatalError("Cannot create an element of type <" + tagName + ">"); // Throws SAXException
       // Extract the attributes, if any, and attach them as Attr to the Element
      while (true) {
        buffer = new StringBuffer(InitialBufferLength);
         // Skip whitespace
        while (true) {
          if (character == '/') {
             // If the bra tag is a bracket tag ("<.../>"); return its childless Element
            if ((character = (char)reader.read()) == '>') return element;
            else                                          fatalError("TDOMParser.parseNode: spurious \'/\'");
          } else if (character == '>') {
             // The bra tag is a bra tag ("<...>"); append any children and return the Element
            if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.XMLPARSER_MASK))
              SysLog.println("TDOMParser.parseNode: starting to parse the children of \"" + element.getTagName() + "\"");
             // Construct and append the children
            Node childNode;
            while ((childNode = parseNode(element)) != null) element.appendChild(childNode);
             // A null node signals that the ket tag has been reached; the Element is thus complete
            if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.XMLPARSER_MASK))
              SysLog.println("TDOMParser.parseNode: finished parsing the children of \"" + element.getTagName() + "\"");
            return element;
          } else if (character == EOF) {
            fatalError("Premature end of file"); // Throws SAXException
          } else if (!Character.isWhitespace(character)) {
            buffer.append(character);
            break;
          }
          character = (char)reader.read();
        }
         // Get the attribute name
        accumulateTo('=');  // Throws SAXException
        String attributeName = buffer.toString().trim();
        if (skipWhitespace() == EOF) eofError();
         // Get the attribute value
        if (character != '\"' && character != '\'') fatalError("TDOMParser.parseNode: illegal quote character \'" + character + "\'");
        buffer = new StringBuffer(InitialBufferLength);
        decodeTo(character);
        String attributeValue = buffer.toString();
         // Construct the Attr
        Attr attribute = document.createAttribute(attributeName); // Throws DOMException
        if (attribute == null) fatalError("TDOMParser.parseNode: cannot create Attr \"" + attributeName + "\"");
        attribute.setNodeValue(attributeValue);                   // Throws DOMException
         // Attach it to the Element
        element.setAttributeNode(attribute);                      // Throws DOMException
        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.XMLPARSER_MASK))
          SysLog.println("TDOMParser.parseNode: set attribute (" + attributeName + " = \"" + attributeValue + "\") on Element <" + element.getTagName() + ">");
        character = (char)reader.read();
      }
    }
    fatalError("TDOMParser.parseNode: software logic error");
    return null;
  }
    
   // Error Handling

  /**-------------------------------------------------------------------------*
   * Throw a SAXParseException reporting an unexpected EOF
   *-------------------------------------------------------------------------**/
  private void eofError() throws SAXException {
    fatalError("Premature end of file"); // Throws SAXException
  }

   /** Warning */
  private void warning(SAXParseException exception) throws SAXException {
    if (errorHandler != null) errorHandler.warning(exception);
  }

   /** Warning */
  private void warning(String message) throws SAXException {
    if (errorHandler != null) errorHandler.warning(new SAXParseException(message, null, null, 0, 0));
  }

   /** Error */
  private void error(SAXParseException exception) throws SAXException {
    if (errorHandler != null) errorHandler.error(exception);
    else                      throw exception;
  }

   /** Error */
  private void error(String message) throws SAXException {
    SAXParseException exception = new SAXParseException(message, null, null, 0, 0);
    if (errorHandler != null) errorHandler.error(exception);
    else                      throw exception;
  }

   /** Fatal error */
  private void fatalError(SAXParseException exception) throws SAXException {
    if (errorHandler != null) errorHandler.fatalError(exception);
    else                      throw exception;
  }

   /** Fatal error */
  private void fatalError(String message) throws SAXException {
    SAXParseException exception = new SAXParseException(message, null, null, 0, 0);
    if (errorHandler != null) errorHandler.fatalError(exception);
    else                      throw exception;
  }
}
