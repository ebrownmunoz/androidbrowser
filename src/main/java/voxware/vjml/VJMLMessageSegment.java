package voxware.vjml;

import java.lang.*;
import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class VJMLMessageSegment implements Serializable {
  public byte[]   content  = null;
  public String   address  = null;
  public int      status   = TerminalSegment;
  public int      priority = VJMLMessage.DefaultPriority;
  public int      index    = 0;

  public static final int TerminalSegment = 0;
  public static final int InternalSegment = 1;
  public static final int Success         = 3;
  public static final int NetworkFailure  = 4;
  public static final int OutOfRange      = 5;
  public static final int QueueOverflow   = 6;
  public static final int SerializeError  = 7;
  public static final int JavaException   = 8;

  public VJMLMessageSegment(byte[] content, String address) {
    this.content = content;
    this.address = address;
  }

  public VJMLMessageSegment(byte[] content, String address, int status) {
    this.content = content;
    this.address = address;
    this.status = status;
  }

  public VJMLMessageSegment(byte[] content, String address, int status, int priority) {
    this.content = content;
    this.address = address;
    this.status = status;
    this.priority = priority;
  }

  public VJMLMessageSegment(byte[] content, String address, int status, int priority, int index) {
    this.content = content;
    this.address = address;
    this.status = status;
    this.priority = priority;
    this.index = index;
  }

  public byte[] content() {
    return content;
  }

  public void content(byte[] content) {
    this.content = content;
  }

  public String address() {
    return address;
  }

  public void address(String address) {
    this.address = address;
  }

  public InetAddress inetAddress() throws UnknownHostException {
    int cpos;
    String ipaddr = address;

    InetAddress ia = null;
    if ((cpos = address.indexOf(':')) > 0) {
      try {
        ipaddr = address.substring(0, cpos);
      } catch (StringIndexOutOfBoundsException e) {
        e.printStackTrace();
        return null;
      }
    }      
    ia = InetAddress.getByName(ipaddr);

    return ia;
  }

  public int port() {
    int cpos;
    String sPort = "";

    if ((cpos = address.indexOf(':')) > 0) {
      try {        
        sPort = address.substring(cpos+1);
      } catch (StringIndexOutOfBoundsException e) {
        e.printStackTrace();
        return 0;
      }
    } else {
      return 0;
    }

    return Integer.parseInt(sPort);
  }

  public int status() {
    return status;
  }

  public void status(int status) {
    this.status = status;
  }

  public int priority() {
    return priority;
  }

  public void priority(int priority) {
    this.priority = priority;
  }

  public int index() {
    return index;
  }

  public void index(int index) {
    this.index = index;
  }

  private void writeObject(ObjectOutputStream s) throws IOException {
     // Write the fields
    s.writeInt(content.length);
    s.write(content);
    s.writeObject(address);
    s.writeByte(status);
    s.writeByte(priority);
    s.writeShort(index);
  }

  private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
     // Read content length & allocate
    int contentLen = s.readInt();
    content = new byte[contentLen];

     // Read the contents and the other fields
    int rlen = s.read(content, 0, contentLen);
    if (rlen != contentLen) 
      System.out.println("VJMLMessageSegment::readObject ERROR contents " + rlen + " != " + contentLen);
    address = (String) s.readObject();
    status = s.readUnsignedByte();
    priority = s.readUnsignedByte();
    index = s.readUnsignedShort();
  }
}
