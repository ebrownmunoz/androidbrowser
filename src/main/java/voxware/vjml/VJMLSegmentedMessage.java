package voxware.vjml;

import java.lang.*;
import java.io.*;
import java.util.*;

import voxware.util.*;

class VJMLSegmentedMessage extends Vector {

   // A VJMLSegmentedMessage is a Vector of VJMLMessageSegments whose concatenated contents constitute the
   // serialization of any serializable Object.  The Object is normally a VJMLMessage, a Vector of VJMLMessages
   // (a so-called message cluster), or a Vector of Vectors of VJMLMessages (a so-called supercluster).

  public static final int DefaultMaxSegmentSize = 1200;
  private static int maxSegSize = DefaultMaxSegmentSize;

   // Construct an empty VJMLSegmentedMessage
  VJMLSegmentedMessage() {
    super();
  }

   // Construct a VJMLSegmentedMessage from the given serializable Object
  VJMLSegmentedMessage(Object object, String address, int priority) {
    this();
    encodeAndSegment(object, address, priority);
  }

   // Set the maximum size of a segment's content in bytes
  static void maxSegSize(int size) {
    maxSegSize = size;
  }

   // Return the maximum size of a segment's content in bytes
  static int maxSegSize() {
    return maxSegSize;
  }

   // Make this Vector a valid sequence of VJMLSegments whose combined contents are the serialization of the given Object
  void encodeAndSegment(Object object, String address, int priority) {
    byte[] data = encode(object);
    this.removeAllElements();
    if (data.length > 0) {
      int index = 0;
      int base = 0;
      int segSize = maxSegSize;  // Get a local copy in case it changes
      while (data.length - base > segSize) {
        byte[] content = new byte[segSize];
        System.arraycopy(data, base, content, 0, segSize);
        this.addElement(new VJMLMessageSegment(content, address, VJMLMessageSegment.InternalSegment, priority, index++));
        base += segSize;
      }
      if (data.length - base > 0) {
        byte[] content;
         // Avoid copying the byte array when only one segment is needed
        if (index == 0) {
          content = data;
        } else {
          content = new byte[data.length - base];
          System.arraycopy(data, base, content, 0, data.length - base);
        }
        this.addElement(new VJMLMessageSegment(content, address, VJMLMessageSegment.TerminalSegment, priority, index));
      }
    }
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
      SysLog.println("VJMLSegmentedMessage.encodeAndSegment: encoded a " + data.length + " byte message in " + this.size() + " segment(s)");
  }

  private int totalSize;

   // Return the decoded Object if the given segment terminates a valid VJMLSegmentedMessage; otherwise, return null
  Object concatenateAndDecode(VJMLMessageSegment segment) {
    Object object = null;
    if (segment.index == 0) {
      this.removeAllElements();
      totalSize = 0;
    }
    if (segment.index == this.size()) {
      this.addElement(segment);
      totalSize += segment.content.length;
      if (segment.status == VJMLMessageSegment.TerminalSegment) {
         // Combine and decode into object...
        byte[] data;
         // Avoid copying the content array if possible
        if (segment.index == 0) {
          data = segment.content;
        } else {
          data = new byte[totalSize];
          int base = 0;
          Enumeration enumeration = this.elements();
          while (enumeration.hasMoreElements()) {
            segment = (VJMLMessageSegment)enumeration.nextElement();
            System.arraycopy(segment.content, 0, data, base, segment.content.length);
            base += segment.content.length;
          }
        }
        object = decode(data);
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
          SysLog.println("VJMLSegmentedMessage.concatenateAndDecode: decoded a " + data.length + " byte message from " + this.size() + " segment(s)");
         // Reset
        this.removeAllElements();
        totalSize = 0;
      } else if (segment.status != VJMLMessageSegment.InternalSegment) {
         // Software ERROR - only InternalSegment and TerminalSegment types are possible
        throw new RuntimeException("VJMLSegmentedMessage.accumulateAndDecode: UNEXPECTED segment type " + (new Integer(segment.status)).toString());
      }
    } else {
       // Corrupted sequence -- ignore the segment
      SysLog.println("VJMLSegmentedMessage.accumulateAndDecode: WARNING - segment index = " + segment.index + "; expected " + this.size());
    }
    return object;
  }

   // Serialize an Object into a byte array
  private static byte[] encode(Object object) {
    byte[] data = null;
    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(object);
      oos.flush();
      oos.close();
      data = baos.toByteArray();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return data;
  }

   // De-serialize a byte array into an Object
  private static Object decode(byte[] array) {
    Object object = null;
    try {
      ByteArrayInputStream bais = new ByteArrayInputStream(array);
      ObjectInputStream ois = new ObjectInputStream(bais);
      object = ois.readObject();
      ois.close();
      bais.close();
    } catch (ClassNotFoundException e) {
      SysLog.println("VJMLSegmentedMessage: " + e);
    } catch (Exception e) {
      SysLog.println("VJMLSegmentedMessage: " + e);
      e.printStackTrace();
    }
    return object;
  }

}
