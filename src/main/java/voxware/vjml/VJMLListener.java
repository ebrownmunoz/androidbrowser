package voxware.vjml;

import java.lang.*;

public interface VJMLListener {
  public void exception(Throwable exception);  // Report an exception
  public void message(VJMLChannel channel);    // Report an incoming message
}
