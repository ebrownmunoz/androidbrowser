package voxware.vjml;

import java.lang.*;

public class VJMLException extends Exception {

  Object offender;

  public VJMLException() {
    super();
  }

  public VJMLException(Object offender) {
    super();
    this.offender = offender;
  }

  public VJMLException(String message) {
    super(message);
  }

  public VJMLException(Object offender, String message) {
    super(message);
    this.offender = offender;
  }

  public Object offender() {
    return offender;
  }
}
