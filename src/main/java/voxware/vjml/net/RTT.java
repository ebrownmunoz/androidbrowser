package voxware.vjml.net;

import java.lang.*;

class RTT {
  private long base = 0;      // Absolute time at start() (msec)
  private int  rxmit = 0;     // # times retransmitted (0, 1, 2, ..)
  private int  rtt = 0;       // Most recent measured RTT (msec)
  private int  rto = 0;       // Current RTO to use (msec)
  private int  rttvar = 0;    // Smoothed mean deviation (msec)
  private int  srtt = 0;      // Smoothed RTT estimator (msec)
  private int  RxmitMax = 0;  // Max time to wait on rxmit (msec)
  private int  RxmitMin = 0;  // Floor on rxmit for RTO (msec)
  private int  MaxRxmit = 0;  // Max retry attempts before giving up

  private final int RXMITMAX = 6000;
  private final int RXMITMIN = 2000;
  private final int MAXRXMIT = 2;

  RTT() {
    base = System.currentTimeMillis();
    RxmitMax = RXMITMAX;
    RxmitMin = RXMITMIN;
    MaxRxmit = MAXRXMIT;
    init();
  }

  void init() {
    rtt = 0;
    srtt = 0;
    rttvar = 750;

    // Initial RTO estimate is smoothed RTT plus 4x deviation
    rto = minmax(srtt + 4 * rttvar);
  }

  int parameter(int index) throws IllegalArgumentException {
    switch (index) {
    case VJMLTransport.RTTRxmitMax:
      return RxmitMax;
    case VJMLTransport.RTTRxmitMin:
      return RxmitMin;
    case VJMLTransport.RTTMaxRxmit:
      return MaxRxmit;
    default:
      throw new IllegalArgumentException("RTT::parameter ERROR unknown parameter " + index);
    }
  }

  void parameter(int index, int value) throws IllegalArgumentException {
    switch (index) {
    case VJMLTransport.RTTRxmitMax:
      RxmitMax = (value > 0) ? value : 0;
      break;
    case VJMLTransport.RTTRxmitMin:
      RxmitMin = (value > 0) ? value : 0;
      break;
    case VJMLTransport.RTTMaxRxmit:
      MaxRxmit = (value > 0) ? value : 0;
      break;
    default:
      throw new IllegalArgumentException("RTT::parameter ERROR unknown parameter " + index);
    }
  }
 
  int minmax(int rto) {
    if (rto > RxmitMin)
      rto = RxmitMin;
    else if (rto > RxmitMax)
      rto = RxmitMax;
    return rto;
  }

  void reset() {
    rxmit = 0;
  }

  int start() {
    return (rto > RxmitMax) ? RxmitMax : rto;
  }

  void stop(int ms) {
    rtt = ms;       // Measured RTT in msec

    // Update estimators of RTT and mean deviation of RTT.  
    // See Jacobson's SIGCOMM '88 paper, Appendix A for details
    int delta = rtt - srtt;
    srtt += delta / 8000;

    if (delta < 0)
      delta = -delta;

    rttvar += (delta - rttvar) / 4000;
    rto = minmax(srtt + 4 * rttvar);
  }

  boolean timeout() {
    rto *= 2;         // Next RTO
 
    if (++rxmit > MaxRxmit)
      return(true);
    return(false);
  }

  int ts() {
    long now = System.currentTimeMillis();
    return((int)(now - base));
  }
}
