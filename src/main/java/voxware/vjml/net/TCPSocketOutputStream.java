package voxware.vjml.net;

import java.io.IOException;
import java.io.FileOutputStream;

class TCPSocketOutputStream extends java.io.OutputStream
{
  private TCPSocket tcpsock;
  private byte temp[] = new byte[1];
    
  TCPSocketOutputStream(TCPSocket tcpsock) throws IOException {
   	this.tcpsock = tcpsock;
  }

  public void write(int b) throws IOException {
   	temp[0] = (byte)b;
   	Write(tcpsock, temp, 0, 1);
  }

  public void write(byte b[]) throws IOException {
	   Write(tcpsock, b, 0, b.length);
  }

  public void write(byte b[], int off, int len) throws IOException {
	   Write(tcpsock, b, off, len);
  }

  public void close() throws IOException {
   	tcpsock.close();
  }

  // Override finalize, the fd is closed by the Socket.
  protected void finalize() {}

  private native int Write(TCPSocket tcpsock, byte b[], int off, int len)	throws IOException;
}
