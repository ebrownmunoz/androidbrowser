package voxware.vjml.net;

import java.net.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.InterruptedIOException;
import java.io.ByteArrayOutputStream;

public class TCPSocket {
  private static      boolean nativeLoaded =true;
  private int         port = 0;
  private int         localport = 0;
  private InetAddress address = null;
  private InetAddress localAddress = null;
  private int         timeout;              // SO_TIMEOUT (msec)
  private long        cobj = 0;             // Javaland pointer to underlying C object
  private Socket      sock = null;

  // Constants taken from java.net.SocketOptions
  final static int TCP_NODELAY = 0x0001;
  final static int SO_BINDADDR = 0x000F;
  final static int SO_REUSEADDR = 0x04;
  final static int IP_MULTICAST_IF = 0x10;
  final static int SO_LINGER = 0x0080;
  final static int SO_TIMEOUT = 0x1006;

  static {
    try {
      System.loadLibrary("vjml");
      nativeLoaded = true;
    } catch (UnsatisfiedLinkError ule) {
      nativeLoaded = false;
    }
  }

  public TCPSocket(String host, int port) throws UnknownHostException, IOException {
    if (nativeLoaded) {
      initialize(InetAddress.getByName(host), port, null, 0, 0);
    } else {
      sock = new Socket(InetAddress.getByName(host), port, null, 0);
    }
  }

  public TCPSocket(String host, int port, int timeout) throws UnknownHostException, IOException {
    if (nativeLoaded) {
      initialize(InetAddress.getByName(host), port, null, 0, timeout);
    } else {
      sock = new Socket(InetAddress.getByName(host), port, null, 0);
    }
  }

  public TCPSocket(InetAddress address, int port) throws IOException {
    if (nativeLoaded) {
      initialize(address, port, null, 0, 0);
    } else {
      sock = new Socket(address, port, null, 0);
    }
  }

  public TCPSocket(String host, int port, InetAddress localAddr, int localPort) throws IOException {
    if (nativeLoaded) {
      initialize(InetAddress.getByName(host), port, localAddr, localPort, 0);
    } else {
      sock = new Socket(InetAddress.getByName(host), port, localAddr, localPort);
    }
  }

  public TCPSocket(InetAddress address, int port, InetAddress localAddr, 
            int localPort) throws IOException {
    if (nativeLoaded) {
      initialize(address, port, localAddr, localPort, 0);
    } else {
      sock = new Socket(address, port, localAddr, localPort);
    }
  }                 

  private void initialize(InetAddress address, int port, InetAddress localAddr, 
                          int localPort, int timeout) throws IOException {
    if (port < 0 || port > 0xFFFF) {
      throw new IllegalArgumentException("port out range:"+port);
    }

    if (localPort < 0 || localPort > 0xFFFF) {
      throw new IllegalArgumentException("port out range:"+localPort);
    }

    try {
      constructor(); 
      if (localAddr != null || localPort > 0) {
        if (localAddr == null) {
          localAddr = InetAddress.getByName("0.0.0.0");
        }
        bind(localAddr, localPort);
      }

      if (timeout > 0) {
        // System.out.println("TCPSocket::initialize: connect timeout is " + timeout);
        setSoTimeout(timeout);
      }
      connect(address, port);
    } catch (SocketException e) {
      close();
      throw e;
    }
  }

  public void close() throws IOException {
    Close();
  }

  public InetAddress getInetAddress() {
    if (sock != null) {
      return sock.getInetAddress();
    } else {
      return address;
    }
  }

  public InetAddress getLocalAddress() {
    if (sock != null) {
      return sock.getLocalAddress();
    } else {
      InetAddress in = null;
      try {
        in = (InetAddress) getOption(SO_BINDADDR);
      } catch (Exception e) {
        try {
          in = InetAddress.getByName("0.0.0.0");
        } catch (Exception ee) {
          System.out.println("TCPSocket::getLocalAddress: ERROR InetAddress.getByName() failed");
          System.out.println(ee);
          ee.printStackTrace();
        }
      }
      return in;
    }
  }

  public int getPort() {
    if (sock != null) {
      return sock.getPort();
    } else {
      return port;
    }
  }

  public int getLocalPort() {
    if (sock != null) {
      return sock.getLocalPort();
    } else {
      return localport;
    }
  }

  public InputStream getInputStream() throws IOException {
    if (sock != null) {
      return sock.getInputStream();
    } else {
      return new TCPSocketInputStream(this);
    }
  }

  public OutputStream getOutputStream() throws IOException {
    if (sock != null) {
      return sock.getOutputStream();
    } else {
      return new TCPSocketOutputStream(this);
    }
  }

  protected void constructor() throws IOException {
    Create();
    Open();
  }
  
  protected void connect(String host, int port) throws UnknownHostException, IOException {
   	IOException pending = null;

   	try {
      InetAddress address = InetAddress.getByName(host);
      try {
        Connect(address, port);
      		return;
      } catch (IOException e) {
      		pending = e;
      }
   	} catch (UnknownHostException e) {
  	   pending = e;
  	 }

    // Everything failed
   	close();
   	throw pending;
  }

  protected void connect(InetAddress address, int port) throws IOException {
   	this.port = port;
   	this.address = address;

   	try {
	    Connect(address, port);
	    return;
   	} catch (IOException e) {
	    close();
	    throw e;
   	}
  }

  private void setAddress(InetAddress address) {
    this.address = address;
  }

  private void setLocalAddress(InetAddress localAddress) {
    this.localAddress = localAddress;
  }

  public void setTcpNoDelay(boolean on) throws SocketException {
    if (sock != null) {
      sock.setTcpNoDelay(on);
    } else {
      setOption(TCP_NODELAY, new Boolean(on));
    }
  }

  public boolean getTcpNoDelay() throws SocketException {
    if (sock != null) {
      return sock.getTcpNoDelay();
    } else {
      return ((Boolean) getOption(TCP_NODELAY)).booleanValue();
    }
  }

  public void setSoLinger(boolean on, int val) throws SocketException {
    if (sock != null) {
      sock.setSoLinger(on, val);
    } else {
      if (!on) {
        setOption(SO_LINGER, new Boolean(on));
      } else {
        setOption(SO_LINGER, new Integer(val));
      }
    }
  }

  public int getSoLinger() throws SocketException {
    if (sock != null) {
      return sock.getSoLinger();
    } else {
      Object o = getOption(SO_LINGER);
      if (o instanceof Integer) {
        return ((Integer) o).intValue();
      } else {
        return -1;
      }
    }
  }

  public void setSoTimeout(int timeout) throws SocketException {
    if (sock != null) {
      sock.setSoTimeout(timeout);
    } else {
      this.timeout = timeout;
    }
  }

  public int getSoTimeout() throws SocketException {
    if (sock != null) {
      return sock.getSoTimeout();
    } else {
      return timeout;
    }
  }

  private void setOption(int opt, Object val) throws SocketException {
   	int enable = 1;
   	switch (opt) {
    /* check type safety before calling native routine */
    case SO_LINGER:
    		if (val == null || (!(val instanceof Integer) && !(val instanceof Boolean)))
  		    throw new SocketException("TCPSocketImpl::setOption: ERROR Bad parameter for option");
    		if (val instanceof Boolean) { 
  		    // True only if disabling - enabling should be Integer
  		    enable = 0;
    		}
    		break;
    case SO_TIMEOUT:
  	  	if (val == null || (!(val instanceof Integer)))
  		    throw new SocketException("TCPSocketImpl::setOption: ERROR bad parameter for SO_TIMEOUT");
    		int tmp = ((Integer) val).intValue();
    		if (tmp < 0)
  		    throw new IllegalArgumentException("TCPSocketImpl::setOption: ERROR timeout < 0");
    		timeout = tmp;
    		return;
    case SO_BINDADDR:
    		throw new SocketException("TCPSocketImpl::setOption: ERROR cannot re-bind socket");
    case TCP_NODELAY:
    		if (val == null || !(val instanceof Boolean))
  		    throw new SocketException("TCPSocketImpl::setOption: bad parameter for TCP_NODELAY");
      enable = ((Boolean)val).booleanValue() ? 1 : 0;
      val = new Integer(0);
    		break;
    default:
    		throw new SocketException("TCPSocketImpl::setOption: unrecognized TCP option: " + opt);
	   }
	   SetOption(opt, enable, ((Integer)val).intValue());
  }

  private Object getOption(int opt) throws SocketException {
   	if (opt == SO_TIMEOUT) {
 	    return new Integer(timeout);
   	}  

   	int ret = GetOption(opt);

   	switch (opt) {
   	case TCP_NODELAY:
 	    return (ret == -1) ? new Boolean(false): new Boolean(true);
   	case SO_LINGER:
 	    return (ret == -1) ? new Boolean(false): (Object)(new Integer(ret));
   	case SO_BINDADDR:
      return localAddress;
   	}
   	return null;      // This should "never happen"
  }

  protected void bind(InetAddress address, int lport)	throws IOException {
    Bind(address, lport);
  }

  protected void listen(int count) throws IOException {
  	 Listen(count);
  }

  protected void accept(SocketImpl s) throws IOException {
   	Accept(s);
  }

  protected int available() throws IOException {
   	return Available();
  }

  protected void finalize() throws IOException {
    close();
    Destroy();
  }

  private native void Accept(SocketImpl s)	throws IOException;
  private native int  Available()	throws IOException;
  private native void Bind(InetAddress address, int port) throws IOException;
  private native void Connect(InetAddress address, int port) throws IOException;
  private native void Create() throws IOException;
  private native void Close() throws IOException;
  private native void Destroy();
  private native void Listen(int count)	throws IOException;
  private native void Open() throws IOException; 
  private native void SetOption(int opt, int enable, int value)	throws SocketException;
  private native int  GetOption(int opt) throws SocketException;
}
