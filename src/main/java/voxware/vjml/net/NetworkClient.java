package voxware.vjml.net;

import java.io.*;
import voxware.net.VoxwareURLConnection;
import voxware.vjml.net.TCPSocket;
import java.net.UnknownHostException;
import java.net.URL;


// Base class for network clients.

class NetworkClient {
  // Socket for server connection
  protected TCPSocket	serverSocket = null;

  // Stream for writing to the server
  PrintStream	serverOutput;

  // Buffered stream for reading replies from server
  InputStream	serverInput;

  // Open a connection to the server
  void openServer(String server, int port)	throws IOException, UnknownHostException {
   	if (serverSocket != null)
 	    closeServer();
	   serverSocket = doConnect(server, port);
   	serverOutput = new PrintStream(new BufferedOutputStream(serverSocket.getOutputStream()), true);
	   serverInput = new BufferedInputStream(serverSocket.getInputStream());
  }

  // Return a socket connected to the server, with any appropriate options established
  protected TCPSocket doConnect(String server, int port) throws IOException, UnknownHostException {
   	return new TCPSocket(server, port, VoxwareURLConnection.DefaultHttpConnectionTimeout);
  }

  // Close an open connection to the server
  void closeServer() throws IOException {
   	if (! serverIsOpen()) {
      return;
   	}
   	serverSocket.close();
   	serverSocket = null;
   	serverInput = null;
   	serverOutput = null;
  }

  // Return server connection status
  boolean serverIsOpen() {
   	return serverSocket != null;
  }

  // Create connection with host <i>host</i> on port <i>port</i>
  NetworkClient(String host, int port) throws IOException {
   	openServer(host, port);
  }

  NetworkClient() {}
}
