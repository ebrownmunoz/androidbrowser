package voxware.vjml.net;

import java.io.*;
import java.net.*;
import java.util.*;

import voxware.vjml.VJMLMessageSegment;

public class UDPClient {
  public static void main(String[] args) throws IOException, InterruptedException {
    boolean init = false;
    String  destAddr = "";

    if (args.length == 1) {
      init = true;
      destAddr = args[0];
    }

    VJMLTransport vjmlt = null;
    try {
      vjmlt = new VJMLTransport();
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }

    // Create a segment and send it to get things going
    VJMLMessageSegment segment = null;
    if (init) {
      System.out.println("main: INFO - initializing loop w/destAddr " + destAddr);
      int ret;
      String str = "ThisIsATestString";
      String addr = destAddr + ":4450";
      segment = new VJMLMessageSegment(str.getBytes(), addr);
      vjmlt.send(segment);
    }

    // Await a response.  Discard ACKs, resend the segment
    while (true) {
      segment = vjmlt.receive();
      if (segment.status() == VJMLMessageSegment.Success) {
        System.out.println("main: Got ACK, continuing..");
        continue;
      }

      // Readdress, then resend
      String addr = "";
      try {
        addr = (segment.inetAddress()).getHostAddress() + ":4450";
      } catch (Exception e) {
        e.printStackTrace();
      }
      segment.address(addr);
      vjmlt.send(segment);
    }
  }
}
