package voxware.vjml.net;

import java.io.*;
import java.net.*;
import java.util.*;

import voxware.util.*;
import voxware.vjml.VJMLMessageSegment;

public class VJMLTransportClient {
  static final int reportIters = 50;

  public static void main(String[] args) throws IOException, InterruptedException {
    boolean init = false;
    String  destAddr = "";

    if (args.length == 1) {
      init = true;
      destAddr = args[0];
    }

    VJMLTransport vjmlt = null;
    try {
      vjmlt = new VJMLTransport();
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }

    // Create a segment and send it to get things going
    VJMLMessageSegment segment = null;
    if (init) {
      System.out.println("main: INFO - initializing loop w/destAddr " + destAddr);
      int ret;
      String str = "";
      for (int i = 0; i < 135; i++)
         str += "0123456789";
      String addr = destAddr;
      segment = new VJMLMessageSegment(str.getBytes(), addr, VJMLMessageSegment.TerminalSegment);
      vjmlt.send(segment);
    }

    // Await a response.  Discard ACKs, resend the segment
    int cnt = 0;
    int msec = 0;
    long then = System.currentTimeMillis();
    long now = 0;
    while (true) {
      segment = vjmlt.receive();
      if (segment.status() == VJMLMessageSegment.Success) {
        if (SysLog.printLevel > 1)
          SysLog.println("main: Got ACK, continuing..");
        continue;
      }
      if (cnt > 0 && (cnt % reportIters) == 0) {
        now = System.currentTimeMillis();
        msec = (int)(now - then);
        System.out.println("Iter " + cnt + " bps " + 1350 * reportIters * 1000/msec +
                            " msec " + msec);
        then = now;
      }

      // Readdress, then resend
      String addr = "";
      try {
        addr = (segment.inetAddress()).getHostAddress() + ":4450";
      } catch (Exception e) {
        e.printStackTrace();
      }
      segment.address(addr);
      vjmlt.send(segment);
      cnt++;
    }
  }
}
