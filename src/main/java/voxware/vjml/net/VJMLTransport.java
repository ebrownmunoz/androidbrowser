package voxware.vjml.net;

import java.util.Vector;
import java.lang.Thread;
import java.io.*;
import java.net.InetAddress;

import voxware.util.*;
import voxware.vjml.VJMLMessageSegment;

class MsgThread extends Thread {
  Mailbox mbox;

  MsgThread(String name) {
    super(name);
    this.mbox = new Mailbox();
    this.setDaemon(true);
  }

  void putMsg(Object obj) {
    mbox.insert(obj);
  }

  void putVector(Vector v) {
    if (v != null) {
      int cnt = v.size();
      for (int i = 0; i < cnt; i++) {
        putMsg(v.elementAt(i));
      }
    }
  }

  Object getMsg() throws InterruptedException {
    return mbox.remove();
  }

  int msgCnt() {
    return mbox.numobj();
  }

  Vector flush() {
    return mbox.flush();
  }
}

class SendThread extends MsgThread {
  VJMLTransport parent;

  SendThread(VJMLTransport parent) {
    super("VJMLTransportSender");
    this.parent = parent;
  }

  public void run() {
    try {
      while (true) {
        Object obj = getMsg();
        VJMLMessageSegment segment = (VJMLMessageSegment) obj;

        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
          SysLog.println("VJMLTransport.run: calling UDPTransport.send");
        int status = parent.udpt.send(segment);

        VJMLMessageSegment statseg = new VJMLMessageSegment(null, segment.address(), status, segment.priority);
        parent.receiveThread.putMsg(statseg);
      }
    } catch (InterruptedException e) {
      // Just fall through to terminate run()
    }
  }
}

class ReceiveThread extends MsgThread {
  VJMLTransport parent;

  ReceiveThread(VJMLTransport parent) {
    super("VJMLTransportReceiver");
    this.parent = parent;
  }

  public void run() {
    while (true) {
      VJMLMessageSegment segment = parent.udpt.receive();
      putMsg(segment);
    }
  }
}

public class VJMLTransport {
  ReceiveThread receiveThread = null;
  SendThread    sendThread = null;
  UDPTransport  udpt = null;

  public static final int RTTRxmitMax = 100;
  public static final int RTTRxmitMin = 101;
  public static final int RTTMaxRxmit = 102;

  public VJMLTransport() throws IOException {
    udpt = new UDPTransport();
    initialize();
  }

  public VJMLTransport(ClassLoader classLoader) throws IOException {
    udpt = new UDPTransport(classLoader);
    initialize();
  }

  public VJMLTransport(int udpListenPort, ClassLoader classLoader) throws IOException {
    udpt = new UDPTransport(udpListenPort, classLoader);
    initialize();
  }

  public VJMLTransport(int udpListenPort, InetAddress address, ClassLoader classLoader) throws IOException {
    udpt = new UDPTransport(udpListenPort, address, classLoader);
    initialize();
  }

  public VJMLTransport(int sendPort, int udpListenPort, ClassLoader classLoader) throws IOException {
    udpt = new UDPTransport(sendPort, udpListenPort, classLoader);
    initialize();
  }

  public VJMLTransport(int sendPort, int udpListenPort, InetAddress address, ClassLoader classLoader) throws IOException {
    udpt = new UDPTransport(sendPort, udpListenPort, address, classLoader);
    initialize();
  }

  private void initialize() {
    receiveThread = new ReceiveThread(this);
    sendThread = new SendThread(this);

    receiveThread.start();
    sendThread.start();
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
      SysLog.println("VJMLTransport: instantiated and started");
  }

  public void parameter(int index, int value) throws IllegalArgumentException {
    udpt.parameter(index, value);
  }

  public int parameter(int index) throws IllegalArgumentException {
    return udpt.parameter(index);
  }

  public InetAddress localAddress() {
    return udpt.localAddress();
  }

  public void send(VJMLMessageSegment segment) {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
      SysLog.println("VJMLTransport.send: called");
    sendThread.putMsg(segment);
  }

  public VJMLMessageSegment receive() throws InterruptedException {
    return (VJMLMessageSegment)receiveThread.getMsg();
  }
}
