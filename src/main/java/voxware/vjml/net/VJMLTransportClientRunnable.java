package voxware.vjml.net;

import java.io.*;
import java.net.*;
import java.util.*;

import voxware.vjml.VJMLMessageSegment;

public class VJMLTransportClientRunnable implements Runnable {

  public void run() {
    boolean init = false;
    String  destAddr = "";

    VJMLTransport vjmlt = null;
    try {
      vjmlt = new VJMLTransport();
      System.out.println("VJMLTransportClientRunnable.main: starting at IP address " + vjmlt.localAddress().getHostAddress());
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }

    // Create a segment and send it to get things going
    VJMLMessageSegment segment = null;
    if (init) {
      System.out.println("main: INFO - initializing loop w/destAddr " + destAddr);
      int ret;
      String str = "ThisIsATestString";
      String addr = destAddr + ":4450";
      segment = new VJMLMessageSegment(str.getBytes(), addr);
      vjmlt.send(segment);
    }

    // Await a response.  Discard ACKs, resend the segment
    while (true) {
      try {
        segment = vjmlt.receive();
      } catch (InterruptedException e) {
        return;
      }
      if (segment.status() == VJMLMessageSegment.Success) {
        continue;
      }

      // Readdress, then resend
      String addr = "";
      try {
        addr = (segment.inetAddress()).getHostAddress() + ":4450";
      } catch (Exception e) {
        e.printStackTrace();
      }
      segment.address(addr);
      vjmlt.send(segment);
    }
  }
}
