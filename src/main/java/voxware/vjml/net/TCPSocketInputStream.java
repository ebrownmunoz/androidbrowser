package voxware.vjml.net;

import java.io.IOException;

class TCPSocketInputStream extends java.io.InputStream
{
  private boolean eof;
  private TCPSocket tcpsock;
  private byte temp[] = new byte[1];

  TCPSocketInputStream(TCPSocket tcpsock) throws IOException {
   	this.tcpsock = tcpsock;
  }

  public int read(byte b[]) throws IOException {
   	return read(b, 0, b.length);
  }

  public int read(byte b[], int off, int length) throws IOException {
   	if (eof) {
      return -1;
   	}

   	int n = Read(tcpsock, b, off, length);
   	if (n <= 0) {
 	    eof = true;
 	    return -1;
   	}
   	return n;
  }

  public int read() throws IOException {
   	if (eof) {
 	    return -1;
   	}

   	int n = read(temp, 0, 1);
     	if (n <= 0) {
 	    return -1;
   	}
   	return temp[0] & 0xff;
  }

  public long skip(long numbytes) throws IOException {
   	if (numbytes <= 0) {
 	    return 0;
   	}
   	long n = numbytes;
   	int buflen = (int) Math.min(1024, n);
   	byte data[] = new byte[buflen];
   	while (n > 0) {
 	    int r = read(data, 0, (int) Math.min((long) buflen, n));
	     if (r < 0) {
	      	break;
      }
	     n -= r;
	   }
   	return numbytes - n;
  }

  public int available() throws IOException {
   	return tcpsock.available();
  }

  public void close() throws IOException {
   	tcpsock.close();
  }

  // Override finalize, the fd is closed by the Socket.
  protected void finalize() {}

  private native int Read(TCPSocket tcpsock, byte ba[], int offset, int len)	throws IOException;
}

