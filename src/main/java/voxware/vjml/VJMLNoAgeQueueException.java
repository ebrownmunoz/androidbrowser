package voxware.vjml;

import java.lang.*;

public class VJMLNoAgeQueueException extends VJMLException {

  public VJMLNoAgeQueueException(Object offender) {
    super(offender);
  }
}
