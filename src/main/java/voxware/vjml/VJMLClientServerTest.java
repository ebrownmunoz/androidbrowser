package voxware.vjml;

import java.lang.*;
import java.util.*;
import java.io.*;

import voxware.net.NameValuePair;
import voxware.util.*;

public class VJMLClientServerTest {

  public static void main(String[] args) throws IOException {

    int     msgCount = 0;
    String  destAddr = null;
    String  chanName = null;
    int     priority = VJMLMessage.NormalPriority;
    int     tailprnt = 1;
    int     maxPairs = 0;

    SysLog.initialize(System.out);
    SysLog.clearShowStamp();

    VJMLAttributes attributes = new VJMLAttributes();

     // Parse the command line
    for (int i = 0; i < args.length; i++) {
      String arg = args[i];

       // Options
      if (arg.startsWith("-")) {

        if (arg.equals("-addr")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing server IP address");
            return;
          }
          destAddr = args[++i];                    // Server IP address (including port if different from default)
          continue;
        }

        if (arg.equals("-batt")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing batch threshold");
            return;
          }
          attributes.batchThreshold(Integer.parseInt(args[++i]));  // The batching threshold (priority)
          continue;
        }

        if (arg.equals("-hark")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing hark boolean");
            return;
          }
          if (Boolean.valueOf(args[++i]).booleanValue()) attributes.listener(new VJMLListenerImpl());  // The tagAlong Boolean
          continue;
        }

        if (arg.equals("-mask")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing print mask");
            return;
          }
          SysLog.setPrintMask(Long.parseLong(args[++i], 16));
          continue;
        }

        if (arg.equals("-name")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing channel name");
            return;
          }
          chanName = args[++i];                    // Channel name (generated randomly if not present)
          continue;
        }

        if (arg.equals("-nump")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing number of priorities");
            return;
          }
          attributes.numPriorities(Integer.parseInt(args[++i]));   // Message priorities (highest = 0; lowest = numPriorities - 1)
          continue;
        }

        if (arg.equals("-oint")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing output interval");
            return;
          }
          attributes.outputInterval(Long.parseLong(args[++i]));    // Output interval in milliseconds
          continue;
        }

        if (arg.equals("-port")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing local port name");
            return;
          }
          attributes.defaultReceivePort(Integer.parseInt(args[++i]));  // Local and default remote receive port
          continue;
        }

        if (arg.equals("-prio")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing sending priority");
            return;
          }
          priority = Integer.parseInt(args[++i]);  // Message priority (highest = 0; lowest = numPriorities - 1)
          continue;
        }

        if (arg.equals("-prnt")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing print level");
            return;
          }
          SysLog.setPrintLevel(Integer.parseInt(args[++i]));  // Diagnostic print level (0 - 5)
          continue;
        }

        if (arg.equals("-size")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing maximum number of NVPairs");
            return;
          }
          maxPairs = Integer.parseInt(args[++i]);  // Maximum number of NVPairs in a message
          continue;
        }

        if (arg.equals("-sprt")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing sending port name");
            System.exit(1);
          }
          attributes.sendPort(Integer.parseInt(args[++i]));  // Local sending port
          continue;
        }

        if (arg.equals("-taga")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing tagAlong boolean");
            return;
          }
          attributes.tagAlong(Boolean.valueOf(args[++i]).booleanValue());   // The tagAlong Boolean
          continue;
        }

        if (arg.equals("-tail")) {
          if (i == args.length - 1 || args[i + 1].startsWith("-")) {
            SysLog.println("main(): ERROR - missing tail count");
            return;
          }
          tailprnt = Integer.parseInt(args[++i]);  // Number of tailing NVPairs to print
          continue;
        }
      } else {
        SysLog.println("main(): ERROR - invalid command line syntax");
      }
    }

    VJML  vjml = new VJML(attributes);

    VJMLChannel    channel = null;
    VJMLMessage    message  = null;
    Vector         nVPs = null;
    String         strIn = null;
    NameValuePair  nVPIn = null;
    NameValuePair  nVPOut = null;
    String prefix;

     // If there is a server IP address on the command line, construct a CLIENT (originator) and send the first message
    if (destAddr != null) {
      prefix = "Client";
      try {
         // If no globally unique channel name has been specified on the command line, make one up
        if (chanName == null) chanName = "CHAN_" + Integer.toHexString((new Random()).nextInt());  // This is likely to be unique
        channel = vjml.channel(chanName, destAddr);
      } catch (VJMLNameConflictException e) {
        SysLog.println("main(): ERROR - " + e.toString());
        e.printStackTrace();
      }
      nVPOut = new NameValuePair("Original Client Message", Integer.toString(msgCount++));
      message = new VJMLMessage(nVPOut, priority);
      channel.putMessage(message);
      SysLog.println("Client sent original message on channel " + channel.name());

     // Otherwise, construct a SERVER (listener)
    } else {
      prefix = "Server";
    }

    if (SysLog.printLevel == 0) 
      SysLog.println(prefix + " waiting for message on " + vjml.numChannels() + " channel(s)");

    while (true) {
      if (SysLog.printLevel > 0) 
        SysLog.println(prefix + " waiting for message on " + vjml.numChannels() + " channel(s)");
      try {
        channel = vjml.oldestChannel();
        if (!channel.conduit().intact()) SysLog.println(prefix + " finds peer on \"" + channel.name() + "\" out of range");
        message = channel.getMessage();
      } catch (InterruptedException e) {
        return;
      } catch (VJMLNoAgeQueueException e) {
        return;
      }
      Object object = message.content();
      if (object instanceof String) {
        strIn = (String)object;
        if (SysLog.printLevel > 0) {
           // Strip off newline, if any
          if (strIn.endsWith("\n")) strIn = strIn.substring(0, strIn.length() - 1);
          SysLog.println(prefix + " received [\"" + strIn + "\"] on channel " + channel.name());
        } else {
          SysLog.print(strIn);
        }
        continue;
      } else if (object instanceof NameValuePair) {
        nVPIn = (NameValuePair)object;
        if (SysLog.printLevel > 0) 
          SysLog.println(prefix + " received [\"" + nVPIn.name() + "\", \"" + nVPIn.value() + "\"] on channel " + channel.name());
        nVPs = new Vector();
        nVPs.addElement(nVPIn);
      } else if (object instanceof Vector) {
        nVPs = (Vector)object;
        if (SysLog.printLevel > 0 || tailprnt > 0) {
          SysLog.print(prefix + " received Vector of " + nVPs.size() + " NVPairs on channel " + channel.name());
          if (tailprnt > 0) {
            SysLog.println(", ending with:");
            int size = nVPs.size();
            NameValuePair nVP;
            for (int index = (size - tailprnt < 0) ? 0 : size - tailprnt; index < size; index++) {
               nVP = (NameValuePair)nVPs.elementAt(index);
               SysLog.println("    [\"" + nVP.name() + "\", \"" + nVP.value() + "\"]");
            }
          } else {
            SysLog.println("");
          }
        }
      } else {
        nVPs = new Vector();
      }
      nVPOut = new NameValuePair(prefix + "Message", Integer.toString(msgCount++));
      nVPs.addElement(nVPOut);
      if (maxPairs > 0) while (nVPs.size() > maxPairs) nVPs.removeElementAt(0);
      message = new VJMLMessage(nVPs, message.priority());
      channel.putMessage(message);
      if (SysLog.printLevel > 0 || tailprnt > 0) 
        SysLog.println(prefix + " sent Vector of " + nVPs.size() + " NVPairs on channel " + channel.name());
    }
  }
}

class VJMLListenerImpl implements VJMLListener {

   // Report an exception
  public void exception(Throwable exception) {
    SysLog.println("VJMLListener.exception: " + exception.toString());
  }

   // Report an incoming message
  public void message(VJMLChannel channel) {
    if (SysLog.printLevel > 0) SysLog.println("VJMLListener.message: message arrived on channel \"" + channel.name() + "\"");
  }
}
