package voxware.vjml;

import java.lang.*;

public class VJMLOutOfRangeException extends VJMLException {

  public VJMLOutOfRangeException(Object offender) {
    super(offender);
  }
}
