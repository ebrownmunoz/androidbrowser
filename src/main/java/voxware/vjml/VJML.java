package voxware.vjml;

import java.lang.*;
import java.io.*;
import java.net.InetAddress;
import java.util.*;

import voxware.util.*;
import voxware.vjml.net.*;

public class VJML {

  // VJML Exports (EXPORTED BY THE voxware.vjml PACKAGE)

  public static final long DefaultRetryInterval = 20000;   // Default milliseconds between transmission attempts when peer is inaccessible
  public static final long DefaultOutputInterval = 10000;  // Default milliseconds between normal priority message transmission attempts
  public static final int  DefaultReceivePort = 4450;      // Default port number for both local and remote channels to receive messages on

  public VJML(VJMLAttributes attributes) throws IOException {
    if (attributes == null) attributes = new VJMLAttributes();
    construct(attributes);                                 // Throws IOException
  }

  public VJML() throws IOException {
    VJMLAttributes attributes = new VJMLAttributes();
    construct(attributes);                                 // Throws IOException
  }

   // Return the number of priorities (highest is always 0; lowest is numPriorities - 1)
  public int numPriorities() {
    return this.numPriorities;
  }

   // Install a VJMLListener, replacing and returning the existing one, if any (DEPRECATED)
  public VJMLListener listener(VJMLListener listener) {
    VJMLListener oldListener;
    try {
      oldListener = (VJMLListener)listeners.firstElement();
    } catch (NoSuchElementException e) {
      oldListener = null;
    }
    listeners.removeAllElements();
    if (listener != null) listeners.addElement(listener);
    return oldListener;
  }

   // Install a VJMLListener
  public void installListener(VJMLListener listener) {
    listeners.addElement(listener);
  }

   // Remove a VJMLListener
  public void removeListener(VJMLListener listener) {
    listeners.removeElement(listener);
  }

   // Return the VJMLTransport that this VJML uses (to access its parameter() methods)
  public VJMLTransport transport() {
    return transport;
  }

   // Return the VJMLChannel having the given name and destination IP address, constructing it first if it doesn't already exist
  public VJMLChannel channel(String name, String destination) throws VJMLNameConflictException {
    VJMLChannel channel = (VJMLChannel)channels.get(name);
     // If the channel doesn't exist, construct it and register it with this VJML
    if (channel == null) {
       // Supply a default port number if none is present
      if (destination.indexOf(":") < 0) destination = destination + ":" + Integer.toString(defaultRcvPort);
      channel = new VJMLChannel(this, name, destination);
       // Register the channel with this vjml
      channels.put(channel.name(), channel);
       // Get the conduit to the channel's destination address, constructing it if necessary
      VJMLConduit conduit = conduit(channel.address());
       // Attach the channel to the conduit
      channel.conduit(conduit);
      conduit.addChannel(channel);
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
        SysLog.println("VJML.channel: registered a new channel \"" + channel.name() + "\" with destination address " + channel.address());
    } else if (channel.address() != destination) {
      throw new VJMLNameConflictException(channel, "Address conflict: " + channel.address() + " != " + destination);
    }
    return channel;
  }

   // Return the VJMLChannel having the given name, or null if none exists
  public VJMLChannel channel(String name) {
    return (VJMLChannel)channels.get(name);
  }

   // Return an array of all the VJMLChannels implicitly created to service incoming messages since the last call to this method
  public VJMLChannel[] newChannels() {
    VJMLChannel[] array = null;
    synchronized (newChannels) {
      array = new VJMLChannel[newChannels.size()];
      newChannels.copyInto(array);
      newChannels.removeAllElements();
    }
    return array;
  }

   // Return the number of channels currently in this VJML
  public int numChannels() {
    return channels.size();
  }

   // Enumerate the channel names
  public Enumeration channelNames() {
    return channels.keys();
  }

   // Enumerate the channels
  public Enumeration channels() {
    return channels.elements();
  }

   // Return the VJMLChannel whose in-box contains the oldest message at the highest priority, blocking if none
  public VJMLChannel oldestChannel() throws VJMLNoAgeQueueException, InterruptedException {
    if (ageQueue == null) throw new VJMLNoAgeQueueException(this);
    VJMLMessage message = (VJMLMessage)ageQueue.peek();
    return (message != null) ? (VJMLChannel)channels.get(message.channel) : null;
  }

   // Return the VJMLChannel whose in-box contains the oldest message at the highest priority, blocking until one arrives or timeout is exceeded
  public VJMLChannel oldestChannel(long timeout) throws VJMLNoAgeQueueException, InterruptedException {
    if (ageQueue == null) throw new VJMLNoAgeQueueException(this);
    VJMLMessage message = (VJMLMessage)ageQueue.peek(timeout);
    return (message != null) ? (VJMLChannel)channels.get(message.channel) : null;
  }

   // Return the VJMLChannel whose in-box contains the oldest message at the highest priority, or null if none
  public VJMLChannel oldestChannelNow() throws VJMLNoAgeQueueException {
    if (ageQueue == null) throw new VJMLNoAgeQueueException(this);
    VJMLMessage message = (VJMLMessage)ageQueue.peekNow();
    return (message != null) ? (VJMLChannel)channels.get(message.channel) : null;
  }

   // Return the VJMLChannel whose in-box contains the oldest message at the given priority, or null if none
  public VJMLChannel oldestChannelNow(int priority) throws VJMLNoAgeQueueException {
    if (ageQueue == null) throw new VJMLNoAgeQueueException(this);
    priority = hardLimitedPriority(priority);
    VJMLMessage message = (VJMLMessage)ageQueue.peekNow(priority);
    return (message != null) ? (VJMLChannel)channels.get(message.channel) : null;
  }

   // Send the message at its internally specified priority on every channel
  public void broadcast(VJMLMessage message) {
    int priority =  hardLimitedPriority(message.priority);
    broadcast(message, priority);
  }

   // Send the message at the specified priority on every channel
  public void broadcast(VJMLMessage message, int priority) {
    priority = hardLimitedPriority(priority);
    Enumeration enumeration = channels.elements();
    while (enumeration.hasMoreElements()) ((VJMLChannel)enumeration.nextElement()).putMessage(message, priority);
  }

   // Send all messages currently waiting to be sent in all channels, regardless of the outputInterval
  public void sendNow() {
    Enumeration enumeration = conduits.elements();
    while (enumeration.hasMoreElements()) send((VJMLConduit)enumeration.nextElement());
  }

   // Return the total number of messages available in the in-boxes of all channels
  public int messageCount() throws VJMLNoAgeQueueException {
    if (ageQueue == null) throw new VJMLNoAgeQueueException(this);
    return ageQueue.numobj();
  }

   // Return the total number of messages available in the in-boxes of all channels at the specified priority
  public int messageCount(int priority) throws VJMLNoAgeQueueException {
    if (ageQueue == null) throw new VJMLNoAgeQueueException(this);
    priority = hardLimitedPriority(priority);
    return ageQueue.numobj(priority);
  }

   // Return the retry interval (the time in milliseconds between retransmission attempts when the remote host is inaccessible)
  public long retryInterval() {
    return retryInterval;
  }

   // Set the retry interval (the time in milliseconds between retransmission attempts when the remote host is inaccessible)
  public void retryInterval(long interval) {
    this.retryInterval = interval;
    retryDaemon.wake();
  }

   // Return the output interval (the time in milliseconds between batch priority message transmission attempts during normal operation)
  public long outputInterval() {
    return outputInterval;
  }

   // Set the output interval (the time in milliseconds between batch priority message transmission attempts during normal operation)
  public void outputInterval(long interval) {
     // A value of zero causes VJML never to send batched messages implicitly
    this.outputInterval = interval;
    senderDaemon.wake();
  }

   // Return the highest priority subject to message batching (default is VJMLMessage.DefaultBatchThreshold)
  public int batchThreshold() {
    return batchThreshold;
  }

   // Set the highest priority subject to message batching (default is VJMLMessage.DefaultBatchThreshold)
  public void batchThreshold(int priority) {
     // Setting this to numPriorities defeats all message batching
    this.batchThreshold = priority;
  }

   // Report whether messages waiting at batch priorities tag along after any high priority messages (default is true)
  public boolean tagAlong() {
    return tagAlong;
  }

   // Set whether messages waiting at batch priorities tag along after any high priority messages (default is true)
  public void tagAlong(boolean tagAlong) {
    this.tagAlong = tagAlong;
  }

   // Discard all messages in the in-boxes of all channels
  public void flush() {
    Enumeration enumeration = channels.elements();
    while (enumeration.hasMoreElements()) ((VJMLChannel)enumeration.nextElement()).flush();
  }

   // Discard all messages in the in-boxes of all channels at the given priority
  public void flush(int priority) {
    priority = hardLimitedPriority(priority);
    Enumeration enumeration = channels.elements();
    while (enumeration.hasMoreElements()) ((VJMLChannel)enumeration.nextElement()).flush(priority);
  }

  // VJML MEMBERS NOT EXPORTED BY THE voxware.vjml PACKAGE

  static int instanceCount = 0;

          int             numPriorities = VJMLMessage.DefaultNumPriorities;
  private int             defaultRcvPort = DefaultReceivePort;     // The default port number for remote and local channels to receive messages on
  private VJMLTransport   transport;
  private long            retryInterval = DefaultRetryInterval;    // Milliseconds between transmission attempts when peer is inaccessible
  private long            outputInterval = DefaultOutputInterval;  // Milliseconds between normal priority message transmission attempts
          int             batchThreshold = VJMLMessage.DefaultBatchThreshold;
          boolean         tagAlong = true;                         // Whether sending higher priority messages provokes sending batched ones
  private Vector          listeners;
  public  ClassLoader     classLoader;
  private Hashtable       channels;                                // The VJMLChannels hashed by channel name
  private Hashtable       conduits;                                // The VJMLConduits hashed by target IP address
  private Mailbox         ageQueue;                                // Prioritized queue of incoming messages
  private Vector          newChannels;
  private ReceiverThread  receiverDaemon;
  private RetryThread     retryDaemon;
  private SenderThread    senderDaemon;

  private class ReceiverThread extends Thread {

    ReceiverThread() {
      super("VJMLReceiver" + Integer.toString(instanceCount));
      this.setDaemon(true);
    }

    public void run() {
      if (classLoader != null) {
        // System.out.println("VJML: ReceiverThread using non-default classloader " + classLoader);
        Thread.currentThread().setContextClassLoader(classLoader);
      }
      try {
        receive();
      } catch (InterruptedException e) {
        // Fall through to terminate run()
      }
    }
  }

  private void construct(VJMLAttributes attributes) throws IOException {
    this.numPriorities = attributes.numPriorities;
    this.defaultRcvPort = attributes.defaultRcvPort;
    if (attributes.inetAddress == null) {
      if (attributes.sendPort == null) transport = new VJMLTransport(attributes.defaultRcvPort, attributes.classLoader);  // Throws IOException
      else                             transport = new VJMLTransport(attributes.sendPort.intValue(), attributes.defaultRcvPort, attributes.classLoader);  // Throws IOException
    } else {
      if (attributes.sendPort == null) transport = new VJMLTransport(attributes.defaultRcvPort, attributes.inetAddress, attributes.classLoader);  // Throws IOException
      else                             transport = new VJMLTransport(attributes.sendPort.intValue(), attributes.defaultRcvPort, attributes.inetAddress, attributes.classLoader);  // Throws IOException
    }
    this.retryInterval = attributes.retryInterval;
    this.outputInterval = attributes.outputInterval;
    this.batchThreshold = attributes.batchThreshold;
    this.tagAlong = attributes.tagAlong;
    channels = new Hashtable(3);                                   // The VJMLChannels hashed by name
    conduits = new Hashtable(3);                                   // The VJMLConduits hashed by target IP address
    ageQueue = attributes.maintainAgeQueue ? new Mailbox(numPriorities) : null;
    newChannels = new Vector();
    this.listeners = attributes.listeners;
    this.classLoader = attributes.classLoader;
    receiverDaemon = new ReceiverThread();
    retryDaemon = new RetryThread(this);
    senderDaemon = new SenderThread(this);
    instanceCount++;
    receiverDaemon.start();
    retryDaemon.start();
    senderDaemon.start();
  }

   // Report an exception to the specified listeners (or, if none, to this.listeners) and optionally print the stack trace
  void exception(Vector listeners, Throwable throwable, int level) {
    exception(listeners, throwable);
    if (SysLog.printLevel >= level) throwable.printStackTrace(SysLog.stream);
  }

   // Report an exception to the specified listeners (or, if none, to the this.listeners)
  void exception(Vector listeners, Throwable throwable) {
    if (listeners.isEmpty()) listeners = this.listeners;
    Enumeration listenerEnum = listeners.elements();
    while (listenerEnum.hasMoreElements())
      ((VJMLListener)listenerEnum.nextElement()).exception(throwable);
  }

   // Report an exception to this.listeners and optionally print the stack trace
  void exception(Throwable throwable, int level) {
    exception(listeners, throwable, level);
  }

   // Report an exception to this.listeners and print the stack trace
  void exception(Throwable throwable) {
    exception(throwable, 0);
  }

  int hardLimitedPriority(int priority) {
    if      (priority < VJMLMessage.HighestPriority) priority = VJMLMessage.HighestPriority;
    else if (priority >= numPriorities) priority = numPriorities - 1;
    return priority;
  }

   // Return the VJMLConduit for the given destination address, constructing it if none exists
  private VJMLConduit conduit(String address) {
     // First see if one already exists
    VJMLConduit conduit = (VJMLConduit)conduits.get(address);
     // If not, construct one and hash it in the conduits table by its address String
    if (conduit == null) {
      conduit = new VJMLConduit(this, address);
      conduits.put(address, conduit);
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
        SysLog.println("VJML.conduit: registered a new conduit with destination address " + address);
    }
    return conduit;
  }

   // Remove the VJMLConduit having the given target IP address
  void removeConduit(String address) {
    conduits.remove(address);
  }

   // Remove the VJMLChannel having the given name, or do nothing if such a channel does not exist
  void removeChannel(String name) {
    VJMLChannel channel = (VJMLChannel)channels.remove(name);
    if (channel != null) {
      synchronized (newChannels) {
        newChannels.removeElement(channel);
      }
    }
  }

   // Send a VJMLMessage through the given VJMLConduit
  void send(VJMLMessage message, VJMLConduit conduit) {
    conduit.encode(message, message.priority);
     // Send a segment if the conduit is not busy; otherwise, just leave it for someone else to send later
    VJMLMessageSegment segment = conduit.peekAndTakeToken();
    if (segment != null) {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
        SysLog.println("VJML.send(VJMLMessage): sending segment with address = " + segment.address);
      transport.send(segment);
    }
  }

   // Consolidate all outgoing messages accumulated in a conduit and send them
  void send(VJMLConduit conduit) {
    conduit.encode();
       // Send a segment if the conduit is not busy; otherwise, just leave it for someone else to send later
    VJMLMessageSegment segment = conduit.peekAndTakeToken();
    if (segment != null) {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
        SysLog.println("VJML.send(): sending segment with address = " + segment.address);
      transport.send(segment);
    }
  }

   // Try to send the oldest VJMLMessageSegment languishing in each VJMLConduit
  void retry() {
    Enumeration queues = conduits.elements();
    while (queues.hasMoreElements()) {
       // Send a segment if the conduit is not busy; otherwise, just leave it for someone else to send later
      VJMLMessageSegment segment = (VJMLMessageSegment)((VJMLConduit)queues.nextElement()).peekAndTakeToken();
      if (segment != null) {
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
          SysLog.println("VJML.retry: sending segment with address = " + segment.address);
        transport.send(segment);
      }
    }
  }

   // Return the channel having the given name, constructing it and listing it as new if it does not exist
  private VJMLChannel channelByName(String name, String address) throws VJMLNameConflictException {
    VJMLChannel channel = channel(name);
     // If no such channel exists, construct one
    if (channel == null) {
      channel = channel(name, address);          // Throws VJMLNameConflictException
      synchronized (newChannels) {
        newChannels.addElement(channel);
      }
    }
    return channel;
  }

   // Have the given VJMLChannel receive a VJMLMessage or Vector of VJMLMessages
  private void receive(VJMLChannel channel, Object object) {
    VJMLConduit conduit = channel.conduit();
     // Obviously the channel is intact, since a message just arrived
    conduit.intact(true);
     // Have the channel receive the message or Vector of messages
    channel.receive(object);
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
      SysLog.println("VJML.receive: received " + object.toString() + " on channel " + channel.toString());
     // Notify the channel or conduit or vxml listeners that there are new messages in the channel
    if (!channel.notifyListener() && !conduit.notifyListeners(channel))
        notifyListeners(channel);
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
      SysLog.println("VJML.receive: notified listener that " +  object.toString() + " was received on " + channel.toString());
  }

   // Notify the listeners that messages have arrived
  private void notifyListeners(VJMLChannel channel) {
    if (!listeners.isEmpty()) {
      Enumeration listenerEnum = listeners.elements();
      while (listenerEnum.hasMoreElements())
      ((VJMLListener)listenerEnum.nextElement()).message(channel);
    }
  }

   // Forever receive incoming VJMLMessageSegments, reconstituting incoming messages and dispatching them to their target channels
  private void receive() throws InterruptedException {
    VJMLConduit conduit = null;
    VJMLMessageSegment segment = null;
    VJMLMessageSegment segmentOut;
    VJMLChannel channel;
    VJMLMessage message;
    Object object;
    while (true) {
       // Wait for the next segment to arrive
      segment = transport.receive();    // Throws InterruptedException
      switch (segment.status) {
        case VJMLMessageSegment.Success:
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
            SysLog.println("VJML.receive: Success segment - address = " + segment.address + ", priority = " + segment.priority);
          conduit = (VJMLConduit)conduits.get(segment.address);
          if (conduit != null) {
            conduit.intact(true);
            conduit.removeAndReturnToken(segment.priority);
             // Send the next segment, if any, queued for transmission in this conduit
            segmentOut = (VJMLMessageSegment)conduit.peekAndTakeToken();
            if (segmentOut != null) transport.send(segmentOut);
          }
          break;
        case VJMLMessageSegment.TerminalSegment:
        case VJMLMessageSegment.InternalSegment:
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
            SysLog.println("VJML.receive: Incoming segment - address = " + segment.address + ", priority = " + segment.priority);
           // Accumulate the incoming segment into a VJMLSegmentedMessage
          conduit = conduit(segment.address);
          conduit.intact(true);
           // Send the next segment queued for transmission in this conduit, if any
          segmentOut = (VJMLMessageSegment)conduit.peekAndTakeToken();
          if (segmentOut != null) transport.send(segmentOut);
           // If the VJMLSegmentedMessage is complete, decode it and dispatch its constituent VJMLMessages to their target channels
          object = conduit.decode(segment);
          if (object != null) {
            try {
              if (object instanceof VJMLMessage) {
                message = (VJMLMessage)object;
                channel = channelByName(message.channel, segment.address);
                receive(channel, message);
              } else if (object instanceof Vector) {
                Enumeration clusters = ((Vector)object).elements();
                while (clusters.hasMoreElements()) {
                  Object element = clusters.nextElement();
                  if (element instanceof VJMLMessage) {
                    message = (VJMLMessage)element;
                    channel = channelByName(message.channel, segment.address);
                    receive(channel, message);
                  } else if (element instanceof Vector) {
                    channel = channelByName(((VJMLMessage)((Vector)element).firstElement()).channel, segment.address);
                    receive(channel, (Vector)element);
                  } else {
                    exception(new RuntimeException("VJML.receive: UNEXPECTED " + element.getClass().getName() + " in message")); // Software Error
                  }
                }
              } else {
                exception(new RuntimeException("VJML.receive: UNEXPECTED " + object.getClass().getName() + " in message")); // Software Error
              }
            } catch (VJMLNameConflictException exception) {
              exception((conduit != null) ? conduit.listeners : null, exception);
            }
          }
          break;
        case VJMLMessageSegment.OutOfRange:
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
            SysLog.println("VJML.receive: OutOfRange segment - address = " + segment.address + ", priority = " + segment.priority);
          conduit = (VJMLConduit)conduits.get(segment.address);
          if (conduit != null) {
            conduit.intact(false);
            conduit.returnToken();
          }
          break;
        case VJMLMessageSegment.NetworkFailure:
        case VJMLMessageSegment.QueueOverflow:
        case VJMLMessageSegment.SerializeError:
        case VJMLMessageSegment.JavaException:
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
            SysLog.println("VJML.receive: Error segment - address = " + segment.address + ", priority = " + segment.priority);
          conduit = (VJMLConduit)conduits.get(segment.address);
          if (conduit != null) conduit.removeAndReturnToken(segment.priority);
           // Notify the listener that an error has occurred
          exception((conduit != null) ? conduit.listeners : null, new VJMLException(conduit, "VJML.receive - ERROR: " + Integer.toString(segment.status)), 1);
          break;
        default:
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
            SysLog.println("VJML.receive: Unknown segment - address = " + segment.address + ", priority = " + segment.priority);
          break;
      }
    }
  }

   // Serialize a Vector of Vectors of VJMLMessages into a byte array
  private byte[] encode(Vector clusters) {
    byte data[] = null;
    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(clusters);
      oos.flush();
      oos.close();
      data = baos.toByteArray();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return data;
  }

   // De-serialize a byte array into a Vector of Vectors of VJMLMessages
  private Vector decode(byte[] array) {
    Vector clusters = null;
    try {
      ByteArrayInputStream bais = new ByteArrayInputStream(array);
      ObjectInputStream ois = new ObjectInputStream(bais);
      clusters = (Vector)ois.readObject();
      ois.close();
      bais.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return clusters;
  }

   // Insert a message into the age queue
  void insert(VJMLMessage message) {
    if (ageQueue != null) ageQueue.insert(message, message.priority);
  }

   // Insert a cluster of messages into the age queue
  void insert(Vector messages) {
    if (ageQueue != null) {
      Enumeration enumeration = messages.elements();
      while (enumeration.hasMoreElements()) {
        VJMLMessage message = (VJMLMessage)enumeration.nextElement();
        ageQueue.insert(message, message.priority);
      }
    }
  }

   // Remove a message from the age queue
  void remove(VJMLMessage message) {
    if (ageQueue != null) ageQueue.removeInstance(message, message.priority);
  }

   // Remove a cluster of messages from the age queue
  void remove(Vector messages) {
    if (messages != null && ageQueue != null) {
      Enumeration enumeration = messages.elements();
      while (enumeration.hasMoreElements()) {
        VJMLMessage message = (VJMLMessage)enumeration.nextElement();
        ageQueue.removeInstance(message, message.priority);
      }
    }
  }
}

class RetryThread extends Thread {

  private VJML parent;

  RetryThread(VJML parent) {
    super("VJMLRetry" + Integer.toString(parent.instanceCount));
    this.parent = parent;
    this.setDaemon(true);
  }

  public synchronized void run() {
    while (true) {
      try {
        wait(parent.retryInterval());
        parent.retry();
      } catch (InterruptedException e) {
        // Fall through to terminate run()
      }
    }
  }

  synchronized void wake() {
    notify();
  }
}

class SenderThread extends Thread {

  private VJML parent;

  SenderThread(VJML parent) {
    super("VJMLSender" + Integer.toString(parent.instanceCount));
    this.parent = parent;
    this.setDaemon(true);
  }

  public synchronized void run() {
    if (parent.classLoader != null) {
      // System.out.println("VJML: SenderThread using non-default classloader " + parent.classLoader);
      Thread.currentThread().setContextClassLoader(parent.classLoader);
    }
    while (true) {
      try {
        wait(parent.outputInterval());
        parent.sendNow();
      } catch (InterruptedException e) {
        // Fall through to terminate run()
      }
    }
  }

  synchronized void wake() {
    notify();
  }
}

