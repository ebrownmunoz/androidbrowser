#ifndef __voxware_engine_recognition_BaseResult__
#define __voxware_engine_recognition_BaseResult__

#pragma interface


extern "Java"
{
  namespace java
  {
    namespace applet
    {
      class AudioClip;
    }
  }
  namespace javax
  {
    namespace speech
    {
      namespace recognition
      {
        class RuleGrammar;
        class ResultListener;
        class ResultToken;
        class Result;
        class Grammar;
      }
    }
  }
  namespace voxware
  {
    namespace engine
    {
      namespace recognition
      {
        class BaseGrammar;
        class BaseRecognizer;
        class BaseResult;
      }
    }
    namespace util
    {
      class ObjectSet;
    }
  }
}

class voxware::engine::recognition::BaseResult : public ::java::lang::Object
{
public:
  BaseResult (::javax::speech::recognition::Grammar *);
  BaseResult (::javax::speech::recognition::Grammar *, JArray< ::java::lang::String *> *, jboolean);
  BaseResult (::javax::speech::recognition::Grammar *, ::java::lang::String *, jboolean);
public:  // actually protected
  virtual void construct (::javax::speech::recognition::Grammar *, JArray< ::java::lang::String *> *, jboolean);
public: // actually package-private
  static ::voxware::engine::recognition::BaseResult *copyResult (::javax::speech::recognition::Result *);
public:
  virtual jint getResultState () { return state; }
  virtual ::javax::speech::recognition::Grammar *getGrammar () { return grammar; }
  virtual jint numTokens ();
  virtual ::javax::speech::recognition::ResultToken *getBestToken (jint);
  virtual JArray< ::javax::speech::recognition::ResultToken *> *getBestTokens ();
  virtual JArray< ::javax::speech::recognition::ResultToken *> *getUnfinalizedTokens ();
  virtual void addResultListener (::javax::speech::recognition::ResultListener *);
  virtual void removeResultListener (::javax::speech::recognition::ResultListener *);
  virtual jboolean isTrainingInfoAvailable ();
  virtual void releaseTrainingInfo ();
  virtual void tokenCorrection (JArray< ::java::lang::String *> *, ::javax::speech::recognition::ResultToken *, ::javax::speech::recognition::ResultToken *, jint);
  virtual jboolean isAudioAvailable ();
  virtual void releaseAudio ();
  virtual ::java::applet::AudioClip *getAudio ();
  virtual ::java::applet::AudioClip *getAudio (::javax::speech::recognition::ResultToken *, ::javax::speech::recognition::ResultToken *);
  virtual jint getNumberGuesses ();
  virtual JArray< ::javax::speech::recognition::ResultToken *> *getAlternativeTokens (jint);
  virtual ::javax::speech::recognition::RuleGrammar *getRuleGrammar (jint);
  virtual ::java::lang::String *getRuleName (jint);
  virtual JArray< ::java::lang::String *> *getTags ();
  virtual JArray<JArray< ::javax::speech::recognition::ResultToken *> *> *getAlternativeTokens (::javax::speech::recognition::ResultToken *, ::javax::speech::recognition::ResultToken *, jint);
  virtual void resultUpdated (::voxware::engine::recognition::BaseRecognizer *, ::voxware::engine::recognition::BaseGrammar *);
  virtual void resultUpdated (::voxware::engine::recognition::BaseRecognizer *, ::voxware::engine::recognition::BaseGrammar *, jboolean, jboolean);
  virtual void grammarFinalized (::voxware::engine::recognition::BaseRecognizer *, ::voxware::engine::recognition::BaseGrammar *);
  virtual void grammarFinalized (::voxware::engine::recognition::BaseRecognizer *, ::voxware::engine::recognition::BaseGrammar *, jboolean, jboolean);
  virtual void resultAccepted (::voxware::engine::recognition::BaseRecognizer *, ::voxware::engine::recognition::BaseGrammar *);
  virtual void resultAccepted (::voxware::engine::recognition::BaseRecognizer *, ::voxware::engine::recognition::BaseGrammar *, jboolean, jboolean);
  virtual void resultRejected (::voxware::engine::recognition::BaseRecognizer *, ::voxware::engine::recognition::BaseGrammar *);
  virtual void resultRejected (::voxware::engine::recognition::BaseRecognizer *, ::voxware::engine::recognition::BaseGrammar *, jboolean, jboolean);
  virtual void audioReleased (::voxware::engine::recognition::BaseRecognizer *, ::voxware::engine::recognition::BaseGrammar *);
  virtual void trainingInfoReleased (::voxware::engine::recognition::BaseRecognizer *, ::voxware::engine::recognition::BaseGrammar *);
  virtual ::java::lang::String *toString ();
  virtual void addUnfinalizedToken (::java::lang::String *);
  virtual void finalizeTokens (::java::lang::String *);
  virtual void addFinalizedToken (::java::lang::String *);
  virtual void setGrammar (::javax::speech::recognition::Grammar *);
  virtual void setResultState (jint);
public:  // actually protected
  virtual void checkResultState (jint);
  JArray<JArray< ::java::lang::String *> *> * __attribute__((aligned(__alignof__( ::java::lang::Object )))) nBest;
  ::java::util::Vector *unfinalizedTokens;
  jint state;
  JArray< ::java::lang::String *> *tags;
  ::java::lang::String *ruleName;
  ::voxware::util::ObjectSet *resultListeners;
  ::javax::speech::recognition::Grammar *grammar;

  friend class voxware_engine_recognition_BaseResult$BaseResultToken;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_recognition_BaseResult__ */
