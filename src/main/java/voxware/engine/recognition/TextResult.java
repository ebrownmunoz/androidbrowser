package voxware.engine.recognition;

import javax.speech.recognition.Grammar;

/**
 * For now a Signal class to use for results from the Text Recognizer
 * @author eric
 *
 */
public class TextResult extends BaseResult {

	public TextResult(Grammar grammar) {
		super(grammar);
		
	}

	public TextResult(Grammar grammar, String[] nBest, boolean parse) {
		super(grammar, nBest, parse);
		
	}

	public TextResult(Grammar grammar, String best, boolean parse) {
		super(grammar, best, parse);
		
	}

}
