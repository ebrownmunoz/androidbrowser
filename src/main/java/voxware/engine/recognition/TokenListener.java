package voxware.engine.recognition;

import java.util.EventListener;

import javax.speech.*;
import javax.speech.recognition.*;

/**
 * Interface defining a method to call to pass tokens to a
 * recognizer.  A recognizer must implement this interface
 * to receive <code>TokenEvents</code>
 * <P>
 *
 * @see SpeechEvent
 * @see TokenEvent
 * @see EventListener
 */

public interface TokenListener extends EventListener {

  /**
   * Pass a <code>TokenEvent</code> to the listener
   * <P>
   *
   * @see TokenEvent
   */

  public void token(TokenEvent e);
}
