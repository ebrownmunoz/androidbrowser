package voxware.engine.recognition.sapivise;

public class Initializer {

  /**
   * The static initialization method for library jsapi
   */
  public static String initialize(String logFileName) {
    return logFileName != null ? logFileName : "current library logfile";
  }

}
