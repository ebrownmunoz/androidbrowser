#ifndef __voxware_engine_recognition_sapivise_SapiViseGrammar__
#define __voxware_engine_recognition_sapivise_SapiViseGrammar__

#pragma interface

#include <voxware/engine/recognition/BaseRuleGrammar.h>

extern "Java"
{
  namespace javax
  {
    namespace speech
    {
      namespace recognition
      {
        class RuleName;
      }
    }
  }
  namespace voxware
  {
    namespace engine
    {
      namespace recognition
      {
        namespace sapivise
        {
          class SapiViseGrammar;
          class SapiVise;
        }
      }
    }
  }
  namespace gnu
  {
    namespace gcj
    {
      class RawData;
    }
  }
}

class voxware::engine::recognition::sapivise::SapiViseGrammar : public ::voxware::engine::recognition::BaseRuleGrammar
{
private:
  ::gnu::gcj::RawData *newGrammar (::gnu::gcj::RawData *);
  jint n_release ();
  void n_load (jbyteArray);
public:
  virtual void n_setactive (::java::lang::String *, jboolean);
  virtual ::java::lang::String *n_grammarname ();
  virtual ::java::lang::String *n_defaultrulename ();
private:
  JArray< ::java::lang::String *> *n_parse (::java::lang::String *);
  JArray< ::java::lang::String *> *n_rulenames ();
  JArray< ::java::lang::String *> *n_trainphrase (jint);
  jboolean n_startpass ();
  jboolean n_endpass (JArray< ::java::lang::String *> *, jboolean);
  jboolean n_savemodels (JArray< ::java::lang::String *> *, jint);
public:
  SapiViseGrammar (::voxware::engine::recognition::sapivise::SapiVise *, ::java::lang::String *);
public: // actually package-private
  SapiViseGrammar (::voxware::engine::recognition::sapivise::SapiVise *, ::java::io::InputStream *);
public:
  virtual void deallocate ();
public:  // actually protected
  virtual void finalize ();
public:
  virtual jboolean isResultAudioProvided ();
  virtual jboolean isTrainingProvided ();
public:  // actually protected
  virtual void load ();
public:
  virtual ::java::lang::String *getDefaultRuleName () { return defaultRuleName; }
  virtual jboolean activate (jboolean);
  virtual jint addRule (::javax::speech::recognition::RuleName *);
  virtual JArray< ::java::lang::String *> *parse (::java::lang::String *);
  virtual JArray< ::java::lang::String *> *trainPhrase (jint);
  virtual jboolean startTrainingPass ();
  virtual jboolean endTrainingPass ();
  virtual jboolean endTrainingPass (JArray< ::java::lang::String *> *);
  virtual jboolean completeTraining ();
  virtual jboolean completeTraining (JArray< ::java::lang::String *> *);
  virtual jboolean saveModels (jint);
  virtual jboolean saveModels (JArray< ::java::lang::String *> *, jint);
  static ::java::lang::String *EnrollmentRule;
  static ::java::lang::String *CalibrationRule;
  static const jint RECOMMENDED = 1L;
  static const jint REQUIRED = 2L;
private:
  jbyteArray __attribute__((aligned(__alignof__( ::voxware::engine::recognition::BaseRuleGrammar ))))  recImage;
  ::java::lang::String *defaultRuleName;
public: // actually package-private
  ::gnu::gcj::RawData *grammar;
  ::gnu::gcj::RawData *sapivise;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_recognition_sapivise_SapiViseGrammar__ */
