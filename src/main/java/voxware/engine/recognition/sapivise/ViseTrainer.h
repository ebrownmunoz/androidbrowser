#ifndef __voxware_engine_recognition_sapivise_ViseTrainer__
#define __voxware_engine_recognition_sapivise_ViseTrainer__

#pragma interface


extern "Java"
{
  namespace gnu
  {
    namespace gcj
    {
      class RawData;
    }
  }
  namespace voxware
  {
    namespace engine
    {
      namespace recognition
      {
        namespace sapivise
        {
          class ViseTrainer;
          class ViseEnrollStatus;
          class ViseResult;
          class SapiVise;
        }
      }
    }
  }
}

class voxware::engine::recognition::sapivise::ViseTrainer : public ::java::lang::Object
{
private:
  jboolean n_initialize (::gnu::gcj::RawData *);
  jboolean n_release ();
  jboolean n_insertcorrection (::java::lang::String *, ::voxware::engine::recognition::sapivise::ViseResult *);
  ::voxware::engine::recognition::sapivise::ViseEnrollStatus *n_enrollword ();
public:
  ViseTrainer (::voxware::engine::recognition::sapivise::SapiVise *);
  virtual jboolean insertCorrection (::java::lang::String *, ::voxware::engine::recognition::sapivise::ViseResult *);
  virtual ::voxware::engine::recognition::sapivise::ViseEnrollStatus *enrollWord ();
  virtual jboolean release ();
  virtual void finalize ();
public: // actually package-private
  ::voxware::engine::recognition::sapivise::SapiVise * __attribute__((aligned(__alignof__( ::java::lang::Object )))) CMSapiVise;
  ::gnu::gcj::RawData *IVbxSpkrTrainPtr;
  ::gnu::gcj::RawData *IVbxTrainPtr;
  ::gnu::gcj::RawData *TrainerPtr;
public:
  static const jint DATAFILEERROR = 100L;
  static const jint NOTENOUGHDATA = 101L;
  static const jint VALUEOUTOFRANGE = 102L;
private:
  ::java::util::Vector *refHolder;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_recognition_sapivise_ViseTrainer__ */
