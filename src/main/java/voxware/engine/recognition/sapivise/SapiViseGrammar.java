package voxware.engine.recognition.sapivise;

import javax.speech.*;
import javax.speech.recognition.*;

import java.io.*;
import java.net.*;
import java.util.*;

import voxware.engine.recognition.*;

import voxware.util.LibraryLoader;
import voxware.util.SysLog;

public class SapiViseGrammar extends BaseRuleGrammar {

  public static final String EnrollmentRule = "<ANY_WORD>";
  public static final String CalibrationRule = "<CALIBRATION>";

  static {
    SapiVise.loadLibrary();
  }

  private native gnu.gcj.RawData newGrammar(gnu.gcj.RawData sapivise);
  private native int      n_release();
  private native void     n_load(byte[] recImage);
  public  native void     n_setactive(String name, boolean state);
  public  native String   n_grammarname();
  public  native String   n_defaultrulename();
  private native String[] n_parse(String phrase);
  private native String[] n_rulenames();
  private native String[] n_trainphrase(int extent);
  private native boolean  n_startpass();
  private native boolean  n_endpass(String[] words, boolean finalPass);
  private native boolean  n_savemodels(String[] words, int minCount);


  public static final int RECOMMENDED = 1;
  public static final int REQUIRED    = 2;

  private byte      recImage[];  // Kept around pending future versions that can switch RuleGrammars
  private String    defaultRuleName;

  gnu.gcj.RawData   grammar;                 // A reference to the underlying CNISapiViseGram
  gnu.gcj.RawData   sapivise;                // the owning CNISapiVise engine

  public SapiViseGrammar(SapiVise sapiVise, String name) {
    super((BaseRecognizer)sapiVise, name);
    grammar = newGrammar(sapiVise.recognizer);
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK))
      SysLog.println("SapiViseGrammar constructor CALLED");
  }

  SapiViseGrammar(SapiVise sapiVise, InputStream recStream) throws IOException {
     // The grammar (application) name is supplied by the load(); no name is yet known here
    this(sapiVise, (String)null);
     // Read the grammar image from recStream into the recImage buffer
    int recSize = recStream.available();            // Throws IOException
    this.recImage = new byte[recSize];
    recStream.read(this.recImage);                  // Throws IOException
     // Load the grammar into SAPIVISE and get its name and the names of its rules
    this.load();
  }

  public void deallocate() {
    int refCount = n_release();
    if (refCount == 0) {
      grammar = null;
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK))
        SysLog.println("SapiViseGrammar.deallocate reduced CNISapiViseGram refCount to " + refCount);
    } else {
      SysLog.println("SapiViseGrammar.deallocate: UNEXPECTED - refCount (" + refCount + ") non-zero after deallocation");
    }
  }

  protected void finalize() throws Throwable {
    try {
      if (grammar != null)
        SysLog.println("SapiViseGrammar.finalize: UNEXPECTED - underlying CNISapiViseGram not deallocated");
    } finally {
      super.finalize();
    }
  }

  public boolean isResultAudioProvided() {
    return (((SapiVise)recognizer).getParameter(ViseProperties.VISE_AUDQUEUELEN, 0).IntVal() > 0);
  }

  public boolean isTrainingProvided() {
    return ((SapiVise)recognizer).trainingProvided;
  }

   // Load SAPIVISE with the recfile image in recImage
  protected void load() {

     // Load SAPIVISE with the recfile image in recImage
    n_load(recImage);

     // Get the names of things
    myName = n_grammarname();
    String[] ruleNames = n_rulenames();
    defaultRuleName = n_defaultrulename();
     // If there is no default rule name, use the first rule name as the default
    if (defaultRuleName == null && ruleNames.length > 0) defaultRuleName = ruleNames[0];
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK) && defaultRuleName != null)
      SysLog.println("SapiViseGrammar.load: the default rule for grammar \"" + myName + "\" is \"" + getDefaultRuleName() + "\"");
    
     // Add the enrollment rule
    RuleName rn = new RuleName(null, getName(), EnrollmentRule);
    setRule(EnrollmentRule, (Rule) rn, true);

     // Add the calibration rule
    rn = new RuleName(null, getName(), CalibrationRule);
    setRule(CalibrationRule, (Rule) rn, true);

     // Add all the grammar's rules to the superclass's rule hash
    for (int i = 0; i < ruleNames.length; i++) {
      rn = new RuleName(null, getName(), ruleNames[i]);
      setRule(ruleNames[i], (Rule) rn, true);
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK))
        SysLog.println("SapiViseGrammar.load: rule[" + i + "] is \"" + ruleNames[i] + "\"");
    }
  }

  public String getDefaultRuleName() {
    return defaultRuleName;
  }

   // Overrides activate() in BaseGrammar to add actual SAPIVISE activate/deactivate call
  public boolean activate(boolean active) {
    String rnames[] = listRuleNames();
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK))
      SysLog.println("SapiViseGrammar.activate: " + (active ? "activating" : "deactivating") + " grammar \"" + this.getName() + "\"");
    for (int j = 0; j < rnames.length; j++) {
      if (isRulePublic(rnames[j])) {
        boolean activate = active && isEnabled(rnames[j]);
        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK))
          SysLog.println("  Rule \"" + rnames[j] + "\" " + (activate ? "activated" : "deactivated"));
        this.n_setactive(rnames[j], activate);
      }
    }
    return super.activate(active);
  }

  public int addRule(RuleName rn) {
    int i = ((BaseRuleGrammar)((SapiVise)recognizer).currentGrammar).getRuleID(rn.getSimpleRuleName());
    if (i == -1) {
      String gname = rn.getSimpleGrammarName();
      SapiViseGrammar SG = (SapiViseGrammar)(recognizer.getRuleGrammar(gname));
      if (SG == null) return 0;
      i = SG.getRuleID(rn.getSimpleRuleName());
    }
    return i;
  }

  public String[] parse(String phrase) {
    String[] ruleNames = null;

    if (phrase == null) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK))
        SysLog.println("SapiViseGrammar.parse: ERROR - null phrase");
    } else {
      ruleNames = n_parse(phrase);
    }
    return ruleNames;
  }

  public String[] trainPhrase(int extent) {
    String[] prompts = null;

    switch (extent) {
    case RECOMMENDED:
    case REQUIRED:
      prompts = n_trainphrase(extent);
      break;
    default:
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK))
        SysLog.println("SapiViseGrammar.trainPhrase: ERROR - unknown/unsupported extent " + extent);
    }
    return prompts;
  }

  public boolean startTrainingPass() {
    return n_startpass();
  }

  public boolean endTrainingPass() {
    return endTrainingPass(null);
  }

  public boolean endTrainingPass(String[] words) {
    return n_endpass(words, false);
  }

  public boolean completeTraining() {
    return completeTraining(null);
  }

  public boolean completeTraining(String[] words) {
    return n_endpass(words, true);
  }

  public boolean saveModels(int minTrainingCount) {
    return n_savemodels(null, minTrainingCount);
  }

  public boolean saveModels(String[] words, int minTrainingCount) {
    return n_savemodels(words, minTrainingCount);
  }
}
