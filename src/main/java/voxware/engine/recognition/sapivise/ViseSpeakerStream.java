package voxware.engine.recognition.sapivise;

import voxware.util.SysLog;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.SequenceInputStream;

public class ViseSpeakerStream {

  SequenceInputStream inputStream;

   // Cconstruct an InputStream that is the concatenation of the UTF-8 encoded speaker name followed by the voice file contents
  public ViseSpeakerStream(String spkrName, String voiPathName) throws java.io.IOException {
    if (spkrName == null || spkrName.length() == 0) {
      SysLog.println("ViseSpeakerStream: ERROR - null/zero length spkrName");
      return;
    } else if (voiPathName == null || voiPathName.length() == 0) {
      SysLog.println("ViseSpeakerStream: ERROR - null/zero length voiPathName");
      return;
    } else {
      int voiBytes = 0;
      FileInputStream fileInputStream = new FileInputStream(voiPathName);
      if (fileInputStream == null) {
        SysLog.println("ViseSpeakerStream: ERROR voice file InputStream is null");
        return;
      } else if ((voiBytes = fileInputStream.available()) == 0) {
        SysLog.println("ViseSpeakerStream: ERROR - unable to read \"" + voiPathName + "\"; available bytes = " + voiBytes);
        return;
      } else {
        inputStream = new SequenceInputStream(new ByteArrayInputStream(spkrName.getBytes("UTF-8")), fileInputStream);
      }
    }
  }

   // Construct an InputStream that is the concatenation of the UTF-8 encoded speaker name followed by the voice stream
  public ViseSpeakerStream(String spkrName, InputStream voiStream) throws java.io.IOException {
    if (spkrName == null || spkrName.length() == 0) {
      SysLog.println("ViseSpeakerStream: ERROR - null/zero length spkrName");
      return;
    } else if (voiStream == null) {
      SysLog.println("ViseSpeakerStream: ERROR - null/zero length voiStream");
      return;
    } else {
      inputStream = new SequenceInputStream(new ByteArrayInputStream(spkrName.getBytes("UTF-8")), voiStream);
    }
  }

  public InputStream inputStream() {
    return (InputStream) this.inputStream;
  }
}
