#ifndef __voxware_engine_recognition_sapivise_RawResult__
#define __voxware_engine_recognition_sapivise_RawResult__

#pragma interface

#include <java/lang/Object.h>

extern "Java"
{
  namespace voxware
  {
    namespace engine
    {
      namespace recognition
      {
        namespace sapivise
        {
          class RawResult;
        }
      }
    }
  }
  namespace gnu
  {
    namespace gcj
    {
      class RawData;
    }
  }
}

class voxware::engine::recognition::sapivise::RawResult : public ::java::lang::Object
{
public:
  RawResult ();
public: // actually package-private
  jboolean __attribute__((aligned(__alignof__( ::java::lang::Object ))))  accepted;
  jboolean training;
  jboolean audio;
  ::gnu::gcj::RawData *result;
  ::java::lang::String *grammarName;
  JArray< ::java::lang::String *> *nbestArray;
  ::java::lang::String *hostTrans__;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_recognition_sapivise_RawResult__ */
