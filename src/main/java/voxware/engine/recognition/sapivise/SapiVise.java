package voxware.engine.recognition.sapivise;

import javax.speech.*;
import javax.speech.recognition.*;

import voxware.engine.recognition.*;
import voxware.engine.recognition.sapivise.ViseProperties;
import voxware.engine.recognition.sapivise.ViseSpeakerManager;

import java.io.*;
import java.net.*;
import java.util.*;

import voxware.util.LibraryLoader;
import voxware.util.SysLog;

class NotifyThread extends Thread {
  SapiVise parent;

  NotifyThread(SapiVise parent) {
    super("JSAPINotifier");
    this.parent = parent;
    this.setDaemon(true);
  }

  public void run() {
    RawResult rawresult = new RawResult();
    setPriority(Thread.MAX_PRIORITY - 3);

    while (true) {
      parent.n_notify(rawresult);
      parent.notifyResult(rawresult);
    }
  }
}

public class SapiVise extends BaseRecognizer {

  public static final String LibraryName = "jsapi";
  public static final String LibraryInitializer = "voxware.engine.recognition.sapivise.Initializer.initialize";

  static boolean loadLibrary() {
    return LibraryLoader.loadLibrary(SapiVise.LibraryName, SapiVise.LibraryInitializer, null);
  }
  
  static {
    if (!loadLibrary()) SysLog.println("voxware.engine.recognition.sapivise.SapiVise: ERROR - jsapi library NOT loaded");
  }

  public  native void     n_notify(RawResult rawresult);
  private native gnu.gcj.RawData newRecognizer();
  private native void     n_deallocate();
  private native void     n_pause();
  private native void     n_resume();
  private native boolean  n_initialize(String mode);
  private native boolean  n_calibrate();
  private native int      n_levelget();
  private native boolean  n_levelset(int level);
  private native boolean  n_deletespeaker(String spkrName);
  private native String[] n_listspeakers();
  private native boolean  n_readspeaker(String spkrName, byte [] spkrData);
  private native byte[]   n_writespeaker(String spkrName);
  private native boolean  n_selectspeaker(String spkrName);
  private native String   n_queryspeaker();
  private native boolean  n_setparameter(int index);
  private native boolean  n_getparameter(int index, int value);
  private native boolean  n_setparameter(int index, int value);
  private native boolean  n_getparameter(int index, float value);
  private native boolean  n_setparameter(int index, float value);
  private native boolean  n_setparameter(int index, String value);
  private native boolean  n_settimeout(int timeout);
  private native boolean  n_setutterance(String fullPath);
  private native void     n_flush();
  private native int      n_getVU();
  NotifyThread      notifyThread;
  Grammar           currentGrammar;
  private Hashtable wordList;
  boolean           trainingProvided;
  int               IntVal;
  float             FloatVal;
  public gnu.gcj.RawData recognizer;

  public SapiVise() {
    this(null);
  }

  public SapiVise(RecognizerModeDesc mode) {
    super(mode, true);
    recognizer = newRecognizer();
  }

  public ViseTrainerFactory getTrainerFactory() {
    return new ViseTrainerFactory(this);
  }

  /*
   * SAPIVISE Extensions
   */

  public ParameterResult setParameter(int index) {
    boolean rval = n_setparameter(index);
    return new ParameterResult(rval, 0, (float) 0.);
  }

  public ParameterResult getParameter(int index, int value) {
    IntVal = 0;
    boolean rval = n_getparameter(index, value);
    return new ParameterResult(rval, IntVal, (float) 0.);
  }

  public ParameterResult setParameter(int index, int value) {
    boolean rval = n_setparameter(index, value);
    return new ParameterResult(rval, 0, (float) 0.);
  }

  public ParameterResult getParameter(int index, float value) {
    FloatVal = (float) 0.;
    boolean rval = n_getparameter(index, value);
    return new ParameterResult(rval, 0, FloatVal);
  }

  public ParameterResult setParameter(int index, float value) {
    boolean rval = n_setparameter(index, value);
    return new ParameterResult(rval, 0, (float) 0.);
  }

  public ParameterResult setParameter(int index, String value) {
    boolean rval = n_setparameter(index, value);
    return new ParameterResult(rval, 0, (float) 0.);
  }

  private static float gains[] = {
     0.0F,  1.5F,  3.0F,  4.5F,  6.0F,  7.5F,  9.0F, 10.5F,
    12.0F, 13.5F, 15.0F, 16.5F, 18.0F, 19.5F, 20.0F, 21.0F,
    21.5F, 22.5F, 23.0F, 24.5F, 26.0F, 27.5F, 29.0F, 30.5F,
    32.0F, 33.5F, 35.0F, 36.5F, 38.0F, 39.5F, 41.0F, 42.5F
  };

  public float levelGet() {
    int level = n_levelget();
    if (level < 0) return Float.NaN;
    level /= 2048;
    if (level > 31) level = 31;
    return gains[level];
  }

  public boolean levelSet(float gain) {
    int   imin = 0, level;
    float fmin = 100.0F, f;

    if (gain >= 42.5F) {
      level = 65535;
    } else if (gain <= 0) {
      level = 0;
    } else {
      for (int i = 0; i < 32; i++) {
        f = Math.abs(gain - gains[i]);
        if (f < fmin) {
          fmin = f;
          imin = i;
        }
      }
      level = imin * 2048;
    }
    return n_levelset(level);
  }

  public boolean calibrate() {
    return n_calibrate();
  }

  public boolean setTimeout(int timeout) {
    return n_settimeout(timeout);
  }

  public int getVU() {
    return n_getVU();
  }
  /*
   * JSAPI Infrastructure Overrides
   */

  /** 
   * Override getRecognizerProperties() in BaseRecognizer to get ViseProperties
   * FROM javax.speech.recognition.Recognizer
   */
  public RecognizerProperties getRecognizerProperties() {
    ViseProperties viseProperties = new ViseProperties(this);
    return (RecognizerProperties)viseProperties;
  }

  /** 
   * Override getSpeakerManager() in BaseRecognizer to get a ViseSpeakerManager
   * FROM javax.speech.recognition.Recognizer
   */
  public SpeakerManager getSpeakerManager() {
    ViseSpeakerManager manager = new ViseSpeakerManager(this);
    return (SpeakerManager)manager;
  }

  /** 
   * Override readVendorGrammar() in BaseRecognizer to read a SapiViseGrammar
   * FROM javax.speech.recognition.Recognizer
   */
  public Grammar readVendorGrammar(InputStream input) throws VendorDataException, IOException, EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);           // Throws EngineStateError

    currentGrammar = new SapiViseGrammar(this, input);                // Throws IOException

     // Remove and delete any older grammar already on the list under the same name
    RuleGrammar oldG = null;
    if ((oldG = retrieveGrammar(currentGrammar.getName())) != null) {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK))
        SysLog.println("SapiVise.readVendorGrammar: deleting old version of grammar \"" + currentGrammar.getName() + "\"");
      deleteRuleGrammar(oldG);
      ((SapiViseGrammar)oldG).deallocate();
    }
    oldG = null;

     // Store the new grammar in this Recognizer
    storeGrammar((RuleGrammar)currentGrammar);

    return currentGrammar;
  }

  /** 
   * Override linkGrammars() in BaseRecognizer
   * FROM voxware.engine.recognition.BaseRecognizer
   */
  protected void linkGrammars() throws GrammarException {
    if (currentGrammar == null)
      throw new GrammarException("SapiVise.linkGrammars: current grammar is null");
  }

  /** 
   * Override endGrammarChanges() in BaseRecognizer to print out rule names for debugging
   * FROM voxware.engine.recognition.BaseRecognizer
   */
  protected void endGrammarChanges() { 
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK)) {
      String ruleNames[] = ((RuleGrammar)currentGrammar).listRuleNames();
      StringBuffer message = new StringBuffer("SapiVise.endGrammarChanges: grammar ");
      message.append(currentGrammar.getName()).append(" has ").append(ruleNames.length).append(" rules");
      for (int i = 0; i < ruleNames.length; i++) message.append("\n  Rule ").append(i).append(": ").append(ruleNames[i]);
      SysLog.println(message.toString());
    }
  }


  /** 
   * Override changeEnabled() in BaseRecognizer to check for multiple rule activation
   * FROM voxware.engine.recognition.BaseRecognizer
   *
   * @exception GrammarException if more than one grammar rule is active
   */
  protected void changeEnabled(Vector activeRules) throws GrammarException {
     // Verify that no more than one rule is active
    if (activeRules.size() > 1) {
      StringBuffer message = new StringBuffer("Attempt to activate multiple grammar rules simultaneously:");
      Enumeration rules = activeRules.elements();
      while (rules.hasMoreElements()) message.append(' ').append((String)rules.nextElement()).append(',');
      message.setLength(message.length() - 1);
      throw new GrammarException(message.toString());
    }
  }

  /** 
   * Override allocateEngine in BaseEngine to allocate the resources required for the SapiVise recognizer and start running (Running is the "resume" state)
   * FROM voxware.engine.BaseEngine
   *
   * @exception EngineException
   */
  protected void allocateEngine() throws EngineException {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK)) SysLog.println("SapiVise.allocateEngine: called");
    super.allocateEngine();
    if (!n_initialize(engineModeDesc.getModeName()))
      throw new EngineException("SapiVise.n_initialize() failed");
    notifyThread = new NotifyThread(this);
    notifyThread.start();
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK)) SysLog.println("SapiVise.allocateEngine: finished");
  }

  /**
   * Override deallocateEngine() in BaseEngine to deallocate the resources used by the SapiVise recognizer
   * FROM voxware.engine.BaseEngine
   *
   * @exception EngineException 
   */
  protected void deallocateEngine() throws EngineException {            
    super.deallocateEngine();
    n_deallocate();
  }
      
  /**
   * Override pauseEngine() in BaseEngine to pause the underlying SAPIVISE engine
   * FROM voxware.engine.BaseEngine
   *
   * @exception EngineStateError
   */
  protected void pauseEngine() throws EngineStateError {
    super.pauseEngine();
    n_pause();
  }
 
  /**
   * Override resumeEngine() in BaseEngine to resume the underlying SAPIVISE engine
   * FROM voxware.engine.BaseEngine
   *
   * @exception AudioException, EngineStateError 
   */
  protected void resumeEngine() throws AudioException, EngineStateError {
    super.resumeEngine();
    n_resume();
  }

  /**
   * SAPIVISE Extensions
   */
   
  public boolean deleteSpeaker(SpeakerProfile profile) {
    return n_deletespeaker(profile.getName());
  }

  public boolean readVendorSpeakerProfile(String spkrName, byte[] voiData) {
    return n_readspeaker(spkrName, voiData);
  }

  public byte[] writeVendorSpeakerProfile(String spkrName) {
    return n_writespeaker(spkrName);
  }

  public boolean setCurrentSpeaker(SpeakerProfile profile) {
    return n_selectspeaker(profile.getName());
  }

  public SpeakerProfile getCurrentSpeaker() {
    String name = n_queryspeaker();
    SpeakerProfile profile = new SpeakerProfile();
    profile.setName(name);
    return profile;
  }

  public SpeakerProfile[] listKnownSpeakers() {
    String[]  spkrNames;
    
    spkrNames = n_listspeakers();
    if (spkrNames == null)
      return null;

    SpeakerProfile[] knownSpkrs = new SpeakerProfile[spkrNames.length];
    for (int i = 0; i < spkrNames.length; i++)
      knownSpkrs[i] = new SpeakerProfile("", spkrNames[i], "");
    return knownSpkrs;
  }
    
  public boolean setUtterance(String fullPath) {
    return n_setutterance(fullPath);
  }

  public void flushUtterance() {
    n_flush();
  }

   // Utility function to forward a speech result string to the appropriate grammar
  public void notifyResult(RawResult rawresult) {
    BaseGrammar G = (BaseGrammar)getRuleGrammar(rawresult.grammarName);

    if (G == null) {
      System.err.println("SapiVise.notifyResult: ERROR -- UNKNOWN GRAMMAR FOR RESULT " + rawresult.grammarName);
      return;
    }
    
    ViseResult result = new ViseResult(G, rawresult.result, rawresult.nbestArray, false);
    this.resultCreated(result);
    result.training = rawresult.training;
    result.audio = rawresult.audio;
    result.hostTrans(rawresult.hostTrans);
    result.grammarFinalized(this, G);
    if (rawresult.accepted) {
      result.setResultState(Result.ACCEPTED);
      result.resultAccepted(this, G);
    } else {
      result.setResultState(Result.REJECTED);
      result.resultRejected(this, G);
    }
  }
}
