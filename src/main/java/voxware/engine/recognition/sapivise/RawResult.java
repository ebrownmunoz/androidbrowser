package voxware.engine.recognition.sapivise;

import java.lang.*;
import java.util.*;

public class RawResult {

  public boolean  accepted = false;
  boolean  training = false;
  boolean  audio = false;
  public gnu.gcj.RawData result = null;
  public String   grammarName = "";
  public String[] nbestArray = null;
  String   hostTrans = "";

  public RawResult() {}
}
