package voxware.engine.recognition.sapivise;

import javax.speech.*;
import javax.speech.recognition.*;

public class SapiViseEngineCentral implements EngineCentral {

  static private SapiViseModeDesc voiceModeDesc = new SapiViseModeDesc("voice");
  static private SapiViseModeDesc textModeDesc  = new SapiViseModeDesc("text");

  public EngineList createEngineList(EngineModeDesc require) {
    EngineList engineList = null;
    if (require == null || voiceModeDesc.match(require)) {
      if (engineList == null) engineList = new EngineList();
      engineList.addElement(voiceModeDesc);
    }
    if (require == null || textModeDesc.match(require)) {
      if (engineList == null) engineList = new EngineList();
      engineList.addElement(textModeDesc);
    }
    return engineList;
  }
}
