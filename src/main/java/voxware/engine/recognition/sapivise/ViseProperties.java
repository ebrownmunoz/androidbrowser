package voxware.engine.recognition.sapivise;

import javax.speech.*;
import javax.speech.recognition.*;

import voxware.engine.recognition.*;
import voxware.util.SysLog;

import java.io.*;
import java.util.*;

public class ViseProperties implements RecognizerProperties {

  public static final int DFLT_AUDQUEUELEN = 10000;

   // Enumeration of parameter indices for VISE and FlexVISE
   // IMPORTANT: This must match the enumeration in vbxparms.h
  public static final int VISE_NHYPOTHESES = 100;   // VISE & FlexVISE

   // The scaling of the following thresholds differ between VISE & FlexVISE
  public static final int VISE_WCTHRESHOLD = 101;   // VISE & FlexVISE
  public static final int VISE_ABSWILDCARD = 102;   // VISE & FlexVISE
  public static final int VISE_RELWILDCARD = 103;   // VISE & FlexVISE

  public static final int VISE_SAMPSIGBITS = 104;   // VISE & FlexVISE

  public static final int VISE_JINCLASSIZE = 105;   // VISE only
  public static final int VISE_MAXUNPRFRMS = 106;   // VISE only
  public static final int VISE_UNPRQUANTUM = 107;   // VISE only

  public static final int VISE_QUEUELENGTH = 108;   // VISE & FlexVISE
  public static final int VISE_QUEUEFWDTOL = 109;   // VISE & FlexVISE
  public static final int VISE_QUEUEBWDTOL = 110;   // VISE & FlexVISE

  public static final int VISE_BOUPADLNGTH = 111;   // VISE & FlexVISE
  public static final int VISE_EOUPADLNGTH = 112;   // VISE & FlexVISE

  public static final int VISE_FRAMEIDLEMS = 113;   // Obsolete

  public static final int VISE_PRINTFRAMES = 114;   // FlexVISE only (same as VISE_SHOWFRMDATA)
  public static final int VISE_PRINTSTATUS = 115;   // VISE & FlexVISE (same as VISE_USEPROFILER for FlexVISE)

  public static final int VISE_TRAINWEIGHT = 116;   // VISE only
  public static final int VISE_BATCHWEIGHT = 117;   // VISE only

  public static final int VISE_MUTHATHRESH = 118;   // VISE only
  public static final int VISE_LOOSETHRESH = 119;   // VISE only
  public static final int VISE_TIGHTTHRESH = 120;   // VISE only

  public static final int VISE_DFLTTRCOUNT = 121;   // VISE only
  public static final int VISE_DFLTTRSCORE = 122;   // VISE only

  public static final int VISE_ENRINIWCABS = 123;   // VISE only
  public static final int VISE_ENRINIWCREL = 124;   // VISE only
  public static final int VISE_ENRINICOUNT = 125;   // VISE only
  public static final int VISE_ENRBODWCABS = 126;   // VISE only
  public static final int VISE_ENRBODWCREL = 127;   // VISE only
  public static final int VISE_ENRBODMINDW = 128;   // VISE only
  public static final int VISE_ENRBODMAXDW = 129;   // VISE only
  public static final int VISE_ENRMAXKERNL = 130;   // VISE only
  public static final int VISE_ENRMINKERNL = 131;   // VISE only
  public static final int VISE_ENRMAXDWELL = 132;   // VISE only
  public static final int VISE_ENRMINDWELL = 133;   // VISE only
  public static final int VISE_ENRTOLERNCE = 134;   // VISE only

  public static final int VISE_CALIBLENGTH = 135;   // VISE only
  public static final int VISE_CALIBWEIGHT = 136;   // VISE only
  public static final int VISE_CALIBRWCREL = 137;   // VISE only
  public static final int VISE_CALIBRWCABS = 138;   // VISE only

  public static final int VISE_VUSCALEMULT = 139;   // Unsupported
  public static final int VISE_VUOFFSETADD = 140;   // Unsupported

  public static final int VISE_SILMAXDWELL = 141;   // FlexVISE only (same as VISE_INCOMPLTTO, except arg is int milliseconds)
  public static final int VISE_ENDMINDWELL = 142;   // VISE & FlexVISE (same as VISE_COMPLETETO, except arg is int milliseconds)

  public static final int VISE_SILADAPTWGT = 143;   // VISE only
  public static final int VISE_SILADAPTLAG = 144;   // VISE only

  public static final int VISE_MAXDURATION = 145;   // VISE & FlexVISE (same as VISE_MAXFRAMECNT, except arg is int milliseconds)
  public static final int VISE_AUDQUEUELEN = 146;   // VISE & FlexVISE
  public static final int VISE_BOUSILMSECS = 147;   // VISE & FlexVISE
  public static final int VISE_EOUSILMSECS = 148;   // VISE & FlexVISE
  public static final int VISE_NOISETRKING = 149;   // VISE
  public static final int VISE_MAXFRAMECNT = 150;   // VISE & FlexVISE (same as VISE_MAXDURATION, except arg is int frames)

   // FlexVISE only
  public static final int VISE_CEPCLASSIZE = VISE_JINCLASSIZE;
  public static final int VISE_MAXRESLTLEN = 151;
  public static final int VISE_SHOWFRMDATA = 152;
  public static final int VISE_STARTSHOWFR = 153;
  public static final int VISE_FINALSHOWFR = 154;
  public static final int VISE_SHOWALLTBKS = 155;
  public static final int VISE_SHOWFINALTB = 156;
  public static final int VISE_SHOWTBSTRNG = 157;
  public static final int VISE_MAXNODDWELL = 158;
  public static final int VISE_FORWARDONLY = 159;
  public static final int VISE_VSBEAMWIDTH = 160;
  public static final int VISE_NPBEAMWIDTH = 161;
  public static final int VISE_NWBEAMWIDTH = 162;
  public static final int VISE_CHANSPERFRM = 163;
  public static final int VISE_MAXCPFLBINS = 164;
  public static final int VISE_METTHRCOUNT = 165;
  public static final int VISE_CMTTHRCOUNT = 166;
  public static final int VISE_NMTTHRCOUNT = 167;
  public static final int VISE_USEPROFILER = 168;
  public static final int VISE_USEBACKOFFP = 169;
  public static final int VISE_SHOWALTPRON = 170;
  public static final int VISE_FORCEFINISH = 171;
  public static final int VISE_NBSTFWDTOLR = 172;
  public static final int VISE_NBSTBWDTOLR = 173;
  public static final int VISE_NBSTSTKSIZE = 174;
  public static final int VISE_NBSTPRUNSIZ = 175;
  public static final int VISE_PATHTBLSIZE = 176;
  public static final int VISE_SHOWCEPDATA = 177;
  public static final int VISE_SHOWFEATDAT = 178;
  public static final int VISE_SHOWVQFRAME = 179;
  public static final int VISE_SHOWEPFRAME = 180;
  public static final int VISE_USECEPPACKS = 181;
  public static final int VISE_EPCLASSSIZE = 182;
  public static final int VISE_EPTHREADCNT = 183;
  public static final int VISE_NPRECOMPFWD = 184;
  public static final int VISE_NPRECOMPBWD = 185;
  public static final int VISE_USEWILDCARD = 186;

  public static final int VISE_SPEAKERPATH = 200;   // VISE & FlexVISE
  public static final int VISE_DFLTSPEAKER = 201;   // VISE & FlexVISE
  public static final int VISE_FRONTENDVER = 202;   // VISE & FlexVISE
  public static final int VISE_TRAINSCHEME = 203;   // VISE
  public static final int VISE_VUONOFFSWCH = 204;   // VISE

   // Additional parameter indices for JSAPI VISE
   // IMPORTANT: These are trapped in the CNI and have no counterparts in vbxparms.h
  public static final int VISE_COMPLETETO  = 1000;  // VISE & FlexVISE (same as VISE_ENDMINDWELL, except arg is float seconds)
  public static final int VISE_INCOMPLTTO  = 1001;  // FlexVISE only (same as VISE_SILMAXDWELL, except arg is float seconds)

  SapiVise Owner;

  public ViseProperties(SapiVise owner) {
    Owner = owner;
  }

  public int getNHypotheses() {
    ParameterResult pr = Owner.getParameter(VISE_NHYPOTHESES, 0);
    return pr.IntVal();
  }

  public void setNHypotheses(int nHypotheses) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_NHYPOTHESES, nHypotheses);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getWCThreshold() {
    ParameterResult pr = Owner.getParameter(VISE_WCTHRESHOLD, 0);
    return pr.IntVal();
  }

  public void setWCThreshold(int wcThreshold) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_WCTHRESHOLD, wcThreshold);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getAbsWildcard() {
    ParameterResult pr = Owner.getParameter(VISE_ABSWILDCARD, 0);
    return pr.IntVal();
  }

  public void setAbsWildcard(int absWildcard) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ABSWILDCARD, absWildcard);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getRelWildcard() {
    ParameterResult pr = Owner.getParameter(VISE_RELWILDCARD, 0);
    return pr.IntVal();
  }

  public void setRelWildcard(int relWildcard) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_RELWILDCARD, relWildcard);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getSampSigBits() {
    ParameterResult pr = Owner.getParameter(VISE_SAMPSIGBITS, 0);
    return pr.IntVal();
  }

  public void setSampSigBits(int sampSigBits) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SAMPSIGBITS, sampSigBits);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getJinClasSize() {
    ParameterResult pr = Owner.getParameter(VISE_JINCLASSIZE, 0);
    return pr.IntVal();
  }

  public void setJinClasSize(int jinClasSize) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_JINCLASSIZE, jinClasSize);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getMaxUnprFrms() {
    ParameterResult pr = Owner.getParameter(VISE_MAXUNPRFRMS, 0);
    return pr.IntVal();
  }

  public void setMaxUnprFrms(int maxUnprFrms) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_MAXUNPRFRMS, maxUnprFrms);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getUnprQuantum() {
    ParameterResult pr = Owner.getParameter(VISE_UNPRQUANTUM, 0);
    return pr.IntVal();
  }

  public void setUnprQuantum(int unprQuantum) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_UNPRQUANTUM, unprQuantum);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getQueueLength() {
    ParameterResult pr = Owner.getParameter(VISE_QUEUELENGTH, 0);
    return pr.IntVal();
  }

  public void setQueueLength(int queueLength) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_QUEUELENGTH, queueLength);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getQueueFwdTol() {
    ParameterResult pr = Owner.getParameter(VISE_QUEUEFWDTOL, 0);
    return pr.IntVal();
  }

  public void setQueueFwdTol(int queueFwdTol) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_QUEUEFWDTOL, queueFwdTol);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getQueueBwdTol() {
    ParameterResult pr = Owner.getParameter(VISE_QUEUEBWDTOL, 0);
    return pr.IntVal();
  }

  public void setQueueBwdTol(int queueBwdTol) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_QUEUEBWDTOL, queueBwdTol);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getBOUPadLngth() {
    ParameterResult pr = Owner.getParameter(VISE_BOUPADLNGTH, 0);
    return pr.IntVal();
  }

  public void setBOUPadLngth(int bouPadLngth) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_BOUPADLNGTH, bouPadLngth);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEOUPadLngth() {
    ParameterResult pr = Owner.getParameter(VISE_EOUPADLNGTH, 0);
    return pr.IntVal();
  }

  public void setEOUPadLngth(int eouPadLngth) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_EOUPADLNGTH, eouPadLngth);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getFrameIdleMs() {
    ParameterResult pr = Owner.getParameter(VISE_FRAMEIDLEMS, 0);
    return pr.IntVal();
  }

  public void setFrameIdleMs(int frameIdleMs) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_FRAMEIDLEMS, frameIdleMs);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getPrintFrames() {
    ParameterResult pr = Owner.getParameter(VISE_PRINTFRAMES, 0);
    return pr.IntVal();
  }

  public void setPrintFrames(int printFrames) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_PRINTFRAMES, printFrames);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getPrintStatus() {
    ParameterResult pr = Owner.getParameter(VISE_PRINTSTATUS, 0);
    return pr.IntVal();
  }

  public void setPrintStatus(int printStatus) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_PRINTSTATUS, printStatus);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getTrainWeight() {
    ParameterResult pr = Owner.getParameter(VISE_TRAINWEIGHT, 0);
    return pr.IntVal();
  }

  public void setTrainWeight(int trainWeight) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_TRAINWEIGHT, trainWeight);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getBatchWeight() {
    ParameterResult pr = Owner.getParameter(VISE_BATCHWEIGHT, 0);
    return pr.IntVal();
  }

  public void setBatchWeight(int batchWeight) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_BATCHWEIGHT, batchWeight);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getMuthaThresh() {
    ParameterResult pr = Owner.getParameter(VISE_MUTHATHRESH, 0);
    return pr.IntVal();
  }

  public void setMuthaThresh(int muthaThresh) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_MUTHATHRESH, muthaThresh);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getLooseThresh() {
    ParameterResult pr = Owner.getParameter(VISE_LOOSETHRESH, 0);
    return pr.IntVal();
  }

  public void setLooseThresh(int looseThresh) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_LOOSETHRESH, looseThresh);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getTightThresh() {
    ParameterResult pr = Owner.getParameter(VISE_TIGHTTHRESH, 0);
    return pr.IntVal();
  }

  public void setTightThresh(int tightThresh) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_TIGHTTHRESH, tightThresh);
    if (!pr.Status()) throw new PropertyVetoException();
  }    

  public int getDfltTrCount() {
    ParameterResult pr = Owner.getParameter(VISE_DFLTTRCOUNT, 0);
    return pr.IntVal();
  }

  public void setDfltTrCount(int dfltTrCount) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_DFLTTRCOUNT, dfltTrCount);
    if (!pr.Status()) throw new PropertyVetoException();
  }    

  public int getDfltTrScore() {
    ParameterResult pr = Owner.getParameter(VISE_DFLTTRSCORE, 0);
    return pr.IntVal();
  }

  public void setDfltTrScore(int dfltTrScore) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_DFLTTRSCORE, dfltTrScore);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEnrIniWCAbs() {
    ParameterResult pr = Owner.getParameter(VISE_ENRINIWCABS, 0);
    return pr.IntVal();
  }

  public void setEnrIniWCAbs(int enrIniWCAbs) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRINIWCABS, enrIniWCAbs);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEnrIniWCRel() {
    ParameterResult pr = Owner.getParameter(VISE_ENRINIWCREL, 0);
    return pr.IntVal();
  }

  public void setEnrIniWCRel(int enrIniWCRel) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRINIWCREL, enrIniWCRel);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEnrIniCount() {
    ParameterResult pr = Owner.getParameter(VISE_ENRINICOUNT, 0);
    return pr.IntVal();
  }

  public void setEnrIniCount(int enrIniCount) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRINICOUNT, enrIniCount);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEnrBodWCAbs() {
    ParameterResult pr = Owner.getParameter(VISE_ENRBODWCABS, 0);
    return pr.IntVal();
  }

  public void setEnrBodWCAbs(int enrBodWCAbs) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRBODWCABS, enrBodWCAbs);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEnrBodWCRel() {
    ParameterResult pr = Owner.getParameter(VISE_ENRBODWCREL, 0);
    return pr.IntVal();
  }

  public void setEnrBodWCRel(int enrBodWCRel) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRBODWCREL, enrBodWCRel);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEnrBodMinDw() {
    ParameterResult pr = Owner.getParameter(VISE_ENRBODMINDW, 0);
    return pr.IntVal();
  }

  public void setEnrBodMinDw(int enrBodMinDw) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRBODMINDW, enrBodMinDw);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEnrBodMaxDw() {
    ParameterResult pr = Owner.getParameter(VISE_ENRBODMAXDW, 0);
    return pr.IntVal();
  }

  public void setEnrBodMaxDw(int enrBodMaxDw) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRBODMAXDW, enrBodMaxDw);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEnrMaxKernl() {
    ParameterResult pr = Owner.getParameter(VISE_ENRMAXKERNL, 0);
    return pr.IntVal();
  }

  public void setEnrMaxKernl(int enrMaxKernl) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRMAXKERNL, enrMaxKernl);
    if (!pr.Status())  throw new PropertyVetoException();
  }

  public int getEnrMinKernl() {
    ParameterResult pr = Owner.getParameter(VISE_ENRMINKERNL, 0);
    return pr.IntVal();
  }

  public void setEnrMinKernl(int enrMinKernl) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRMINKERNL, enrMinKernl);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEnrMaxDwell() {
    ParameterResult pr = Owner.getParameter(VISE_ENRMAXDWELL, 0);
    return pr.IntVal();
  }

  public void setEnrMaxDwell(int enrMaxDwell) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRMAXDWELL, enrMaxDwell);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEnrMinDwell() {
    ParameterResult pr = Owner.getParameter(VISE_ENRMINDWELL, 0);
    return pr.IntVal();
  }

  public void setEnrMinDwell(int enrMinDwell) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRMINDWELL, enrMinDwell);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEnrTolernce() {
    ParameterResult pr = Owner.getParameter(VISE_ENRTOLERNCE, 0);
    return pr.IntVal();
  }

  public void setEnrTolernce(int enrTolernce) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENRTOLERNCE, enrTolernce);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getCalibLength() {
    ParameterResult pr = Owner.getParameter(VISE_CALIBLENGTH, 0);
    return pr.IntVal();
  }

  public void setCalibLength(int calibLength) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_CALIBLENGTH, calibLength);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getCalibWeight() {
    ParameterResult pr = Owner.getParameter(VISE_CALIBWEIGHT, 0);
    return pr.IntVal();
  }

  public void setCalibWeight(int calibWeight) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_CALIBWEIGHT, calibWeight);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getCalibrWCRel() {
    ParameterResult pr = Owner.getParameter(VISE_CALIBRWCREL, 0);
    return pr.IntVal();
  }

  public void setCalibrWCRel(int calibrWCRel) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_CALIBRWCREL, calibrWCRel);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getCalibrWCAbs() {
    ParameterResult pr = Owner.getParameter(VISE_CALIBRWCABS, 0);
    return pr.IntVal();
  }

  public void setCalibrWCAbs(int calibrWCAbs) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_CALIBRWCABS, calibrWCAbs);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getVUScaleMult() {
    ParameterResult pr = Owner.getParameter(VISE_VUSCALEMULT, 0);
    return pr.IntVal();
  }

  public void setVUScaleMult(int vuScaleMult) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_VUSCALEMULT, vuScaleMult);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getVUOffsetAdd() {
    ParameterResult pr = Owner.getParameter(VISE_VUOFFSETADD, 0);
    return pr.IntVal();
  }

  public void setVUOffsetAdd(int vuOffsetAdd) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_VUOFFSETADD, vuOffsetAdd);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getSilMaxDwell() {
    ParameterResult pr = Owner.getParameter(VISE_SILMAXDWELL, 0);
    return pr.IntVal();
  }

  public void setSilMaxDwell(int silMaxDwell) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SILMAXDWELL, silMaxDwell);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEndMinDwell() {
    ParameterResult pr = Owner.getParameter(VISE_ENDMINDWELL, 0);
    return pr.IntVal();
  }

  public void setEndMinDwell(int endMinDwell) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_ENDMINDWELL, endMinDwell);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getSilAdaptWgt() {
    ParameterResult pr = Owner.getParameter(VISE_SILADAPTWGT, 0);
    return pr.IntVal();
  }

  public void setSilAdaptWgt(int silAdaptWgt) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SILADAPTWGT, silAdaptWgt);
    if (!pr.Status()) throw new PropertyVetoException();
  }

 public int getSilAdaptLag() {
    ParameterResult pr = Owner.getParameter(VISE_SILADAPTLAG, 0);
    return pr.IntVal();
  }

  public void setSilAdaptLag(int silAdaptLag) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SILADAPTWGT, silAdaptLag);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getMaxDuration() {
    ParameterResult pr = Owner.getParameter(VISE_MAXDURATION, 0);
    return pr.IntVal();
  }

  public void setMaxDuration(int maxDuration) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_MAXDURATION, maxDuration);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getAudQueueLen() {
    ParameterResult pr = Owner.getParameter(VISE_AUDQUEUELEN, 0);
    return pr.IntVal();
  }

  public void setAudQueueLen(int audQueueLen) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_AUDQUEUELEN, audQueueLen);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getBouSilMSecs() {
    ParameterResult pr = Owner.getParameter(VISE_BOUSILMSECS, 0);
    return pr.IntVal();
  }

  public void setBouSilMSecs(int bouSilMSecs) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_BOUSILMSECS, bouSilMSecs);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getEouSilMSecs() {
    ParameterResult pr = Owner.getParameter(VISE_EOUSILMSECS, 0);
    return pr.IntVal();
  }

  public void setEouSilMSecs(int eouSilMSecs) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_EOUSILMSECS, eouSilMSecs);
    if (!pr.Status()) throw new PropertyVetoException();
  }

   public int getMaxFrameCnt() {
    ParameterResult pr = Owner.getParameter(VISE_MAXFRAMECNT, 0);
    return pr.IntVal();
  }

  public void setMaxFrameCnt(int maxFrameCnt) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_MAXFRAMECNT, maxFrameCnt);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public void setSpeakerPath(String speakerPath) throws PropertyVetoException {
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PROPERTY_MASK)) {
      SysLog.println("ViseProperties.setSpeakerPath: setting VISE_SPEAKERPATH to \"" + speakerPath + "\"");
      if (SysLog.printLevel > 3) (new Throwable()).printStackTrace(SysLog.stream);
    }
    ParameterResult pr = Owner.setParameter(VISE_SPEAKERPATH, speakerPath);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public void setDfltSpeaker(String dfltSpeaker) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_DFLTSPEAKER, dfltSpeaker);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getFEVersion() {
    ParameterResult pr = Owner.getParameter(VISE_FRONTENDVER, 0);
    return pr.IntVal();
  }

  public void setFEVersion(int feVersion) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_FRONTENDVER, feVersion);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public boolean getVUOnOffSwitch() {
    ParameterResult pr = Owner.getParameter(VISE_VUONOFFSWCH, 0);
    return pr.IntVal() != 0;
  }

  public void setVUOnOffSwitch(boolean on) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_VUONOFFSWCH, on ? 1 : 0);
    if (!pr.Status()) throw new PropertyVetoException();
  }

   // FlexVISE Parameters
  public boolean getShowFrameData() {
    ParameterResult pr = Owner.getParameter(VISE_SHOWFRMDATA, 0);
    return pr.IntVal() != 0;
  }
  public void setShowFrameData(boolean showFrameData) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SHOWFRMDATA, showFrameData ? 1 : 0);
    if (!pr.Status()) throw new PropertyVetoException();
  }

   public int getStartShowFrame() {
    ParameterResult pr = Owner.getParameter(VISE_STARTSHOWFR, 0);
    return pr.IntVal();
  }

  public void setStartShowFrame(int startFrame) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_STARTSHOWFR, startFrame);
    if (!pr.Status()) throw new PropertyVetoException();
  }

   public int getFinalShowFrame() {
    ParameterResult pr = Owner.getParameter(VISE_FINALSHOWFR, 0);
    return pr.IntVal();
  }

  public void setFinalShowFrame(int startFrame) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_FINALSHOWFR, startFrame);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public boolean getShowAllTracebacks() {
    ParameterResult pr = Owner.getParameter(VISE_SHOWALLTBKS, 0);
    return pr.IntVal() != 0;
  }
  public void setShowAllTracebacks(boolean show) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SHOWALLTBKS, show ? 1 : 0);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public boolean getShowFinalTraceback() {
    ParameterResult pr = Owner.getParameter(VISE_SHOWFINALTB, 0);
    return pr.IntVal() != 0;
  }
  public void setShowFinalTraceback(boolean show) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SHOWFINALTB, show ? 1 : 0);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public boolean getShowTracebackString() {
    ParameterResult pr = Owner.getParameter(VISE_SHOWTBSTRNG, 0);
    return pr.IntVal() != 0;
  }
  public void setShowTracebackString(boolean show) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SHOWTBSTRNG, show ? 1 : 0);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getMaxNodeDwell() {
    ParameterResult pr = Owner.getParameter(VISE_MAXNODDWELL, 0);
    return pr.IntVal();
  }

  public void setMaxNodeDwell(int dwell) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_MAXNODDWELL, dwell);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public float getSearchBeamWidth() {
    ParameterResult pr = Owner.getParameter(VISE_VSBEAMWIDTH, (float)0.0);
    return (float) pr.FloatVal();
  }

  public void setSearchBeamWidth(float beamWidth) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_VSBEAMWIDTH, beamWidth);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public float getNewPhoneBeamWidth() {
    ParameterResult pr = Owner.getParameter(VISE_NPBEAMWIDTH, (float)0.0);
    return (float) pr.FloatVal();
  }

  public void setNewPhoneBeamWidth(float beamWidth) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_NPBEAMWIDTH, beamWidth);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public float getNewWordBeamWidth() {
    ParameterResult pr = Owner.getParameter(VISE_NWBEAMWIDTH, (float)0.0);
    return (float) pr.FloatVal();
  }

  public void setNewWordBeamWidth(float beamWidth) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_NWBEAMWIDTH, beamWidth);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public int getChannelsPerFrameLimit() {
    ParameterResult pr = Owner.getParameter(VISE_CHANSPERFRM, 0);
    return pr.IntVal();
  }

  public void setChannelsPerFrameLimit(int limit) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_CHANSPERFRM, limit);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public boolean getUseProfiler() {
    ParameterResult pr = Owner.getParameter(VISE_USEPROFILER, 0);
    return pr.IntVal() != 0;
  }
  public void setUseProfiler(boolean use) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_USEPROFILER, use ? 1 : 0);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public boolean getShowCepstralFrame() {
    ParameterResult pr = Owner.getParameter(VISE_SHOWCEPDATA, 0);
    return pr.IntVal() != 0;
  }
  public void setShowCepstralFrame(boolean show) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SHOWCEPDATA, show ? 1 : 0);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public boolean getShowFeatureFrame() {
    ParameterResult pr = Owner.getParameter(VISE_SHOWFEATDAT, 0);
    return pr.IntVal() != 0;
  }
  public void setShowFeatureFrame(boolean show) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SHOWFEATDAT, show ? 1 : 0);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public boolean getShowVQFrame() {
    ParameterResult pr = Owner.getParameter(VISE_SHOWVQFRAME, 0);
    return pr.IntVal() != 0;
  }
  public void setShowVQFrame(boolean show) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SHOWVQFRAME, show ? 1 : 0);
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public boolean getShowEPFrame() {
    ParameterResult pr = Owner.getParameter(VISE_SHOWEPFRAME, 0);
    return pr.IntVal() != 0;
  }
  public void setShowEPFrame(boolean show) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_SHOWEPFRAME, show ? 1 : 0);
    if (!pr.Status()) throw new PropertyVetoException();
  }

   public int getNoiseTracking() {
    ParameterResult pr = Owner.getParameter(VISE_NOISETRKING, 0);
    return pr.IntVal();
  }

  public void setNoiseTracking(int noiseTracking) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_NOISETRKING, noiseTracking);
    if (!pr.Status()) throw new PropertyVetoException();
  }

   public int getTrainScheme() {
    ParameterResult pr = Owner.getParameter(VISE_TRAINSCHEME, 0);
    return pr.IntVal();
  }

   // Implementation of RecognizerProperties interface

  public float getCompleteTimeout() {
    ParameterResult pr = Owner.getParameter(VISE_COMPLETETO, 0);
    return (float) pr.IntVal() / 1000;
  }

  public void setCompleteTimeout(float completeTO) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_COMPLETETO, (int)(1000 * completeTO));
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public float getIncompleteTimeout() {
    ParameterResult pr = Owner.getParameter(VISE_INCOMPLTTO, 0);
    return (float) pr.IntVal() / 1000;
  }

  public void setIncompleteTimeout(float incompleteTO) throws PropertyVetoException {
    ParameterResult pr = Owner.setParameter(VISE_INCOMPLTTO, (int)(1000 * incompleteTO));
    if (!pr.Status()) throw new PropertyVetoException();
  }

  public boolean isResultAudioProvided() { return getAudQueueLen() > 0; }
  public void    setResultAudioProvided(boolean ap) throws PropertyVetoException { setAudQueueLen(ap ? DFLT_AUDQUEUELEN : 0); }

  public boolean isTrainingProvided() { return Owner.trainingProvided; }
  public void    setTrainingProvided(boolean tp) throws PropertyVetoException { Owner.trainingProvided = tp; }

  public float   getConfidenceLevel() { return (float)0.; }
  public void    setConfidenceLevel(float foo) throws PropertyVetoException {}

  public float   getSensitivity() { return (float)0.; }
  public void    setSensitivity(float foo) throws PropertyVetoException {}

  public int     getNumResultAlternatives() { return getNHypotheses(); }
  public void    setNumResultAlternatives(int nHypotheses) throws PropertyVetoException { setNHypotheses(nHypotheses); }

  public float   getSpeedVsAccuracy() { return (float)0.; }
  public void    setSpeedVsAccuracy(float foo) throws PropertyVetoException {}

   // Implementation of EngineProperties interface (inherited via RecognizerProperties)

  public java.awt.Component getControlComponent() { return null; }

  public void    reset() {}

  public void    addPropertyChangeListener(java.beans.PropertyChangeListener l) {}
  public void    removePropertyChangeListener(java.beans.PropertyChangeListener l) {}
}
