#ifndef __voxware_engine_recognition_sapivise_SapiVise__
#define __voxware_engine_recognition_sapivise_SapiVise__

#pragma interface

#include <voxware/engine/recognition/BaseRecognizer.h>

extern "Java"
{
  namespace gnu
  {
    namespace gcj
    {
      class RawData;
    }
  }
  namespace javax
  {
    namespace speech
    {
      namespace recognition
      {
        class SpeakerProfile;
        class SpeakerManager;
        class RecognizerProperties;
        class RecognizerModeDesc;
        class Grammar;
      }
    }
  }
  namespace voxware
  {
    namespace engine
    {
      namespace recognition
      {
        namespace sapivise
        {
          class SapiVise;
          class ParameterResult;
          class ViseTrainerFactory;
          class RawResult;
          class NotifyThread;
        }
      }
    }
  }
}

class voxware::engine::recognition::sapivise::SapiVise : public ::voxware::engine::recognition::BaseRecognizer
{
public: // actually package-private
  static jboolean loadLibrary ();
public:
  virtual void n_notify (::voxware::engine::recognition::sapivise::RawResult *);
private:
  ::gnu::gcj::RawData *newRecognizer ();
  void n_deallocate ();
  void n_pause ();
  void n_resume ();
  jboolean n_initialize (::java::lang::String *);
  jboolean n_calibrate ();
  jint n_levelget ();
  jboolean n_levelset (jint);
  jboolean n_deletespeaker (::java::lang::String *);
  JArray< ::java::lang::String *> *n_listspeakers ();
  jboolean n_readspeaker (::java::lang::String *, jbyteArray);
  jbyteArray n_writespeaker (::java::lang::String *);
  jboolean n_selectspeaker (::java::lang::String *);
  ::java::lang::String *n_queryspeaker ();
  jboolean n_setparameter (jint);
  jboolean n_getparameter (jint, jint);
  jboolean n_setparameter (jint, jint);
  jboolean n_getparameter (jint, jfloat);
  jboolean n_setparameter (jint, jfloat);
  jboolean n_setparameter (jint, ::java::lang::String *);
  jboolean n_settimeout (jint);
  jboolean n_setutterance (::java::lang::String *);
  void n_flush ();
  jint n_getVU ();
public:
  SapiVise ();
  SapiVise (::javax::speech::recognition::RecognizerModeDesc *);
  virtual ::voxware::engine::recognition::sapivise::ViseTrainerFactory *getTrainerFactory ();
  virtual ::voxware::engine::recognition::sapivise::ParameterResult *setParameter (jint);
  virtual ::voxware::engine::recognition::sapivise::ParameterResult *getParameter (jint, jint);
  virtual ::voxware::engine::recognition::sapivise::ParameterResult *setParameter (jint, jint);
  virtual ::voxware::engine::recognition::sapivise::ParameterResult *getParameter (jint, jfloat);
  virtual ::voxware::engine::recognition::sapivise::ParameterResult *setParameter (jint, jfloat);
  virtual ::voxware::engine::recognition::sapivise::ParameterResult *setParameter (jint, ::java::lang::String *);
  virtual jfloat levelGet ();
  virtual jboolean levelSet (jfloat);
  virtual jboolean calibrate ();
  virtual jboolean setTimeout (jint);
  virtual jint getVU ();
  virtual ::javax::speech::recognition::RecognizerProperties *getRecognizerProperties ();
  virtual ::javax::speech::recognition::SpeakerManager *getSpeakerManager ();
  virtual ::javax::speech::recognition::Grammar *readVendorGrammar (::java::io::InputStream *);
public:  // actually protected
  virtual void linkGrammars ();
  virtual void endGrammarChanges ();
  virtual void changeEnabled (::java::util::Vector *);
  virtual void allocateEngine ();
  virtual void deallocateEngine ();
  virtual void pauseEngine ();
  virtual void resumeEngine ();
public:
  virtual jboolean deleteSpeaker (::javax::speech::recognition::SpeakerProfile *);
  virtual jboolean readVendorSpeakerProfile (::java::lang::String *, jbyteArray);
  virtual jbyteArray writeVendorSpeakerProfile (::java::lang::String *);
  virtual jboolean setCurrentSpeaker (::javax::speech::recognition::SpeakerProfile *);
  virtual ::javax::speech::recognition::SpeakerProfile *getCurrentSpeaker ();
  virtual JArray< ::javax::speech::recognition::SpeakerProfile *> *listKnownSpeakers ();
  virtual jboolean setUtterance (::java::lang::String *);
  virtual void flushUtterance ();
  virtual void notifyResult (::voxware::engine::recognition::sapivise::RawResult *);
  static ::java::lang::String *LibraryName;
  static ::java::lang::String *LibraryInitializer;
public: // actually package-private
  ::voxware::engine::recognition::sapivise::NotifyThread * __attribute__((aligned(__alignof__( ::voxware::engine::recognition::BaseRecognizer )))) notifyThread;
  ::javax::speech::recognition::Grammar *currentGrammar;
private:
  ::java::util::Hashtable *wordList;
public: // actually package-private
  jboolean trainingProvided;
  jint IntVal;
  jfloat FloatVal;
public:
  ::gnu::gcj::RawData *recognizer;
private:
  static jfloatArray gains;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_recognition_sapivise_SapiVise__ */
