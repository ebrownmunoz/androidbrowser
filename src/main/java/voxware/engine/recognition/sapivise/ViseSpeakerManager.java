package voxware.engine.recognition.sapivise;

import javax.speech.*;
import javax.speech.recognition.*;

import voxware.engine.recognition.*;
import voxware.util.SysLog;

import java.io.*;
import java.util.*;

public class ViseSpeakerManager implements SpeakerManager {
  
  SapiVise Owner;

  public ViseSpeakerManager(SapiVise owner) {
    Owner = owner;
  }

  public void setCurrentSpeaker(SpeakerProfile profile) throws IllegalArgumentException {
    if (!Owner.setCurrentSpeaker(profile)) throw new IllegalArgumentException();
  }

  public SpeakerProfile getCurrentSpeaker() {
    return Owner.getCurrentSpeaker();
  }

  public SpeakerProfile[] listKnownSpeakers() {
    return Owner.listKnownSpeakers();
  }

  public SpeakerProfile newSpeakerProfile(SpeakerProfile profile) throws IllegalArgumentException {
    return null;
  }

  public void deleteSpeaker(SpeakerProfile profile) {
    if (!Owner.deleteSpeaker(profile)) SysLog.println("ViseSpeakerManager ERROR - can't delete " + profile.getName());
  }

  public void saveCurrentSpeakerProfile() {
  }

  public void revertCurrentSpeaker() {
  }

  public void writeVendorSpeakerProfile(java.io.OutputStream out, SpeakerProfile profile) throws java.io.IOException {
    byte[] voiData = Owner.writeVendorSpeakerProfile(profile.getName());
    if (voiData == null) throw new java.io.IOException("ViseSpeakerManager ERROR - wvsp encounters \"null\" voiData");
    out.write(voiData, 0, voiData.length);
  }

  public SpeakerProfile readVendorSpeakerProfile(java.io.InputStream input) throws java.io.IOException, VendorDataException {

    int spkrNameLen = input.available();
    byte[] byteArr = new byte[spkrNameLen];
    input.read(byteArr, 0, spkrNameLen);

    String spkrName = new String(byteArr, "UTF-8");      

    int firstByte = input.read();
    int voiBufLen = input.available() + 1;

    byte[] voiBuf = new byte[voiBufLen];
    voiBuf[0] = (byte) firstByte;
    input.read(voiBuf, 1, voiBufLen - 1);
  
     // Add the speaker to SapiVise
    if (!Owner.readVendorSpeakerProfile(spkrName, voiBuf)) {
      SysLog.println("readVendorSpeakerProfile: ERROR - commit FAILED");
      return null;
    }
    
    SpeakerProfile profile = new SpeakerProfile();
    profile.setName(spkrName);
    return profile;
  }

  public java.awt.Component getControlComponent() {
    return null;
  }
}
  
