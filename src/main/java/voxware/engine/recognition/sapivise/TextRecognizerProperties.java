/**
 * 
 */
package voxware.engine.recognition.sapivise;

import java.awt.Component;
import java.beans.PropertyChangeListener;

import javax.speech.PropertyVetoException;
import javax.speech.recognition.RecognizerProperties;

/**
 * @author eric
 *
 */
public class TextRecognizerProperties implements RecognizerProperties {

	/**
	 * 
	 */
	public TextRecognizerProperties() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see javax.speech.EngineProperties#getControlComponent()
	 */
	@Override
	public Component getControlComponent() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see javax.speech.EngineProperties#reset()
	 */
	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.speech.EngineProperties#addPropertyChangeListener(java.beans.PropertyChangeListener)
	 */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.speech.EngineProperties#removePropertyChangeListener(java.beans.PropertyChangeListener)
	 */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#getConfidenceLevel()
	 */
	@Override
	public float getConfidenceLevel() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#setConfidenceLevel(float)
	 */
	@Override
	public void setConfidenceLevel(float confidenceLevel)
			throws PropertyVetoException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#getSensitivity()
	 */
	@Override
	public float getSensitivity() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#setSensitivity(float)
	 */
	@Override
	public void setSensitivity(float sensitivity) throws PropertyVetoException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#getSpeedVsAccuracy()
	 */
	@Override
	public float getSpeedVsAccuracy() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#setSpeedVsAccuracy(float)
	 */
	@Override
	public void setSpeedVsAccuracy(float speedVsAccuracy)
			throws PropertyVetoException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#getCompleteTimeout()
	 */
	@Override
	public float getCompleteTimeout() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#setCompleteTimeout(float)
	 */
	@Override
	public void setCompleteTimeout(float timeout) throws PropertyVetoException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#getIncompleteTimeout()
	 */
	@Override
	public float getIncompleteTimeout() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#setIncompleteTimeout(float)
	 */
	@Override
	public void setIncompleteTimeout(float timeout)
			throws PropertyVetoException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#getNumResultAlternatives()
	 */
	@Override
	public int getNumResultAlternatives() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#setNumResultAlternatives(int)
	 */
	@Override
	public void setNumResultAlternatives(int num) throws PropertyVetoException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#isResultAudioProvided()
	 */
	@Override
	public boolean isResultAudioProvided() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#setResultAudioProvided(boolean)
	 */
	@Override
	public void setResultAudioProvided(boolean audio)
			throws PropertyVetoException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#isTrainingProvided()
	 */
	@Override
	public boolean isTrainingProvided() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see javax.speech.recognition.RecognizerProperties#setTrainingProvided(boolean)
	 */
	@Override
	public void setTrainingProvided(boolean trainingProvided)
			throws PropertyVetoException {
		// TODO Auto-generated method stub

	}

}
