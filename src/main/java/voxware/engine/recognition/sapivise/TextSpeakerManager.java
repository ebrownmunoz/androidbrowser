package voxware.engine.recognition.sapivise;

import javax.speech.*;
import javax.speech.recognition.*;

import voxware.engine.recognition.*;
import voxware.textbrowser.SapiTextRecognizer;
import voxware.util.SysLog;

import java.io.*;
import java.util.*;

public class TextSpeakerManager implements SpeakerManager {

  SapiTextRecognizer Owner;

  public TextSpeakerManager(SapiTextRecognizer recognizer) {
	  this.Owner = recognizer;
  }

  public void setCurrentSpeaker(SpeakerProfile profile) throws IllegalArgumentException {
	  Owner.setCurrentSpeaker(profile);
  }

  public SpeakerProfile getCurrentSpeaker() {
    return Owner.getCurrentSpeaker();
  }

  public SpeakerProfile[] listKnownSpeakers() {
    return new SpeakerProfile[0];
  }

  public SpeakerProfile newSpeakerProfile(SpeakerProfile profile) throws IllegalArgumentException {
    return null;
  }

  public void deleteSpeaker(SpeakerProfile profile) {
   
  }

  public void saveCurrentSpeakerProfile() {
  }

  public void revertCurrentSpeaker() {
  }

  public void writeVendorSpeakerProfile(java.io.OutputStream out, SpeakerProfile profile) throws java.io.IOException {
    
  }

  public SpeakerProfile readVendorSpeakerProfile(java.io.InputStream input) throws java.io.IOException, VendorDataException {

	    int spkrNameLen = input.available();
	    byte[] byteArr = new byte[spkrNameLen];
	    input.read(byteArr, 0, spkrNameLen);

	    String spkrName = new String(byteArr, "UTF-8");      

	    int firstByte = input.read();
	    int voiBufLen = input.available() + 1;

	    byte[] voiBuf = new byte[voiBufLen];
	    voiBuf[0] = (byte) firstByte;
	    input.read(voiBuf, 1, voiBufLen - 1);
	  
	     // Add the speaker to SapiVise
	    if (!Owner.readVendorSpeakerProfile(spkrName, voiBuf)) {
	      SysLog.println("readVendorSpeakerProfile: ERROR - commit FAILED");
	      return null;
	    }
	    
	    SpeakerProfile profile = new SpeakerProfile();
	    profile.setName(spkrName);
	    return profile;
	  }



  public java.awt.Component getControlComponent() {
    return null;
  }
}
  
