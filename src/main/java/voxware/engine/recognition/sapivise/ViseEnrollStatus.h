#ifndef __voxware_engine_recognition_sapivise_ViseEnrollStatus__
#define __voxware_engine_recognition_sapivise_ViseEnrollStatus__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace engine
    {
      namespace recognition
      {
        namespace sapivise
        {
          class ViseEnrollStatus;
        }
      }
    }
  }
}

class voxware::engine::recognition::sapivise::ViseEnrollStatus : public ::java::lang::Object
{
public:
  ViseEnrollStatus (jint, jint, jint);
  virtual jint Status () { return CMStatus; }
  virtual jint Count () { return CMCount; }
  virtual jint Needed () { return CMNeeded; }
private:
  jint __attribute__((aligned(__alignof__( ::java::lang::Object ))))  CMStatus;
  jint CMCount;
  jint CMNeeded;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_recognition_sapivise_ViseEnrollStatus__ */
