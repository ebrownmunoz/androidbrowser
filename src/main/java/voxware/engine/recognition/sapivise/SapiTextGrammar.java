package voxware.engine.recognition.sapivise;

import javax.speech.*;
import javax.speech.recognition.*;

import java.io.*;
import java.net.*;
import java.util.*;

import voxware.engine.recognition.*;
import voxware.engine.recognition.mock.FileMockSpeaker;
import voxware.engine.recognition.mock.MockSpeaker;
import voxware.textbrowser.SapiTextRecognizer;
import voxware.util.LibraryLoader;
import voxware.util.SysLog;
import voxware.voxfile.VoxFile;
import voxware.voxfile.VoxFormatException;
import voxware.voxfile.VoxGrammar;

public class SapiTextGrammar extends BaseRuleGrammar {

  public static final String EnrollmentRule = "<ANY_WORD>";
  public static final String CalibrationRule = "<CALIBRATION>";


  public static final int RECOMMENDED = 1;
  public static final int REQUIRED    = 2;

  private byte      recImage[];  // Kept around pending future versions that can switch RuleGrammars
  private String    defaultRuleName;
  
  private MockSpeaker mockSpeaker; // kept for FileMockSpeaker
  
  private VoxFile voxFile;

  public SapiTextGrammar(SapiTextRecognizer sapiVise, String name) {
    super((BaseRecognizer)sapiVise, name);
  }

  public SapiTextGrammar(SapiTextRecognizer sapiVise, InputStream recStream) throws IOException {
     // The grammar (application) name is supplied by the load(); no name is yet known here
    this(sapiVise, (String)null);
     // Read the grammar image from recStream into the recImage buffer
    int recSize = recStream.available();            // Throws IOException
     // Load the grammar into SAPIVISE and get its name and the names of its rules
    
    voxFile = new VoxFile();
    
    try {
		voxFile.read(recStream);
	} catch (VoxFormatException e) {
		System.err.println("VoxFile: Bad Grammar");
	}
    this.load();
  }

  public void deallocate() {
  }

  protected void finalize() throws Throwable {
  }

  public boolean isResultAudioProvided() {
    return (((SapiVise)recognizer).getParameter(ViseProperties.VISE_AUDQUEUELEN, 0).IntVal() > 0);
  }

  public boolean isTrainingProvided() {
    return ((SapiVise)recognizer).trainingProvided;
  }

   // Load SAPIVISE with the recfile image in recImage
  protected void load() {
	    // Load SAPIVISE with the recfile image in recImage
	    // n_load(recImage);

	     // Get the names of things
	    myName = "picking";
	    
	    List grammars = voxFile.getGrammars();
	    String[] ruleNames = new String[grammars.size()];
	    int i = 0;
	    
	    for (VoxGrammar each : (List<VoxGrammar>)grammars) {
	    	ruleNames[i++] = voxFile.getText(each.getGrammarName()); 
	    }
	    
	    defaultRuleName = ruleNames[0];
	    

	    
	    
	    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK) && defaultRuleName != null)
	      SysLog.println("SapiViseGrammar.load: the default rule for grammar \"" + myName + "\" is \"" + getDefaultRuleName() + "\"");
	    RuleName rn = new RuleName(null, getName(), EnrollmentRule);
	    setRule(EnrollmentRule, (Rule) rn, true);

	     // Add the calibration rule
	    rn = new RuleName(null, getName(), CalibrationRule);
	    setRule(CalibrationRule, (Rule) rn, true);

	     // Add all the grammar's rules to the superclass's rule hash
	    for (int jj = 0; jj < ruleNames.length; jj++) {
	      rn = new RuleName(null, getName(), ruleNames[jj]);
	      setRule(ruleNames[jj], (Rule) rn, true);
	      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK))
	        SysLog.println("SapiViseGrammar.load: rule[" + jj + "] is \"" + ruleNames[jj] + "\"");
	    }

  }

  public String getDefaultRuleName() {
    return defaultRuleName;
  }

   // Overrides activate() in BaseGrammar to add actual SAPIVISE activate/deactivate call
  public boolean activate(boolean active) {
      if (this.active != active) {
          this.active = active;
          if (active) {
        	  synchronized (this) { // Guards mockSpeaker
        		  mockSpeaker.requestStartRecognition(getName());
        	  }
        	  grammarActivated();
          } else {
        	  grammarDeactivated();
          }
          return true;
        } else {
          return false;
        }
    
    
    
  }

  public int addRule(RuleName rn) {
    
    return 1; // Never called
  }

  public String[] parse(String phrase) {
    String[] ruleNames = null;
    return ruleNames; // Only called by GrammarRules Applet
  }

  public String[] trainPhrase(int extent) {
    String[] prompts = null;

    return prompts;
  }

  public boolean startTrainingPass() {
    return true;
  }

  public boolean endTrainingPass() {
    return endTrainingPass(null);
  }

  public boolean endTrainingPass(String[] words) {
    return true;
  }

  public boolean completeTraining() {
    return completeTraining(null);
  }

  public boolean completeTraining(String[] words) {
    return true;
  }

  public boolean saveModels(int minTrainingCount) {
    return true;
  }

  public boolean saveModels(String[] words, int minTrainingCount) {
    return true;
  }
  
  public synchronized void setMockSpeaker (MockSpeaker speaker) {
	  // synchronized to gaurd mockSpeaker
	  this.mockSpeaker = speaker;
  }
}
