package voxware.engine.recognition.keypad;

import java.util.Locale;

import javax.speech.*;
import javax.speech.recognition.*;

public class KeypadModeDesc extends RecognizerModeDesc implements EngineCreate {

  public KeypadModeDesc() {
    super("VICE",               // Engine name (Voxware Integrated Character Engine)
          "dtmf",               // Mode name
           Locale.US,           // Locale
           null,                // Running?
           Boolean.FALSE,       // Dictation grammar supported?
           null);               // Profile[]
  }

  public Engine createEngine() throws IllegalArgumentException, EngineException, SecurityException {
    Recognizer recognizer = new KeypadRecognizer(this);
    if (recognizer == null) throw new EngineException();
    return recognizer;
  }
}
