package voxware.engine.recognition.keypad;

import javax.speech.*;
import javax.speech.recognition.*;

public class KeypadEngineCentral implements EngineCentral {

  private static KeypadModeDesc keypadModeDesc = new KeypadModeDesc();

  public EngineList createEngineList(EngineModeDesc require) {
    EngineList engineList = null;
    if (require == null || keypadModeDesc.match(require)) {
      engineList = new EngineList();
      engineList.addElement(keypadModeDesc);
    }
    return engineList;
  }
}
