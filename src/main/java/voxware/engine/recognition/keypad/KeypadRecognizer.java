package voxware.engine.recognition.keypad;

import java.util.*;
import javax.speech.*;
import javax.speech.recognition.*;

import voxware.engine.*;
import voxware.engine.recognition.*;
import voxware.util.Mailbox;
import voxware.util.Flag;
import voxware.util.SysLog;

 /** The KeypadRecognizer */
public class KeypadRecognizer extends BaseRecognizer implements TokenListener, Cloneable {

  public static final int  COMMAND_PRIORITY = 0;
  public static final int  KEYPRESS_PRIORITY = COMMAND_PRIORITY + 1;
  public static final int  NUM_PRIORITIES = KEYPRESS_PRIORITY + 1;

  public static final String ABORT_SIGNAL = "ABORT";

  protected RecognitionThread  recognitionThread;           // The underlying recognizer
  protected Mailbox            mailbox;                     // Prioritized mailbox of Objects mailed to the RecognitionThread (e.g., keypress events)
  protected int                timeout;                     // Milliseconds
  protected int                completeTimeout;             // Milliseconds
  protected int                incompleteTimeout;           // Milliseconds
  protected int                numHypotheses;
  protected boolean            paused;

  public KeypadRecognizer(RecognizerModeDesc mode) {
    super(mode, true);
  }

  /// TokenListener Implementation ///

  public void token(TokenEvent tokenEvent) {
    if (!paused) mailbox.insert(tokenEvent, KEYPRESS_PRIORITY);
  }

  /// End of TokenListener Implementation ///

  /// Engine Methods (override BaseEngine) ///

  public EngineProperties getEngineProperties() {
    return getRecognizerProperties();
  }

  /// End of Engine Methods ///

  /// BaseEngine Methods ///

  protected void allocateEngine() throws EngineException {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK)) SysLog.println("KeypadRecognizer.allocateEngine: allocating resources");
    super.allocateEngine();
    mailbox = new Mailbox(NUM_PRIORITIES);
    recognitionThread = new RecognitionThread("KeypadRecognizer");
    recognitionThread.start();
    addEngineListener(recognitionThread);                   // The internal recognition Thread is a RecognizerListener
  }

  protected void deallocateEngine() throws EngineException {
    super.deallocateEngine();
    removeEngineListener(recognitionThread);
    mailbox.insert(ABORT_SIGNAL, COMMAND_PRIORITY);
    try {
      recognitionThread.join();
    } catch (InterruptedException e) {}
    recognitionThread = null;
    mailbox = null;
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK)) SysLog.println("KeypadRecognizer.deallocateEngine: deallocated");
  }

  protected void pauseEngine() throws EngineStateError {
    super.pauseEngine();                                    // Throws EngineStateError
    paused = true;
    mailbox.flush(KEYPRESS_PRIORITY);
  }

  protected void resumeEngine() throws AudioException, EngineStateError {
    super.resumeEngine();                                   // Throws AudioException, EngineStateError
    mailbox.flush(KEYPRESS_PRIORITY);
    paused = false;
  }

  /// End of BaseEngine Methods ///

  /// Recognizer Methods (override BaseRecognizer) ///

  public RecognizerProperties getRecognizerProperties() {
    return new KeypadRecognizer.Properties();
  }

  public class Properties implements RecognizerProperties {

    /// Implementation of RecognizerProperties interface
    public float   getCompleteTimeout( ) { return (float)completeTimeout / (float)1000.0; }
    public void    setCompleteTimeout(float cto) { completeTimeout = Math.round(cto * (float)1000.0); }
    public float   getIncompleteTimeout() { return (float)incompleteTimeout / (float)1000.0; }
    public void    setIncompleteTimeout(float icto) { incompleteTimeout = Math.round(icto * (float)1000.0); }
    public boolean isResultAudioProvided() { return false; }
    public void    setResultAudioProvided(boolean ap) {}
    public boolean isTrainingProvided() { return false; }
    public void    setTrainingProvided(boolean tp) {}
    public float   getConfidenceLevel() { return (float)1.0; }
    public void    setConfidenceLevel(float cl) {}
    public float   getSensitivity() { return (float)1.0; }
    public void    setSensitivity(float cl) {}
    public int     getNumResultAlternatives() { return numHypotheses; }
    public void    setNumResultAlternatives(int na) { numHypotheses = na; }
    public float   getSpeedVsAccuracy() { return (float)0.0; }
    public void    setSpeedVsAccuracy(float sva) {}

    /// Implementation of EngineProperties interface (inherited via RecognizerProperties)
    public java.awt.Component getControlComponent() { return null; }
    public void    reset() {}
    public void    addPropertyChangeListener(java.beans.PropertyChangeListener l) {}
    public void    removePropertyChangeListener(java.beans.PropertyChangeListener l) {}

    /// Extensions
    public int     getMaxDuration() { return timeout; }
    public void    setMaxDuration(int timeout) { KeypadRecognizer.this.setTimeout(timeout); }
  }

  /// End of Recognizer Methods ///

  /// BaseRecognizer Methods ///

   // Override BaseRecognizer getAudioManager() method
  public AudioManager getAudioManager() {
    SysLog.println("KeypadRecognizer.getAudioManager: WARNING - not supported");
    return audioManager;
  }

  /// End of BaseRecognizer Methods ///

  /// Local Methods ///

   // Adjust the recognition timeout on the fly
  public void setTimeout(int timeout) {
    Mailbox mailboxCopy = mailbox;
    this.timeout = timeout;
    if (mailboxCopy != null) mailboxCopy.insert(new Integer(timeout));
  }

   // Utility method to allow the RecognitionThread to transition the recognizer to PROCESSING state
  protected synchronized void startProcessing() {
    if (!testEngineState(PROCESSING)) {
      long oldEngineState = engineState;
      engineState |= PROCESSING;
      recognizerProcessing(oldEngineState, engineState);
    }
  }

   // Utility method to allow the RecognitionThread to generate RESULT_CREATED event and send it to any ResultListeners
  protected void resultCreated(Result result) {
    super.resultCreated(result);
  }

   // Utility method to allow the RecognitionThread to clone this
  protected BaseRecognizer copy() {
    return (BaseRecognizer)super.clone();
  }

   // The recognitionThread
  protected class RecognitionThread extends Thread implements RecognizerListener {

    protected boolean        suspended;                     // Is currently suspended
    protected BaseRecognizer recognizer;                    // The internal copy of the Recognizer
    protected Vector         parsers = new Vector();        // The set of active RuleParsers
    protected Flag           suspensionFlag = new Flag();
    protected Flag           resumptionFlag = new Flag();
    protected ParsedResult   result;

    public RecognitionThread(String name) {
      super(name);
      setDaemon(true);
      suspensionFlag.receiver(mailbox, suspensionFlag, COMMAND_PRIORITY);
      resumptionFlag.receiver(mailbox, resumptionFlag, COMMAND_PRIORITY);
    }

     // A nested Runnable to send results to the ResultListeners (doing it this way prevents potential deadlock with commitChanges())
    private class Notifier extends Mailbox implements Runnable {
      public void run() {
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
          SysLog.println("KeypadRecognizer.RecognitionThread.Notifier.run: launched");
        setPriority(Thread.MAX_PRIORITY - 3);
        try {
          while (true) {
            Object mail = this.remove();
            if (mail instanceof ParsedResult) {
              ParsedResult result = (ParsedResult)mail;
              int state = result.getResultState();
              if (state == Result.ACCEPTED) {
                if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                  SysLog.println("KeypadRecognizer.RecognitionThread.Notifier.run: calling resultAccepted");
                result.resultAccepted(KeypadRecognizer.this, (BaseGrammar)result.getGrammar());
              } else if (state == Result.REJECTED) {
                if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                  SysLog.println("KeypadRecognizer.RecognitionThread.Notifier.run: calling resultRejected");
                result.resultRejected(KeypadRecognizer.this, null);
              }
            } else {
              throw new RuntimeException("KeypadRecognizer.RecognitionThread.Notifier.run: UNEXPECTED " + mail.getClass().getName() + " in mailbox");
            }
          }
        } catch (InterruptedException e) {
           // Fall through to terminate run()
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
            SysLog.println("KeypadRecognizer.RecognitionThread.Notifier.run: terminating due to InterruptedException");
        }
      }
    }

    public void run() {

      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
        SysLog.println("KeypadRecognizer.RecognitionThread.run: launched");

      Notifier notifier = new Notifier();
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
        SysLog.println("KeypadRecognizer.RecognitionThread.run: constructed Notifier " + notifier.toString());
      (new Thread(notifier, "KeypadNotifier")).start();
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
        SysLog.println("KeypadRecognizer.RecognitionThread.run: started notifier thread");
      Object mail;
      long   end = System.currentTimeMillis() + timeout;

      try {

        while (true) {

           // Get the next Object from the mailbox, waiting for it if necessary
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
            SysLog.println("KeypadRecognizer.RecognitionThread.run: waiting for mail in " + mailbox.toString());

          if (suspended) {

             // Wait for a command while SUSPENDED
            mail = mailbox.remove(COMMAND_PRIORITY);

             // If it is the resumption Flag, lower it and resume
            if (mail == resumptionFlag) {
              suspended = false;
              resumptionFlag.lower();
              if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                SysLog.println("KeypadRecognizer.RecognitionThread.run: resuming");
               // Construct a list of initialized RuleParsers, one RuleParser for each active grammar
              parsers.removeAllElements();
               // Get a local copy of the recognizer, in case changesCommitted() tries to change it out from under us
              BaseRecognizer recognizerCopy = recognizer;
              if (recognizerCopy != null) {
                RuleGrammar[] grammars = recognizerCopy.listRuleGrammars();
                if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                  SysLog.println("KeypadRecognizer.RecognitionThread.run: constructing RuleParsers for the " + grammars.length + " RuleGrammars");
                for (int i = 0; i < grammars.length; i++) {
                  if (grammars[i].isActive()) {
                    RuleParser ruleParser = new RuleParser(recognizerCopy, grammars[i]);
                    try {
                      ruleParser.startParse();                  // Throws GrammarException
                      parsers.addElement(ruleParser);
                    } catch (GrammarException e) {
                      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                        SysLog.println("KeypadRecognizer.RecognitionThread.run: " + e.toString());
                    }
                  }
                }
                if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                  SysLog.println("KeypadRecognizer.RecognitionThread.run: there are " + parsers.size() + " active RuleParsers");
              }
               // Stop the source if there are no active RuleParsers; start it otherwise
              try {
                if (parsers.isEmpty()) KeypadRecognizer.this.pause();
                else                   KeypadRecognizer.this.resume();
              } catch (AudioException e) {}
              result = null;
              if (timeout != 0) end = System.currentTimeMillis() + timeout;

             // Else if it is the suspension Flag, lower it and ignore it
            } else if (mail == suspensionFlag) {
              suspensionFlag.lower();
              if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                SysLog.println("KeypadRecognizer.RecognitionThread.run: ignoring suspension flag received while SUSPENDED");

              // Else if it is a timeout, set the timeout
            } else if (mail instanceof Integer) {
              timeout = ((Integer)mail).intValue();
              if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                SysLog.println("KeypadRecognizer.RecognitionThread.run: new timeout (" + timeout + " ms) received while SUSPENDED");

             // Else if it is the ABORT_SIGNAL, abort
            } else if (mail instanceof String && ((String)mail).equals(ABORT_SIGNAL)) {
              if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                SysLog.println("KeypadRecognizer.RecognitionThread.run: ABORT signal received while SUSPENDED");
              break;

             // Otherwise, something evil has come our way
            } else {
              throw new RuntimeException("KeypadRecognizer.RecognitionThread.run: UNEXPECTED " + mail.getClass().getName() + " in mailbox while SUSPENDED");
            }

          } else {

            long timeRemaining;

             // Wait for anything while LISTENING
            if (timeout == 0) {
              mail = mailbox.remove();
            } else if ((timeRemaining = end - System.currentTimeMillis()) > 0L) {
              mail = mailbox.remove(timeRemaining);
            } else {
              mail = null;
            }

             // If it is the suspension Flag, lower it and suspend
            if (mail == suspensionFlag) {
              suspended = true;
              suspensionFlag.lower();
              if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                SysLog.println("KeypadRecognizer.RecognitionThread.run: suspending");

             // Else if it is a TokenEvent, parse it as a keypress id
            } else if (mail instanceof TokenEvent) {

              String token = ((TokenEvent)mail).token;
              if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                SysLog.println("KeypadRecognizer.RecognitionThread.run: received token \"" + token + "\"");

               // If this is the first keypress, start processing, create a Result and notify any ResultListeners
              if (result == null) {
                startProcessing();
                result = new ParsedResult();
                resultCreated(result);
              }

              RuleParse parse = null;
              RuleParser ruleParser;
              int i = parsers.size();
              while (i-- > 0) {
                ruleParser = (RuleParser)parsers.elementAt(i);
                try {
                  if (ruleParser.parseToken(token)) {            // Throws GrammarException
                     // The ith RuleParser can absorb the token
                    parse = ruleParser.parseResult();
                    if (parse != null) {
                       // The ith RuleParser has produced a complete parse; break with it
                      result.setParse(parse);
                      result.setGrammar(ruleParser.grammar());
                      break;
                    }
                  } else {
                   // The ith RuleParser can't absorb the token, so remove the RuleParser
                  parsers.removeElementAt(i);
                  }
                } catch (GrammarException e) {
                   // The ith RuleParser throws a GrammarException parsing the token, so remove the RuleParser
                  parsers.removeElementAt(i);
                  if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                    SysLog.println("KeypadRecognizer.RecognitionThread.run: " + e.toString());
                }
              }

              if (!parsers.isEmpty()) {
                 // At least one RuleParser remains active, so add the token as unfinalized to the Result and notify the ResultListeners
                result.addFinalizedToken(token);
                if (parse == null) {
                  result.resultUpdated(KeypadRecognizer.this, null /*Grammar unfinalized*/, true /*New token finalized*/, false /*No unfinalized tokens added*/);
                } else if (!suspensionFlag.raised()) {
                   // The parse has succeeded, so finalize the grammar and accept the result
                  result.grammarFinalized(KeypadRecognizer.this, (BaseGrammar)result.getGrammar(), true, false);
                  result.setResultState(Result.ACCEPTED);
                  if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                    SysLog.println("KeypadRecognizer.RecognitionThread.run: ACCEPTED result \"" + result.toString() + "\"" +
                                                                                      " via rule \"" + result.getRuleName(0) + "\"" +
                                                                                      " in grammar \"" + result.getRuleGrammar(0).getName() + "\"");
                  KeypadRecognizer.this.suspend();
                  notifier.insert(result);
                }
              } else if (!suspensionFlag.raised()) {
                 // The parse has failed, so reject the result
                result.setResultState(Result.REJECTED);
                if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                  SysLog.println("KeypadRecognizer.RecognitionThread.run: REJECTED result");
                KeypadRecognizer.this.suspend();
                notifier.insert(result);
              }

              // If recognition has timed out, reject the result
            } else if (mail == null && !suspensionFlag.raised()) {
              result.setResultState(Result.REJECTED);
              if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                SysLog.println("KeypadRecognizer.RecognitionThread.run: REJECTED result due to timeout");
              KeypadRecognizer.this.suspend();
              notifier.insert(result);

             // If it is the resumption Flag, lower it and ignore it
            } else if (mail == resumptionFlag) {
              resumptionFlag.lower();
              if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                SysLog.println("KeypadRecognizer.RecognitionThread.run: ignoring resumption flag received while LISTENING");

              // Else if it is a timeout, set the timeout
            } else if (mail instanceof Integer) {
              timeout = ((Integer)mail).intValue();
              if (timeout != 0) end = System.currentTimeMillis() + timeout;
              if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                SysLog.println("KeypadRecognizer.RecognitionThread.run: new timeout (" + timeout + " ms) received while SUSPENDED");

             // Else if it is the ABORT_SIGNAL, abort
            } else if (mail instanceof String && ((String)mail).equals(ABORT_SIGNAL)) {
              if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
                SysLog.println("KeypadRecognizer.RecognitionThread.run: ABORT signal received while LISTENING");
              break;

             // Otherwise, something evil has come our way
            } else {
              throw new RuntimeException("KeypadRecognizer.RecognitionThread.run: UNEXPECTED " + mail.getClass().getName() + " in mailbox while LISTENING");
            }
          }
        }
      } catch (InterruptedException e) {
         // Fall through to terminate run()
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK))
          SysLog.println("KeypadRecognizer.RecognitionThread.run: terminating due to InterruptedException");
      }
    }

    /// EngineListener Implementation ///

    public void engineAllocated(EngineEvent event) {
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK)) SysLog.println("KeypadRecognizer.engineAllocated");
    }
    public void engineAllocatingResources(EngineEvent event) {}
    public void engineDeallocated(EngineEvent event) {}
    public void engineDeallocatingResources(EngineEvent event) {
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK)) SysLog.println("KeypadRecognizer.engineDeallocatingResources");
    }
    public void engineError(EngineErrorEvent event) {}
    public void enginePaused(EngineEvent event) {}
    public void engineResumed(EngineEvent event) {}

    /// RecognizerListener Implementation ///

     // Change the Recognizer from the SUSPENDED state to the LISTENING state and resume recognition
    public void changesCommitted(RecognizerEvent event) {
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK)) SysLog.println("KeypadRecognizer.changesCommitted: requesting resumption");
       // Have to clone this here instead of at the end of commitChangesInternal() in order to get the activation status inserted by notifyGrammarActivation()
       // This is EXTREMELY inefficient; we are cloning the entire recognizer just to get activation changes, even if none of the grammars has changed.
       // Since activation status has to do with the underlying recognizer (this.recognizer), not the JSAPI wrapper (KeypadRecognizer.this), we may only
       // need to override BaseRecognizer.notifyGrammarActivation() with something that gets its information (focus and enablement) from KeypadRecognizer.this
       // but sets grammar activation status in this.recognizer. This would involve some combination of BaseRecognizer.changeEnabled() (called in
       // commitChangesInternal()) and notifyGrammarActivation(). DO SOMETHING ABOUT THIS !!!
      recognizer = KeypadRecognizer.this.copy();
      resumptionFlag.raise();
    }

     // Change the Recognizer from the FOCUS_OFF state to the FOCUS_ON state
    public void focusGained(RecognizerEvent event) {
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK)) SysLog.println("KeypadRecognizer.focusGained: requesting suspension");
      suspensionFlag.raise();
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK)) SysLog.println("KeypadRecognizer.focusGained: requesting resumption");
      resumptionFlag.raise();
    }

     // Change the Recognizer from the FOCUS_ON state to the FOCUS_OFF state
    public void focusLost(RecognizerEvent event) {
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK)) SysLog.println("KeypadRecognizer.focusLost: requesting suspension");
      suspensionFlag.raise();
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK)) SysLog.println("KeypadRecognizer.focusLost: requesting resumption");
      resumptionFlag.raise();
    }

     // Change the Recognizer from the LISTENING state to the PROCESSING state
    public void recognizerProcessing(RecognizerEvent event) {}

     // Change the Recognizer from either the LISTENING state or the PROCESSING state to the SUSPENDED state
    public void recognizerSuspended(RecognizerEvent event) {
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK)) SysLog.println("KeypadRecognizer.recognizerSuspended: requesting suspension");
      suspensionFlag.raise();
    }

    /// End of RecognizerListener Implementation ///
  }
} // class KeypadRecognizer
