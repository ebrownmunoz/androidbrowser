package voxware.engine.recognition.mock;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import voxware.engine.recognition.sapivise.RawResult;

/**
 * A Mock Speaker for use with the text browser. This will take input from 
 * a file and use them as the responses for the Text Recognizer.
 * 
 * @author eric
 *
 */
public class FileMockSpeaker extends MockSpeaker implements Runnable{

	private static final long DELAY_MS =800;
	private final String fileName;
	private BufferedReader fileReader; // Guarded by this
	private String nextLine;
	
	public FileMockSpeaker(NotifiableByMocks recognizer, String fileName) {
		super(recognizer);
		this.fileName = fileName;
	}
	
	private String readNextRunnableLine(BufferedReader reader) throws IOException {
		String line = null;
		while (true) {
			line = reader.readLine();

			if (line == null) { // end of file
				break;
			}
			
			if (line.contains("//")) { // strip off comments
				line = line.substring(0, line.indexOf("//"));
			}
			
			line = line.trim(); // get rid of whitespace
			
			if ((line.length() > 0) && (!line.startsWith("-"))) {
				break; // this returns the line.
			}
			
			// or else try next line.
		};
		
		return line;
		
		
	}

	public String getNextResponse() {
		synchronized (this) {
			try {
				wait(DELAY_MS);
				if (this.fileReader == null) {
					if (fileName != null) {
						this.fileReader = new BufferedReader(new FileReader(fileName));
					} else {
						this.fileReader = new BufferedReader(new java.io.InputStreamReader(System.in));
					}
					nextLine = readNextRunnableLine(fileReader);
				}
				
				String line = nextLine;
				nextLine = readNextRunnableLine(fileReader);;
				if (nextLine == null) {
					setDone(true);
				}
				
				return line;

			} catch (InterruptedException e) {
				return null;
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		
	}

}
