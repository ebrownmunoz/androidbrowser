package voxware.engine.recognition.mock;

import voxware.engine.recognition.sapivise.RawResult;

/**
 * This allows us to send a mock result.
 * @author eric
 *
 */
public interface NotifiableByMocks {
	public void notifyResult(RawResult result);
}
