package voxware.engine.recognition.mock;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import voxware.engine.recognition.sapivise.RawResult;

/**
 * A Mock Speaker for use with the text browser. This will take input from 
 * a file and use them as the responses for the Text Recognizer.
 * 
 * @author eric
 *
 */
public class InteractiveMockSpeaker extends MockSpeaker implements Runnable{

	private static final long DELAY_MS =800;
	private final String userId;
	private final String applicationId;
	private final String languageId;
	private BufferedReader instreamReader; // Guarded by this
	private final AtomicInteger linesGiven = new AtomicInteger(0);
	
	
	public InteractiveMockSpeaker(NotifiableByMocks recognizer, String userId, String applicationId, String languageId) {
		super(recognizer);
		this.userId = userId;
		this.applicationId = applicationId;
		this.languageId = languageId;
	}
	
	private String readNextRunnableLine(BufferedReader reader) throws IOException {
		String line = null;
		
		int lineNumber = linesGiven.getAndIncrement();
		
		if (lineNumber == 0) {
			return "@noinput";
		} else if (lineNumber == 1) {
			return "userId=" + userId + "&applicationId=" + applicationId + "&action=launch&languageId="+languageId;
		}
		
		
		while (true) {
			line = reader.readLine();

			if (line == null) { // end of file
				break;
			}
			
			if (line.contains("//")) { // strip off comments
				line = line.substring(0, line.indexOf("//"));
			}
			
			line = line.trim(); // get rid of whitespace
			
			if ((line.length() > 0) && (!line.startsWith("-"))) {
				break; // this returns the line.
			}
			
			// or else try next line.
		};
		
		return line;
		
		
	}

	public String getNextResponse() {
		synchronized (this) {
			try {
				wait(DELAY_MS);
				if (this.instreamReader == null) {
					
					this.instreamReader = new BufferedReader(new java.io.InputStreamReader(System.in));
				}
				
				String line = readNextRunnableLine(instreamReader);;
				if (line == null) {
					setDone(true);
				}
				
				return line;

			} catch (InterruptedException e) {
				return null;
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		
	}

}
