package voxware.engine.recognition.mock;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import voxware.engine.recognition.sapivise.RawResult;

public abstract class MockSpeaker implements Runnable {

	private final ExecutorService threadService; // runs this.

	// we call the notifyResult method asynchronously to send result
	private final NotifiableByMocks recognizer; 
	private volatile boolean isDone = false;
	private final Queue<String> grammarQueue = new ConcurrentLinkedQueue<String>();

	public MockSpeaker(NotifiableByMocks recognizer) {
		threadService = Executors.newSingleThreadExecutor();

		this.recognizer = recognizer;
	}

	public void requestStartRecognition(String grammarName) {
		grammarQueue.offer(grammarName);
		
		if (!isDone()) {
			threadService.execute(this);
		}
	}
	
	public synchronized void setDone(boolean done) {
		this.isDone = done;
	}
	
	public synchronized boolean isDone () {
		return isDone;
	}

	/**
	 * After a delay, notify of result.
	 */
	public void run() {


		String response = getNextResponse(); // may include a delay

		if (response != null) {
			String[] strarray = new String[1];
			strarray[0] = response;
			

			RawResult result = new RawResult();
			if (response.trim().startsWith("@noinput")) {
				result.accepted = false;
			} else {
				result.accepted = true;
			}


			result.nbestArray = strarray;

			result.grammarName = grammarQueue.poll();

			recognizer.notifyResult(result );
			System.out.println("MockSpeaker: offering " + response);
		} else {
			System.err.println("MockSpeaker: null from getNextResponse()");
		}
	}


	public abstract String getNextResponse();

}