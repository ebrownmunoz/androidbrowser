package voxware.engine.recognition;

import java.util.*;
import java.io.*;

import javax.speech.*;
import javax.speech.recognition.*;

import voxware.engine.recognition.*;
import voxware.util.SysLog;

public class JSGFTest {

  // Constants

  public static final String  DEFAULT_RULE_DELIMITER = "::";

  // Data

  private static String  RuleNameDelimiter = DEFAULT_RULE_DELIMITER;
  private static String  DefaultRuleName = null;
  private static boolean Incremental = false;

  // Main

   /** Main program entry point. */
  public static void main(String argv[]) {
    SysLog.initialize(System.out);
    SysLog.clearShowStamp();

     // Is there anything to do?
    if (argv.length == 0) {
      printUsage();
      System.exit(1);
    }

     // Variables
    String   grammarFileName = null;
    String   outputFileName  = null;
    String   testFileName    = null;
    boolean  verify          = false;                                 // Verify rules against embedded examples

     // Get the parameters from the command line
    for (int i = 0; i < argv.length; i++) {

      String arg = argv[i];

       // Options
      if (arg.startsWith("-")) {

        if (arg.equals("-d")) {
          if (i == argv.length - 1) {
            SysLog.println("JSGFTest.main: ERROR - missing rule name delimiter");
            System.exit(1);
          }
          RuleNameDelimiter = argv[++i];
          continue;
        }

        if (arg.equals("-h") || arg.equals("-?")) {
          printUsage();
          System.exit(1);
        }

        if (arg.equals("-i")) {
          Incremental = true;
          continue;
        }

        if (arg.equals("-l")) {
          int printLevel = Integer.parseInt(argv[++i]);
          SysLog.setPrintLevel(printLevel);
          continue;
        }

        if (arg.equals("-m")) {
          long printMask = Long.parseLong(argv[++i], 16);
          SysLog.setPrintMask(printMask);
          continue;
        }

        if (arg.equals("-o")) {
          if (i == argv.length - 1) {
            SysLog.println("JSGFTest.main: ERROR - missing output file name");
            System.exit(1);
          }
          outputFileName = argv[++i];
          continue;
        }

        if (arg.equals("-r")) {
          if (i == argv.length - 1) {
            SysLog.println("JSGFTest.main: ERROR - missing default rule name");
            System.exit(1);
          }
          DefaultRuleName = argv[++i];
          continue;
        }

        if (arg.equals("-t")) {
          if (i == argv.length - 1) {
            SysLog.println("JSGFTest.main: ERROR - missing test file name");
            System.exit(1);
          }
          testFileName = argv[++i];
          continue;
        }

        if (arg.equals("-v")) {
          verify = true;
          continue;
        }

      }
      grammarFileName = arg;
    }

    if (grammarFileName == null) {
      printUsage();
      System.exit(1);
    }

    SysLog.println("JSGFTest.main: Starting with grammar file \"" + grammarFileName + "\"");
    SysLog.println("JSGFTest.main: System architecture is " + System.getProperty("os.arch"));
    SysLog.println("JSGFTest.main: Operating system is " + System.getProperty("os.name") + " " + System.getProperty("os.version"));

    try {
      BaseRecognizer recognizer = new BaseRecognizer();
      if (recognizer == null) throw new RuntimeException("JSGFTest.main: recognizer is null");
      recognizer.allocate();                                          // Throws EngineException, EngineStateError
      if (SysLog.printLevel > 0) SysLog.println("JSGFTest.main: Starting JSGF Grammar compilation ...");
      long time = System.currentTimeMillis();
      RuleGrammar grammar = recognizer.loadJSGF(new FileInputStream(grammarFileName));  // Throws GrammarException, IOException, EngineStateError
      if (grammar == null) throw new RuntimeException("JSGFTest.main: grammar is null");
      grammar.setEnabled(true);
      recognizer.commitChanges();
      time = System.currentTimeMillis() - time;
      if (SysLog.printLevel > 0) SysLog.println("JSGFTest.main: JSGF Grammar compilation took " + time + " msec");
      // if (SysLog.printLevel > 0) SysLog.print(grammar.toString());
      if (outputFileName != null) {
        if (SysLog.printLevel > 0) SysLog.println("JSGFTest.main: writing RuleGrammar to file \"" + outputFileName + "\"");
        time = System.currentTimeMillis();
        recognizer.suspend();
        FileOutputStream out = new FileOutputStream(outputFileName);
        recognizer.writeVendorGrammar(out, grammar);
        out.flush();
        out.close();
        time = System.currentTimeMillis() - time;
        if (SysLog.printLevel > 0) SysLog.println("JSGFTest.main: RuleGrammar serialization and output took " + time + " msec");
        time = System.currentTimeMillis();
        recognizer.deleteRuleGrammar(grammar);
        FileInputStream in = new FileInputStream(outputFileName);
        grammar = (RuleGrammar)recognizer.readVendorGrammar(in);      // Throws ClassNotFoundException
        in.close();
        if (grammar == null) throw new RuntimeException("JSGFTest.main: grammar copy is null");
        grammar.setEnabled(true);
        recognizer.commitChanges();
        time = System.currentTimeMillis() - time;
        if (SysLog.printLevel > 0) SysLog.println("JSGFTest.main: RuleGrammar input and deserialization took " + time + " msec");
      }
      if (grammar == null) throw new RuntimeException("JSGFTest.main: grammar is null");
      if (verify) verify(grammar);
      if (testFileName != null) test(grammar, testFileName);
    } catch (GrammarException e) {
      SysLog.println("JSGFTest.main: caught " + e.toString());
      GrammarSyntaxDetail[] details = e.getDetails();
      if (details != null) {
        for (int i = 0; i < details.length; i++) {
          GrammarSyntaxDetail detail = details[i];
          SysLog.println("JSGFTest.main: GrammarException at line " + detail.lineNumber + "." + detail.charNumber + ": " + detail.message);
        }
      }
      e.printStackTrace(SysLog.stream);
    } catch (Exception exc) {
      SysLog.println("JSGFTest.main: caught " + exc.toString());
      exc.printStackTrace(System.err);
    }
  }

   // Print the usage
  private static void printUsage() {
    SysLog.println("");
    SysLog.println("usage: java voxware.engine.recognition.JSGFTest (options) grammarFileName");
    SysLog.println("");
    SysLog.println("options:");
    SysLog.println("  -d string Rule name delimiter (default is \"" + DEFAULT_RULE_DELIMITER + "\")");
    SysLog.println("  -h        Display this help screen");
    SysLog.println("  -i        Use incremental parsing");
    SysLog.println("  -l int    Log level (default is 0)");
    SysLog.println("  -m long   Hexadecimal bit mask to enable selective logging (default is 0)");
    SysLog.println("  -o file   Output file name (default is none)");
    SysLog.println("  -r name   Default rule name (default is none)");
    SysLog.println("  -t file   File of test sequences to parse (default is none)");
    SysLog.println("  -v        Verify rules against embedded examples (default is not to verify)");
    SysLog.println("");
  }

   // Verify the rules against embedded examples
  private static void verify(RuleGrammar grammar) {
    int  trials = 0;
    int  successes = 0;
    SysLog.println("JSGFTest.verify: starting");
    SysLog.println("JSGFTest.verify: not supported");
    SysLog.println("JSGFTest.verify: " + trials + " trials; " + successes + " successes; " + (trials - successes) + " failures");
    SysLog.println("JSGFTest.verify: complete");
  }

   // Parse the phrases in the test file
  private static void test(RuleGrammar grammar, String fileName) {
    int  trials = 0;
    int  successes = 0;
    long start = 0;
    try {
      FileReader testFile = new FileReader(fileName);
      BufferedReader testSequences = new BufferedReader(testFile);
      String sequence;
      SysLog.println("JSGFTest.test: starting");
      start = System.currentTimeMillis();
      while (true) {
        sequence = testSequences.readLine();
        if (sequence == null) break;
        int endOfRuleName = sequence.indexOf(RuleNameDelimiter);
        String ruleName = DefaultRuleName;
        if (endOfRuleName > 0) {
          ruleName = sequence.substring(0, endOfRuleName).trim();
          sequence = sequence.substring(endOfRuleName + RuleNameDelimiter.length());
        }
        if (parse(grammar, ruleName, sequence.trim(), "  ")) successes++;
        trials++;
      }
    } catch (IOException e) {
      SysLog.println("JSGFTest.test: exception reading line from test file -- " + e.toString());
    }
    if (trials > 0) SysLog.println("JSGFTest.test: mean time per trial = " + ((double)(System.currentTimeMillis() - start) / (double)trials) + " msec");
    SysLog.println("JSGFTest.test: " + trials + " trials; " + successes + " successes; " + (trials - successes) + " failures");
  }

   // Parse the given phrase using the given GrammarRule and return success
  private static boolean parse(RuleGrammar grammar, String ruleName, String phrase, String indentation) {
    RuleParse result = null;
    try {
      boolean incrementalSuccess = false;
      if (Incremental) {
        StringBuffer phraseBuffer = new StringBuffer();
        RuleParser parser = new RuleParser(grammar.getRecognizer(), grammar, ruleName);
        String[] tokens = RuleParser.tokenize(phrase);
        parser.startParse();
        incrementalSuccess = true;
        for (int i = 0; i < tokens.length; i++) {
          if (incrementalSuccess) {
            incrementalSuccess = parser.parseToken(tokens[i]);
            if (!incrementalSuccess) phraseBuffer.append(i == 0 ? "<> " : " <>");
          } else if (parser.parseToken(tokens[i])) {
            throw new GrammarException();
          }
          if (i > 0) phraseBuffer.append(' ');
          phraseBuffer.append(tokens[i]);
        }
        if (incrementalSuccess && parser.parseIsFinished())
          result = parser.parseResult();
        else
          phrase = phraseBuffer.toString();
      } else {
        result = grammar.parse(phrase, ruleName);                     // Throws GrammarException
      }
      if (result != null) {
        SysLog.println(indentation + "Success: \"" + phrase + "\"");
        // SysLog.println(indentation + "  Parse: " + result.toString());
      } else if (incrementalSuccess) {
        SysLog.println(indentation + "PARTIAL: \"" + phrase + "\"");
      } else {
        SysLog.println(indentation + "FAILURE: \"" + phrase + "\"");
      }
    } catch (GrammarException e) {
      SysLog.println(indentation + "GRAMMAR EXCEPTION: \"" + phrase + "\"");
    }
    return (result != null);
  }
}
