#ifndef __voxware_engine_recognition_BaseRuleGrammar__
#define __voxware_engine_recognition_BaseRuleGrammar__

#pragma interface

#include <voxware/engine/recognition/BaseGrammar.h>

extern "Java"
{
  namespace javax
  {
    namespace speech
    {
      namespace recognition
      {
        class FinalRuleResult;
        class RuleParse;
        class RuleName;
        class Rule;
      }
    }
  }
  namespace voxware
  {
    namespace engine
    {
      namespace recognition
      {
        class BaseRuleGrammar;
        class BaseRuleGrammar$GRule;
        class BaseRecognizer;
      }
    }
    namespace util
    {
      class ObjectSet;
    }
  }
}

class voxware::engine::recognition::BaseRuleGrammar : public ::voxware::engine::recognition::BaseGrammar
{
public:
  BaseRuleGrammar (::voxware::engine::recognition::BaseRecognizer *, ::java::lang::String *);
  virtual void setEnabled (jboolean);
  virtual ::javax::speech::recognition::Rule *ruleForJSGF (::java::lang::String *);
  virtual void setRule (::java::lang::String *, ::javax::speech::recognition::Rule *, jboolean);
  virtual ::javax::speech::recognition::Rule *getRule (::java::lang::String *);
  virtual ::javax::speech::recognition::Rule *getRuleInternal (::java::lang::String *);
  virtual jboolean isRulePublic (::java::lang::String *);
  virtual JArray< ::java::lang::String *> *listRuleNames ();
  virtual void deleteRule (::java::lang::String *);
  virtual void setEnabled (::java::lang::String *, jboolean);
  virtual void setEnabled (JArray< ::java::lang::String *> *, jboolean);
  virtual jboolean isEnabled (::java::lang::String *);
  virtual jboolean isEnabled ();
  virtual ::javax::speech::recognition::RuleName *resolve (::javax::speech::recognition::RuleName *);
  virtual void addImport (::javax::speech::recognition::RuleName *);
  virtual void removeImport (::javax::speech::recognition::RuleName *);
  virtual JArray< ::javax::speech::recognition::RuleName *> *listImports ();
  virtual ::javax::speech::recognition::RuleParse *parse (::java::lang::String *, ::java::lang::String *);
  virtual ::javax::speech::recognition::RuleParse *parse (JArray< ::java::lang::String *> *, ::java::lang::String *);
  virtual ::javax::speech::recognition::RuleParse *parse (::javax::speech::recognition::FinalRuleResult *, jint, ::java::lang::String *);
  virtual ::java::lang::String *toString ();
  virtual JArray< ::voxware::engine::recognition::BaseRuleGrammar$GRule *> *listRules ();
  virtual jboolean hasChanged () { return grammarChanged; }
  virtual void hasChanged (jboolean);
public:  // actually protected
  virtual void setRuleChanged (::java::lang::String *, jboolean);
  virtual void resolveAllRules ();
  virtual void resolveRule (::javax::speech::recognition::Rule *);
  virtual jboolean isRuleChanged (::java::lang::String *);
public: // actually package-private
  virtual void addRuleDocComment (::java::lang::String *, ::java::lang::String *);
public:
  virtual ::java::lang::String *getRuleDocComment (::java::lang::String *);
public: // actually package-private
  virtual void addImportDocComment (::javax::speech::recognition::RuleName *, ::java::lang::String *);
public:
  virtual ::java::lang::String *getImportDocComment (::javax::speech::recognition::RuleName *);
public: // actually package-private
  virtual void addGrammarDocComment (::java::lang::String *);
public:
  virtual ::java::lang::String *getGrammarDocComment () { return grammarDocComment; }
public: // actually package-private
  virtual void setSourceLine (::java::lang::String *, jint);
public:
  virtual jint getSourceLine (::java::lang::String *);
public: // actually package-private
  virtual void addSampleSentence (::java::lang::String *, ::java::lang::String *);
  virtual void setSampleSentences (::java::lang::String *, ::java::util::Vector *);
public:
  virtual ::java::util::Vector *getSampleSentences (::java::lang::String *);
  virtual jint getRuleID (::java::lang::String *);
public:  // actually protected
  virtual ::java::lang::String *stripRuleName (::java::lang::String *);
  virtual ::java::lang::Object *clone ();
  ::java::util::Hashtable * __attribute__((aligned(__alignof__( ::voxware::engine::recognition::BaseGrammar )))) rules;
  ::java::util::Vector *imports;
  ::voxware::util::ObjectSet *deletedRules;
  ::voxware::util::ObjectSet *changedRules;
  ::voxware::util::ObjectSet *removedImports;
  ::voxware::util::ObjectSet *addedImports;
  jboolean grammarChanged;
public: // actually package-private
  ::java::util::Properties *ruleDocComments;
  ::java::util::Properties *importDocComments;
  ::java::lang::String *grammarDocComment;

  friend class voxware_engine_recognition_BaseRuleGrammar$GRule;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_recognition_BaseRuleGrammar__ */
