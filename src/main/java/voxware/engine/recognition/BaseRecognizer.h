#ifndef __voxware_engine_recognition_BaseRecognizer__
#define __voxware_engine_recognition_BaseRecognizer__

#pragma interface

#include <voxware/engine/BaseEngine.h>

extern "Java"
{
  namespace java
  {
    namespace net
    {
      class URL;
    }
  }
  namespace javax
  {
    namespace speech
    {
      class AudioManager;
      namespace recognition
      {
        class Recognizer;
        class Rule;
        class GrammarException;
        class Result;
        class Grammar;
        class SpeakerManager;
        class RecognizerProperties;
        class ResultListener;
        class DictationGrammar;
        class RuleGrammar;
        class RecognizerModeDesc;
      }
    }
  }
  namespace voxware
  {
    namespace engine
    {
      namespace recognition
      {
        class BaseRecognizer;
      }
    }
    namespace util
    {
      class ObjectSet;
    }
  }
}

class voxware::engine::recognition::BaseRecognizer : public ::voxware::engine::BaseEngine
{
public:
  BaseRecognizer ();
  BaseRecognizer (::javax::speech::recognition::RecognizerModeDesc *, jboolean);
  virtual ::javax::speech::AudioManager *getAudioManager ();
  virtual ::javax::speech::recognition::RuleGrammar *newRuleGrammar (::java::lang::String *);
  virtual ::javax::speech::recognition::RuleGrammar *loadJSGF (::java::io::Reader *);
  virtual ::javax::speech::recognition::RuleGrammar *loadJSGF (::java::net::URL *, ::java::lang::String *);
  virtual ::javax::speech::recognition::RuleGrammar *loadJSGF (::java::net::URL *, ::java::lang::String *, jboolean, jboolean, ::java::util::Vector *);
  virtual ::javax::speech::recognition::RuleGrammar *getRuleGrammar (::java::lang::String *);
  virtual JArray< ::javax::speech::recognition::RuleGrammar *> *listRuleGrammars ();
  virtual void deleteRuleGrammar (::javax::speech::recognition::RuleGrammar *);
  virtual ::javax::speech::recognition::DictationGrammar *getDictationGrammar (::java::lang::String *);
  virtual void commitChanges ();
  virtual void suspend ();
  virtual void forceFinalize (jboolean);
  virtual void requestFocus ();
  virtual void releaseFocus ();
  virtual void addResultListener (::javax::speech::recognition::ResultListener *);
  virtual void removeResultListener (::javax::speech::recognition::ResultListener *);
  virtual ::javax::speech::recognition::RecognizerProperties *getRecognizerProperties ();
  virtual ::javax::speech::recognition::SpeakerManager *getSpeakerManager () { return 0; }
  virtual ::javax::speech::recognition::Grammar *readVendorGrammar (::java::io::InputStream *);
  virtual void writeVendorGrammar (::java::io::OutputStream *, ::javax::speech::recognition::Grammar *);
  virtual ::javax::speech::recognition::Result *readVendorResult (::java::io::InputStream *);
  virtual void writeVendorResult (::java::io::OutputStream *, ::javax::speech::recognition::Result *);
public:  // actually protected
  virtual void storeGrammar (::javax::speech::recognition::RuleGrammar *);
  virtual ::javax::speech::recognition::RuleGrammar *retrieveGrammar (::java::lang::String *);
public:
  virtual ::javax::speech::recognition::RuleGrammar *loadJSGF (::java::io::InputStream *);
  virtual void rejectUtterance ();
  virtual void notifyResult (::java::lang::String *, ::java::lang::String *);
public:  // actually protected
  virtual void changesCommitted (jlong, jlong, ::javax::speech::recognition::GrammarException *);
  virtual void focusGained (jlong, jlong);
  virtual void focusLost (jlong, jlong);
  virtual void recognizerProcessing (jlong, jlong);
  virtual void recognizerSuspended (jlong, jlong);
  virtual void resultCreated (::javax::speech::recognition::Result *);
  virtual jboolean grammarsHaveChanged ();
  virtual void commitChangesInternal (jboolean);
  virtual void startGrammarChanges () { }
  virtual void endGrammarChanges () { }
  virtual void changeEnabled (::java::util::Vector *) { }
  virtual void changeRule (::java::lang::String *, ::java::lang::String *, ::javax::speech::recognition::Rule *, jboolean) { }
public:
  static void loadAllImports (::javax::speech::recognition::Recognizer *);
private:
  static void loadImports (::javax::speech::recognition::Recognizer *, ::javax::speech::recognition::RuleGrammar *);
public:  // actually protected
  virtual void linkGrammars ();
  virtual jboolean hasEnabledModalGrammars ();
  virtual jboolean notifyGrammarActivation (jboolean);
  virtual void deallocateEngine ();
  virtual void suspendInternal () { }
  virtual ::java::lang::Object *clone ();
  ::voxware::util::ObjectSet * __attribute__((aligned(__alignof__( ::voxware::engine::BaseEngine )))) resultListeners;
  ::java::util::Hashtable *grammarList;
  ::voxware::util::ObjectSet *deletedGrammars;
  jboolean caseSensitiveGrammarNames;
  jboolean reloadAll;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_recognition_BaseRecognizer__ */
