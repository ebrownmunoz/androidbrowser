/*
 * Copyright (c) 1997-8 Sun Microsystems, Inc. All Rights Reserved.
 *
 */
package voxware.engine.recognition;

import java.util.*;
import javax.speech.*;
import javax.speech.recognition.*;
import java.io.Serializable;

import voxware.util.ObjectSet;

/**
 * Implementation of javax.speech.recognition.Grammar.
 *
 * @version 1.8 12/16/98 16:43:43
 */
public class BaseGrammar implements Grammar, Serializable, Cloneable {

  public    transient BaseRecognizer recognizer;
  protected transient ObjectSet      grammarListeners;
  protected transient ObjectSet      resultListeners;
  protected transient boolean        active;      // Activation state, maintained by the recognizer

  protected           String         myName;
  protected           boolean        grammarEnabled;
  protected           int            activationMode;    

  /**
   * Create a new BaseGrammar
   * @param recognizer the BaseRecognizer for this Grammar.
   * @param name the name of this Grammar.
   */
  public BaseGrammar(BaseRecognizer recognizer, String name) {
      this.recognizer = recognizer;
      grammarListeners = new ObjectSet();
      resultListeners = new ObjectSet();
      active = false;
      myName = name;
      grammarEnabled = true;
      activationMode = RECOGNIZER_FOCUS;
  }

//////////////////////
// Begin Grammar Methods
//////////////////////

    /**
     * Return a reference to the recognizer that owns this Grammar.
     * From javax.speech.recognition.Grammar.
     */
    public Recognizer getRecognizer() {
        return this.recognizer;
    }
    
    /**
     * Get the name of the Grammar.
     * From javax.speech.recognition.Grammar.
     */
    public String getName() {
        return myName;
    }
  
    /**
     * Set the enabled property of the Grammar.
     * From javax.speech.recognition.Grammar.
     * @param enabled the new desired state of the enabled property.
     */
    public void setEnabled(boolean enabled) {
        if (grammarEnabled != enabled) grammarEnabled = enabled;
    }
    
    /**
     * Determine if this Grammar is enabled or not.
     * From javax.speech.recognition.Grammar.
     */
    public boolean isEnabled() {
        return grammarEnabled;
    }

    /**
     * Set the activation mode of this Grammar.
     * From javax.speech.recognition.Grammar
     * 
     * @param mode the new activation mode.
     */
    public void setActivationMode(int mode) throws IllegalArgumentException {
        if (mode != GLOBAL && mode != RECOGNIZER_MODAL && mode != RECOGNIZER_FOCUS) {
            throw new IllegalArgumentException("BaseGrammar.setActivationMode: invalid activation mode (" + mode + ")");
        } else if (activationMode != mode) {
            activationMode = mode;
        }
    }
    
    /**
     * Get the activation mode of this Grammar.
     * From javax.speech.recognition.Grammar
     */
    public int getActivationMode() {
        return activationMode;
    }
    
    /**
     * Determine if the Grammar is active or not.  This is a combination
     * of the enabled state and activation conditions of the Grammar.
     * From javax.speech.recognition.Grammar.
     */
    public boolean isActive() {
        return active;
    }
    
    /**
     * Add a new GrammarListener to the listener list if it is not 
     * already in the list.
     * From javax.speech.recognition.Grammar.
     * @param listener the listener to add.
     */
    public void addGrammarListener(GrammarListener listener) {
      grammarListeners.add(listener);
    }

    /**
     * Remove a GrammarListener from the listener list.
     * From javax.speech.recognition.Grammar.
     * @param listener the listener to remove.
     */
    public void removeGrammarListener(GrammarListener listener) {
      grammarListeners.remove(listener);
    }

    /**
     * Add a new ResultListener to the listener list if it is not 
     * already in the list.
     * From javax.speech.recognition.Grammar.
     * @param listener the listener to add.
     */
    public void addResultListener(ResultListener listener) {
      resultListeners.add(listener);
    }

    /**
     * Remove a ResultListener from the listener list.
     * From javax.speech.recognition.Grammar.
     * @param listener the listener to remove.
     */
    public void removeResultListener(ResultListener listener) {
      resultListeners.remove(listener);
    }

//////////////////////
// End Grammar Methods
//////////////////////

//////////////////////
// Begin utility methods for sending GrammarEvents
//////////////////////

    /**
     * Utility function to generate GRAMMAR_ACTIVATED event and send it to all grammar listeners.
     */
    protected void grammarActivated() {
        if (grammarListeners == null) return;
        GrammarEvent event = new GrammarEvent(this, GrammarEvent.GRAMMAR_ACTIVATED);
        Enumeration listeners = grammarListeners.iterator();
        while (listeners.hasMoreElements()) {
            GrammarListener listener = (GrammarListener)listeners.nextElement();
            listener.grammarActivated(event);
        }
    }

    /**
     * Utility function to generate GRAMMAR_DEACTIVATED event and send it to all grammar listeners.
     */
    protected void grammarDeactivated() {
        if (grammarListeners == null) return;
        GrammarEvent event = new GrammarEvent(this, GrammarEvent.GRAMMAR_DEACTIVATED);
        Enumeration listeners = grammarListeners.iterator();
        while (listeners.hasMoreElements()) {
            GrammarListener listener = (GrammarListener)listeners.nextElement();
            listener.grammarDeactivated(event);
        }
    }

    /**
     * Utility function to generate GRAMMAR_CHANGES_COMMITTED event and send it to all grammar listeners.
     */
    protected void grammarChangesCommitted() {
        if (grammarListeners == null) return;
        GrammarEvent event = new GrammarEvent(this, GrammarEvent.GRAMMAR_CHANGES_COMMITTED);
        Enumeration listeners = grammarListeners.iterator();
        while (listeners.hasMoreElements()) {
            GrammarListener listener = (GrammarListener)listeners.nextElement();
            listener.grammarChangesCommitted(event);
        }
    }

//////////////////////
// End utility methods for sending GrammarEvents
//////////////////////

//////////////////////
// NON-JSAPI METHODS
//////////////////////

    /**
     * Activate/Deactivate the grammar, returning true iff this changes the grammar's activation
     */
    public boolean activate(boolean active) {
      if (this.active != active) {
        this.active = active;
        if (active) grammarActivated();
        else        grammarDeactivated();
        return true;
      } else {
        return false;
      }
    }

    /**
     * Set the name of this Grammar.
     */
    public void setName(String name) {
        myName = name;
    }

    /**
     * Set the Recognizer that owns this Grammar.
     */
    public void setRecognizer(BaseRecognizer recognizer) {
        this.recognizer = recognizer;
    }
}
