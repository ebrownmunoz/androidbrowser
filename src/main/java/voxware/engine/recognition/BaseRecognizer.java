/*
 * Copyright (c) 1997-8 Sun Microsystems, Inc. All Rights Reserved.
 */
package voxware.engine.recognition;

import javax.speech.*;
import javax.speech.recognition.*;

import java.net.*;
import java.io.*;
import java.util.*;

import voxware.engine.*;
import voxware.util.ObjectSet;
import voxware.util.SysLog;

/**
 * Skeletal Implementation of the JSAPI Recognizer interface.
 * 
 * This class is useful by itself for debugging, e.g. you
 * can load grammars and simulate a recognizer recognizing
 * some text, etc.
 * <P>
 *
 * Actual JSAPI recognizer implementations might want to extend or
 * modify this implementation.
 * <P>
 *
 * Also contains utility routines for:
 * <UL>
 *  <LI>Loading imported grammars and resolving inter-grammar
 *      references.
 *
 *  <LI>Printing/dumping grammars in an extensible way
 *      (used to dump grammar to under-lying recognizer
 *       implementation via ascii strins)
 *
 *  <LI>Routines for copying grammars from one recognizer implementation
 *      to another.
 * </UL>
 *
 * @author Stuart Adams
 * @version 1.11 12/14/98 16:22:27
 */
public class BaseRecognizer extends BaseEngine implements Recognizer, Cloneable {

  protected ObjectSet resultListeners;
  protected Hashtable grammarList;
  protected ObjectSet deletedGrammars;
  protected boolean   caseSensitiveGrammarNames = true;
  protected boolean   reloadAll = false;     // True iff recognizer cannot handle partial grammar loading

//////////////////////
// Begin Constructors
//////////////////////

  /**
   * Create a new Recognizer in the DEALLOCATED state.
   */
  public BaseRecognizer() {
    this(null, false);
  }

  /**
   * Create a new Recognizer in the DEALLOCATED state.
   * @param reloadAll set to true if recognizer cannot handle 
   * partial grammar loading.  Default = false.
   */
  public BaseRecognizer(RecognizerModeDesc mode, boolean reloadAll) {
    super(mode);
    this.reloadAll = reloadAll;
    resultListeners = new ObjectSet();
    grammarList = new Hashtable();
    deletedGrammars = new ObjectSet();
    audioManager = new BaseRecognizerAudioManager();
  }

//////////////////////
// End Constructors
//////////////////////

//////////////////////
// Begin overridden Engine Methods
//////////////////////

  /**
   * Return an object that provides management of the audio input or output of the Engine.
   * From javax.speech.Engine.
   */
  public synchronized AudioManager getAudioManager() {
    if (audioManager == null) audioManager = new BaseRecognizerAudioManager();
    return audioManager;
  }

//////////////////////
// End overridden Engine Methods
//////////////////////
    
//////////////////////
// Begin Recognizer Methods
//////////////////////

  /**
   * Create a new RuleGrammar with the given name.
   * From javax.speech.recognition.Recognizer.
   * @param name the name of the RuleGrammar
   */
  public synchronized RuleGrammar newRuleGrammar(String name) throws IllegalArgumentException, EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    BaseRuleGrammar grammar = new BaseRuleGrammar(this, name);
    storeGrammar(grammar);
    return grammar;
  }

  /** 
   * NOT IMPLEMENTED YET.
   * We use a JavaCC generated parser for reading jsgf file. It 
   * does not support reading from java.io.Reader yet - once it
   * does we will implement this method.  The current implementation
   * assumes the Reader can be converted into an ASCII input stream.
   * From javax.speech.recognition.Recognizer.
   * @param JSGFinput the Reader containing JSGF input.
   */
  public synchronized RuleGrammar loadJSGF(Reader JSGFinput) throws GrammarException, IOException, EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    RuleGrammar grammar = JSGFParser.newGrammarFromJSGF(JSGFinput, this);
    if (grammar == null) throw new IOException();             // Should never happen
    if (grammar.getName() != null) storeGrammar(grammar);
    return grammar;
  }

  /**
   * Load a RuleGrammar and its imported grammars from a URL containing JSGF text.
   * From javax.speech.recognition.Recognizer.
   * @param baseURL the base URL containing the JSGF grammar file.
   * @param grammarName the name of the JSGF grammar to load.
   */
  public synchronized RuleGrammar loadJSGF(java.net.URL baseURL, String grammarName)
  throws GrammarException, MalformedURLException, IOException, EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);

     // Convert each period in the grammar name to a slash "/"
     // Append a slash and the converted grammar name to the base URL
     // Append the ".gram" suffix
    grammarName.replace('.', '/');
    String uslString = baseURL.toString();
    uslString = uslString.concat(((uslString.lastIndexOf((int)'/') == (uslString.length() - 1)) ? "" : "/") + grammarName + ".gram");
    URL grammarURL = null;
    InputStream ins = null;
    try {
      grammarURL = new URL(uslString);
      ins = grammarURL.openStream();
    } catch (MalformedURLException me) {
      throw new MalformedURLException(uslString);
    }
    RuleGrammar grammar = JSGFParser.newGrammarFromJSGF(ins, this);
    loadAllImports(this);
    if (grammar == null) throw new IOException();             // Should never happen
    if (grammar.getName() != null) storeGrammar(grammar);
    return grammar;
  }

  /** 
   * NOT IMPLEMENTED YET.
   * From javax.speech.recognition.Recognizer.
   */
  public synchronized RuleGrammar loadJSGF(URL context, String grammarName, boolean loadImports, boolean reloadGrammars, Vector loadedGrammars) 
  throws GrammarException, MalformedURLException, IOException, EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    throw new RuntimeException("BaseRecognizer.loadJSGF: variant not yet implemented");
  }

  /**
   * Return the named RuleGrammar 
   * From javax.speech.recognition.Recognizer.
   * @param name the name of the RuleGrammar.
   */
  public synchronized RuleGrammar getRuleGrammar(String name) throws EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    return retrieveGrammar(name);
  }

  /**
   * Get a list of the RuleGrammars known to the Recognizer.
   * From javax.speech.recognition.Recognizer.
   */
  public synchronized RuleGrammar[] listRuleGrammars() throws EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    RuleGrammar[] ruleGrammars = null;
    if (grammarList == null) {
      ruleGrammars = new RuleGrammar[0];
    } else {
      ruleGrammars = new RuleGrammar[grammarList.size()];
      int i = 0;
      Enumeration grammars = grammarList.elements();
      while (grammars.hasMoreElements()) ruleGrammars[i++] = (RuleGrammar)grammars.nextElement();
    }
    return ruleGrammars;
  }

  /**
   * Delete a RuleGrammar from the Recognizer.
   * From javax.speech.recognition.Recognizer.
   * @param grammar the RuleGrammar to delete.
   */
  public synchronized void deleteRuleGrammar(RuleGrammar grammar) throws IllegalArgumentException, EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    String name = grammar.getName();
    grammarList.remove(name);
    deletedGrammars.add(grammar);
  }

  /**
   * NOT IMPLEMENTED YET.
   * Get the DicationGrammar for this Recognizer.
   * From javax.speech.recognition.Recognizer.
   */
  public synchronized DictationGrammar getDictationGrammar(String name) throws EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    return null;
  }

  /** 
   * Commit grammar changes.
   * From javax.speech.recognition.Recognizer.
   *
   * N.B. Synchronizing this entire method DOES NOT WORK with the Wind River JVM!
   *      We MUST synchronize explicitly on "this", as done here, whether or not
   *      the debug printout is used. Why this is so is anyone's guess, since the
   *      two should be logically equivalent. Whatever the reason, this is EVIL!
   */
  public void commitChanges() throws GrammarException, EngineStateError {
    if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
      SysLog.println("BaseRecognizer.commitChanges: invoked");
    synchronized (this) {
      suspend();                                                // Throws EngineStateError

      boolean grammarsHaveChanged = grammarsHaveChanged();
      if (grammarsHaveChanged) linkGrammars();
      commitChangesInternal(grammarsHaveChanged);               // Throws GrammarException

       // Notify any grammars whose activation state has changed
      notifyGrammarActivation(testEngineState(FOCUS_ON));

      long oldEngineState = engineState;
      engineState &= ~(SUSPENDED | PROCESSING);
      engineState |= LISTENING;
      changesCommitted(oldEngineState, engineState, null);
    }
  }

  /**
   * Temporarily suspend recognition while the application updates
   * grammars prior to a call to commitChanges.
   * From javax.speech.recognition.Recognizer.
   */
  public synchronized void suspend() throws EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    if (testEngineState(SUSPENDED)) return;
    suspendInternal();
    long oldEngineState = engineState;
    engineState &= ~(LISTENING | PROCESSING);
    engineState |= SUSPENDED;
    recognizerSuspended(oldEngineState, engineState);
  }

  /**
   * NOT IMPLEMENTED YET.
   * If the Recognizer is in the PROCESSING state, force the Recognizer
   * to immediately complete processing of that result by finalizing it.
   * From javax.speech.recognition.Recognizer.
   * @param flush whether audio buffer should be processed or flushed.
   */
  public synchronized void forceFinalize(boolean flush) throws EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    throw new RuntimeException("BaseRecognizer.forceFinalize: not supported");
  }

  /**
   * Request speech focus for this Recognizer from the underlying speech recognition system.
   * From javax.speech.recognition.Recognizer.
   */
  public synchronized void requestFocus() throws EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    if (testEngineState(FOCUS_ON)) return;
    notifyGrammarActivation(true);        
    long oldEngineState = engineState;
    engineState &= ~(FOCUS_OFF);
    engineState |= FOCUS_ON;
    focusGained(oldEngineState, engineState);
  }

  /**
   * Release speech focus for this Recognizer from the underlying speech recognition system.
   * From javax.speech.recognition.Recognizer.
   */
  public synchronized void releaseFocus() throws EngineStateError {
    checkEngineState(DEALLOCATED);
    if (testEngineState(FOCUS_OFF)) return;
    notifyGrammarActivation(false);
    long oldEngineState = engineState;
    engineState &= ~(FOCUS_ON);
    engineState |= FOCUS_OFF;
    focusLost(oldEngineState, engineState);
  }

  /**
   * Request notification of Result events from the Recognizer.
   * From javax.speech.recognition.Recognizer.
   * @param listener the listener to add
   */
  public void addResultListener(ResultListener listener) {
    resultListeners.add(listener);
  }

  /**
   * Remove a ResultListener from the list of ResultListeners.
   * From javax.speech.recognition.Recognizer.
   * @param listener the listener to remove
   */
  public void removeResultListener(ResultListener listener) {
    resultListeners.remove(listener);
  }

  /**
   * Get the RecognizerProperties of this Recognizer.
   * From javax.speech.recognition.Recognizer.
   */
  public RecognizerProperties getRecognizerProperties() {
    return (RecognizerProperties)getEngineProperties();
  }

  /**
   * Get the object that manages the speakers of the Recognizer.
   * From javax.speech.recognition.Recognizer.
   */
  public SpeakerManager getSpeakerManager() {
    return null;
  }

  /**
   * Create a new gramar by reading in a grammar stored in a vendor-specific format.  Since BaseGrammar is serializable,
   * we just use read/writeObject to store/restore grammars.
   * From javax.speech.recognition.Recognizer.
   */
  public synchronized Grammar readVendorGrammar(InputStream input) throws VendorDataException, IOException, EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    Grammar grammar = null;
    try {
      grammar = (Grammar)(new ObjectInputStream(input)).readObject();
    } catch (IOException e) {
      throw e;
    } catch (Exception e) {
      throw new VendorDataException("BaseRecognizer.readVendorGrammar: " + e.toString());
    }
    if (grammar == null) throw new VendorDataException("BaseRecognizer.readVendorGrammar: empty grammar");
    if (grammar instanceof BaseGrammar && grammar instanceof RuleGrammar && grammar.getName() != null) {
      storeGrammar((RuleGrammar)grammar);
      ((BaseGrammar)grammar).setRecognizer(this);
    } else {
      throw new VendorDataException("BaseRecognizer.readVendorGrammar: grammar is not a RuleGrammar");
    }
    return grammar;
  }

  /**
   * Create a new grammar by reading in a grammar stored in a
   * vendor-specific format.  Since BaseGrammar is serializable we just 
   * use read/write object to store/restore grammars.
   * From javax.speech.recognition.Recognizer.
   */
  public synchronized void writeVendorGrammar(OutputStream output, Grammar gram) throws IOException, EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    ObjectOutputStream stream = new ObjectOutputStream(output);
    stream.writeObject(gram);
  }

  /**
   * Read a Result from a stream in a vendor-specific format.
   * Since BaseResult is serializable we just 
   * use read/write object to store/restore grammars.
   * From javax.speech.recognition.Recognizer.
   */
  public synchronized Result readVendorResult(InputStream output) throws VendorDataException, IOException, EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    BaseResult result = null;
    try {
      result = (BaseResult)(new ObjectInputStream(output)).readObject();
    } catch (IOException e) {
      throw e;
    } catch (Exception e) {
      throw new VendorDataException("BaseRecognizer.readVendorResult: " + e.toString());
    }
    return (FinalResult)result;
  }

  /**
   * Store a Result to a stream in a vendor-specific format.
   * Since BaseResult is serializable we just 
   * use read/write object to store/restore grammars.
   * From javax.speech.recognition.Recognizer.
   */
  public synchronized void writeVendorResult(OutputStream output, Result result) throws IOException, ResultStateError, EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    ObjectOutputStream stream = new ObjectOutputStream(output);
    stream.writeObject(result);
  }

//////////////////////
// End Recognizer Methods
//////////////////////

//////////////////////
// NON-JSAPI METHODS
//////////////////////

  /**
   * Add a grammar to the grammar list.
   * NOT JSAPI.
   */
  protected void storeGrammar(RuleGrammar grammar) {
    if (caseSensitiveGrammarNames) grammarList.put(grammar.getName(), grammar);
    else                           grammarList.put(grammar.getName().toLowerCase(), grammar);
  }

  /**
   * Retrieve a grammar from the grammar list.
   * NOT JSAPI.
   */
  protected RuleGrammar retrieveGrammar(String name) {
    if (caseSensitiveGrammarNames) return (RuleGrammar)grammarList.get(name);
    else                           return (RuleGrammar)grammarList.get(name.toLowerCase());
  }

  /**
   * Until the loadJSAPI(Reader) method can be implemented we support loading JSGF grammars from an InputStream
   * NOT JSAPI.
   */
  public synchronized RuleGrammar loadJSGF(InputStream JSGFinput) throws GrammarException, IOException, EngineStateError {
    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);
    RuleGrammar grammar = JSGFParser.newGrammarFromJSGF(JSGFinput, (Recognizer)this);
    if ((grammar != null) && (grammar.getName() != null)) storeGrammar(grammar);
    return grammar;
  }

  /**
   * Let listeners know the recognizer rejected something.
   * Not JSAPI.
   */
  public void rejectUtterance() {
    BaseResult result = new BaseResult(null);
    this.resultCreated(result);
     // Can reject without finalizing the grammar, which may not be known
    result.resultRejected(this, null);
  }

  /**
   * Let listeners know the recognizer recognized/finalized/accepted result.
   * NOT JSAPI.
   */
  public void notifyResult(String grammarName, String words) throws IllegalArgumentException {
    BaseRuleGrammar grammar = (BaseRuleGrammar)getRuleGrammar(grammarName);
    if (grammar == null)
      throw new IllegalArgumentException("BaseRecognizer.notifyResult: grammar \"" + grammarName + "\" is unknown");
    BaseResult result = new BaseResult(grammar, words, false);
    this.resultCreated(result);
    result.setResultState(Result.ACCEPTED);
    result.grammarFinalized(this, grammar);
    result.resultAccepted(this, grammar);
  }

//////////////////////
// Begin utility methods for sending RecognizerEvents
//////////////////////

  /**
   * Utility function to generate CHANGES_COMMITTED event and send it to all recognizer listeners.
   */
  protected void changesCommitted(long oldState, long newState, GrammarException exception) {
    if (engineListeners == null) return;
    RecognizerEvent event = new RecognizerEvent(this, RecognizerEvent.CHANGES_COMMITTED, oldState, newState, exception);
    Enumeration listeners = engineListeners.iterator();
    while (listeners.hasMoreElements()) {
      EngineListener listener = (EngineListener)listeners.nextElement();
      if (listener instanceof RecognizerListener) ((RecognizerListener)listener).changesCommitted(event);
    }
  }

  /**
   * Utility function to generate FOCUS_GAINED event and send it to all recognizer listeners.
   */
  protected void focusGained(long oldState, long newState) {
    if (engineListeners == null) return;
    RecognizerEvent event = new RecognizerEvent(this, RecognizerEvent.FOCUS_GAINED, oldState, newState, null);
    Enumeration listeners = engineListeners.iterator();
    while (listeners.hasMoreElements()) {
      EngineListener listener = (EngineListener)listeners.nextElement();
      if (listener instanceof RecognizerListener) ((RecognizerListener)listener).focusGained(event);
    }
  }

  /**
   * Utility function to generate FOCUS_LOST event and send it to all recognizer listeners.
   */
  protected void focusLost(long oldState, long newState) {
    if (engineListeners == null) return;
    RecognizerEvent event = new RecognizerEvent(this, RecognizerEvent.FOCUS_LOST, oldState, newState, null);
    Enumeration listeners = engineListeners.iterator();
    while (listeners.hasMoreElements()) {
      EngineListener listener = (EngineListener)listeners.nextElement();
      if (listener instanceof RecognizerListener) ((RecognizerListener)listener).focusLost(event);
    }
  }

  /**
   * Utility function to generate RECOGNIZER_PROCESSING event and send it to all recognizer listeners.
   */
  protected void recognizerProcessing(long oldState, long newState) {
    if (engineListeners == null) return;
    RecognizerEvent event = new RecognizerEvent(this, RecognizerEvent.RECOGNIZER_PROCESSING, oldState, newState, null);
    Enumeration listeners = engineListeners.iterator();
    while (listeners.hasMoreElements()) {
      EngineListener listener = (EngineListener)listeners.nextElement();
      if (listener instanceof RecognizerListener) ((RecognizerListener)listener).recognizerProcessing(event);
    }
  }

  /**
   * Utility function to generate RECOGNIZER_SUSPENDED event and send it to all recognizer listeners.
   */
  protected void recognizerSuspended(long oldState, long newState) {
    if (engineListeners == null) return;
    RecognizerEvent event = new RecognizerEvent(this, RecognizerEvent.RECOGNIZER_SUSPENDED, oldState, newState, null);
    Enumeration listeners = engineListeners.iterator();
    while (listeners.hasMoreElements()) {
      EngineListener listener = (EngineListener)listeners.nextElement();
      if (listener instanceof RecognizerListener) ((RecognizerListener)listener).recognizerSuspended(event);
    }
  }

//////////////////////
// End utility methods for sending RecognizerEvents
//////////////////////

//////////////////////
// Begin utility methods for sending ResultEvents
// The remaining utility methods are in BaseResult.java.
//////////////////////

  /**
   * Utility function to generate RESULT_CREATED event and send it to all result listeners.
   */
  protected void resultCreated(Result r) {
    if (resultListeners != null) {
      ResultEvent resultEvent = new ResultEvent(r, ResultEvent.RESULT_CREATED);
      Enumeration listeners = resultListeners.iterator();
      while (listeners.hasMoreElements()) ((ResultListener)listeners.nextElement()).resultCreated(resultEvent);
    }
  }

//////////////////////
// End utility methods for sending ResultEvents
//////////////////////

  /**
   * Return true iff any grammar has been added, deleted or changed
   */
  protected boolean grammarsHaveChanged() {
    boolean haveChanges = false;
    if (!deletedGrammars.isEmpty()) {
       // At least one grammar has been deleted
      haveChanges = true;
    } else {
       // Find out if any of the RuleGrammars have changed
      RuleGrammar[] grammars = listRuleGrammars();
      for (int i = 0; i < grammars.length; i++) {
        BaseRuleGrammar ruleGrammar = (BaseRuleGrammar)grammars[i];
        if (ruleGrammar.hasChanged()) { 
          haveChanges = true; 
          break; 
        }
      }
    }
    return haveChanges;
  }

  /**
   * Cycle through all the rules in each grammar, calling the method changeRule() on each one
   * that has changed or is new. A grammar changes when a rule or import is added or removed,
   * but not when its rules are enabled or disabled or when its activation mode changes.
   *
   * Calling changeRule() should propogate the change to the underlying recognizer. If the
   * recognizer can use the rules directly from the list of enabled rules without making
   * copies of its own, changeRule() can be empty. Otherwise, changeRule() should modify
   * the recognizer's copy.
   *
   * Make a list of all enabled rules and call changeEnabled() to pass it to the underlying
   * recognizer.
   */
  protected void commitChangesInternal(boolean haveChanges) throws GrammarException {
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
      SysLog.println("BaseRecognizer.commitChangesInternal: have" + (haveChanges ? "" : " NO") + " changes to commit");
    Vector enabledRules = new Vector();
     // If there are to be rule changes, let the underlying recognizer know they are about to happen
    if (haveChanges) {
      startGrammarChanges();
       // Notify the underlying recognizer of any grammar deletions
      if (!deletedGrammars.isEmpty()) {
        RuleGrammar[] defunctGrammars = new RuleGrammar[deletedGrammars.size()];
        defunctGrammars = (RuleGrammar[])deletedGrammars.toArray(defunctGrammars);
        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
          SysLog.println("BaseRecognizer.commitChangesInternal: notifying underlying recognizer of " + defunctGrammars.length + " deleted grammars");
        // CURRENTLY DOES NOTHING !!!
        deletedGrammars.clear();
      }
    }
     // Assemble a list of enabled rules and propogate rule changes to the underlying recognizer
    RuleGrammar[] grammars = listRuleGrammars();
    for (int i = 0; i < grammars.length; i++) {
      String grammarName = grammars[i].getName();
      BaseRuleGrammar ruleGrammar = (BaseRuleGrammar)grammars[i];
      String[] ruleNames = ruleGrammar.listRuleNames();
      for (int j = 0; j < ruleNames.length; j++) {
        String ruleName = ruleNames[j];
         // Assemble a list of all enabled (RuleGrammar, Rule) pairs
        if (ruleGrammar.isRulePublic(ruleName) && ruleGrammar.isEnabled(ruleName))
          enabledRules.addElement(grammarName + "_" + ruleName);
         // Propogate any changes to the underlying recognition engine
        if (haveChanges && (ruleGrammar.isRuleChanged(ruleName) || reloadAll)) {
          ruleGrammar.setRuleChanged(ruleName, false);
          boolean isPublic = false;
          try { 
            isPublic = ruleGrammar.isRulePublic(ruleName);
          } catch (IllegalArgumentException nse) {
          }
          Rule rule = ruleGrammar.getRule(ruleName);
          changeRule(grammarName, ruleName, rule, isPublic);
        }
      }
      ruleGrammar.grammarChanged = false;
      ruleGrammar.grammarChangesCommitted();
    }
     // If there have been rule changes, let the underlying recognizer know they are finished
    if (haveChanges) endGrammarChanges();
     // Pass the list of enabled rules to the underlying recognizer
    changeEnabled(enabledRules);                            // Throws GrammarException
  }

  /** 
   * Called at the start of the commit process iff there are to be any rule changes
   * The underlying recognizer class should override this if it needs to know.
   */
  protected void startGrammarChanges() { 
  }

  /** 
   * Called at the end of the commit process iff there have been any rule changes
   * The underlying recognizer class should override this if it needs to know.
   */
  protected void endGrammarChanges() { 
  }

  /** 
   * Called with list of rules that should be enabled.
   * Often the underlying recognizer will be able to use this directly, without explicitly knowing
   * about any grammar changes. The underlying recognizer class will normally override this to get
   * the list and process it.
   */
  protected void changeEnabled(Vector enabled) throws GrammarException { 
  }

  /**
   * Called to propogate a rule change to the underying recognizer.
   * The underlying recognizer class should override this if it needs to know.
   */
  protected void changeRule(String grammarName, String ruleName, Rule rule, boolean isPublic) {
    
  }

  /**
   * Check each grammar and load any imported grammars that are not already loaded.
   */
  static public void loadAllImports(Recognizer recognizer) throws GrammarException, IOException {
      RuleGrammar ruleGrammars[] = recognizer.listRuleGrammars();
      for (int i = 0; i < ruleGrammars.length; i++) loadImports(recognizer, ruleGrammars[i]);
  }

  /**
   * Load grammars imported by the specified RuleGrammar if they are not already loaded.
   */
  static private void loadImports(Recognizer recognizer, RuleGrammar grammar) throws GrammarException, IOException {
    RuleName imports[] = grammar.listImports();
    if (imports != null) {
      for (int i = 0; i < imports.length; i++) {
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
          SysLog.println("BaseRecognizer.loadImports: checking for import \"" + imports[i].getRuleName() + "\"");
        String grammarName = imports[i].getFullGrammarName();
        if (recognizer.getRuleGrammar(grammarName) == null) {
           // Handle imports as URL's and/or resources? !!!
          String fileName = imports[i].getFullGrammarName() + ".gram";
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
          SysLog.println("BaseRecognizer.loadImports: loading imported grammar file \"" + fileName + "\"");
          RuleGrammar importedGrammar = null;
          if (recognizer instanceof BaseRecognizer)
            importedGrammar = ((BaseRecognizer)recognizer).loadJSGF(new FileInputStream(fileName));
          else
            importedGrammar = (RuleGrammar)recognizer.loadJSGF(new FileReader(fileName));
          if (importedGrammar == null)
            throw new GrammarException("BaseRecognizer.loadImports: grammar imported from file \"" + fileName + "\" is null");
        }
      }
    }
  }

  /**
   * Resolve all rule references in all rules in all rule grammars.
   */
  protected void linkGrammars() throws GrammarException {
    RuleGrammar ruleGrammars[] = listRuleGrammars();
    for (int i = 0; i < ruleGrammars.length; i++) ((BaseRuleGrammar)(ruleGrammars[i])).resolveAllRules();
  }

  /**
   * Determine if the Recognizer has any enabled modal grammars.
   */
  protected boolean hasEnabledModalGrammars() {
    boolean hasEnabledModalGrammars = false;
    if (grammarList != null) {
      Enumeration grammars = grammarList.elements();
      while (grammars.hasMoreElements()) {
        Grammar grammar = (Grammar)grammars.nextElement();
        if (grammar.isEnabled() && grammar.getActivationMode() == Grammar.RECOGNIZER_MODAL) {
          hasEnabledModalGrammars = true;
          break;
        }
      }
    }
    return hasEnabledModalGrammars;
  }

  /** 
   * Notify any grammars whose activation state has changed, returning true iff there is at least one such grammar.
   * The only methods that immediately affect grammar activation status are requestFocus(), releaseFocus() and commitChanges().
   * The Grammar methods setEnabled() and setActivationMode() don't take effect until the next commitChanges().
   */
  protected boolean notifyGrammarActivation(boolean haveFocus) {
    boolean activationStateChanged = false;
    if (grammarList != null) {
      boolean hasEnabledModalGrammars = hasEnabledModalGrammars();
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
        SysLog.println("BaseRecognizer.notifyGrammarActivation: " + (haveFocus ? "have" : "do not have") + " focus; have " + (hasEnabledModalGrammars ? "" : "no") + " enabled modal grammars");
      Enumeration grammars = grammarList.elements();
      while (grammars.hasMoreElements()) {
        BaseRuleGrammar grammar = (BaseRuleGrammar)grammars.nextElement(); 
        boolean active = grammar.isEnabled() &&
                         (grammar.getActivationMode() == Grammar.GLOBAL ||
                          haveFocus && (grammar.getActivationMode() == Grammar.RECOGNIZER_MODAL || !hasEnabledModalGrammars));
        activationStateChanged = grammar.activate(active) || activationStateChanged;
      }
    }
    return activationStateChanged;
  }

  /**
   * Deallocate the resources for the Recognizer.
   * Overrides deallocateEngine() in BaseEngine
   *
   * @exception EngineException 
   */
  protected void deallocateEngine() throws EngineException {            
    releaseFocus();
  }

  /**
   * Suspend the underlying Recognizer.
   *
   * @exception EngineException 
   */
  protected void suspendInternal() {            
  }

  /**
   * Clone the entire Recognizer.
   * Overrides clone() in Object class to clone the grammarList and its elements
   */
  protected Object clone() {
    try {
      BaseRecognizer clone = (BaseRecognizer)super.clone();
       // Clone the grammarList
      clone.grammarList = (Hashtable)grammarList.clone();
       // Replace each Grammar in the grammarList with a clone
      Enumeration enumeration = grammarList.elements();
      while (enumeration.hasMoreElements()) {
        RuleGrammar grammarClone = (RuleGrammar)(((BaseRuleGrammar)enumeration.nextElement()).clone());
        clone.grammarList.put(grammarClone.getName(), grammarClone);
      }
      return clone;
    } catch (CloneNotSupportedException e) {
      throw new RuntimeException("BaseRecognizer.clone: UNEXPECTED " + e.toString());
    }
  }
}
