/*
 * Copyright (c) 1997-8 Sun Microsystems, Inc. All Rights Reserved.
 */

package voxware.engine.recognition;

import java.util.*;
import javax.speech.*;
import javax.speech.recognition.*;

import voxware.util.SysLog;

/**
 * Implementation of the parse method(s) on 
 * javax.speech.recognition.RuleGrammar.
 *
 * @version 1.3 11/25/98 16:51:17
 */
public class RuleParser {

  public static boolean caseSensitive = false;                                    // Whether tokens are case sensitive

  protected Recognizer  recognizer;
  protected RuleGrammar grammar;
  protected String      ruleName;
  protected String[]    ruleNames;
  protected String[]    tokens;
  protected Vector      completeParses = new Vector();
  protected boolean     validPrologue;

  public RuleParser(Recognizer recognizer, RuleGrammar grammar, String ruleName) {
    this.recognizer = recognizer;
    this.grammar = grammar;
    this.ruleName = ruleName;
     // If a rule name is specified, use it; otherwise, try all enabled rules
    if (ruleName != null) {
      ruleNames = new String[1];
      ruleNames[0] = ruleName;
    } else {
      ruleNames = grammar.listRuleNames();
    }
  }

  public RuleParser(Recognizer recognizer, RuleGrammar grammar) {
    this(recognizer, grammar, null);
  }

  // Static Methods

  /**
   * Static convenience version of parse(String) that instantiates a RuleParser
   */
  public static RuleParse parse(String text, Recognizer recognizer, RuleGrammar grammar, String ruleName) throws GrammarException {
    RuleParser ruleParser = new RuleParser(recognizer, grammar, ruleName);
    return ruleParser.parse(text);                                                // Throws GrammarException
  }

  /**
   * Static convenience version of parse(String[]) that instantiates a RuleParser
   */
  public static RuleParse parse(String tokens[], Recognizer recognizer, RuleGrammar grammar, String ruleName) throws GrammarException {
    RuleParser ruleParser = new RuleParser(recognizer, grammar, ruleName);
    return ruleParser.parse(tokens);                                              // Throws GrammarException
  }

   // Tokenize a string
  public static String[] tokenize(String text) {
    StringTokenizer tokenizer = new StringTokenizer(text);
    int size = tokenizer.countTokens();
    String tokens[] = new String[size];
    int i = 0;
    while (tokenizer.hasMoreTokens()) tokens[i++] = caseSensitive ? tokenizer.nextToken() : tokenizer.nextToken().toLowerCase();
    return tokens;
  }

  // Public Methods

  /**
   * Parse a text string against a particular rule from a particluar grammar,
   * returning a RuleParse data structure if successful and null otherwise
   */
  public RuleParse parse(String text) throws GrammarException {
    mparse(text);                                                                 // Throws GrammarException
    return parseResult();
  }

  /**
   * Parse a sequence of tokens against a particular rule from a particluar grammar,
   * returning a RuleParse data structure if successful and null otherwise
   */
  public RuleParse parse(String tokens[]) throws GrammarException {
    mparse(tokens);                                                               // Throws GrammarException
    return parseResult();
  }

  /**
   * Initialize the RuleParser for an incremental parse
   * This convenience method is equivalent to parse(new String[0])
   */
  public void startParse() throws GrammarException {
    tokens = new String[0];
     // Parse of the empty phrase, against the possibility that it may be grammatical
    mparse(tokens);                                                               // Throws GrammarException
  }

  /**
   * Parse a token incrementally, returning true iff the parse is successful so far
   */
  public boolean parseToken(String token) throws GrammarException {
    if (tokens == null) {
      tokens = new String[1];
      tokens[0] = token;
    } else {
      String[] oldTokens = tokens;
      tokens = new String[oldTokens.length + 1];
      System.arraycopy(oldTokens, 0, tokens, 0, oldTokens.length);
      tokens[oldTokens.length] = token;
    }
    return mparse(tokens) != null || validPrologue;                               // Throws GrammarException
  }

  /**
   * Return true iff there is at least one complete parse
   */
  public boolean parseIsFinished() {
    return !completeParses.isEmpty();
  }

  /**
   * Return the first complete RuleParse, if any; otherwise, return null
   */
  public RuleParse parseResult() {
    return completeParses.isEmpty() ? null : (RuleParse)completeParses.elementAt(0);
  }

  /**
   * Return all of the complete RuleParses, if any; otherwise, return null
   */
  public RuleParse[] parseResults() {
    RuleParse[] ruleParses = null;
    if (!completeParses.isEmpty()) {
      ruleParses = new RuleParse[completeParses.size()];
      completeParses.copyInto(ruleParses);
    }
    return ruleParses;
  }

  /**
   * Return the grammar
   */
  public RuleGrammar grammar() {
    return grammar;
  }

  /**
   * Return the array of tokens
   */
  public String[] tokens() {
    return tokens;
  }

  // Protected internal parsing methods

  protected Vector mparse(String text) throws GrammarException {
    tokens = tokenize(text);
    return mparse(tokens);                                                        // Throws GrammarException
  }

  protected Vector mparse(String tokens[]) throws GrammarException {
    this.tokens = tokens;
    validPrologue = false;
    completeParses.removeAllElements();
    for (int j = 0; j < ruleNames.length; j++) {
       // If a rule name is specified, use it regardless of whether it is enabled
      if (ruleName != null || grammar.isRulePublic(ruleNames[j]) && grammar.isEnabled(ruleNames[j])) {
        Rule startRule = grammar.getRule(ruleNames[j]);
        if (startRule == null) {
          throw new GrammarException("RuleParser.mparse: unknown rule \"" + ruleNames[j] + "\"");
        } else {
          Vector parses = this.parse(grammar, startRule, tokens, 0);              // Throws GrammarException
           // Search for complete parses
          if ((parses != null) && (parses.size() != 0)) {
            for (int i = 0; i < parses.size(); i++) {
              TokenPosition tokenPosition = (TokenPosition)parses.elementAt(i);
              if (tokenPosition.position() == tokens.length) {
                if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
                  SysLog.println("RuleParser.mparse: rule \"" + ruleNames[j] + "\" complete parse " + i + ": " + ((Rule)tokenPosition).toString());
                completeParses.addElement(new RuleParse(new RuleName(ruleNames[j]), (Rule)tokenPosition));
              } else if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK)) {
                  SysLog.println("RuleParser.mparse: rule \"" + ruleNames[j] + "\" parse fragment " + i + ": " + ((Rule)tokenPosition).toString());
              }
            }
          }
        }
      }
    }
    return completeParses.isEmpty() ? null : completeParses;
  }

  // Private Recursive Parse Method

  /*
   * Parse routine called recursively while traversing the Rule structure in a depth first manner.
   * Returns a list of valid parses or null.
   */
  private Vector parse(RuleGrammar grammar, Rule rule, String input[], int position) throws GrammarException {

    if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
      SysLog.println("RuleParser.parse: parsing " + rule.getClass().getName() + " at position " + position + ": " + rule.toString());

     // The Vector of parses to return
    Vector parses = null;

    if (rule instanceof RuleName) {

       // REFERENCE: Rule is a RuleName (rule reference)

      RuleName ruleName = (RuleName)rule;
       // Use the resolved RuleName if it is available
      if (ruleName instanceof BaseRuleName) {
        BaseRuleName baseRuleName = (BaseRuleName)ruleName;
        if (baseRuleName.resolvedRuleName != null) ruleName = baseRuleName.resolvedRuleName;
      }
      String simpleName = ruleName.getSimpleRuleName();
      Rule ruleReference = grammar.getRule(simpleName);
      if (ruleReference == null) {
        String grammarName = ruleName.getFullGrammarName();
        if (grammarName.length() > 0) {
          grammar = recognizer.getRuleGrammar(grammarName);
          if (grammar != null) ruleReference = grammar.getRule(simpleName);
          else                 throw new GrammarException("RuleParser.parse: unknown grammar \"" + grammarName + "\"");
        }
        if (ruleReference == null) throw new GrammarException("RuleParser.parse: unknown rule \"" + ruleName.toString() + "\"");
      }
      Vector subParses = parse(grammar, ruleReference, input, position);
      if (subParses != null) {
        parses = new Vector();
        for (int j = 0; j < subParses.size(); j++) {
          TokenPosition tokenPosition = (TokenPosition)subParses.elementAt(j);
          if (tokenPosition instanceof ParserEmptyToken) { 
            parses.addElement(tokenPosition); 
          } else {
            parses.addElement(new ParserRuleParse(ruleName, (Rule)tokenPosition, tokenPosition.position()));
          }
        }
      }

    } else if (rule instanceof RuleToken) {

       // TOKEN: Rule is a RuleToken

      if (position < input.length) {
        String inputText = input[position];
        RuleToken ruleToken = (RuleToken)rule;
        String ruleText = caseSensitive ? ruleToken.getText() : ruleToken.getText().toLowerCase();
        if (SysLog.printLevel > 4 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
          SysLog.println("RuleParser.parse: parsing input token \"" + inputText + "\" against \"" + ruleText + "\"");
        if (inputText.equals(ruleText) || inputText.equals("%")  || inputText.equals("*")) {
          parses = new Vector();
           // Anything matches itself, and "%" matches one occurence of anything (NOT IN JSGF SPECIFICATION!)
          parses.addElement(new ParserRuleToken(ruleText, position + 1));
           // "*" matches zero or more occurences of anything (NOT IN JSGF SPECIFICATION!)
          if (inputText.equals("*")) {
            parses.addElement(new ParserRuleToken(ruleText, position));
          }
        } else if (ruleText.startsWith(inputText)) {
          String ruleTokens[] = tokenize(ruleText);
          int j = 0;
          while (j < ruleTokens.length && position + j < input.length && ruleTokens[j].equals(input[position + j])) j++;
          if (j == ruleTokens.length) {
             // The input satisfies the tokenization of the text of the RuleToken
            parses = new Vector();
            parses.addElement(new ParserRuleToken(ruleText, position + j));
          }
        }
      } else {
        validPrologue = true;
      }

    } else if (rule instanceof RuleAlternatives) {

       // ALTERNATIVES: Rule is a RuleAlternatives

      RuleAlternatives ruleAlternatives = (RuleAlternatives)rule;
      Rule alternativeRules[] = ruleAlternatives.getRules();
      parses = new Vector();
      for (int i = 0; i < alternativeRules.length; i++) {
        Vector subParses = parse(grammar, alternativeRules[i], input, position);
        if (subParses != null)
          for (int j = 0; j < subParses.size(); j++)
            parses.addElement(subParses.elementAt(j));
      }

    } else if (rule instanceof RuleSequence) {

       // SEQUENCE: Rule is a RuleSequence

      RuleSequence ruleSequence = (RuleSequence)rule;
      Rule[] sequence = ruleSequence.getRules();
      if (sequence != null && sequence.length > 0) {
        Vector headParses = parse(grammar, sequence[0], input, position);
        if (headParses != null) {
          parses = new Vector();
          for (int j = 0; j < headParses.size(); j++) {
            Rule headRule = (Rule)headParses.elementAt(j);
            int headPosition = ((TokenPosition)headRule).position();
            if (sequence.length == 1) {
               // The sequence has no tail, so we're finished
              if (headRule instanceof ParserEmptyToken) { 
                parses.addElement(headRule); 
              } else {
                Rule[] parseSequence = new Rule[1];
                parseSequence[0] = headRule;
                parses.addElement(new ParserRuleSequence(parseSequence, headPosition));
              }
            } else {
               // Parse the tail as a RuleSequence
              Rule tailSequence[] = new Rule[sequence.length - 1];
              System.arraycopy(sequence, 1, tailSequence, 0, tailSequence.length);
              Vector tailParses = parse(grammar, new RuleSequence(tailSequence), input, headPosition);
              if (tailParses != null) {
                for (int k = 0; k < tailParses.size(); k++) {
                  Rule tailRule = (Rule)tailParses.elementAt(k);
                  if (tailRule instanceof ParserEmptyToken) { 
                    parses.addElement(headRule); 
                  } else if (headRule instanceof ParserEmptyToken) { 
                    parses.addElement(tailRule); 
                  } else {
                     // The parse of the whole sequence is the concatenation of the head and tail parses
                    Rule[] parseSequence;
                    if (tailRule instanceof RuleSequence) {
                      Rule[] tailParseSequence = ((RuleSequence)tailRule).getRules();
                      parseSequence = new Rule[tailParseSequence.length + 1];
                      parseSequence[0] = headRule;
                      System.arraycopy(tailParseSequence, 0, parseSequence, 1, tailParseSequence.length);
                    } else {
                      parseSequence = new Rule[2];
                      parseSequence[0] = headRule;
                      parseSequence[1] = tailRule;
                    }
                    parses.addElement(new ParserRuleSequence(parseSequence, ((TokenPosition)tailRule).position()));
                  }
                }
              }
            }
          }
        }
      }

    } else if (rule instanceof RuleTag) {

       // TAG: Rule is a RuleTag

      RuleTag ruleTag = (RuleTag)rule;
      String theTag = ruleTag.getTag();
      if (SysLog.printLevel > 4 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
        SysLog.println("RuleParser.parse: parsing tag \"" + theTag + "\"");
      Vector subParses = parse(grammar, ruleTag.getRule(), input, position);
      if (subParses != null) {
        parses = new Vector();
        for (int j = 0; j < subParses.size(); j++) {
          TokenPosition tokenPosition = (TokenPosition)subParses.elementAt(j);
          if (tokenPosition instanceof ParserEmptyToken) { 
            parses.addElement(tokenPosition); 
          } else {
            parses.addElement(new ParserRuleTag((Rule)tokenPosition, theTag, tokenPosition.position()));
          }
        }
      }

    } else if (rule instanceof RuleCount) {

       // COUNT: Rule is a RuleCount ([], *, or + )

      RuleCount ruleCount = (RuleCount)rule;
      int count = ruleCount.getCount();
      Rule countedRule = ruleCount.getRule();
      parses = parse(grammar, countedRule, input, position);
      if (parses == null) {
         // There are no valid non-empty sub-parses; return the empty parse if it is allowed
        if (count != RuleCount.ONCE_OR_MORE) {
          parses = new Vector();
          parses.addElement(new ParserEmptyToken(position));
        }
      } else {
         // Add the empty parse if it is allowed
        if (count != RuleCount.ONCE_OR_MORE)
          parses.addElement(new ParserEmptyToken(position));
         // Add multiple parses if they are allowed
        if (count != RuleCount.OPTIONAL) {
           // Count is ZERO_OR_MORE or ONCE_OR_MORE and have one sub-parse; do multiple sub-parses until input or parser is exhausted
          for (int i = 2; i <= input.length - position; i++) {
             // Produce a RuleSequence of i copies of the counted Rule
            Rule[] sequence = new Rule[i];
            for (int j = 0; j < i; j++) sequence[j] = countedRule;
            Vector multipleSubParses = parse(grammar, new RuleSequence(sequence), input, position);
            if (multipleSubParses != null) {
              for (int k = 0; k < multipleSubParses.size(); k++) parses.addElement(multipleSubParses.elementAt(k));
            } else {
              break;
            }
          }
        }
      }

    } else {

       // UNKNOWN: Rule type is unknown
      throw new GrammarException("RuleParser.parse: unknown rule type \"" + rule.getClass().getName() + "\"");
    }

    return parses;
  }

  // Interface and extended Rule Classes to keep track of position in the token string

   // Interface for keeping track of where a token occurs in the tokenized input string
  interface TokenPosition {
    public int position();
  }

   // Extension of RuleToken with TokenPosition interface
  class ParserRuleToken extends RuleToken implements TokenPosition {
    int position;
    public ParserRuleToken(String x, int position) { super(x); this.position = position; }
    public int position() { return position; }
  }

  class ParserEmptyToken extends ParserRuleToken {
    public ParserEmptyToken(int position) { super("EMPTY", position); }
  }

   // Extension of RuleTag with TokenPosition interface
  class ParserRuleTag extends RuleTag implements TokenPosition {
    int position;
    public ParserRuleTag(Rule r, String x, int position) { super(r, x); this.position = position; }
    public int position() { return position; }
  }

   // Extension of RuleSequence with TokenPosition interface
  class ParserRuleSequence extends RuleSequence implements TokenPosition {
    int position;
    public ParserRuleSequence(Rule rules[], int position) { super(rules); this.position = position; }
    public int position() { return position; }
  }

   // Extension of RuleParse with TokenPosition interface
  class ParserRuleParse extends RuleParse implements TokenPosition {
    int position;
    public ParserRuleParse(int position) { super(); this.position = position; }
    public ParserRuleParse(RuleName rn, Rule r, int position) { super(rn, r); this.position = position; }
    public int position() { return position; }
  }
}
