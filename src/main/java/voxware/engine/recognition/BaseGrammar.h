#ifndef __voxware_engine_recognition_BaseGrammar__
#define __voxware_engine_recognition_BaseGrammar__

#pragma interface


extern "Java"
{
  namespace javax
  {
    namespace speech
    {
      namespace recognition
      {
        class ResultListener;
        class GrammarListener;
        class Recognizer;
      }
    }
  }
  namespace voxware
  {
    namespace util
    {
      class ObjectSet;
    }
    namespace engine
    {
      namespace recognition
      {
        class BaseGrammar;
        class BaseRecognizer;
      }
    }
  }
}

class voxware::engine::recognition::BaseGrammar : public ::java::lang::Object
{
public:
  BaseGrammar (::voxware::engine::recognition::BaseRecognizer *, ::java::lang::String *);
  virtual ::javax::speech::recognition::Recognizer *getRecognizer () { return reinterpret_cast< ::javax::speech::recognition::Recognizer *> (recognizer); }
  virtual ::java::lang::String *getName () { return myName; }
  virtual void setEnabled (jboolean);
  virtual jboolean isEnabled () { return grammarEnabled; }
  virtual void setActivationMode (jint);
  virtual jint getActivationMode () { return activationMode; }
  virtual jboolean isActive () { return active; }
  virtual void addGrammarListener (::javax::speech::recognition::GrammarListener *);
  virtual void removeGrammarListener (::javax::speech::recognition::GrammarListener *);
  virtual void addResultListener (::javax::speech::recognition::ResultListener *);
  virtual void removeResultListener (::javax::speech::recognition::ResultListener *);
public:  // actually protected
  virtual void grammarActivated ();
  virtual void grammarDeactivated ();
  virtual void grammarChangesCommitted ();
public:
  virtual jboolean activate (jboolean);
  virtual void setName (::java::lang::String *);
  virtual void setRecognizer (::voxware::engine::recognition::BaseRecognizer *);
  ::voxware::engine::recognition::BaseRecognizer * __attribute__((aligned(__alignof__( ::java::lang::Object )))) recognizer;
public:  // actually protected
  ::voxware::util::ObjectSet *grammarListeners;
  ::voxware::util::ObjectSet *resultListeners;
  jboolean active;
  ::java::lang::String *myName;
  jboolean grammarEnabled;
  jint activationMode;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_recognition_BaseGrammar__ */
