package voxware.engine.recognition;

import java.util.*;
import javax.speech.*;
import javax.speech.recognition.*;
import java.io.Serializable;

import voxware.util.ObjectSet;
import voxware.util.SysLog;

/**
 * Implementation of javax.speech.recognition.RuleGrammar.
 *
 * @version 1.5 12/16/98 16:43:44
 */
public class BaseRuleGrammar extends BaseGrammar implements RuleGrammar, Serializable, Cloneable {

  protected Hashtable rules;
  protected Vector    imports;    

  protected transient ObjectSet deletedRules;
  protected transient ObjectSet changedRules;
  protected transient ObjectSet removedImports;
  protected transient ObjectSet addedImports;
  protected transient boolean   grammarChanged;

  /**
   * Create a new BaseRuleGrammar
   * @param recognizer the BaseRecognizer for this Grammar.
   * @param name the name of this Grammar.
   */
  public BaseRuleGrammar(BaseRecognizer recognizer, String name) {
    super(recognizer, name);
    rules = new Hashtable();
    imports = new Vector();
    deletedRules = new ObjectSet();
    changedRules = new ObjectSet();
    grammarChanged = true;
  }

//////////////////////
// Begin overridden Grammar Methods
//////////////////////

  /**
   * Set the enabled property of the Grammar.
   * From javax.speech.recognition.Grammar.
   * @param enabled the new desired state of the enabled property.
   */
  public void setEnabled(boolean enabled) {
    if (rules != null) {
      Enumeration e = rules.elements();
      while (e.hasMoreElements()) {
        GRule rule = (GRule)e.nextElement();
        if (rule.isPublic) rule.isEnabled = enabled;
      }
    }
  }

//////////////////////
// End overridden Grammar Methods
//////////////////////

//////////////////////
// Begin RuleGrammar Methods
//////////////////////

  /**
   * Parse partial JSGF text to a Rule object.
   * From javax.speech.recognition.RuleGrammar.
   */
  public Rule ruleForJSGF(String text) {
    return JSGFParser.ruleForJSGF(text);
  }

  /**
   * Set a rule in the grammar either by creating a new rule or by updating an existing one.
   * @param ruleName the name of the rule.
   * @param rule the definition of the rule.
   * @param isPublic whether this rule is public or not.
   */
  public void setRule(String ruleName, Rule rule, boolean isPublic) throws NullPointerException, IllegalArgumentException {
    if (ruleName == null)
      throw new NullPointerException("BaseRuleGrammar.setRule: ruleName is null");
    if (rule == null)
      throw new NullPointerException("BaseRuleGrammar.setRule: rule is null");
    GRule grule = new GRule();
    grule.ruleName = stripRuleName(ruleName);
     // Copy and convert all RuleNames to BaseRuleNames so we can keep track of the fully resolved name
    grule.rule = BaseRuleName.copy(rule);                                             // Throws IllegalArgumentException
    grule.isPublic = isPublic;
    grule.isEnabled = false;
    grule.hasChanged = true;
    rules.put(grule.ruleName, grule);
    grammarChanged = true;
  }

  /**
   * Return a copy of the data structure for the named rule.
   * From javax.speech.recognition.RuleGrammar.
   * @param ruleName the name of the rule.
   */
  public Rule getRule(String ruleName) {
    GRule rule = (GRule)rules.get(stripRuleName(ruleName));
    return (rule == null) ? null : rule.rule.copy(); 
  }

  /**
   * Return the data structure for the named rule.
   * From javax.speech.recognition.RuleGrammar.
   * @param ruleName the name of the rule.
   */
  public Rule getRuleInternal(String ruleName) {
    GRule rule = (GRule)rules.get(ruleName);
    return (rule == null) ? null : rule.rule; 
  }

  /** 
   * Test whether the specified rule is public.
   * From javax.speech.recognition.RuleGrammar.
   * @param ruleName the name of the rule.
   */
  public boolean isRulePublic(String ruleName) throws IllegalArgumentException {
    GRule rule = (GRule)rules.get(stripRuleName(ruleName));
    if (rule == null) throw new IllegalArgumentException("BaseRuleGrammar.isRulePublic: unknown Rule \"" + ruleName + "\"");
    return rule.isPublic;
  }

  /**
   * List the names of all rules defined in this Grammar.
   * From javax.speech.recognition.RuleGrammar.
   */
  public String[] listRuleNames() {
    String ruleNames[] = new String[rules.size()];
    int i = 0;
    Enumeration e = rules.elements();
    while (e.hasMoreElements()) ruleNames[i++] = ((GRule)e.nextElement()).ruleName;
    return ruleNames;
  }

  /** 
   * Delete a rule from the grammar.
   * From javax.speech.recognition.RuleGrammar.
   * @param ruleName the name of the rule.
   */
  public void deleteRule(String ruleName) throws IllegalArgumentException {
    if (rules.get(stripRuleName(ruleName)) == null) throw new IllegalArgumentException("BaseRuleGrammar.deleteRule: unknown Rule \"" + ruleName + "\"");
    rules.remove(ruleName);
    deletedRules.add(ruleName);
    grammarChanged = true;
  }

  /**
   * Set the enabled state of the listed rule.
   * From javax.speech.recognition.RuleGrammar.
   * @param ruleName the name of the rule.
   * @param enabled the new enabled state.
   */
  public void setEnabled(String ruleName, boolean enabled) throws IllegalArgumentException {
    GRule rule = (GRule)rules.get(stripRuleName(ruleName));
    if (rule == null || !rule.isPublic) throw new IllegalArgumentException("BaseRuleGrammar.setEnabled: unknown Rule \"" + ruleName + "\"");
    if (rule.isEnabled != enabled) rule.isEnabled = enabled;
  }

  /**
   * Set the enabled state of the listed rules.
   * From javax.speech.recognition.RuleGrammar.
   * @param ruleNames the names of the rules.
   * @param enabled the new enabled state.
   */
  public void setEnabled(String[] ruleNames, boolean enabled) throws IllegalArgumentException {
    for (int i = 0; i < ruleNames.length; i++) {
      GRule rule = (GRule)rules.get(stripRuleName(ruleNames[i]));
      if (rule == null || !rule.isPublic) throw new IllegalArgumentException("BaseRuleGrammar.setEnabled: unknown Rule \"" + ruleNames[i] + "\"");
      if (rule.isEnabled != enabled) rule.isEnabled = enabled;
    }
  }

  /**
   * Return enabled state of a public rule.
   * From javax.speech.recognition.RuleGrammar.
   * @param ruleName the name of the rule.
   */
  public boolean isEnabled(String ruleName) throws IllegalArgumentException {
    if (ruleName == null) {
      return isEnabled();
    } else {
      GRule rule = (GRule)rules.get(stripRuleName(ruleName));
      if (rule == null || !rule.isPublic) throw new IllegalArgumentException("BaseRuleGrammar.isEnabled: unknown Rule \"" + ruleName + "\"");
      return rule.isEnabled;
    }
  }   

  /**
   * Return true iff any public rule is enabled.
   * From javax.speech.recognition.RuleGrammar.
   */
  public boolean isEnabled() {
    Enumeration e = rules.elements();
    while (e.hasMoreElements()) {
      GRule rule = (GRule)e.nextElement();
      if (rule.isPublic && rule.isEnabled) return true;
    }
    return false;
  }

  /**
   * Resolve a simple or qualified rulename as a full rulename.
   * From javax.speech.recognition.RuleGrammar.
   *
   * The following referencing and name resolution principles apply:
   *  (1) Any rule in a local grammar may be referenced with (a) its simple rulename or (b) its qualified rulename or fully-qualified rulename.
   *  (2) A public rule of another grammar may be referenced by (a) its simple rulename or (b) its qualified rulename if the rule is imported
   *      and the name is not ambiguous with another imported rule.
   *  (3) A public rule of another grammar may be referenced by its fully-qualified rulename (a) with or (b) without a corresponding import statement.
   *
   * @param ruleName the name of the rule.
   */
  public RuleName resolve(RuleName name) throws GrammarException {
    RuleName rn = new RuleName(name.getRuleName());

    String simpleName = rn.getSimpleRuleName();
    String grammarName = rn.getSimpleGrammarName();
    String packageName = rn.getPackageName();
    String fullGrammarName = rn.getFullGrammarName();

    if (packageName != null && grammarName == null) {
       // RuleName is ill-formed (can't have package name without grammar name)
      throw new GrammarException("BaseRuleGrammar.resolve: ill-formed rulename \"" + rn + "\"", null);
    } else if (fullGrammarName == null && this.getRule(simpleName) != null) {
       // Principle 1a: RuleName is a simple rule name local to this RuleGrammar
      return new RuleName(myName + "." + simpleName);                                 // Principle 1a: return unique reference
    } else if (fullGrammarName != null) {
       // RuleName is a qualified or fully qualified rule name
      RuleGrammar grammar = recognizer.getRuleGrammar(fullGrammarName);
      if (grammar != null && grammar.getRule(simpleName) != null)
         // Principle 1b (grammar == this) or 3b (grammar != this)
        return new RuleName(fullGrammarName + "." + simpleName);                      // Principle 1b or 3b: return unique reference
    }

     // Collect all matching imports into a Vector, whose size will then indicate resolvability:
     //  size() = 0 if rn is unresolvable
     //  size() = 1 if rn is uniquely resolvable
     //  size() > 1 if rn is ambiguous
    Vector matches = new Vector();

     // Get list of imports
    RuleName imports[] = listImports();

     // Add the local grammar to simplify checking for a qualified or fully-qualified local reference
    if (imports == null) {
      imports = new RuleName[1];
      imports[0] = new RuleName(myName + ".*");
    } else {
      RuleName[] tmp = new RuleName[imports.length + 1];
      System.arraycopy(imports, 0, tmp, 0, imports.length);
      tmp[imports.length] = new RuleName(myName + ".*");
      imports = tmp;
    }

     // Check each import statement for a possible match
    for (int i = 0; i < imports.length; i++) {

         // TO-DO: update for JSAPI 1.0
        String iSimpleName = imports[i].getSimpleRuleName();
        String iGrammarName = imports[i].getSimpleGrammarName();
        String iPackageName = imports[i].getPackageName();
        String iFullGrammarName = imports[i].getFullGrammarName();

         // Check for badly formed import name
        if (iFullGrammarName == null) throw new GrammarException("BaseRuleGrammar.resolve: ill-formed import \"" + imports[i] + "\"", null);

         // Get the imported grammar and test for a match against it
        RuleGrammar gref = recognizer.getRuleGrammar(iFullGrammarName);
        if (gref == null) {
           // Ignore import statement that specifies an undefined grammar
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
            SysLog.println("BaseRuleGrammar.resolve: ignoring import of unknown grammar \"" + imports[i] + "\" in " + myName);
        } else if (!iSimpleName.equals("*") && gref.getRule(iSimpleName) == null) {
           // Ignore import statement that includes an undefined simpleName field
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.JSAPI_MASK))
            SysLog.println("BaseRuleGrammar.resolve: import of undefined rule \"" + imports[i] + "\" in " + myName);
        } else if (iFullGrammarName.equals(fullGrammarName) || iGrammarName.equals(fullGrammarName)) {
           // Reference is qualified or fully-qualified: import <ipkg.igram.???> must match <pkg.gram.???> or <gram.???>, where ipkg may be null
          if (iSimpleName.equals("*")) {
            if (gref.getRule(simpleName) != null)
               // Import <pkg.gram.*> matches <pkg.gram.rulename>
              matches.addElement(new RuleName(iFullGrammarName + "." + simpleName));  // Principle 2b (qualified) || 3a (fully qualified)
          } else if (iSimpleName.equals(simpleName)) {
             // Import <pkg.gram.rulename> matches <???.gram.rulename>
            matches.addElement(new RuleName(iFullGrammarName + "." + simpleName));    // Principle 2b (qualified) || 3a (fully qualified)
          }
        } if (fullGrammarName != null) {
           // rn is qualified or fully-qualified, so the match has failed
          ;
        } else if (iSimpleName.equals("*")) {
          if (gref.getRule(simpleName) != null)
             // Import <pkg.gram.*> matches <simpleName>
            matches.addElement(new RuleName(iFullGrammarName + "." + simpleName));    // Principle 2a
        } else if (iSimpleName.equals(simpleName)) {
           // Import <ipkg.igram.iSimpleName> matches <simpleName>
          matches.addElement(new RuleName(iFullGrammarName + "." + simpleName));      // Principle 2a
        }
    }

     // The return behavior depends upon number of matches
    if (matches.size() == 0) {
      return null;                                                                    // Reference is unresolvable
    } else if (matches.size() > 1) {
      StringBuffer b = new StringBuffer();
      b.append("BaseRuleGrammar.resolve: ambiguous reference " + rn + " in " + myName + " to ");
      for (int i = 0; i<matches.size(); i++) {
        if (i > 0) b.append(" and ");
        b.append((RuleName)(matches.elementAt(i)));
      }
      throw new GrammarException(b.toString(), null);                                 // Reference to imported rules is ambiguous
    } else {
       // Reference is unique; return the matching RuleName
      return (RuleName)(matches.elementAt(0));                                        // Return unique reference to imported rule
    }
  }

  /**
   * Import all rules or a specified rule from another grammar.
   * From javax.speech.recognition.RuleGrammar.
   * @param importName the name of the rule(s) to import.
   */
  public void addImport(RuleName importName) {
    if (!imports.contains(importName)) {
      imports.addElement(importName);
      grammarChanged = true;
    }
  }

  /**
   * Remove an import.
   * From javax.speech.recognition.RuleGrammar.
   * @param importName the name of the rule(s) to remove.
   */
  public void removeImport(RuleName importName) throws IllegalArgumentException {
    if (imports.contains(importName)) {
      imports.removeElement(importName);
      grammarChanged = true;
    } else {
      throw new IllegalArgumentException("BaseRuleGrammar.removeImport: rule \"" + importName.toString() + "\" not imported");
    }
  }

  /**
   * List the current imports.
   * From javax.speech.recognition.RuleGrammar.
   */
  public RuleName[] listImports() {
    if (imports == null) return new RuleName[0];
    RuleName rn[] = new RuleName[imports.size()];
    Enumeration e = imports.elements();
    int i = 0;
    while (e.hasMoreElements()) rn[i++] = (RuleName)e.nextElement();
    return rn;
  }

  /**
   * Parse the text string against the specified rule.
   * Uses the RuleParser class.
   * From javax.speech.recognition.RuleGrammar.
   * @param text the text to parse.
   * @param ruleName the name of rule to use for parsing.
   */
  public RuleParse parse(String text, String ruleName) throws GrammarException {
    if (ruleName != null) ruleName = stripRuleName(ruleName);
    return RuleParser.parse(text, recognizer, this, ruleName);                        // Throws GrammarException
  }

  /**
   * Parse the tokens string against the specified rule.
   * Uses the RuleParser class.
   * From javax.speech.recognition.RuleGrammar.
   * @param tokens the tokens to parse.
   * @param ruleName the name of rule to use for parsing.
   */
  public RuleParse parse(String tokens[], String ruleName) throws GrammarException {
    if (ruleName != null) ruleName = stripRuleName(ruleName);
    return RuleParser.parse(tokens, recognizer, this, ruleName);                      // Throws GrammarException
  }

  /**
   * Parse the nth best result of a FinalRuleResult against the specified rule.
   * Uses the RuleParser class.
   * From javax.speech.recognition.RuleGrammar.
   * @param r the FinalRuleResult.
   * @param nBest the nth best result to use.
   * @param ruleName the name of rule to use for parsing.
   */
  public RuleParse parse(FinalRuleResult result, int nBest, String ruleName) throws GrammarException {
     // Some JSAPI implementations we run into are not JSAPI compliant, so try a few alternatives
    ResultToken resultToken[] = result.getAlternativeTokens(nBest);
    if (resultToken != null || (resultToken = result.getBestTokens()) != null) {
      String tokens[] = new String[resultToken.length];
      for (int i = 0; i < resultToken.length; i++) tokens[i] = resultToken[i].getSpokenText();
      return parse(tokens, ruleName);                                                 // Throws GrammarException
    } else {
      return parse(result.toString(), ruleName);                                      // Throws GrammarException
    }
  }

  /**
   * NOT IMPLEMENTED YET.
   * Return a String containing the specification for this Grammar.
   */
  public String toString() {
    throw new RuntimeException("BaseRuleGrammar.toString: not yet implemented");
  }

//////////////////////
// End RuleGrammar Methods
//////////////////////

//////////////////////
// NON-JSAPI METHODS
//////////////////////

  /**
   * List all of the rules defined in this Grammar.
   * From javax.speech.recognition.RuleGrammar.
   */
  public GRule[] listRules() {
    GRule theRules[] = new GRule[rules.size()];
    int i = 0;
    Enumeration e = rules.elements();
    while (e.hasMoreElements()) theRules[i++] = (GRule)e.nextElement();
    return theRules;
  }

  /**
   * Report whether the grammar has changed
   */
  public boolean hasChanged() {
    return grammarChanged;
  }

  /**
   * Mark the grammar as changed/unchanged
   */
  public void hasChanged(boolean changed) {
    grammarChanged = changed;
  }

  /**
   * Mark a rulename as changed
   */
  protected void setRuleChanged(String ruleName, boolean changed) {
    GRule rule = (GRule)rules.get(stripRuleName(ruleName));
    if (rule == null) return;
    rule.hasChanged = changed;
    grammarChanged = true;
  }

  /**
   * Resolve all rule references contained in all rules
   */
  protected void resolveAllRules() throws GrammarException {

     // Make sure that all imports exist
    RuleName imports[] = listImports();
    StringBuffer buffer = new StringBuffer();
    if (imports != null) {
      for (int i = 0; i < imports.length; i++) {
        String importedGrammarName = imports[i].getFullGrammarName();
        if (recognizer.getRuleGrammar(importedGrammarName) == null) {
          if (buffer.length() > 0) buffer.append(" and ");
          buffer.append(importedGrammarName);
        }
      }
    }
    if (buffer.length() > 0) {
      throw new GrammarException(buffer.insert(0, "BaseRuleGrammar.resolveAllRules: undefined grammar(s) ").append(" imported in " + myName).toString(), null);
    }

     // Resolve all the rules
    String ruleNames[] = listRuleNames();
    for (int i = 0; i < ruleNames.length; i++) resolveRule(getRuleInternal(ruleNames[i]));
  }

  /**
   * Resolve the given rule
   */
  protected void resolveRule(Rule rule) throws GrammarException {

    if (rule instanceof RuleToken) {
      return;
    }
    if (rule instanceof RuleAlternatives) {
      RuleAlternatives ruleAlternatives = (RuleAlternatives)rule;
      Rule alternatives[] = ruleAlternatives.getRules();
      for (int i = 0; i < alternatives.length; i++) resolveRule(alternatives[i]);
      return;
    }
    if (rule instanceof RuleSequence) {
      RuleSequence ruleSequence = (RuleSequence)rule;
      Rule sequence[] = ruleSequence.getRules();
      for (int i = 0; i < sequence.length; i++) resolveRule(sequence[i]);
      return;
    }
    if (rule instanceof RuleCount) {
      RuleCount ruleCount = (RuleCount)rule;
      resolveRule(ruleCount.getRule());
      return;
    }
    if (rule instanceof RuleTag) {
      RuleTag ruleTag = (RuleTag)rule;
      resolveRule(ruleTag.getRule());
      return;
    }
    if (rule instanceof BaseRuleName) {
      BaseRuleName ruleName = (BaseRuleName)rule;
      RuleName resolved = resolve(ruleName);
      if (resolved == null) {
        throw new GrammarException("BaseRuleGrammar.resolveRule: unresolvable rule \"" + ruleName.toString() + "\" in grammar \"" + myName + "\"", null);
      } else {
        ruleName.resolvedRuleName = resolved;
        return;
      }
    }
    throw new GrammarException("BaseRuleGrammar.resolveRule: unknown rule type \"" + rule.getClass().getName() + "\"", null);
  }

  /**
   * See if a rule has changed
   */
  protected boolean isRuleChanged(String ruleName) {
    GRule rule = (GRule)rules.get(stripRuleName(ruleName));
    return (rule != null) ? rule.hasChanged : false;
  }

  /** 
   * Storage for documentation comments for rules for jsgfdoc
   */
  Properties ruleDocComments = new Properties();

  /** 
   * Storage for documentation comments for imports for jsgfdoc
   */
  Properties importDocComments = new Properties();

  /** 
   * Storage for documentation comments for the grammar for jsgfdoc
   */
  String grammarDocComment = null;

  /**
   * Add a new RuleGrammar comment
   */
  void addRuleDocComment(String ruleName, String comment) { 
    ruleDocComments.put(ruleName, comment); 
  }

  /**
   * Retrieve a RuleGrammar comment
   */
  public String getRuleDocComment(String ruleName) { 
    return ruleDocComments.getProperty(ruleName, null); 
  }

  /**
   * Add a new import comment
   */
  void addImportDocComment(RuleName importName, String comment) {
    importDocComments.put(importName.toString(), comment); 
  }

  /**
   * Retrieve an import comment
   */
  public String getImportDocComment(RuleName importName) { 
    return importDocComments.getProperty(importName.toString(), null); 
  }

  /**
   * Add the Grammar comment
   */
  void addGrammarDocComment(String comment) { 
    grammarDocComment = comment; 
  }

  /**
   * Retrieve the Grammar comment
   */
  public String getGrammarDocComment() { 
    return grammarDocComment; 
  }

  /**
   * Annote the specified rule with its line number from the source file for debugging purposes
   */
  void setSourceLine(String ruleName, int i) {
    GRule r = (GRule)rules.get(stripRuleName(ruleName));
     /* TODO : exception */
    if (r != null) r.lineno = i;
  }

  /**
   * Get the line number in the source file for the specified rule name
   */
  public int getSourceLine(String ruleName) {
    GRule r = (GRule)rules.get(stripRuleName(ruleName));
     /* TODO : exception */
    return (r != null) ? r.lineno : 0;
  }

  /**
   * Add a sample sentence to the list of sample sentences that go with the specified rule
   */
  void addSampleSentence(String ruleName, String sample) {
    GRule r = (GRule)rules.get(stripRuleName(ruleName));
     /* TODO : exception */
    if (r != null) r.samples.addElement(sample);
  }

  /**
   * Add a sample sentence to the list of sample sentences that go with the specified rule
   */
  void setSampleSentences(String ruleName, Vector samples) {
    GRule rule = (GRule)rules.get(stripRuleName(ruleName));
     /* TODO : exception */
    if (rule != null) rule.samples = samples;
  }

  /**
   * Get the list of sample sentences that go with the specified rule
   */
  public Vector getSampleSentences(String ruleName) {
    GRule r = (GRule)rules.get(stripRuleName(ruleName));
    return (r != null) ? r.samples : null;
  }

  /**
   * Return a unique 32-bit id for the specified rule
   */
  public int getRuleID(String ruleName) {
    GRule r = (GRule)rules.get(stripRuleName(ruleName));
    return (r != null) ? r.hashCode() : -1;
  }

  /**
   * Remove a pair of leading/trailing angle brackets <>
   */
  protected String stripRuleName(String name) {
    return (name.length() > 2 && name.startsWith("<") && name.endsWith(">")) ? name.substring(1, name.length() - 1) : name;
  }

  /**
   * Clone this BaseRuleGrammar
   */
  protected Object clone() {
    try {
      BaseRuleGrammar grammarClone = (BaseRuleGrammar)super.clone();
      grammarClone.rules = (Hashtable)rules.clone();
      Enumeration enumeration = rules.elements();
      while (enumeration.hasMoreElements()) {
        GRule ruleClone = (GRule)((GRule)enumeration.nextElement()).clone();
        grammarClone.rules.put(ruleClone.ruleName, ruleClone);
      }
      grammarClone.imports = new Vector(imports.size());
      enumeration = imports.elements();
      while (enumeration.hasMoreElements()) {
        grammarClone.imports.addElement(((RuleName)enumeration.nextElement()).copy());
      }
      return grammarClone;
    } catch (CloneNotSupportedException e) {
      throw new RuntimeException("BaseRuleGrammar.clone: UNEXPECTED " + e.toString());
    }
  }

  /** 
   * Internal class used to hold rules and their attributes
   */
  protected class GRule implements Serializable, Cloneable {

    protected String   ruleName;
    protected Rule     rule;
    protected boolean  isPublic;
    protected boolean  isEnabled;
    protected boolean  hasChanged;
    protected Vector   samples = new Vector();
    protected int      lineno = 0;

    protected Object clone() {
      try {
        GRule clone = (GRule)super.clone();
        if (rule != null) clone.rule = rule.copy();
        return clone;
      } catch (CloneNotSupportedException e) {
        throw new RuntimeException("BaseRuleGrammar.GRule.clone: UNEXPECTED " + e.toString());
      }
    }
  }
}
