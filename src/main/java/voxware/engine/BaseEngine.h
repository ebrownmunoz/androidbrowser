#ifndef __voxware_engine_BaseEngine__
#define __voxware_engine_BaseEngine__

#pragma interface


extern "Java"
{
  namespace javax
  {
    namespace speech
    {
      class EngineListener;
      class EngineProperties;
      class VocabManager;
      class EngineModeDesc;
      class AudioManager;
    }
  }
  namespace voxware
  {
    namespace engine
    {
      class BaseEngine;
    }
    namespace util
    {
      class ObjectSet;
    }
  }
}

class voxware::engine::BaseEngine : public ::java::lang::Object
{
public:
  BaseEngine (::javax::speech::EngineModeDesc *);
  virtual jlong getEngineState () { return engineState; }
  virtual void waitEngineState (jlong);
  virtual jboolean testEngineState (jlong);
  virtual void allocate ();
  virtual void deallocate ();
  virtual void pause ();
  virtual void resume ();
  virtual ::javax::speech::AudioManager *getAudioManager ();
  virtual ::javax::speech::VocabManager *getVocabManager () { return 0; }
  virtual ::javax::speech::EngineProperties *getEngineProperties ();
  virtual ::javax::speech::EngineModeDesc *getEngineModeDesc () { return engineModeDesc; }
  virtual void addEngineListener (::javax::speech::EngineListener *);
  virtual void removeEngineListener (::javax::speech::EngineListener *);
public:  // actually protected
  virtual void engineAllocatingResources (jlong, jlong);
  virtual void engineAllocated (jlong, jlong);
  virtual void engineDeallocatingResources (jlong, jlong);
  virtual void engineDeallocated (jlong, jlong);
  virtual void enginePaused (jlong, jlong);
  virtual void engineResumed (jlong, jlong);
  virtual void checkEngineState (jlong);
  virtual void allocateEngine () { }
  virtual void deallocateEngine () { }
  virtual void pauseEngine () { }
  virtual void resumeEngine () { }
  jlong __attribute__((aligned(__alignof__( ::java::lang::Object ))))  engineState;
  ::voxware::util::ObjectSet *engineListeners;
  ::javax::speech::AudioManager *audioManager;
  ::javax::speech::EngineModeDesc *engineModeDesc;

  friend class voxware_engine_BaseEngine$StateChangeListener;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_BaseEngine__ */
