package voxware.engine.audioplayer;

public class AudioAllocationException extends Exception {

  AudioAllocationException() {
    super();
  }

  AudioAllocationException(String detail) {
    super(detail);
  }
}
