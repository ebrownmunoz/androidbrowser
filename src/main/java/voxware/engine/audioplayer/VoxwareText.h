#ifndef __voxware_engine_audioplayer_VoxwareText__
#define __voxware_engine_audioplayer_VoxwareText__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace engine
    {
      namespace audioplayer
      {
        class VoxwarePlayable;
        class VoxwareText;
        class AsyncListener;
      }
    }
  }
  namespace gnu
  {
    namespace gcj
    {
      class RawData;
    }
  }
}

class voxware::engine::audioplayer::VoxwareText : public ::java::lang::Object
{
public:
  static void setSpeed (jfloat);
  static jfloat getSpeed ();
  static void setPitch (jfloat);
  static jfloat getPitch ();
  static void setVolume (jfloat);
  static jfloat getVolume ();
  static void setVoice (::java::lang::String *);
  static ::java::lang::String *getVoice ();
  static void setLanguage (::java::lang::String *);
  static ::java::lang::String *getLanguage ();
  static void setEngine (::java::lang::String *);
  static ::java::lang::String *getEngine ();
  VoxwareText (::voxware::engine::audioplayer::VoxwareText *);
  VoxwareText (::java::lang::String *);
  VoxwareText ();
  virtual ::gnu::gcj::RawData *getPlayable () { return playable; }
  virtual void setListener (::voxware::engine::audioplayer::AsyncListener *);
  virtual ::voxware::engine::audioplayer::AsyncListener *getListener () { return listener; }
  virtual jboolean isPrefetched () { return prefetched; }
  virtual void setText (::java::lang::String *);
  virtual ::java::lang::String *getText () { return text; }
  virtual ::voxware::engine::audioplayer::VoxwarePlayable *copy ();
  virtual jboolean prefetch ();
  virtual jboolean prefetch (::voxware::engine::audioplayer::VoxwarePlayable *);
  virtual void deallocate ();
public:  // actually protected
  virtual void finalize ();
private:
  void constructor ();
  jboolean jniConstructor ();
  void jniFinalizer ();
  jboolean jniPrefetch (::gnu::gcj::RawData *);
  static jboolean nativeLoaded;
  static jfloat ttsSpeed;
public:
  static const jfloat MinSpeed = 0x1.4f8b58p-17f;
private:
  static jfloat ttsPitch;
  static jfloat ttsVolume;
  static ::java::lang::String *ttsVoice;
  static ::java::lang::String *ttsLanguage;
  static ::java::lang::String *ttsEngine;
public:
  ::gnu::gcj::RawData * __attribute__((aligned(__alignof__( ::java::lang::Object )))) playable;
private:
  ::voxware::engine::audioplayer::AsyncListener *listener;
  jboolean prefetched;
  ::java::lang::String *text;
  jfloat speed;
  jfloat pitch;
  jfloat volume;
  ::java::lang::String *voice;
  ::java::lang::String *language;
  ::java::lang::String *engine;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_audioplayer_VoxwareText__ */
