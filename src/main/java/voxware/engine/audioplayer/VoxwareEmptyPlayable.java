package voxware.engine.audioplayer;

import java.io.*;
import java.io.IOException;

public class VoxwareEmptyPlayable implements VoxwarePlayable {

  public VoxwareEmptyPlayable() {}

  public VoxwareEmptyPlayable(VoxwareEmptyPlayable vep) {
    text = vep.text;
    listener = vep.listener;
  }

  private AsyncListener      listener;
  public  AsyncListener   getListener()                       { return listener; }
  public  void            setListener(AsyncListener listener) { this.listener = listener; }

  private String             text = "";
  public  String          getText()                           { return text; }
  public  void            setText(String s)                   { this.text = text; }

  public  gnu.gcj.RawData    playable;
  public  gnu.gcj.RawData getPlayable()                       { return playable; }

  public  boolean          isPrefetched()                     { return true; }

  public VoxwarePlayable copy() {
    return new VoxwareEmptyPlayable(this);
  }

  public boolean prefetch() throws FileNotFoundException, IOException { return true; }
  public boolean prefetch(VoxwarePlayable container) throws FileNotFoundException, IOException { return true; }

  public void  deallocate() {}

}
