/**
 *  VoxwareInputStream - provides an input stream for rendering
 *  on the pacer/endeavor codec
 */
package voxware.engine.audioplayer;

import java.io.*;
import java.io.IOException;

public class VoxwareInputStream extends FilterInputStream implements VoxwarePlayable {

  private static boolean nativeLoaded = VoxwareAudioPlayer.loadLibrary();

   // Constructors

  public VoxwareInputStream() {
    super((FilterInputStream) null);
  }

  public VoxwareInputStream(VoxwareInputStream vis) throws AudioAllocationException {
    super(vis.in);
    text = vis.text;
    listener = vis.listener;
    constructor();  // Throws AudioAllocationException
  }

  public VoxwareInputStream(InputStream in) throws AudioAllocationException {
    super(in);
    text = null;
    constructor();  // Throws AudioAllocationException
  }

  public VoxwareInputStream(InputStream in, String s) throws AudioAllocationException {
    super(in);
    text = s;
    constructor();  // Throws AudioAllocationException
  }

   // Fields and access methods

  private AsyncListener      listener;
  public  AsyncListener   getListener()                       { return listener; }
  public  void            setListener(AsyncListener listener) { this.listener = listener; }

  private String             text;
  public  String          getText()                           { return this.text; }
  public  void            setText(String text)                { this.text = text; }

  public  gnu.gcj.RawData    playable;
  public  gnu.gcj.RawData getPlayable()                       { return playable; }

  private boolean            prefetched;
  public  boolean          isPrefetched()                     { return prefetched; }

   // Methods

  public VoxwarePlayable copy() throws AudioAllocationException {
    return new VoxwareInputStream(this);
  }

  public boolean prefetch() throws FileNotFoundException, IOException {
    return prefetch(null);
  }

  public boolean prefetch(VoxwarePlayable container) throws FileNotFoundException, IOException {
    boolean success = false;
    if (container == null) container = this;
    if (markSupported()) reset();
    if (nativeLoaded) success = jniPrefetch(container.getPlayable());
    prefetched = true;
    return(success);
  }

  public synchronized void deallocate() {
    if (nativeLoaded) jniFinalizer();
    prefetched = false;
  }

  protected synchronized void finalize() {
    if (prefetched) {
      if (nativeLoaded) jniFinalizer();
      prefetched = false;
    }
  }

  protected void constructor() throws AudioAllocationException {
    try {
      if (!jniConstructor()) throw new AudioAllocationException("No Voxware Playables left");
    } catch (UnsatisfiedLinkError e) {
      nativeLoaded = false;
    }
  }

  private native boolean jniConstructor();
  private native void    jniFinalizer();
  private native boolean jniPrefetch(gnu.gcj.RawData playable);
}
