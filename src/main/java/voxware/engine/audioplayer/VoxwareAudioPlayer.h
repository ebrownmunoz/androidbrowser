#ifndef __voxware_engine_audioplayer_VoxwareAudioPlayer__
#define __voxware_engine_audioplayer_VoxwareAudioPlayer__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace engine
    {
      namespace audioplayer
      {
        class VoxwareAudioPlayer;
        class VoxwarePlayable;
      }
    }
  }
  namespace gnu
  {
    namespace gcj
    {
      class RawData;
    }
  }
}

class voxware::engine::audioplayer::VoxwareAudioPlayer : public ::java::lang::Object
{
public: // actually package-private
  static jboolean loadLibrary ();
public:
  VoxwareAudioPlayer ();
  VoxwareAudioPlayer (::java::util::Vector *);
  VoxwareAudioPlayer (::voxware::engine::audioplayer::VoxwarePlayable *);
  virtual jboolean setGain (jfloat);
  virtual jfloat getGain ();
  virtual void setMute (jboolean);
  virtual jboolean getMute ();
  virtual jint getState ();
  virtual void prefetch ();
  virtual void playback (::voxware::engine::audioplayer::VoxwarePlayable *);
  virtual void playback (::java::util::Vector *);
  virtual void playback ();
private:
  void constructor ();
public:  // actually protected
  virtual void finalize ();
private:
  void open ();
public:
  virtual void close ();
  virtual void cancel ();
  virtual void start ();
  virtual void stop ();
private:
  void play (::voxware::engine::audioplayer::VoxwarePlayable *);
  void jniConstructor ();
  void jniFinalizer ();
  void jniOpen ();
  void jniClose ();
  void jniCancel ();
  void jniStart ();
  void jniStop ();
  jboolean jniSetGain (jfloat);
  jfloat jniGetGain ();
  void jniSetMute (jboolean);
  jboolean jniGetMute ();
  void jniPlay (::gnu::gcj::RawData *);
public:
  static ::java::lang::String *LibraryName;
  static ::java::lang::String *LibraryInitializer;
private:
  static jboolean nativeLoaded;
  static const jint REALIZED = 0L;
  static const jint PREFETCHING = 1L;
  static const jint READY = 2L;
  static const jint PLAYING = 3L;
  ::java::util::Vector * __attribute__((aligned(__alignof__( ::java::lang::Object )))) playlist;
  jint state;
  jboolean open__;
  jboolean mute;
public:
  ::gnu::gcj::RawData *player;

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_audioplayer_VoxwareAudioPlayer__ */
