/**
 *  VoxwareVector - a Vector of VoxwarePlayables for audio rendering
 */
package voxware.engine.audioplayer;

import java.io.*;
import java.io.IOException;
import java.util.Vector;
import java.util.Enumeration;

import voxware.util.SysLog;

public class VoxwareVector extends Vector implements VoxwarePlayable {

  private static boolean nativeLoaded = VoxwareAudioPlayer.loadLibrary();

   // Constructors

  public VoxwareVector(VoxwareVector vv) throws AudioAllocationException {
    super(vv);
    text = vv.text;
    listener = vv.listener;
    vv.copy = true;
    copy = true;
    if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
      SysLog.println("VoxwareVector::VoxwareVector(VoxwareVector) called");
    constructor();  // Throws AudioAllocationException
  }

  public VoxwareVector() throws AudioAllocationException {
    super();
    text = null;
    constructor();  // Throws AudioAllocationException
  }

  public VoxwareVector(int capacity) throws AudioAllocationException {
    super(capacity);
    text = null;
    constructor();  // Throws AudioAllocationException
  }

  public VoxwareVector(Vector in) throws AudioAllocationException {
    super(in);
    text = null;
    if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
      SysLog.println("VoxwareVector::VoxwareVector(Vector) called");
    constructor();  // Throws AudioAllocationException
  }


   // Fields and access methods

  private AsyncListener      listener;
  public  void            setListener(AsyncListener listener) { this.listener = listener; }
  public  AsyncListener   getListener()                       { return listener; }

  private String             text;
  public  void            setText(String text)                { this.text = text; }
  public  String          getText() {
    StringBuffer subtext = new StringBuffer();
    if (this.text != null) subtext.append(this.text);
    Enumeration playables = this.elements();
    while (playables.hasMoreElements()) {
      String textElement = ((VoxwarePlayable)playables.nextElement()).getText();
      if (textElement != null && textElement.length() > 0) {
        if (subtext.length() > 0) subtext.append(" ");
        subtext.append(textElement);
      }
    }
    return subtext.toString();
  }

  public  gnu.gcj.RawData    playable;
  public  gnu.gcj.RawData getPlayable()                       { return playable; }

  private boolean            prefetched;
  public  boolean          isPrefetched()                     { return prefetched; }

  protected boolean  copy = false;

   // Methods

  public VoxwarePlayable copy() throws AudioAllocationException {
    return new VoxwareVector(this);
  }

  public boolean prefetch() throws FileNotFoundException, IOException {
    return prefetch(null);
  }

  public boolean prefetch(VoxwarePlayable container) throws FileNotFoundException, IOException {
    boolean success = false;
    if (container == null) container = this;
    if (nativeLoaded) {
      success = true;
      Enumeration playables = this.elements();
      while (playables.hasMoreElements()) 
        success = success && ((VoxwarePlayable)playables.nextElement()).prefetch(container);
    }
    prefetched = true;
    return(success);
  }

  public synchronized void deallocate() {
     // Deallocate component VoxwarePlayables here iff this is not a copy
     // If this is a copy, relegate such deallocation to the finalizer
     // This allows us to deallocate the components of almost all VoxwareVectors immediately, but still use a shallow copy.
    if (!copy) {
      Enumeration playables = this.elements();
      while (playables.hasMoreElements()) ((VoxwarePlayable)playables.nextElement()).deallocate();
    }
    clear();
    if (nativeLoaded) jniFinalizer();
    prefetched = false;
  }

  protected synchronized void finalize() throws Throwable {
    try {
      clear();
      if (prefetched) {
        if (nativeLoaded) jniFinalizer();
        prefetched = false;
      }
    } finally {
      super.finalize();
    }
  }

  private void constructor() throws AudioAllocationException {
    try {
      if (!jniConstructor()) throw new AudioAllocationException("No Voxware Playables left");
    } catch (UnsatisfiedLinkError e) {
      nativeLoaded = false;
    }
  }

  private native boolean jniConstructor();
  private native void    jniFinalizer();
}
