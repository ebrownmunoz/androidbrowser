#ifndef __voxware_engine_audioplayer_VoxwareVector__
#define __voxware_engine_audioplayer_VoxwareVector__

#pragma interface

#include <java/util/Vector.h>

extern "Java"
{
  namespace gnu
  {
    namespace gcj
    {
      class RawData;
    }
  }
  namespace voxware
  {
    namespace engine
    {
      namespace audioplayer
      {
        class VoxwarePlayable;
        class VoxwareVector;
        class AsyncListener;
      }
    }
  }
}

class voxware::engine::audioplayer::VoxwareVector : public ::java::util::Vector
{
public:
  VoxwareVector (::voxware::engine::audioplayer::VoxwareVector *);
  VoxwareVector ();
  VoxwareVector (jint);
  VoxwareVector (::java::util::Vector *);
  virtual void setListener (::voxware::engine::audioplayer::AsyncListener *);
  virtual ::voxware::engine::audioplayer::AsyncListener *getListener () { return listener; }
  virtual void setText (::java::lang::String *);
  virtual ::java::lang::String *getText ();
  virtual ::gnu::gcj::RawData *getPlayable () { return playable; }
  virtual jboolean isPrefetched () { return prefetched; }
  virtual ::voxware::engine::audioplayer::VoxwarePlayable *copy ();
  virtual jboolean prefetch ();
  virtual jboolean prefetch (::voxware::engine::audioplayer::VoxwarePlayable *);
  virtual void deallocate ();
public:  // actually protected
  virtual void finalize ();
private:
  void constructor ();
  jboolean jniConstructor ();
  void jniFinalizer ();
  static jboolean nativeLoaded;
  ::voxware::engine::audioplayer::AsyncListener * __attribute__((aligned(__alignof__( ::java::util::Vector )))) listener;
  ::java::lang::String *text;
public:
  ::gnu::gcj::RawData *playable;
private:
  jboolean prefetched;
public:  // actually protected
  jboolean copy__;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_audioplayer_VoxwareVector__ */
