/**
 *  VoxwareText - Render text for playback by a VoxwareAudioPlayer 
 */
package voxware.engine.audioplayer;

import java.io.IOException;
import java.io.FileNotFoundException;

public class VoxwareText implements VoxwarePlayable {

  private static boolean nativeLoaded = VoxwareAudioPlayer.loadLibrary();

   // Statics

  private static float      ttsSpeed = 1.0F;              // Linear factor
  public static void        setSpeed(float speed)        { ttsSpeed = speed < MinSpeed ? MinSpeed : speed; }
  public static float       getSpeed()                   { return ttsSpeed; }
  public static final float MinSpeed = 0.00001F;          // To avoid dividing by zero
  private static float      ttsPitch = 0.0F;              // Octave
  public static void        setPitch(float pitch)        { ttsPitch = pitch; }
  public static float       getPitch()                   { return ttsPitch; }
  private static float      ttsVolume = 0.0F;             // dB
  public static void        setVolume(float volume)      { ttsVolume = volume; }
  public static float       getVolume()                  { return ttsVolume; }
  private static String     ttsVoice = null;
  public static void        setVoice(String voice)       { ttsVoice = voice; }
  public static String      getVoice()                   { return ttsVoice; }
  private static String     ttsLanguage = null;
  public static void        setLanguage(String language) { ttsLanguage = language; }
  public static String      getLanguage()                { return ttsLanguage; }
  private static String     ttsEngine = null;
  public static void        setEngine(String engine)     { ttsEngine = engine; }
  public static String      getEngine()                  { return ttsEngine; }

   // Constructors

  public VoxwareText(VoxwareText vt) throws AudioAllocationException {
    text = vt.text;
    listener = vt.listener;
    speed = vt.speed;
    pitch = vt.pitch;
    volume = vt.volume;
    voice = vt.voice;
    language = vt.language;
    engine = vt.engine;
    constructor();  // Throws AudioAllocationException
  }

  public VoxwareText(String text) throws AudioAllocationException {
    this.text = text;
    speed  = ttsSpeed;
    pitch  = ttsPitch;
    volume = ttsVolume;
    voice = ttsVoice;
    language = ttsLanguage;
    engine = ttsEngine;
    constructor();  // Throws AudioAllocationException
  }
    
  public VoxwareText() throws AudioAllocationException {
    this((String) null);
  }

   // Fields and access Methods

  public gnu.gcj.RawData       playable;
  public gnu.gcj.RawData    getPlayable()                       { return playable; }

  private AsyncListener        listener = null;
  public void               setListener(AsyncListener listener) { this.listener = listener; }
  public AsyncListener      getListener()                       { return listener; }

  private boolean              prefetched = false;
  public boolean             isPrefetched()                     { return prefetched; }

  private String               text;
  public void               setText(String text)                { this.text = text; }
  public String             getText()                           { return text; }

  private float                speed    = ttsSpeed;
  private float                pitch    = ttsPitch;
  private float                volume   = ttsVolume;
  private String               voice    = ttsVoice;
  private String               language = ttsLanguage;
  private String               engine   = ttsEngine;

   // Methods

  public VoxwarePlayable copy() throws AudioAllocationException {
    return new VoxwareText(this);
  }

  public boolean prefetch() throws FileNotFoundException, IOException {
    return prefetch(null);
  }

  public boolean prefetch(VoxwarePlayable container) throws FileNotFoundException, IOException {
    boolean success = false;
    if (container == null) container = this;
    if (nativeLoaded) success = jniPrefetch(container.getPlayable());
    prefetched = true;
    return(success);
  }

  public synchronized void deallocate() {
    if (nativeLoaded) jniFinalizer();
    prefetched = false;
  }

  protected synchronized void finalize() {
    if (prefetched) {
      if (nativeLoaded) jniFinalizer();
      prefetched = false;
    }
  }

  private void constructor() throws AudioAllocationException {
    try {
      if (!jniConstructor()) throw new AudioAllocationException("No Voxware Playables left");
    } catch (UnsatisfiedLinkError e) {
      nativeLoaded = false;
    }
  }

  private native boolean jniConstructor();
  private native void    jniFinalizer();
  private native boolean jniPrefetch(gnu.gcj.RawData playable);
}
