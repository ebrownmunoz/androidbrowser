/**
 *  SynthesizedPlayable - provides a synthesized input audio stream for rendering
 *  on the pacer/endeavor codec
 */
package voxware.engine.audioplayer;

import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

public class SynthesizedPlayable extends VoxwareInputStream {

   // Constructors

  public SynthesizedPlayable(SynthesizedPlayable sp) throws AudioAllocationException {
    super(sp);
  }

  public SynthesizedPlayable(double amplitude, List tones) throws AudioAllocationException, IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    new Synthesizer().synthesize(baos, amplitude, tones); // Throws IOException
    in = new ByteArrayInputStream(baos.toByteArray());
    constructor();  // Throws AudioAllocationException
  }

  public SynthesizedPlayable(double amplitude, double offset, double duration, List toneNames) throws AudioAllocationException, IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    new Synthesizer().synthesizeCluster(baos, amplitude, offset, duration, toneNames); // Throws IOException
    in = new ByteArrayInputStream(baos.toByteArray());
    constructor();  // Throws AudioAllocationException
  }

   // Methods

  public VoxwarePlayable copy() throws AudioAllocationException {
    return new SynthesizedPlayable(this);
  }
}
