package voxware.engine.audioplayer;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.ListIterator;

public class Synthesizer {

  public static final double DEFAULT_SAMPLE_RATE = 11025.0; // Hz
  public static final double DEFAULT_ZERO_DECIBELS = 4096.0;

  public static Hashtable keys = new Hashtable(300);
  static {
    keys.put("C8", new Double(4186.01));
    keys.put("C8", new Double(4186.01));
    keys.put("B7", new Double(3951.07));
    keys.put("B7-flat", new Double(3729.31));
    keys.put("A7-sharp", new Double(3729.31));
    keys.put("A7", new Double(3520.00));
    keys.put("A7-flat", new Double(3322.44));
    keys.put("G7-sharp", new Double(3322.44));
    keys.put("G7", new Double(3135.96));
    keys.put("G7-flat", new Double(2959.96));
    keys.put("F7-sharp", new Double(2959.96));
    keys.put("F7", new Double(2793.83));
    keys.put("E7", new Double(2637.02));
    keys.put("E7-flat", new Double(2489.02));
    keys.put("D7-sharp", new Double(2489.02));
    keys.put("D7", new Double(2349.32));
    keys.put("D7-flat", new Double(2217.46));
    keys.put("C7-sharp", new Double(2217.46));
    keys.put("DoubleHighC", new Double(2093.00));
    keys.put("C7", new Double(2093.00));
    keys.put("B6", new Double(1975.53));
    keys.put("B6-flat", new Double(1864.66));
    keys.put("A6-sharp", new Double(1864.66));
    keys.put("A6", new Double(1760.00));
    keys.put("A6-flat", new Double(1661.22));
    keys.put("G6-sharp", new Double(1661.22));
    keys.put("G6", new Double(1567.98));
    keys.put("G6-flat", new Double(1479.98));
    keys.put("F6-sharp", new Double(1479.98));
    keys.put("F6", new Double(1396.91));
    keys.put("E6", new Double(1318.51));
    keys.put("E6-flat", new Double(1244.51));
    keys.put("D6-sharp", new Double(1244.51));
    keys.put("D6", new Double(1174.66));
    keys.put("D6-flat", new Double(1108.73));
    keys.put("C6-sharp", new Double(1108.73));
    keys.put("SopranoC", new Double(1046.50));
    keys.put("C6", new Double(1046.50));
    keys.put("B5", new Double(987.767));
    keys.put("B5-flat", new Double(932.328));
    keys.put("A5-sharp", new Double(932.328));
    keys.put("A5", new Double(880.000));
    keys.put("A5-flat", new Double(830.609));
    keys.put("G5-sharp", new Double(830.609));
    keys.put("G5", new Double(783.991));
    keys.put("G5-flat", new Double(739.989));
    keys.put("F5-sharp", new Double(739.989));
    keys.put("F5", new Double(698.456));
    keys.put("E5", new Double(659.255));
    keys.put("E5-flat", new Double(622.254));
    keys.put("D5-sharp", new Double(622.254));
    keys.put("D5", new Double(587.330));
    keys.put("D5-flat", new Double(554.365));
    keys.put("C5-sharp", new Double(554.365));
    keys.put("TenorC", new Double(523.251));
    keys.put("C5", new Double(523.251));
    keys.put("B4", new Double(493.883));
    keys.put("B4-flat", new Double(466.164));
    keys.put("A4-sharp", new Double(466.164));
    keys.put("440A", new Double(440.000));
    keys.put("A4", new Double(440.000));
    keys.put("A4-flat", new Double(415.305));
    keys.put("G4-sharp", new Double(415.305));
    keys.put("G4", new Double(391.995));
    keys.put("G4-flat", new Double(369.994));
    keys.put("F4-sharp", new Double(369.994));
    keys.put("F4", new Double(349.228));
    keys.put("E4", new Double(329.628));
    keys.put("E4-flat", new Double(311.127));
    keys.put("D4-sharp", new Double(311.127));
    keys.put("D4", new Double(293.665));
    keys.put("D4-flat", new Double(277.183));
    keys.put("C4-sharp", new Double(277.183));
    keys.put("MiddleC", new Double(261.626));
    keys.put("C4", new Double(261.626));
    keys.put("B3", new Double(246.942));
    keys.put("B3-flat", new Double(233.082));
    keys.put("A3-sharp", new Double(233.082));
    keys.put("A3", new Double(220.000));
    keys.put("A3-flat", new Double(207.652));
    keys.put("G3-sharp", new Double(207.652));
    keys.put("G3", new Double(195.998));
    keys.put("G3-flat", new Double(184.997));
    keys.put("F3-sharp", new Double(184.997));
    keys.put("F3", new Double(174.614));
    keys.put("E3", new Double(164.814));
    keys.put("E3-flat", new Double(155.563));
    keys.put("D3-sharp", new Double(155.563));
    keys.put("D3", new Double(146.832));
    keys.put("D3-flat", new Double(138.591));
    keys.put("C3-sharp", new Double(138.591));
    keys.put("LowC", new Double(130.813));
    keys.put("C3", new Double(130.813));
    keys.put("B2", new Double(123.471));
    keys.put("B2-flat", new Double(116.541));
    keys.put("A2-sharp", new Double(116.541));
    keys.put("A2", new Double(110.000));
    keys.put("A2-flat", new Double(103.826));
    keys.put("G2-sharp", new Double(103.826));
    keys.put("G2", new Double(97.9989));
    keys.put("G2-flat", new Double(92.4986));
    keys.put("F2-sharp", new Double(92.4986));
    keys.put("F2", new Double(87.3071));
    keys.put("E2", new Double(82.4069));
    keys.put("E2-flat", new Double(77.7817));
    keys.put("D2-sharp", new Double(77.7817));
    keys.put("D2", new Double(73.4162));
    keys.put("D2-flat", new Double(69.2957));
    keys.put("C2-sharp", new Double(69.2957));
    keys.put("DeepC", new Double(65.4064));
    keys.put("C2", new Double(65.4064));
    keys.put("B1", new Double(61.7354));
    keys.put("B1-flat", new Double(58.2705));
    keys.put("A1-sharp", new Double(58.2705));
    keys.put("A1", new Double(55.0000));
    keys.put("A1-flat", new Double(51.9130));
    keys.put("G1-sharp", new Double(51.9130));
    keys.put("G1", new Double(48.9995));
    keys.put("G1-flat", new Double(46.2493));
    keys.put("F1-sharp", new Double(46.2493));
    keys.put("F1", new Double(43.6536));
    keys.put("E1", new Double(41.2035));
    keys.put("E1-flat", new Double(38.8909));
    keys.put("D1-sharp", new Double(38.8909));
    keys.put("D1", new Double(36.7081));
    keys.put("D1-flat", new Double(34.6479));
    keys.put("C1-sharp", new Double(34.6479));
    keys.put("C1", new Double(32.7032));
    keys.put("B0", new Double(30.8677));
    keys.put("B0-flat", new Double(29.1353));
    keys.put("A0-sharp", new Double(29.1353));
    keys.put("A0", new Double(27.5000));
  }

  public static class Tone {

    private String         name;
    public  String      getName()                 { return this.name; }
    public  boolean     setName(String name)      { Double freq = (Double) keys.get(name);
                                                    if (freq != null) {
                                                      this.frequency = freq.doubleValue();
                                                      this.name = name;
                                                      return true;
                                                    } else {
                                                      return false;
                                                    }
                                                  }

    private double    frequency;
    public  double getFrequency()                 { return this.frequency; }
    public  void   setFrequency(double frequency) { this.frequency = frequency; }

    private double    amplitude = 0.0;
    public  double getAmplitude()                 { return this.amplitude; }
    public  void   setAmplitude(double amplitude) { this.amplitude = amplitude; }

    private double        start;
    public  double     getStart()                 { return this.start; }
    public  void       setStart(double start)     { this.start = start; }

    private double     duration;
    public  double  getDuration()                 { return this.duration; }
    public  void    setDuration(double duration)  { this.duration = duration; }

    private double twoPiFrequency;
    private double amplitudeMultiplier;
    private int startIndex;
    private int endIndex;

    public Tone(String name, double start, double duration) {
      if (setName(name)) {
        setStart(start);
        setDuration(duration);
      }
    }

    public Tone(double frequency, double start, double duration) {
      setFrequency(frequency);
      setStart(start);
      setDuration(duration);
    }

    public void initialize(double sampleRate) {
      twoPiFrequency = (2.0 * Math.PI * frequency) / sampleRate;
      amplitudeMultiplier = dBToMultiplier(amplitude);
      startIndex = (int) Math.round(start * sampleRate);
      endIndex = (int) Math.round((start + duration) * sampleRate);
    }

    public String toString() {
      return super.toString() + "[" + name + "/" + frequency + "Hz/" + amplitude + "dB/" + start + "sec/" + duration + "sec]";
    }
  }

  private static double dBToMultiplier(double amplitude) {
    return java.lang.Math.exp(amplitude * java.lang.Math.log(10) / 10.0);
  }

  private double      sampleRate = DEFAULT_SAMPLE_RATE;
  public  double   getSampleRate()                    { return this.sampleRate; }
  public  void     setSampleRate(double sampleRate)   { this.sampleRate = sampleRate; }

  private double    zeroDecibels = DEFAULT_ZERO_DECIBELS;
  public  double getZeroDecibels()                    { return this.zeroDecibels; }
  public  void   setZeroDecibels(double zeroDecibels) { this.zeroDecibels = zeroDecibels; }

  public Synthesizer() {}

  public Synthesizer(double sampleRate) {
    this.sampleRate = sampleRate;
  }

  public Synthesizer(double sampleRate, double zeroDecibels) {
    this.sampleRate = sampleRate;
    this.zeroDecibels = zeroDecibels;
  }

  public void synthesize(OutputStream stream, List tones) throws IOException {
    synthesize(stream, 0.0, tones); // Throws IOException
  }

  public void synthesize(OutputStream stream, double amplitude, List tones) throws IOException {
    amplitude = dBToMultiplier(amplitude) * zeroDecibels;
    DataOutputStream sampleStream =  (stream instanceof DataOutputStream) ? (DataOutputStream) stream : new DataOutputStream(stream);
    Iterator tonesIterator =  tones.iterator();
    while (tonesIterator.hasNext()) ((Tone) tonesIterator.next()).initialize(sampleRate);
    ArrayList toneStarts = new ArrayList(tones);
    Collections.sort(toneStarts, new Comparator() { public int compare(Object first, Object second) { return ((Tone) first).startIndex - ((Tone) second).startIndex; } });
    // System.out.println("toneStarts has " + toneStarts.size() + " elements");
    ArrayList toneEnds = new ArrayList(tones);
    Collections.sort(toneEnds, new Comparator() { public int compare(Object first, Object second) { return ((Tone) first).endIndex - ((Tone) second).endIndex; } });
    // System.out.println("toneEnds has " + toneEnds.size() + " elements");
    Iterator starts = toneStarts.iterator();
    Iterator ends = toneEnds.iterator();
    HashSet activeToneSet = new HashSet();
    Tone startTone = (Tone) starts.next();
    Tone endTone = (Tone) ends.next();
    double sample = 0.0;
    for (int index = 0; ends.hasNext() || index < endTone.endIndex; index++) {
      if (index == startTone.startIndex) {
        activeToneSet.add(startTone);
        while (starts.hasNext() && (startTone = (Tone) starts.next()).startIndex == index) activeToneSet.add(startTone);
      }
      if (endTone.endIndex == index) {
        activeToneSet.remove(endTone);
        while (ends.hasNext() && (endTone = (Tone) ends.next()).endIndex == index) activeToneSet.remove(endTone);
      }
      sample = 0.0;
      if (!activeToneSet.isEmpty()) {
        Iterator activeTones = activeToneSet.iterator();
        while (activeTones.hasNext()) {
          Tone activeTone = (Tone) activeTones.next();
          sample += Math.sin(activeTone.twoPiFrequency * (double) index) * activeTone.amplitudeMultiplier;
        }
      }
      sampleStream.writeShort((short) Math.round(amplitude * sample));
    }
  }

   // Synthesize a cluster from a list of Tones and tone names; the start and duration of any Tones in the list are ignored
  public void synthesizeCluster(OutputStream stream, double amplitude, double offset, double duration, List toneNames) throws IOException {

    int numTones = toneNames.size();

    ListIterator tones = toneNames.listIterator();
    double start = 0.0;
    while (tones.hasNext()) {
      Object obj = tones.next();
      if (obj instanceof Tone) {
        Tone tone = (Tone) obj;
        tone.setStart(start);
        tone.setDuration(duration);
      } else {
        tones.set(new Tone((String) obj, start, duration));
      }
      start += offset;
    }

    new Synthesizer().synthesize(stream, amplitude, toneNames); // Throws IOException
    stream.close();
  }

  public void synthesizeSequence(OutputStream stream, double amplitude, double duration, List toneNames) throws IOException {
    synthesizeCluster(stream, amplitude, duration, duration, toneNames);
  }

  public static void main(String[] args) {

    double MinDuration = 0.15; // Seconds

    if (args == null || args.length == 0) args = new String[] { "synthesis.wav", "0.0", "0.2", "0.2", "440A" };

    String filename = args[0];

    double amplitude = Double.parseDouble(args[1]); // dB
    double offset    = Double.parseDouble(args[2]); // Seconds
    double duration  = Double.parseDouble(args[3]); // Seconds

    int numTones = args.length - 4;

    ArrayList toneList = new ArrayList(numTones);
    for (int tone = 0; tone < numTones; tone++) toneList.add(args[tone + 4]);

    System.out.println("Synthesizing: " + filename);
    System.out.println("  Amplitude:   " + amplitude);
    System.out.println("  Duration:    " + duration + " seconds per tone");
    Iterator tones = toneList.iterator();
    while (tones.hasNext()) System.out.println("  " + ((String) tones.next()));

    try {
      DataOutputStream stream = new DataOutputStream(new FileOutputStream(filename));
      new Synthesizer().synthesizeCluster(stream, amplitude, offset, duration, toneList);
    } catch (Exception e) {
      System.out.println(e);
    }
  }

}
