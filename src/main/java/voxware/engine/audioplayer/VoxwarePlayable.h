#ifndef __voxware_engine_audioplayer_VoxwarePlayable__
#define __voxware_engine_audioplayer_VoxwarePlayable__

#pragma interface


extern "Java"
{
  namespace gnu
  {
    namespace gcj
    {
      class RawData;
    }
  }
  namespace voxware
  {
    namespace engine
    {
      namespace audioplayer
      {
        class AsyncListener;
        class VoxwarePlayable;
      }
    }
  }
}

class voxware::engine::audioplayer::VoxwarePlayable : public ::java::lang::Object
{
public:
  virtual ::voxware::engine::audioplayer::VoxwarePlayable *copy () = 0;
  virtual ::voxware::engine::audioplayer::AsyncListener *getListener () = 0;
  virtual void setListener (::voxware::engine::audioplayer::AsyncListener *) = 0;
  virtual ::java::lang::String *getText () = 0;
  virtual void setText (::java::lang::String *) = 0;
  virtual ::gnu::gcj::RawData *getPlayable () = 0;
  virtual jboolean prefetch () = 0;
  virtual jboolean prefetch (::voxware::engine::audioplayer::VoxwarePlayable *) = 0;
  virtual jboolean isPrefetched () = 0;
  virtual void deallocate () = 0;

  static ::java::lang::Class class$;
} __attribute__ ((java_interface));

#endif /* __voxware_engine_audioplayer_VoxwarePlayable__ */
