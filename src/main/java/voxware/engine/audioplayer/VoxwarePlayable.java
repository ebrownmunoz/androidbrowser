/**
 *  Playable interface for Voxware Playable objects.
 */
package voxware.engine.audioplayer;

import java.io.IOException;
import java.io.FileNotFoundException;

public abstract interface VoxwarePlayable
{
  public VoxwarePlayable copy() throws AudioAllocationException;
  public AsyncListener   getListener();
  public void            setListener(AsyncListener listener);
  public String          getText();
  public void            setText(String s);
  public gnu.gcj.RawData getPlayable();
  public boolean         prefetch() throws FileNotFoundException, IOException;
  public boolean         prefetch(VoxwarePlayable container) throws FileNotFoundException, IOException;
  public boolean         isPrefetched();
  public void            deallocate();
}
