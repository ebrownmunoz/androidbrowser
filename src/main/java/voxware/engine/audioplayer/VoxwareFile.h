#ifndef __voxware_engine_audioplayer_VoxwareFile__
#define __voxware_engine_audioplayer_VoxwareFile__

#pragma interface

#include <java/io/File.h>

extern "Java"
{
  namespace gnu
  {
    namespace gcj
    {
      class RawData;
    }
  }
  namespace voxware
  {
    namespace engine
    {
      namespace audioplayer
      {
        class VoxwarePlayable;
        class VoxwareFile;
        class AsyncListener;
      }
    }
  }
}

class voxware::engine::audioplayer::VoxwareFile : public ::java::io::File
{
public:
  VoxwareFile (::voxware::engine::audioplayer::VoxwareFile *);
  VoxwareFile (::java::lang::String *);
  VoxwareFile (::java::lang::String *, ::java::lang::String *);
  VoxwareFile (::java::lang::String *, ::java::lang::String *, ::java::lang::String *);
  VoxwareFile (::java::io::File *, ::java::lang::String *);
  VoxwareFile (::java::io::File *, ::java::lang::String *, ::java::lang::String *);
  virtual ::voxware::engine::audioplayer::AsyncListener *getListener () { return listener; }
  virtual void setListener (::voxware::engine::audioplayer::AsyncListener *);
  virtual ::java::lang::String *getText ();
  virtual void setText (::java::lang::String *);
  virtual ::gnu::gcj::RawData *getPlayable () { return playable; }
  virtual jboolean isPrefetched () { return prefetched; }
  virtual ::voxware::engine::audioplayer::VoxwarePlayable *copy ();
  virtual jboolean prefetch ();
  virtual jboolean prefetch (::voxware::engine::audioplayer::VoxwarePlayable *);
  virtual void deallocate ();
public:  // actually protected
  virtual void finalize ();
private:
  void constructor ();
  jboolean jniConstructor ();
  void jniFinalizer ();
  jboolean jniPrefetch (::gnu::gcj::RawData *);
  static jboolean nativeLoaded;
  ::voxware::engine::audioplayer::AsyncListener * __attribute__((aligned(__alignof__( ::java::io::File )))) listener;
  ::java::lang::String *text;
public:
  ::gnu::gcj::RawData *playable;
private:
  jboolean prefetched;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_audioplayer_VoxwareFile__ */
