/**
 *  VoxwareAudioPlayer - Renders sampled waveforms and TTS audio
 *  on the Pacer/Endeavor codec.
 */

package voxware.engine.audioplayer;

import java.util.Vector;
import java.io.*;
import java.lang.Thread;

import voxware.util.SysLog;
import voxware.util.LibraryLoader;

public class VoxwareAudioPlayer {

  public static final String LibraryName = "vjap";
  public static final String LibraryInitializer = "voxware.engine.audioplayer.Initializer.initialize";

  static boolean loadLibrary() {
    //return LibraryLoader.loadLibrary(VoxwareAudioPlayer.LibraryName, VoxwareAudioPlayer.LibraryInitializer, null);
	  return true;
  }

  protected static boolean nativeLoaded = VoxwareAudioPlayer.loadLibrary();

  private static final int REALIZED = 0;
  private static final int PREFETCHING = 1;
  private static final int READY = 2;
  private static final int PLAYING = 3;

  private Vector playlist;
  private int state = REALIZED;
  private boolean open = false;
  private boolean mute = false;
  public  gnu.gcj.RawData player;

  
  
  public VoxwareAudioPlayer() {
    constructor();
    try {
      open();
    } catch(VoxwareResourceUnavailable vru) {
      SysLog.println("VoxwareAudioPlayer(): open() fails (vru)");
    }
  }

  public VoxwareAudioPlayer(Vector playables) {
    playlist = new Vector(playables.size());

    // Make our list with only VoxwarePlayables on it
    int count = playables.size();
    for (int i = 0; i < count; i++) {
      if (playables.elementAt(i) instanceof VoxwarePlayable) {
        playlist.addElement(playables.elementAt(i));
      }
    }

    constructor();
  }

  /** Create a Player with a single VoxwarePlayable object */
  public VoxwareAudioPlayer(VoxwarePlayable vp) {
    playlist = new Vector(1);
    playlist.addElement(vp);

    constructor();
  }

  /** Set the gain for the Player.  Range is 0 (min) to 0xffff (max) */
  public synchronized boolean setGain(float gain) {
    return (nativeLoaded && jniSetGain(gain));
  }

  /** Get the gain for the Player */
  public synchronized float getGain() {
    if (nativeLoaded) {
      return jniGetGain();
    }
    return Float.NaN;
  }

  /** Mute the playback output. Output muted for "true", unmuted for "false" */
  public synchronized void setMute(boolean m) {
    mute = m;
    if (nativeLoaded) {
      jniSetMute(mute);
    }
  }

  /** Get the state of the mute for the Player */
  public synchronized boolean getMute() {
    if (nativeLoaded) {
      mute = jniGetMute();
    }
    return(mute);
  }

  public synchronized int getState() {
    return(state);
  }

 /**
  *  Prefetch the playables vector.
  *  All items in the playables vector are prefetched. If an exception is
  *  encountered when accessing one of the playables the item is ignored.
  *  When this method returns all items have been prefetched.
  *
  *  This blocking method is intended for use with the playback() method.
  */
  public synchronized void prefetch() {
    if (state == REALIZED) {
      int count = playlist.size();
      for (int i = 0; i < count; i++) {
        VoxwarePlayable vp = (VoxwarePlayable) playlist.elementAt(i);
        if (vp.isPrefetched() == false) {
          try {
            vp.prefetch();
          } catch(FileNotFoundException e) {
            SysLog.println(e.toString());
          } catch(IOException e) {
            SysLog.println(e.toString());
          }
        }
      }
      state = READY;
    }
  }

  public synchronized void playback(VoxwarePlayable vp) throws VoxwareResourceUnavailable {
    playlist = new Vector(1);
    playlist.addElement(vp);

    state = REALIZED;
    prefetch();

    if (state == READY) {
      state = PLAYING;
      play(vp);
    }

    return;
  }

  public synchronized void playback(Vector playables) throws VoxwareResourceUnavailable {
    playlist = new Vector(playables.size());

    // make our list with only VoxwarePlayables on it
    int count = playables.size();
    for (int i = 0; i < count; i++) {
      if (playables.elementAt(i) instanceof VoxwarePlayable) {
        playlist.addElement(playables.elementAt(i));
      }
    }

    state = REALIZED;
    prefetch();

    if (state == READY) {
      state = PLAYING;

      count = playlist.size();
      for (int i = 0; i < count; i++) {
        VoxwarePlayable vp = (VoxwarePlayable) playlist.elementAt(i);
        play(vp);
      }
    }

    return;
  }

 /**
  *  Play the playables vector; prefetch is performed if required
  *
  *  VoxwareResourceUnavailable exception is thrown if codec is unavailable
  *
  *  The Player must be either be in the REALIZED (just instantiated) or
  *  READY (prefetch complete) state for this method to have any effect.
  *  This method is synchronous:  it blocks until all items have been played.
  *
  *  This blocking method is intended for use with prefetch() method.
  *  @exception VoxwareResourceUnavailable Codec is in use
  */
  public synchronized void playback() throws VoxwareResourceUnavailable {
    if (state == REALIZED) {
      prefetch();
    }

    if (state == READY) {
      state = PLAYING;

      int count = playlist.size();
      for (int i = 0; i < count; i++) {
        VoxwarePlayable vp = (VoxwarePlayable) playlist.elementAt(i);
        play(vp);
      }
    }

    return;
  }

  private void constructor() {
    if (nativeLoaded) {
      jniConstructor();
    }
  }

  protected void finalize() {
    if (nativeLoaded) {
      close();
      jniFinalizer();
    }
  }

  private void open() throws VoxwareResourceUnavailable {
    if (nativeLoaded) {
      if (!open) {
        open = true;
        jniOpen();
      }
    }
  }

  public void close() {
    if (nativeLoaded) {
      if (open) {
        jniClose();
        open = false;
      }
    }
  }

  public void cancel() {
    if (nativeLoaded) {
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
        SysLog.println("VoxwareAudioPlayer.cancel: calling jniCancel at time: " + System.currentTimeMillis());
      jniCancel();
    }
  }

  public void start() {
    if (nativeLoaded) {
      jniStart();
    }
  }

  public void stop() {
    if (nativeLoaded) {
      jniStop();
    }
  }

  protected void play(VoxwarePlayable vp) {
    SysLog.println("VoxwareAudioPlayer.play: playing <" + vp.getText() + "> ... ");
    if (nativeLoaded) {
      jniPlay(vp.getPlayable());
    }
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
      SysLog.println(" done.");
  }

  protected native void jniConstructor();
  protected native void jniFinalizer();
  protected native void jniOpen() throws VoxwareResourceUnavailable;
  protected native void jniClose();
  protected native void jniCancel();
  protected native void jniStart();
  protected native void jniStop();
  protected native boolean jniSetGain(float gain);
  protected native float  jniGetGain();
  protected native void jniSetMute(boolean mute);
  protected native boolean jniGetMute();
  protected native void jniPlay(gnu.gcj.RawData playable);
}
