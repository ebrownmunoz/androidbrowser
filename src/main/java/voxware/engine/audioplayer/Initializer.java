package voxware.engine.audioplayer;

public class Initializer {

  /**
   * The static initialization method for library vjap
   */
  public static native String initialize(String unused);

}
