/**
 *  VoxwareFile - allows text and/or a type to be associated with a file.
 *  The text provides an alternative that may be spoken via TTS
 *  in lieu of playing the contents of the file. The type allows a
 *  player to identify the format of the file.
 */
package voxware.engine.audioplayer;

import java.io.*;
import java.io.IOException;

public class VoxwareFile extends File implements VoxwarePlayable {

  private static boolean nativeLoaded = VoxwareAudioPlayer.loadLibrary();

   // Constructors
    
  public VoxwareFile(VoxwareFile vf) throws AudioAllocationException {
    super(vf.getPath());
    text = vf.text;
    listener=vf.listener;
    constructor();  // Throws AudioAllocationException
  }

  public VoxwareFile(String path) throws AudioAllocationException {
    super(path);
    text = null;
    constructor();  // Throws AudioAllocationException
  }

  public VoxwareFile(String path, String name) throws AudioAllocationException {
    this(path, name, null);
  }

  public VoxwareFile(String path, String name, String text) throws AudioAllocationException {
    super(path, name);
    this.text = text;
    constructor();  // Throws AudioAllocationException
  }

  public VoxwareFile(File dir, String name) throws AudioAllocationException {
    this(dir, name, null);
  }

  public VoxwareFile(File dir, String name, String text) throws AudioAllocationException {
    super(dir, name);
    this.text = text;
    constructor();  // Throws AudioAllocationException
  }

   // Fields and access methods

  private AsyncListener      listener;
  public  AsyncListener   getListener()                       { return listener; }
  public  void            setListener(AsyncListener listener) { this.listener = listener; }

  private String             text;
  public  String          getText()                           { return (this.text != null) ? this.text : getPath(); }
  public  void            setText(String text)                { this.text = text; }

  public  gnu.gcj.RawData    playable;
  public  gnu.gcj.RawData getPlayable()                       { return playable; }

  private boolean            prefetched;
  public  boolean          isPrefetched()                     { return prefetched; }

   // Methods

  public VoxwarePlayable copy() throws AudioAllocationException {
    return new VoxwareFile(this);
  }

  public boolean prefetch() throws FileNotFoundException, IOException {
    return prefetch(null);
  }

  public boolean prefetch(VoxwarePlayable container) throws FileNotFoundException, IOException {
    boolean success = false;
    if (container == null) container = this;
    if (exists() && canRead() && isFile()) {
      if (nativeLoaded) success = jniPrefetch(container.getPlayable());
      prefetched = true;
    }
    return(success);
  }

  public synchronized void deallocate() {
    if (nativeLoaded) jniFinalizer();
    prefetched = false;
  }

  protected synchronized void finalize() {
    if (prefetched) {
      if (nativeLoaded) jniFinalizer();
      prefetched = false;
    }
  }

  private void constructor() throws AudioAllocationException {
    try {
      if (!jniConstructor()) throw new AudioAllocationException("No Voxware Playables left");
    } catch (UnsatisfiedLinkError e) {
      nativeLoaded = false;
    }
  }

  private native boolean jniConstructor();
  private native void    jniFinalizer();
  private native boolean jniPrefetch(gnu.gcj.RawData playable);
}
