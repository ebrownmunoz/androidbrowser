package voxware.engine.audioplayer;

import java.util.Enumeration;
import java.util.Vector;
import java.lang.Thread;
import java.io.*;

import org.mozilla.javascript.Context;

import voxware.util.*;

class MsgThread extends Thread {

  Mailbox mbox;

  MsgThread(String name) {
    super(name);
    this.mbox = new Mailbox();
    this.setDaemon(true);
  }

  public void putMsg(Object obj) {
    mbox.insert(obj);
  }

  public void putVector(Vector v) {
    if (v != null) {
      int cnt = v.size();
      for (int i = 0; i < cnt; i++) {
        putMsg(v.elementAt(i));
      }
    }
  }

  public Object getMsg() throws InterruptedException {
    return mbox.remove();
  }

  public Object getMsg(long timeout) throws InterruptedException {
    return mbox.remove(timeout);
  }

  public int msgCnt() {
    return mbox.numobj();
  }

  public Vector flush() {
    return mbox.flush();
  }
}

class PrefetchThread extends MsgThread {

  AsyncAudioPlayer parent;

  PrefetchThread(AsyncAudioPlayer parent) {
    super("AudioPrefetch");
    this.parent = parent;
  }

  public void run() {
    try {
      VoxwarePlayable original = null;
      VoxwarePlayable vp = null;
      Object message;
      long wait = 0L;   // First wait is always forever
      while (true) {

        message = getMsg(wait);   // Throws InterruptedException

        if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
            SysLog.println("PrefetchThread.run: received " + message + " at time " + System.currentTimeMillis() + "; wait = " + wait + " msec");

         // Synchronize with cancel()
        if (message == parent.cancelFlag) {
          parent.cancelFlag.lower();
          if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
            SysLog.println("PrefetchThread.run: synchronized with cancel()");
          wait = 0L;    // Wait forever after cancellation
          continue;
        }
        wait = parent.interval;

        if (message != null) {
          vp = (VoxwarePlayable) message;
          if (vp instanceof VoxwareEmptyPlayable) {
            wait = 0L;                  // No sense in repeating a VoxwareEmptyPlayable
            original = null;
          } else if (wait > 0L) {
            original = vp.copy();       // Save an un-prefetched copy for potential repeat
            original.setListener(null); // Report completion of at most only the first playback
          } else {
            original = null;
          }
        } else if (original != null) {
          vp = original.copy();         // Play a copy of the VoxwarePlayable previously played
        } else {
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
              SysLog.println("PrefetchThread.run: continuing after receiving null message; wait = " + wait + " msec");
          wait = 0L;
          continue;
        }

         // We expect vp NOT to be prefetched, but deal with it even if it is
        if (vp.isPrefetched() == false) {
          try {
            if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
              SysLog.println("PrefetchThread.run: prefetching VoxwarePlayable \"" + vp.getText() + "\" at time: " + System.currentTimeMillis());
            if (!vp.prefetch())
              SysLog.println("PrefetchThread.run: WARNING - could not completely prefetch \"" + vp.getText() + "\"");
          } catch (FileNotFoundException e) {
            SysLog.println(e.toString());
          } catch(IOException e) {
            SysLog.println(e.toString());
          }
        }

        synchronized(parent.cancelFlag) {
          if (parent.cancelFlag.raised() || !vp.isPrefetched()) {
            if (!vp.isPrefetched())
              SysLog.println("PrefetchThread.run: WARNING - failed to prefetch \"" + vp.getText() + "\"");
            if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
              SysLog.println("PrefetchThread.run: deallocating VoxwarePlayable \"" + vp.getText() + "\"");
            vp.deallocate();
          } else {
            if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
              SysLog.println("PrefetchThread.run: sending VoxwarePlayable \"" + vp.getText() + "\" to the PlayThread at time: " + System.currentTimeMillis());
            parent.playThread.putMsg(vp);
          }
        }
        yield();
      }
    } catch (InterruptedException e) {
       // Fall through to terminate run()
      SysLog.println("PrefetchThread.run: terminating due to " + e);
    } catch (Exception e) {
       // Fall through to terminate run()
      SysLog.println("PrefetchThread.run: terminating due to " + e);
    }
  }
}
  
class PlayThread extends MsgThread {

  AsyncAudioPlayer parent;

  PlayThread(AsyncAudioPlayer parent) {
    super("AudioPlay");
    this.parent = parent;
  }

  public void run() {
    setPriority(Thread.MAX_PRIORITY);
    boolean started = false;
    Context context = Context.enter();
    try {
      Object message;
      while (true) {

        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
          SysLog.println("PlayThread.run: awaiting playable at time: " + System.currentTimeMillis());
        message = getMsg();

        if (!started) {
          parent.audioPlayer.start();
          started = true;
        }

        VoxwarePlayable vp = (VoxwarePlayable) message;
        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
          SysLog.println("PlayThread.run: received VoxwarePlayable \"" + vp.getText() + "\" at time: " + System.currentTimeMillis());
        if (!parent.cancelFlag.raised() && !(vp instanceof VoxwareEmptyPlayable)) {
          try {
            parent.audioPlayer.playback(vp);
          } catch (VoxwareResourceUnavailable vru) {
            SysLog.println("PlayThread.run: playback produces VoxwareResourceUnavailable");
          }
        }

        if (!parent.cancelFlag.raised()) {
          AsyncListener listener = vp.getListener();
          if (listener != null)
            listener.finished();
        }

        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
          SysLog.println("PlayThread.run: finished playing VoxwarePlayable \"" + vp.getText() + "\" at time: " + System.currentTimeMillis());

        vp.deallocate();

        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
          SysLog.println("PlayThread.run: deallocation complete for VoxwarePlayable \"" + vp.getText() + "\" at time: " + System.currentTimeMillis());

        if (msgCnt() == 0) {
          parent.audioPlayer.stop();
          started = false;
        }
      }
    } catch (InterruptedException e) {
       // Fall through to terminate run()
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
        SysLog.println("PlayThread.run: terminating due to InterruptedException");
    } finally {
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
        SysLog.println("PlayThread.run: exiting Context at time: " + System.currentTimeMillis());
      Context.exit();
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
        SysLog.println("PlayThread.run: ALL DONE at time: " + System.currentTimeMillis());
    }
  }
}

class SyncListener implements AsyncListener {

  public boolean finished;

  public synchronized void finished() {
    finished = true;
    notify();
  }

  public void environment(Object env) {
  }

  public Object environment() {
    return null;
  }
}
    
public class AsyncAudioPlayer {

  public PrefetchThread prefetchThread;
  public PlayThread     playThread;
  public Flag           cancelFlag;
  public long           interval = 0L;
  public final VoxwareTextAudioPlayer audioPlayer;
  private final String name;
  
  public AsyncAudioPlayer(String name) {
    this(name, 0L);
  }

  public AsyncAudioPlayer(String name, long interval) {
    this.interval = interval;
    this.name = name;
    this.audioPlayer = new VoxwareTextAudioPlayer(name);
    
    cancelFlag = new Flag();
    prefetchThread = new PrefetchThread(this);
    playThread = new PlayThread(this);

    prefetchThread.start();
    playThread.start();
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
      SysLog.println("AsyncAudioPlayer: instantiated and started");
  }

  public synchronized void setInterval(long interval) {
    this.interval = interval;
  }

  public synchronized long getInterval() {
    return this.interval;
  }

  public synchronized void play(VoxwarePlayable vp) {
    prefetchThread.putMsg(vp);
  }

  public synchronized void play(VoxwarePlayable vp, AsyncListener listener) {
    vp.setListener(listener);
    prefetchThread.putMsg(vp);
  }

  public void playSync(VoxwarePlayable vp) throws InterruptedException {
    SyncListener listener = new SyncListener();
    listener.finished = false;
    play(vp, listener);
    synchronized(listener) {
      while (!listener.finished)
        listener.wait();
    }
  }

  public void cancel() {
    Vector notYetPrefetched;
    Vector notYetPlayed;

    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK)) {
      SysLog.println("AsyncAudioPlayer.cancel: CALLED");
    }

     // Synchronized to prevent prefetchThread.run() from sending a playable to the playThread after the playThread is flushed
    synchronized(cancelFlag) {
      cancelFlag.receiver(null, null);
      cancelFlag.raise();
      notYetPrefetched = prefetchThread.flush();
      notYetPlayed = playThread.flush();
    }

    if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
      SysLog.println("AsyncAudioPlayer.cancel: calling cancel()");

    audioPlayer.cancel();

    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
      SysLog.println("AsyncAudioPlayer.cancel: cancel() has returned");

    deallocate(notYetPrefetched);
    deallocate(notYetPlayed);

     // Synchronize with the prefetch thread
    cancelFlag.receiver(prefetchThread.mbox, cancelFlag);
    cancelFlag.raiseAndWait();

     // Must NOT similarly synchronize with play thread, as this causes a deadlock condition via the AsyncListener
  }

  private void deallocate(Vector playables) {
    if (playables != null) {
      Enumeration effluent = playables.elements();
      while (effluent.hasMoreElements()) {
        Object obj = effluent.nextElement();
        if (!(obj instanceof VoxwarePlayable)) {
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
            SysLog.println("AsyncAudioPlayer.deallocate: WARNING - cannot deallocate " + obj.getClass().getName() + " " + obj.toString());
        } else {
          VoxwarePlayable vp = (VoxwarePlayable) obj;
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
            SysLog.println("AsyncAudioPlayer.deallocate: deallocating VoxwarePlayable \"" + vp.getText() + "\"");
          vp.deallocate();
        }
      }
    }
  }
}
