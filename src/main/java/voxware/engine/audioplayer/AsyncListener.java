package voxware.engine.audioplayer;

public abstract interface AsyncListener {
  void   finished(); 
  void   environment(Object env); 
  Object environment(); 
}
