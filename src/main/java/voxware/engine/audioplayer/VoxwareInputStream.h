#ifndef __voxware_engine_audioplayer_VoxwareInputStream__
#define __voxware_engine_audioplayer_VoxwareInputStream__

#pragma interface

#include <java/io/FilterInputStream.h>

extern "Java"
{
  namespace gnu
  {
    namespace gcj
    {
      class RawData;
    }
  }
  namespace voxware
  {
    namespace engine
    {
      namespace audioplayer
      {
        class VoxwarePlayable;
        class VoxwareInputStream;
        class AsyncListener;
      }
    }
  }
}

class voxware::engine::audioplayer::VoxwareInputStream : public ::java::io::FilterInputStream
{
public:
  VoxwareInputStream ();
  VoxwareInputStream (::voxware::engine::audioplayer::VoxwareInputStream *);
  VoxwareInputStream (::java::io::InputStream *);
  VoxwareInputStream (::java::io::InputStream *, ::java::lang::String *);
  virtual ::voxware::engine::audioplayer::AsyncListener *getListener () { return listener; }
  virtual void setListener (::voxware::engine::audioplayer::AsyncListener *);
  virtual ::java::lang::String *getText () { return text; }
  virtual void setText (::java::lang::String *);
  virtual ::gnu::gcj::RawData *getPlayable () { return playable; }
  virtual jboolean isPrefetched () { return prefetched; }
  virtual ::voxware::engine::audioplayer::VoxwarePlayable *copy ();
  virtual jboolean prefetch ();
  virtual jboolean prefetch (::voxware::engine::audioplayer::VoxwarePlayable *);
  virtual void deallocate ();
public:  // actually protected
  virtual void finalize ();
  virtual void constructor ();
private:
  jboolean jniConstructor ();
  void jniFinalizer ();
  jboolean jniPrefetch (::gnu::gcj::RawData *);
  static jboolean nativeLoaded;
  ::voxware::engine::audioplayer::AsyncListener * __attribute__((aligned(__alignof__( ::java::io::FilterInputStream )))) listener;
  ::java::lang::String *text;
public:
  ::gnu::gcj::RawData *playable;
private:
  jboolean prefetched;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_engine_audioplayer_VoxwareInputStream__ */
