package voxware.engine.audioplayer;

import gnu.gcj.RawData;

import java.util.Vector;

import voxware.util.SysLog;

public class VoxwareTextAudioPlayer extends VoxwareAudioPlayer {
	
	private final String tag;

	public VoxwareTextAudioPlayer(String tag) {
		super();
		this.tag = tag;
	}

	public VoxwareTextAudioPlayer(Vector playables, String tag) {
		super(playables);
		this.tag = tag;
	}

	public VoxwareTextAudioPlayer(VoxwarePlayable vp, String tag) {
		super(vp);
		this.tag = tag;
	}

	protected void play(VoxwarePlayable vp) {
		SysLog.println("VoxwareAudioPlayer.play: playing <" + vp.getText() + "> ... ");
		if (nativeLoaded) {
			jniPlay(new RawData(vp.getText()));
		}
		if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
			SysLog.println(" done.");
	}
	
	  protected  void jniConstructor() {
		  // Do nothing
	  };
	  
	  protected  void jniFinalizer() {
		// Do nothing
	  }
	  protected  void jniOpen() throws VoxwareResourceUnavailable {
		// Do nothing
	  };
	  
	  protected  void jniClose() {
		  // Do nothing
	  };

	  protected  void jniCancel() {
		  // interrupt?
	  };
	  
	  protected  void jniStart() {
		  
	  };
	  
	  protected  void jniStop() {
		  
	  };
	  
	  protected  boolean jniSetGain(float gain) {
		  return true;
	  };
	  protected  float  jniGetGain() {
		  return 1f;
	  };

	  protected  void jniSetMute(boolean mute) {
		  
	  };
	  protected  boolean jniGetMute() {
		return true;  
	  };
	  
	  protected  void jniPlay(gnu.gcj.RawData playable) {
		  System.out.println( "Text Audio Out: " +  ((playable != null) ? playable.toString() : "null") );
	  };
}
