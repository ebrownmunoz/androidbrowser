package voxware.browser;

import java.util.*;
import org.mozilla.javascript.*;

import voxware.browser.*;

 // A singleton Converter for boolean designators
class BooleanConverter implements Converter {

  private static BooleanConverter converter = null;

  public static BooleanConverter instance() {
    if (converter == null) converter = new BooleanConverter();
    return converter;
  }

  public Object convert(Object value) throws VXMLEvent {
    if (value instanceof String)
       // Override ECMA, so that "false" returns false
      return new Boolean((String)value);
    else
      return new Boolean(ScriptRuntime.toBoolean(value));
  }
}
