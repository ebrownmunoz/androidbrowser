package voxware.browser;

import java.util.Enumeration;
import java.util.Hashtable;

import voxware.browser.*;

 /** All of the event Catchers for a given scope
   * This is a Hashtable of Catchers objects, hashed by event name */
class EventHandler extends Hashtable {

  public EventHandler() {
    super();
  }

  public EventHandler(int initialCapacity) {
    super(initialCapacity);
  }

   /** Reset all the event counts */
  public void resetEventCounts() {
    Enumeration handler = elements();
    while (handler.hasMoreElements())
      ((Catchers)handler.nextElement()).resetEventCount();
  }

   /** Make and return the Catchers for an event -- call this for every <throw> */
  public Catchers makeCatchers(String event) {
    Catchers catchers = (Catchers)super.get(event);
    if (catchers == null) {
      catchers = new Catchers();
      super.put(event, catchers);
    }
    return catchers;
  }

   /** Return the Catchers for an event, or null if there are none */
  public Catchers catchers(String event) {
    return (Catchers)super.get(event);
  }

   /** Put a Catcher into the EventHandler -- call this for every <catch> */
  public void put(Catcher catcher) {
    Enumeration siblings = catcher.siblings();
     // If this is a simple Catcher, make a Catchers object for it if none exists and put it in it
    if (siblings == null) {
      String event = catcher.event();
      Catchers catchers = makeCatchers(event);
      catchers.insert(catcher);
     // Otherwise, it is compound, so make a Catchers object for each sibling and put the sibling in it
    } else {
      while (siblings.hasMoreElements()) {
        Catchers sibling = makeCatchers((String)siblings.nextElement());
        sibling.insert(catcher);
      }
    }
  }

   /** Get the appropriate Catcher for an event, or null if no Catchers exist for it */
  public Catcher catcher(String event, ScopeElement scope) throws VXMLEvent {
    Catchers catchers = (Catchers)super.get(event);
    return (catchers != null) ? catchers.catcher(scope) : null;
  }

   /** Get the appropriate Catcher for an event with the given event count, or null if no Catchers exist for it */
  public Catcher catcher(String event, int eventCount, ScopeElement scope) throws VXMLEvent {
    Catchers catchers = (Catchers)super.get(event);
    return (catchers != null) ? catchers.catcher(eventCount, scope) : null;
  }

   /** Update the sibling event counts */
  public void updateSiblings(Catcher catcher, int eventCount) {
    Enumeration siblings = catcher.siblings();
    if (siblings != null) {
      while (siblings.hasMoreElements()) {
        Catchers sibling = makeCatchers((String)siblings.nextElement());
        if (sibling.eventCount() < eventCount) sibling.eventCount(eventCount);
      }
    }
  }
} // class EventHandler
