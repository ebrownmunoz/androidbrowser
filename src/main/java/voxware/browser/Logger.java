package voxware.browser;

import java.util.BitSet;
import java.util.Hashtable;
import java.util.StringTokenizer;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

 /** A Logger */
public class Logger extends Cardinal {

   // Names of built-in types of loggers
  public static final String STDOUT = "stdout";
  public static final String STDERR = "stderr";
  public static final String SYSLOG = "syslog";
  public static final String SYSMON = "sysmon";
  public static final String APPSRV = "appsrv";

   // Synonyms to match interrupt source names
  public static final String SYSMON_SYNONYM = "sysmonitor";
  public static final String APPSRV_SYNONYM = "appserver";

   // Built-in types of loggers
  private static class SysLogDevice implements SystemMonitor {
    public void logEvent(int index, String text) { SysLog.println(text); }
  }
  private static class StdOutDevice implements SystemMonitor {
    public void logEvent(int index, String text) { System.out.println(text); }
  }
  private static class StdErrDevice implements SystemMonitor {
    public void logEvent(int index, String text) { System.err.println(text); }
  }
  private static class DefaultSysMonitor implements SystemMonitor {
    public void logEvent(int index, String text) { System.out.println("DefaultSysMonitor.logEvent(" + index + ", \"" + text + "\")"); }
  }
  private static class DefaultAppServer implements SystemMonitor {
    public void logEvent(int index, String text) { System.out.println("DefaultAppServer.logEvent(" + index + ", \"" + text + "\")"); }
  }

  private static class SysMonitorClone implements SystemMonitor {
    public void logEvent(int index, String text) {
      SystemMonitor monitor = getSysMonitor();
      if (monitor != null) monitor.logEvent(index, text);
      else new DefaultSysMonitor().logEvent(index, text);
    }
  }
  private static class AppServerClone implements SystemMonitor {
    public void logEvent(int index, String text) {
      SystemMonitor monitor = getAppServer();
      if (monitor != null) monitor.logEvent(index, text);
      else new DefaultAppServer().logEvent(index, text);
    }
  }

  private static class LoggerWrapper {
    private SystemMonitor monitor;
    private LoggerWrapper(SystemMonitor mon) { monitor = mon; }
    private void setMonitor(SystemMonitor mon) { monitor = mon; }
  }

  private static LoggerWrapper   sysMonitor = null;
  public static void          setSysMonitor(SystemMonitor monitor) { sysMonitor = addTypeInternal(SYSMON, monitor); }
  public static SystemMonitor getSysMonitor()                      { return (sysMonitor != null) ? sysMonitor.monitor : null; }

  private static LoggerWrapper    appServer = null;
  public static void           setAppServer(SystemMonitor monitor) { appServer = addTypeInternal(APPSRV, monitor); }
  public static SystemMonitor  getAppServer()                      { return (appServer != null) ? appServer.monitor : null; }

   // The hashtable of types of loggers, hashed by name
  private static Hashtable loggers = new Hashtable();
  static {
    addTypeInternal(STDOUT, new StdOutDevice());
    addTypeInternal(STDERR, new StdErrDevice());
    addTypeInternal(SYSLOG, new SysLogDevice());
    setSysMonitor(new DefaultSysMonitor());
    setAppServer(new DefaultAppServer());
    addTypeInternal(SYSMON_SYNONYM, new SysMonitorClone());
    addTypeInternal(APPSRV_SYNONYM, new AppServerClone());
  }

   // Public method to add a new type of logger or replace the SystemMonitor that underlies an existing one
  public static void addType(String name, SystemMonitor device) {
    addTypeInternal(name, device);
  }

   // Add a new type of logger or replace the SystemMonitor that underlies an existing one, and return the LoggerWrapper
  private static LoggerWrapper addTypeInternal(String name, SystemMonitor device) {
    LoggerWrapper wrapper = (LoggerWrapper) loggers.get(name);
    if (wrapper == null) {
      wrapper = new LoggerWrapper(device);
      loggers.put(name, wrapper);
    } else {
      wrapper.setMonitor(device);
    }
    return wrapper;
  }

  private Element       logger;
  private Expr          cexpr;        // Expression to evaluate to determine whether or not to log (OPTIONAL)
  private boolean       cond = true;  // The value of the preceeding conditional expression
  private LoggerWrapper device;       // Where to log to (OPTIONAL)
  private BitSet        eventFlags;   // What events to log
  private String        label;        // String to log (OPTIONAL)
  private Expr          texpr;        // Expression to evaluate to a String to log (OPTIONAL)
  private String        exprText;     // The preprocessed String value of the preceding expression
  private String        childText;    // The preprocessed String value of the assembled children

   /** Construct a Logger from a <logger> element in the DOM tree */
  public Logger(Element logger, boolean executable) throws InvalidTag, DOMException, InvalidExpressionException, VXMLEvent {
    this.logger = logger;
    this.setIndex(Elements.LOG_INDEX);

     // Get the attributes of the <logger> tag
    NamedNodeMap nnm = logger.getAttributes();
    String deviceName = null;
    for (int j = 0; j < nnm.getLength(); j++) {
      Node node = nnm.item(j);
      String nodeName = node.getNodeName();
      if (nodeName.equalsIgnoreCase("cond"))
        cexpr = new Expr(node.getNodeValue(), null);      // Throws DOMException
      else if (nodeName.equalsIgnoreCase("expr"))
        texpr = new Expr(node.getNodeValue(), null);      // Throws DOMException
      else if (nodeName.equalsIgnoreCase("label"))
        label = node.getNodeValue();                      // Throws DOMException
      else if (nodeName.equalsIgnoreCase("events"))
        eventFlags = setEventFlags(node.getNodeValue());  // Throws DOMException
      else if (nodeName.equalsIgnoreCase("to"))
        deviceName = node.getNodeValue();                 // Throws DOMException
      else if (SysLog.printLevel > 0)
        SysLog.println("Logger.Logger(): WARNING - <log> has unexpected attribute node \"" + nodeName + "\"");
    }

     // The event set is FORBIDDEN in executable contexts and REQUIRED elsewhere
    if ((eventFlags != null) == executable) throw new InvalidTag(logger);

     // The logging device is OPTIONAL, defaulting to syslog
    device = (LoggerWrapper) ((deviceName == null) ? loggers.get(SYSLOG) : loggers.get(deviceName));
    if (device == null || device.monitor == null) {
      if (SysLog.printLevel > 0) SysLog.println("Logger.Logger(): ERROR - invalid logging device \"" + deviceName + "\"");
      throw new InvalidTag((Node)logger);
    }

     // If the conditional expression is constant, evaluate it now, once and for all
    try {
       // Throw UndeclaredVariableException if cexpr references any variables
      cond = shouldLogEvents(null);                       // Throws UndeclaredVariableException, InvalidExpressionException, VXMLEvent
       // Don't need cexpr anymore -- nullify it to show that expression has been evaluated
      cexpr = null;
     // Otherwise, evaluate it during interpretation
    } catch (UndeclaredVariableException e) {
    }

     // The text expression is OPTIONAL, defaulting to null
     // If it is constant, evaluate it now, once and for all
    try {
       // Throw UndeclaredVariableException if texpr references any variables
      exprText = exprText(null);                          // Throws UndeclaredVariableException, InvalidExpressionException, VXMLEvent
     // Otherwise, evaluate it during interpretation
    } catch (UndeclaredVariableException e) {
    }

     // If the children evaluate to a constant text string, do it now
    try {
       // Throw UndeclaredVariableException if the children reference any variables
      childText = childText(null);
     // Otherwise, evaluate it during interpretation
    } catch (UndeclaredVariableException e) {
    }
  }

   // Public methods

   // Determine whether to log the indexed event
  public boolean shouldLogEvent(int eventIndex, Scope scope) throws VXMLEvent {
    if (eventFlags.get(eventIndex)) {
      try {
        return shouldLogEvents(scope);                    // Throws UndeclaredVariableException, InvalidExpressionException, VXMLEvent
      } catch (UndeclaredVariableException e) {
        throw new VXMLEvent("error.semantic.undeclared", e);
      } catch (InvalidExpressionException e) {
        throw new VXMLEvent("error.syntactic.badexpression", e);
      }
    } else {
      return false;
    }
  }

   // Determine whether to log the named event
  public boolean shouldLogEvent(String eventName, Scope scope) throws VXMLEvent {
    return shouldLogEvent(SystemEvent.index(eventName), scope);
  }

   // Log the indexed event with a text string
  public void logEvent(int eventIndex, Scope scope, String message) throws VXMLEvent {
    String logName = SystemEvent.logName(eventIndex);
    if (exprText == null || childText == null) {
      Scriptable logscope = scope.scope();
       // Fill in a shadow Scriptable named "log$" detailing the logging event, creating it if necessary:
       //   log$.name is the name of the event to be logged;
       //   log$.text is the log String passed to this method;
      Object value = logscope.get("log$", logscope);
      Scriptable shadow;
      if (value != Scriptable.NOT_FOUND && value instanceof Scriptable) {
        shadow = (Scriptable)value;
      } else {
        shadow = scope.newObject();
        logscope.put("log$", logscope, shadow);
      }
       // Put the log name into the shadow
      shadow.put("name", shadow, logName != null ? (Object)logName : VXMLTypes.PROTO_UNDEFINED);
      shadow.put("text", shadow, message != null ? (Object)message : VXMLTypes.PROTO_UNDEFINED);
    }

     // Try to construct a logString from attributes and children
    StringBuffer logString = (label == null) ? new StringBuffer() : new StringBuffer(label);
    try {
       // Throws UndeclaredVariableException, InvalidExpressionException, VXMLEvent
      logString.append(exprText != null ? exprText : exprText(scope)).append(childText != null ? childText : childText(scope));
    } catch (UndeclaredVariableException e) {
      throw new VXMLEvent("error.semantic.undeclared", e);
    } catch (InvalidExpressionException e) {
      throw new VXMLEvent("error.syntactic.badexpression", e);
    }

     // Default if logString is empty
    if (logString.length() == 0) {
      if (logName != null) logString.append(logName);
      if (message != null && message.length() > 0) {
        if (logString.length() > 0) logString.append('=');
        logString.append('\'').append(message).append('\'');
      }
    }

     // Log the resulting String
    device.monitor.logEvent(eventIndex, logString.toString());
  }

   // Log the indexed event with an integer value
  public void logEvent(int eventIndex, Scope scope, int value) throws VXMLEvent {
    logEvent(eventIndex, scope, Integer.toString(value));
  }

   // Log the indexed event
  public void logEvent(int eventIndex, Scope scope) throws VXMLEvent {
    logEvent(eventIndex, scope, null);
  }

   // Log a null event (for logging text in executable context)
  public void logEvent(Scope scope) throws VXMLEvent {
    try {
      if (shouldLogEvents(scope)) logEvent(SystemEvent.NULL_INDEX, scope, null);
    } catch (UndeclaredVariableException e) {
      throw new VXMLEvent("error.semantic.undeclared", e);
    } catch (InvalidExpressionException e) {
      throw new VXMLEvent("error.syntactic.badexpression", e);
    }
  }

   // Private methods

   // Set the eventFlags from a whitespace-delimited String of event names
  private BitSet setEventFlags(String events) {
    StringTokenizer tokens = new StringTokenizer(events);
    BitSet eventFlags = new BitSet(SystemEvent.NUM_SYSEVENTS);
    while (tokens.hasMoreTokens())
      eventFlags.set(SystemEvent.index(tokens.nextToken()));
    eventFlags.clear(SystemEvent.NULL_INDEX);
    return eventFlags;
  }

   // Evaluate the conditional expression in the given scope
  private boolean shouldLogEvents(Scope scope) throws UndeclaredVariableException, InvalidExpressionException, VXMLEvent {
    if (cexpr != null) cond = ScriptRuntime.toBoolean(cexpr.evaluate(scope));
    return cond;
  }

   // Evaluate the text expression in the given scope
  private String exprText(Scope scope) throws UndeclaredVariableException, InvalidExpressionException, VXMLEvent {
    return texpr != null ? ScriptRuntime.toString(texpr.evaluate(scope)) : "";
  }

   // Assemble the children of <log> into a text String to log
  private String childText(Scope scope) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {

    StringBuffer logText = new StringBuffer();
    NodeList     logChildren = logger.getChildNodes();

    String nodeName;
    for (int i = 0; i < logChildren.getLength(); i++) {
      Node childNode = logChildren.item(i);
      nodeName = childNode.getNodeName();
      if (nodeName.equalsIgnoreCase("#text")) {
        logText.append(childNode.getNodeValue());
      } else if (nodeName.equalsIgnoreCase("value")) {
        Object value = ValueNode.parse(childNode, scope);       // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
        if (value instanceof String && ((String)value).length() > 0)
          logText.append((String)value);
      } else if (SysLog.printLevel > 0) {
        SysLog.println("Logger.childText: WARNING - <log> has unexpected child node \"" + nodeName + "\"");
      }
    }

    return logText.toString();
  }
} // class Logger
