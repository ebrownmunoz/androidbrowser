package voxware.browser;

import java.util.*;

import voxware.util.SysLog;


 /** An AudioSegment */
class AudioSegment {

  // An AudioSegment embodies the audio data for a word or sequence of words intended for use in a particular context.
  // The left and right couplers are integers that define the context. For example, a segment with firstWord == "foo",
  // leftCoupler == 3, rightCoupler == 17, and subsequentWords == null, represents a version of the word "foo" intended
  // for use following any word whose right coupler is 3 and preceding any word whose left coupler is 17. Similarly,
  // one with subsequentWords == "bar" but otherwise the same represents a two-word sequence, "foo bar", intended for
  // use in the same context.

  public int    leftCoupler;      // The index of the left coupler
  public String firstWord;        // The first word in the segment
  public Vector subsequentWords;  // The sequence of words making up the rest of the segment, if any
  public int    rightCoupler;     // The index of the right coupler
  public byte[] audioData;        // The audio data for this word or sequence of words

  AudioSegment(int leftCoupler, int rightCoupler, byte[] audioData) {
    this.leftCoupler = leftCoupler;
    this.rightCoupler = rightCoupler;
    this.audioData = audioData;
  }

  Enumeration subsequentWords() {
    return (subsequentWords != null) ? subsequentWords.elements() : null;
  }

   // Return a concatenation of the constituent words separated by blanks
  String orthography() {
    StringBuffer orthography = new StringBuffer(firstWord);
    if (subsequentWords != null) {
      Enumeration theRest = subsequentWords.elements();
      while (theRest.hasMoreElements())
        orthography.append(" ").append(theRest.nextElement());
    }
    return orthography.toString();
  }
}
