package voxware.browser;

import java.util.ArrayList;
import java.util.Iterator;

import voxware.browser.*;
import voxware.util.SysLog;

 /** Interrupts **/
class Interrupts extends ArrayList {

  private ArrayList queued; // The vector of pending interrupts
  private int hardCount;    // The number of hard interrupts pending

  public Interrupts() {
    super();
    queued = new ArrayList();
    hardCount = 0;
  }

   // Queue the first interrupt whose template the event satisfies, if any, and return true iff there is such an interrupt
   // The InterruptHandler calls this to queue interrupts when interrupts are disabled
  public synchronized boolean recordEvent(SystemEvent event) {
    Iterator interrupts = this.iterator();
    while (interrupts.hasNext()) {
      Interrupt interrupt = (Interrupt)interrupts.next();
      if (interrupt.template != null || interrupt.getTemplate() != null) {
        if (event.satisfies(interrupt.template)) {
          interrupt = (Interrupt) interrupt.clone();
          interrupt.template.setMessage(event.message());
           // Queue up the satisfied interrupt and return true
          queued.add(interrupt);
          if (interrupt.hard) hardCount++;
          return true;
        }
      } else if (SysLog.printLevel > 0) {
        SysLog.println("Interrupts.recordEvent: WARNING - could not lazily construct a template for source \"" + interrupt.getSource() + "\"");
      }
    }
    return false;
  }

   // If the event is not an interrupt, return null. Otherwise, if onlyIfHard and it's soft, queue it; else return it.
   // The InterruptHandler calls this when interrupts are enabled
  public synchronized Interrupt reportEvent(SystemEvent event, boolean onlyIfHard) {
    Iterator interrupts = this.iterator();
    while (interrupts.hasNext()) {
      Interrupt interrupt = (Interrupt)interrupts.next();
      if (interrupt.template != null || interrupt.getTemplate() != null) {
        if (event.satisfies(interrupt.template)) {
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.INTERRUPT_MASK))
            SysLog.println("Interrupts.reportEvent: " + SystemEvent.name(event.index) + " (" + event.index + ") interrupt satisfies " +
                            SystemEvent.name(interrupt.template.index) + " (" + interrupt.template.index + ") template");
          interrupt = (Interrupt) interrupt.clone();
          interrupt.template.setMessage(event.message());
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.INTERRUPT_MASK))
            SysLog.println("Interrupts.reportEvent: clone of " + SystemEvent.name(interrupt.template.index) + " (" + interrupt.template.index + ") interrupt is " +
                            (String)(!interrupt.hard ? "not" : "") + " hard");
           // If onlyIfHard and interrupt is not hard, queue it and return null
          if (onlyIfHard && !interrupt.hard) {
            queued.add(interrupt);
            return null;
           // Otherwise, return it
          } else {
            return interrupt;
          }
        } else if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.INTERRUPT_MASK)) {
          SysLog.println("Interrupts.reportEvent: " + SystemEvent.name(event.index) + " (" + event.index + ") interrupt does not satisfy " +
                          SystemEvent.name(interrupt.template.index) + " (" + interrupt.template.index + ") template");
        }
      } else if (SysLog.printLevel > 0)
        SysLog.println("Interrupts.reportEvent: WARNING - could not lazily construct a template for source \"" + interrupt.getSource() + "\"");
    }
    return null;
  }

   // Dequeue and return the oldest queued interrupt -- hard if onlyIfHard, any otherwise, null if none queued
   // The InterruptHandler calls this when enabling interrupts, to get any queued while interrupts have been disabled
  public synchronized Interrupt oldestEvent(boolean onlyIfHard) {
    Interrupt interrupt = null;
    if (queued.size() != 0) {
      if (onlyIfHard) {
        if (hardCount != 0) {
          Iterator interrupts = queued.iterator();
          while (interrupts.hasNext()) {
            Interrupt thisInterrupt = (Interrupt)interrupts.next();
            if (thisInterrupt.hard) {
              interrupt = thisInterrupt;
              interrupts.remove();
              break;
            }
          }
        }
      } else {
        interrupt = (Interrupt)queued.get(0);
        queued.remove(0);
      }
    }
    return interrupt;
  }

   // Discard all pending interrupts
  public synchronized void clear() {
    queued.clear();
  }

   // Insert an Interrupt
  public synchronized void insertEvent(Interrupt event) {
    this.add(event);
  }

   // Remove an Interrupt
  public synchronized void removeEvent(Interrupt event) {
    this.remove(indexOf(event));
  }
}
