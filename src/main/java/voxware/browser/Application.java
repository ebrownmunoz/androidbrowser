package voxware.browser;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

 /** An Application
   * An Application's parent is a Session and its child is a Page */
class Application extends Page {

  private boolean  initialized = false;  // Application.interpret() will initialize the application variables

   /** Construct a childless Application from the <vxml> element in a DOM tree */
  public Application(Element page) throws DOMException {
    super(page);
    prefix = "application";

    makeVariable("lastresult$", null, new Expr(VXMLTypes.PROTO_UNDEFINED));

    handler = new EventHandler();
  }

   /** Construct a childless Application */
  public Application() throws DOMException {
    this(null);
  }

   /** Interpret preprocessed Application */
  public Variables interpret(Goto whereToGo, InterruptHandler interruptHandler) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.INTERPRET_MASK))
      SysLog.println("Application.interpret: starting to interpret \"" + this.rootURL + "\"");
    try {
      interruptHandler.pushScope(this);
      modifyProperties();
      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.enter(page, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request entering root <page> " + name);
      if (!initialized) {
        this.initializeVariables();  // Throws VXMLEvent
        this.executeScript();        // Throws VXMLEvent
        initialized = true;
      } else {
        SysLog.println("Application.interpret: UNEXPECTED re-initialization");
      }

      while (whereToGo.page() != null) {
        try {
          try {
            ((Page)child).interpret(whereToGo, interruptHandler);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
          } catch (VXMLEvent event) {
             // Catch a VXML event in Application scope
            if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.CATCH_MASK))
              SysLog.println("Application.interpret: caught VXMLEvent \"" + event.name() + "\"");
            catchEvent(event, whereToGo);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
            throw new VXMLExit("Application-level catcher for event \'" + event.name() + "\' falls through");
          }
        } catch (Goto gt) {
           // If it's not a page level Goto, just re-throw it
          if (!gt.isPageLevel()) throw gt;
          whereToGo = gt;
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK))
            SysLog.println("Application.interpret: Page level Goto caught for \"" + whereToGo.next() + "\"");
        }
      }
    } finally {
      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.leave(page, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request leaving root <page> " + name);
      restoreProperties();
      interruptHandler.popScope();
    }
    return null;
  }

} // class Application
