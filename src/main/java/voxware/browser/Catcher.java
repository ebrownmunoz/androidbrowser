package voxware.browser;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

 /** A VXML Event Catcher for a given event and event count */
class Catcher extends ScopeElement implements Count, Executables {

   // A Hashtable of the full names of events having nicknames
  private static Hashtable fullName = new Hashtable();
  static {
    fullName.put("badfetch",       "error.badfetch");
    fullName.put("badnext",        "error.badfetch");     // DEPRECATED
    fullName.put("error.badnext",  "error.badfetch");     // DEPRECATED
    fullName.put("badurl",         "error.badfetch");     // DEPRECATED
    fullName.put("badurlload",     "error.badfetch");     // DEPRECATED
    fullName.put("cancel",         "event.cancel");
    fullName.put("help",           "event.help");
    fullName.put("noinput",        "event.noinput");
    fullName.put("nomatch",        "event.nomatch");
  }

   // Return the full name of an event
  static String fullName(String event) {
    String result = (String)fullName.get(event);
    if (result == null) result = event;
    return result;
  }

  private   Element  catcher;        // The <catch> element in the DOM tree
  protected String   event;          // The name of the event or events to catch
  protected Vector   siblings;       // A list of the events to be caught by this catcher, if more than one
  protected Expr     condition;
  protected int      count;          // The event count
  protected final Session session;

   /** Construct a Catcher */
  public Catcher(Session session) {
    super();
    this.session = session;
    prefix = "_anonymous";
  }

   /** Construct a Catcher from a <catch> element in the DOM tree */
  public Catcher(Session session, Element catcher, Goto context, boolean preprocess)
  throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException, VXMLEvent {
    this(session, catcher, context, null, preprocess);  // Throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException, VXMLEvent
  }

   /** Construct a Catcher from a <catch> element in the DOM tree and an event name */
  public Catcher(Session session, Element catcher, Goto context, String event, boolean preprocess)
  throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException, VXMLEvent {

    this(session);
    this.catcher = catcher;

     /* Get the event count (OPTIONAL, defaults to 1) */
    Attr attr = catcher.getAttributeNode("count");
    count = (attr != null) ? (new Integer(attr.getNodeValue())).intValue() : 1;  // Throws DOMException and NumberFormatException
     /* Get the condition (OPTIONAL, defaults to true) */
    attr = catcher.getAttributeNode("cond");
    condition = (attr != null) ? new Expr(attr.getNodeValue(), null) : new Expr(Boolean.TRUE); // Throws DOMException and InvalidExpressionException

     /* Get the event name (REQUIRED if event is null) */
    if (event != null) {
      this.event = event;
    } else {
      attr = catcher.getAttributeNode("event");
      if (attr != null) this.event = attr.getNodeValue();  // Throws DOMException
      else throw new InvalidTag((Node)catcher);            // Throws InvalidTag
    }

    expandEventName();

     // Construct the ExecutableContent
    executableContent = (preprocess) ? new ExecutableContent(session, catcher, context) : new ExecutableContent(session, context);  // Throws VXMLEvent
  }

   /** Expand the event name, constructing a Vector of siblings if warranted */
  protected void expandEventName() {
     // If the event is compound, construct a Vector of the full constituent sibling event names
    StringTokenizer tokenizer = new StringTokenizer(this.event);
    int tokenCount = tokenizer.countTokens();
    if (tokenCount > 1) {
      siblings = new Vector(tokenCount);
      while (tokenizer.hasMoreTokens())
        siblings.addElement(fullName(tokenizer.nextToken()));
     // Otherwise, just get the full event name
    } else {
      this.event = fullName(this.event);
      siblings = null;
    }
  }

   /** Make this a child of a ScopeElement (extends makeChildOf() in ScopeElement to inherit the parent's EventHandler) */
  public void makeChildOf(ScopeElement parent) {
     // Inherit the parent's EventHandler
    handler = parent.handler();
     // Make this a child of the parent witbout having the parent adopt it
    try {
      super.makeChildOf(parent, false);  // Throws VXMLEvent
    } catch (VXMLEvent e) {
      throw new RuntimeException("Catcher.makeChildOf(): UNEXPECTED " + e.toString());
    }
  }

   /** Implement the Count interface */
  public int count() {
    return count;
  }
  public void set(int count) {
    this.count = count;
  }

   /** Implement the Executables interface */
  private ExecutableContent executableContent;
  public ExecutableContent executableContent() {
    return executableContent;
  }

   /** Get the event name */
  public String event() {
    return event;
  }

   /** Return the boolean value of the cond attribute */
  public boolean evaluateCondition(ScopeElement scope) throws UndeclaredVariableException, InvalidExpressionException, VXMLEvent {
    return ScriptRuntime.toBoolean(condition.evaluate(scope));
  }

   /** Return an Enumeration of the sibling event names */
  public Enumeration siblings() {
    return (siblings != null) ? siblings.elements() : null;
  }

   /** Execute the "executable content" of the <catch> */
  public Variables interpret(ScopeElement scope, Goto context) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
    Variables variables = null;
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.CATCH_MASK))
      SysLog.println("Catcher.interpret(): caught event \"" + event + "\" in scope \"" + scope.getClass().getName() + "\"");
     // Munches through the children of <catch>, as through any other "executable content"
    if (catcher != null) {
      initializeScope(scope);
      try {
        variables = executableContent.execute(catcher, this, context, true);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
      } finally {
        orphan();
      }
    }
    return variables;
  }
} // class Catcher
