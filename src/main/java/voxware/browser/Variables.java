package voxware.browser;

import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.HashVector;

 /** All of the variables in a given scope -- a HashVector of Variable objects, hashed by name */
@SuppressWarnings("serial")
public class Variables extends LinkedHashMap<String, Variable> {

   // Static Methods

   /* Return a scriptable containing each property named in the given whitespace-delimited list */
  public static Scriptable setProp(String names, Scope scope) throws UndeclaredVariableException{
    return setProp(VXMLTypes.PROTO_UNDEFINED, names, scope);  // Throws UndeclaredVariableException
  }

   /* Put each property named in a whitespace-delimited list into the given scriptable and return it */
  public static Scriptable setProp(Scriptable scriptable, String names, Scope scope) throws UndeclaredVariableException {
    if (scriptable == VXMLTypes.PROTO_UNDEFINED) scriptable = (scope == null) ? null : scope.newObject();
    if (scope != null && names != null && names.length() > 0) {
      StringTokenizer st = new StringTokenizer(names);
      while (st.hasMoreTokens()) {
        String token = st.nextToken();
        Object value = scope.variable(token);  // Throws UndeclaredVariableException
        Scope.setProp(scriptable, token, value);
      }
    }
    return scriptable;
  }

   /* Return a scriptable containing each property named in the given arbitrarily delimited list */
  public static Scriptable setProp(String names, String delimiters, Scope scope) throws UndeclaredVariableException {
    return setProp(null, names, delimiters, scope);  // Throws UndeclaredVariableException
  }

   /* Put each property named in an arbitrarily delimited list into the given scriptable and return it */
  public static Scriptable setProp(Scriptable scriptable, String names, String delimiters, Scope scope) throws UndeclaredVariableException {
    if (scriptable == null) scriptable = (scope == null) ? null : scope.newObject();
    if (scope != null && names != null && names.length() > 0) {
      StringTokenizer st = new StringTokenizer(names, delimiters);
      while (st.hasMoreTokens()) {
        String token = st.nextToken();
        Object value = scope.variable(token);  // Throws UndeclaredVariableException
        Scope.setProp(scriptable, token, value);
      }
    }
    return scriptable;
  }

   // Constructors

  public Variables() {
    super();
  }

  public Variables(int initialCapacity) {
    super(initialCapacity);
  }

   /* Construct a Variables containing an Undefined Variable with no initial value for each name in a whitespace-delimited list */
  public Variables(String names) {
    super();
    if (names != null && names.length() > 0) {
      StringTokenizer st = new StringTokenizer(names);
      while (st.hasMoreTokens()) {
        String token = st.nextToken();
        Variable variable = new Variable();
        super.put(token, variable);
      }
    }
  }

   /* Construct a Variables containing an Undefined Variable with no initial value for each name in an arbitrarily delimited list */
  public Variables(String names, String delimiters) {
    super();
    if (names != null && names.length() > 0) {
      StringTokenizer st = new StringTokenizer(names, delimiters);
      while (st.hasMoreTokens()) {
        String token = st.nextToken();
        Variable variable = new Variable();
        super.put(token, variable);
      }
    }
  }

   /* Construct a Variables from an arbitrarily delimited list of name=value pairs, interpreting value as a String */
  public Variables(String names, String delimiters, char equals, Scope scope)
  throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    this(names, delimiters, equals, scope, null, false);
  }

   /* Construct a Variables from an arbitrarily delimited list of name=value pairs, interpreting value as a String */
  public Variables(String names, String delimiters, char equals, Scope scope, String dfltName)
  throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    this(names, delimiters, equals, scope, dfltName, false);
  }

   /* Construct a Variables from an arbitrarily delimited list of name=value pairs, interpreting value as a String and optionally trimming it */
  public Variables(String names, String delimiters, char equals, Scope scope, String dfltName, boolean trim)
  throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    super();
    if (names != null && names.length() > 0) {
      StringTokenizer st = new StringTokenizer(names, delimiters);
      int index = 0;
      while (st.hasMoreTokens()) {
        String token = st.nextToken();
        int indexOfEquals = token.indexOf(equals);
        if (indexOfEquals >= 0 || dfltName != null) {
          Variable variable = new Variable(new Expr(trim ? token.substring(indexOfEquals + 1).trim() : token.substring(indexOfEquals + 1)));
          variable.initialize(scope);    // Throws InvalidExpressionException and UndeclaredVariableException, VXMLEvent
          if (indexOfEquals >= 0)
            super.put(token.substring(0, indexOfEquals), variable);  // Use the name if there is one
          else
            super.put(dfltName, variable);                           // Otherwise, use the default name
          index += (token.length() + 1);
        } else {
          throw new InvalidExpressionException(names, index);
        }
      }
    }
  }

   // Methods

   /* Join another Variables with this one */
  public void putVariables(Variables variables) {
    if (variables != null) {
       for (String name : variables.keySet()) {
           super.put(name, variables.getByName(name));
       }
    }
  }

   /* Return a Vector of the names of all the variables in Variables, NOT necessarily in the order of their insertion */
  public Set<String> names() {
    return Collections.unmodifiableSet(keySet()); // Changing this set would impact the Variables object
  }

   /* Insert the Variable and return true iff it is not already there */
  public boolean putBoolean(String name, Variable variable) {
    if (containsKey(name)) return false;
    super.put(name, variable);
    return true;
  }

   /* Get a Variable by name (overrides get() in HashVector */
  public Object get(String name) {
    return super.get(name);
  }

   /* Get a Variable by name */
  public Variable getByName(String name) {
    return (Variable)super.get(name);
  }

   /* Initialize all the Variables in a Variables, in the order of their insertion */
  public void initialize(Scope scope) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    for (Variable each : values()) {
    	each.initialize(scope);
    }
  }
}
