package voxware.browser;

import java.util.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

 /** A SubFormItem
   * A SubFormItem's parent is a Form */
class SubFormItem extends FormItem implements FieldItem {

  protected Scope   parameters;
  protected String  explicitName;                                  // The given name, or null if none
  protected int     caching = CachingConverter.NULL_INDEX;
  protected int     fetchhint = FetchHintConverter.NULL_INDEX;
  protected int     fetchtimeout = DurationConverter.UNSPECIFIED;  // Maximum time allowed to acquire the URL
  protected String  fetchaudio;

  public SubFormItem(Session session, Element element) throws DOMException, InvalidExpressionException, VXMLEvent {
    super(session, element);                // Throws DOMException, InvalidExpressionException

    parameters = new Scope();
    explicitName = name;

    NamedNodeMap map = element.getAttributes();
    for (int j = 0; j < map.getLength(); j++) {
      Node node = map.item(j);
      String nodeName = node.getNodeName();
      if (nodeName.equalsIgnoreCase("caching"))
        caching = ((Integer)CachingConverter.instance().convert(node.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
      else if (nodeName.equalsIgnoreCase("fetchhint"))
        fetchhint = ((Integer)FetchHintConverter.instance().convert(node.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
      else if (nodeName.equalsIgnoreCase("fetchtimeout"))
        fetchtimeout = ((Integer)DurationConverter.instance().convert(node.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
      else if (nodeName.equalsIgnoreCase("fetchaudio"))
        fetchaudio = node.getNodeValue();                          // Throws DOMException
    }
  }

   /** FieldItem Implementation */
  public String explicitName() {
    return explicitName;
  }

   // Configure the parameters object
  public void makeParameters(Scope parent) {
    parameters.makeChildOf(parent);
  }

   // Make a parameter with an initializing expression
  public void makeParameter(String name, Expr initializer) {
    parameters.makeVariable(name, VXMLTypes.PROTO_UNDEFINED, initializer);
  }

   // Make a READONLY parameter with no initializing expression
  public void makeParameter(String name, Object value) {
    parameters.makeVariable(name, value);
  }

   // Return the parameters as a ScriptableObject
  public ScriptableObject parameters() {
    return parameters.scope();
  }
}
