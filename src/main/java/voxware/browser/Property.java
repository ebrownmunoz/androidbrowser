package voxware.browser;

import java.util.*;
import java.lang.reflect.*;
import java.net.MalformedURLException;
import java.net.URL;

import voxware.browser.speech.PropertyVetoException;
import voxware.engine.recognition.sapivise.*;
import voxware.engine.audioplayer.*;

import org.mozilla.javascript.*;

import voxware.util.PCFG;         // To get PCFG.decodeLong() for Java 1.1
import voxware.util.SysLog;

public abstract class Property {

   // These are defined here because there are no such definitions in ViseProperties
  public static final String VISE_NHYPOTHESES_NAME = "vise.numhypotheses";
  public static final String VISE_WCTHRESHOLD_NAME = "vise.wildcard.threshold";
  public static final String VISE_ABSWILDCARD_NAME = "vise.wildcard.absolute";
  public static final String VISE_RELWILDCARD_NAME = "vise.wildcard.relative";
  public static final String VISE_SAMPSIGBITS_NAME = "vise.fe.samplesigbits";
  public static final String VISE_NOISETRKING_NAME = "vise.fe.noisetracking";
  public static final String VISE_JINCLASSIZE_NAME = "vise.fe.jinclassize";
  public static final String VISE_MAXUNPRFRMS_NAME = "vise.maxunprocessedframes";
  public static final String VISE_UNPRQUANTUM_NAME = "vise.unprocessedquantum";
  public static final String VISE_QUEUELENGTH_NAME = "vise.queue.length";
  public static final String VISE_QUEUEFWDTOL_NAME = "vise.queue.fwdtol";
  public static final String VISE_QUEUEBWDTOL_NAME = "vise.queue.bwdtol";
  public static final String VISE_BOUPADLNGTH_NAME = "vise.boupadlength";
  public static final String VISE_EOUPADLNGTH_NAME = "vise.eoupadlength";
  public static final String VISE_FRAMEIDLEMS_NAME = "vise.frameidlems";
  public static final String VISE_PRINTFRAMES_NAME = "vise.printframes";
  public static final String VISE_PRINTSTATUS_NAME = "vise.printstatus";
  public static final String VISE_MUTHATHRESH_NAME = "vise.threshold.mutha";
  public static final String VISE_LOOSETHRESH_NAME = "vise.threshold.loose";
  public static final String VISE_TIGHTTHRESH_NAME = "vise.threshold.tight";
  public static final String VISE_BATCHWEIGHT_NAME = "vise.training.batchweight";   // Weight of old data (as a percentage); default is 0
  public static final String VISE_DFLTTRCOUNT_NAME = "vise.training.defaultcount";
  public static final String VISE_DFLTTRSCORE_NAME = "vise.training.defaultscore";
  public static final String VISE_TRAINWEIGHT_NAME = "vise.training.phraseweight";  // The number of equivalent frames of old data; default is 6
  public static final String VISE_ENRINIWCABS_NAME = "vise.enrollment.iniwcabs";
  public static final String VISE_ENRINIWCREL_NAME = "vise.enrollment.iniwcrel";
  public static final String VISE_ENRINICOUNT_NAME = "vise.enrollment.inicount";
  public static final String VISE_ENRBODWCABS_NAME = "vise.enrollment.bodwcabs";
  public static final String VISE_ENRBODWCREL_NAME = "vise.enrollment.bodwcrel";
  public static final String VISE_ENRBODMINDW_NAME = "vise.enrollment.bodmindw";
  public static final String VISE_ENRBODMAXDW_NAME = "vise.enrollment.bodmaxdw";
  public static final String VISE_ENRMAXKERNL_NAME = "vise.enrollment.maxkernels";
  public static final String VISE_ENRMINKERNL_NAME = "vise.enrollment.minkernels";
  public static final String VISE_ENRMAXDWELL_NAME = "vise.enrollment.maxdwell";
  public static final String VISE_ENRMINDWELL_NAME = "vise.enrollment.mindwell";
  public static final String VISE_ENRTOLERNCE_NAME = "vise.enrollment.tolerance";
  public static final String VISE_CALIBLENGTH_NAME = "vise.calibration.length";
  public static final String VISE_CALIBWEIGHT_NAME = "vise.calibration.weight";
  public static final String VISE_CALIBRWCREL_NAME = "vise.calibration.wcrelative";
  public static final String VISE_CALIBRWCABS_NAME = "vise.calibration.wcabsolute";
  public static final String VISE_VUONOFFSWCH_NAME = "vise.vu.onoffswitch";
  public static final String VISE_VUSCALEMULT_NAME = "vise.vu.scalemultiplier";
  public static final String VISE_VUOFFSETADD_NAME = "vise.vu.offset";
  public static final String VISE_SILMAXDWELL_NAME = "vise.silence.maxdwell";
  public static final String VISE_ENDMINDWELL_NAME = "vise.silence.end.mindwell";
  public static final String VISE_SILADAPTWGT_NAME = "vise.silence.adaptation.weight";
  public static final String VISE_SILADAPTLAG_NAME = "vise.silence.adaptation.lag";
  public static final String VISE_MAXDURATION_NAME = "vise.maxduration";
  public static final String VISE_MAXFRAMECNT_NAME = "vise.maxframecount";
  public static final String VISE_SPEAKERPATH_NAME = "vise.speaker.path";
  public static final String VISE_DFLTSPEAKER_NAME = "vise.speaker.default";
  public static final String VISE_AUDQUEUELEN_NAME = "vise.audiodata.maxduration";
  public static final String VISE_BOUSILMSECS_NAME = "vise.audiodata.bousilmsecs";
  public static final String VISE_EOUSILMSECS_NAME = "vise.audiodata.eousilmsecs";
  public static final String VISE_COMPLETETO_NAME  = "vise.timeout.complete";
  public static final String VISE_INCOMPLTTO_NAME  = "vise.timeout.incomplete";
   // FlexVISE only
  public static final String VISE_SHOWFRMDATA_NAME = "vise.frames.showdata";
  public static final String VISE_STARTSHOWFR_NAME = "vise.frames.startshow";
  public static final String VISE_FINALSHOWFR_NAME = "vise.frames.stopshow";
  public static final String VISE_SHOWALLTBKS_NAME = "vise.traceback.showall";
  public static final String VISE_SHOWFINALTB_NAME = "vise.traceback.showfinal";
  public static final String VISE_SHOWTBSTRNG_NAME = "vise.traceback.showstrings";
  public static final String VISE_MAXNODDWELL_NAME = "vise.maxnodedwell";
  public static final String VISE_VSBEAMWIDTH_NAME = "vise.beamwidth.search";
  public static final String VISE_NPBEAMWIDTH_NAME = "vise.beamwidth.newphone";
  public static final String VISE_NWBEAMWIDTH_NAME = "vise.beamwidth.newword";
  public static final String VISE_CHANSPERFRM_NAME = "vise.channelsperframelimit";
  public static final String VISE_USEPROFILER_NAME = "vise.profilesearch";
  public static final String VISE_SHOWCEPDATA_NAME = "vise.frames.showcepstrum";
  public static final String VISE_SHOWFEATDAT_NAME = "vise.frames.showfeatures";
  public static final String VISE_SHOWVQFRAME_NAME = "vise.frames.showvq";
  public static final String VISE_SHOWEPFRAME_NAME = "vise.frames.showep";
  private static final int NUM_RECOGNIZER_PROPERTIES = 72;
   // Read-only properties
  public static final String VISE_FRONTENDVER_NAME = "vise.fe.version";
  public static final String VISE_TRAINSCHEME_NAME = "vise.training.scheme";

   // TTS Properties
  private static final int TTS_NULL_INDEX = 0;
  public static final int    TTS_SPEED_INDEX    = TTS_NULL_INDEX + 1;
  public static final String TTS_SPEED_NAME     = "tts.speed";
  public static final int    TTS_PITCH_INDEX    = TTS_SPEED_INDEX + 1;
  public static final String TTS_PITCH_NAME     = "tts.pitch";
  public static final int    TTS_VOLUME_INDEX   = TTS_PITCH_INDEX + 1;
  public static final String TTS_VOLUME_NAME    = "tts.volume";
  public static final int    TTS_ENGINE_INDEX   = TTS_VOLUME_INDEX + 1;
  public static final String TTS_ENGINE_NAME    = "tts.engine";
  public static final int    TTS_LANGUAGE_INDEX = TTS_ENGINE_INDEX + 1;
  public static final String TTS_LANGUAGE_NAME  = "tts.language";
  public static final int    TTS_VOICE_INDEX    = TTS_LANGUAGE_INDEX + 1;
  public static final String TTS_VOICE_NAME     = "tts.voice";
  private static final int NUM_TTS_PROPERTIES = TTS_VOICE_INDEX;

   // Aberrant Properties (those that have no inherent indexing scheme)
  private static final int ABERRANT_NULL_INDEX = 0;
  public static final int    SYSTEM_PRINTLEVEL_INDEX = ABERRANT_NULL_INDEX + 1;
  public static final String SYSTEM_PRINTLEVEL_NAME  = "printlevel";
  public static final int    SYSTEM_PRINTMASK_INDEX  = SYSTEM_PRINTLEVEL_INDEX + 1;
  public static final String SYSTEM_PRINTMASK_NAME   = "printmask";
  public static final int    SYSTEM_URLROOT_INDEX    = SYSTEM_PRINTMASK_INDEX + 1;
  public static final String SYSTEM_URLROOT_NAME     = "urlroot";
  public static final int    VISE_INPUTLEVEL_INDEX   = SYSTEM_URLROOT_INDEX + 1;
  public static final String VISE_INPUTLEVEL_NAME    = "vise.inputlevel";
  public static final int    VISE_ACTIVESPKR_INDEX   = VISE_INPUTLEVEL_INDEX + 1;
  public static final String VISE_ACTIVESPKR_NAME    = "vise.activespeaker";
  public static final int    VISE_SAVERESULTS_INDEX  = VISE_ACTIVESPKR_INDEX + 1;
  public static final String VISE_SAVERESULTS_NAME   = "vise.saveresults";
  private static final int NUM_ABERRANT_PROPERTIES = VISE_SAVERESULTS_INDEX;

  private static final int NUM_SYSTEM_PROPERTIES = 6;
  private static final int NUM_PROPERTIES =
    NUM_SYSTEM_PROPERTIES + BrowserProperty.NUM_PROPERTIES + BrowserProperty.NUM_ALIASES + NUM_RECOGNIZER_PROPERTIES + NUM_TTS_PROPERTIES + NUM_ABERRANT_PROPERTIES;

   // A Hashtable of [property Constructor, property index, ... other property Constructor arguments ] n-tuples, hashed by property name
  private static Hashtable constructors = new Hashtable((NUM_PROPERTIES * 4) / 3 + 1);

  private static Class[] C2Signature = {Object.class, Integer.class};
  private static Class[] C3Signature = {Object.class, Integer.class, Converter.class};

   // Construct the Hashtable of arrays { constructor, index, ... other constructor arguments }
  static {
    try {
       // Construct entries for the system properties
      Object[] powerManagement =
        { SystemProperty.class.getConstructor(C3Signature), new Integer(SystemEvent.POWERMANAGEMENT_INDEX), BooleanConverter.instance() };
      constructors.put(SystemEvent.POWERMANAGEMENT_NAME, powerManagement);
      Object[] microphoneGain = { SystemProperty.class.getConstructor(C2Signature), new Integer(SystemEvent.MICROPHONEGAIN_INDEX) };
      constructors.put(SystemEvent.MICROPHONEGAIN_NAME, microphoneGain);
      Object[] radioStatusPingIP = { SystemProperty.class.getConstructor(C2Signature), new Integer(SystemEvent.RADIOSTATUSPINGIP_INDEX) };
      constructors.put(SystemEvent.RADIOSTATUSPINGIP_NAME, radioStatusPingIP);
      Object[] sidetoneGain = { SystemProperty.class.getConstructor(C2Signature), new Integer(SystemEvent.SIDETONEGAIN_INDEX) };
      constructors.put(SystemEvent.SIDETONEGAIN_NAME, sidetoneGain);
      Object[] speakerMute =
        { SystemProperty.class.getConstructor(C3Signature), new Integer(SystemEvent.SPEAKERMUTE_INDEX), BooleanConverter.instance() };
      constructors.put(SystemEvent.SPEAKERMUTE_NAME, speakerMute);
      Object[] speakerVolume = { SystemProperty.class.getConstructor(C2Signature), new Integer(SystemEvent.SPEAKERVOLUME_INDEX) };
      constructors.put(SystemEvent.SPEAKERVOLUME_NAME, speakerVolume);

       // Aberrant system properties
      Object[] printlevel = { AberrantProperty.class.getConstructor(C2Signature), new Integer(SYSTEM_PRINTLEVEL_INDEX) };
      constructors.put(SYSTEM_PRINTLEVEL_NAME, printlevel);
      Object[] printmask = { AberrantProperty.class.getConstructor(C2Signature), new Integer(SYSTEM_PRINTMASK_INDEX) };
      constructors.put(SYSTEM_PRINTMASK_NAME, printmask);
      Object[] urlroot = { AberrantProperty.class.getConstructor(C2Signature), new Integer(SYSTEM_URLROOT_INDEX) };
      constructors.put(SYSTEM_URLROOT_NAME, urlroot);

       // Construct entries for the browser properties
      /* System.out.println("Property.static: BrowserProperty.AUDIOFETCHHINT_INDEX = " + BrowserProperty.AUDIOFETCHHINT_INDEX +
         "; BrowserProperty.AUDIOFETCHHINT_NAME = " + BrowserProperty.AUDIOFETCHHINT_NAME); */
      Object[] audiofetchhint =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.AUDIOFETCHHINT_INDEX), FetchHintConverter.instance() };
      constructors.put(BrowserProperty.AUDIOFETCHHINT_NAME, audiofetchhint);
      Object[] bargein =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.BARGEIN_INDEX), BooleanConverter.instance() };
      constructors.put(BrowserProperty.BARGEIN_NAME, bargein);
      Object[] caching =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.CACHING_INDEX), CachingConverter.instance() };
      constructors.put(BrowserProperty.CACHING_NAME, caching);
      Object[] documentfetchhint =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.DOCUMENTFETCHHINT_INDEX), FetchHintConverter.instance() };
      constructors.put(BrowserProperty.DOCUMENTFETCHHINT_NAME, documentfetchhint);
      Object[] fetchaudio = { BrowserProperty.class.getConstructor(C2Signature), new Integer(BrowserProperty.FETCHAUDIO_INDEX) };
      constructors.put(BrowserProperty.FETCHAUDIO_NAME, fetchaudio);
      Object[] fetchtimeout =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.FETCHTIMEOUT_INDEX), DurationConverter.instance() };
      constructors.put(BrowserProperty.FETCHTIMEOUT_NAME, fetchtimeout);
      Object[] grammarfetchhint =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.GRAMMARFETCHHINT_INDEX), FetchHintConverter.instance() };
      constructors.put(BrowserProperty.GRAMMARFETCHHINT_NAME, grammarfetchhint);
      Object[] inputmodes = { BrowserProperty.class.getConstructor(C2Signature), new Integer(BrowserProperty.INPUTMODES_INDEX) };
      constructors.put(BrowserProperty.INPUTMODES_NAME, inputmodes);
      Object[] objectfetchhint =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.OBJECTFETCHHINT_INDEX), FetchHintConverter.instance() };
      constructors.put(BrowserProperty.OBJECTFETCHHINT_NAME, objectfetchhint);
      Object[] scriptfetchhint =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.SCRIPTFETCHHINT_INDEX), FetchHintConverter.instance() };
      constructors.put(BrowserProperty.SCRIPTFETCHHINT_NAME, scriptfetchhint);
      Object[] speakerfetchhint =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.SPEAKERFETCHHINT_INDEX), FetchHintConverter.instance() };
      constructors.put(BrowserProperty.SPEAKERFETCHHINT_NAME, speakerfetchhint);
      Object[] timeout =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.TIMEOUT_INDEX), DurationConverter.instance() };
      constructors.put(BrowserProperty.TIMEOUT_NAME, timeout);
      Object[] recursionlimit =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.EVENTRECURSIONLIMIT_INDEX), NumberConverter.instance() };
      constructors.put(BrowserProperty.EVENTRECURSIONLIMIT_NAME, recursionlimit);
      Object[] maxpercentused =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.JAVAMAXPERCENTUSED_INDEX), NumberConverter.instance() };
      constructors.put(BrowserProperty.JAVAMAXPERCENTUSED_NAME, maxpercentused);
      Object[] minbytesfree =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.MINBYTESFREE_INDEX), NumberConverter.instance() };
      constructors.put(BrowserProperty.MINBYTESFREE_NAME, minbytesfree);
      Object[] minmaxfreeblocksize =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.MINMAXFREEBLOCKSIZE_INDEX), NumberConverter.instance() };
      constructors.put(BrowserProperty.MINMAXFREEBLOCKSIZE_NAME, minmaxfreeblocksize);
      Object[] maxconnectretries =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.MAXCONNECTRETRIES_INDEX), NumberConverter.instance() };
      constructors.put(BrowserProperty.MAXCONNECTRETRIES_NAME, maxconnectretries);
      Object[] maxfetchtries =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.MAXFETCHTRIES_INDEX), NumberConverter.instance() };
      constructors.put(BrowserProperty.MAXFETCHTRIES_NAME, maxfetchtries);
      Object[] matchrequired =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.MATCHREQUIRED_INDEX), BooleanConverter.instance() };
      constructors.put(BrowserProperty.MATCHREQUIRED_NAME, matchrequired);
      Object[] suppressspeechinput =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.SUPPRESSSPEECHINPUT_INDEX), BooleanConverter.instance() };
      constructors.put(BrowserProperty.SUPPRESSSPEECHINPUT_NAME, suppressspeechinput);
      Object[] suppresskeypadinput =
        { BrowserProperty.class.getConstructor(C3Signature), new Integer(BrowserProperty.SUPPRESSKEYPADINPUT_INDEX), BooleanConverter.instance() };
      constructors.put(BrowserProperty.SUPPRESSKEYPADINPUT_NAME, suppresskeypadinput);

       // Construct entries for the recognizer properties
      Object[] nhypotheses = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_NHYPOTHESES) };
      constructors.put(VISE_NHYPOTHESES_NAME, nhypotheses);
      constructors.put(BrowserProperty.MAXNBEST_NAME, nhypotheses); // Alias
      Object[] wcthreshold = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_WCTHRESHOLD) };
      constructors.put(VISE_WCTHRESHOLD_NAME, wcthreshold);
      Object[] abswildcard = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ABSWILDCARD) };
      constructors.put(VISE_ABSWILDCARD_NAME, abswildcard);
      Object[] relwildcard = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_RELWILDCARD) };
      constructors.put(VISE_RELWILDCARD_NAME, relwildcard);
      Object[] sampsigbits = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SAMPSIGBITS) };
      constructors.put(VISE_SAMPSIGBITS_NAME, sampsigbits);
      Object[] noisetrking = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_NOISETRKING) };
      constructors.put(VISE_NOISETRKING_NAME, noisetrking);
      Object[] jinclassize = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_JINCLASSIZE) };
      constructors.put(VISE_JINCLASSIZE_NAME, jinclassize);
      Object[] maxunprfrms = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_MAXUNPRFRMS) };
      constructors.put(VISE_MAXUNPRFRMS_NAME, maxunprfrms);
      Object[] unprquantum = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_UNPRQUANTUM) };
      constructors.put(VISE_UNPRQUANTUM_NAME, unprquantum);
      Object[] queuelength = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_QUEUELENGTH) };
      constructors.put(VISE_QUEUELENGTH_NAME, queuelength);
      Object[] queuefwdtol = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_QUEUEFWDTOL) };
      constructors.put(VISE_QUEUEFWDTOL_NAME, queuefwdtol);
      Object[] queuebwdtol = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_QUEUEBWDTOL) };
      constructors.put(VISE_QUEUEBWDTOL_NAME, queuebwdtol);
      Object[] boupadlngth =
        { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_BOUPADLNGTH), DurationConverter.instance() };
      constructors.put(VISE_BOUPADLNGTH_NAME, boupadlngth);
      Object[] eoupadlngth =
        { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_EOUPADLNGTH), DurationConverter.instance() };
      constructors.put(VISE_EOUPADLNGTH_NAME, eoupadlngth);
      Object[] frameidlems =
        { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_FRAMEIDLEMS), DurationConverter.instance() };
      constructors.put(VISE_FRAMEIDLEMS_NAME, frameidlems);
      Object[] printframes = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_PRINTFRAMES) };
      constructors.put(VISE_PRINTFRAMES_NAME, printframes);
      Object[] printstatus = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_PRINTSTATUS) };
      constructors.put(VISE_PRINTSTATUS_NAME, printstatus);
      Object[] muthathresh = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_MUTHATHRESH) };
      constructors.put(VISE_MUTHATHRESH_NAME, muthathresh);
      Object[] loosethresh = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_LOOSETHRESH) };
      constructors.put(VISE_LOOSETHRESH_NAME, loosethresh);
      Object[] tightthresh = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_TIGHTTHRESH) };
      constructors.put(VISE_TIGHTTHRESH_NAME, tightthresh);
      Object[] batchweight = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_BATCHWEIGHT) };
      constructors.put(VISE_BATCHWEIGHT_NAME, batchweight);
      Object[] dflttrcount = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_DFLTTRCOUNT) };
      constructors.put(VISE_DFLTTRCOUNT_NAME, dflttrcount);
      Object[] dflttrscore = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_DFLTTRSCORE) };
      constructors.put(VISE_DFLTTRSCORE_NAME, dflttrscore);
      Object[] trainweight = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_TRAINWEIGHT) };
      constructors.put(VISE_TRAINWEIGHT_NAME, trainweight);
      Object[] enriniwcabs = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRINIWCABS) };
      constructors.put(VISE_ENRINIWCABS_NAME, enriniwcabs);
      Object[] enriniwcrel = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRINIWCREL) };
      constructors.put(VISE_ENRINIWCREL_NAME, enriniwcrel);
      Object[] enrinicount = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRINICOUNT) };
      constructors.put(VISE_ENRINICOUNT_NAME, enrinicount);
      Object[] enrbodwcabs = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRBODWCABS) };
      constructors.put(VISE_ENRBODWCABS_NAME, enrbodwcabs);
      Object[] enrbodwcrel = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRBODWCREL) };
      constructors.put(VISE_ENRBODWCREL_NAME, enrbodwcrel);
      Object[] enrbodmindw = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRBODMINDW) };
      constructors.put(VISE_ENRBODMINDW_NAME, enrbodmindw);
      Object[] enrbodmaxdw = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRBODMAXDW) };
      constructors.put(VISE_ENRBODMAXDW_NAME, enrbodmaxdw);
      Object[] enrmaxkernl = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRMAXKERNL) };
      constructors.put(VISE_ENRMAXKERNL_NAME, enrmaxkernl);
      Object[] enrminkernl = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRMINKERNL) };
      constructors.put(VISE_ENRMINKERNL_NAME, enrminkernl);
      Object[] enrmaxdwell = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRMAXDWELL) };
      constructors.put(VISE_ENRMAXDWELL_NAME, enrmaxdwell);
      Object[] enrmindwell = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRMINDWELL) };
      constructors.put(VISE_ENRMINDWELL_NAME, enrmindwell);
      Object[] enrtolernce = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_ENRTOLERNCE) };
      constructors.put(VISE_ENRTOLERNCE_NAME, enrtolernce);
      Object[] caliblength = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_CALIBLENGTH) };
      constructors.put(VISE_CALIBLENGTH_NAME, caliblength);
      Object[] calibweight = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_CALIBWEIGHT) };
      constructors.put(VISE_CALIBWEIGHT_NAME, calibweight);
      Object[] calibrwcrel = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_CALIBRWCREL) };
      constructors.put(VISE_CALIBRWCREL_NAME, calibrwcrel);
      Object[] calibrwcabs = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_CALIBRWCABS) };
      constructors.put(VISE_CALIBRWCABS_NAME, calibrwcabs);
      Object[] vuonoffswitch = { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_VUONOFFSWCH), BooleanConverter.instance() };
      constructors.put(VISE_VUONOFFSWCH_NAME, vuonoffswitch);
      Object[] vuscalemult = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_VUSCALEMULT) };
      constructors.put(VISE_VUSCALEMULT_NAME, vuscalemult);
      Object[] vuoffsetadd = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_VUOFFSETADD) };
      constructors.put(VISE_VUOFFSETADD_NAME, vuoffsetadd);
      Object[] silmaxdwell =
        { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_SILMAXDWELL), DurationConverter.instance() };
      constructors.put(VISE_SILMAXDWELL_NAME, silmaxdwell);
      Object[] endmindwell =
        { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_ENDMINDWELL), DurationConverter.instance() };
      constructors.put(VISE_ENDMINDWELL_NAME, endmindwell);
      Object[] siladaptwgt = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SILADAPTWGT) };
      constructors.put(VISE_SILADAPTWGT_NAME, siladaptwgt);
      Object[] siladaptlag = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SILADAPTLAG) };
      constructors.put(VISE_SILADAPTLAG_NAME, siladaptlag);
      Object[] maxduration =
        { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_MAXDURATION), DurationConverter.instance() };
      constructors.put(VISE_MAXDURATION_NAME, maxduration);
      Object[] maxframecnt = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_MAXFRAMECNT) };
      constructors.put(VISE_MAXFRAMECNT_NAME, maxframecnt);
      // Object[] speakerpath = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SPEAKERPATH) };
      // constructors.put(VISE_SPEAKERPATH_NAME, speakerpath);
      // Object[] dfltspeaker = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_DFLTSPEAKER) };
      // constructors.put(VISE_DFLTSPEAKER_NAME, dfltspeaker);
      Object[] audqueuelen =
        { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_AUDQUEUELEN), DurationConverter.instance() };
      constructors.put(VISE_AUDQUEUELEN_NAME, audqueuelen);
      Object[] bouSilMSecs =
        { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_BOUSILMSECS), DurationConverter.instance() };
      constructors.put(VISE_BOUSILMSECS_NAME, bouSilMSecs);
      Object[] eouSilMSecs =
        { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_EOUSILMSECS), DurationConverter.instance() };
      constructors.put(VISE_EOUSILMSECS_NAME, eouSilMSecs);
      Object[] showdata = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SHOWFRMDATA) };
      constructors.put(VISE_SHOWFRMDATA_NAME, showdata);
      Object[] startshow = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_STARTSHOWFR) };
      constructors.put(VISE_STARTSHOWFR_NAME, startshow);
      Object[] stopshow = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_FINALSHOWFR) };
      constructors.put(VISE_FINALSHOWFR_NAME, stopshow);
      Object[] showall = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SHOWALLTBKS) };
      constructors.put(VISE_SHOWALLTBKS_NAME, showall);
      Object[] showfinal = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SHOWFINALTB) };
      constructors.put(VISE_SHOWFINALTB_NAME, showfinal);
      Object[] showstring = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SHOWTBSTRNG) };
      constructors.put(VISE_SHOWTBSTRNG_NAME, showstring);
      Object[] maxnodedwell = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_MAXNODDWELL) };
      constructors.put(VISE_MAXNODDWELL_NAME, maxnodedwell);
      Object[] searchbw = { SRFloatProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_VSBEAMWIDTH) };
      constructors.put(VISE_VSBEAMWIDTH_NAME, searchbw);
      Object[] newphonebw = { SRFloatProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_NPBEAMWIDTH) };
      constructors.put(VISE_NPBEAMWIDTH_NAME, newphonebw);
      Object[] newwordbw = { SRFloatProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_NWBEAMWIDTH) };
      constructors.put(VISE_NWBEAMWIDTH_NAME, newwordbw);
      Object[] chansperframe = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_CHANSPERFRM) };
      constructors.put(VISE_CHANSPERFRM_NAME, chansperframe);
      Object[] profile = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_USEPROFILER) };
      constructors.put(VISE_USEPROFILER_NAME, profile);
      Object[] showcepstrum = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SHOWCEPDATA) };
      constructors.put(VISE_SHOWCEPDATA_NAME, showcepstrum);
      Object[] showfeatures = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SHOWFEATDAT) };
      constructors.put(VISE_SHOWFEATDAT_NAME, showfeatures);
      Object[] showvq = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SHOWVQFRAME) };
      constructors.put(VISE_SHOWVQFRAME_NAME, showvq);
      Object[] showep = { SRIntProperty.class.getConstructor(C2Signature), new Integer(ViseProperties.VISE_SHOWEPFRAME) };
      constructors.put(VISE_SHOWEPFRAME_NAME, showep);
      Object[] completeto =
        { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_COMPLETETO), DurationConverter.instance() };
      constructors.put(VISE_COMPLETETO_NAME, completeto);
      Object[] incompltto =
        { SRIntProperty.class.getConstructor(C3Signature), new Integer(ViseProperties.VISE_INCOMPLTTO), DurationConverter.instance() };
      constructors.put(VISE_INCOMPLTTO_NAME, incompltto);
       // Aberrant recognizer properties
      Object[] inputlevel = { AberrantProperty.class.getConstructor(C2Signature), new Integer(VISE_INPUTLEVEL_INDEX) };
      constructors.put(VISE_INPUTLEVEL_NAME, inputlevel);
      Object[] saveresults =
        { AberrantProperty.class.getConstructor(C3Signature), new Integer(VISE_SAVERESULTS_INDEX), BooleanConverter.instance() };
      constructors.put(VISE_SAVERESULTS_NAME, saveresults);
      Object[] activespkr = { AberrantProperty.class.getConstructor(C2Signature), new Integer(VISE_ACTIVESPKR_INDEX) };
      constructors.put(VISE_ACTIVESPKR_NAME, activespkr);

       // Construct entries for the TTS properties
      Object[] ttsSpeed = { TTSProperty.class.getConstructor(C2Signature), new Integer(TTS_SPEED_INDEX) };
      constructors.put(TTS_SPEED_NAME, ttsSpeed);
      Object[] ttsPitch = { TTSProperty.class.getConstructor(C2Signature), new Integer(TTS_PITCH_INDEX) };
      constructors.put(TTS_PITCH_NAME, ttsPitch);
      Object[] ttsVolume = { TTSProperty.class.getConstructor(C2Signature), new Integer(TTS_VOLUME_INDEX) };
      constructors.put(TTS_VOLUME_NAME, ttsVolume);
      Object[] ttsVoice = { TTSProperty.class.getConstructor(C2Signature), new Integer(TTS_VOICE_INDEX) };
      constructors.put(TTS_VOICE_NAME, ttsVoice);
      Object[] ttsLanguage = { TTSProperty.class.getConstructor(C2Signature), new Integer(TTS_LANGUAGE_INDEX) };
      constructors.put(TTS_LANGUAGE_NAME, ttsLanguage);
      Object[] ttsEngine = { TTSProperty.class.getConstructor(C2Signature), new Integer(TTS_ENGINE_INDEX) };
      constructors.put(TTS_ENGINE_NAME, ttsEngine);

    } catch (NoSuchMethodException e) {
      e.printStackTrace();
    }
  }

  public static Property newInstance(String name, Object value) {
     // Get the Constructor and its arguments
    Object[] constructor = (Object[])constructors.get(name);
     // Ignore undefined properties, per the VXML specification
    if (constructor == null) return null;
     // Construct the array of arguments for the Constructor
    Object[] arguments = new Object[constructor.length];
    arguments[0] = value;
    for (int i = 1; i < constructor.length; i++) 
      arguments[i] = constructor[i];
     // Invoke the Constructor
    try {
      return (Property)((Constructor)constructor[0]).newInstance(arguments);
    } catch (IllegalAccessException e) {
      throw new RuntimeException("Property.newInstance(): " + e.toString());
    } catch (InstantiationException e) {
      throw new RuntimeException("Property.newInstance(): " + e.toString());
    } catch (InvocationTargetException e) {
      throw new RuntimeException("Property.newInstance(): " + e.toString());
    }
  }

  protected int       index;
  protected Converter converter = NullConverter.instance();
  protected Object    newValue;

  public Property() {
  }

  public Property(Object value, Integer index) {
    newValue = value;
    this.index = index.intValue();
  }

  public Property(Object value, Integer index, Converter converter) throws VXMLEvent {
    this((value instanceof Expr) ? value : converter.convert(value), index);  // Throws VXMLEvent
    this.converter = converter;
  }

  public abstract void enter(Scope scope) throws PropertyVetoException, InvalidExpressionException, UndeclaredVariableException, VXMLEvent;
  public abstract void leave() throws PropertyVetoException;
}

class SRFloatProperty extends Property {

  private ParameterResult oldValue;

  public SRFloatProperty(Object value, Integer index) {
    super(value, index);
  }

  public SRFloatProperty(Object value, Integer index, Converter converter) throws VXMLEvent {
    super(value, index, converter);
  }

  public void enter(Scope scope) throws PropertyVetoException, InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    
  }

  public void leave() {
    
  }
}

class SRIntProperty extends Property {

  private ParameterResult oldValue;

  public SRIntProperty(Object value, Integer index) {
    super(value, index);
  }

  public SRIntProperty(Object value, Integer index, Converter converter) throws VXMLEvent {
    super(value, index, converter);
  }

  public void enter(Scope scope) throws PropertyVetoException, InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    
  }

  public void leave() {
   
  }
}

class SystemProperty extends Property {

  private Object oldValue;

  public SystemProperty(Object value, Integer index) {
    super(value, index);
  }

  public SystemProperty(Object value, Integer index, Converter converter) throws VXMLEvent {
    super(value, index, converter);
  }

  public void enter(Scope scope) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
   
  }

  public void leave() {
   
  }
}

class TTSProperty extends Property {

  private Object oldValue;

  public TTSProperty(Object value, Integer index) {
    super(value, index);
  }

  public TTSProperty(Object value, Integer index, Converter converter) throws VXMLEvent {
    super(value, index, converter);
  }

  public void enter(Scope scope) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    Object value = (newValue instanceof Expr) ? converter.convert(((Expr)newValue).evaluate(scope)) : newValue;
    switch(index) {
      case TTS_SPEED_INDEX:
        oldValue = new Float(VoxwareText.getSpeed());
        VoxwareText.setSpeed((float)ScriptRuntime.toNumber(value));
        break;
      case TTS_PITCH_INDEX:
        oldValue = new Float(VoxwareText.getPitch());
        VoxwareText.setPitch((float)ScriptRuntime.toNumber(value));
        break;
      case TTS_VOLUME_INDEX:
        oldValue = new Float(VoxwareText.getVolume());
        VoxwareText.setVolume((float)ScriptRuntime.toNumber(value));
        break;
      case TTS_VOICE_INDEX:
        oldValue = VoxwareText.getVoice();
        VoxwareText.setVoice(ScriptRuntime.toString(value));
        break;
      case TTS_LANGUAGE_INDEX:
        oldValue = VoxwareText.getLanguage();
        VoxwareText.setLanguage(ScriptRuntime.toString(value));
        break;
      case TTS_ENGINE_INDEX:
        oldValue = VoxwareText.getEngine();
        VoxwareText.setEngine(ScriptRuntime.toString(value));
        break;
      default:
        break;
    }
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.PROPERTY_MASK))
      SysLog.println("TTSProperty.enter: property " + index + " set to " + ScriptRuntime.toString(value));
  }

  public void leave() {
    switch(index) {
      case TTS_SPEED_INDEX:
        VoxwareText.setSpeed(((Float)oldValue).floatValue());
        break;
      case TTS_PITCH_INDEX:
        VoxwareText.setPitch(((Float)oldValue).floatValue());
        break;
      case TTS_VOLUME_INDEX:
        VoxwareText.setVolume(((Float)oldValue).floatValue());
        break;
      case TTS_VOICE_INDEX:
        VoxwareText.setVoice((String)oldValue);
        break;
      case TTS_LANGUAGE_INDEX:
        VoxwareText.setLanguage((String)oldValue);
        break;
      case TTS_ENGINE_INDEX:
        VoxwareText.setEngine((String)oldValue);
        break;
      default:
        break;
    }
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.PROPERTY_MASK))
      SysLog.println("TTSProperty.leave: property " + index + " restored to " + ScriptRuntime.toString(oldValue));
    oldValue = null;
  }
}

 // A loathsome catchall Class to deal with Properties that have no inherent indexing scheme
class AberrantProperty extends Property {

  private Object oldValue;

  public AberrantProperty(Object value, Integer index) {
    super(value, index);
  }

  public AberrantProperty(Object value, Integer index, Converter converter) throws VXMLEvent {
    super(value, index, converter);
  }

  public void enter(Scope scope) throws InvalidExpressionException, NumberFormatException, PropertyVetoException, UndeclaredVariableException, VXMLEvent {
     switch(index) {
      case VISE_INPUTLEVEL_INDEX:
        
        break;
      case VISE_SAVERESULTS_INDEX:
        
        break;
      case VISE_ACTIVESPKR_INDEX:
        
        break;
      case SYSTEM_PRINTLEVEL_INDEX:
        oldValue = new Integer(SysLog.getPrintLevel());
        int printLevel = ScriptRuntime.toInt32((newValue instanceof Expr) ? ((Expr)newValue).evaluate(scope) : newValue);
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.PROPERTY_MASK))
          SysLog.println("AberrantProperty.enter: property " + index + " set to " + printLevel);
        SysLog.setPrintLevel(printLevel);
        break;
      case SYSTEM_PRINTMASK_INDEX:
        oldValue = new Long(SysLog.getPrintMask());
        long mask = (newValue instanceof Expr) ? (long)ScriptRuntime.toNumber(((Expr)newValue).evaluate(scope)) : PCFG.decodeLong((String)newValue).longValue();
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.PROPERTY_MASK))
          SysLog.println("AberrantProperty.enter: property " + index + " set to 0x" + Long.toHexString(mask));
        SysLog.setPrintMask(mask);
        break;
      case SYSTEM_URLROOT_INDEX:
        oldValue = SystemVariables.getUrlRoot();
        String urlRoot = ScriptRuntime.toString((newValue instanceof Expr) ? ((Expr)newValue).evaluate(scope) : newValue);
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.PROPERTY_MASK))
          SysLog.println("AberrantProperty.enter: property " + index + " set to " + urlRoot);
        try {
			SystemVariables.setUrlRoot(new URL(urlRoot));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new InvalidExpressionException("Invalid Expression " + urlRoot);
		}
        break;
      default:
        break;
    }
  }

  public void leave() throws PropertyVetoException {
    if (oldValue != null) {
      switch(index) {
        case VISE_INPUTLEVEL_INDEX:
          
          break;
        case VISE_SAVERESULTS_INDEX:
          
          break;
        case VISE_ACTIVESPKR_INDEX:
         
          break;
        case SYSTEM_PRINTLEVEL_INDEX:
          SysLog.setPrintLevel(((Integer)oldValue).intValue());
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.PROPERTY_MASK))
            SysLog.println("AberrantProperty.leave: property " + index + " restored to " + oldValue.toString());
          break;
        case SYSTEM_PRINTMASK_INDEX:
          SysLog.setPrintMask(((Long)oldValue).longValue());
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.PROPERTY_MASK))
            SysLog.println("AberrantProperty.leave: property " + index + " restored to 0x" + Long.toHexString(((Long)oldValue).longValue()));
          break;
        case SYSTEM_URLROOT_INDEX:
          SystemVariables.setUrlRoot((URL)oldValue);
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.PROPERTY_MASK))
            SysLog.println("AberrantProperty.leave: property " + index + " restored to " + (String)oldValue);
          break;
        default:
          break;
      }
      oldValue = null;
    }
  }
}
