package voxware.browser;

import org.mozilla.javascript.*;

 /** A variable */
public class Variable {

  public final static Variable PROTO_VARIABLE = new Variable();

  protected Object  currentValue;
  protected Expr    initialValue;

   // Construct an Undefined Variable with no initialValue Expr
  public Variable() {
    this(null);
  }

   // Construct an Undefined Variable with the specified initialValue Expr
  public Variable(Expr init) {
    currentValue = VXMLTypes.PROTO_UNDEFINED;
    initialValue = init;
  }

   // Return the current value
  public Object value() {
    return currentValue;
  }

   // Return the initialization Expr
  public Expr initializer() {
    return initialValue;
  }

   // Assign the value of the initialValue Expr if it is known not to contain references
  public void initialize() throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    assign((initialValue == null) ? VXMLTypes.PROTO_UNDEFINED : initialValue.evaluate(null));
  }

   // Assign the value of the initialValue Expr
  public void initialize(Scope scope) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    assign((initialValue == null) ? VXMLTypes.PROTO_UNDEFINED : initialValue.evaluate(scope));
  }

   // Assign a value of Type Undefined
  public void clear() {
    currentValue = VXMLTypes.PROTO_UNDEFINED;
  }

   // Assign a default value
  public void setDefault() {
    currentValue = new Boolean(true);
  }

   // Assign a value
  public void assign(Object value) {
    currentValue = value;
  }

   // Assign the value of an Expr known not to contain references
  public void assign(Expr value) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    currentValue = value.evaluate(null);
  }

   // Assign the value of an Expr
  public void assign(Expr value, Scope scope) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    currentValue = value.evaluate(scope);
  }

   // Return the Type of this Variable's value
  public Class type() {
    return currentValue != null ? currentValue.getClass() : null;
  }
}
