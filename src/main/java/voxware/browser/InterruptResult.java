package voxware.browser;

import voxware.engine.recognition.*;

import voxware.browser.*;

 /** An InterruptResult */
class InterruptResult extends BaseResult {

  private Interrupt interrupt;

  public InterruptResult(Interrupt interrupt) {
    super(null, SystemEvent.name(interrupt.template.index) + " interrupt", false);
    this.interrupt = interrupt;
  }

  public Interrupt interrupt() {
    return interrupt;
  }
}
