package voxware.browser;

 /** The Executables Interface (Block, Catcher, Filled and If implement this) */
public interface Executables {
   // Return the ExecutableContent
  public ExecutableContent executableContent();
}
