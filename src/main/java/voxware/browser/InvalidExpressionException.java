package voxware.browser;

public class InvalidExpressionException extends Exception {

  public int    position;

  InvalidExpressionException(String text) {
    super(text);
    this.position = 0;
  }

  InvalidExpressionException(String text, int position) {
    super(text);
    this.position = position;
  }
}
