package voxware.browser;

import java.util.*;

public class VXMLEvent extends Exception {

  private String name;
  private Throwable exception;

   // Constructors

  public VXMLEvent(String name, Throwable exception, String message) {
    super(message);
    this.name = name;
    this.exception = exception;
  }

  public VXMLEvent(String name, Throwable exception) {
    super();
    this.name = name;
    this.exception = exception;
  }

  public VXMLEvent(String name, String message) {
    super(message);
    this.name = name;
  }

  public VXMLEvent(String name) {
    super();
    this.name = name;
  }

   // Access Methods

  public String name() {
    return this.name;
  }

  public Throwable exception() {
    return this.exception;
  }

  public String toString() {
    return "VXMLEvent \"" + name + "\"" + (String)(exception != null ? (" containing " + exception.toString()) : "");
  }
}


