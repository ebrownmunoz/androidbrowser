package voxware.browser;

import java.util.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.*;

 /** FILLED ACTIONS **/

 /** A <filled> action */
class Filled extends ScopeElement implements Executables {

  Element filled;
  List<String>  names;    // Names of the variables to be filled
  boolean fillAll;  // True for "all", false for "any"
  private final Session session;

   /** Construct a Filled object from a <filled> element in the DOM tree */
  public Filled(Session session, Element filled, Goto context, boolean preprocess) throws DOMException, VXMLEvent {
    super();
    
    this.session = session;
    prefix = "_anonymous";
    this.filled = filled;
    this.setIndex(Elements.FILLED_INDEX);

     // Get the list of variables to trigger on (OPTIONAL)
    Attr attr = this.filled.getAttributeNode("namelist");
    names = null;
    if (attr != null) {
      String filledString = attr.getNodeValue();                // Throws DOMException
      if (filledString != null && filledString.length() > 0) {
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.FILLED_MASK))
          SysLog.println("Filled.Filled(): namelist = \"" + filledString + "\"");
        names = new ArrayList<String>();
        names.addAll(new Variables(filledString).names());
      }
    } else {
       // DEFAULT to a Form's field items, but delay until execution, in case not all field items are defined yet
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.FILLED_MASK))
        SysLog.println("Filled.Filled(): namelist = DEFAULT");
    }

     // Get the mode ("any" or "all") (OPTIONAL)
    attr = this.filled.getAttributeNode("mode");
    if (attr != null) {
      String modeString = attr.getNodeValue();                  // Throws DOMException
      fillAll = (modeString != null && modeString.equalsIgnoreCase("all"));
    } else {
      fillAll = true;   // DEFAULT = "all" (changed from VXML 0.9!)
    }

     // Construct the ExecutableContent
    executableContent = (preprocess) ? new ExecutableContent(session, filled, context) : new ExecutableContent(session, context);  // Throws VXMLEvent
  }

   /** Construct a Filled object for a single Variable */
  public Filled(Session session, Element filled, Goto context, String name, boolean preprocess) throws VXMLEvent{
    super();
    
    this.session = session;
    prefix = "_anonymous";
    this.filled = filled;
    names = new Vector(1);
    names.add(name);
    fillAll = false;
     // Construct the ExecutableContent
    executableContent = (preprocess) ? new ExecutableContent(session, filled, context) : new ExecutableContent(session, context);  // Throws VXMLEvent
  }

   /** Make this a child of a ScopeElement (Form) (extends makeChildOf() in ScopeElement to inherit the parent's EventHandler) */
  public void makeChildOf(ScopeElement parent) {
     // Inherit the parent Form's EventHandler (since Filled cannot contain <catch> elements)
    handler = parent.handler();
     // Make this a child of the parent Form witbout having the Form adopt it
    try {
      super.makeChildOf(parent, false);  // Throws VXMLEvent
    } catch (VXMLEvent e) {
      throw new RuntimeException("Filled.makeChildOf(): UNEXPECTED " + e.toString());
    }
  }

   /** Implement the Executables interface */
  private ExecutableContent executableContent;
  public ExecutableContent executableContent() {
    return executableContent;
  }

   /** Execute a Filled action */
  public void execute(Variables filledVars, Form scope, Goto context) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
    if (filledVars != null) {
       // DEFAULT to all the parent Form's field items
      if (this.names == null) {
        HashVector fieldItems = scope.fieldItems();
        if (fieldItems != null && fieldItems.size() > 0) {
          this.names = new Vector<String>(fieldItems.size());
          Enumeration items = scope.fieldItems().elements();
          while (items.hasMoreElements())
            this.names.add(((FormItem)items.nextElement()).name());
        }
      }
      if (this.names != null) {
         // Check that at least one of the variables to be filled is in the filledVars
    	  
    	boolean anyFilled = false;
    	
        for (String name : this.names) {
        	if (filledVars.containsKey(name)){
        		anyFilled = true;
        		break;
        	}
        }
        
        if (!anyFilled) {
        	return;
        }

        // If mode == "all", check that all of the variables to be filled are defined
        if (fillAll) {
          try {
            for (String name : names){
              if (VXMLTypes.isUndefined(scope.variable((String)name))) return;
            }
          } catch (UndeclaredVariableException e) {
            if (SysLog.printLevel > 0) SysLog.println("Filled.execute(): ERROR - " + e.toString());
            throw new VXMLEvent("error.semantic.undeclared", e);
          }
        }
        if (SysTimer.granularity > 2) SysTimer.stamp(2, "Filled.execute(): elapsed time to start = ", " ms");
         // Munch through the children of <filled>, as through any other "executable content" */
        if (prompts != null) prompts.resetPromptCount();
        initializeScope(scope);
        executableContent.execute(filled, this, context, true);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
      }
    }
  }
}
