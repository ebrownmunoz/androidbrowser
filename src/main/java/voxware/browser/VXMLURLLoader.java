package voxware.browser;

import java.net.*;
import java.io.*;
import java.util.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.mozilla.javascript.ScriptRuntime;
import org.mozilla.javascript.NativeJavaArray;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.NoHttpResponseException;

import voxware.util.SysLog;

public class VXMLURLLoader {

  private static final HttpClient httpClient = new DefaultHttpClient();
  
  
  protected int    fetchTimeout = DurationConverter.UNSPECIFIED;
  public void   setFetchTimeout(int fetchTimeout) { this.fetchTimeout = fetchTimeout; }
  public int    getFetchTimeout()                 { return this.fetchTimeout; }
  private URL url;
  private int connectRetries = 0; // TODO this seems to have been ignored in the legacy code.

   // If this.fetchTimeout is defined, return it; otherwise, return the default fetchTimeout
  protected int fetchTimeout() {
    return (this.fetchTimeout == DurationConverter.UNSPECIFIED) ? ((Integer)BrowserProperty.value(BrowserProperty.FETCHTIMEOUT_INDEX)).intValue() : this.fetchTimeout;
  }

  public VXMLURLLoader(URL url) {
    this.url = url;
  }


   // Return the contents of the URL in a byte array, using GET
  public byte[] bytes() throws VXMLEvent {
    return bytes("get", null, DurationConverter.UNSPECIFIED);
  }

   // Return the contents of the URL in a byte array, using GET
  public byte[] bytes(int fetchTimeout) throws VXMLEvent {
    return bytes("get", null, fetchTimeout);
  }

   // Return the contents of the URL in a byte array, using the specified method
  public byte[] bytes(String method, Variables variables) throws VXMLEvent {
    return bytes(method, variables, DurationConverter.UNSPECIFIED);
  }

   // Return the contents of the URL in a byte array, using the specified method
  public byte[] bytes(String method, Variables variables, int fetchTimeout) throws VXMLEvent {
    byte[] buffer = null;
    setConnectRetries(((Integer)BrowserProperty.value(BrowserProperty.MAXCONNECTRETRIES_INDEX)).intValue());
     // If defined, the given fetchTimeout overrides this.fetchTimeout()
    if (fetchTimeout == DurationConverter.UNSPECIFIED) fetchTimeout = fetchTimeout();
    if (fetchTimeout > 0 && url.getProtocol().equalsIgnoreCase("http") && ((fetchTimeout = SystemEvent.waitForRadioStatus(fetchTimeout)) == 0)) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.URLLOADER_MASK)) SysLog.println("VXMLURLLoader.bytes: out of range");
      throw new VXMLEvent("error.badfetch.outofrange");
    }

    if (SysLog.shouldLog(1, SysLog.URLLOADER_MASK)) SysLog.println("VXMLURLLoader.bytes: loading \"" + url.toString() + "\"");
    if (method.equals("get")) {
    	buffer = bytesUsingGet(variables, fetchTimeout);
    } else if (method.equals("post")) {
    	buffer = bytesUsingPost(variables, fetchTimeout);
    }

    if (SysLog.shouldLog(1, SysLog.URLLOADER_MASK)) SysLog.println("VXMLURLLoader.bytes: done");
    return buffer;
  }
  
  private byte[] bytesUsingGet(Variables variables, int fetchTimeOut) throws VXMLEvent {
	  int retries = 
			  (Integer)BrowserProperty.value(BrowserProperty.MAXCONNECTRETRIES_INDEX);
	  

	  HttpRequestRetryHandler retry = new DefaultHttpRequestRetryHandler(retries, true);
	  
	  CloseableHttpClient httpClient = HttpClientBuilder.create().setRetryHandler(retry).build();

	  byte[] responseBytes = null;

	  try {

		  URIBuilder uriBuilder = new URIBuilder(url.toURI());
		  uriBuilder.addParameters(nameValuePairs(variables));

		  HttpGet request = new HttpGet(uriBuilder.build());
		  HttpResponse httpResponse = httpClient.execute(request);
		  HttpEntity entity = httpResponse.getEntity();

		  try {
			  ByteArrayOutputStream baos = new ByteArrayOutputStream();
			  entity.writeTo(baos);
			  responseBytes = baos.toByteArray();
		  } finally {
			  EntityUtils.consume(entity);
		  }
	  } catch (URISyntaxException e) {
		  throw new VXMLEvent("error.badfetch.filenotfound", e);
	  } catch (ClientProtocolException cpe) {
		  throw new VXMLEvent("error.badfetch.httperror.badheader", cpe);
	  } catch (NoHttpResponseException nhre) {
		  throw new VXMLEvent("error.badfetch.badsocket.noconnection", nhre);
	  } catch (ConnectTimeoutException cte) {
		  throw new VXMLEvent("error.badfetch.timedout", cte);
	  } catch (IOException e) {
		  throw new VXMLEvent("error.badfetch.ioerror", e);
	  } finally {
		  try {
			httpClient.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }

	  return responseBytes;

  }
  
  private byte[] bytesUsingPost(Variables variables, int fetchTimeOut) throws VXMLEvent {
	  int retries = 
			  (Integer)BrowserProperty.value(BrowserProperty.MAXCONNECTRETRIES_INDEX);

	  HttpRequestRetryHandler retry = new DefaultHttpRequestRetryHandler(retries, true);
	  
	  CloseableHttpClient httpClient = HttpClientBuilder.create().setRetryHandler(retry).build();
	  
	  byte[] responseBytes = null;

	  try {

		  HttpPost post = new HttpPost(url.toURI());

		  MultipartEntityBuilder mpEntityBuilder = MultipartEntityBuilder.create();
		 for (NameValuePair each : nameValuePairs(variables)) {
			 mpEntityBuilder.addTextBody(each.getName(), each.getValue());
		 }
		  
		  URIBuilder uriBuilder = new URIBuilder(url.toURI());
		  uriBuilder.addParameters(nameValuePairs(variables));

		  HttpGet request = new HttpGet(uriBuilder.build());

		  HttpResponse httpResponse = httpClient.execute(request);
		  HttpEntity entity = httpResponse.getEntity();

		  try {
			  ByteArrayOutputStream baos = new ByteArrayOutputStream();
			  entity.writeTo(baos);
			  responseBytes = baos.toByteArray();
		  } finally {
			  EntityUtils.consume(entity);
		  }
	  } catch (URISyntaxException e) {
		  throw new VXMLEvent("error.badfetch.filenotfound", e);
	  } catch (ClientProtocolException cpe) {
		  throw new VXMLEvent("error.badfetch.httperror.badheader", cpe);
	  } catch (NoHttpResponseException nhre) {
		  throw new VXMLEvent("error.badfetch.badsocket.noconnection", nhre);
	  } catch (ConnectTimeoutException cte) {
		  throw new VXMLEvent("error.badfetch.timedout", cte);
	  } catch (IOException e) {
		  throw new VXMLEvent("error.badfetch.ioerror", e);
	  }

	  return responseBytes;

  }


   // Return an array containing an NameValuePair for each simple variable in a Variables
  private List<NameValuePair> nameValuePairs(Variables variables) {
	List<NameValuePair> pairs = new ArrayList<NameValuePair>();
    if (variables != null) {
      for (String name : variables.keySet()) {
        Object value = variables.getByName(name).value();
         // We use org.mozilla.javascript.ScriptRuntime.toString(value) here instead of just value.toString(); otherwise. every integer
         // gets sent with ".0" appended, since a Javascript integer is a Double internally, and that's what Double.toString() does.
         // The "." can wreak havoc if the other end is expecting an integer and thus uses Integer.parseInt() to parse it.
        if (!(value instanceof byte[]) &&
            !(value instanceof org.mozilla.javascript.NativeJavaArray && ((org.mozilla.javascript.NativeJavaArray) value).unwrap() instanceof byte[])) {
          pairs.add(new BasicNameValuePair(name, ScriptRuntime.toString(value)));
          if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.URLLOADER_MASK))
            SysLog.println("  Name = " + name + ", Class = " + (value != null ? value.getClass().getName() : "null"));
        }
      }

    }
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.URLLOADER_MASK)) {
      int length = pairs != null ? pairs.size() : 0;
      SysLog.println("VXMLURLLoader.nameValuePairs: returns " + length + " pair(s)");
      for (int i = 0; i < length; i++)
        SysLog.println("  Name = " + pairs.get(i).getName() + ", Value = " + pairs.get(i).getValue());
    }
    return pairs;
  }
  
  class NamedByteArray {
	  String name;
	  byte[] array;
  }

   // Return an array containing a NamedByteArray for each byte array variable in a Variables
  private List<NamedByteArray> namedByteArrays(Variables variables) {
    List<NamedByteArray> pairs = new ArrayList<VXMLURLLoader.NamedByteArray>();
    if (variables != null) {
    	
      for (String name : variables.keySet()) {
    	  NamedByteArray pair;
    	  Object value = variables.getByName(name).value();
          if (value instanceof byte[]) {
        	  pair = new NamedByteArray();
        	  pair.name = name;
        	  pair.array = (byte[])value;
        	  pairs.add(pair);
          } else if (value instanceof org.mozilla.javascript.NativeJavaArray && ((org.mozilla.javascript.NativeJavaArray) value).unwrap() instanceof byte[]) {
           
        	  pair = new NamedByteArray();
        	  pair.name = name;
        	  pair.array = (byte[]) ((org.mozilla.javascript.NativeJavaArray) value).unwrap();
        	  pairs.add(pair);
          }
      }
      
    }
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.URLLOADER_MASK)) {
      int length = pairs != null ? pairs.size() : 0;
      SysLog.println("VXMLURLLoader.namedByteArrays: returns " + length + " pair(s)");
      for (int i = 0; i < length; i++)
        SysLog.println("  Name = " + pairs.get(i).name + ", Length = " + pairs.get(i).array.length);
    }
    return pairs;
  }
public int getConnectRetries() {
	return connectRetries;
}
public void setConnectRetries(int connectRetries) {
	this.connectRetries = connectRetries;
}
}
