package voxware.browser;

import java.net.MalformedURLException;
import java.util.Enumeration;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.Context;

import voxware.browser.*;
import voxware.util.*;

 /** An Object
   * An Object's parent is a Form */
class VXMLObj extends SubFormItem implements FieldItem {

  private static int instanceCount = 0;

  private String         classFileName;                 // Where to get the object (A URL ending in DOMBrowser.classFileExtension or a fully qualified class name)
  private VoxwareApplet  prefetchedApplet;              // The applet if prefetched; null otherwise
  private Variables      submitVars;                    // Variables for the "submit=" directive of <object> (DEPRECATED)
  private Variables      expectVars;                    // Variables returned from <object> (DEPRECATED)
  
   /** Construct an orphan VXMLObj from an <object> element in a DOM tree */
  VXMLObj(Session session, Element vxmlobj) throws DOMException, InvalidExpressionException, InvalidTag, VXMLEvent {
    super(session, vxmlobj);                                     // Throws DOMException, InvalidExpressionException, VXMLEvent
    prefix = "object";
    this.setIndex(Elements.OBJECT_INDEX);
    
    handler = new EventHandler();

    submitVars = null;                                  // DEPRECATED
    expectVars = null;                                  // DEPRECATED

     // Supply an internal name if none has been given
    if (name == null)
      name = "_OBJECT" + Integer.toString(++instanceCount);

     // Get the source of the object document (REQUIRED)
    Attr attr = vxmlobj.getAttributeNode("classid");
    if (attr == null) attr = vxmlobj.getAttributeNode("src");  // DEPRECATED
    if (attr != null)
      classFileName = attr.getNodeValue();              // Throws DOMException
    else 
      throw new InvalidTag(vxmlobj);                    // Throws InvalidTag

     // Get the "submit" variables to pass to the applet (OPTIONAL, DEPRECATED).
     // We set up a Variables, submitVars, that has all of the variables (with default values) that are in the "submit" list.  
     // At interpretation time, we will copy in any values we can find in the Form, etc. variables that have matching names.
    attr = vxmlobj.getAttributeNode("submit");
    if (attr != null) {
      String submitString = attr.getNodeValue();
      if (submitString != null && submitString.length() > 0) {
        if (SysLog.printLevel >= 1) SysLog.println("VXMLObj: submitString=\"" + submitString + "\"");
        submitVars = new Variables(submitString);
      }
    }

     // The "expect" variables are treated similarly during preprocessing, but are put into the parent <form> Variables as well (DEPRECATED)
    attr = vxmlobj.getAttributeNode("expect");
    if (attr != null) {
      String expectString = attr.getNodeValue();
      if (expectString != null && expectString.length() > 0) {
        if (SysLog.printLevel >= 2) SysLog.println("VXMLObj: expectString=\"" + expectString + "\"");
        expectVars = new Variables(expectString);
      }
    }

     // Do everything permissible at preprocessing time
    if (classFileName != null) {
       // Prefetch the applet if permitted
      if (prefetch()) {
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.OBJECT_MASK))
          SysLog.println("VXMLObj.preprocess: prefetching \"" + classFileName + "\"");
        prefetchedApplet = applet();                    // Throws VXMLEvent
      } else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.OBJECT_MASK)) {
        int dflt = ((Integer)BrowserProperty.value(BrowserProperty.OBJECTFETCHHINT_INDEX)).intValue();
        SysLog.println("VXMLObj.preprocess: no prefetching - fetchhint = " + fetchhint + " and default = " + dflt);
      }
    } else {
       // The source of the object document is REQUIRED
      throw new InvalidTag((Node)element);              // Throws InvalidTag
    }
  }

   // Determine whether to prefetch
  private boolean prefetch() {
    return (fetchhint == FetchHintConverter.PREFETCH_INDEX ||
            fetchhint == FetchHintConverter.NULL_INDEX &&
            ((Integer)BrowserProperty.value(BrowserProperty.OBJECTFETCHHINT_INDEX)).intValue() == FetchHintConverter.PREFETCH_INDEX);
  }

   // Acquire the VoxwareApplet
  private VoxwareApplet applet() throws VXMLEvent {

    VoxwareApplet applet = null;
    if (classFileName == null) throw new VXMLEvent("error.badfetch.nosource");

     // If the applet was prefetched and the prefetch policy still stands, use the prefetched version
    if (prefetchedApplet != null && prefetch()) {
      applet = prefetchedApplet;
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.OBJECT_MASK))
        SysLog.println("VXMLObj.applet: acquired prefetched applet \"" + classFileName + "\"");

     // Otherwise, get the applet from the cache or from the source
    } else {
       // If the caching policy is "safe", or the class is not in the cache, acquire the class file anew
      Object cachedApplet = null;
      int cachingPolicy = (caching != CachingConverter.NULL_INDEX) ? caching : ((Integer)BrowserProperty.value(BrowserProperty.CACHING_INDEX)).intValue();
      if (cachingPolicy == CachingConverter.SAFE_INDEX || !((cachedApplet = DOMBrowser.cache.get(classFileName)) instanceof VoxwareApplet)) {
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.OBJECT_MASK)) {
          if (cachingPolicy == CachingConverter.SAFE_INDEX) SysLog.println("VXMLObj.applet: caching policy is \"safe\", getting from source");
          else if (cachedApplet == null) SysLog.println("VXMLObj.applet: no such cache entry, getting from source");
          else SysLog.println("VXMLObj.applet: cached Object is not a VoxwareApplet, getting from source");
        }
         // Get the applet class and cache it
        try {
          Class appletClass = DOMBrowser.loadClass(classFileName, fetchtimeout);   // Throws ClassNotFoundException
          applet = (VoxwareApplet) appletClass.newInstance();
          DOMBrowser.cache.put(classFileName, applet);
        } catch (ClassNotFoundException e) {
          throw new VXMLEvent("error.unsupported.object", e);
        } catch (IllegalAccessException e) {
          throw new VXMLEvent("error.unsupported.object", e);
        } catch (InstantiationException e) {
          throw new VXMLEvent("error.unsupported.object", e);
        } catch (ClassCastException e) {
          throw new VXMLEvent("error.unsupported.object", e);
        }
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.OBJECT_MASK)) {
          cachedApplet = DOMBrowser.cache.get(classFileName);
          if (cachedApplet == null) SysLog.println("VXMLObj.applet: CACHE FAILURE - null");
          else if (!cachedApplet.equals(applet)) SysLog.println("VXMLObj.applet:  CACHE FAILURE - unequal");
          else SysLog.println("VXMLObj.applet: cache holds " + DOMBrowser.cache.size() + " entries in " + DOMBrowser.cache.totalBytes() + " bytes");
        }
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.OBJECT_MASK))
          SysLog.println("VXMLObj.applet: acquired applet \"" + classFileName + "\" from source");

       // Otherwise, acquire the class from the cache
      } else {
        applet = (VoxwareApplet)cachedApplet;
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.OBJECT_MASK))
          SysLog.println("VXMLObj.applet: acquired applet \"" + classFileName + "\" from cache");
      }
    }

    return applet;
  }

   /** Make the VXMLObj a child of a Form (extends FormItem's makeChildOf() to register the "expect" variables) */
  public void makeChildOf(ScopeElement parent) throws VXMLEvent {
     // Inherit the parent's JavaScript scope 
    scope = parent.scope();
     // Have the parent Form adopt this child and give the child Form Scope
    super.makeChildOf(parent);  // Throws VXMLEvent
     // Configure the parameters object
    makeParameters(parent);
     // Register the "expect" variables, if any, in Form scope
    if (expectVars != null) {
      Enumeration varnames = expectVars.keys();
      while (varnames.hasMoreElements()) {
        String name = (String)varnames.nextElement();
        parent.putVariable(name, expectVars.getByName(name));
      }
    }
  }

   /** Interpret the VXMLObj */
  public Variables interpret(Goto context, InterruptHandler interruptHandler) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.OBJECT_MASK))
      SysLog.println("VXMLObj.interpret(): called");
     // Clear the guard variable, in case this visit is the result of a form item goto
    Scope.setProp(scope, name, VXMLTypes.PROTO_UNDEFINED);

    Variables retVars = null;

    try {

      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.enter(element, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request entering <object> " + name);

      modifyProperties();

      Enumeration varnames;

       // Initialize the JavaScript Object containing the parameters
      parameters.initializeVariables(null, this);       // Throws VXMLEvent

       // Assign current variable values to "submit" list variables (DEPRECATED)
      if (submitVars != null) {
        varnames = submitVars.keys();
        while (varnames.hasMoreElements()) {
          String key = (String)varnames.nextElement();
          if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.VARIABLES_MASK))
            SysLog.println("VXMLObj.interpret(): key = \"" + key + "\"");
          submitVars.getByName(key).assign(parent.variable(key));  // Throws UndeclaredVariableException
        }
      }

      VoxwareApplet applet = applet();

       // Initialize the VoxwareApplet, giving it the JSAPI Recognizer and the VoxwareAudioPlayer (installs the JSAPI resultListener)
      applet.initialize(session.getRecognizer().getSpeechRecognizer(), session.getAudioPlayer().getPlayer());

       // RUN the VoxwareApplet

       // If either the "submit" or the "expect" attribute is present, use the DEPRECATED calling mechanism
      if (submitVars != null || expectVars != null) {

        try {
           // Call the DEPRECATED run() method
          retVars = applet.run(submitVars);
        } finally {
           // Tear down the applet (removes the JSAPI resultListener)
          applet.teardown();
        }

         // Store the "expect" variables to Form scope
        if (retVars != null) {
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VARIABLES_MASK))
            SysLog.println("VXMLOBJ.interpret(): got expect variables");
          varnames = retVars.keys();
          String varName;
          while (varnames.hasMoreElements()) {
            varName = (String)varnames.nextElement();
            if (expectVars.getByName(varName) != null) {
              if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VARIABLES_MASK))
                SysLog.println("VXMLOBJ.interpret(): adding expect variable " + varName + "=" + (retVars.getByName(varName)).value());
              setProperty(varName, retVars.getByName(varName).value());  // Throws UndeclaredVariableException
            }
          }
        }

         // Set the guard variable
        Scope.setProp(scope, name, VXMLTypes.PROTO_DEFINED);

       // Neither the "submit" nor the "expect" attribute is present, so use the NEW calling mechanism
      } else {

        Object results = null;

        try {
           // Call the VXML 1.0 run() method
          results = applet.run(parameters());
        } finally {
           // Tear down the applet (removes the JSAPI resultListener)
          applet.teardown();
        }

         // Set the guard variable
        if (results == null) results = VXMLTypes.PROTO_UNDEFINED;
        Scope.setProp(scope, name, results);

      }

      if (retVars == null) retVars = new Variables(1);  // Make a Variables to contain the guard Variable
      retVars.put(name, guard);                         // All that matters is the name

     // Catch any VXMLEvent in this VXMLObj's scope
    } catch (VXMLEvent event) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.CATCH_MASK))
        SysLog.println("VXMLObj.interpret(): <vxmlobj> \"" + name + "\" caught VXMLEvent \"" + event.name() + "\"");
      retVars = catchEvent(event, context);             // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
    } catch (UndeclaredVariableException e) {
      if (SysLog.printLevel > 0) SysLog.println("VXMLObj.interpret(): ERROR - " + e.toString());
      retVars = catchEvent(new VXMLEvent("error.semantic.undeclared", e), context);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
    } finally {
      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.leave(element, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request leaving <object> " + name);
      restoreProperties();
    }
    return retVars;
  }
} // class VXMLObj
