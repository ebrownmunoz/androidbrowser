package voxware.browser;

import java.net.*;
import java.io.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.mozilla.javascript.*;

import javax.speech.*;
import javax.speech.recognition.*;

import voxware.engine.recognition.*;
import voxware.engine.recognition.sapivise.*;
import voxware.net.URLLoader;
import voxware.util.*;

 /** A Speaker */
public class Speaker {

  private static final String UNKNOWN_SPEAKER_NAME = "Unknown";

  public static final String DEFAULT_FILE_EXTENSION = ".voi";
  private final Session session;

  public static String UNKNOWN_SPEAKER_URL = null;

  private static Speaker unknownSpeaker = null;

  public static Speaker unknown(Session session) throws InvalidTag, MalformedURLException, VXMLEvent {
    if (unknownSpeaker == null)
      unknownSpeaker = new Speaker(session, (VXMLURLLoader)null, UNKNOWN_SPEAKER_NAME);
    return unknownSpeaker;
  }

  public static Speaker unknown(Session session, String url) throws InvalidTag, MalformedURLException, VXMLEvent {
    if (url == null || url.length() == 0) {
      return unknown(session);
    } else {
      unknownSpeaker = new Speaker(session, new VXMLURLLoader(url), UNKNOWN_SPEAKER_NAME);
      UNKNOWN_SPEAKER_URL = url;
      return unknownSpeaker;
    }
  }

   /** Acquire a SpeakerProfile from the speaker cache, optionally loading it if it is not there 
 * @throws InvalidTag */
  public SpeakerProfile profile(VXMLURLLoader url, boolean load) throws VXMLEvent, InvalidTag {
    SpeakerProfile profile = (SpeakerProfile)session.getSpeakerCache().get(url);
    if (profile == null && load) profile = new Speaker(session, url, CachingConverter.SAFE_INDEX, FetchHintConverter.PREFETCH_INDEX).profile();
    return profile;
  }

  private Element         speaker;
  private VXMLURLLoader   url;                                           // The target URL (this or expr REQUIRED)
  private Expr            expr;                                          // An expression that evaluates to a target URL string
  private String          name;                                          // The speaker's name (extracted from the URL or supplied as an attribute)
  private Expr            nameexpr;                                      // An expression that evaluates to a target name string
  private String          type;                                          // How the speaker data is encoded (MIME type)
  private int             caching = CachingConverter.NULL_INDEX;
  private int             fetchhint = FetchHintConverter.NULL_INDEX;
  private int             fetchtimeout = DurationConverter.UNSPECIFIED;  // Maximum time allowed to fetch the grammar
  private SpeakerProfile  prefetchedProfile;

   /** Construct a Speaker from a URL */
  public Speaker(Session session, VXMLURLLoader url) throws InvalidTag, VXMLEvent {
	  this(session, url, null);
  }
  
  public Speaker (Session session, VXMLURLLoader url, String name) throws InvalidTag, VXMLEvent {	 
  	this.url = url;
  	this.session = session;
  	this.name = name;
  	preprocess();
  }

   /** Construct a Speaker with the given caching and fetchhint properties from a URL */
  public Speaker(Session session, VXMLURLLoader url, int caching, int fetchhint) throws InvalidTag, VXMLEvent {
    this.url = url;
    this.caching = caching;
    this.fetchhint = fetchhint;
    this.session = session;
    preprocess();                                                        // Throws InvalidTag and VXMLEvent
  }

   /** Construct a Speaker from a <speaker> element in the DOM tree */
  public Speaker(Session session, Element speaker) throws DOMException, InvalidExpressionException, InvalidTag, MalformedURLException, VXMLEvent {

	this.session = session;
     // Get the attributes of the <speaker> element
    NamedNodeMap nnm = ((Node)speaker).getAttributes();
    if (nnm != null) {
      for (int j = 0; j < nnm.getLength(); j++) {
        Node attrNode = nnm.item(j);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("src"))
          url = new VXMLURLLoader(attrNode.getNodeValue());              // Throws DOMException and MalformedURLException
        else if (nodeName.equalsIgnoreCase("expr"))
          expr = new Expr(attrNode.getNodeValue(), null);                // Throws DOMException and InvalidExpressionException 
        if (nodeName.equalsIgnoreCase("name"))
          name = attrNode.getNodeValue();                                // Throws DOMException
        else if (nodeName.equalsIgnoreCase("nameexpr"))
          nameexpr = new Expr(attrNode.getNodeValue(), null);            // Throws DOMException and InvalidExpressionException 
        else if (nodeName.equalsIgnoreCase("type"))
          type = attrNode.getNodeValue();                                // Throws DOMException
        else if (nodeName.equalsIgnoreCase("caching"))
          caching = ((Integer)CachingConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
        else if (nodeName.equalsIgnoreCase("fetchhint"))
          fetchhint = ((Integer)FetchHintConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
        else if (nodeName.equalsIgnoreCase("fetchtimeout"))
          fetchtimeout = ((Integer)DurationConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
      }
    }
    preprocess();                                                        // Throws InvalidTag and VXMLEvent
  }

   // Do everything permissible at preprocessing time
  private void preprocess() throws InvalidTag, VXMLEvent {
     // The URL of the speaker document is REQUIRED
    if (url != null) {
      if (prefetch()) {
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SPEAKER_MASK))
          SysLog.println("Speaker:preprocess: prefetching \"" + StringLiteralSupport.stringLiteralFromUnicode(url.getURL().toString()) + "\"");
        prefetchedProfile = profile((Scope)null);                        // Throws VXMLEvent
      } else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SPEAKER_MASK)) {
        int dflt = ((Integer)BrowserProperty.value(BrowserProperty.SPEAKERFETCHHINT_INDEX)).intValue();
        SysLog.println("Speaker:preprocess: no prefetching - fetchhint = " + fetchhint + " and default = " + dflt);
      }
    } else if (expr == null) {
      if (name == null && nameexpr == null) throw new InvalidTag((Node)speaker);  // Throws InvalidTag
    } else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SPEAKER_MASK)) {
      SysLog.println("Speaker:preprocess: no prefetching - new Speaker has expr attribute but no src attribute");
    }
  }

   // Determine whether to prefetch
  private boolean prefetch() {
    return (fetchhint == FetchHintConverter.PREFETCH_INDEX ||
            fetchhint == FetchHintConverter.NULL_INDEX &&
            ((Integer)BrowserProperty.value(BrowserProperty.SPEAKERFETCHHINT_INDEX)).intValue() == FetchHintConverter.PREFETCH_INDEX);
  }

  public String name() {
    return name;
  }

   /** Acquire the SpeakerProfile in the given Scope */
  public SpeakerProfile profile(Scope scope) throws VXMLEvent {

     // If there is a scope, evaluate any expression in the scope (expr overrides nameexpr)
    if (scope != null) {
       // If there is an expr, evaluate it in the scope and interpret the result as the target URL (expr overrides src)
      if (expr != null) {
        try {
          Object value = expr.evaluate(scope);                             // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
          url = value instanceof URL ? new VXMLURLLoader((URL)value) : new VXMLURLLoader(ScriptRuntime.toString(value)); // Throws MalformedURLException
        } catch (UndeclaredVariableException e) {
          throw new VXMLEvent("error.semantic.undeclared", e);
        } catch (InvalidExpressionException e) {
          throw new VXMLEvent("error.syntactic.badexpression", e);
        } catch (MalformedURLException e) {
          throw new VXMLEvent("error.badfetch.malformedurl", e);
        }
       // Else, if there is a nameexpr, evaluate it in the scope and interpret the result as the target name (nameexpr overrides name)
      } else if (nameexpr != null) {
        try {
          Object value = nameexpr.evaluate(scope);                         // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
          name = ScriptRuntime.toString(value);
        } catch (UndeclaredVariableException e) {
          throw new VXMLEvent("error.semantic.undeclared", e);
        } catch (InvalidExpressionException e) {
          throw new VXMLEvent("error.syntactic.badexpression", e);
        }
      }
    }

    return profile();                                                 // Throws VXMLEvent
  }

   /** Acquire the SpeakerProfile */
  public SpeakerProfile profile() throws VXMLEvent {
	  
	Cache speakerCache = session.getSpeakerCache();

     // A url overrides a name
    if (url == null) {
      // If there is no URL, IT IS ASSUMED THAT THE NAMED SPEAKER'S PROFILE IS ALREADY IN THE SPEAKER DIRECTORY
      if (name != null) return new SpeakerProfile(name + "base", name, "base");
      else              throw new VXMLEvent("error.badfetch.nourl");
    }

    SpeakerProfile profile = null;

     // Extract the speaker's name from the URL
    String fileName = url.getFile();
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SPEAKER_MASK))
      SysLog.println("Speaker.profile: filename = \"" + StringLiteralSupport.stringLiteralFromUnicode(fileName) + "\"");
    int slash = fileName.lastIndexOf('/') + 1;
    int dot = fileName.indexOf('.', slash);
    name = (dot < 0) ? fileName.substring(slash) : fileName.substring(slash, dot);
     // name = name.replace('?', '&');      // WinCE cannot deal with '?' in file names
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SPEAKER_MASK))
      SysLog.println("Speaker.profile: name = \"" + StringLiteralSupport.stringLiteralFromUnicode(name) + "\"");

     // If the speaker profile was prefetched, and the URL was not computed on the fly, and the prefetch policy still stands, ignore "safe" caching
    int cachingPolicy;
    if (prefetchedProfile != null && expr == null && prefetch())
      cachingPolicy = CachingConverter.FAST_INDEX;
    else
      cachingPolicy = (caching != CachingConverter.NULL_INDEX) ? caching : ((Integer)BrowserProperty.value(BrowserProperty.CACHING_INDEX)).intValue();

     // If the caching policy is "safe", or the URL is not in the cache, acquire the URL anew
    Object cachedProfile = null;
    if (cachingPolicy == CachingConverter.SAFE_INDEX || !((cachedProfile = speakerCache.get(url)) instanceof SpeakerProfile)) {
      try {
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SPEAKER_MASK)) {
          if (cachingPolicy == CachingConverter.SAFE_INDEX) SysLog.println("Speaker.profile: caching policy is \"safe\", getting from URL");
          else if (cachedProfile == null) SysLog.println("Speaker.profile: no such cache entry, getting from URL");
          else SysLog.println("Speaker.profile: cached object is not a SpeakerProfile, getting from URL");
        }
         // Get a ViseSpeakerStream to load into the SpeakerManager
        javax.speech.recognition.Recognizer recognizer = session.getRecognizer().getSpeechRecognizer();
        if (recognizer == null) throw new VXMLEvent("error.vise.speaker.norecognizer");
        SpeakerManager sm = recognizer.getSpeakerManager();              // Throws SecurityException
        if (sm == null) throw new VXMLEvent("error.vise.speaker.nomanager");
        byte[] voiData = url.bytes(fetchtimeout);                        // Throws VXMLEvent
        ByteArrayInputStream bis = new ByteArrayInputStream(voiData);
        ViseSpeakerStream spkrStream = new ViseSpeakerStream(name, bis); // Throws IOException
         // Load the ViseSpeakerStream into the SpeakerManager and return its SpeakerProfile
        profile = sm.readVendorSpeakerProfile(spkrStream.inputStream()); // Throws IOException and VendorDataException
         // Cache the profile with room for a backup copy
        speakerCache.put(url, profile, voiData.length * 2, new SpeakerRemover());
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SPEAKER_MASK)) {
          cachedProfile = speakerCache.get(url);
          if (cachedProfile == null) SysLog.println("Speaker.profile: CACHE FAILURE - null");
          else if (!cachedProfile.equals(profile)) SysLog.println("Speaker.profile:  CACHE FAILURE - unequal");
          else SysLog.println("Speaker.profile: cache holds " + speakerCache.size() + " entries in " + speakerCache.totalBytes() + " bytes");
        }
      } catch (SecurityException e) {
        throw new VXMLEvent("error.vise.speaker.nomanager", e);
      } catch (VendorDataException e) {
        throw new VXMLEvent("error.vise.speaker.badimage", e);
      } catch (IOException e) {
        throw new RuntimeException("Speaker.profile: UNEXPECTED " + e.toString());
      }
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SPEAKER_MASK))
        SysLog.println("Speaker.profile: acquired speaker profile \"" + StringLiteralSupport.stringLiteralFromUnicode(name) + "\" from URL");
    } else {
      profile = (SpeakerProfile)cachedProfile;
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SPEAKER_MASK)) {
        if (prefetchedProfile == null)
          SysLog.println("Speaker.profile: acquired speaker profile \"" + StringLiteralSupport.stringLiteralFromUnicode(name) + "\" from cache");
        else
          SysLog.println("Speaker.profile: acquired prefetched speaker profile \"" + StringLiteralSupport.stringLiteralFromUnicode(name) + "\" from cache");
      }
    }
    return profile;
  }

  class SpeakerRemover implements Cache.Remover {
    public void remove(Object element) {
      try {
        if (element instanceof SpeakerProfile) {
          javax.speech.recognition.Recognizer recognizer = session.getRecognizer().getSpeechRecognizer();
          if (recognizer != null) {
            SpeakerManager sm = recognizer.getSpeakerManager();          // Throws SecurityException
            if (sm != null) {
              sm.deleteSpeaker((SpeakerProfile)element);                 // Throws IllegalArgumentException
              if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SPEAKER_MASK))
                SysLog.println("Speaker.SpeakerRemover.remove: removed speaker profile \"" + StringLiteralSupport.stringLiteralFromUnicode(((SpeakerProfile)element).getName()) + "\" from cache");
            }
          }
        }
      } catch (SecurityException e) {
      } catch (IllegalArgumentException e) {}
    }
  }
} // class Speaker
