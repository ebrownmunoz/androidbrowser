package voxware.browser;

 // Use a Converter to convert, say, a duration String ("1.2s", "1200ms", etc.) into Integer milliseconds
interface Converter {
  public Object convert(Object value) throws VXMLEvent;
}
