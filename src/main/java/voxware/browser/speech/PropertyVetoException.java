/**
 * 
 */
package voxware.browser.speech;

/**
 * @author eric
 *
 */
public class PropertyVetoException extends Exception {

	/**
	 * 
	 */
	public PropertyVetoException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param detailMessage
	 */
	public PropertyVetoException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param throwable
	 */
	public PropertyVetoException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param detailMessage
	 * @param throwable
	 */
	public PropertyVetoException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

}
