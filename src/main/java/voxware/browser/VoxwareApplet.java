package voxware.browser;

import java.io.*;

 // JSAPI Imports
import javax.speech.*;
import javax.speech.recognition.*;
import voxware.engine.recognition.sapivise.*;
import voxware.engine.audioplayer.*;

 // JavaScript Imports
import org.mozilla.javascript.*;

 // Voxware Imports
import voxware.browser.*;
import voxware.util.SysLog;

public class VoxwareApplet extends ResultAdapter {

  public javax.speech.recognition.Recognizer         rec;
  public voxware.engine.audioplayer.AsyncAudioPlayer player;

  public void initialize(javax.speech.recognition.Recognizer rec, voxware.engine.audioplayer.AsyncAudioPlayer player) {

    if (rec == null) {
      if (SysLog.printLevel >= 1) SysLog.println("VoxwareApplet: no Recognizer supplied, defaulting to debug mode");
    } else {
      this.rec = rec;
       // Add the listener to get results
      rec.addResultListener(this);
    }

    if (player == null) {
      if (SysLog.printLevel >= 1) SysLog.println("VoxwareApplet: no AsyncAudioPlayer supplied, defaulting to debug mode");
    } else {
      this.player = player;
    }
  }
    
  public void teardown() {
    if (rec != null)
      rec.removeResultListener(this);
  }

  public synchronized void resultAccepted(ResultEvent e) {
    SysLog.println("ERROR: VoxwareApplet.resultAccepted() called");
  }

  public synchronized void resultRejected(ResultEvent e) {
    SysLog.println("ERROR: VoxwareApplet.resultRejected() called");
  }

   // Return a Scriptable, String, Boolean or Number
  public synchronized Object run(Scriptable variables) throws VXMLEvent {
    SysLog.println("ERROR: VoxwareApplet.run(Scriptable) called");
    throw new VXMLEvent("error.unsupported.object", "run(Scriptable) not overridden");
  }

   // Return a Variables (DEPRECATED)
  public synchronized Variables run(Variables variables) {
    SysLog.println("ERROR: VoxwareApplet.run(Variables) called");
    return null;
  }

   // Utility method to convert a JavaScript Array of Strings to a String[]
   // The source argument must be a Scriptable or FlattenedObject embodying a JavaScript Array
  protected String[] toStringArray(Object source, String name) throws VXMLEvent, IllegalArgumentException {
    String[] result = null;
    if (source != null && source != Undefined.instance) {
      Scriptable resultObject = null;
      if (source instanceof FlattenedObject) resultObject = ((FlattenedObject) source).getObject();
      else if (source instanceof Scriptable) resultObject = (Scriptable) source;
      else throw new VXMLEvent("error.applet.illegalparameter", "\"" + name + "\" parameter of " + getClass().getName() + " applet is a " + source.getClass().getName());
       // Construct a Java String[] of the elements
      if (resultObject != Undefined.instance) {
        Object[] elements = Context.getCurrentContext().getElements(resultObject);  // Throws IllegalArgumentException
        result = new String[elements.length];
        Object element;
        for (int i = 0; i < elements.length; i++) {
          element = elements[i];
          if (element == Context.getUndefinedValue() || element == null) result[i] = null;
          else                                                           result[i] = Context.toString(element);
        }
      }

    }
    return result;
  }

}
