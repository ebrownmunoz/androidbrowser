package voxware.browser;

import voxware.util.SysLog;

/** A VXML browser */
public class AuxiliaryTest implements Runnable {

  public void run() {

    Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

    SysLog.println("voxware.browser.AuxiliaryTest: running");
    try {
      throw new Exception("Test Exception");
    } catch (Exception e) {
      SysLog.println("voxware.browser.AuxiliaryTest: caught test exception \"" + e.toString() + "\"");
    } finally {
      SysLog.println("voxware.browser.AuxiliaryTest: in \"finally\" clause");
    }
    SysLog.println("voxware.browser.AuxiliaryTest: exiting");
  }

}

