package voxware.browser;

import java.io.ByteArrayInputStream;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import voxware.engine.audioplayer.*;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.*;

 /** A static utility for parsing <value> nodes */
class ValueNode {

   // Parse a <value> node and return its value as a String, a VoxwareInputStream, or a Vector of VoxwareInputStreams
  public static Object parse(Node node, Scope scope) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    Object       retval = null;
    NamedNodeMap nnm = node.getAttributes();
    Node         attrNode = null;
    boolean      useTTS = true;
    String       recsrc = null;
    String       sayas = null;     // <sayas> class (currently ignored)
    if (nnm != null) {
       // Get the value of the expression to render
      if ((attrNode = nnm.getNamedItem("expr")) != null)
        retval = (new Expr(attrNode.getNodeValue(), scope)).evaluate(scope); // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
      else if ((attrNode = nnm.getNamedItem("name")) != null)                // DEPRECATED
        retval = (new Expr(attrNode.getNodeValue(), scope)).evaluate(scope); // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
      else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
        SysLog.println("AudioPlayer.parseValueNode: WARNING - missing \"expr\" attribute");
       // Get the rendering parameters
      sayas = ((attrNode = nnm.getNamedItem("class")) != null) ? attrNode.getNodeValue() : null;
      useTTS = (attrNode = nnm.getNamedItem("mode")) == null || !attrNode.getNodeValue().equalsIgnoreCase("recorded");
      recsrc = ((attrNode = nnm.getNamedItem("recsrc")) != null) ? attrNode.getNodeValue() : null;
    }
     // Render the value of the expression
    if (retval != null) {
      if (retval instanceof byte[]) {
         // Since the value is already an array of bytes, just wrap it in a VoxwareInputStream
        try {
          retval = (Object)(new VoxwareInputStream(new ByteArrayInputStream((byte[])retval), "<byte array>")); // Throws AudioAllocationException
        } catch (AudioAllocationException e) {
          throw new VXMLEvent("error.audio.unplayable", e);
        }
      } else if (useTTS || recsrc == null) {
         // Just turn the value into a String for the TTS to render
        retval = ScriptRuntime.toString(retval);
      } else {
         // Return a rendering of the value as a VoxwareVector (of VoxwareInputStreams), if possible; otherwise, return a String
        retval = AudioRenderer.rendering(retval, recsrc, sayas);
      }
    }
    return retval;
  }
}
