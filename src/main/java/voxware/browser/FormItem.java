package voxware.browser;

import java.util.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

 /** A FormItem
   * Implements behavior common to Form items: Field, Initial, Block, VXMLObj, Record and Subdialog */
class FormItem extends ScopeElement {

  protected Element    element;       // The DOM element
  protected Variable   guard;         // The guard variable
  protected Expr       condition;
  protected final Session session;
  
   /** Construct a childless, orphan FormItem from an element in a DOM tree */
  public FormItem(Session session, Element element) throws DOMException, InvalidExpressionException {
    super();
    
    this.session = session;

    this.element = element;

     // Get the name
    Attr attr = element.getAttributeNode("name");
    name = (attr != null) ? attr.getNodeValue() : null; // Throws DOMException
     // Get the initial value of the guard variable (OPTIONAL)
    attr = element.getAttributeNode("expr");
     // Create the guard variable (throws DOMException, InvalidExpressionException)
    guard = new Variable((attr != null) ? new Expr(attr.getNodeValue(), null) : new Expr(VXMLTypes.PROTO_UNDEFINED));
     // Get the condition expression (OPTIONAL)
    attr = element.getAttributeNode("cond");
     // Create the condition expression (throws DOMException, InvalidExpressionException)
    condition = (attr != null) ? new Expr(attr.getNodeValue(), null) : new Expr(Boolean.TRUE);
  }

   /** Return the Element */
  public Element element() {
    return element;
  }

   /** Make this a child of a ScopeElement (Form) (extends makeChildOf() in ScopeElement to declare the guard variable) */
  public void makeChildOf(ScopeElement parent) throws VXMLEvent {
     // Have the parent adopt this child
    super.makeChildOf(parent);  // Throws VXMLEvent
     // Declare the guard Variable in the parent scope
    parent.putVariable(name, guard);
  }

   /** Return the value of the conditional expression */
  public boolean condition() throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    return Context.toBoolean(condition.evaluate(this));
  }

   /** Clear the FormItem */
  public void clear(boolean undefine) {
    if (prompts != null) prompts.resetPromptCount();
    if (handler != null) handler.resetEventCounts();
    if (undefine && name != null && name.length() > 0) {
      try {
        setProperty(name, VXMLTypes.PROTO_UNDEFINED);  // Throws UndeclaredVariableException
      } catch (UndeclaredVariableException e) {
        throw new RuntimeException("Form.clearFormItems(): UNEXPECTED " + e.toString());
      }
    }
  }

   /** Make a Filled and register it in the parent Form's scope */
  public Filled makeFilled(Element element, Goto context, boolean preprocess) throws VXMLEvent {
    Filled filled = new Filled(session, element, context, name, preprocess);  // Throws VXMLEvent
    ((Form)parent).addFilled(filled);
    return filled;
  }
}
