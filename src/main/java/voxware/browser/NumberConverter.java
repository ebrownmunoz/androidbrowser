package voxware.browser;

import java.util.*;
import org.mozilla.javascript.*;

import voxware.browser.*;

 // A singleton Converter for number designators
class NumberConverter implements Converter {

  private static NumberConverter converter = null;

  public static NumberConverter instance() {
    if (converter == null) converter = new NumberConverter();
    return converter;
  }

  public Object convert(Object value) throws VXMLEvent {
    return new Double(ScriptRuntime.toNumber(value));
  }
}
