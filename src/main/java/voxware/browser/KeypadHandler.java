package voxware.browser;

import java.util.*;
import voxware.engine.recognition.*;
import voxware.hw.event.*;                                  // Pacer/Endeavor hardware imports

import voxware.browser.*;
import voxware.util.SysLog;

 /** The Keypad Handler */
public class KeypadHandler implements HwEventListener {

   // Instantiate this singleton class by calling the static method KeypadHandler.getInstance()

  public static int  NUMBER_OF_KEYS = 4;                    // Set this before first calling getInstance() to get other values
  public static long MIN_DURATION = 50L;                    // The minimum duration of a significant keypad event (in milliseconds)

  private static KeypadHandler ego = null;                  // The unique KeypadHandler

  protected HwEventSource source = null;
  protected Vector tokenListeners = null;                   // Vector of TokenListeners
  protected long[] onset;                                   // The time of the onset of the most recent event for each key

   // Only getInstance calls this
  protected KeypadHandler(int numKeys) {
    source = HwEventSource.getInstance();
    tokenListeners = new Vector();
    onset = new long[numKeys];
  }

   // Instantiate the KeypadHandler -- may safely be called any number of times
  public static KeypadHandler getInstance() {
    if (ego == null) ego = new KeypadHandler(NUMBER_OF_KEYS);
    return ego;
  }

  /// Implementation of the HwEventListener interface

   // Handle a ButtonHwEvent
  public synchronized void eventArrived(ButtonHwEvent hwEvent) {
    long now = System.currentTimeMillis();
    int id = hwEvent.getButtonID();
    int state = hwEvent.getState();
    if (now - onset[id] > MIN_DURATION) {
      onset[id] = now;
      if (state == ButtonHwEvent.BUTTON_RELEASED) {
         // Send a token of the keypress to each TokenListener
        Enumeration listeners = tokenListeners.elements();
        while (listeners.hasMoreElements()) {
          TokenEvent tokenEvent = new TokenEvent(this, Integer.toString(id));
          ((TokenListener)listeners.nextElement()).token(tokenEvent);
        }
      }
    }
  }
    
   // The InterruptHandler handles all events other than ButtonHwEvents
  public void eventArrived(BatteryChargeHwEvent hwEvent) {}
  public void eventArrived(BatteryLowHwEvent hwEvent) {}
  public void eventArrived(RadioStatusHwEvent hwEvent) {}
  public void eventArrived(StandbyHwEvent hwEvent) {}

  /// End of implementation of HwEventListener interface

   // Turn the source on
  public synchronized void enable() {
    long now = System.currentTimeMillis();
    for (int i = 0; i < onset.length; i++) {
      onset[i] = now;
      source.addHwButtonListener(this, i);
    }
  }

   // Turn the source off
  public synchronized void disable() {
    for (int i = 0; i < onset.length; i++)
      source.removeHwButtonListener(this, i);
  }

   // Add a tokenListener
  public synchronized void addTokenListener(TokenListener tokenListener) {
    tokenListeners.addElement(tokenListener);
  }

   // Remove a tokenListener
  public synchronized void removeTokenListener(TokenListener tokenListener) {
    if (!tokenListeners.removeElement(tokenListener) && SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.KEYPAD_MASK)) {
      SysLog.println("KeypadHandler::removeTokenListener(): ERROR - can't remove TokenListener");
    }
  }
}
