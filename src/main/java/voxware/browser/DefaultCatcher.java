package voxware.browser;

import java.util.Vector;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

 /** A Default VXML Event Catcher for a given event */
class DefaultCatcher extends Catcher {

  private boolean reprompt;  // Reprompt?
  private boolean exit;      // Exit?
  private String  urlName;   // The URL of the waveform file
  private String  message;   // Message to play, or null if none

   /** Construct a DefaultCatcher from arguments */
  public DefaultCatcher(Session session, String event, boolean reprompt, boolean exit, String urlName, String message) {

    super(session);

    this.event = event;
    this.reprompt = reprompt;
    this.exit = exit;
    this.urlName = urlName;
    this.message = message;

    this.count = 1;
    condition = new Expr(Boolean.TRUE);

    expandEventName();
  }

   /** Execute the DefaultCatcher */
  public Variables interpret(ScopeElement scope, Goto context) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
    if (SysLog.printLevel >= 1)
      SysLog.println("DefaultCatcher.Interpret(): caught event \"" + event + "\" in scope \"" + scope.getClass().getName() + "\"");

    AudioPlayer player = session.getAudioPlayer();
    Vector playables = null;
     // Try to play the waveform file, if any
    try {
      playables = player.playables(urlName);
      if (playables != null) player.play(scope, playables);
    } catch (VXMLEvent e) {}
     // Otherwise, try to play the message using TTS
    try {
      if (playables == null) player.play(scope, message);
    } catch (VXMLEvent e) {}

    context.noPrompt(!reprompt);

    if (exit) throw new VXMLExit(message);
    return null;
  }
} // class DefaultCatcher
