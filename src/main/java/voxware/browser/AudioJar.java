package voxware.browser;

import java.io.*;
import java.util.*;
import java.util.zip.*;

import voxware.util.SysLog;


 /** An AudioJar */
class AudioJar extends Hashtable {

  // An AudioJar is a Hashtable of Vectors of AudioSegments hashed by the first word in the segment

  // An AudioSegment embodies the audio data for a word or sequence of words intended for use in a particular context.

  // The left and right couplers (arbitrary Strings) define the context. The couplerGlue glues the couplers to the word 
  // sequence. For example, init_foo_boo, a version of the word "foo" with left coupler "init" and right coupler "boo",
  // is intended for use in initial position preceding any word whose left coupler is "boo". init_one+two_boo is similarly
  // a two-word sequence, "one two", intended for use in the same context. Since init_foo_boo contains a single word,
  // its subsequentWords Vector will be null, whereas subsequentWords for init_one+two_boo is a Vector of length one
  // containing a single String, "two".

  // Static fields

  public static final int    DefaultZipChunkSize    = 8192;

  public static final int    DefaultCouplerGlue     = '_';
  public static final String DefaultWordSeparators  = "+";
  public static final int    DefaultExtensionGlue   = '.';
  public static final int    DefaultDirectoryGlue   = '/';
  public static final String DefaultManifestPrefix  = "META-INF/";
  public static final int    DefaultMaxCouplerTypes = 50;

   // Default coupler names
  public static String DefaultWildcardCoupler = "";         // Empty String ALWAYS represents a wildcard
  public static String DefaultInitialCoupler  = "init";
  public static String DefaultTerminalCoupler = "fin";

  // Public fields

   // Delimiters
  public int    couplerGlue     = DefaultCouplerGlue;       // Must be a single character
  public String wordSeparators  = DefaultWordSeparators;    // May be any set of characters

   // Coupler names
  public String wildcardCoupler = DefaultWildcardCoupler;
  public String initialCoupler  = DefaultInitialCoupler;
  public String terminalCoupler = DefaultTerminalCoupler;

  // Private fields

   // Coupler types
  public static final int WildcardCouplerType = 0;
  public static final int InitialCouplerType  = WildcardCouplerType + 1;
  public static final int TerminalCouplerType = InitialCouplerType + 1;
  private             int numCouplerTypes     = TerminalCouplerType + 1;

   // Hashtable of coupler types hashed by name
  private Hashtable couplerTypes = new Hashtable(DefaultMaxCouplerTypes);

   // Initialize the Hashtable of coupler types
  private void initCouplerTypes() {
    if (couplerTypes == null) couplerTypes = new Hashtable(DefaultMaxCouplerTypes);
    couplerTypes.put(wildcardCoupler, new Integer(WildcardCouplerType));
    couplerTypes.put(initialCoupler,  new Integer(InitialCouplerType));
    couplerTypes.put(terminalCoupler, new Integer(TerminalCouplerType));
  }

   // Return the type of a coupler, creating it if necessary
  private int couplerType(String couplerName) {
    Integer type = (Integer)couplerTypes.get(couplerName);
    if (type == null)
      couplerTypes.put(couplerName, (type = new Integer(numCouplerTypes++)));
    return type.intValue();
  }

   // Construct an empty AudioJar
  public AudioJar() {
    super();
  }

   // Construct an AudioJar from the given ZipInputStream
  public AudioJar(ZipInputStream zin) throws ZipException, IOException {
    this();
    this.populate(zin);
  }

   // Set the Initial Coupler
  public void initialCoupler(String initialCoupler) {
    this.initialCoupler = initialCoupler;
  }

   // Set the Terminal Coupler
  public void terminalCoupler(String terminalCoupler) {
    this.terminalCoupler = terminalCoupler;
  }

   // Set the Word Separators
  public void wordSeparators(String wordSeparators) {
    this.wordSeparators = wordSeparators;
  }

   // Populate an AudioJar from the given ZipInputStream
  public void populate(ZipInputStream zin) throws ZipException, IOException {
    initCouplerTypes();
    ZipEntry zentry;
    String   zname;                          // The name of the ZipEntry
    while (true) {
      try {
        zentry = zin.getNextEntry();         // Throws IOException
        if (zentry == null) break;           // Assume this is the end of the file
        if (zentry.isDirectory()) continue;  // Ignore directories
        zname = zentry.getName();
        if (zname.startsWith(DefaultManifestPrefix)) continue;  // Ignore the manifest

         // Read the ZipEntry data into a byte[]
        int zsize = (int)zentry.getSize();   // Uncompressed size of the data in the ZipEntry
        byte[] zdata = bytes(zin, zsize);

         // Strip excrescences from the name
        int     delimiter;
         // Discard the path, if any
        delimiter = zname.lastIndexOf(DefaultDirectoryGlue);
        if (delimiter >= 0) zname = zname.substring(delimiter + 1);
         // Discard the file extension, if any
        delimiter = zname.lastIndexOf(DefaultExtensionGlue);
        if (delimiter > 0) zname = zname.substring(0, delimiter);

        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
          SysLog.println("AudioJar.populate: entry for \"" + zname + "\" uncompressed to " + zdata.length + " bytes");

         // Parse the name to get the word sequence and its left and right contexts
        String  wname;                       // The word sequence
        int     wstart = zname.indexOf(couplerGlue);
        int     wend;
        int     leftCoupler;
        int     rightCoupler;
        AudioSegment segment;
        if (wstart < 0) {
           // The unadorned name is an abbreviation for the eponymous word with wildcard left and right couplers
          wname = zname;
          leftCoupler = WildcardCouplerType;
          rightCoupler = WildcardCouplerType;
        } else {
          wend = zname.lastIndexOf(couplerGlue);
          if (wend == wstart)
            throw new ZipException("AudioJar.populate: ill-formed ZipEntry name \"" + zname + "\"");
           // Empty coupler names always denote wildcards, even if the wildcard coupler name is changed
          leftCoupler = (wstart == 0) ? WildcardCouplerType : couplerType(zname.substring(0, wstart));
          wname = zname.substring(++wstart, wend++);
          rightCoupler = (wend == zname.length()) ? WildcardCouplerType : couplerType(zname.substring(wend));
        }

         // Construct the AudioSegment for this word or word sequence in the specified context
        segment = new AudioSegment(leftCoupler, rightCoupler, zdata);

         // File the AudioSegment under the first word in the word sequence
        StringTokenizer wordSequence = new StringTokenizer(wname, wordSeparators);
        segment.firstWord = wordSequence.nextToken();
        Object segments = this.get(segment.firstWord);
        if (segments == null) {
           // There are no existing segments; just put the new one directly into the Hashtable
          this.put(segment.firstWord, segment);
        } else {
          Vector newSegments;
          if (segments instanceof AudioSegment) {
             // There's already a single segment there; construct a Vector to hold it and the new one
            newSegments = new Vector();
            newSegments.addElement(segments);
             // Keep the Vector sorted in ascending order by left coupler type
            if (((AudioSegment)segments).leftCoupler <= leftCoupler)
              newSegments.addElement(segment);
            else
              newSegments.insertElementAt(segment, 0);
             // Replace the old AudioSegment with the new Vector of AudioSegments in the Hashtable
            this.put(segment.firstWord, newSegments);
          } else {
             // There's already a Vector of segments; insert the new segment into it
            newSegments = (Vector)segments;
             // Keep the Vector sorted in ascending order by left coupler type
            int numContexts = newSegments.size();
            int index = numContexts;
            while (index-- > 0) {
              AudioSegment context = (AudioSegment)newSegments.elementAt(index);
              if (context.leftCoupler <= leftCoupler) break;
            }
            newSegments.insertElementAt(segment, index + 1);
          }
        }
         // Construct a Vector of the subsequent words in the phrase, if any
        int numSubsequentWords = wordSequence.countTokens();
        if (numSubsequentWords > 0) {
           // There are subsequent words in the phrase, so construct a Vector to hold them
          segment.subsequentWords = new Vector(numSubsequentWords);
          do {
            segment.subsequentWords.addElement(wordSequence.nextToken());
          } while (wordSequence.hasMoreTokens());
        }
      } catch (EOFException eof) {}
    }
  }

   // Read zsize bytes from the current ZipEntry in a ZipInputStream, or all the bytes in the entry if zsize < 0
  private byte[] bytes(ZipInputStream zin, int zsize) throws IOException {

    int zalloc = (zsize < 0) ? DefaultZipChunkSize : zsize;  // The size of the zipchunks to be read
    Vector zchunks = null;                                   // Vector of zipchunks
    byte[] zdata = new byte[zalloc];                         // The first zipchunk
    int ztotal = 0;                                          // The total number of bytes read
    int zread;                                               // The number of bytes read by a call to read()
    int zrequested = zalloc;                                 // The requested size of the first read

    while (true) {
      zread = zin.read(zdata, zalloc - zrequested, zrequested);  // Throws IOException
      if (zread < 0) break;
      ztotal += zread;
      if (ztotal == zsize) break;
      zrequested -= zread;
       // If this chunk is full, queue it and construct another
      if (zrequested == 0) {
        if (zchunks == null) zchunks = new Vector();
        zchunks.addElement(zdata);
        zdata = new byte[zalloc];
        zrequested = zalloc;                                 // The requested size of the next read
      }
    }

     // If the size was not known beforehand, allocate a byte[] of the actual size and copy the zipchunks into it
    if (zsize < 0) {
      if (ztotal != DefaultZipChunkSize) {
        if (zchunks == null) zchunks = new Vector();
        zchunks.addElement(zdata);
        zdata = new byte[ztotal];
        int idx = 0;
        while (ztotal >= DefaultZipChunkSize) {
          System.arraycopy((byte[])zchunks.elementAt(idx), 0, zdata, idx * DefaultZipChunkSize, DefaultZipChunkSize);
          ztotal -= DefaultZipChunkSize;
          idx++;
        }
        if (ztotal > 0)
          System.arraycopy((byte[])zchunks.elementAt(idx), 0, zdata, idx * DefaultZipChunkSize, ztotal);
      }
    }

    return zdata;
  }


   // Return a Vector of the AudioSegments that start with the specified word and have the specified or wildcard left coupler
  Vector audioSegments(int leftCoupler, String wordName) {
    Vector segments = null;
    Object entry = this.get(wordName);
    if (entry != null) {
      AudioSegment leftContext;
      if (entry instanceof AudioSegment) {
        leftContext = (AudioSegment)entry;
        if (leftContext.leftCoupler == leftCoupler || leftContext.leftCoupler == WildcardCouplerType) {
          segments = new Vector(1);
          segments.addElement((AudioSegment)entry);
        }
      } else if (entry instanceof Vector) {
        Enumeration leftContexts = ((Vector)entry).elements();
        while (leftContexts.hasMoreElements()) {
          leftContext = (AudioSegment)leftContexts.nextElement();
          if (leftContext.leftCoupler > leftCoupler) break;  // Because of the ordering of the segments, we need look no farther
          if (leftContext.leftCoupler == leftCoupler || leftContext.leftCoupler == WildcardCouplerType) {
            if (segments == null) segments = new Vector();
            segments.addElement(leftContext);
          }
        }
      } else {
        throw new RuntimeException("AudioJar.audioSegments: UNEXPECTED entry of type " + entry.getClass().getName());
      }
    }
    return segments;
  }
}
