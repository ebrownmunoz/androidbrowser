package voxware.browser;

import java.util.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.speech.*;
import javax.speech.recognition.*;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;
import voxware.util.SysTimer;

 /** An Initial
   * An Initial's parent is a Form and it has no children */
class Initial extends FormItem {

  static int instanceCount = 0;

  private Scriptable   shadow;                                        // The name$ shadow variable to contain raw input
  
   /** Construct a childless, orphan Initial from a <initial> element in a DOM tree */
  public Initial(Session session, Element initial) throws DOMException, InvalidExpressionException, NumberFormatException {
    super(session, initial);                                                   // Throws DOMException, InvalidExpressionException
    
    prefix = "dialog";
    this.setIndex(Elements.INITIAL_INDEX);

    handler = new EventHandler();
    prompts = new Prompts();
    
     // Supply an internal name if none has been given
    if (name == null)
      name = "_INITIAL" + Integer.toString(++instanceCount);
  }

   /** Make this a child of a ScopeElement (Form) (extends makeChildOf() in FormItem to inherit the parent's JavaScript scope) */
  public void makeChildOf(ScopeElement parent) throws VXMLEvent {
     // Inherit the parent's JavaScript scope 
    scope = parent.scope();
     // Have the parent adopt this child
    super.makeChildOf(parent);                                        // Throws VXMLEvent
     // Inherit the parent ScopeElement's Grammars
    grammars = ((ScopeElement)parent).grammars();
     // Construct a shadow variable
    shadow = newObject();
    scope.put(name + "$", scope, shadow);
    shadow.put("confidence", shadow, VXMLTypes.PROTO_UNDEFINED);
  }

   /** Interpret the Initial */
  public Variables interpret(Goto whereToGo, InterruptHandler interruptHandler) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
     // Implement the contents of "if(an initial is chosen)" from Appendix C of the VXML Spec
     // Clear the guard variable, in case this visit is the result of a form item goto
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.INTERPRET_MASK))
      SysLog.println("Initial.interpret(): starting to interpret <initial>");
    Scope.setProp(scope, name, VXMLTypes.PROTO_UNDEFINED);
    Variables retVars = null;
    interruptHandler.pushScope(this);
    try {

      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.enter(element, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request entering <initial> " + name);

      modifyProperties();

       // Acquire the Recognizer
      voxware.browser.Recognizer recognizer = session.getRecognizer();
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.INTERPRET_MASK))
        SysLog.println("Initial.interpret(): acquired recognizer " + recognizer.toString());

       // Recognize using this scope's speaker and grammars, optionally queueing up the appropriate prompt(s) and playing them
      try {
        recognizer.recognize(this, !whereToGo.noPrompt() || (whereToGo.lastScope() != this.hashcode()));  // Throws InterruptedException, VXMLEvent
      } catch (InterruptedException ie) {
        recognizer.clearResult();
      }

       // Process the recognition results
      try {
        retVars = recognizer.results(this, name, shadow, whereToGo);  // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
        if (retVars != null) {
          boolean fieldItemFilled = false;
           // Assign the values returned by recognition to the appropriate variables
          Enumeration names = retVars.keys();
          while (names.hasMoreElements()) {
            String varName = (String)names.nextElement();
            Object value = retVars.getByName(varName).value();        // Throws UndeclaredVariableException
            setProperty(varName, value);
            if (!fieldItemFilled && value != VXMLTypes.PROTO_UNDEFINED && ((Form)parent).isFieldItemName(varName))
              fieldItemFilled = true;
          }
           // Set the guard variables of all the Initials to true
          if (fieldItemFilled) {
            Enumeration initials = ((Form)parent).initials().elements();
            while (initials.hasMoreElements())
              setProperty(((Initial)initials.nextElement()).name(), Boolean.TRUE);
          }
        } else {
          if (SysLog.printLevel > 0) SysLog.println("Initial.interpret(): ERROR - recognizer returns null Variables");
        }
      } catch (UndeclaredVariableException e) {
        if (SysLog.printLevel > 0) SysLog.println("Initial.interpret(): ERROR - " + e.toString());
        throw new VXMLEvent("error.semantic.undeclared", e);
      } catch (InvalidExpressionException e) {
        if (SysLog.printLevel > 0) SysLog.println("Initial.interpret(): ERROR - " + e.toString());
        throw new VXMLEvent("error.syntactic.badexpression", e);
      }

     // Catch any VXMLEvent in this Initial's scope
    } catch (VXMLEvent event) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.CATCH_MASK))
        SysLog.println("Initial.interpret(): <initial> \"" + name + "\" caught VXMLEvent \"" + event.name() + "\"");
      retVars = catchEvent(event, whereToGo);                         // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
    } finally {
      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.leave(element, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request leaving <initial> " + name);
      restoreProperties();
      interruptHandler.popScope();
    }
    return retVars;
  }
} // class Initial
