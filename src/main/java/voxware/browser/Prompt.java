package voxware.browser;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.ScriptRuntime;

import voxware.engine.audioplayer.AsyncListener;

import voxware.browser.*;

 /** A Prompt */
class Prompt implements Count {
  private Element  prompt;                                   // The <prompt> element in the DOM tree
  private ScopeElement parent;                               // <value> requires access to the parent's vars
  private String   promptString;
  private int      bargein = -1;                             // -1 == unspecified; 0 == false; 1 == true;  
  private int      timeout = DurationConverter.UNSPECIFIED;  // Recognition timeout for the following recognition
  private int      count;                                    // The prompt count
  private Expr     cexpr;                                    // Expression to evaluate to determine whether or not to play this prompt (OPTIONAL)
  private boolean  cond = true;                              // The value of the preceeding conditional expression
  private final Session session;
  
   /** Construct a Prompt from a <prompt> element in the DOM tree */
  public Prompt(Session session, Element prompt) throws DOMException, InvalidExpressionException, NumberFormatException, VXMLEvent {
    this(session, prompt, null);
  }

   /** Construct a Prompt from a <prompt> element in the DOM tree */
  public Prompt(Session session, Element prompt, ScopeElement parent) throws DOMException, InvalidExpressionException, NumberFormatException, VXMLEvent {

	this.session = session;
    this.prompt = prompt;
    this.parent = parent;

     // Get the attributes of the <prompt> tag
    Attr attr = prompt.getAttributeNode("count");   // OPTIONAL
    count = (attr != null) ? (new Integer(attr.getNodeValue())).intValue() : 1;                             // Throws DOMException, NumberFormatException
    attr = prompt.getAttributeNode("bargein");      // OPTIONAL
    if (attr != null) 
      bargein = ((Boolean)BooleanConverter.instance().convert(attr.getNodeValue())).booleanValue() ? 1 : 0; // Throws DOMException, VXMLEvent
    attr = prompt.getAttributeNode("timeout");      // OPTIONAL
    if (attr != null)
      timeout = ((Integer)DurationConverter.instance().convert(attr.getNodeValue())).intValue();            // Throws DOMException, VXMLEvent
    attr = prompt.getAttributeNode("cond");         // OPTIONAL
    if (attr != null)
      cexpr = new Expr(attr.getNodeValue(), null);                                                          // Throws DOMException, InvalidExpressionException

     // If conditional expression is constant, evaluate it now, once and for all
    try {
       // Throw UndeclaredVariableException if cexpr references any variables
      cond = shouldPrompt(null);                    // Throws UndeclaredVariableException, InvalidExpressionException, VXMLEvent
       // Don't need cexpr anymore -- nullify it to show that expression has been evaluated
      cexpr = null;
     // Otherwise, evaluate it during interpretation
    } catch (UndeclaredVariableException e) {
    }
  }

   /** Implement the Count interface */
  public int count() {
    return count;
  }
  public void set(int count) {
    this.count = count;
  }

  public boolean contains(Element element) {
    if (element == this.prompt)
      return true;
    else
      return false;
  }

  public Element element() {
    return this.prompt;
  }

  public String string() {
    return this.promptString;
  }

  public boolean bargein() {
    if (bargein < 0)
      return ((Boolean)BrowserProperty.value(BrowserProperty.BARGEIN_INDEX)).booleanValue();
    else
      return bargein == 0 ? false : true;
  }

  public int timeout() {
    return timeout;
  }

  public void timeout(int timeout) {
    this.timeout = timeout;
  }

  public ScopeElement getParent() {
    return this.parent;
  }

   /** Queue the prompt for playing in this Prompt's parent scope */
  public void play() throws VXMLEvent {
    play(this.parent, session.getRecognizer());
  }

  public void play(voxware.browser.Recognizer recognizer) throws VXMLEvent {
    play(this.parent, recognizer);
  }

   /** Queue the prompt for playing in the given scope */
  public void play(Scope parent) throws VXMLEvent {
    play(parent, session.getRecognizer());
  }

  public void play(Scope parent, voxware.browser.Recognizer recognizer) throws VXMLEvent {
    try {
      if (shouldPrompt(parent)) {
        AsyncListenerImpl listener = new AsyncListenerImpl(recognizer);
        listener.environment(this);
        recognizer.addPrompt(this);                        // Throws VXMLEvent
        session.getAudioPlayer().play(parent, prompt, listener);  // Throws VXMLEvent
      }
    } catch (UndeclaredVariableException e) {
      throw new VXMLEvent("error.semantic.undeclared", e);
    } catch (InvalidExpressionException e) {
      throw new VXMLEvent("error.syntactic.badexpression", e);
    }
  }

   // Evaluate the conditional expression in the given scope
  private boolean shouldPrompt(Scope scope) throws UndeclaredVariableException, InvalidExpressionException, VXMLEvent {
    if (cexpr != null) cond = ScriptRuntime.toBoolean(cexpr.evaluate(scope));
    return cond;
  }

   // AsyncListener Implementation
  private class AsyncListenerImpl implements AsyncListener {

    private voxware.browser.Recognizer recognizer;
    private Object                     environment;

    public AsyncListenerImpl(voxware.browser.Recognizer rec) {
      recognizer = rec;
    }

     // The Callback Routine
    public void finished() {
      try {
        recognizer.callback((Prompt)environment);
      } catch (VXMLEvent e) {
        throw new RuntimeException("AsyncListenerImpl.finished(): UNEXPECTED " + e.toString());
      }
    }

    public void environment(Object env) {
      environment = env;
    }

    public Object environment() {
      return environment;
    }
  }
} // class Prompt
