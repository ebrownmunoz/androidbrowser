package voxware.browser;

import java.util.*;
import org.mozilla.javascript.*;

import voxware.browser.*;

 // A singleton Converter for fetchhints
public class FetchHintConverter implements Converter {

  public static final int    NULL_INDEX = 0;
  public static final String NULL_STRING = "";
  public static final int    PREFETCH_INDEX = NULL_INDEX + 1;
  public static final String PREFETCH_STRING = "prefetch";
  public static final int    SAFE_INDEX = PREFETCH_INDEX + 1;
  public static final String SAFE_STRING = "safe";
  public static final int    STREAM_INDEX = SAFE_INDEX + 1;
  public static final String STREAM_STRING = "stream";

  public static final int NUM_FETCHHINTS = STREAM_INDEX + 1;

  private static FetchHintConverter converter = null;

  public static FetchHintConverter instance() {
    if (converter == null) converter = new FetchHintConverter();
    return converter;
  }

  public Object convert(Object value) throws VXMLEvent {
    int index = PREFETCH_INDEX;
    if (value instanceof String) {
      if (((String)value).equalsIgnoreCase(PREFETCH_STRING))
        index = PREFETCH_INDEX;
      else if (((String)value).equalsIgnoreCase(SAFE_STRING))
        index = SAFE_INDEX;
      else if (((String)value).equalsIgnoreCase(STREAM_STRING))
        index = STREAM_INDEX;
    } else {
      index = ScriptRuntime.toInt32(value);
    }
    if (NULL_INDEX < index && index < NUM_FETCHHINTS)
      return new Integer(index);
    else
      throw new VXMLEvent("error.attribute.badvalue");
  }

  public static String convert(int index) {
    switch(index) {
      case PREFETCH_INDEX: return PREFETCH_STRING;
      case SAFE_INDEX:     return SAFE_STRING;
      case STREAM_INDEX:   return STREAM_STRING;
      default:             return "";
    }
  }
}
