package voxware.browser;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

 /** A Block
   * A Block's parent is a Form */
class Block extends FormItem implements Executables {

  static int instanceCount = 0;

   /** Construct an orphan Block from a <block> element in a DOM tree */
  public Block(Session session, Element block, Goto context, boolean preprocess) throws DOMException, InvalidExpressionException, VXMLEvent {
    super(session, block);          // Throws DOMException, InvalidExpressionException
    prefix = "_anonymous";
    this.setIndex(Elements.BLOCK_INDEX);

     // Supply an internal name if none has been given
    if (name == null)
      name = "_BLOCK" + Integer.toString(++instanceCount);

     // Construct the ExecutableContent
    executableContent = (preprocess) ? new ExecutableContent(session, block, context) : new ExecutableContent(session, context);  // Throws VXMLEvent
  }

   /** Make this a child of a ScopeElement (Form) (extends makeChildOf() in FormItem to inherit the parent's EventHandler) */
  public void makeChildOf(ScopeElement parent) throws VXMLEvent {
     // Inherit the parent Form's EventHandler (since Block cannot contain <catch> elements)
    handler = parent.handler();
     // Have the parent Form adopt this child
    super.makeChildOf(parent);  // Throws VXMLEvent
  }

   /** Implement the Executables interface */
  private ExecutableContent executableContent;
  public ExecutableContent executableContent() {
    return executableContent;
  }

   /** Interpret the contents of the Block */
  public Variables interpret(Goto whereToGo, InterruptHandler interruptHandler) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
    Variables result = null;
     // Set the guard variable to true
    Scope.setProp(parent.scope(), name, Boolean.TRUE);
    try {
      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.enter(element, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request entering <block> " + name);
      if (prompts != null) prompts.resetPromptCount();
      initializeScope(parent);
      result = executableContent.execute(element, this, whereToGo, true);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
    } finally {
      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.leave(element, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request leaving <block> " + name);
    }
    return result;
  }
} // class Block
