package voxware.browser;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import voxware.browser.*;

 /** Wraps a DOM parser */
public interface DOMParserWrapper {
   /** Parses the specified URI and returns the Document */
  public Document parse(String uri) throws VXMLEvent; 
   /** Parses the specified InputSource and returns the Document */
  public Document parse(InputSource source) throws VXMLEvent; 
} // interface DOMParserWrapper
