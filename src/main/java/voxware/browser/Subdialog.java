package voxware.browser;

import java.util.*;
import java.net.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

 /** A Subdialog
   * A Subdialog's parent is a Form */
class Subdialog extends SubFormItem implements FieldItem {

  static int instanceCount = 0;

  private String         source;                              // Where to get the subdialog
  private String         method;                              // ("get" | "post")
  private String         enctype;                             // The MIME encoding type of the subdialog document
  private Variables      submit;
  private GotoSubdialog  whereToGo;                           // The Subdialog's context

   /** Construct an orphan Subdialog from a <subdialog> element in a DOM tree */
  public Subdialog(Session session, Element subdialog, Goto context) throws DOMException, InvalidExpressionException, InvalidTag, VXMLEvent {
    super(session, subdialog);                                         // Throws DOMException, InvalidExpressionException, VXMLEvent
    prefix = "subdialog";
    this.setIndex(Elements.OBJECT_INDEX);

    handler = new EventHandler();

     // Supply an internal name if none has been given
    if (name == null)
      name = "_SUBDIALOG" + Integer.toString(++instanceCount);

    NamedNodeMap map = subdialog.getAttributes();
    for (int j = 0; j < map.getLength(); j++) {
      Node node = map.item(j);
      String nodeName = node.getNodeName();
      if (nodeName.equalsIgnoreCase("src")) {
        source = node.getNodeValue();                         // Throws DOMException
      } else if (nodeName.equalsIgnoreCase("method")) {
        method = node.getNodeValue();                         // Throws DOMException
      } else if (nodeName.equalsIgnoreCase("enctype")) {
        enctype = node.getNodeValue();                        // Throws DOMException
      } else if (nodeName.equalsIgnoreCase("namelist")) {
        String submitString = node.getNodeValue();            // Throws DOMException
        if (submitString != null && submitString.length() > 0) {
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SUBDIALOG_MASK))
            SysLog.println("Subdialog: submitString = \"" + submitString + "\"");
          submit = new Variables(submitString);
        }
      }
    }

     // Construct the subdialog's context
    whereToGo = new GotoSubdialog(subdialog, context);        // Throws InvalidTag, VXMLEvent
  }

  // An inner helper class to get around Java's lack of multiple inheritance
  protected class GotoSubdialog extends Goto {
    GotoSubdialog(Element element, Goto context) throws InvalidTag, VXMLEvent {
      this.next = source;
      this.method = Subdialog.this.method;
      this.enctype = Subdialog.this.enctype;
      this.caching = Subdialog.this.caching;
      this.fetchhint = Subdialog.this.fetchhint;
      this.fetchtimeout = Subdialog.this.fetchtimeout;
      this.fetchaudio = Subdialog.this.fetchaudio;
      this.submit = Subdialog.this.submit;

      this.preprocess(element, context);                      // Throws InvalidTag
    }
  }

   /** Make the Subdialog a child of a Form (extends FormItem's makeChildOf() to inherit the parent's scope) */
  public void makeChildOf(ScopeElement parent) throws VXMLEvent {
     // Inherit the parent's JavaScript scope 
    scope = parent.scope();
     // Have the parent Form adopt this child and give the child Form Scope
    super.makeChildOf(parent);  // Throws VXMLEvent
     // Configure the parameters object
    makeParameters(parent);
  }

   /** Interpret the Subdialog */
  public Variables interpret(Goto context, InterruptHandler interruptHandler) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SUBDIALOG_MASK))
      SysLog.println("Subdialog.interpret: called");
     // Clear the guard variable, in case this visit is the result of a form item goto
    Scope.setProp(scope, name, VXMLTypes.PROTO_UNDEFINED);

    Variables retVars = null;

    try {

      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.enter(element, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request entering <subdialog> " + name);

      modifyProperties();

      Scriptable results = null;

       // Initialize and insert the JavaScript Object containing the parameters
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SUBDIALOG_MASK))
        SysLog.println("Subdialog.interpret: scope = " + scope.toString());
      parameters.initializeVariables(null, this);             // Throws VXMLEvent
      whereToGo.parameters(parameters());
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SUBDIALOG_MASK))
        SysLog.println("Subdialog.interpret: parameters = " + parameters().toString());

       // Acquire and parse the subdialog's document
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SUBDIALOG_MASK))
        SysLog.println("Subdialog.interpret: parsing subdialog \"" + source + "\"");
      whereToGo.acquireTarget(this, context);                 // Throws VXMLEvent
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SUBDIALOG_MASK))
        SysLog.println("Subdialog.interpret: subdialog parsed");

       // Construct a new Session for the subdialog
      Session session;
      try {
        session = new Session(this.session.getRecognizer(), this.session.getAudioPlayer(), this.session.getInterruptHandler(), this.session.getSystemVariables(), true, false);                   // Throws IllegalAccessException, InstantiationException, ClassNotFoundException
      } catch (IllegalAccessException e) {
        throw new RuntimeException("Subdialog.interpret: UNEXPECTED " + e.toString());
      } catch (InstantiationException e) {
        throw new RuntimeException("Subdialog.interpret: UNEXPECTED " + e.toString());
      } catch (ClassNotFoundException e) {
        throw new RuntimeException("Subdialog.interpret: UNEXPECTED " + e.toString());
      }
       // Run the subdialog's Session
      results = session.run(whereToGo, speaker()); // Throws VXMLEvent, VXMLExit

       // If the subdialog returns an event to throw, throw it
      try {
        Object eventProp = Scope.getProp(results, "_event");  // Throws UndeclaredVariableException
        if (eventProp != VXMLTypes.PROTO_UNDEFINED)
          throw new VXMLEvent(ScriptRuntime.toString(eventProp));
      } catch (UndeclaredVariableException e) {
         // Ignore - the subdialog has not returned an event
      }

       // Otherwise, return the results as the value of the guard variable
      Scope.setProp(scope, name, results);

       // Construct a Variables containing just the name of the guard variable
      retVars = new Variables(1);
      retVars.put(name, guard);

     // Catch any VXMLEvent in this Subdialog's scope
    } catch (VXMLEvent event) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.CATCH_MASK))
        SysLog.println("Subdialog.interpret: <subdialog> \"" + name + "\" caught VXMLEvent \"" + event.name() + "\"");
      retVars = catchEvent(event, context);                   // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
    } finally {
      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.leave(element, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request leaving <subdialog> " + name);
      restoreProperties();
    }
    return retVars;
  }
} // class Subdialog
