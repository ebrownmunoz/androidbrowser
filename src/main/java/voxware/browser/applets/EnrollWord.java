package voxware.browser.applets;

import java.net.MalformedURLException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

 // JSAPI Imports
import javax.speech.*;
import javax.speech.recognition.*;
import voxware.engine.recognition.sapivise.*;

 // JavaScript Imports
import org.mozilla.javascript.*;

 // Voxware Imports
import voxware.browser.*;
import voxware.util.*;

/**
 * EnrollWord is a VoxwareApplet that modifies a Trainer object by calling insertCorrection() with
 * specified "wordname" and "result" parameters. It then calls enrollWord(). since there is no way
 * to determine which utterance passed enrollment's scrutiny. The Audio is released before the
 * result is inserted into the Trainer.
 *
 * EnrollWord takes two or three parameters:
 *
 *   result  - a ViseResult (REQUIRED)
 *   word    - a String representing wordName being enrolled (REQUIRED)
 *   request - a Boolean specifying whether to return the speaker profile
 *             after completion of the current word enrollment (OPTIONAL, defaults to false)
 *
 * It returns a JavaScript Object containing up to four properties:
 *
 *   more  - the number of extra utterances necessary to complete current word enrollment
 *           (zero when word is successfully enrolled)
 *   count - the the number of utterances currently available in the Trainer object
 *   name  - the name of the speaker profile being trained, if more == 0 && request == true
 *   image - the byte[] image of the speaker profile (.voi), if more == 0 && request == true
 *
 * Throws:

 *   VXMLEvent "error.applet.illegalparameter" if the result parameter is not a ViseResult
 *   VXMLEvent "error.applet.missingparameter" if the result or word parameter is missing
 *   VXMLEvent "error.enrollment.insertion"    if insertCorrection() fails
 *   VXMLEvent "error.enrollment"              if enrollWord() fails for any reason other than NOTENOUGHDATA
 *   VXMLEvent "error.vise.speaker.nospeaker"  if the speaker profile cannot be acquired from the recognizer
 *   Miscellaneous other VXMLEvents common to all applets
 */

public class EnrollWord extends voxware.browser.VoxwareApplet {

   // Fields
  private static javax.speech.recognition.Recognizer recognizer = null;
  private static ViseTrainerFactory factory = null;
  private static ViseTrainer trainer = null;
  private static String prevWordName = null;

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    FlattenedObject flatParams = new FlattenedObject(params);
    FlattenedObject flatReturn = null;

    try {

       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Put the params into the scope
      scope.put("params", scope, params);

       // Create a results Object and put it into the scope
       // Throws PropertyException, NotAFunctionException, JavaScriptException
      Scriptable results = context.newObject(scope);
      scope.put("results", scope, results);
       // Flatten it to make it easy to deal with
      flatReturn = new FlattenedObject(results);

       // Get the parameters
      Object resultParam = flatParams.getProperty("result");
      Object wordParam   = flatParams.getProperty("word");
      Object request     = flatParams.getProperty("request");

      if (resultParam == Undefined.instance) {
        throw new VXMLEvent("error.applet.missingparameter", "REQUIRED \"result\" parameter of EnrollWord applet missing");
      } else if (!(resultParam instanceof ViseResult)) {
        throw new VXMLEvent("error.applet.illegalparameter", "\"result\" parameter of EnrollWord applet must be a ViseResult, not a " + resultParam.getClass().getName());
      } else if (wordParam == Undefined.instance) {
        throw new VXMLEvent("error.applet.missingparameter", "REQUIRED \"word\" parameter of EnrollWord applet missing");
      } else {
        ViseResult result = (ViseResult)resultParam;
        String     wordName = Context.toString(wordParam);
         // When first called or when a new word comes in, get a new Trainer
        if (prevWordName == null || !wordName.equals(prevWordName)) {
          prevWordName = wordName;
          if (recognizer == null) {
        	  
        	// TODO ERK this isn't going to work
            //recognizer = voxware.browser.Recognizer.speechRecognizer();
            factory = ((SapiVise)recognizer).getTrainerFactory();
          }
          trainer = factory.getTrainer();
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
            SysLog.println("EnrollWord: started with Word \"" + wordName + "\"");
        }
        if (trainer.insertCorrection(wordName, result)) {
          ViseEnrollStatus enrollmentStatus = trainer.enrollWord();
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
            SysLog.println("  EnrollWord: have " + enrollmentStatus.Count() + " utterances; need a total of " + enrollmentStatus.Needed());
          flatReturn.putProperty("count", VXMLTypes.intToNumber(enrollmentStatus.Count()));
          if (enrollmentStatus.Status() == 0) {
            if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
              SysLog.println("EnrollWord: success on Word \"" + wordName + "\"");
            trainer.release();
            flatReturn.putProperty("more", VXMLTypes.intToNumber(0));
             // Force new trainer for next utterance, so backing to this word would work from scratch
            prevWordName = null;
            if (request != Undefined.instance && Context.toBoolean(request)) {
               // Send back voice file
              SpeakerManager speakerManager = recognizer.getSpeakerManager();
              SpeakerProfile profile = speakerManager.getCurrentSpeaker();
              if (profile != null) {
                ByteArrayOutputStream profileStream = new ByteArrayOutputStream();
                speakerManager.writeVendorSpeakerProfile(profileStream, profile); // Throws SecurityException, MalformedURLException, IOException
                flatReturn.putProperty("name", profile.getName());
                flatReturn.putProperty("image", profileStream.toByteArray());
              } else {
                throw new VXMLEvent("error.vise.speaker.nospeaker", "EnrollWord: cannot get the current speaker profile from the SpeakerManager");
              }
            }
          } else if (enrollmentStatus.Status() == ViseTrainer.NOTENOUGHDATA) {
             // Signal that more utterances are needed
            int num = enrollmentStatus.Needed() - enrollmentStatus.Count();
            if (num <= 0) num = 1;
            flatReturn.putProperty("more", VXMLTypes.intToNumber(num));
          } else {
            throw new VXMLEvent("error.enrollment", "enrollment status is " + enrollmentStatus.Status());
          }
        } else {
          throw new VXMLEvent("error.enrollment.insertion", "cannot insert ViseResult for \"" + wordName + "\"");
        }
      }

    } catch (PropertyException e) {
      throw new VXMLEvent("error.applet.badproperty", e);
    } catch (NotAFunctionException e) {
      throw new VXMLEvent("error.applet.notafunction", e);
    } catch (JavaScriptException e) {
      throw new VXMLEvent("error.javascript.uncaughtevent", e);
    } catch (ResultStateError e) {
      throw new VXMLEvent("error.enrollment.insertion", e, "EnrollWord.insertCorrection");
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e, "EnrollWord");
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return the results Object (NOT FlattenedObject)
    return flatReturn.getObject();
  }
}
