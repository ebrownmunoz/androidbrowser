package voxware.browser.applets;

 // JSAPI Imports
import javax.speech.*;
import javax.speech.recognition.*;
import voxware.engine.recognition.sapivise.*;

 // JavaScript Imports
import org.mozilla.javascript.*;

 // Voxware Imports
import voxware.browser.*;

/**
 * Calibrate is a VoxwareApplet that calibrates the recognizer for the acoustical background. It
 * 
 * Calibrate takes no parameters.
 *
 * Returns a JavaScript Boolean, which is false if calibration fails, true if calibration
 * succeeds or if there is no recognizer.
 *
 * Throws:
 *
 *   Miscellaneous VXMLEvents common to all applets
 */
public class Calibrate extends voxware.browser.VoxwareApplet {

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    Object success = VXMLTypes.PROTO_UNDEFINED;

    try {
       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Calibrate
      success = new Boolean(rec != null ? ((SapiVise)rec).calibrate() : true);

    }  catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return status
    return success;
  }
}
