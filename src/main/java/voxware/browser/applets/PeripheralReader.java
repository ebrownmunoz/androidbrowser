package voxware.browser.applets;

import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.PushbackReader;
import java.io.OutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;

import voxware.browser.VoxwareApplet;
import voxware.util.SysLog;
import voxware.browser.InterruptHandler;
import voxware.browser.VXMLEvent;

 
// JavaScript Imports
import org.mozilla.javascript.*;

public  class PeripheralReader implements Runnable {
      private Scriptable readParams;
      public  boolean    shouldStop = false;
      public PeripheralReader(Scriptable readParams) {
        shouldStop = false;
        this.readParams = readParams;
      }

      public void run() {
        Context    ctx = null;
        Scriptable scope = null;
        Scriptable s = null;
        Object     data = null;
        Object     jsOpStatus = null;
        String     opStatus = null;

        try {
          ctx = Context.enter();
          while (!shouldStop && !Thread.currentThread().isInterrupted()) {
            s = Peripheral.read(readParams);
            jsOpStatus = ScriptableObject.getProperty(s, "opStatus");
            opStatus = Context.toString(jsOpStatus);
            if (opStatus.equals(ConfigurePeripheral.OP_SUCCESS)) {
              data = ScriptableObject.getProperty(s, "data");
              if ((data != Scriptable.NOT_FOUND) && 
                  (data != Context.getUndefinedValue()) &&
                  (data instanceof Scriptable)) {
                processData((Scriptable)data);
              }
            } else if (opStatus.equals(ConfigurePeripheral.OP_CANCELLED)) {
              // the main reason that we get an op cancel is if config is called
              // during a read or the device is being closed
              if (shouldStop) return;
              Thread.sleep(1000);
            } else if (opStatus.equals(ConfigurePeripheral.OP_ERROR)) {
              // this is an error
              // for now, just stop the reader thread
              return;
            } else if (opStatus.equals(ConfigurePeripheral.OP_UNSUPPORTED)) {
              return;
            } else {
              return;
              // some status we've never heard about?
            }
          }
        } catch (Exception e) {
          SysLog.println(1, "PeripheralReader: caught exception " + e);
        } finally {
          SysLog.println(1, "PeripheralReader: exitting");
          Context.exit();
        }
      }

      private void processData(Scriptable data) throws JavaScriptException, NotAFunctionException, PropertyException {
        SysLog.println(5, "PeripheralReader.processData: handling some object");
        Context    ctx;
        Scriptable scope;
        Object     jsDataType;
        String     dataType;
        String     text = null;
        
        try {
          ctx = Context.enter();
          scope = ctx.initStandardObjects(null);
          ctx.setLanguageVersion(Context.VERSION_1_2);
          jsDataType = ScriptableObject.getProperty(data, "type");
          dataType = Context.toString(jsDataType);
          if (dataType.equals("text")) {
            SysLog.println(5, "type is text");
            text = Context.toString(ScriptableObject.getProperty(data, "text"));
          } else if (dataType.equals("binary")) {
            SysLog.println(5, "type is binary");
            Object dataArray = ScriptableObject.getProperty(data, "bytes");
            if (Peripheral.asJSResult) {
              SysLog.println(5, "calling toString on an array");
              text = Context.toString(ScriptableObject.callMethod(Context.toObject(dataArray, scope), "toString", new Object[0]));
            } else {
              SysLog.println(5, "treating byte array as a string...");
              text = new String((byte[])ctx.toType(dataArray, byte[].class));
            }
          } else if (dataType.equals("js")) {
            SysLog.println(5, "type is js");
            if (Peripheral.asJSResult) {
              SysLog.println(5, "calling toString on js object");
              Object dataObject = ScriptableObject.getProperty(data, "jsObject");
              text = Context.toString(ScriptableObject.callMethod(Context.toObject(dataObject, scope), "toString", new Object[0]));
            } else {
              if (ScriptableObject.hasProperty(data, "text")) {
                SysLog.println(5, "found a text alternative");
                text = Context.toString(ScriptableObject.getProperty(data, "text"));
              } else if (ScriptableObject.hasProperty(data, "bytes")) {
                SysLog.println(5, "found a binary alternative");
                Object dataArray = ScriptableObject.getProperty(data, "bytes");
                text = new String((byte[])ctx.toType(dataArray, byte[].class));
              }
            }
          }
        } finally {
          Context.exit();
        }
        if (text != null) {
          SysLog.println(5, "Interrupting using index " + Peripheral.interruptIndex + " with " + text);
          // InterruptHandler.getInstance().genericInterrupt(Peripheral.interruptIndex, text);
        }
      }
    }

