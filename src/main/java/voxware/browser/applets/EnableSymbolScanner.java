package voxware.browser.applets;

import org.mozilla.javascript.*;

import voxware.browser.VXMLEvent;

/**
 * This class provided purely for backwards compatibility
 */
public class EnableSymbolScanner extends ConfigurePeripheral {
  public synchronized Object run(Scriptable params) throws VXMLEvent {
    Context    ctx;
    Scriptable scope;

    Object interrupt;
    Object deviceName;
    Object asJSResult;
    Object decoders;

    Object addr;
    Object port;
    Object exec;

    String peripheralName;

    Scriptable deviceSpec;
    Scriptable deviceConfig;

    try {
      ctx = Context.enter();
      scope = ctx.initStandardObjects(null);
      
      interrupt  = ConfigurePeripheral.getProperty(params, "interrupt");
      deviceName = ConfigurePeripheral.getProperty(params, "devicename");
      asJSResult = ConfigurePeripheral.getProperty(params, "asJSResult");
      decoders   = ConfigurePeripheral.getProperty(params, "decoders");

      addr = ConfigurePeripheral.getProperty(params, "addr");
      port = ConfigurePeripheral.getProperty(params, "port");
      exec = ConfigurePeripheral.getProperty(params, "exec");

      if ((interrupt == null) ||
          (interrupt == Scriptable.NOT_FOUND) ||
          (interrupt == Context.getUndefinedValue())) {
        peripheralName = "scanner";
      } else {
        peripheralName = Context.toString(interrupt);
      }

      deviceSpec = ctx.newObject(scope);
      deviceConfig = ctx.newObject(scope);
      ScriptableObject.putProperty(scope, "deviceName", peripheralName);
      ScriptableObject.putProperty(scope, "deviceSpec", deviceSpec);
      ScriptableObject.putProperty(deviceSpec, "type", "scanner");
      if (deviceName != Context.getUndefinedValue()) {
        ScriptableObject.putProperty(deviceSpec, "name", Context.toString(deviceName));
      }
      ScriptableObject.putProperty(scope, "deviceConfig", deviceConfig);
      if (decoders != Context.getUndefinedValue()) {
        ScriptableObject.putProperty(deviceConfig, "symbologies", decoders);
      }
      if (asJSResult != Context.getUndefinedValue()) {
        ScriptableObject.putProperty(scope, "asJSResult", asJSResult);
      }

      ScriptableObject.putProperty(scope, "addr", addr);
      ScriptableObject.putProperty(scope, "port", port);
      ScriptableObject.putProperty(scope, "exec", exec);
      
      return super.run(scope);
    } catch (PropertyException e) {
      throw new VXMLEvent("error.applet.badproperty", e);
    } catch (NotAFunctionException e) {
      throw new VXMLEvent("error.applet.notafunction", e);
    } catch (JavaScriptException e) {
      throw new VXMLEvent("error.javascript.uncaughtevent", e);
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
      Context.exit();
    }
  }
}
