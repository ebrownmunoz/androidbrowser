package voxware.browser.applets;

import java.net.MalformedURLException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

 // JSAPI Imports
import javax.speech.*;
import javax.speech.recognition.*;
import voxware.engine.recognition.sapivise.*;

 // JavaScript Imports
import org.mozilla.javascript.*;

 // Voxware Imports
import voxware.browser.DurationConverter;
import voxware.browser.Speaker;
import voxware.browser.VXMLEvent;
import voxware.browser.VXMLTypes;
import voxware.browser.VXMLURLLoader;

/**
 * GetSpeakerProfile is a VoxwareApplet that returns a JavaScript Object containing the name
 * and the contents of the profile for the given speaker. Optionally, it can be made to acquire
 * the profile from the speaker URL if the profile is not in the speaker cache, though by default
 * it will fail when the profile is not already cached locally.
 *
 * GetSpeakerProfile takes up to three parameters:
 *
 *   speaker - the URL of the speaker (REQUIRED)
 *   load    - a boolean specifying whether to acquire the profile from the URL if it is not
 *             already in the speaker cache (OPTIONAL, defaults to false)
 *   timeout - the timeout (in milliseconds) on acquisition from a URL; it has no effect when
 *             the "load" parameter is false (OPTIONAL, defaults to session.fetchtimeout)
 *
 * Returns a JavaScript Object containing two properties:
 *
 *   name - the name of the speaker profile (a String)
 *   image - the byte[] image of the speaker profile (.voi)
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter" if either parameter is missing or empty
 *   VXMLEvent "error.badfetch.malformedurl"   if the grammar URL is ill-formed
 *   VXMLEvent "error.badfetch.timedout"       if "load" is on and acquisition times out
 *   VXMLEvent "error.badfetch.ioerror"        if "load" is on and acquisition fails
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class GetSpeakerProfile extends voxware.browser.VoxwareApplet {

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    FlattenedObject flatParams = new FlattenedObject(params);
    FlattenedObject flatResults = null;

    try {
       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Put the params into the scope
      scope.put("params", scope, params);

       // Create a results Object and put it into the scope
       // Throws PropertyException, NotAFunctionException, JavaScriptException
      Scriptable results = context.newObject(scope);
      scope.put("results", scope, results);
       // Flatten it to make it easy to deal with
      flatResults = new FlattenedObject(results);

       // Get the parameters
      Object speakerParam = flatParams.getProperty("speaker");
      Object loadParam    = flatParams.getProperty("load");
      Object timeoutParam = flatParams.getProperty("timeout");

       // Interpret them
      String url;
      if (speakerParam == Undefined.instance || (url = Context.toString(speakerParam)).length() == 0)
        throw new VXMLEvent("error.applet.missingparameter", "\"speaker\" parameter of GetSpeakerProfile applet missing or empty");
      boolean load = (loadParam != Undefined.instance) ? Context.toBoolean(loadParam) : false;
      int timeout = (timeoutParam != Undefined.instance) ? (int)Context.toNumber(timeoutParam) : DurationConverter.UNSPECIFIED;

       // Get the image of the speaker profile in a byte[]
      ByteArrayOutputStream profileStream = new ByteArrayOutputStream();
      VXMLURLLoader loader = new VXMLURLLoader(url);
      loader.setFetchTimeout(timeout);
      
      // TODO ERK this is hacked to get it to work
      SpeakerProfile profile = null;
      //SpeakerProfile profile = Speaker.profile(loader, load);                       // Throws MalformedURLException
      if (profile != null) {
        rec.getSpeakerManager().writeVendorSpeakerProfile(profileStream, profile);  // Throws SecurityException, IOException
        flatResults.putProperty("name", profile.getName());
        flatResults.putProperty("image", profileStream.toByteArray());
      } else {
        flatResults.putProperty("name", VXMLTypes.PROTO_UNDEFINED);
        flatResults.putProperty("image", VXMLTypes.PROTO_UNDEFINED);
      }

     // Convert all the exceptions into VXML Events
    } catch (SecurityException e) {
      throw new VXMLEvent("error.vise.speaker.nomanager");
    } catch (MalformedURLException e) {
      throw new VXMLEvent("error.badfetch.malformedurl", e);
    } catch (IOException e) {
      throw new VXMLEvent("error.badfetch.ioerror", e);
    } catch (PropertyException e) {
      throw new VXMLEvent("error.applet.badproperty", e);
    } catch (NotAFunctionException e) {
      throw new VXMLEvent("error.applet.notafunction", e);
    } catch (JavaScriptException e) {
      throw new VXMLEvent("error.javascript.uncaughtevent", e);
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return the results Object (NOT FlattenedObject)
    return flatResults.getObject();
  }
}
