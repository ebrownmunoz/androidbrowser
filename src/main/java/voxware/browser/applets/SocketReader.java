package voxware.browser.applets;

import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.PushbackReader;
import java.io.OutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;
import voxware.browser.VoxwareApplet;
import voxware.util.SysLog;
import voxware.browser.VXMLEvent;

 // JavaScript Imports
import org.mozilla.javascript.*;

public class SocketReader implements Runnable {
      	private static final int BUFSIZE = 768;
	private char buf[];
	private InputStreamReader isr;
      	private PushbackReader pr;
      	private Socket socket ;
	private Vector inputQueue;

	SocketReader (Socket socket,Vector inputQueue) throws IOException{
		this.socket  = socket;
		this.inputQueue = inputQueue;
		isr = new InputStreamReader(socket.getInputStream());
		pr = new PushbackReader(isr); 	
	}

      public String getLine() throws IOException {
        String s = null;
        int i;
        char c;

        for (i = 0; i < BUFSIZE; i++) {
          buf[i] = '\0';
        }

        i = 0;
        buf[i++] = '(';
        while ((c = (char)pr.read()) != -1) {
          // Handle Windows line delimiters (/r/n) if required
          if (c == '\n' || c == '\r') {
            if (c == '\r') {
              if ((c = (char)pr.read()) != '\n')
                pr.unread(c);
            }
            if (i > 1) {
              buf[i++] = ')';
              s = new String(buf,0,i);
            }
            break;
          }

          buf[i++] = (char)c;
        }
        return s;
      }

    private Scriptable compile(String s) throws JavaScriptException{
	Context ctx = Context.enter();
        Scriptable scope = ctx.initStandardObjects(null);
        ctx.setLanguageVersion(Context.VERSION_1_2);
	Scriptable msg = (Scriptable)ctx.evaluateString(scope, s, "socket", 1, null);
	return msg;
    }  
    public void run() {
        String       s;
        Context      ctx;
        Scriptable   scope;
        Scriptable   msg=null;

        buf = new char[BUFSIZE];

        try {
          ctx = Context.enter();
          scope = ctx.initStandardObjects(null);
          ctx.setLanguageVersion(Context.VERSION_1_2);

          while ((s = getLine()) != null) {
            SysLog.println("SocketReader: read: " + s);
            try {
              if (ctx.stringIsCompilableUnit(s)) {
		//AR:Start of awful code. This part of the code was throwing a completely irrelevant ClassCastException.So I made this into a method and try it a few times before giving up.
		int retries = 3;
		while(true){
			try{
                		msg = compile( s);
	              		break;
			}catch(Exception e){
				retries--;
				SysLog.println("SocketReader: exception: " + e);
				if(retries == 0)
					break;
			}
		}
		postMessage(msg);
              }
            } catch (Exception e) {
              SysLog.println("SocketReader: exception: " + e + " (Accumulator will be cleared)");
            }
          }
              SysLog.println("SocketReader: Socket Closed ");
          // socket closed!
        } catch (Exception e) {
          SysLog.println( "SocketReader: exception: " + e);
        } finally {
          Context.exit();
        }
      }
      protected void postMessage(Scriptable s) {
        synchronized (inputQueue) {
          inputQueue.add(s);
          inputQueue.notifyAll();
        }
      }
    }

