package voxware.browser.applets;

import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.PushbackReader;
import java.io.OutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;

import voxware.browser.VoxwareApplet;
import voxware.util.SysLog;
import voxware.browser.Logger;
import voxware.browser.SystemEvent;
import voxware.browser.VXMLEvent;

 

// JavaScript Imports
import org.mozilla.javascript.*;

/**
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter"     if a required parameter is missing or empty
 *   VXMLEvent "error.applet.connectionerror"      if a socket error occurs while communicating with the external process
 *   VXMLEvent "error.applet.protocolerror"        if the external process returned unexpected or invalid JavaScript
 *   VXMLEvent "error.applet.nameconflict"         if some other applet has already used the given interrupt source name
 *   VXMLEvent "error.applet.peripheralnotfound"
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class ConfigurePeripheral extends VoxwareApplet {
  public static final String OP_SUCCESS     = "success";
  public static final String OP_ERROR       = "error";
  public static final String OP_CANCELLED   = "cancelled";
  public static final String OP_UNSUPPORTED = "unsupported";

  public static boolean isNotJSNull(Scriptable s) {
    return (s != null) && (s != Scriptable.NOT_FOUND) && (s != Context.getUndefinedValue());
  }

  public static Scriptable getProperty(Scriptable s, String name) {
    Object o = ScriptableObject.getProperty(s, name);
    if (o == null) return (Scriptable)Context.getUndefinedValue();
    if (o == Scriptable.NOT_FOUND) return (Scriptable)Context.getUndefinedValue();
    if (o instanceof Scriptable) {
      if (isNotJSNull((Scriptable)o)) {
        return (Scriptable)o;
      } else {
        return (Scriptable)Context.getUndefinedValue();
      }
    }
    return Context.toObject(o, s);
  }

 

  static Map peripherals = new HashMap();
  
  public synchronized Object run(Scriptable params) throws VXMLEvent {

    SysLog.println(3, "ConfigurePeripheral called");
    //FlattenedObject flatParams = new FlattenedObject(params);

    try {

      // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

      // Put the params into the scope
      // EH: do we really need to do this?!
      //scope.put("params", scope, params);

      Scriptable jsDeviceName    = ConfigurePeripheral.getProperty(params, "deviceName");
      Scriptable jsAsJSResult    = ConfigurePeripheral.getProperty(params, "asJSResult");
      Scriptable jsDeviceSpec    = ConfigurePeripheral.getProperty(params, "deviceSpec");
      Scriptable jsDeviceConfig  = ConfigurePeripheral.getProperty(params, "deviceConfig");
      Scriptable jsReadParams    = ConfigurePeripheral.getProperty(params, "readParams");
      Scriptable jsWriteParams   = ConfigurePeripheral.getProperty(params, "writeParams");

      Scriptable jsPeripheralAddr = ConfigurePeripheral.getProperty(params, "addr");
      Scriptable jsPeripheralPort = ConfigurePeripheral.getProperty(params, "port");
      Scriptable jsPeripheralExec = ConfigurePeripheral.getProperty(params, "exec");

      Scriptable jsResult;

      String  peripheralName = "";
      boolean asJSResult     = true;

      String  addr = "127.0.0.1";
      int     port = 40001;

      int interruptSourceIndex = SystemEvent.NO_INDEX;


      if (jsDeviceName == Context.getUndefinedValue()) {
          // missing a required parameter
          throw new VXMLEvent("error.applet.missingparameter");
      }

      peripheralName = Context.toString(jsDeviceName);

      if (jsAsJSResult != Context.getUndefinedValue()) {
        asJSResult = Context.toBoolean(jsAsJSResult);
      }

      if (jsPeripheralAddr != Context.getUndefinedValue()) {
        addr = Context.toString(jsPeripheralAddr);
      }

      if (jsPeripheralPort != Context.getUndefinedValue()) {
        port = (int)Context.toNumber(jsPeripheralPort);
      }

      if (addr.equals("localhost") || addr.equals("127.0.0.1")) {
        if (jsPeripheralExec == Context.getUndefinedValue()) {
          SysLog.println(2, "Starting PeripheralHost.exe");
          Runtime.getRuntime().exec("PeripheralHost.exe");
        } else {
          SysLog.println(2, "Starting " + Context.toString(jsPeripheralExec));
          Runtime.getRuntime().exec(Context.toString(jsPeripheralExec));
        }
      }

      Socket     s = null;
      Peripheral p = null;
      if (!peripherals.containsKey(peripheralName)) {
        try {
          s = new Socket(addr, port);
          SysLog.println(2, "Connected to external process");
        } catch (Exception e) {
          throw new VXMLEvent("error.applet.connectionerror", "Failed to connect to external process");
        }
        interruptSourceIndex = SystemEvent.addType(peripheralName);

        if (interruptSourceIndex == SystemEvent.NO_INDEX) {
          try {
            s.close();
          } catch (IOException e) {
          }
          throw new VXMLEvent("error.applet.nameconflict", "The name " + peripheralName + " is already in use as an interrupt source");
        }
        SysLog.println(5, peripheralName + " added as SystemEvent index " + interruptSourceIndex);

        p = new Peripheral(s, peripheralName, interruptSourceIndex, asJSResult);
        //Logger.addType(peripheralName, p); ERK
        p = (Peripheral)peripherals.get(peripheralName);
        p.close();
      }

      p.setReadParams(jsReadParams);
      p.setWriteParams(jsWriteParams);

      jsResult = p.open(jsDeviceSpec);
      if (jsResult != Context.getUndefinedValue()) {
        if (Context.toString(ScriptableObject.getProperty(jsResult, "opStatus")).equals(OP_SUCCESS)) {
          SysLog.println(2, "Opened a device");
        } else {
          SysLog.println(2, "Failed to open a device");
          throw new VXMLEvent("error.applet.peripheralnotfound");
        }
      }

      jsResult = p.config(jsDeviceConfig);
      if (jsResult != Context.getUndefinedValue()) {
        if (Context.toString(ScriptableObject.getProperty(jsResult, "opStatus")).equals(OP_SUCCESS)) {
          SysLog.println(2, "Configured a device");
        } else {
          SysLog.println(2, "Failed to configure a device");
          throw new VXMLEvent("error.applet.configurationinvalid");
        }
      }

      peripherals.put(peripheralName, p);
 
      p.startReader();
    // Convert exceptions into VXML Events
    } catch (PropertyException e) {
      throw new VXMLEvent("error.applet.badproperty", e);
    } catch (NotAFunctionException e) {
      throw new VXMLEvent("error.applet.notafunction", e);
    } catch (JavaScriptException e) {
      throw new VXMLEvent("error.javascript.uncaughtevent", e);
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

    return new Boolean(true);
  }
}
