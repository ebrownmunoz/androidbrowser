package voxware.browser.applets;

import java.net.MalformedURLException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

 // JavaScript Imports
import org.mozilla.javascript.*;

 // Voxware Imports
import voxware.browser.DurationConverter;
import voxware.browser.VXMLEvent;
import voxware.browser.VXMLTypes;
import voxware.browser.VXMLURLLoader;

/**
 * PutFile is a VoxwareApplet that writes a byte[] to a specified file.
 *
 * PutFile takes two parameters:
 *
 *   source - the byte[] image of the file (REQUIRED)
 *   target - the file name (REQUIRED)
 *
 * Returns a JavaScript Boolean indicating success or failure
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter"     if the "source" or "target" parameter is missing or empty
 *   VXMLEvent "error.badfetch.filenotfound"       if the file cannot be found
 *   VXMLEvent "error.badfetch.ioerror"            if an error occurs during acquisition
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class PutFile extends voxware.browser.VoxwareApplet {

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    FlattenedObject flatParams = new FlattenedObject(params);
    FlattenedObject flatResults = null;

    Object success = VXMLTypes.PROTO_UNDEFINED;

    try {
       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Put the params into the scope
      scope.put("params", scope, params);

       // Get the parameters
      if (!flatParams.hasProperty("source"))
        throw new VXMLEvent("error.applet.missingparameter", "\"source\" parameter of PutFile applet missing");
      if (!flatParams.hasProperty("target"))
        throw new VXMLEvent("error.applet.missingparameter", "\"target\" parameter of PutFile applet missing");
      Object source = flatParams.getProperty("source");
      if (source == null || !(source instanceof byte[]))
        throw new VXMLEvent("error.applet.illegalparameter", "\"source\" parameter of PutFile applet empty");
      byte[] image = (byte[]) source;
      String target = Context.toString(flatParams.getProperty("target"));

       // Write the image to the specified target file
      System.out.println("PutFile.run(): target file name is " + target);
      File file = new File(target);                                 // Throws IOException
      System.out.println("PutFile.run(): target file is " + file.toString());
      FileOutputStream fos = new FileOutputStream(file);            // Throws FileNotFoundException
      fos.write(image);
      fos.close();                                                  // Throws IOException

      success = new Boolean(true);

     // Convert all the exceptions into VXML Events
    } catch (SecurityException e) {
      throw new VXMLEvent("error.badfetch.readprotected");
    } catch (FileNotFoundException e) {
      throw new VXMLEvent("error.badfetch.filenotfound", e);
    } catch (IOException e) {
      throw new VXMLEvent("error.badfetch.ioerror", e);
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return status
    return success;
  }
}
