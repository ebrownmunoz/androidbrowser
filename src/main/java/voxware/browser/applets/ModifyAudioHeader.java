package voxware.browser.applets;

import java.io.*;

 // JavaScript Imports
import org.mozilla.javascript.*;

 // Voxware Imports
import voxware.browser.*;
import voxware.util.*;

/**
 * ModifyAudioHeader is a VoxwareApplet that allows you to add and modify fields in the header of a
 * NIST SPHERE audio file.
 *
 * It takes two parameters, both REQUIRED:
 *
 *   image - a byte[] containing the image of the audio file (REQUIRED)
 *   map   - a JavaScript Array containing map entries (REQUIRED)
 *
 * Each map entry is a JavaScript Object with the following properties:
 *   oldname - a String specifying the current name of the field to be modified (OPTIONAL)
 *   newname - a String specifying the new name of the field to be added or modified (OPTIONAL, defaults to the old name)
 *   value   - a JavaScript type containing the new value of the field (OPTIONAL)
 *   type    - a String specifying the type (integer, real or string) of the new field value (OPTIONAL, defaults to string)
 *
 * If neither oldname nor newname are present, ModifyAudioHeader throws an "error.applet.missingparameter" VXMLEvent;
 * If oldname is present, but newname and value are missing, ModifyAudioHeader removes the old field;
 *   if there is no old field with the old name, it does nothing;
 * If oldname and value are present, but newname is missing, ModifyAudioHeader gives the old field the new value;
 *   if there is no old field with the old name, it does nothing;
 * If newname is present, but oldname and value are missing, ModifyAudioHeader does nothing;
 * If newname and value are present, but oldname is missing, ModifyAudioHeader creates a new field with the new value;
 *   if there is already a field with that name, its value is replaced;
 * If oldname, newname and value are all three present, ModifyAudioHeader changes the name of the field and gives it the new value;
 *   if there is no old field with the old name, it still creates a field with the new name and gives it the value.
 * Thus, if newname and value are present, the modified header will contain a field with the new name and value.
 *
 * Returns the modified image as a byte[].
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter"      if the result parameter is not supplied
 *   VXMLEvent "error.applet.illegalparameter"      if the result parameter is not a ViseResult or the prompt is empty
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class ModifyAudioHeader extends voxware.browser.VoxwareApplet {

  public static final int ChunkSize = 8192;

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    FlattenedObject flatParams = new FlattenedObject(params);
    Object results = Undefined.instance;

    try {
       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Put the params into the scope
      scope.put("params", scope, params);

       // Get the parameters
      Object imageParam = flatParams.getProperty("image");
      Object mapParam = flatParams.getProperty("map");

       // Interpret the parameters
      if (imageParam == Undefined.instance) {
        throw new VXMLEvent("error.applet.missingparameter", "REQUIRED \"image\" parameter of ModifyAudioHeader applet missing");
      } else if (!(imageParam instanceof byte[])) {
        throw new VXMLEvent("error.applet.illegalparameter", "\"image\" parameter of ModifyAudioHeader applet must be a byte[], not a " + imageParam.getClass().getName());
      } else if (mapParam == Undefined.instance) {
        throw new VXMLEvent("error.applet.missingparameter", "REQUIRED \"map\" parameter of ModifyAudioHeader applet missing");
      } else if (!(mapParam instanceof FlattenedObject)) {
        throw new VXMLEvent("error.applet.illegalparameter", "\"map\" parameter of ModifyAudioHeader applet must be a JavaScript Array, not a " + mapParam.getClass().getName());
      } else {

         // Acquire the old header
        byte[] image = (byte[])imageParam;
        ByteArrayInputStream inputStream = new ByteArrayInputStream(image);
        NISTHeader header = new NISTHeader(1);
        if (!header.read(inputStream)) throw new VXMLEvent("error.nist.badheader");
         // Calculate the length of the image minus the header
        int dataLength = image.length - header.length();

         // Process the map
        FlattenedObject flatMap = (FlattenedObject) mapParam;
        Object[] ids = flatMap.getIds();
        for (int index = 0; index < ids.length; index++) {
          Object fieldParam = flatMap.getProperty(ids[index]);
          if (fieldParam != Undefined.instance && fieldParam instanceof FlattenedObject) {
            FlattenedObject flatField = (FlattenedObject) fieldParam;
            Object oldnameParam = flatField.getProperty("oldname");
            Object newnameParam = flatField.getProperty("newname");
            Object valueParam = flatField.getProperty("value");
            Object typeParam = flatField.getProperty("type");

            String oldname = (oldnameParam != Undefined.instance) ? Context.toString(oldnameParam) : null;
            String newname = (newnameParam != Undefined.instance) ? Context.toString(newnameParam) : null;
            if (oldname == null && newname == null)
              throw new VXMLEvent("error.applet.missingparameter", "Both \"oldname\" and \"newname\" parameters missing from ModifyAudioHeader map");
            String type = (typeParam != Undefined.instance) ? Context.toString(typeParam).toLowerCase() : null;
            Object value = null;
            if (valueParam != Undefined.instance) {
              if (type != null) {
                if      (type.startsWith("i")) value = new Integer((int)Context.toNumber(valueParam));
                else if (type.startsWith("r")) value = new Float((float)Context.toNumber(valueParam));
                else if (type.startsWith("s")) value = Context.toString(valueParam);
                else throw new VXMLEvent("error.applet.illegalparameter", "Unknown \"type\" parameter (\"" + type + "\") in ModifyAudioHeader map");
              } else {
                 // Default to String
                value = Context.toString(valueParam);
              }
            }

            if (oldname != null) {
              if      (newname != null) header.changeFieldName(oldname, newname);
              else if (value == null)   header.remove(oldname);
              else                      newname = oldname;
            } else if (newname != null && value == null) {
              throw new VXMLEvent("error.applet.missingparameter", "\"value\" parameter for \"newname\" \"" + newname + "\" missing from ModifyAudioHeader map");
            }
            if (newname != null && value != null) header.addField(newname, value);
          }
        }

        ByteArrayOutputStream headerImage = header.bytes();

         // Construct a ByteArrayOutputStream with a buffer big enough to hold the image with its new header
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(dataLength + header.length());

         // Write the header into it
        outputStream.write(headerImage.toByteArray());
         // Write the rest of the image into it
        byte[] buffer = new byte[ChunkSize];
        while (true) {
          int byteCount = inputStream.read(buffer);
          if (byteCount < 0) break;
          outputStream.write(buffer, 0, byteCount);
        }

        results = outputStream.toByteArray();

        inputStream.close();
        outputStream.close();
      }

     // Convert exceptions into VXML Events
    } catch (IOException e) {
      throw new VXMLEvent("error.badfetch.ioerror", e, "ModifyAudioHeader");
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e, "ModifyAudioHeader");
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return the results byte[]
    return results;
  }
}
