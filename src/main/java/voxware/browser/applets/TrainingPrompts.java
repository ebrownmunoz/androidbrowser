package voxware.browser.applets;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.TreeSet;

import org.w3c.dom.Element;

 // JSAPI Imports
import javax.speech.*;
import javax.speech.recognition.*;
import voxware.engine.recognition.sapivise.*;

 // JavaScript Imports
import org.mozilla.javascript.*;

 // Voxware Imports
import voxware.browser.*;
import voxware.util.SysLog;

/**
 * TrainingPrompts is a VoxwareApplet that returns a JavaScript object containing an Array of training prompts for a
 * given script or grammar, each prompt embodied in a JavaScript String. If both a script and grammar are specified,
 * the script takes precedence. By supplying an optional Array of words to be trained, you may limit the array of
 * prompts to those that contain occurrences of words (or of their variants) from the list you supply; otherwise,
 * the entire script is returned. TrainingPrompts optionally starts the initial batch training pass if the "batch"
 * parameter is true.
 *
 * TrainingPrompts takes up to seven parameters:
 *
 *   grammar - the URL of the grammar (ignored if script is present and batch is false, otherwise REQUIRED)
 *   script  - an Array of Strings containing the prompts (OPTIONAL if grammar is present, else REQUIRED)
 *   words   - an Array of Strings containing the words to be trained (OPTIONAL, defaults to the whole vocabulary)
 *   strip   - a String that triggers stripping of the suffixes that it begins (OPTIONAL, defaults to undefined)
 *   count   - the minimum number of instances of words from words[] that a prompt must contain (OPTIONAL, defaults to 1)
 *   batch   - a boolean specifying whether to start the initial batch training pass (OPTIONAL, defaults to false)
 *   type    - a String specifying "recommended", "unenrolled" or "required" phrases (OPTIONAL, defaults to "recommended")
 *
 * If the script or words parameter is present, it must evaluate to a JavaScript Array of Strings
 * 
 * The "prompts" property in the JavaScript Object that TrainingPrompts returns is a JavaScript Array that contains a
 * String for each prompt in the script or in the grammar's training script that contains at least "count" occurences
 * of words from the given word list. A script takes precedence over a grammar; the grammar is ignored if a script is
 * specified. If no "words" parameter is supplied, the word list defaults to the script or grammar's entire vocabulary,
 * and thus the array will have an element for every prompt in the script or grammar.  On the other hand, if the "words"
 * parameter is an empty array, the result will be an empty array, unless the "count" parameter is zero. The "count"
 * parameter has no effect if "words" is not also present. Setting "count" to zero has the same effect as not supplying
 * "words"; all of the phrases will end up in the resulting array.. If the optional "strip" parameter is present, a word
 * matches all of its variants, where a variant is either the word itself or the word with a suffix starting with the
 * strip string. For example, if strip is "_", the word "word" will match "word", "word_foo", "word_anything", etc.
 *
 * Returns a JavaScript Object with two properties:
 *
 *   prompts - an Array of prompt Strings
 *   words   - an Array of Strings containing the words to be trained and their variants, in ascending lexical order
 *
 * The "words" property is returned only if the "words" parameter is present.
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter"     if both the script and grammar parameters are missing or empty
 *   VXMLEvent "error.applet.missingparameter"     if batch is true and grammar parameter is missing or empty
 *   VXMLEvent "error.badgrammar"                  if the grammar parameter is not a SapiViseGrammar
 *   VXMLEvent "error.badfetch.malformedurl"       if the grammar URL is ill-formed
 *   VXMLEvent "error.applet.illegalparameter"     if the words parameter is present but is not a JavaScript Array
 *   VXMLEvent "error.applet.recognition.training" if it cannot start a training pass
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class TrainingPrompts extends voxware.browser.VoxwareApplet {

   // Defaults
  public static boolean batchDefault = false;
  public static int     countDefault = 1;

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    FlattenedObject flatParams = new FlattenedObject(params);
    FlattenedObject flatReturn = null;

    try {
       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Create a results Object and put it into the scope
       // Throws PropertyException, NotAFunctionException, JavaScriptException
      Scriptable results = context.newObject(scope);
      scope.put("results", scope, results);
       // Flatten it to make it easy to deal with
      flatReturn = new FlattenedObject(results);

       // Put the params into the scope
      scope.put("params", scope, params);

       // Get the parameters
      Object grammarParam = flatParams.getProperty("grammar");
      Object scriptParam  = flatParams.getProperty("script");
      Object wordsParam   = flatParams.getProperty("words");
      Object stripParam   = flatParams.getProperty("strip");
      Object countParam   = flatParams.getProperty("count");
      Object batchParam   = flatParams.getProperty("batch");
      Object typeParam    = flatParams.getProperty("type");

       // Interpret the parameters
      String grammarUrl;
      if (grammarParam == Undefined.instance || (grammarUrl = Context.toString(grammarParam)).length() == 0) grammarUrl = null;
      String[] script = toStringArray(scriptParam, "script"); // Throws VXMLEvent, IllegalArgumentException
      String[] words  = toStringArray(wordsParam, "words");   // Throws VXMLEvent, IllegalArgumentException
       // Optimize variant matching by using a simple char match if the suffix link is one character long
      String linkString;
      char linkChar = 0;
      if (stripParam == Undefined.instance || (linkString = Context.toString(stripParam)).length() == 0) {
        linkString = null;
      } else {
        linkChar = linkString.charAt(0);
        if (linkString.length() == 1) linkString = null;
      }
      int count = (countParam != Undefined.instance) ? (int)Context.toNumber(countParam) : countDefault;;
      boolean batch = (batchParam != Undefined.instance) ? Context.toBoolean(batchParam) : batchDefault;
      String type = Context.toString(typeParam);
      int typeIndex = (type.equalsIgnoreCase("required") || type.equalsIgnoreCase("unenrolled")) ? SapiViseGrammar.REQUIRED : SapiViseGrammar.RECOMMENDED;

      if (grammarUrl == null) {
        if (script == null)
          throw new VXMLEvent("error.applet.missingparameter", "\"grammar\" and \"script\" parameters of TrainingPrompts applet are both missing or empty");
        if (batch || (typeIndex == SapiViseGrammar.REQUIRED))
          throw new VXMLEvent("error.applet.missingparameter", "\"grammar\" parameter of TrainingPrompts applet is missing or empty");
      }

       // Get the SapiViseGrammar if batch is true or there is no script or if type index is SapiViseGrammar.REQUIRED
      SapiViseGrammar viseGrammar = null;
      if ((batch || (script == null) || (typeIndex == SapiViseGrammar.REQUIRED))) {
    	  //voxware.browser.Recognizer recognizer = voxware.browser.Recognizer.instance();
    	  voxware.browser.Recognizer recognizer = null; // TODO ERK
          javax.speech.recognition.Grammar grammar = recognizer.loadGrammar(new voxware.browser.Grammar(grammarUrl), null);  // Throws MalformedURLException, VXMLEvent
        if (!(grammar instanceof SapiViseGrammar))
          throw new VXMLEvent("error.badgrammar", "\"" + grammarUrl + "\" is a " + grammar.getClass().getName() + ", not a SapiViseGrammar");
        viseGrammar = (SapiViseGrammar)grammar;
      }

       // Get the prompt strings from the script, if present, otherwise from the grammar
      String[] phrases = null;
      if (script != null) {
        phrases = (typeIndex == SapiViseGrammar.REQUIRED) ? viseGrammar.trainPhrase(typeIndex) : script;
      } else if (viseGrammar != null) {
        phrases = viseGrammar.trainPhrase(typeIndex);
      }
      if (phrases == null) phrases = new String[0];

       // Prune the prompts to those containing enough occurrences of words from the word list
      if (words != null) {
        TreeSet wordSet = new TreeSet(Arrays.asList(words));
        if (phrases.length > 0) {
           // Null out the phrases in phrases[] that do not contain enough instances of the specified words or their variants
          int phraseCount = 0;
          int phraseIndex;
          int phraseLength;
          String phrase;
          for (phraseIndex = 0; phraseIndex < phrases.length; phraseIndex++) {
            phrase = phrases[phraseIndex];
            if (phrase != null) {
              phraseLength = phrase.length();
              int wordIndex = 0;
              int instanceCount = 0;
              int wordStart;
              int wordLength;
              int wordEnd;
              int suffixEnd;
              String word;
              while (wordIndex < words.length) {
                word = words[wordIndex];
                if (word != null) {
                  wordStart = 0;
                  wordLength = word.length();
                  wordEnd = 0;
                   // Add the number of occurrences of this word to the overall instance count for this phrase
                  while ((linkChar != 0 || instanceCount < count) && (wordStart = phrase.indexOf(word, wordEnd)) >= 0) {
                    wordEnd = wordStart + wordLength;
                    if (wordStart > 0 && phrase.charAt(wordStart - 1) != ' ' && phrase.charAt(wordStart - 1) != '\"') continue;
                    if (wordEnd < phraseLength && phrase.charAt(wordEnd) != ' ' && phrase.charAt(wordEnd) != '\"') {
                      if (phrase.charAt(wordEnd) != linkChar) continue;
                      if (linkString != null && !phrase.startsWith(linkString, wordEnd)) continue;
                       // The word in the phrase is a variant of this word; add it to the wordSet
                      if ((suffixEnd = phrase.indexOf(' ', wordEnd)) > 0 || (suffixEnd = phrase.indexOf('\"', wordEnd)) > 0) wordEnd = suffixEnd;
                      else wordEnd = phraseLength;
                      if (wordSet.add(phrase.substring(wordStart, wordEnd)))
                        SysLog.println("TrainingPrompts: added word \"" + phrase.substring(wordStart, wordEnd) + "\" to the word set"); // DEBUG
                    }
                    instanceCount++;
                  }
                   // If we've found enough instances of the specified words or their variants in this phrase, leave it in phrases[]
                  if (instanceCount >= count) {
                     // Strip double quotes from the phrase
                    int index = phrase.indexOf('\"');
                    if (index > -1) {
                      StringBuffer buffer = new StringBuffer(phrase);
                      while (index < buffer.length()) {
                        if (buffer.charAt(index) == ('\"')) buffer.deleteCharAt(index);
                        index++;
                      }
                      phrases[phraseIndex] = buffer.toString();
                    }
                    phraseCount++;
                    break;
                  }
                }
                 // If we've exhausted the word list, then the phrase doesn't contain enough of the words; remove it from phrases[]
                if (++wordIndex == words.length) {
                  phrases[phraseIndex] = null;
                  break;
                }
              }
            }
          }
           // Prune phrases[] of any null phrases
          if (phraseCount < phrases.length) {
            int prunedPhraseIndex = 0;
            String[] prunedPhrases = new String[phraseCount];
            for (phraseIndex = 0; phraseIndex < phrases.length && prunedPhraseIndex < phraseCount; phraseIndex++) {
              if (phrases[phraseIndex] != null) prunedPhrases[prunedPhraseIndex++] = phrases[phraseIndex];
            }
            phrases = prunedPhrases;
          }
        }
         // Wrap the wordSet in a JavaScript Array and put it in the results object as the "words" property
        flatReturn.putProperty("words", context.newArray(scope, wordSet.toArray(new String[0])));
      }

       // Wrap the prompts in a JavaScript Array and put it in the results object as the "prompts" property
      flatReturn.putProperty("prompts", context.newArray(scope, phrases));

       // Optionally start the training pass
      if (batch && !viseGrammar.startTrainingPass())
        throw new VXMLEvent("error.applet.recognition.training", "Cannot start training pass for grammar \"" + grammarUrl + "\"");

     // Convert exceptions into VXML Events
    } catch (MalformedURLException e) {
      throw new VXMLEvent("error.badfetch.malformedurl", e);
    } catch (PropertyException e) {
      throw new VXMLEvent("error.applet.badproperty", e);
    } catch (NotAFunctionException e) {
      throw new VXMLEvent("error.applet.notafunction", e);
    } catch (JavaScriptException e) {
      throw new VXMLEvent("error.javascript.uncaughtevent", e);
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return the results Object (NOT FlattenedObject)
    return flatReturn.getObject();
  }
}
