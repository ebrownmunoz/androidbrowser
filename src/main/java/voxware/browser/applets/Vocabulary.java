package voxware.browser.applets;

import java.net.MalformedURLException;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.StringTokenizer;

import org.w3c.dom.Element;

 


// JSAPI Imports
import javax.speech.recognition.SpeakerProfile;

import voxware.engine.recognition.sapivise.SapiVise;

 


// JavaScript Imports
import org.mozilla.javascript.*;

// Voxware Imports
import voxware.browser.CachingConverter;
import voxware.browser.FetchHintConverter;
import voxware.browser.Grammar;
import voxware.browser.Speaker;
import voxware.browser.Recognizer;
import voxware.browser.VXMLEvent;
import voxware.browser.VXMLTypes;
import voxware.browser.VXMLURLLoader;
import voxware.browser.VoxwareApplet;
import voxware.voxfile.VoxReport;
import voxware.voxfile.VoxFormatException;

/**
 * Vocabulary is a VoxwareApplet that returns a JavaScript Array of the words in a given script or grammar,
 * each as a JavaScript String.
 *
 * Vocabulary takes up to six parameters:
 *
 *   grammar - the URL of the grammar (OPTIONAL)
 *   script  - a JavaScript Array of phrases (OPTIONAL, defaults to undefined)
 *   profile - the URL of the speaker profile (OPTIONAL unless the type parameter is "unenrolled")
 *   strip   - a String that triggers stripping of the suffixes that it begins (OPTIONAL, defaults to undefined)
 *   type    - a String specifying "all" or "unenrolled" (OPTIONAL, defaults to "all")
 *   caching - a String specifying whether to try to get the grammar/profile from cache (OPTIONAL, defaults to "fast")
 *
 * If the grammar or the script parameter is present and the type is "all", Vocabulary returns a JavaScript Array
 * containing a String for each word in the grammar or script, the script taking precedence. If neither the grammar
 * nor the script is present, but profile is and the type is "all", Vocabulary returns a JavaScript Array containing
 * a String for each word model in the profile. If both the grammar or script and the profile are present and type
 * is "unenrolled", Vocabulary returns a JavaScript Array containing a String for each unenrolled word in the grammar
 * or script. Again, the script takes precedence. Optionally, if strip is present, Vocabulary strips any suffixes
 * starting with the strip String. For example, if strip is "_", Vocabulary strips any suffixes beginning with "_".
 * If stripping maps more than one underlying word name into a String in the resulting Array, that String occurs
 * only once. In other words, the Array that Vocabulary returns is a set, not a list; it contains no duplicate elements.
 *
 * Returns a JavaScript Array of word name Strings.
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter"     if a required parameter is missing or empty
 *   VXMLEvent "error.badgrammar"                  if the grammar parameter is not a valid VISE rec file image
 *   VXMLEvent "error.badprofile"                  if the profile parameter is not a valid VISE voice file image
 *   VXMLEvent "error.badfetch.malformedurl"       if the grammar or profile URL is ill-formed
 *   VXMLEvent "error.applet.illegalparameter"     if the words parameter is present but is not a JavaScript Array
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class Vocabulary extends voxware.browser.VoxwareApplet {

   // Defaults
  public static boolean batchDefault = false;
  public static int     countDefault = 1;

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    FlattenedObject flatParams = new FlattenedObject(params);
    Scriptable results = VXMLTypes.PROTO_UNDEFINED;

    try {
       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Put the params into the scope
      scope.put("params", scope, params);

       // Get the parameters
      Object grammarParam = flatParams.getProperty("grammar");
      Object scriptParam  = flatParams.getProperty("script");
      Object profileParam = flatParams.getProperty("profile");
      Object stripParam   = flatParams.getProperty("strip");
      Object typeParam    = flatParams.getProperty("type");
      Object cachingParam = flatParams.getProperty("caching");

       // Interpret the parameters
      String grammarUrl;
      if (grammarParam == Undefined.instance || (grammarUrl = Context.toString(grammarParam)).length() == 0) grammarUrl = null;
      String[] script = toStringArray(scriptParam, "script");
      String profileUrl;
      if (profileParam == Undefined.instance || (profileUrl = Context.toString(profileParam)).length() == 0) profileUrl = null;
      String strip;
      if (stripParam == Undefined.instance || (strip = Context.toString(stripParam)).length() == 0) strip = null;
      boolean all = !Context.toString(typeParam).equalsIgnoreCase("unenrolled");
      int caching = CachingConverter.FAST_INDEX;
      try {
        caching = ((Integer) CachingConverter.instance().convert(Context.toString(cachingParam))).intValue();
      } catch (Exception e) {}

       // Acquire the necessary images
      byte[] recImage = null;
      if (grammarUrl != null) {
        voxware.browser.Grammar grammar = new voxware.browser.Grammar(grammarUrl, caching, FetchHintConverter.PREFETCH_INDEX);
        voxware.browser.Grammar.Wrapper wrapper = grammar.wrapper(null);
        recImage = (byte[]) wrapper.content();
      }
      byte[] voiImage = null;
      if ((recImage == null || !all) && profileUrl != null) {
    	  
    	  // TODO ERK
//        voxware.browser.Speaker speaker = new voxware.browser.Speaker(new VXMLURLLoader(profileUrl), caching, FetchHintConverter.PREFETCH_INDEX);
//        SpeakerProfile profile = speaker.profile(null);
//        voiImage = ((SapiVise) Recognizer.speechRecognizer()).writeVendorSpeakerProfile(profile.getName());
      }

      String[] words = null;
      if (script == null && recImage == null && voiImage == null) {
        throw new VXMLEvent("error.applet.missingparameter", "\"script\", \"grammar\" and \"profile\" parameters of Vocabulary applet are all missing or empty");
      } else if (!all) {
        if (script == null && recImage == null)
          throw new VXMLEvent("error.applet.missingparameter", "\"script\" and \"grammar\" parameter of Vocabulary applet are both missing or empty");
        else if (voiImage == null)
          throw new VXMLEvent("error.applet.missingparameter", "\"profile\" parameter of Vocabulary applet is missing or empty");
        if (script != null)
          words = missingWords(script, voiImage);
        else
          words = VoxReport.missingWords(new ByteArrayInputStream(recImage), new ByteArrayInputStream(voiImage));  // Throws IOException, VoxFormatException
      } else {
        if (script != null) words = allWords(script);
        else if (recImage != null) words = VoxReport.allWords(new ByteArrayInputStream(recImage));  // Throws IOException, VoxFormatException
        else if (voiImage != null) words = VoxReport.allWords(new ByteArrayInputStream(voiImage));  // Throws IOException, VoxFormatException
      }
      if (words == null) words = new String[0];

      if (strip != null) {
        for (int i = 0; i < words.length; i++) {
          int stripIndex = words[i].indexOf(strip);
          if (stripIndex > 0) words[i] = words[i].substring(0, stripIndex);
        }
         // Eliminate duplicate words
        words = (String[]) new HashSet(Arrays.asList(words)).toArray(new String[0]);
      }

       // Wrap the words in a JavaScript Array
      results = context.newArray(scope, words);

     // Convert exceptions into VXML Events
    } catch (MalformedURLException e) {
      throw new VXMLEvent("error.badfetch.malformedurl", e);
    } catch (IOException e) {
      throw new VXMLEvent("error.badfetch.ioexception", e);
    } catch (VoxFormatException e) {
      throw new VXMLEvent("error.badvoxfile", e);
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return the Array of words
    return results;
  }

  private String[] missingWords(String[] script, byte[] voiImage) throws IOException, VoxFormatException {
    HashSet wordSet = new HashSet(script.length);
    for (int index = 0; index < script.length; index++) {
      StringTokenizer tokens = new StringTokenizer(script[index]);
      while (tokens.hasMoreTokens()) {
        String token = tokens.nextToken();
         // Deal with double-quoted compound words
        if (token.startsWith("\"")) {
          StringBuffer tokenBuffer = new StringBuffer(token);
          tokenBuffer.deleteCharAt(0);
          int length = 0;
          boolean unterminated = true;
          while (((length = tokenBuffer.length()) == 0 || (unterminated = tokenBuffer.charAt(length - 1) != '\"')) && tokens.hasMoreTokens())
            tokenBuffer.append(" ").append(tokens.nextToken());
          if (!unterminated) tokenBuffer.deleteCharAt(length - 1);
          token = tokenBuffer.toString().trim();
        }
        if (token.length() > 0) wordSet.add(token);
      }
    }
    if (voiImage != null) {
      String[] enrolledWords = VoxReport.allWords(new ByteArrayInputStream(voiImage));  // Throws IOException, VoxFormatException
      for (int index = 0; index < enrolledWords.length; index++) wordSet.remove(enrolledWords[index]);
    }
    return (String[]) wordSet.toArray(new String[0]);
  }

  private String[] allWords(String[] script) throws IOException, VoxFormatException {
    return missingWords(script, null);  // Throws IOException, VoxFormatException
  }
}
