package voxware.browser.applets;

import java.net.MalformedURLException;
import org.w3c.dom.Element;

 // JSAPI Imports
import javax.speech.*;
import javax.speech.recognition.*;
import voxware.engine.recognition.sapivise.*;

 // JavaScript Imports
import org.mozilla.javascript.*;

 // Voxware Imports
import voxware.browser.*;
import voxware.util.*;

/**
 * GrammarRules is a VoxwareApplet that returns a JavaScript Array of the names of the rules in the given grammar
 * that parse the given text String. If no rules parse the text, it returns an empty array.
 *
 * GrammarRules takes two parameters:
 *
 *   grammar - the URL of the grammar (REQUIRED)
 *   text    - a String containing the text to be parsed (REQUIRED)
 *
 * Returns a JavaScript Array of rule names.
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter"     if either parameter is missing or empty
 *   VXMLEvent "error.badgrammar"                  if the grammar parameter is not a SapiViseGrammar
 *   VXMLEvent "error.badfetch.malformedurl"       if the grammar URL is ill-formed
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class GrammarRules extends voxware.browser.VoxwareApplet {

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    FlattenedObject flatParams = new FlattenedObject(params);
    Scriptable rules = VXMLTypes.PROTO_UNDEFINED;

    try {
       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Put the params into the scope
      scope.put("params", scope, params);

       // Get the parameters
      Object grammarParam = flatParams.getProperty("grammar");
      Object textParam    = flatParams.getProperty("text");

       // Interpret the parameters
      String url;
      if (grammarParam == Undefined.instance || (url = Context.toString(grammarParam)).length() == 0)
        throw new VXMLEvent("error.applet.missingparameter", "\"grammar\" parameter of GrammarRules applet missing or empty");
      String text;
      if (textParam == Undefined.instance || (text = Context.toString(textParam)).length() == 0)
        throw new VXMLEvent("error.applet.missingparameter", "\"text\" parameter of GrammarRules applet missing or empty");
      
       // Get the SapiViseGrammar
      //voxware.browser.Recognizer recognizer = voxware.browser.Recognizer.instance();
      voxware.browser.Recognizer recognizer = null; // TODO ERK
      javax.speech.recognition.Grammar grammar = recognizer.loadGrammar(new voxware.browser.Grammar(url), null);  // Throws MalformedURLException, VXMLEvent
      if (!(grammar instanceof SapiViseGrammar))
        throw new VXMLEvent("error.badgrammar", "\"" + url + "\" is a " + grammar.getClass().getName() + ", not a SapiViseGrammar");
      SapiViseGrammar viseGrammar = (SapiViseGrammar)grammar;

       // Get the names of the rules that parse the given text
      String[] ruleNames = viseGrammar.parse(text);
      if (ruleNames == null) ruleNames = new String[0];

       // Wrap the rule names in a JavaScript Array
      rules = context.newArray(scope, ruleNames);

     // Convert exceptions into VXML Events
    } catch (MalformedURLException e) {
      throw new VXMLEvent("error.badfetch.malformedurl", e);
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return the Array of rule names
    return rules;
  }
}
