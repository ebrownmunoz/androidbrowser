package voxware.browser.applets;

import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PushbackReader;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import javax.comm.CommPortIdentifier;
import javax.comm.CommPort;
import javax.comm.SerialPort;
import javax.comm.PortInUseException;
import javax.comm.UnsupportedCommOperationException;

import voxware.browser.VoxwareApplet;
import voxware.util.SysLog;
import voxware.browser.InterruptHandler;
import voxware.browser.SystemEvent;
import voxware.browser.SystemMonitor;
import voxware.browser.VXMLEvent;

 


// JavaScript Imports
import org.mozilla.javascript.*;

/**
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter"     if a required parameter is missing or empty
 *   VXMLEvent "error.badgrammar"                  if the grammar parameter is not a valid VISE rec file image
 *   VXMLEvent "error.badprofile"                  if the profile parameter is not a valid VISE voice file image
 *   VXMLEvent "error.badfetch.malformedurl"       if the grammar or profile URL is ill-formed
 *   VXMLEvent "error.applet.illegalparameter"     if any of the parameters for configuring the serial port are illegal
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class EnableSerialIO extends VoxwareApplet {

  static class StringLengthComparator implements Comparator {
    public int compare(Object a, Object b) {
      return ((String)b).length() - ((String)a).length();
    }
  }
  static class LineReader extends Reader {
    protected String[] terminalSequences;
    protected PushbackReader in; // underlying reader
    protected BufferedReader br;
    protected int pushbackAmt = 0;

    public LineReader(Reader r, String[] terminalSequences) {
      if (terminalSequences == null) {
        this.br = new BufferedReader(r);
      } else {
        String[] termSeq = new String[terminalSequences.length];
        System.arraycopy(terminalSequences, 0, termSeq, 0, terminalSequences.length);
        Arrays.sort(termSeq, new StringLengthComparator());
        this.terminalSequences = termSeq;
        this.in = new PushbackReader(r, 1024);
      }
    }

    public String readLine() throws IOException {
      if (br != null) {
        return br.readLine();
      } else {
        return readOneLine();
      }
    }

    /**
     * Reads one line, pushing any extra characters pack onto the PushbackReader
     * @return a String, or null if the underlying stream is closed
     * @throws IOException if the underlying PushbackReader throws an exception
     */
    protected synchronized String readOneLine() throws IOException {
      StringBuffer sb = new StringBuffer();
      int termLoc = -1;
      int bytesRead = 0;

      char[] buffer = new char[1024];
      SysLog.println(5, "pushbackAmt is " + pushbackAmt + " chars");
      bytesRead = in.read(buffer, 0, pushbackAmt > 0 ? pushbackAmt : 1024);
      pushbackAmt = 0;

      if (bytesRead == -1) {
        // EOF reached
        return null;
      }

      while (bytesRead != -1) {
        int oldEOL = sb.length();
        int eol = -1;
        String terminal = null;
        String bufString = new String(buffer, 0, bytesRead);
        sb.append(bufString);
        for (int i = 0; i < terminalSequences.length; i++) {
          SysLog.println(5, "Checking terminal: " + terminalSequences[i]);
//        this does not exist in gcj
//          int potEOL = sb.indexOf(terminalSequences[i], oldEOL);
          int potEOL = bufString.indexOf(terminalSequences[i]);
          if (potEOL != -1) {
            potEOL = oldEOL + potEOL;
          }
          if (potEOL != -1 && (potEOL <= eol || eol == -1)) {
            if (terminal == null || terminal.length() < terminalSequences[i].length() || potEOL < eol) {
              terminal = terminalSequences[i];
            }
            eol = potEOL;
          }
        }

        if (eol != -1) {
          SysLog.println(5, "Found terminal: " + terminal + " (" + terminal.length() + ") bytes");
          // we found a terminal sequence
          String ret = sb.substring(0, eol);
          char[] pushback = sb.substring(eol + terminal.length(), sb.length()).toCharArray();
          pushbackAmt = pushback.length;
          in.unread(pushback);
          return ret;
        }

        bytesRead = in.read(buffer);
      }
      // eof reached?
      return sb.toString();
    }

    public int read(char[] buf, int off, int len) throws IOException {
      if (br != null) {
        return br.read(buf, off, len);
      } else {
        return in.read(buf, off, len);
      }
    }
    public void close() throws IOException {
      if (br != null) {
        br.close();
      } else {
        in.close();
      }
    }
  }

  static class SerialInput implements Runnable {
    private SerialPort sp;
    private InputStream is;
    private InputStreamReader isr;
    private LineReader lr;
    private int index;
    public SerialInput(SerialPort sp, int index, String[] terminalSequences) throws IOException {
      this.sp = sp;
      this.is = sp.getInputStream();
      this.isr = new InputStreamReader(is);      
      this.index = index;
      this.lr = new LineReader(isr, terminalSequences);
    }
    public void run() {
      try {
        String s;
        s = lr.readLine();
        while (!Thread.currentThread().isInterrupted() && s != null) {
          // InterruptHandler.getInstance().genericInterrupt(index, s); ERK
          s = lr.readLine();
        }
      } catch (IOException e) {
        SysLog.println(0, "IOException reading from " + sp.getName() + ": " + e);
        try {
          is.close();
        } catch (IOException e1) {
        }
      }
    }
  }

  static class SerialOutput implements SystemMonitor {
    private SerialPort sp;
    private OutputStream os;
    private int index;
//    private int logLevel = 0;
    public SerialOutput(SerialPort sp) throws IOException {
      this.sp = sp;
      this.os = sp.getOutputStream();
    }
    public void logEvent(int index, String text) {
      try {
        os.write(text.getBytes());
      } catch (IOException e) {
        SysLog.println(0, "IOException writing to " + sp.getName() + ": " + e);
//        SysLog.println(logLevel, "IOException reading from " + sp.getName() + ": " + e);
//        logLevel = 1;
        try {
          os.close();
        } catch (IOException e1) {
        }
      }
    }
  }

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    FlattenedObject flatParams = new FlattenedObject(params);

    try {
       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Put the params into the scope
      scope.put("params", scope, params);

      Object jsLoggerName    = flatParams.getProperty("logger");
      Object jsInterruptName = flatParams.getProperty("interrupt");
      Object jsDeviceName    = flatParams.getProperty("devicename");
      Object jsBaudRate      = flatParams.getProperty("baudrate");
      Object jsDataBits      = flatParams.getProperty("databits");
      Object jsStopBits      = flatParams.getProperty("stopbits");
      Object jsParity        = flatParams.getProperty("parity");
      Object jsFlowControl   = flatParams.getProperty("flowcontrol");
      Object jsTerminator    = flatParams.getProperty("terminator");

      String loggerName    = "comm";
      String interruptName = "comm"; 
      String deviceName    = null;
      int baudRate    = 9600;
      int dataBits    = SerialPort.DATABITS_8;
      int stopBits    = SerialPort.STOPBITS_1;
      int parity      = SerialPort.PARITY_NONE;
      int flowControl = SerialPort.FLOWCONTROL_NONE;
      String[] terminator = null;

      CommPortIdentifier cpIdent;
      SerialPort sp; 
      int interruptSourceIndex = SystemEvent.NO_INDEX;

      if (jsLoggerName != Undefined.instance) {
        loggerName = Context.toString(jsLoggerName);
      }

      if (jsInterruptName != Undefined.instance) {
        interruptName = Context.toString(jsInterruptName);
      }

      if (jsDeviceName != Undefined.instance) {
        String temp = Context.toString(jsDeviceName);
        if (temp.length() != 0) {
          deviceName = temp;
        }
      }
      if (deviceName == null) {
        if (System.getProperty("os.name").equalsIgnoreCase("wince")) {
          deviceName = "COM1:";
        } else {
          deviceName = "/dev/ttyS1";
        }
      }

      if (jsBaudRate != Undefined.instance) {
        baudRate = (int)Context.toNumber(jsBaudRate);
      }

      if (jsDataBits != Undefined.instance) {
        int temp = (int)Context.toNumber(jsDataBits);
        switch (temp) {
          case 5: dataBits = SerialPort.DATABITS_5; break;
          case 6: dataBits = SerialPort.DATABITS_6; break;
          case 7: dataBits = SerialPort.DATABITS_7; break;
          case 8: dataBits = SerialPort.DATABITS_8; break;
          default:
            throw new VXMLEvent("error.applet.illegalparameter", "dataBits parameter must be a number of [5, 6, 7, 8]");
        }
      }
      if (jsStopBits != Undefined.instance) {

        // String temp = Context.toString(jsStopBits);
        double temp = Context.toNumber(jsStopBits);
        if (temp == 1f) {
          stopBits = SerialPort.STOPBITS_1;
        } else if (temp == 1.5f) {
          stopBits = SerialPort.STOPBITS_1_5;
        } else if (temp == 2f) {
          stopBits = SerialPort.STOPBITS_2;
        } else {
          //throw new VXMLEvent("error.applet.illegalparameter", "stopbits parameter must be a string of ['1', '1.5', '2'], not " + temp);
          throw new VXMLEvent("error.applet.illegalparameter", "stopbits parameter must be a number of [1, 1.5, 2], not " + temp);
        }
      }

        
      if (jsParity != Undefined.instance) {
        String temp = Context.toString(jsParity);
        if (temp.equalsIgnoreCase("none")) {
          parity = SerialPort.PARITY_NONE;
        } else if (temp.equalsIgnoreCase("odd")) {
          parity = SerialPort.PARITY_ODD;
        } else if (temp.equalsIgnoreCase("even")) {
          parity = SerialPort.PARITY_EVEN;
        } else if (temp.equalsIgnoreCase("mark")) {
          parity = SerialPort.PARITY_MARK;
        } else if (temp.equalsIgnoreCase("space")) {
          parity = SerialPort.PARITY_SPACE;
        } else {
          throw new VXMLEvent("error.applet.illegalparameter", "parity parameter must be a string of ['none', 'odd', 'even', 'mark', 'space'], not " + temp);
        }
      }

      if (jsFlowControl != Undefined.instance) {
        String temp = Context.toString(jsFlowControl);
        if (temp.equalsIgnoreCase("NONE")) {
          flowControl = SerialPort.FLOWCONTROL_NONE;
        } else if (temp.equalsIgnoreCase("SOFTWARE")) {
          flowControl = SerialPort.FLOWCONTROL_XONXOFF_IN | SerialPort.FLOWCONTROL_XONXOFF_OUT;
        } else if (temp.equalsIgnoreCase("HARDWARE")) {
          flowControl = SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT;
        } else {
          throw new VXMLEvent("error.applet.illegalparameter", "flowcontrol parameter must be a string of ['none', 'software', 'hardware']");
        }
      }
      
      if (jsTerminator != Undefined.instance) {
        throw new VXMLEvent("error.applet.parameter.unsupported", "specifying terminal sequences is unsupported at this time");
      }

      cpIdent = CommPortIdentifier.getPortIdentifier(deviceName);
      if (cpIdent == null) {
        SysLog.println(0, "Could not find device " + deviceName);
        throw new VXMLEvent("error.applet.illegalparameter", "the device \"" + deviceName + "\" does not exist");
      }
      try {
        SysLog.println(0, "Opening " + deviceName);
        sp = (SerialPort)cpIdent.open("browser", 0);
      } catch (PortInUseException e) {
        throw new VXMLEvent("error.applet.illegalparameter", e, "Port is in use");
      }

      try {
        sp.setSerialPortParams(baudRate, dataBits, stopBits, parity);
        sp.setFlowControlMode(flowControl);
        sp.disableReceiveTimeout();
      } catch (UnsupportedCommOperationException e) {
        sp.close();
        throw new VXMLEvent("error.applet.illegalparameter", e, "The given port configuration is not supported by the hardware");
      }

      interruptSourceIndex = SystemEvent.addType(interruptName);
      if (interruptSourceIndex == SystemEvent.NO_INDEX) {
        sp.close();
        throw new VXMLEvent("error.applet.nameconflict", "The name " + interruptName + " is already in use as an interrupt source");
      }

      SerialInput si = new SerialInput(sp, interruptSourceIndex, terminator);
      SerialOutput so = new SerialOutput(sp);

      Thread t = new Thread(si, interruptName + " source");
      t.start();
      
      //Logger.addType(loggerName, so);

     // Convert exceptions into VXML Events
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return the Array of words
    return new Boolean(true);
  }
}
