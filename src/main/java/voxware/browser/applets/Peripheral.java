package voxware.browser.applets;

import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.PushbackReader;
import java.io.OutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;

import voxware.browser.VoxwareApplet;
import voxware.util.SysLog;
import voxware.browser.SystemMonitor;
import voxware.browser.VXMLEvent;

 
// JavaScript Imports
import org.mozilla.javascript.*;

public class Peripheral implements SystemMonitor {

    private String            peripheralName;
    public static int               interruptIndex;

    private static Socket            socket;
    private static InputStreamReader isr;
    private static PushbackReader    pr;
    private static PrintWriter       pw;
    
    private Thread           readerThread; // calls read and fires interrupt?
    private PeripheralReader reader;

    private Thread       socketReaderThread;
    private SocketReader socketReader;
    private static Vector       inputQueue;

    private static int    msgCount   = 0; // counter for messages

    public static boolean asJSResult;
    private boolean shouldQuit;

    private Scriptable readParams;
    private Scriptable writeParams;

	private static Object syncObject;

    public Peripheral(Socket socket, String peripheralName, int interruptIndex, boolean asJSResult) throws IOException {
      syncObject = new Object();
      this.socket = socket;
      this.peripheralName = peripheralName;
      this.interruptIndex = interruptIndex;
      this.asJSResult = asJSResult;
      isr = new InputStreamReader(socket.getInputStream());
      pr = new PushbackReader(isr);
      pw = new PrintWriter(socket.getOutputStream());
      inputQueue = new Vector();
      socketReader = new SocketReader(socket,inputQueue);
      socketReaderThread = new Thread(socketReader, this.peripheralName + " CommThread");
      socketReaderThread.start();
    }

    public void startReader() {
      reader = new PeripheralReader(readParams);
      readerThread = new Thread(reader, this.peripheralName + " Reader");
      readerThread.start();
    }

    public void destroy() {

      pw.close();

      try { 
        pr.close();
      } catch (IOException e) {
      }
      try { 
        pr.close();
      } catch (IOException e) {
      }
      try { 
        socket.close();
      } catch (IOException e) {
      }
    }

    public Scriptable open(Scriptable deviceSpec) throws JavaScriptException, NotAFunctionException, PropertyException {
      SysLog.println(3, "ConfigurePeripheral$Peripheral: open");
      Context    ctx = null;
      Scriptable scope = null;
      Scriptable s = null;

      try {
        ctx = Context.enter();
        scope = ctx.initStandardObjects(null);
        ctx.setLanguageVersion(Context.VERSION_1_2);
        s = ctx.newObject(scope);
        ScriptableObject.putProperty(s, "op", "open");
        ScriptableObject.putProperty(s, "deviceSpec", deviceSpec);
        return sendMessage(s);
      } finally {
        Context.exit();
      }
    }

    public Scriptable config(Scriptable deviceConfig) throws JavaScriptException, NotAFunctionException, PropertyException {
      SysLog.println(3, "ConfigurePeripheral$Peripheral: config");
      Context    ctx = null;
      Scriptable scope = null;
      Scriptable s = null;

      try {
        ctx = Context.enter();
        scope = ctx.initStandardObjects(null);
        ctx.setLanguageVersion(Context.VERSION_1_2);
        s = ctx.newObject(scope);
        ScriptableObject.putProperty(s, "op", "config");
        ScriptableObject.putProperty(s, "deviceConfig", deviceConfig);
        return sendMessage(s);
      } finally {
        Context.exit();
      }
    }

    public static Scriptable read(Scriptable readParams) throws JavaScriptException, NotAFunctionException, PropertyException {
      SysLog.println(3, "ConfigurePeripheral$Peripheral: read");
      Context    ctx = null;
      Scriptable scope = null;
      Scriptable s = null;

      try {
        ctx = Context.enter();
        scope = ctx.initStandardObjects(null);
        ctx.setLanguageVersion(Context.VERSION_1_2);
        s = ctx.newObject(scope);
        ScriptableObject.putProperty(s, "op", "read");
        if ((readParams != null) &&
            (readParams != Context.getUndefinedValue())) {
          ScriptableObject.putProperty(s, "readParams", readParams);
        }
        return sendMessage(s);
      } finally {
        Context.exit();
      }
    }

    public Scriptable write(Scriptable writeParams, Scriptable data) throws JavaScriptException, NotAFunctionException, PropertyException {
      SysLog.println(3, "ConfigurePeripheral$Peripheral: write");
      Context    ctx = null;
      Scriptable scope = null;
      Scriptable s = null;

      try {
        ctx = Context.enter();
        scope = ctx.initStandardObjects(null);
        ctx.setLanguageVersion(Context.VERSION_1_2);
        s = ctx.newObject(scope);
        ScriptableObject.putProperty(s, "op", "write");
        if ((writeParams != null) &&
            (writeParams != Context.getUndefinedValue())) {
          ScriptableObject.putProperty(s, "writeParams", writeParams);
        }
        ScriptableObject.putProperty(s, "data", data);
        return sendMessage(s);
      } finally {
        Context.exit();
      }
    }

    public Scriptable close() throws JavaScriptException, NotAFunctionException, PropertyException {
      SysLog.println(3, "ConfigurePeripheral$Peripheral: close");
      Context    ctx = null;
      Scriptable scope = null;
      Scriptable s = null;

      reader.shouldStop = true;
      readerThread.interrupt();
      try {
        ctx = Context.enter();
        scope = ctx.initStandardObjects(null);
        ctx.setLanguageVersion(Context.VERSION_1_2);
        s = ctx.newObject(scope);
        ScriptableObject.putProperty(s, "op", "close");
        return sendMessage(s);
      } finally {
        Context.exit();
      }
    }

    public void logEvent(int index, String text) {
      SysLog.println(4, "ConfigurePeripheral$Peripheral: logEvent");
      Scriptable scope = null;
      Scriptable s = null;
      Scriptable ret = null;
      Context    ctx = null;      

      try {
        ctx = Context.enter();
        scope = ctx.initStandardObjects(null);
        ctx.setLanguageVersion(Context.VERSION_1_2);
        s = ctx.newObject(scope);
        ScriptableObject.putProperty(s, "type", "text");
        ScriptableObject.putProperty(s, "text", text);
        ret = write(this.writeParams, s);
        // we ignore ret, because we don't have a way of reporting an error to the user
        // maybe we can throw an event, but it is iffy
      } catch (Exception e) {
        SysLog.println(1, "ConfigurePeripheral$Peripheral.logEvent: exception logging data to device: " + e);
      } finally {
        Context.exit();
      }
    }

    protected static Scriptable sendMessage(Scriptable s) throws JavaScriptException, NotAFunctionException, PropertyException {
      SysLog.println(4, "ConfigurePeripheral$Peripheral: sendMessage");
      Context    ctx;
      Scriptable scope;
      Scriptable msg;
      Object     jsToStringResult;
      String     jString;
      int        mCount;

      try {
        synchronized (syncObject) {

          mCount = getMsgCount();
          ctx = Context.enter();
          scope = ctx.initStandardObjects(null);
          ctx.setLanguageVersion(Context.VERSION_1_2);

          msg = ctx.newObject(scope);
          ScriptableObject.putProperty(msg, "msgId", new Integer(mCount));
          ScriptableObject.putProperty(msg, "msgData", s);

          // change toString to toSourceString if language version ever changes
          jsToStringResult = ScriptableObject.callMethod(msg, "toString", new Object[0]);
          if (jsToStringResult != Context.getUndefinedValue()) {
            jString = Context.toString(jsToStringResult);
            doWrite(mCount, jString);
          }
        }

        return doRead(mCount);
      } finally {
        Context.exit();   
      }
    }

    private static int getMsgCount() {
      return msgCount++;
    }

    private static  void doWrite(int mCount, String s) {
      SysLog.println(2, "Peripheral.doWrite(" + mCount + "): " + s);
      pw.println(s);
      pw.flush();
    }

    private static Scriptable doRead(int mCount) throws JavaScriptException, NotAFunctionException, PropertyException {
      SysLog.println(2, "Peripheral.doRead(" + mCount + ")");
      Scriptable s = null;
      Object     ackId = null;
      Object     ackData = null;
      
      try {
        synchronized (inputQueue) {
          while (true) {
            while (inputQueue.isEmpty()) {
              SysLog.println(2, "Peripheral.doRead(" + mCount + "): queue is empty");
              inputQueue.wait();
            }
            for (int i = 0; i < inputQueue.size(); i++) {
              SysLog.println(2, "Peripheral.doRead(" + mCount + "): checking message at index " + i + " of queue sized " + inputQueue.size());
              s = (Scriptable)inputQueue.get(i);
              ackId = ScriptableObject.getProperty(s, "ackId");
              if ((ackId == null) ||
                  (ackId == Scriptable.NOT_FOUND) ||
                  (ackId == Context.getUndefinedValue())) {
                SysLog.println(2, "Peripheral.doRead(" + mCount + "): queue contains a message without valid ackId");
                inputQueue.remove(i);
                i--;
                continue;
              }
              ackData = ScriptableObject.getProperty(s, "ackData");
              if ((ackData == null) ||
                  (ackData == Scriptable.NOT_FOUND) ||
                  (ackData == Context.getUndefinedValue()) ||
                  !(ackData instanceof Scriptable)) {
                SysLog.println(2, "Peripheral.doRead(" + mCount + "): queue contains a message without valid ackData");
                inputQueue.remove(i);
                i--;
                continue;
              }
              if ((int)Context.toNumber(ackId) == mCount) {
                inputQueue.remove(i);
                return (Scriptable)ackData;
              }
              SysLog.println(2, "Peripheral.doRead(" + mCount + "): message at index " + i + " of queue sized " + inputQueue.size() + " with ackId " + (int)Context.toNumber(ackId) + " is not mine");
            }
            SysLog.println(2, "Peripheral.doRead(" + mCount + "): did not find the message we are looking for");
            inputQueue.wait();
          }
        }
      } catch (Exception e) {
        SysLog.println(2, "Peripheral.doRead(" + mCount + "): exception: " + e);
      }
      return (Scriptable)Context.getUndefinedValue();
    }

    public void setReadParams(Scriptable readParams) {
      SysLog.println(1, "readParams = " + Context.toString(readParams));
      this.readParams = readParams;
    }

    public void setWriteParams(Scriptable writeParams) {
      SysLog.println(1, "writeParams = " + Context.toString(writeParams));
      this.writeParams = writeParams;
    }
  }
