package voxware.browser.applets;

import java.net.MalformedURLException;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.StringTokenizer;

import org.w3c.dom.Element;

 


// JSAPI Imports
import javax.speech.recognition.SpeakerProfile;

import voxware.engine.recognition.sapivise.SapiVise;

 


// JavaScript Imports
import org.mozilla.javascript.*;

// Voxware Imports
import voxware.browser.CachingConverter;
import voxware.browser.DurationConverter;
import voxware.browser.FetchHintConverter;
import voxware.browser.Grammar;
import voxware.browser.Speaker;
import voxware.browser.Recognizer;
import voxware.browser.VXMLEvent;
import voxware.browser.VXMLTypes;
import voxware.browser.VoxwareApplet;
import voxware.voxfile.VoxReport;
import voxware.voxfile.VoxFormatException;

/**
 * GetGrammar is a VoxwareApplet that returns a JavaScript Object containing the name and
 * the contents of the specified grammar. It acquires the grammar from the cache if possible;
 * otherwise, it gets it from the URL.
 *
 * GetGrammar takes one parameters:
 *
 *   grammar - the URL of the grammar (REQUIRED)
 *
 * Returns a JavaScript Object containing two properties:
 *
 *   name - the name of the grammar (a String)
 *   image - the byte[] image of the recognition file (.rec)
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter" if either parameter is missing or empty
 *   VXMLEvent "error.badfetch.malformedurl"   if the grammar URL is ill-formed
 *   VXMLEvent "error.badfetch.timedout"       if "load" is on and acquisition times out
 *   VXMLEvent "error.badfetch.ioerror"        if "load" is on and acquisition fails
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class GetGrammar extends voxware.browser.VoxwareApplet {

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    FlattenedObject flatParams = new FlattenedObject(params);
    FlattenedObject flatResults = null;

    try {
       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Put the params into the scope
      scope.put("params", scope, params);

       // Create a results Object and put it into the scope
       // Throws PropertyException, NotAFunctionException, JavaScriptException
      Scriptable results = context.newObject(scope);
      scope.put("results", scope, results);
       // Flatten it to make it easy to deal with
      flatResults = new FlattenedObject(results);

       // Get the parameters
      Object grammarParam = flatParams.getProperty("grammar");
      Object loadParam    = flatParams.getProperty("load");
      Object timeoutParam = flatParams.getProperty("timeout");

       // Interpret them
      String url;
      if (grammarParam == Undefined.instance || (url = Context.toString(grammarParam)).length() == 0)
        throw new VXMLEvent("error.applet.missingparameter", "\"grammar\" parameter of GetGrammar applet missing or empty");
      boolean load = (loadParam != Undefined.instance) ? Context.toBoolean(loadParam) : false;
      int timeout = (timeoutParam != Undefined.instance) ? (int)Context.toNumber(timeoutParam) : DurationConverter.UNSPECIFIED;

       // Acquire the grammar image
      byte[] recImage = null;
      voxware.browser.Grammar.Wrapper wrapper = null;
      if (url != null) {
        voxware.browser.Grammar grammar = new voxware.browser.Grammar(url);
        if (grammar != null) wrapper = grammar.wrapper(null);
        recImage = (byte[]) wrapper.content();
      }

      if (recImage != null) {
        flatResults.putProperty("name", wrapper.name());
        flatResults.putProperty("image", recImage);
      } else {
        flatResults.putProperty("name", VXMLTypes.PROTO_UNDEFINED);
        flatResults.putProperty("image", VXMLTypes.PROTO_UNDEFINED);
      }

     // Convert all the exceptions into VXML Events
    } catch (SecurityException e) {
      throw new VXMLEvent("error.vise.speaker.nomanager");
    } catch (MalformedURLException e) {
      throw new VXMLEvent("error.badfetch.malformedurl", e);
    } catch (IOException e) {
      throw new VXMLEvent("error.badfetch.ioerror", e);
    } catch (PropertyException e) {
      throw new VXMLEvent("error.applet.badproperty", e);
    } catch (NotAFunctionException e) {
      throw new VXMLEvent("error.applet.notafunction", e);
    } catch (JavaScriptException e) {
      throw new VXMLEvent("error.javascript.uncaughtevent", e);
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return the results Object (NOT FlattenedObject)
    return flatResults.getObject();
  }
}
