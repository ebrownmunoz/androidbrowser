package voxware.browser.applets;

import java.net.MalformedURLException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

 // JSAPI Imports
import javax.speech.*;
import javax.speech.recognition.*;
import voxware.engine.recognition.sapivise.*;

 // JavaScript Imports
import org.mozilla.javascript.*;

 // Voxware Imports
import voxware.browser.*;
import voxware.util.*;

/**
 * TrainUtterances is a VoxwareApplet that terminates an initial training pass and then trains iteratively
 * over the set of ViseResults collected during that pass. It presuppoese that you have used the
 * the TrainingPrompts VoxwareApplet with parameter "batch" true to start the initial pass.
 *
 * It takes up to eight parameters, all but two of which are optional:
 *
 *   grammar    - the URL of the grammar used during collection (REQUIRED)
 *   results    - a JavaScript Array containing the ViseResults collected during the initial pass (REQUIRED)
 *   words      - A JavaScript Array of the words to train (OPTIONAL, defaults to all the words in the grammar)
 *   score      - the total (non-negative) of all pathscores during collection (OPTIONAL, defaults to Long.MAX_VALUE)
 *   passcount  - the maximum number (non-negative) of training iterations (OPTIONAL, defaults to Integer.MAX_VALUE)
 *   traincount - the minimum number (greater than zero) of times a word must occur to be trained (OPTIONAL, defaults to 1)
 *   log        - log training events to SysLog (OPTIONAL, defaults to false)
 *   punt       - throw an exception if an element in the "results" array does not support training (OPTIONAL, defaults to false)
 *
 * If the "passcount" parameter is zero, the initial training pass is terminated but no iterative training is done.
 *
 * The "results" parameter must be a JavaScript Array (or Object) containing the ViseResults collected during the initial
 * training pass. By default, TrainUtterances will silently ignore any result that is not a ViseResult or any ViseResult that
 * is not in a state that supports tokenCorrection(), correctTokens() and pathScore() (i.e., finalized ViseResults on which
 * tokenCorrection() has been successfully called during the initial training and collection pass). You can change this
 * behavior by supplying true values for the "log" and/or "punt" parameters
 * 
 * Returns a JavaScript Object containing two properties:
 *
 *   name - the name of the speaker profile being trained (a String)
 *   image - the byte[] image of the speaker profile (.voi)
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter"     if the grammar parameter or the results parameter is missing
 *   VXMLEvent "error.badgrammar"                  if the grammar parameter is not a SapiViseGrammar
 *   VXMLEvent "error.badfetch.malformedurl"       if the grammar URL is ill-formed
 *   VXMLEvent "error.applet.illegalparameter"     if the results or words parameter is not a JavaScript Object, or
 *                                                 if a "score", "passcount" or "traincount" parameter is out of range.
 *   VXMLEvent "error.applet.recognition.training" if it cannot start or stop a training pass, or (optionally) if
 *                                                 it encounters a result that does not support training
 *   VXMLEvent "error.vise.speaker.nospeaker"      if it cannot get the current speaker profile from the SpeakerManager.
 *                                                 The training has completed, but the applet cannot find the profile
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class TrainUtterances extends voxware.browser.VoxwareApplet {

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    FlattenedObject flatParams = new FlattenedObject(params);
    FlattenedObject flatReturn = null;

    try {
       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Put the params into the scope
      scope.put("params", scope, params);

       // Create a results Object and put it into the scope
       // Throws PropertyException, NotAFunctionException, JavaScriptException
      Scriptable results = context.newObject(scope);
      scope.put("results", scope, results);
       // Flatten it to make it easy to deal with
      flatReturn = new FlattenedObject(results);

       // Get the parameters
      Object grammarParam =    flatParams.getProperty("grammar");
      Object resultsParam =    flatParams.getProperty("results");
      Object wordsParam =      flatParams.getProperty("words");
      Object scoreParam =      flatParams.getProperty("score");
      Object passCountParam =  flatParams.getProperty("passcount");
      Object trainCountParam = flatParams.getProperty("traincount");
      Object logParam =        flatParams.getProperty("log");
      Object puntParam =       flatParams.getProperty("punt");

       // Interpret the parameters
      String grammarURL;
      if (grammarParam == Undefined.instance || (grammarURL = Context.toString(grammarParam)).length() == 0)
        throw new VXMLEvent("error.applet.missingparameter", "\"grammar\" parameter of TrainUtterances applet missing or empty");
      if (resultsParam == Undefined.instance)
        throw new VXMLEvent("error.applet.missingparameter", "\"results\" parameter of TrainUtterances applet missing or empty");
      if (!(resultsParam instanceof FlattenedObject))
        throw new VXMLEvent("error.applet.illegalparameter", "\"results\" parameter of TrainUtterances is a " + resultsParam.getClass().getName());
      FlattenedObject flatResults = (FlattenedObject)resultsParam;
      String[] words = null;
      if (wordsParam == Undefined.instance) {
        words = null;
      } else if (!(wordsParam instanceof FlattenedObject)) {
        throw new VXMLEvent("error.applet.illegalparameter", "\"words\" parameter of TrainUtterances is a " + wordsParam.getClass().getName());
      } else {
         // Construct a Java String[] of the desired word Strings
        Scriptable wordsObject = ((FlattenedObject)wordsParam).getObject();
        if (wordsObject != Undefined.instance) {
          Object[] wordObjects = context.getElements(wordsObject); // Throws IllegalArgumentException
          words = new String[wordObjects.length];
          Object wordObject;
          for (int i = 0; i < wordObjects.length; i++) {
            wordObject = wordObjects[i];
            if (wordObject == Context.getUndefinedValue() || wordObject == null) words[i] = null;
            else                                                                 words[i] = Context.toString(wordObject);
          }
        }
      }
      long score = (scoreParam != Undefined.instance) ? (long)Context.toNumber(scoreParam) : Long.MAX_VALUE;
      if (score < 0)
        throw new VXMLEvent("error.applet.illegalparameter", "\"score\" parameter of TrainUtterances (" + Long.toString(score) + ") is less than zero");
      int passCount = (passCountParam != Undefined.instance) ? (int)Context.toNumber(passCountParam) : Integer.MAX_VALUE;
      if (passCount < 0)
        throw new VXMLEvent("error.applet.illegalparameter", "\"passcount\" parameter of TrainUtterances (" + passCount + ") is not greater than zero");
      int trainCount = (trainCountParam != Undefined.instance) ? (int)Context.toNumber(trainCountParam) : 1;
      if (trainCount <= 0)
        throw new VXMLEvent("error.applet.illegalparameter", "\"traincount\" parameter of TrainUtterances (" + trainCount + ") is not greater than zero");
      boolean log = (logParam != Undefined.instance) ? Context.toBoolean(logParam) : false;
      boolean punt = (puntParam != Undefined.instance) ? Context.toBoolean(puntParam) : false;
      

       // Get the SapiViseGrammar
      voxware.browser.Recognizer recognizer = null; // TODO ERK
      //voxware.browser.Recognizer recognizer = voxware.browser.Recognizer.instance();
      javax.speech.recognition.Grammar grammar = recognizer.loadGrammar(new voxware.browser.Grammar(grammarURL), null);  // Throws MalformedURLException, VXMLEvent
      if (!(grammar instanceof SapiViseGrammar))
        throw new VXMLEvent("error.badgrammar", "TrainUtterances: \"" + grammarURL + "\" is a " + grammar.getClass().getName() + ", not a SapiViseGrammar");
      SapiViseGrammar viseGrammar = (SapiViseGrammar)grammar;

       // End the current training pass
      if (!viseGrammar.endTrainingPass())
        throw new VXMLEvent("error.applet.recognition.training", "TrainUtterances cannot end the initial training pass for grammar \"" + grammarURL + "\"");

      Object[] ids = flatResults.getIds();

      if (log) {
        SysLog.println("======= SUMMARY OF TRAINING PASS 0 =======");
        SysLog.println("       Total Utterances: " + ids.length);
        SysLog.println("            Total Score: " + score);
        SysLog.println("========= END OF TRAINING PASS 0 =========");
      }

       // Iterate over the results until the passCount is exhausted or the total score begins to increase
      int   pass = 0;

      while (pass < passCount) {

        long  previousScore = score;
        int   index;
        int   phraseCount = 0;

        score = 0;
        pass++;

        if (!viseGrammar.startTrainingPass())
          throw new VXMLEvent("error.applet.recognition.training", "TrainUtterances cannot start training pass " + pass + " for grammar \"" + grammarURL + "\"");
        else if (log)
          SysLog.println("======== STARTING TRAINING PASS " + pass + " ========");

         // RECOGNITION LOOP ON RESULTS collected on the first pass of BATCH TRAINING
        for (index = 0; index < ids.length; index++) {
          if (ids[index] != null) {
            try {
              ViseResult result = (ViseResult)flatResults.getProperty(ids[index]);
              result.tokenCorrection(result.correctTokens(), null, null, FinalResult.MISRECOGNITION);
            } catch (ResultStateError e) {
              if (log) SysLog.println("TrainUtterances: result[" + ids[index].toString() + "] does not support correction");
               // Optionally throw a VXMLEvent if the Result does not support tokenCorrection() and correctTokens()
              if (punt) throw new VXMLEvent("error.applet.recognition.training", e, "TrainUtterances: result[" + ids[index].toString() + "] does not support correction");
              ids[index] = null;
              continue;
            } catch (ClassCastException e) {
              if (log) SysLog.println("TrainUtterances: result[" + ids[index].toString() + "] is not a ViseResult");
               // Optionally throw a VXMLEvent if the Result is not a ViseResult
              if (punt) throw new VXMLEvent("error.applet.recognition.training", e, "TrainUtterances: result[" + ids[index].toString() + "] is not a ViseResult");
              ids[index] = null;
              continue;
            } catch (IllegalArgumentException e) {
              throw new VXMLEvent("error.applet.recognition.training", e, "TrainUtterances: result[" + ids[index].toString() + "]");
            }
          }
        }
         // Compute new templates. This synchronized command makes sure that all corrections took place
        if (!viseGrammar.endTrainingPass(words))
          throw new VXMLEvent("error.applet.recognition.training", "TrainUtterances cannot end training pass " + pass + " for grammar \"" + grammarURL + "\"");

         // Now sum the path scores of all the results
        for (index = 0; index < ids.length; index++) {
          if (ids[index] != null) {
            try {
              ViseResult result = (ViseResult)flatResults.getProperty(ids[index]);
              score += result.pathScore();
              phraseCount++;
            } catch (ResultStateError e) {
              if (log) SysLog.println("TrainUtterances: result[" + ids[index].toString() + "] does not support pathscore()");
               // Optionally throw a VXMLEvent if the Result does not support pathScore()
              if (punt) throw new VXMLEvent("error.applet.recognition.training", e, "TrainUtterances: result[" + ids[index].toString() + "] does not support pathscore()");
              ids[index] = null;
              continue;
            } catch (ClassCastException e) {
              if (log) SysLog.println("TrainUtterances: result[" + ids[index].toString() + "] it is not a ViseResult");
               // Optionally throw a VXMLEvent if the Result is not a ViseResult
              if (punt) throw new VXMLEvent("error.applet.recognition.training", e, "TrainUtterances: result[" + ids[index].toString() + "] is not a ViseResult");
              ids[index] = null;
              continue;
            }
          }
        }

        if (log) {
          SysLog.println("======= SUMMARY OF TRAINING PASS " + pass + " =======");
          SysLog.println("  Total Usable Utterances: " + phraseCount);
          SysLog.println("              Total Score: " + score);
          SysLog.println("              Delta Score: " + (score - previousScore));
          SysLog.println("========= END OF TRAINING PASS " + pass + " =========");
        }

         // Decide whether we're finished
        if (score >= previousScore) break;
      }

       // Tighten the min/max-dwells in word models
      if (!viseGrammar.completeTraining(words))
        throw new VXMLEvent("error.applet.recognition.training", "TrainUtterances failed to complete Training");
      else if (!viseGrammar.saveModels(words, trainCount))
        throw new VXMLEvent("error.applet.recognition.training", "TrainUtterances failed to save word models");

       // Get the image of the current speaker profile in a byte[]
      SpeakerManager speakerManager = rec.getSpeakerManager();
      SpeakerProfile profile = speakerManager.getCurrentSpeaker();
      if (profile != null) {
        ByteArrayOutputStream profileStream = new ByteArrayOutputStream();
        speakerManager.writeVendorSpeakerProfile(profileStream, profile); // Throws SecurityException, MalformedURLException, IOException
        flatReturn.putProperty("name", profile.getName());
        flatReturn.putProperty("image", profileStream.toByteArray());
      } else {
        throw new VXMLEvent("error.vise.speaker.nospeaker", "TrainUtterances cannot get the current speaker profile from the SpeakerManager");
      }

     // Convert all the exceptions into VXML Events
    } catch (SecurityException e) {
      throw new VXMLEvent("error.vise.speaker.nomanager", e, "TrainUtterances cannot access the SpeakerManager");
    } catch (MalformedURLException e) {
      throw new VXMLEvent("error.badfetch.malformedurl", e);
    } catch (IOException e) {
      throw new VXMLEvent("error.badfetch.ioerror", e);
    } catch (PropertyException e) {
      throw new VXMLEvent("error.applet.badproperty", e);
    } catch (NotAFunctionException e) {
      throw new VXMLEvent("error.applet.notafunction", e);
    } catch (JavaScriptException e) {
      throw new VXMLEvent("error.javascript.uncaughtevent", e);
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return the results Object (NOT FlattenedObject)
    return flatReturn.getObject();
  }
}
