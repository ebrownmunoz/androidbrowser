package voxware.browser.applets;

import java.net.MalformedURLException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

 // JavaScript Imports
import org.mozilla.javascript.*;

 // Voxware Imports
import voxware.browser.DurationConverter;
import voxware.browser.VXMLEvent;
import voxware.browser.VXMLTypes;
import voxware.browser.VXMLURLLoader;

/**
 * GetFile is a VoxwareApplet that returns a JavaScript Object containing the name and the contents
 * of the specified file. It can acquire the file from the local file system or from a URL.
 *
 * GetFile takes three parameters:
 *
 *   source  - the URL or file name (REQUIRED)
 *   type    - a String ("url" or "file") specifying the type of source (OPTIONAL, defaults to "url")
 *   timeout - the timeout (in milliseconds) on acquisition from a URL; it has no effect when the
 *             acquisition type is "file". (OPTIONAL, defaults to session.fetchtimeout)
 *
 * Returns a JavaScript Object containing two properties:
 *
 *   name  - the name of the file or URL (a String)
 *   image - the byte[] image of the file
 *   size  - the size of the image in bytes
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter"     if the "source" parameter is missing or empty
 *   VXMLEvent "error.applet.illegalparameter"     if a type other than "url" or "file" is requested
 *   VXMLEvent "error.badfetch.malformedurl"       if the URL is ill-formed
 *   VXMLEvent "error.badfetch.filenotfound"       if the file cannot be found
 *   VXMLEvent "error.badfetch.timedout"           if acquisition times out
 *   VXMLEvent "error.badfetch.ioerror"            if an error occurs during acquisition
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class GetFile extends voxware.browser.VoxwareApplet {

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    FlattenedObject flatParams = new FlattenedObject(params);
    FlattenedObject flatResults = null;

    try {
       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Put the params into the scope
      scope.put("params", scope, params);

       // Create a results Object and put it into the scope
       // Throws PropertyException, NotAFunctionException, JavaScriptException
      Scriptable results = context.newObject(scope);
      scope.put("results", scope, results);
       // Flatten it to make it easy to deal with
      flatResults = new FlattenedObject(results);

       // Get the parameters
      if (!flatParams.hasProperty("source"))
        throw new VXMLEvent("error.applet.missingparameter", "\"source\" parameter of GetFile applet missing");
      String source = Context.toString(flatParams.getProperty("source"));
      if (source == null || source.length() == 0)
        throw new VXMLEvent("error.applet.illegalparameter", "\"source\" parameter of GetFile applet empty");
      String type = flatParams.hasProperty("type") ? Context.toString(flatParams.getProperty("type")) : "url";
      int timeout = flatParams.hasProperty("timeout") ? (int)Context.toNumber(flatParams.getProperty("timeout")) : DurationConverter.UNSPECIFIED;

       // Get the image of the file in a byte[]
      byte[] bytes = null;
      String name = null;
      if (type.equalsIgnoreCase("url")) {
        VXMLURLLoader loader = new VXMLURLLoader(source);             // Throws MalformedURLException
        name = loader.toString();
        bytes = loader.bytes(timeout);                                // Throws VXMLEvent
      } else if (type.equalsIgnoreCase("file")) {
        name = source;
        File file = new File(source).getCanonicalFile();              // Throws IOException
        if (!file.isFile())
          throw new VXMLEvent("error.badfetch.filenotfound", source);
        FileInputStream fis = new FileInputStream(file);              // Throws FileNotFoundException
        int unread = (int)file.length();
        bytes = new byte[unread];
        int offset = 0;
        int chunkSize = 0;
        while (chunkSize >= 0 && unread > 0) {
          chunkSize = fis.read(bytes, offset, unread);                // Throws IOException
          offset += chunkSize;
          unread -= chunkSize;
        }
        fis.close();                                                  // Throws IOException
      } else {
        throw new VXMLEvent("error.applet.illegalparameter", "\"" + type + "\" is an invalid source type");
      }

      if (name != null) flatResults.putProperty("name", name);
      else              flatResults.putProperty("name", VXMLTypes.PROTO_UNDEFINED);
      if (name != null) flatResults.putProperty("image", bytes);
      else              flatResults.putProperty("name", VXMLTypes.PROTO_UNDEFINED);
      if (name != null) flatResults.putProperty("size", VXMLTypes.intToNumber(bytes.length));
      else              flatResults.putProperty("size", VXMLTypes.PROTO_UNDEFINED);

     // Convert all the exceptions into VXML Events
    } catch (SecurityException e) {
      throw new VXMLEvent("error.badfetch.readprotected");
    } catch (MalformedURLException e) {
      throw new VXMLEvent("error.badfetch.malformedurl", e);
    } catch (FileNotFoundException e) {
      throw new VXMLEvent("error.badfetch.filenotfound", e);
    } catch (IOException e) {
      throw new VXMLEvent("error.badfetch.ioerror", e);
    } catch (PropertyException e) {
      throw new VXMLEvent("error.applet.badproperty", e);
    } catch (NotAFunctionException e) {
      throw new VXMLEvent("error.applet.notafunction", e);
    } catch (JavaScriptException e) {
      throw new VXMLEvent("error.javascript.uncaughtevent", e);
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e);
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return the results Object (NOT FlattenedObject)
    return flatResults.getObject();
  }
}
