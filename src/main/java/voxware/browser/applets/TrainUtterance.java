package voxware.browser.applets;

import java.util.StringTokenizer;

 // JSAPI Imports
import javax.speech.*;
import javax.speech.recognition.*;
import voxware.engine.recognition.sapivise.*;

 // JavaScript Imports
import org.mozilla.javascript.*;

 // Voxware Imports
import voxware.browser.*;
import voxware.util.*;

/**
 * TrainUtterance is a VoxwareApplet that incrementally trains the words in a ViseResult and returns
 * the training score. You would call this (most likely with "retain" set false) on each result during
 * normal incremental training. You must also call it (with "retain" set true, the default) on each result
 * during the initial (result collection) pass of batch training to install the correct recognition result
 * in the ViseResults object.
 *
 * It takes up to three parameters:
 *
 *   result - a ViseResult (REQUIRED)
 *   prompt - a String representing the correct recognition result (OPTIONAL, defaults to the recognition result)
 *   retain - a Boolean specifying whether to retain the training information in the ViseResult (OPTIONAL, defaults to true)
 *
 * Returns the training score (as a JavaScript Number).
 *
 * Throws:
 *
 *   VXMLEvent "error.applet.missingparameter"      if the result parameter is not supplied
 *   VXMLEvent "error.applet.illegalparameter"      if the result parameter is not a ViseResult or the prompt is empty
 *   VXMLEvent "error.applet.recognition.training"  if the result does not support training
 *   Miscellaneous other VXMLEvents common to all applets
 */
public class TrainUtterance extends voxware.browser.VoxwareApplet {

  public synchronized Object run(Scriptable params) throws VXMLEvent {

    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VARIABLES_MASK)) {
      try {
        SysLog.println("TrainUtterance.run: parameters = " + ((ScriptableObject)params).toString());
      } catch (IllegalArgumentException e) {}
    }
 
    FlattenedObject flatParams = new FlattenedObject(params);
    Object score = Undefined.instance;

    try {

       // Get a Context and root the JavaScript scope chain
      Context context = Context.enter();
      Scriptable scope = context.initStandardObjects(null);

       // Put the params into the scope
      scope.put("params", scope, params);

       // Get the parameters
      Object resultParam = flatParams.getProperty("result");
      Object promptParam = flatParams.getProperty("prompt");
      Object retainParam = flatParams.getProperty("retain");

       // Interpret the parameters
      if (resultParam == Undefined.instance) {
        throw new VXMLEvent("error.applet.missingparameter", "REQUIRED \"result\" parameter of TrainUtterance applet missing");
      } else if (!(resultParam instanceof ViseResult)) {
        throw new VXMLEvent("error.applet.illegalparameter", "\"result\" parameter of TrainUtterance applet must be a ViseResult, not a " + resultParam.getClass().getName());
      } else {
        ViseResult result = (ViseResult)resultParam;
        if (promptParam == Undefined.instance) {
          result.tokenCorrection(null, null, null, ViseResult.DONT_KNOW);                 // Throws ResultStateError, IllegalArgumentException
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.OBJECT_MASK))
            SysLog.println("TrainUtterance.run: called ViseResult.tokenCorrection() without correction");
        } else {
          String prompt = Context.toString(promptParam);
          if (prompt.length() == 0)
            throw new VXMLEvent("error.applet.illegalparameter", "\"prompt\" parameter of TrainUtterance is empty");
          StringTokenizer tokenizer = new StringTokenizer(prompt);
          String[] correctTokens = new String[tokenizer.countTokens()];
          int index = 0;
          while (tokenizer.hasMoreTokens()) correctTokens[index++] = tokenizer.nextToken();
          result.tokenCorrection(correctTokens, null, null, ViseResult.MISRECOGNITION);   // Throws ResultStateError, IllegalArgumentException 
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.OBJECT_MASK))
            SysLog.println("TrainUtterance.run: called ViseResult.tokenCorrection() with correction to \"" + prompt + "\"");
        }
         // Return the result's path score
        score = VXMLTypes.intToNumber(result.pathScore());
         // Release the training information in the result if the "retain" parameter is not true
        if (retainParam != Undefined.instance && !Context.toBoolean(retainParam)) {
          ((ViseResult)result).releaseTrainingInfo();
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.OBJECT_MASK))
            SysLog.println("TrainUtterance.run: released training info");
        }
      }

    } catch (ResultStateError e) {
      throw new VXMLEvent("error.applet.recognition.training", e, "TrainUtterance.tokenCorrection");
    } catch (Exception e) {
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      throw new VXMLEvent("error.applet.exception", e, "TrainUtterance");
    } finally {
       // Leave the applet's Context
      Context.exit();
    }

     // Return the score
    return score;
  }
}
