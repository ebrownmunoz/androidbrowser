package voxware.browser;

import java.util.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.*;

 /** A Form
   * A Form's parent is a Page (document) and its children are Fields, Blocks and Objects */
class Form extends ScopeElement {

  static int instanceCount = 0;

  private Element      form;
  private boolean      bargein;
  private boolean      localscope;
  private Vector       filleds;
  private HashVector   fieldItems;     // A HashVector of the child FieldItems
  private Vector       initials;       // A Vector of the child Initials

private final  Session session;

   /** Construct a childless, orphan Form from a <form> element in a DOM tree */
  public Form(Session session, Element form) throws DOMException, VXMLEvent {
    super();
    
    this.session = session;
    
    prefix = "dialog";

    this.form = form;
    this.setIndex(Elements.FORM_INDEX);

    handler = new EventHandler();
    grammars = new ObjectSet();
    children = new HashVector();
    prompts = new Prompts();
    filleds = new Vector();

     /** Get the attributes of the <form> tag */

     /* Get the Form id */
    Attr attr = form.getAttributeNode("id");
    name = (attr != null) ? attr.getNodeValue() : "_FORM" + Integer.toString(++instanceCount);  // Throws DOMException
     /* Get the bargein boolean */
    attr = form.getAttributeNode("bargein");
    bargein = (attr != null) ? ((Boolean)BooleanConverter.instance().convert(attr.getNodeValue())).booleanValue() : true; // Throws DOMException, VXMLEvent
     /* Get the scope of the <form> grammars */
    attr = form.getAttributeNode("scope");
    localscope = (attr == null || !attr.getNodeValue().equalsIgnoreCase("document"));  // Throws DOMException
  }

   /** Get the Form's ID */
  public String id() {
    return name;
  }

   /** Return a HashVector of all the Form's FieldItems */
  public HashVector fieldItems() {
     // Construct the HashVector if necessary
    if (fieldItems == null) {
      fieldItems = new HashVector(children.size());
      if (children != null && children.size() > 0) {
        for (Enumeration e = children.elements(); e.hasMoreElements();) {
          Object element = e.nextElement();
          if (element instanceof FieldItem)
            fieldItems.put(((FormItem)element).name(), element);
        }
      }
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.FORM_MASK))
        SysLog.println("Form.fieldItems(): accumulated " + ((fieldItems != null) ? fieldItems.size() : 0) + " field item(s)");
    }
    return fieldItems;
  }

   /** Return a Vector of all the Form's Initials */
  public Vector initials() {
     // Construct the Vector if necessary
    if (initials == null) {
      initials = new Vector(1);
      if (children != null && children.size() > 0) {
        for (Enumeration e = children.elements(); e.hasMoreElements();) {
          Object element = e.nextElement();
          if (element instanceof Initial)
            initials.addElement(element);
        }
      }
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.FORM_MASK))
        SysLog.println("Form.initials(): accumulated " + ((initials != null) ? initials.size() : 0) + " initial(s)");
    }
    return initials;
  }

   /** Return true iff the name is the name of a FieldItem */
  public boolean isFieldItemName(String name) {
    fieldItems();  // Make sure that the HashVector of FieldItems has been constructed
    return fieldItems.containsKey(name);
  }

   /** Return true iff any of the Form's FieldItems are filled */
  public boolean anyFieldItemFilled() throws UndeclaredVariableException {
    fieldItems();  // Make sure that the HashVector of FieldItems has been constructed
    if (fieldItems != null) {
      Enumeration items = fieldItems.elements();
      while (items.hasMoreElements())
        if (!VXMLTypes.isUndefined(variable(((FormItem)items.nextElement()).name())))
          return true;
    }
    return false;
  }

   /** Add a Filled object to the filleds */
  public void addFilled(Filled filled) {
    this.filleds.addElement(filled);
    filled.makeChildOf(this);
  }

   /** Make a Filled and register it in this Form's scope */
  public Filled makeFilled(Element element, Goto context, boolean preprocess) throws VXMLEvent {
    Filled filled = new Filled(session, element, context, preprocess);  // Throws VXMLEvent
    addFilled(filled);
    return filled;
  }

   // Execute the <filled> elements in document order
  private void executeFilleds(Variables variables, Goto context) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
    Enumeration filleds = this.filleds.elements();
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.FILLED_MASK))
      SysLog.println("Form.executeFilleds(): trying " + this.filleds.size() + " possible <filled> actions");
    while (filleds.hasMoreElements()) {
      ((Filled)filleds.nextElement()).execute(variables, this, context);
    }
  }

   /** Clear the named form item */
  public void clearFormItem(String name) {
    Object element = children.get(name);
    if (element != null && element instanceof FormItem)
      ((FormItem)element).clear(true);
  }


   /** Clear all the Form's items */
  public void clearFormItems() {
    clearFormItems(true);
  }

   // Clear all the Form's items, optionally clearing their guard variables
  private void clearFormItems(boolean undefine) {
     // Reset each FormItem's prompt and event counts
    for (Enumeration e = children.elements(); e.hasMoreElements();) {
      Object element = e.nextElement();
      if (element instanceof FormItem)
        ((FormItem)element).clear(undefine);
    }
  }

   /** Interpret the Form */
  public Variables interpret(Goto whereToGo, InterruptHandler interruptHandler) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
     // Implements the pseudo-code in Appendix C of the VXML Specification
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.INTERPRET_MASK))
      SysLog.println("Form.interpret: starting to interpret <form> \"" + name + "\"");
    try {
      interruptHandler.pushScope(this);
      modifyProperties();

      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.enter(form, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request entering <form> " + name);

       // Initialize the form variables, thereby initializing the form item guard variables as well
       // Get values from Goto.parameters() (only present in subdialogs) for those variables lacking initializations
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VARIABLES_MASK))
        SysLog.println("Form.interpret: parameters = " + ((whereToGo.parameters() != null) ? whereToGo.parameters().toString() : "null"));
      this.initializeVariables(whereToGo.parameters());   // Throws VXMLEvent
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VARIABLES_MASK))
        SysLog.println("Form.interpret: variables = " + scope.toString());
      this.executeScript();                               // Throws VXMLEvent

       // Reset the Form prompt and event counts
      prompts.resetPromptCount();
      handler.resetEventCounts();
       // Reset each FormItem's prompt and event counts without clearing the guard variable
      clearFormItems(false);

       // Do forever: choose a form item to visit
      while (true) {

        try {
          try {

            FormItem item = null;  // The form item we're looking for

             // If the last form item visit ended with a <goto nextitem>, choose that item next
            if (whereToGo != null && whereToGo.item() != null) {
              item = (FormItem)children.get(whereToGo.item());
              if (item == null && SysLog.printLevel > 0)
                SysLog.println("Form.interpret: ERROR - no child for gotoItem \"" + whereToGo.item() + "\", continuing");
              whereToGo = (Goto)whereToGo.clone();  // Clone so that clearing the item doesn't corrupt the Goto
              whereToGo.item(null);  // Clear the target item so we won't keep going back to it ad nauseum
            }

             // If there is no such item, choose the first form item whose guard variable is undefined
            if (item == null) {
              try {
                Enumeration e = children.elements();
                while (e.hasMoreElements()) {
                  Object element = e.nextElement();
                  if (element instanceof FormItem) {
                    item = (FormItem)element;
                    if (VXMLTypes.isUndefined(Scope.getProp(scope, item.name())) && item.condition())
                       // Throws InvalidExpresionException, UndeclaredVariableException, VXMLEvent
                      break;
                    else
                      item = null;
                  } else {
                    throw new VXMLEvent("error.semantic.badformitem", Integer.toString(((Cardinal)element).index()));
                  }
                }
              } catch (UndeclaredVariableException e) {
                if (SysLog.printLevel > 0) SysLog.println("Form.interpret: ERROR - " + e.toString());
                throw new VXMLEvent("error.semantic.undeclared", e);
              } catch (InvalidExpressionException e) {
                if (SysLog.printLevel > 0) SysLog.println("Form.interpret: ERROR - " + e.toString());
                throw new VXMLEvent("error.syntactic.badexpression", e);
              }
            }

             // Exit if no suitable form item found
            if (item == null)
              throw new VXMLExit("Form exiting without goto");

             // Execute the selected form item
            if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.FORM_MASK))
              SysLog.println("Form.interpret: calling interpret on the " + item.toString() + " named \"" + item.name() + "\"");
            Variables filled = item.interpret(whereToGo, interruptHandler);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
             // Execute the fulfilled <filled> actions
            executeFilleds(filled, whereToGo);                               // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn

          } catch (VXMLEvent event) {
            if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.CATCH_MASK))
              SysLog.println("Form.interpret: <form> \"" + name + "\" caught VXMLEvent \"" + event.name() + "\"");
            catchEvent(event, whereToGo);                                    // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
          } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Form.interpret: UNEXPECTED " + e.toString());
          }
        } catch (Goto gt) {
           // If it's not an item level Goto, just re-throw it
          if (!gt.isItemLevel()) throw gt;
          whereToGo = gt;
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK))
            SysLog.println("Form.interpret: Item level Goto caught for \"" + (gt.item() != null ? gt.item() : "<null>") + "\"");
        }
      }
    } finally {
      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.leave(form, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request leaving <form> " + name);
      restoreProperties();
      interruptHandler.popScope();
    }
  }
} // class Form
