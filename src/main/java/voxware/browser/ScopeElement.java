package voxware.browser;

import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.util.*;
import org.w3c.dom.Element;

import javax.speech.*;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.*;

 /** The Class that gives everything except Variables scope */
class ScopeElement extends Scope {

  private static final int PROPERTY_ALLOCATION = 5;
  private static final int PROPERTY_QUANTUM = 5;

  protected String            name;
  protected HashVector        children;     // The children, if the ScopeElement can have more than one
  protected Speaker           speaker;
  protected ObjectSet         grammars;
  protected Prompts           prompts;
  protected EventHandler      handler;
  protected Interrupts        interrupts;
  protected Loggers           loggers;
  protected Vector            properties;

  public ScopeElement() {
    super();
  }

   /** Get the name */
  public String name() {
    return this.name;
  }

   /** Make this ScopeElement a child of its parent ScopeElement, with adoption by the parent */
  public void makeChildOf(ScopeElement parent) throws VXMLEvent {
    makeChildOf(parent, true);
  }

   /** Make this ScopeElement a child of its parent ScopeElement, with optional adoption by the parent */
  public void makeChildOf(ScopeElement parent, boolean adopt) throws VXMLEvent {
    this.parent = parent;
     // Make a JavaScript scope object for this Scope if it doesn't already have one
    if (scope == null) {
       // Make the JavaScript scope object
      scope = (ScriptableObject)parent.newObject();
       // Make the scope object a property of itself
      Scope.setProp(scope, prefix, scope);
    }
     // Make the scope object of the parent the parent of the scope object, unless they are the same (as in some FieldItems)
    if (scope != parent.scope())
      Scope.setParent(scope, parent.scope());
     // Have the parent ScopeElement adopt it
    if (adopt)
      ((ScopeElement)parent).adoptChild(name, this);  // Throws VXMLEvent
  }

   /** Orphan this ScopeElement */
  public void orphan() {
    parent = null;
    scope = null;
  }

   /** Get the children */
  public HashVector children() {
    return this.children;
  }

   /** Have this ScopeElement adopt a child */
  public void adoptChild(String name, Cardinal child) throws VXMLEvent {
    if (name == null || children == null) {
      super.adoptChild(child);
    } else {
      Object old = children.put(name, child);
      if (old != null) throw new VXMLEvent("error.semantic.ambiguousname", name);
    }
  }

   /** Get the Vector of Grammars in this ScopeElement */
  public ObjectSet grammars() {
    return grammars;
  }

   /** Register a Grammar with this ScopeElement */
  public void putGrammar(SuperGrammar grammar) {
    grammars.add(grammar);
  }

   /** Get the Interrupts in this ScopeElement */
  public Interrupts interrupts() {
    return interrupts;
  }

   /** Register an Interrupt with this ScopeElement */
  public void putInterrupt(Interrupt interrupt) {
    if (interrupts == null) interrupts = new Interrupts();
    interrupts.insertEvent(interrupt);
  }

   /** Record an event as an interrupt if it is one -- call while interrupts are disabled */
  public void recordInterrupt(SystemEvent event) {
    if ((interrupts == null || !interrupts.recordEvent(event)) && parent != null)
      ((ScopeElement)parent).recordInterrupt(event);
  }

   /** Report or record an event as an interrupt if it is one -- call while interrupts are enabled */
  public Interrupt reportInterrupt(SystemEvent event, boolean onlyIfHard) {
    Interrupt interrupt = (interrupts != null) ? interrupts.reportEvent(event, onlyIfHard) : null;
    return (interrupt != null || parent == null) ? interrupt : ((ScopeElement)parent).reportInterrupt(event, onlyIfHard);
  }

  public Interrupt reportInterrupt(SystemEvent event) {
    return reportInterrupt(event, false);
  }

   /** Return the oldest pending interrupt -- call when enabling interrupts */
  public Interrupt oldestInterrupt(boolean onlyIfHard) {
    Interrupt interrupt = (interrupts != null) ? interrupts.oldestEvent(onlyIfHard) : null;
    return (interrupt != null || parent == null) ? interrupt : ((ScopeElement)parent).oldestInterrupt(onlyIfHard);
  }

  public Interrupt oldestInterrupt() {
    return oldestInterrupt(false);
  }

   /** Clear all pending interrupts in this Scope */
  public void clearInterrupts() {
    if (interrupts != null) interrupts.clear();
  }

   /** Interpret this ScopeElement */
  public Variables interpret(Goto context, InterruptHandler handler) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
    throw new RuntimeException("ScopeElement.interpret(): UNEXPECTED - called");
  }

   /** Get the EventHandler for this ScopeElement */
  public EventHandler handler() {
    return this.handler;
  }

   /** Register a Catcher in this ScopeElement */
  public void registerCatcher(Catcher catcher) {
    if (handler == null) handler = new EventHandler();
    handler.put(catcher);
  }

   /** Make Catchers for an event */
  public Catchers makeCatchers(String event) {
    if (handler == null) handler = new EventHandler();
    return handler.makeCatchers(event);
  }

   /** Get the appropriate Catcher for an event whose Catchers is known */
  private Catcher catcher(Catchers catchers, String event, ScopeElement scope) throws VXMLEvent {
    Catcher catcher = catchers.catcher(scope);   // This pre-increments the internal event count
    int eventCount = catchers.eventCount();  // This gets the pre-incremented internal event count
    Catcher parentCatcher = (parent != null) ? ((ScopeElement)parent).catcher(event, eventCount, scope) : null;
    if (catcher == null || parentCatcher != null && (catcher.count() < parentCatcher.count())) catcher = parentCatcher;
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.CATCH_MASK))
      SysLog.println("ScopeElement.catcher: catcher for event \"" + event + "\" in scope \"" + scope.getClass().getName() + "\" is " + catcher);
    return catcher;
  }

   /** Get the appropriate Catcher for an event, given the event count */
  private Catcher catcher(String event, int eventCount, ScopeElement scope) throws VXMLEvent {
    Catcher catcher = (handler != null) ? handler.catcher(event, eventCount, scope) : null;
    Catcher parentCatcher = (parent != null) ? ((ScopeElement)parent).catcher(event, eventCount, scope) : null;
    if (catcher == null || parentCatcher != null && (catcher.count() < parentCatcher.count())) catcher = parentCatcher;
    return catcher;
  }

   /** Catch an event and any events thrown during the catch */
  public Variables catchEvent(VXMLEvent event, Goto context) throws Goto, VXMLExit, VXMLReturn {
    int recursionCount = 0;
     // Process this event and any event thrown while processing it
    while (true) {
      try {
         // Create a shadow Scriptable named "event$" detailing the event:
         //   event$.name is the event's name, as specified in the throw;
         //   event$.description is the String resulting from calling toString() on the ancestral Java Exception, if any;
         //    if there is no such Java Exception, event$.description is set to the detail message of the event itself;
         //    if this is empty, it is set to VXMLTypes.PROTO_UNDEFINED
         //   event$stacktrace is the String that would result from calling printStackTrace() on the ancestral Exception,
         //    if any; if there is no such Exception, it is set to VXMLTypes.PROTO_UNDEFINED
        Object value = scope.get("event$", scope);
        Scriptable shadow;
        if (value != Scriptable.NOT_FOUND && value instanceof Scriptable) {
          shadow = (Scriptable)value;
        } else {
          shadow = newObject();
          scope.put("event$", scope, shadow);
        }
         // Put the event name into the shadow
        shadow.put("name", shadow, event.name());
         // If there is an ancestral Java Exception, put its description and stack trace into the shadow
        Object description;
        Object trace;
        Throwable progenitor = event.exception();
        if (progenitor != null) {
          description = progenitor.toString();
          CharArrayWriter traceArray = new CharArrayWriter();
          progenitor.printStackTrace(new PrintWriter(traceArray, true));
          trace = traceArray.toString();
       	} else {
           // Else, the stack trace is undefined
          trace = VXMLTypes.PROTO_UNDEFINED;
           // If there is a detail message, put it into the shadow
          description = event.getMessage();
           // Otherwise, the description is also undefined
          if (description == null) description = VXMLTypes.PROTO_UNDEFINED;
        }
        shadow.put("description", shadow, description);
        shadow.put("stacktrace", shadow, trace);
         // Make sure there is a Catchers (eventCount) in this scope for this event
        Catchers catchers = makeCatchers(event.name());
         // Find the appropriate Catcher in the scope chain
        String catchName = event.name();
        int end;
        Catcher catcher = catcher(catchers, catchName, this);
        while (catcher == null && (end = catchName.lastIndexOf('.')) > 0) {
          catchName = catchName.substring(0, end);
          catchers = handler.makeCatchers(catchName);
          catcher = catcher(catchers, catchName, this);
        }
        if (catcher == null) {
          catchers = handler.makeCatchers("");
          catcher = catcher(catchers, "", this);     // In case there's a catcher that catches anything that's thrown
        }
         // If a Catcher exists, update its siblings, then link it into the scope chain and interpret it
        if (catcher != null) {
          context.noPrompt(true);                    // A Catcher must explicitly request a reprompt
          context.lastScope(this.hashcode());
          handler.updateSiblings(catcher, catchers.eventCount());
          return catcher.interpret(this, context);   // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
        } else {
          throw new VXMLExit("Uncaught VXML event \"" + event.name() + "\"");
        }
       // Catch any VXMLEvent thrown in this catch
      } catch (VXMLEvent thrown) {
        if (SysLog.printLevel >= 1)
          SysLog.println("ScopeElement.catchEvent(): caught event \"" + thrown.name() + "\" thrown in catcher for event \"" + event.name() + "\"");
         // Limit recursive catches to some reasonable number so we can't get stuck here
        if (++recursionCount > ((Number)BrowserProperty.value(BrowserProperty.EVENTRECURSIONLIMIT_INDEX)).intValue())
          throw new VXMLExit("VXML event \"" + event.name() + "\" has exceeded the recursion limit");
        event = thrown;
      }
    }
  }

   /** Get the ScopeElement's prompts */
  public Prompts prompts() {
    return this.prompts;
  }

    /** Register a Prompt */
  public void registerPrompt(Prompt prompt) {
    if (prompts == null) prompts = new Prompts();
    prompts.insert(prompt);
  }

   /** Get the ScopeElement's speaker */
  public Speaker speaker() {
    return (speaker == null && parent != null) ? ((ScopeElement)parent).speaker() : speaker;
  }

   /** Set the ScopeElement's speaker */
  public void setSpeaker(Speaker speaker) {
    this.speaker = speaker;
  }

   /** Make a Filled object and register it in this ScopeElement */
  public Filled makeFilled(Element filled, Goto context, boolean preprocess) throws VXMLEvent {
    throw new RuntimeException("ScopeElement.makeFilled(): UNEXPECTED - called");
  }

   /** Get the ScopeElement's Loggers */
  public Loggers loggers() {
    return loggers;
  }

   /** Add a Logger to this ScopeElement */
  public void putLogger(Logger logger) {
    if (loggers == null) loggers = new Loggers();
    loggers.addLogger(logger);
  }

   /** Get a Logger to log the indexed event, or null if there is none */
  public Logger getLogger(int eventIndex) throws VXMLEvent {
    Logger logger = (loggers == null) ? null : loggers.getLogger(eventIndex, this);                        // Throws VXMLEvent
    return (logger != null || parent == null) ? logger : ((ScopeElement)parent).getLogger(eventIndex);     // Throws VXMLEvent
  }

   /** Say whether to log the indexed event */
  public boolean shouldLogEvent(int eventIndex) throws VXMLEvent {
    boolean shouldLog = (loggers == null) ? false : loggers.shouldLogEvent(eventIndex, this);              // Throws VXMLEvent
    return (shouldLog || parent == null) ? shouldLog : ((ScopeElement)parent).shouldLogEvent(eventIndex);  // Throws VXMLEvent
  }

   /** Say whether to log the named event */
  public boolean shouldLogEvent(String eventName) throws VXMLEvent {
    return shouldLogEvent(SystemEvent.index(eventName));                                                   // Throws VXMLEvent
  }

   /** Conditionally log the indexed event with a text string */
  public void logEvent(int eventIndex, String text) throws VXMLEvent {
    Logger logger = getLogger(eventIndex);                                                                 // Throws VXMLEvent
    if (logger != null) logger.logEvent(eventIndex, this, text);                                           // Throws VXMLEvent
  }

   /** Conditionally log the indexed event */
  public void logEvent(int eventIndex) throws VXMLEvent {
    Logger logger = getLogger(eventIndex);                                                                 // Throws VXMLEvent
    if (logger != null) logger.logEvent(eventIndex, this);                                                 // Throws VXMLEvent
  }

   /** Conditionally log the indexed event with an integer parameter */
  public void logEvent(int eventIndex, int param) throws VXMLEvent {
    Logger logger = getLogger(eventIndex);                                                                 // Throws VXMLEvent
    if (logger != null) logger.logEvent(eventIndex, this, param);                                          // Throws VXMLEvent
  }

   /** Register a local value for a property */
  public void insertProperty(Property property) throws VXMLEvent {
    if (properties == null) properties = new Vector(PROPERTY_ALLOCATION, PROPERTY_QUANTUM);
    properties.addElement(property);
  }

   /** Assign local values to any properties that have them */
  public void modifyProperties() throws VXMLEvent {
    if (properties != null) {
      Enumeration enumeration = properties.elements();
      try {
        while (enumeration.hasMoreElements())
          ((Property)enumeration.nextElement()).enter(this);
           // Throws PropertyVetoException, InvalidExpressionException, NumberFormatException, UndeclaredVariableException
      } catch (PropertyVetoException e) {
        throw new VXMLEvent("error.property.valueoutofrange", e);
      } catch (InvalidExpressionException e) {
        throw new VXMLEvent("error.syntactic.badexpression", e);
      } catch (NumberFormatException e) {
        throw new VXMLEvent("error.syntactic.badnumberformat", e);
      } catch (UndeclaredVariableException e) {
        throw new VXMLEvent("error.semantic.undeclared", e);
      }
    }
  }

   /** Reassign the values that the properties had in the parent scope */
  public void restoreProperties() {
    if (properties != null) {
      Enumeration enumeration = properties.elements();
      try {
        while (enumeration.hasMoreElements())
          ((Property)enumeration.nextElement()).leave();
      } catch (PropertyVetoException e) {
        throw new RuntimeException("ScopeElement.restoreProperties(): " + e.toString());
      }
    }
  }
} // class ScopeElement
