package voxware.browser;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import voxware.engine.audioplayer.*;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.hw.event.HwControls;
import voxware.util.*;

 /** An AudioPlayer */
class AudioPlayer {

   // Statics

  public  static final float AUDIO_GAIN = 16.5F;

   // Set this false in environments that don't support audio output
  private boolean enabled = true;


  private final AsyncAudioPlayer player;


 

  public  boolean getMute() {
    return player != null ? player.audioPlayer.getMute() : false;
  }

  public  void setMute(boolean mute) {
    if (player != null) player.audioPlayer.setMute(mute);
  }
  
  public AsyncAudioPlayer getPlayer() {
	  return player;
  }

  public  float getGain() {
    return player != null ? player.audioPlayer.getGain() : Float.NaN;
  }

  public  boolean setGain(float gain) {
    return player != null && player.audioPlayer.setGain(gain);
  }
 
   // Constructors

  // Return the AsyncAudioPlayer, first constructing it if necessary
  public AudioPlayer(AsyncAudioPlayer player) {
	  
	  this.player = player;
	  try {

		  if (!player.audioPlayer.setGain(AUDIO_GAIN))
			  HwControls.getInstance().setOutputVolume(AUDIO_GAIN);
		  if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
			  SysLog.println("AudioPlayer.makePlayer: instantiated the unique AsyncAudioPlayer");
	  } catch (Exception e) {
		  SysLog.println("AudioPlayer.player: ERROR - AsyncAudioPlayer NOT instantiated");
		  e.printStackTrace();
		  enabled = false;
	  }
  }
  
  
   // Public Methods

   /** Assemble an audio vector from the children of <prompt> or <audio> and submit it for playback */
  public void play(Scope scope, Element audio, AsyncListener listener) throws VXMLEvent {

    Node node = (Node)audio;
    String name = node.getNodeName();
    VoxwareVector playables = null;
    Logger logger = null;

    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
      SysLog.println("AudioPlayer.play: playing a <" + name + "> element");

    try {
      switch (Elements.index(name)) {
        case Elements.AUDIO_INDEX: {
            playables = parseAudioNode(scope, node);   // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
            break;
          }
        case Elements.PROMPT_INDEX: {
            playables = parseAudioData(scope, node);   // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
            if (playables.isEmpty()) playables.addElement(new VoxwareEmptyPlayable());
            break;
          }
        default: {
            if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
              SysLog.println("AudioPlayer.play: Unhandled node <" + node.getNodeName() + ">");
          }
      }
      logger = (scope != null) ? ((ScopeElement)scope).getLogger(SystemEvent.AUDIOOUTPUT_INDEX) : null;  // Throws VXMLEvent
    } catch (UndeclaredVariableException e) {
      throw new VXMLEvent("error.semantic.undeclared", e);
    } catch (InvalidExpressionException e) {
      throw new VXMLEvent("error.syntactic.badexpression", e);
    }

    play(scope, playables, logger, listener);
  }

   /** Assemble an audio vector from the children of <prompt> or <audio> and play it synchronously */
  public void play(Scope scope, Element audio) throws VXMLEvent {
    play(scope, audio, null);
  }

   /** Construct a playable TTS object from a String and play it synchronously */
  public void play(Scope scope, String text) throws VXMLEvent {
    if (text != null && text.length() > 0) {
      VoxwareVector playables = null;
      try {
        playables = new VoxwareVector();
      } catch (AudioAllocationException e) {
        throw new VXMLEvent("error.audio.unplayable", e);
      }
      playables.addElement(playableTTSObject(text));
      play(scope, playables, null, null);
    }
  }

   /** Assemble and return a Vector of playables from a waveform file, or null if this is not possible */
  public VoxwareVector playables(String waveformURLName) throws VXMLEvent {
    return playables(waveformURLName, CachingConverter.NULL_INDEX, DurationConverter.UNSPECIFIED);
  }

   /** Play a Vector of playables synchronously */
  public void play(Scope scope, Vector playables) throws VXMLEvent {
    VoxwareVector vv = null;
    if (playables instanceof VoxwareVector) {
      vv = (VoxwareVector)playables;
    } else {
      try {
        vv = new VoxwareVector(playables);
      } catch (AudioAllocationException e) {
        throw new VXMLEvent("error.audio.unplayable", e);
      }
    }
    play(scope, vv, null, null);
  }

   /** Cancel asynchronous playback */
  public void cancel() {
    if (player != null) player.cancel();
    cancelSimulation();
  }

   // Private Methods

   // Parse the pertinent info from an <audio> node in the prompt's DOM subtree.  The
   // strategy is to use a waveform, if available, backing off to the vector of playables
   // specified by the children if the waveform is unspecified or unavailable, per the
   // VoiceXML specification
  private VoxwareVector parseAudioNode(Scope scope, Node node) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {

    String  waveformURLName = null;                                   // The target URL (this or expr REQUIRED)
    Expr    expr = null;                                              // An expression that evaluates to a target URL string
    int     caching = CachingConverter.NULL_INDEX;
    int     fetchhint = FetchHintConverter.NULL_INDEX;                // IGNORED
    int     fetchtimeout = DurationConverter.UNSPECIFIED;             // Maximum time allowed to fetch the audio data

     // Get the attributes of the <audio> element
    NamedNodeMap nnm = node.getAttributes();
    if (nnm != null) {
      for (int j = 0; j < nnm.getLength(); j++) {
        Node attrNode = nnm.item(j);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("src"))
          waveformURLName = attrNode.getNodeValue();                  // Throws DOMException
        else if (nodeName.equalsIgnoreCase("expr"))
          expr = new Expr(attrNode.getNodeValue(), null);             // Throws DOMException and InvalidExpressionException 
        else if (nodeName.equalsIgnoreCase("caching"))
          caching = ((Integer)CachingConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
        else if (nodeName.equalsIgnoreCase("fetchhint"))
          fetchhint = ((Integer)FetchHintConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
        else if (nodeName.equalsIgnoreCase("fetchtimeout"))
          fetchtimeout = ((Integer)DurationConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
      }
    }

     // Try to assemble the playables from a waveform file
    VXMLEvent event = null;
    VoxwareVector playables = null;
     // If no source URL name is given, try to evaluate the expression attribute as a URL name
    if (waveformURLName == null && expr != null)
      waveformURLName = ScriptRuntime.toString(expr.evaluate(scope)); // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
    try {
      playables = playables(waveformURLName, caching, fetchtimeout);  // Throws VXMLEvent
    } catch (VXMLEvent e) {
      event = e;
    }
     // If no waveform is available, try to assemble the playables from the children
    if (playables == null) playables = parseAudioData(scope, node);          // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
     // If the children assemble to nothing, throw the event, if any, thrown when trying to assemble the playables from a waveform file
    if ((playables == null || playables.isEmpty()) && event != null) throw event;  // Throws VXMLEvent

    return playables;
  }

   // Try to assemble a VoxwareVector of playables from a waveform file
  private VoxwareVector playables(String waveformURLName, int caching, int fetchtimeout) throws VXMLEvent {
    VoxwareVector playables = null;
    if (waveformURLName != null && waveformURLName.length() > 0) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
        SysLog.println("AudioPlayer.playables: waveform URL  = \"" + waveformURLName + "\"");
      byte[] waveform = null;
      try {
        VXMLURLLoader url = new VXMLURLLoader(waveformURLName);       // Throws MalformedURLException
         // If the caching policy is "safe", or the URL is not in the cache, acquire the waveform anew
        Object cacheEntry = null;
        int cachingPolicy = (caching != CachingConverter.NULL_INDEX) ? caching : ((Integer)BrowserProperty.value(BrowserProperty.CACHING_INDEX)).intValue();
        if (cachingPolicy == CachingConverter.SAFE_INDEX || !((cacheEntry = DOMBrowser.cache.get(url)) instanceof byte[])) {
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK)) {
            if (cachingPolicy == CachingConverter.SAFE_INDEX) SysLog.println("AudioPlayer.playables: caching policy is \"safe\", getting from URL");
            else if (cacheEntry == null) SysLog.println("AudioPlayer.playables: no such cache entry, getting from URL");
            else SysLog.println("AudioPlayer.playables: cached object is not a byte[], getting from URL");
          }
           // Get the waveform from the URL
          waveform= "dummybytes".getBytes(); //= url.bytes(fetchtimeout);                         // Throws VXMLEvent
           // Cache the waveform
          DOMBrowser.cache.put(url, waveform, waveform.length);
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK)) {
            cacheEntry = DOMBrowser.cache.get(url);
            if (cacheEntry == null) SysLog.println("AudioPlayer.playables: CACHE FAILURE - null");
            else if (!cacheEntry.equals(waveform)) SysLog.println("AudioPlayer.playables:  CACHE FAILURE - unequal");
            else SysLog.println("AudioPlayer.playables: cache holds " + DOMBrowser.cache.size() + " entries in " + DOMBrowser.cache.totalBytes() + " bytes");
          }
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
            SysLog.println("AudioPlayer.playables: acquired waveform \"" + waveformURLName + "\" from URL");

         // Otherwise, acquire the waveform from the cache
        } else {
          waveform = (byte[])cacheEntry;
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
            SysLog.println("AudioPlayer.playables: acquired waveform \"" + waveformURLName + "\" from cache");
        }
      } catch (MalformedURLException e) {
        throw new VXMLEvent("error.badfetch.malformedurl", e);
      }
      if (waveform != null && waveform.length > 0) {
         // Should put the transcription into the text field in the VoxwareInputStream, but put the URL name there for now !!!
        try {
          playables = new VoxwareVector();
          playables.addElement(new VoxwareInputStream(new ByteArrayInputStream(waveform), waveformURLName));
        } catch (AudioAllocationException e) {
          throw new VXMLEvent("error.audio.unplayable", e);
        }
      } else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK)) {
        SysLog.println("AudioPlayer.playables: waveform unobtainable from URL \"" + waveformURLName + "\"");
      }
    } else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK)) {
      SysLog.println("AudioPlayer.playables: no src URL specified");
    }
    return playables;
  }

   // Parse the children of <audio> or <prompt>
  private VoxwareVector parseAudioData(Scope scope, Node node) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    
    StringBuffer  ttsText = new StringBuffer();
    NodeList      audioChildren = node.getChildNodes();
    VoxwareVector playables = null;

    try {
      playables = new VoxwareVector();
    } catch (AudioAllocationException e) {
      throw new VXMLEvent("error.audio.unplayable", e);
    }

    String nodeName;
    for (int i = 0; i < audioChildren.getLength(); i++) {
      Node childNode = audioChildren.item(i);
      nodeName = childNode.getNodeName();
      if (nodeName.equalsIgnoreCase("#text")) {
        ttsText.append(childNode.getNodeValue());
      } else if (nodeName.equalsIgnoreCase("value")) {
        Object value = ValueNode.parse(childNode, scope);       // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
        if (value instanceof VoxwareInputStream) {
           // Append a playable Object for any assembled TTS text
          appendVoxwareText(ttsText, playables);                // Throws VXMLEvent
           // Append the VoxwareInputStreams
          playables.addElement((VoxwareInputStream)value);
        } else if (value instanceof VoxwareVector) {
           // Append a playable Object for any assembled TTS text
          appendVoxwareText(ttsText, playables);                // Throws VXMLEvent
           // Append the VoxwareVector
          playables.addElement((VoxwareVector)value);
        } else if (value instanceof String) {
          if (((String)value).length() > 0) ttsText.append((String)value);
        } else {
          throw new RuntimeException("AudioPlayer.parseAudioData: UNEXPECTED return type " + value.getClass().getName() + " from ValueNode.parse()");
        }
      } else if (nodeName.equalsIgnoreCase("audio")) {
        VoxwareVector newPlayables = parseAudioNode(scope, childNode); // Throws VXMLEvent
        if (newPlayables != null) {
          if (!newPlayables.isEmpty()) {
             // Append a playable Object for any assembled TTS text
            appendVoxwareText(ttsText, playables);                // Throws VXMLEvent
             // Append the new playables from the embedded <audio>
            playables.addElement(newPlayables);
          } else {
            newPlayables.deallocate();
          }
        }
      }
    }
     // Append a playable Object for any assembled TTS text
    appendVoxwareText(ttsText, playables);                      // Throws VXMLEvent

    return playables;
  }

   // Append a playable Object for any assembled TTS text
  private void appendVoxwareText(StringBuffer buffer, VoxwareVector playables) throws VXMLEvent {
    if (buffer.length() > 0) {
      String text = buffer.toString().trim();                   // Strip off initial and final whitespace
      if (text.length() > 0)
        playables.addElement(playableTTSObject(text));          // Throws VXMLEvent
      buffer.setLength(0);
    }
  }

   // Convert a text string to a playable Object
  private Object playableTTSObject(String text) throws VXMLEvent {
    try {
      return new VoxwareText(text);
    } catch (AudioAllocationException e) {
      throw new VXMLEvent("error.audio.unplayable", e);
    }
  }

   // Play a VoxwareVector
  private void play(Scope scope, VoxwareVector playables, Logger logger, AsyncListener listener) throws VXMLEvent {

    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
      SysLog.println("AudioPlayer.play: playing a VoxwareVector of " + playables.size() + " element(s)");

    if (playables != null) {
      if (playables.size() > 0) {

         // Log what is being played, if requested
        if (logger != null) logger.logEvent(SystemEvent.AUDIOOUTPUT_INDEX, scope, playables.getText());

         // If there is a VoxwareAudioPlayer, submit the VoxwareVector to it
        if (player != null) {
           // If there is an AsyncListener, play asynchronously; otherwise, play synchronously
          if (listener != null) {
            player.play(playables, listener);
          } else {
            try {
              player.playSync(playables);
            } catch (InterruptedException e) {
              SysLog.println("AudioPlayer.play: synchronous playback interrupted");
            }
          }

         // Otherwise, print out the text, if any
        } else {
           // If there is an AsyncListener, simulate the playback timing
          if (listener != null) simulate(listener, playables.getText());
          else                  SysLog.println(playables.getText());
          playables.deallocate();
        }
      } else {
        playables.deallocate();
      }
    }
  }

   // AudioPlayerSimulator Field and Class to simulate audio playback timing

  private void simulate(AsyncListener listener, String text) {
    if (simulator == null) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
        SysLog.println("AudioPlayer.simulate: instantiated the unique AudioPlayerSimulator");
      simulator = new AudioPlayerSimulator();
      simulator.start();
    }
    simulator.play(listener, text);
  }

  private void cancelSimulation() {
    if (simulator != null) simulator.cancel();
  }

  private static AudioPlayerSimulator simulator = null;

  private class AudioPlayerSimulator extends Thread {
    private Vector queue;

    public AudioPlayerSimulator() {
      super("aSimulator");
      queue = new Vector();
    }

    private class ListenerQueueElement {
      public AsyncListener listener;
      public String        text;

      public ListenerQueueElement(AsyncListener listener, String text) {
        this.listener = listener;
        this.text = text;
      }
    }

    public synchronized void run() {
      Context context = Context.enter();
      try {
        while (true) {
          while (queue.isEmpty()) wait();
          while (!queue.isEmpty()) {
            ListenerQueueElement element = (ListenerQueueElement)queue.elementAt(0);
            if (element.text.length() > 0) {
              SysLog.println((String)(SysLog.printLevel > 0 ? "AudioPlayerSimulator.run: \"" : "  \"") + element.text + "\"");
              wait(element.text.length() * 100);         // Wait 100 milliseconds per character
            }
            if (!queue.isEmpty()) {
              queue.removeElementAt(0);
              element.listener.finished();
            }
          }
        }
      } catch (InterruptedException e) {
         // Fall through to terminate run()
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
          SysLog.println("AudioPlayerSimulator.run: terminating due to InterruptedException");
      } finally {
        Context.exit();
      }
    }

    public synchronized void play(AsyncListener listener, String text) {
      queue.addElement(new ListenerQueueElement(listener, text));
      if (queue.size() == 1) notify();
    }

    public synchronized void cancel() {
      queue.removeAllElements();
      notify();
    }
  }
} // class AudioPlayer
