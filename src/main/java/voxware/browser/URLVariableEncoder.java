package voxware.browser;

import java.lang.*;
import java.util.*;

import voxware.browser.*;
import voxware.util.SysLog;

public class URLVariableEncoder {
  private String    urlString;

  public URLVariableEncoder(String urlString) {
    this.urlString = urlString;
  }

   /* Return a Variables object containing the variables requested in the URL string,
      or null if none are requested */
  public Variables variables() {
    return this.variables(null);
  }

   /* Insert the variables requested in the URL string into an existing Variables object */
  public Variables variables(Variables variables) {
    if (urlString == null || urlString.length() == 0) {
      SysLog.println("URLVariableEncoder: WARNING - null/zero length URL string");
    } else if (!urlString.startsWith("file:") && urlString.indexOf('?') > 0) {

       /* Get everything up to the '?'; this should be the actual URL */
      String token;
      StringTokenizer st = new StringTokenizer(urlString);
      token = st.nextToken("?");

       /* Loop through the remaining string tokens, assuming the "&amp;" XML ampersand 
          separator is being used.  For each token, create a Variable and insert it in
          the Variables object.  Construct the Variables object only if none exists. */
      while (st.hasMoreTokens() && (token = st.nextToken("&\t\n\r\f")) != null) {
        if (variables == null) variables = new Variables();
        variables.put(token, Variable.PROTO_VARIABLE);
      }
    }
    return variables;
  }

  public String encode(Variables variables) {
    String fullURL = null;
    if (urlString == null || urlString.length() == 0) {
      SysLog.println("URLVariableEncoder: WARNING - null/zero length URL string");
    } else {

       /* Get everything up to the '?'; this should be the actual URL */
      fullURL = (new StringTokenizer(urlString)).nextToken("?");

      if (variables != null && !variables.isEmpty()) {
         /* Following an initial "?", append each variable's name and a string representing its
            value to the URL as a "name=value" substring, separating the substrings with "&" */
        fullURL += "?";
        Enumeration names = variables.keys();
        while (names.hasMoreElements()) {
          String   name = (String)names.nextElement();
          fullURL += name + "=" + variables.getByName(name).value().toString() + (names.hasMoreElements() ? "&" : "");
        }
      }
    }

    return fullURL;
  }
}
