package voxware.browser;

import java.util.*;

import org.w3c.dom.Node;

public class InvalidTag extends Exception {
  public Node node;

  public InvalidTag(Node node) {
    this.node = node;
  }
}
