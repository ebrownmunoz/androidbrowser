package voxware.browser;

import java.util.*;
import java.net.*;
import java.io.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

public class VXMLScript {
  private final int      initialSourceBufferSize = 120;
  private Element        node;
  private VXMLURLLoader  url;
  private Expr           expr;                                          // An expression that evaluates to a target URL string
  private String         charset;                                       // The character encoding of the script
  private int            caching = CachingConverter.NULL_INDEX;
  private int            fetchhint = FetchHintConverter.NULL_INDEX;
  private int            fetchtimeout = DurationConverter.UNSPECIFIED;  // Maximum time allowed to fetch the script
  private Expr           expression;

   /** Construct the JavaScript source for a <script> element */
  public VXMLScript(Element node) throws DOMException, InvalidExpressionException, MalformedURLException, VXMLEvent {

    this.node = node;

     // Get the attributes of the <script> element
    NamedNodeMap nnm = node.getAttributes();
    if (nnm != null) {
      for (int j = 0; j < nnm.getLength(); j++) {
        Node attrNode = nnm.item(j);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("src"))
          url = new VXMLURLLoader(attrNode.getNodeValue());         // Throws DOMException, MalformedURLException
        else if (nodeName.equalsIgnoreCase("expr"))
          expr = new Expr(attrNode.getNodeValue(), null);           // Throws DOMException and InvalidExpressionException 
        else if (nodeName.equalsIgnoreCase("charset"))
          charset = attrNode.getNodeValue();                        // Throws DOMException
        else if (nodeName.equalsIgnoreCase("caching"))
          caching = ((Integer)CachingConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
        else if (nodeName.equalsIgnoreCase("fetchhint"))
          fetchhint = ((Integer)FetchHintConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
        else if (nodeName.equalsIgnoreCase("fetchtimeout"))
          fetchtimeout = ((Integer)DurationConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
      }
    }

     // Do everything permissible at preprocessing time
    if (url != null) {
      if (prefetch()) {
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
          SysLog.println("VXMLScript: prefetching new JavaScript from \"" + url.getURL().toString() + "\"");
        expression(null);                                           // Throws InvalidExpressionException, VXMLEvent
      } else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK)) {
        int dflt = ((Integer)BrowserProperty.value(BrowserProperty.SCRIPTFETCHHINT_INDEX)).intValue();
        SysLog.println("VXMLScript: no prefetching - fetchhint = " + fetchhint + " and default = " + dflt);
      }
    } else if (expr == null) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
        SysLog.println("VXMLScript: neither src nor expr attribute specified; assembling source from children");
      expression = new Expr(assembleSource(), null);                // Throws InvalidExpressionException
    } else {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
        SysLog.println("VXMLScript: no prefetching - new VXMLScript has expr attribute but no src attribute");
    }
  }

   // Determine whether to prefetch
  private boolean prefetch() {
    return (fetchhint == FetchHintConverter.PREFETCH_INDEX ||
            fetchhint == FetchHintConverter.NULL_INDEX &&
            ((Integer)BrowserProperty.value(BrowserProperty.SCRIPTFETCHHINT_INDEX)).intValue() == FetchHintConverter.PREFETCH_INDEX);
  }

   // Assemble the script's source from the children
  private String assembleSource() {
    StringBuffer sourceBuffer = new StringBuffer(initialSourceBufferSize);
    NodeList     sourceChildren = this.node.getChildNodes();
    for (int i = 0; i < sourceChildren.getLength(); i++) {
      Node childNode = sourceChildren.item(i);
      String nodeName = childNode.getNodeName();
      if (nodeName.equalsIgnoreCase("#text") || nodeName.equalsIgnoreCase("#cdata-section"))
        sourceBuffer.append(childNode.getNodeValue());
    }

    return sourceBuffer.toString();
  }

   // Return the script's Expr
  public Expr expression(Scope scope) throws InvalidExpressionException, VXMLEvent {

     // If there is an expr and a scope, evaluate the expression in the scope and interpret the result as the source URL
    if (expr != null && scope != null) {
      try {
        Object value = expr.evaluate(scope);                        // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
        url = value instanceof URL ? new VXMLURLLoader((URL)value) : new VXMLURLLoader(ScriptRuntime.toString(value)); // Throws MalformedURLException
      } catch (UndeclaredVariableException e) {
        throw new VXMLEvent("error.semantic.undeclared", e);
      } catch (InvalidExpressionException e) {
        throw new VXMLEvent("error.syntactic.badexpression", e);
      } catch (MalformedURLException e) {
        throw new VXMLEvent("error.badfetch.malformedurl", e);
      }
    }

     // If the script was prefetched, and the URL was not computed on the fly,
     // and either the URL is null or the prefetch policy still stands, use the prefetched version
    if (expression != null && expr == null && (url == null || prefetch())) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
        SysLog.println("VXMLScript.expression: acquired prefetched script " + expression.script() + (String)((url != null) ? (" (from \"" + url.getFile() + "\")") : ""));

     // Otherwise, get the expression from the cache or from the URL
    } else {
       // If the caching policy is "safe", or the URL is not in the cache, acquire the URL anew
      Object cachedExpression = null;
      int cachingPolicy = (caching != CachingConverter.NULL_INDEX) ? caching : ((Integer)BrowserProperty.value(BrowserProperty.CACHING_INDEX)).intValue();
      if (cachingPolicy == CachingConverter.SAFE_INDEX || !((cachedExpression = DOMBrowser.cache.get(url)) instanceof Expr)) {
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK)) {
          if (cachingPolicy == CachingConverter.SAFE_INDEX) SysLog.println("VXMLScript.expression: caching policy is \"safe\", getting from URL");
          else if (cachedExpression == null) SysLog.println("VXMLScript.expression: no such cache entry, getting from URL");
          else SysLog.println("VXMLScript.expression: cached object is not an Expr, getting from URL");
        }
         // Try to get the source from the URL; failing that, try to assemble it from the children
        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
          SysLog.println("VXMLScript.expression: source URL = \"" + url.toString() + "\"");
        byte[] sourceBytes = null;
        sourceBytes = url.bytes(fetchtimeout);                      // Throws VXMLEvent
        if (sourceBytes != null && sourceBytes.length > 0) {
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK)) {
            String encoding = (charset == null) ? System.getProperty("file.encoding") : charset;
            SysLog.println("VXMLScript.expression: script encoding = \"" + encoding + "\"");
          }
          try {
            String source = (charset == null) ? new String(sourceBytes) : new String(sourceBytes, charset);  // Throws UnsupportedEncodingException
            expression = new Expr(source, null);                    // Throws InvalidExpressionException
          } catch (UnsupportedEncodingException e) {
            throw new VXMLEvent("error.badfetch.badencoding", e);
          }
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
            SysLog.println("VXMLScript.expression: acquired script \"" + url.getFile() + "\" from URL");
           // Cache the script
          DOMBrowser.cache.put(url, expression, sourceBytes.length);
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK)) {
            cachedExpression = DOMBrowser.cache.get(url);
            if (cachedExpression == null) SysLog.println("VXMLScript.expression: CACHE FAILURE - null");
            else if (!cachedExpression.equals(expression)) SysLog.println("VXMLScript.expression:  CACHE FAILURE - unequal");
            else SysLog.println("VXMLScript.expression: cache now holds " + DOMBrowser.cache.size() + " entries");
          }
        } else {
          expression = new Expr(assembleSource(), null);            // Throws InvalidExpressionException
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
            SysLog.println("VXMLScript.expression: source unobtainable from URL \"" + url.toString() + "\"; assembled from children");
        }

       // Otherwise, acquire the script from the cache
      } else {
        expression = (Expr)cachedExpression;
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
          SysLog.println("VXMLScript.expression: acquired script " + expression.script() + " (from \"" + url.getFile() + "\") from cache");
      }
    }

    return expression;
  }
}
