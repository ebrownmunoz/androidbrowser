package voxware.browser;

import voxware.util.SysLog;

public class SourcedInterrupt {

  public String   source;
  public void  setSource(String source)  { this.source = source; }

  public Object  content;
  public void setContent(Object Content) { this.content = content; }

  public SourcedInterrupt(String source, Object content) {
    this.source = source;
    this.content = content;
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.INTERRUPT_MASK))
      SysLog.println("SourcedInterrupt(" + source + ", " + ((content == null) ? "null" : content.toString()) + ")");
  }

  public boolean satisfies(Object template) {
    if (template instanceof SourcedInterrupt) {
      SourcedInterrupt message = (SourcedInterrupt) template;
      return (message.content == null || message.content.equals(content)) && (message.source == null || message.source.equals(source));
    }
    return false;
  }

  public String toString() {
    return content != null ? content.toString() : "null";
  }
}
