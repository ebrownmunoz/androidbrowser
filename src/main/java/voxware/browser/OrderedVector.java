package voxware.browser;

import java.util.Vector;

 /** A Vector of Counts maintained in Count order */
class OrderedVector extends Vector {
  static final int initialCapacity = 1;
  static final int capacityIncrement = 1;

  public OrderedVector() {
    super(initialCapacity, capacityIncrement);
  }

  public OrderedVector(int capacity) {
    super(capacity, capacityIncrement);
  }

   /** Maintain OrderedVector in Count order */
  public void insert(Count count) {
    int i;
    for (i = 0; i < size() && ((Count)elementAt(i)).count() <= count.count(); i++);
    if (i < size())
      insertElementAt(count, i);
    else
      addElement(count);
  }

   /** Return an OrderedVector of the greatest Count(s) <= count */
  public OrderedVector greatestLTE(int count) {
    int first;       // Index of the first element to extract
    int last;        // Index of the last element to extract, plus 1
    int actual = 0;  // The actual count value of the extracted elements
     /* Find the index (last) of the first element with count greater than the given count */
    for (first = last = 0; last < size() && ((Count)elementAt(last)).count() <= count; last++) {
      if (((Count)elementAt(last)).count() > actual) {
        actual = ((Count)elementAt(last)).count();
        first = last;
      }
    }
    OrderedVector result = new OrderedVector(last - first);
    for (int i = first; i < last; i++)
      result.addElement(this.elementAt(i));
    return result;
  }
} // class OrderedVector
