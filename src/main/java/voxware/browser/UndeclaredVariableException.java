package voxware.browser;

public class UndeclaredVariableException extends Exception {

  UndeclaredVariableException(String name) {
    super(name);
  }
}
