package voxware.browser;

import java.io.*;
import java.net.MalformedURLException;

import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.InputSource;
import voxware.xml.dom.TDOMParser;

import voxware.util.SysLog;

 /** A Wrapper for voxware.xml.dom.TDOMParser */
public class SimpleDOMParser implements DOMParserWrapper, ErrorHandler {

   // Parser
  voxware.xml.dom.TDOMParser parser = new voxware.xml.dom.TDOMParser();

   // Constructors

   /** The Default Constructor */
  public SimpleDOMParser() {
    parser.setErrorHandler(this);
  }

   // DOMParserWrapper Implementation

   /** Parse the specified URI and return the Document */
  public Document parse(String uri) throws VXMLEvent {
    return parse(new InputSource(uri));
  }

   /** Parse the specified InputSource and return the Document */
  public Document parse(InputSource source) throws VXMLEvent {
    try {
      parser.parse(source);  // Throws DOMException, IOException, MalformedURLException, SAXException
    } catch (FileNotFoundException e) {
      throw new VXMLEvent("error.badfetch.filenotfound", e);
    } catch (MalformedURLException e) {
      throw new VXMLEvent("error.badfetch.malformedurl", e);
    } catch (IOException e) {
      throw new VXMLEvent("error.badfetch.ioerror", e);
    } catch (SAXException e) {
      throw new VXMLEvent("error.xmlparser.sax", e);
    } catch (DOMException e) {
      throw new VXMLEvent("error.xmlparser.dom", e);
    }
    return parser.getDocument();
  }

   // ErrorHandler Implementation

   /** Warning */
  public void warning(SAXParseException e) {
    SysLog.println("[Warning] " + getLocationString(e) + ": " + e.getMessage());
  }

   /** Error */
  public void error(SAXParseException e) throws SAXException {
    String message = "[Error] " + getLocationString(e) + ": " + e.getMessage();
    SysLog.println(message);
    throw new SAXParseException(message, e.getPublicId(), e.getSystemId(), e.getLineNumber(), e.getColumnNumber());
  }

   /** Fatal error */
  public void fatalError(SAXParseException e) throws SAXException {
    String message = "[Fatal Error] " + getLocationString(e) + ": " + e.getMessage();
    SysLog.println(message);
    throw new SAXParseException(message, e.getPublicId(), e.getSystemId(), e.getLineNumber(), e.getColumnNumber());
  }

   // Private methods

   /** Return a string pinpointing the location of the error */
  private String getLocationString(SAXParseException e) {
    StringBuffer location = new StringBuffer();
    String systemId = e.getSystemId();
    if (systemId != null) {
      int index = systemId.lastIndexOf('/');
      if (index != -1) 
        systemId = systemId.substring(index + 1);
      location.append(systemId);
    }
    // location.append(':');
    // location.append(e.getLineNumber());
    // location.append(':');
    // location.append(e.getColumnNumber());
    return location.toString();
  }
} // class SimpleDOMParser
