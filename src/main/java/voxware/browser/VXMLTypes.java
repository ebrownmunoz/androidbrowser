package voxware.browser;

import org.mozilla.javascript.*;

import voxware.browser.*;

 /** The primitive VXML types */
public class VXMLTypes {

  public static final Scriptable PROTO_UNDEFINED = org.mozilla.javascript.Undefined.instance;
  public static final Class UNDEFINED = PROTO_UNDEFINED.getClass();
  public static final Object PROTO_DEFINED = Boolean.TRUE;  // Anything other than undefined will suffice

   // Type classification methods

   // Returns true iff its argument is a VXMLTypes.UNDEFINED
  public static boolean isUndefined(Object thing) {
    return (thing != null ? thing.getClass() == UNDEFINED : false);
  }

   // Some useful type conversion methods

   // Convert an int to a JavaScript Number
  public static Double intToNumber(int value) {
    return new Double((new Integer(value)).doubleValue());
  }

   // Convert a long to a JavaScript Number
  public static Double longToNumber(long value) {
    return new Double((new Long(value)).doubleValue());
  }

   // Convert an float to a JavaScript Number
  public static Double floatToNumber(float value) {
    return new Double((double)value);
  }

   // Convert a boolean to a JavaScript Boolean
  public static Boolean booleanToBoolean(boolean value) {
    return new Boolean(value);
  }
}
