package voxware.browser;

import org.w3c.dom.Element;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

 /** The Debugger Interface */
public interface Debugger {
  /**--------------------------------------------------------------------------------*
   * The browser calls this function before interpreting an Element in the DOM tree
   * @param element
   *   the org.w3c.dom.Element that the browser is about to enter
   * @param name
   *   the String name of the Element
   * @param fetchtimeout
   *   the current org.mozilla.javascript.Context 
   * @param fetchtimeout
   *   the org.mozilla.javascript.Scriptable that is the current browser scope
   * @return 
   *   <code>true</code> to enter the Element, <code>false</code> to exit the browser
   *--------------------------------------------------------------------------------**/
  public boolean enter(Element element, String name, Context context, Scriptable scope);

  /**--------------------------------------------------------------------------------*
   * The browser calls this function after interpreting an Element in the DOM tree
   * @param element
   *   the org.w3c.dom.Element that the browser is about to leave
   * @param name
   *   the String name of the Element
   * @param fetchtimeout
   *   the current org.mozilla.javascript.Context 
   * @param fetchtimeout
   *   the org.mozilla.javascript.Scriptable that is the current browser scope
   * @return 
   *   <code>true</code> to leave the Element, <code>false</code> to exit the browser
   *--------------------------------------------------------------------------------**/
  public boolean leave(Element element, String name, Context context, Scriptable scope);
}
