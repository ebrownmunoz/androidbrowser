package voxware.browser;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.zip.*;

import voxware.engine.audioplayer.*;

import org.mozilla.javascript.*;

import voxware.util.*;


 /** An AudioRenderer */
class AudioRenderer {

  public static final int MaxPlayables = 128;                  // This MUST be <= PLAYABLE_OBJTS (see VoxwareAudioPlayer.c)

  public static int WordPenalty        = 0;
  public static int SegmentPenalty     = 1;
  public static int WildcardPenalty    = 5;
  public static int EOUWildcardPenalty = 10;

   // Render the given object
  public static Object rendering(Object value, String recsrc, String sayas) throws VXMLEvent {
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
      SysLog.println("AudioRenderer.rendering: entered with value = " + value.toString() + ", recsrc = " + recsrc + ", sayas = " + sayas);
    AudioJar clips = null;
    try {
      VXMLURLLoader url = new VXMLURLLoader(recsrc);           // Throws MalformedURLException
       // If the default caching policy is anything other than 'safe', try to get the AudioJar for recsrc from the cache
      if (((Integer)BrowserProperty.value(BrowserProperty.CACHING_INDEX)).intValue() != CachingConverter.SAFE_INDEX)
        clips = (AudioJar)DOMBrowser.cache.get(url);
       // If it isn't there, or if the default caching policy is 'safe', create an AudioJar for recsrc and cache it
      if (clips == null) {
        byte[] byteArray; // = url.bytes();                        // Throws VXMLEvent
        byteArray = "dummybytes".getBytes();
        ZipInputStream zin = new ZipInputStream((new ByteArrayInputStream(byteArray)));
        clips = new AudioJar(zin);                             // Throws VXMLEvent
        DOMBrowser.cache.put(url, clips, byteArray.length);
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK))
          SysLog.println("AudioRenderer.rendering: AudioJar acquired from URL \"" + recsrc + "\"");
      } else if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.AUDIOPLAYER_MASK)) {
        SysLog.println("AudioRenderer.rendering: AudioJar for URL \"" + recsrc + "\" found in cache");
      }
    } catch (MalformedURLException exc) {
      throw new VXMLEvent("error.badfetch.malformedurl", exc);
    } catch (IOException exc) {
      throw new VXMLEvent("error.badfetch.ioerror", exc);
    }
     // Render the object using audio clips from the recsrc AudioJar
    return rendering(value, clips, sayas);                     // Throws VXMLEvent
  }

   // Render an object using audio clips from the given AudioJar, or, failing this, as a String
  private static Object rendering(Object value, AudioJar clips, String sayas) throws VXMLEvent {

    String valueString = ScriptRuntime.toString(value);
    Vector audioVector = null;

    sayasCases:
      if (sayas == null || (sayas = sayas.toLowerCase()).equals("literal")) {
        audioVector = parse(valueString, clips);
      } else if (sayas.equals("digits")) {
        StringBuffer sb = new StringBuffer(valueString.length());
         // Just separate the digits with spaces
        for (int i = 0; i < valueString.length(); i++) {
          if (Character.isDigit(valueString.charAt(i))) {
            sb.append(valueString.charAt(i));
            sb.append(' ');
          } else {
            break sayasCases;
          }
        }
        if (sb.length() > 0) {
          sb.setLength(sb.length() - 1);
          audioVector = parse(sb.toString(), clips);
        }
      } else if (sayas.equals("number")) {
        return valueString;                                    // Not yet supported
      } else if (sayas.equals("time")) {
        return valueString;                                    // Not yet supported
      } else if (sayas.equals("date")) {
        return valueString;                                    // Not yet supported
      } else if (sayas.equals("phone")) {
        return valueString;                                    // Not yet supported
      } else if (sayas.equals("currency")) {
        return valueString;                                    // Not yet supported
      } else {
        throw new VXMLEvent("error.rendering.badsayas");
      }

    if (audioVector != null && audioVector.size() <= MaxPlayables) {
      VoxwareVector playables = null;
      int index = 0;
      try {
        playables = new VoxwareVector(audioVector.size());
        while (index < audioVector.size()) {
          AudioSegment segment = (AudioSegment)audioVector.elementAt(index);
          playables.insertElementAt(new VoxwareInputStream(new ByteArrayInputStream(segment.audioData), segment.orthography()), index);
          index++;
        }
      } catch (AudioAllocationException e) {
        if (playables != null) playables.deallocate();
        return valueString;
      }
      return playables;
    } else {
      return valueString;
    }
  }
    
   // Parse the value String optimally into segments that correspond to AudioSegments in the AudioJar
  private static Vector parse(String value, AudioJar clips) {
    Vector parse = null;
    StringTokenizer st = new StringTokenizer(value);
    int numWords = st.countTokens();
    search:
      if (numWords > 0) {
         // Construct an array of Vectors of Segmentations indexed by word index
        Vector[] segmentations = new Vector[numWords + 1];
         // Construct an initial Segmentation
        AudioSegment segment = new AudioSegment(AudioJar.WildcardCouplerType, AudioJar.InitialCouplerType, null);
        segmentations[0] = new Vector(1);
        segmentations[0].addElement(new Segmentation(null, segment, 0));
        for (int wordIndex = 1; wordIndex <= numWords; wordIndex++) {
          String word = st.nextToken();
          segmentations[wordIndex] = extendSegmentations(segmentations[wordIndex - 1], word, clips);
          if (segmentations[wordIndex] == null || segmentations[wordIndex].isEmpty()) break search;
        }
        parse = bestSegmentation(segmentations[numWords], numWords, clips);
      }
    return parse;
  }

   // Extend the segmentations to include the given word
  private static Vector extendSegmentations(Vector previousSegmentations, String word, AudioJar clips) {
    Vector extendedSegmentations = null;
    if (previousSegmentations != null && previousSegmentations.size() > 0) {
      extendedSegmentations = new Vector(previousSegmentations.size() * 2);
      Segmentation previousSegmentation;
      Enumeration segmentations = previousSegmentations.elements();
      while (segmentations.hasMoreElements()) {
        previousSegmentation = (Segmentation)segmentations.nextElement();
        Enumeration subsequentWords = previousSegmentation.subsequentWords;
        if (subsequentWords != null && subsequentWords.hasMoreElements()) {
           // The previous segmentation ends in a segment containing a sequence of words and the sequence is not exhausted
          if (word.equals((String)subsequentWords.nextElement())) {
             // The next word in the sequence matches the given word, so add WordPenalty to the cost
            previousSegmentation.cost += WordPenalty;
            if (subsequentWords.hasMoreElements()) {
               // The sequence is not exhausted, so just copy the previous segmentation
              extendedSegmentations.addElement(previousSegmentation);
            } else {
               // The sequence is exhausted, so delete the subsequentWords Enumeration to mark it as such
              previousSegmentation.subsequentWords = null;
              int index = segmentationWithRightCoupler(extendedSegmentations, previousSegmentation.segment.rightCoupler);
              if (index >= 0) {
                 // There is already a current segmentation with the same right coupler
                Segmentation bestSegmentation = (Segmentation)extendedSegmentations.elementAt(index);
                if (previousSegmentation.cost < bestSegmentation.cost)
                   // This one is less costly, so replace the existing one with it
                  extendedSegmentations.setElementAt(previousSegmentation, index);
              } else {
                 // There is no segmentation with the same right coupler, so insert this one
                extendedSegmentations.insertElementAt(previousSegmentation, -index-1);
              }
            }
          }
        } else {
           // The previous segmentation can absorb no more words, so extend it
          Vector newSegments = clips.audioSegments(previousSegmentation.segment.rightCoupler, word);
          if (newSegments != null) {
            Enumeration extensions = newSegments.elements();
            while (extensions.hasMoreElements()) {
              AudioSegment newSegment = (AudioSegment)extensions.nextElement();
              int penalty = (newSegment.leftCoupler == AudioJar.WildcardCouplerType) ? WildcardPenalty : SegmentPenalty;
              int index = segmentationWithRightCoupler(extendedSegmentations, newSegment.rightCoupler);
              if (index >= 0) {
                 // There is already a current segmentation with the same right coupler
                Segmentation bestSegmentation = (Segmentation)extendedSegmentations.elementAt(index);
                if (previousSegmentation.cost + penalty < bestSegmentation.cost)
                   // This one is less costly, so replace the existing one with it
                  extendedSegmentations.setElementAt(new Segmentation(previousSegmentation, newSegment, previousSegmentation.cost + penalty), index);
              } else {
                 // There is no segmentation with the same right coupler, so insert this one
                extendedSegmentations.insertElementAt(new Segmentation(previousSegmentation, newSegment, previousSegmentation.cost + penalty), -index-1);
              }
            }
          }
        }
      }
    }
    return extendedSegmentations;
  }

   // Return the index of the unique Segmentation having the specified right coupler, or, if no such Segmentation exists,
   // the negative of 1 greater than the index of the position where such a Segmentation should be inserted. This is a
   // bizarre but efficient way to tell the caller whether to replace or insert a Segmentation, and where to do so. The
   // aim is to keep the Vector of segmentations sorted in ascending order by right coupler type.
  private static int segmentationWithRightCoupler(Vector segmentations, int rightCoupler) {
    int index = segmentations.size();
    while (index-- > 0) {
      Segmentation segmentation = (Segmentation)segmentations.elementAt(index);
      if (segmentation.subsequentWords == null) {
        if (rightCoupler >= segmentation.segment.rightCoupler) {
          if (rightCoupler > segmentation.segment.rightCoupler)
            index = -(index + 2);
          break;
        }
      }
    }
    return index;
  }

   // Return a Vector of AudioSegments embodying the least costly segmentation in the given Vector of segmentations
  private static Vector bestSegmentation(Vector segmentations, int numWords, AudioJar clips) {
    Vector traceback = null;
    if (segmentations != null && !segmentations.isEmpty()) {
      Enumeration hypotheses = segmentations.elements();
       // Find the least costly segmentation
      Segmentation segmentation;
      Segmentation bestSegmentation = null;
      while (hypotheses.hasMoreElements()) {
        segmentation = (Segmentation)hypotheses.nextElement();
        if (segmentation.segment.rightCoupler == AudioJar.WildcardCouplerType)
          segmentation.cost += EOUWildcardPenalty;
        else if (segmentation.segment.rightCoupler == AudioJar.TerminalCouplerType)
          segmentation.cost += SegmentPenalty;
        else continue;
        if (bestSegmentation == null) bestSegmentation = segmentation;
        else if (segmentation.cost < bestSegmentation.cost) bestSegmentation = segmentation;
      }
      traceback = new Vector(numWords);
      while (bestSegmentation != null && bestSegmentation.segment.audioData != null) {
        traceback.insertElementAt(bestSegmentation.segment, 0);
        bestSegmentation = bestSegmentation.previousSegments;
      }
      if (traceback.isEmpty()) traceback = null;
    }
    return traceback;
  }

  private static class Segmentation {
    public Segmentation  previousSegments;
    public AudioSegment  segment;
    public Enumeration   subsequentWords;
    public int           cost;

    public Segmentation(Segmentation segments, AudioSegment segment, int cost) {
      previousSegments = segments;
      this.segment = segment;
      subsequentWords = segment.subsequentWords();
      this.cost = cost;
    }
  }
}
