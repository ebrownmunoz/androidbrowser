package voxware.browser;

import java.io.*;

import org.w3c.dom.Element;
import org.mozilla.javascript.*;

 /** The Default Debugger Implementation */
public class DefaultDebugger implements voxware.browser.Debugger {

  private InputStreamReader reader;
  private boolean           steppingThrough;
  private boolean           steppingOver;
  private String            tagStart;
  
  public DefaultDebugger() {
    reader = new InputStreamReader(System.in);
    steppingThrough = true;
    steppingOver = false;
  }

   // Return true to continue, false to exit
  public boolean enter(Element element, String name, Context context, Scriptable scope) {
    if (!steppingThrough || steppingOver) return true;
    tagStart = tag(element, name);
    System.out.println("VDB> Entering " + tagStart);
    return processCommand(element, name, context, scope);
  }

   // Return true to continue, false to exit
  public boolean leave(Element element, String name, Context context, Scriptable scope) {
    if (!steppingThrough) return true;
    String tagEnd = tag(element, name);
    steppingOver = steppingOver && !tagEnd.equals(tagStart);
    if (steppingOver) return true;
    tagStart = "";
    System.out.println("VDB> Leaving " + tagEnd);
    return processCommand(element, name, context, scope);
  }

  private String tag(Element element, String name) {
    String tag = (element != null) ? element.getTagName() : "unknown";
    return ("<" + tag + "> " + (String)(name != null ? ("\"" + name +"\"") : ""));
  }

  private boolean processCommand(Element element, String name, Context context, Scriptable scope) {
    boolean proceed = true;
    try {
      while (true) {
        System.out.print("VDB> ");
        int character = reader.read();
        if (character != -1) {
          if ((char)character != '\n') System.out.println("");
          if ((char)character == '\n') {
             // Continue stepping through
            proceed = true;
            break;
          } else if ((char)character == 'a') {
             // Show everything
            System.out.println("  Element = " + (String)(element != null ? element.toString() : "null"));
            System.out.println("  Context = " + context.toString());
            System.out.println("    Scope = " + scope.toString());
          } else if ((char)character == 'c') {
             // Show the JS context
            System.out.println("  Context = " + context.toString());
          } else if ((char)character == 'e') {
             // Show the XML element
            System.out.println("  Element = " + (String)(element != null ? element.toString() : "null"));
          } else if ((char)character == 'h' || (char)character == '?') {
             // Help
            System.out.println("  <cr> -- continue stepping");
            System.out.println("     a -- show everything");
            System.out.println("     c -- show JavaScript context");
            System.out.println("     e -- show VXML element");
            System.out.println("     h -- show commands");
            System.out.println("     o -- step over the current tag");
            System.out.println("     q -- quit");
            System.out.println("     r -- run to completion");
            System.out.println("     s -- show JavaScript scope");
          } else if ((char)character == 'o') {
             // Step over
            steppingOver = true;
            proceed = true;
            break;
          } else if ((char)character == 'q') {
             // Abort
            steppingThrough = false;
            proceed = false;
            break;
          } else if ((char)character == 'r') {
             // Run to completion without tracing
            steppingThrough = false;
            proceed = true;
            break;
          } else if ((char)character == 's') {
             // Show the JS scope
            System.out.println("  Scope = " + scope.toString());
          }
        }
      }
    } catch (IOException e) {
      System.out.println("VDB Internal I/O Error - " + e.toString());
    }
    return proceed;
  }
}
