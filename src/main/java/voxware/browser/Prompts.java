package voxware.browser;

import java.util.Enumeration;
import org.w3c.dom.Element;

import voxware.browser.*;
import voxware.util.SysLog;

 /** All the Prompts for a Field */
class Prompts extends OrderedVector {

  private int  promptCount;
  private int  timeout = -1;

  public Prompts() {
    super();
    promptCount = 0;
  }

  public void resetPromptCount() {
    promptCount = 0;
  }

  public int timeout() {
    return timeout;
  }

  public Prompt getPrompt(Element element) {
    Enumeration prompts = this.elements();

    Prompt prompt = null;
    while (prompts.hasMoreElements()) {
      prompt = (Prompt)prompts.nextElement();
      if (element == prompt.element())
        return prompt;
    }

    return null;
  }
  
   /** Play the prompt(s) with the greatest count less than or equal to the promptCount */
  public void play(voxware.browser.Recognizer recognizer) throws VXMLEvent {
    Enumeration prompts = greatestLTE(++promptCount).elements();
    while (prompts.hasMoreElements())
      ((Prompt)prompts.nextElement()).play(recognizer); // Throws VXMLEvent
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VJAP_MASK))
      SysLog.println("Prompts.play: finished queueing prompts at time: " + System.currentTimeMillis());    
  }
} // class Prompts
