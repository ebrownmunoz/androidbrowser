package voxware.browser;

import org.w3c.dom.Element;

import voxware.browser.*;

 /** A SuperGrammar */
class SuperGrammar extends Cardinal {

  private   Element      element;
  protected String       name;      // The grammar's name (from the .rec file or the URL?)
  private   Link         link;      // The document or application-level link this grammar triggers (if any)

   /** Construct a SuperGrammar from a DOM element */
  public SuperGrammar(Element element) {
    this.element = element;
    link = null;                    // Always null for Field and Form grammars
  }

   /** Construct an empty SuperGrammar */
  public SuperGrammar() {
    link = null;                    // Always null for Field and Form grammars
  }

  public String name() {
    return name;
  }

  public void setLink(Link link) {
    this.link = link;
  }

  public Link link() {
    return link;
  }

   // Override Field modality?
  public boolean overrideModality() {
    return (link != null) && link.overrideModality();
  }
} // class SuperGrammar
