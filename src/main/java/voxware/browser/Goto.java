package voxware.browser;

import java.util.*;
import java.net.*;
import java.io.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.*;

public class Goto extends Exception implements Cloneable {

   // Statics

  public static final String DEFAULT_BOOT_URL = "file:BootURI.vxml";
  public static int   totalURLAcquisitions = 0;
  public static long  totalAcquisitionTime = 0;
  public static long  totalVXMLParsingTime = 0;

   // Fields

  protected String            next;        // The target URL string (null for an item-level goto)
  private   Expr              expr;        // An expression that evaluates to a target URL string (null for an item-level goto)
  private   Document          page;        // The target page (null for a form-level or item-level goto)
  private   Document          root;        // The target root page (null except to avoid re-parsing root for subdialogs)
  private   String            form;        // The name of the target form (null for an item-level goto)
  private   String            item;        // The name of the target form item (null if no item specified)
  private   Expr              expritem;    // An expression that evaluates to the name of the target form item
  private   boolean           noPrompt;    // Normally false; set true in a <catch> with no <reprompt>
  private   int               lastScope;   // The hashcode of the last scope in which a catch was made
  protected ScriptableObject  parameters;  // Parameters to pass to a target subdialog
  protected String            method;      // (Submit only) ("get" | "post" | "put")
  protected String            enctype;     // (Submit only) The MIME encoding type of the submitted document
  protected int               caching = CachingConverter.NULL_INDEX;
  protected int               fetchhint = FetchHintConverter.NULL_INDEX;
  protected int               fetchtimeout = DurationConverter.UNSPECIFIED;  // Maximum time allowed to fetch the next page
  protected String            fetchaudio;
  protected Variables         submit;      // (Submit only) (DEPRECATED in Goto)
  private   VXMLURLLoader     url;         // The target URL (to use as a template for Goto's in this context)
  protected boolean           isPageLevel; // True iff the root page does not change as a result of this goto (set by Session.preprocess)

   // Construct an empty Goto
  Goto() {}

   // Construct a Goto with no context from a next String
  public Goto(String next) {
    this.next = next;
  }

   // Construct a Goto with a context from a NamedNodeMap of Goto or Submit attributes
  public Goto(Element element, NamedNodeMap map, Goto context)
  throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException, VXMLEvent {

    for (int j = 0; j < map.getLength(); j++) {
      Node node = map.item(j);
      String nodeName = node.getNodeName();
      if (nodeName.equalsIgnoreCase("next")) {
        next = node.getNodeValue();                              // Throws DOMException
      } else if (nodeName.equalsIgnoreCase("expr")) {
        expr = new Expr(node.getNodeValue(), null);              // Throws DOMException, InvalidExpressionException 
      } else if (nodeName.equalsIgnoreCase("nextitem")) {
        item = node.getNodeValue();                              // Throws DOMException
      } else if (nodeName.equalsIgnoreCase("expritem")) {
        expritem = new Expr(node.getNodeValue(), null);          // Throws DOMException, InvalidExpressionException 
      } else if (nodeName.equalsIgnoreCase("caching")) {
        caching = ((Integer)CachingConverter.instance().convert(node.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
      } else if (nodeName.equalsIgnoreCase("fetchhint")) {
        fetchhint = ((Integer)FetchHintConverter.instance().convert(node.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
      } else if (nodeName.equalsIgnoreCase("fetchtimeout")) {
        fetchtimeout = ((Integer)DurationConverter.instance().convert(node.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
      } else if (nodeName.equalsIgnoreCase("fetchaudio")) {
        fetchaudio = node.getNodeValue();                        // Throws DOMException
      } else if (nodeName.equalsIgnoreCase("namelist") || nodeName.equalsIgnoreCase("submit")) {  // "submit" is DEPRECATED
        String submitString = node.getNodeValue();               // Throws DOMException
        if (submitString != null && submitString.length() > 0) {
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK))
            SysLog.println("Goto: submitString = \"" + submitString + "\"");
          submit = new Variables(submitString);
        }
      } else if (nodeName.equalsIgnoreCase("method")) {
        method = node.getNodeValue();                            // Throws DOMException
      } else if (nodeName.equalsIgnoreCase("enctype")) {
        enctype = node.getNodeValue();                           // Throws DOMException
      } else if (SysLog.printLevel > 0) {
        SysLog.println("Goto.Goto: WARNING - <goto> has illegal attribute \"" + nodeName + "\"");
      }
    }

    preprocess(element, context);                                // Throws InvalidTag, VXMLEvent
  }

   // Do everything permissible at preprocessing time
  protected void preprocess(Element element, Goto context) throws InvalidTag, VXMLEvent {
     // The URL of the next document is REQUIRED
    if (next != null) {
      if (prefetch()) {
        if (context != null) {
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK))
            SysLog.println("Goto:preprocess: prefetching \"" + next + "\"");
          acquireTarget(null, context);       // Throws VXMLEvent
        } else if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK)) {
          SysLog.println("Goto:preprocess: cannot prefetch \"" + next + "\" - no context");
        }
      } else if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK)) {
        int dflt = ((Integer)BrowserProperty.value(BrowserProperty.DOCUMENTFETCHHINT_INDEX)).intValue();
        SysLog.println("Goto:preprocess: not prefetching \"" + next + "\" - fetchhint = " + fetchhint + " and default = " + dflt);
      }
    } else {
      if (expr == null && item == null && expritem == null) throw new InvalidTag(element);  // Throws InvalidTag
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK))
        SysLog.println("Goto:preprocess: no prefetching - new Goto has no next attribute");
    }
  }

   // Determine whether to prefetch
  private boolean prefetch() {
     // Return true iff there is nothing to post and the prefetch policy is "prefetch"
    return (submit == null && next.indexOf('?') < 0 && (fetchhint == FetchHintConverter.PREFETCH_INDEX ||
            fetchhint == FetchHintConverter.NULL_INDEX &&
            ((Integer)BrowserProperty.value(BrowserProperty.DOCUMENTFETCHHINT_INDEX)).intValue() == FetchHintConverter.PREFETCH_INDEX));
  }

  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  public VXMLURLLoader url() {
    return url;
  }

  public String next() {
    return this.next;
  }

  public Document page() {
    return this.page;
  }

  public void root(Document root) {
    this.root = root;
  }
  public Document root() {
    return this.root;
  }

  public String form() {
    return this.form;
  }

  public void item(String item) {
    this.item = item;
  }
  public String item() {
    return this.item;
  }

  public void noPrompt(boolean noPrompt) {
    this.noPrompt = noPrompt;
  }
  public boolean noPrompt() {
    boolean result = this.noPrompt;
    this.noPrompt = false;
    return result;
  }

  public void lastScope(int scope) {
    this.lastScope = scope;
  }
  public int lastScope() {
    return this.lastScope;
  }

  public void parameters(ScriptableObject parameters) {
    this.parameters = parameters;
  }
  public ScriptableObject parameters() {
    return this.parameters;
  }

  public void submit(Variables submit) {
    this.submit = submit;
  }
  public Variables submit() {
    return this.submit;
  }

  public boolean isItemLevel() {
    return (next == null);  // If (item == null), then apply the normal rules of <form> interpretation
  }

  public boolean isFormLevel() {
    return (page == null);  // If (form == null), then go to the default (first) form
  }

  public boolean isPageLevel() {
    return isPageLevel;
  }

  public boolean isPageLevel(boolean isPageLevel) {
    return this.isPageLevel = isPageLevel;
  }

  public void goToTarget(ScopeElement scope) throws Goto, VXMLEvent {
    if (method == null || !method.equalsIgnoreCase("put")) {
      if (page != null) {
         // Search up the scope chain to find the Session
        while (scope != null && !(scope instanceof Session)) scope = (ScopeElement)scope.parent();
        if (scope != null) {
          if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK)) SysLog.println("Goto.goToTarget: preprocessing " + next);
          ((Session)scope).preprocess(this);  // Throws VXMLEvent
        } else {
          throw new RuntimeException("Goto.goToTarget: found no Session at head of Scope chain");
        }
      }
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK)) {
        SysLog.println("Goto.goToTarget: throwing Goto to target " + (String)((next != null) ? next : item));
      }
      throw this;
    }
  }

   // Acquires the target, getting and parsing the target Page if necessary
  public void acquireTarget(ScopeElement scope) throws VXMLEvent {
    acquireTarget(scope, this);  // Throws VXMLEvent
  }

   // Acquires the target in the given context, getting and parsing the target Page if necessary
  public void acquireTarget(ScopeElement scope, Goto context) throws VXMLEvent {
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK))
      SysLog.println("Goto.acquireTarget: entered with scope \"" + (String)((scope != null) ? scope.name() : "null") + "\"");
    try {
       // If there is a scope, evaluate the target expressions, which override supplied target values if present
      if (scope != null) {
        if (expr != null)
          next = ScriptRuntime.toString(expr.evaluate(scope));      // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
        if (expritem != null)
          item = ScriptRuntime.toString(expritem.evaluate(scope));  // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
      }
      if (next == null && item == null) throw new VXMLEvent("error.badfetch.notarget");

       // Parse the URL, if any
      if (next != null) {

         // If next is a pure anchor, treat it specially, since URL() won't treat it right
        if (next.indexOf('#') == 0) {
          form = next.substring(1);
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK))
            SysLog.println("Goto.acquireTarget: URL for <goto> is the pure reference \"#" + form + "\"");

         // If the document was prefetched, the URL was not computed on the fly, the prefetch policy still stands, 
         // and there is nothing to post, use the prefetched version
        } else if (page != null && expr == null && prefetch()) {
          if (SysLog.shouldLog(2, SysLog.GOTO_MASK))
            SysLog.println("Goto.acquireTarget: acquired prefetched document \"" + next + "\"");

         // Otherwise, get the page from the cache or from the URL
        } else {
           // If there is a context, get its URL to use as a template; otherwise, assume no template
          VXMLURLLoader nextURL = new VXMLURLLoader((context != null) ? context.url() : null, next); // Throws MalformedURLException
          form = nextURL.getRef();
          if (SysLog.shouldLog(2, SysLog.GOTO_MASK))
            SysLog.println("Goto.acquireTarget: acquiring target \"" + nextURL.toString() + "\"");

           // If the caching policy is "safe", or there is something to post, or the URL is not in the cache, acquire the URL anew
          int cachingPolicy = 
            (caching != CachingConverter.NULL_INDEX) ? caching : ((Integer)BrowserProperty.value(BrowserProperty.CACHING_INDEX)).intValue();
          if (cachingPolicy == CachingConverter.SAFE_INDEX || submit != null || next.indexOf('?') > 0 ||
              !((page = (Document)DOMBrowser.cache.get(nextURL)) instanceof Document)) {
            if (SysLog.shouldLog(3, SysLog.GOTO_MASK)) {
              if (cachingPolicy == CachingConverter.SAFE_INDEX) {
                SysLog.println("Goto.acquireTarget: caching policy is \"safe\", getting from URL");
              } else if (page == null) {
                SysLog.println("Goto.acquireTarget: no such cache entry, getting from URL");
              } else {
                SysLog.println("Goto.acquireTarget: cached object is not a Document, getting from URL");
              }
            }
            URLVariableEncoder encoder = new URLVariableEncoder(nextURL.toString());
             // Get the Variables requested in the URL and combine them with the submit Variables
            Variables variables = encoder.variables(submit);
             // Assign current values to them
            if (variables != null) {
              Enumeration names = variables.keys();
              while (names.hasMoreElements()) {
                String varName = (String)names.nextElement();
                 // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
                variables.getByName(varName).assign(scope.variable(varName));
              }
            }
             // Strip off any variables requested in the URL
            String pureURL = encoder.encode(null);
            if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK))
              SysLog.println("Goto.acquireTarget: Expanded URL for <goto> is \"" + pureURL + "\"");
             // Try to acquire and parse the new page
            totalURLAcquisitions++;
            long elapsed;
            long then = System.currentTimeMillis();
            VXMLURLLoader loader = new VXMLURLLoader(pureURL);                        // Throws MalformedURLException
            if (SysLog.shouldLog(3, SysLog.GOTO_MASK)) SysLog.println("Goto.acquireTarget: created VXMLURLLoader");
            loader.setCollectGarbage(true);
            if (SysLog.shouldLog(3, SysLog.GOTO_MASK)) SysLog.println("Goto.acquireTarget: collecting garbage during acquisition");
            byte[] byteArray = loader.bytes(method, variables, fetchtimeout);         // Throws VXMLEvent
            if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK)) {
                elapsed = System.currentTimeMillis() - then;
                totalAcquisitionTime += elapsed;
                if (SysLog.printLevel > 1) SysLog.println("Goto.acquireTarget: acquiring \"" + pureURL + "\" (" + byteArray.length + " bytes) took " + elapsed + " ms");
                System.err.println("EB: elapsed aquisition " + elapsed + ". url: " + pureURL);
            }
            

            if (method == null || !method.equalsIgnoreCase("put")) {
              InputSource source = new InputSource(new ByteArrayInputStream(byteArray));
              source.setEncoding(DOMBrowser.getBrowserEncoding());
              if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK)) {
                if (SysLog.printLevel > 2) SysLog.println("Goto.acquireTarget: starting XML parse with character encoding \"" + source.getEncoding() + "\"");
                then = System.currentTimeMillis();
              }
              if (SysLog.shouldLog(1, SysLog.GOTO_MASK)) SysLog.println("Goto.acquireTarget: parsing \"" + pureURL + "\"");
              page = DOMBrowser.parse(source);                                        // Throws VXMLEvent
              if (SysLog.shouldLog(1, SysLog.GOTO_MASK)) {
                elapsed = System.currentTimeMillis() - then;
                totalVXMLParsingTime += elapsed;
                SysLog.println("Goto.acquireTarget: parsing \"" + pureURL + "\" took " + elapsed + " ms");
                if (SysLog.shouldLog(3)) {
                  SysLog.println("Goto.acquireTarget: the document node is \"" + page.getNodeName() + "\"");
                  SysLog.println("Goto.acquireTarget: the local name is \"" + page.getLocalName() + "\"");
                  DocumentType docType = page.getDoctype();
                  SysLog.println("Goto.acquireTarget: the document type name is \"" + docType.getName() + "\"");
                  SysLog.println("Goto.acquireTarget: the public identifier is \"" + docType.getPublicId() + "\"");
                  SysLog.println("Goto.acquireTarget: the system identifier is \"" + docType.getSystemId() + "\"");
                }
              }
            } else {
              page = null;
            }
             // Cache the page
            DOMBrowser.cache.put(nextURL, page, byteArray.length);
            if (SysLog.shouldLog(2, SysLog.GOTO_MASK))
              SysLog.println("Goto.acquireTarget: acquired document \"" + nextURL.getFile() + "\" from URL");

           // Otherwise, acquire the URL from the cache
          } else {
            if (SysLog.shouldLog(2, SysLog.GOTO_MASK))
              SysLog.println("Goto.acquireTarget: acquired document \"" + nextURL.getFile() + "\" from cache");
          }

           // Sweet success! Update the current URL
          url = nextURL;
        }
      } else {
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK)) SysLog.println("Goto.acquireTarget: no next page");
      }
    } catch (UndeclaredVariableException e) {
      if (SysLog.printLevel > 0) SysLog.println("Goto.acquireTarget: ERROR - " + e.toString());
      throw new VXMLEvent("error.semantic.undeclared", e);
    } catch (InvalidExpressionException e) {
      if (SysLog.printLevel > 0) SysLog.println("Goto.acquireTarget: ERROR - " + e.toString());
      throw new VXMLEvent("error.syntactic.badexpression", e);
    } catch (MalformedURLException e) {
      throw new VXMLEvent("error.badfetch.malformedurl", e);
    } catch (Exception e) {
      if (SysLog.getPrintLevel() > 3) e.printStackTrace(SysLog.stream);
      if (e instanceof VXMLEvent) throw (VXMLEvent)e;
      else throw new VXMLEvent("error.badfetch", e);  // Throws VXMLEvent
    }
  }
}
