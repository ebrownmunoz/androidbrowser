package voxware.browser;

 /** The FieldItem Interface (Field, Record, Subdialog and VXMLObj implement this) */
public interface FieldItem {
   // Return the explicit name of the FieldItem, or null if none
  String explicitName();
}