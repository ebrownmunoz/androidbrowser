package voxware.browser;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.net.*;
import java.io.*;
import java.text.*;
import java.lang.reflect.*;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;
import voxware.util.Cache;

 /** An expression */
public class Expr {

  public static final int DEFAULT_MAX_CACHE_ENTRIES = 500;

   // LRU Cache of Scripts keyed by script String
  private static class ScriptCache extends ConcurrentHashMap {
    private int maxEntries;
    private ScriptCache(int maxCacheEntries) {
      super(maxCacheEntries, (float) 0.75);
      maxEntries = maxCacheEntries;
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
        SysLog.println("ScriptCache: constructed with a maximum capacity of " + maxEntries + " entries");      
    }
    protected boolean removeEldestEntry(Map.Entry eldest) {
      if (size() > maxEntries) {
        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK)) {
          if (SysLog.printLevel > 3) SysLog.println("ScriptCache: removeEldestEntry(" + eldest.toString() + ") removing eldest entry (size = " + size() + ")");
          else                       SysLog.println("ScriptCache: removeEldestEntry() removing eldest entry " + eldest.getValue() + " (size = " + size() + ")");
        }
        return true;
      }
      return false;
    }
  }
  /*
  private static class ScriptCache extends voxware.util.Cache {
    private ScriptCache(int maxCacheEntries) {
      super(maxCacheEntries);
    }
  }
  */
  private static ScriptCache scriptCache = null;            // Cache of Scripts keyed by script String
  public static void constructScriptCache(int maxCacheEntries) {
	  if (scriptCache == null) scriptCache = new ScriptCache(maxCacheEntries);
  }

  public static int totalScriptCount = 0;
  public static int totalHashEntries = 0;
  public static int totalHashGetTime = 0;
  public static int totalCompileTime = 0;
  public static int totalHashPutTime = 0;

  private static Hashtable nullScopes = new Hashtable(10);  // Hashtable of empty Scriptables keyed by Context

  private Object  value;
  private Script  script;       // An ECMA Script
  private String  name = null;

   /** Construct a literal Expr from one of the primitive ECMA Types */
  public Expr(Object value) {
    this.value = value;
    this.script = null;
  }

   /** Construct an Expr from a text string */
  public Expr(String text, Scope scope) throws InvalidExpressionException {
    value = null;
    long then = System.currentTimeMillis(); long now = then; totalScriptCount++;
     // Convert the text, if any, to an executable Script
    if (text != null && text.length() > 0) {
      if (scriptCache == null) scriptCache = new ScriptCache(DEFAULT_MAX_CACHE_ENTRIES);
      script = (Script)scriptCache.get(text);
      now = System.currentTimeMillis(); totalHashGetTime += (int)(now - then); then = now;
      if (script == null) {
        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK)) {
          if (SysLog.printLevel > 3) SysLog.println("Expr: " + this + " compiling script from text \"" + text + "\" ...");
          else                       SysLog.println("Expr: " + this + " compiling script from text ...");
        }
        try {
          Context context = Context.getCurrentContext();
          Scriptable compilationScope = (scope != null) ? scope.scope() : (Scriptable)nullScopes.get(context);
          if (compilationScope == null) {
            compilationScope = context.initStandardObjects(null);
            nullScopes.put(context, compilationScope);
            if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
              SysLog.println("Expr: " + this + " constructed surrogate compilation scope " + compilationScope.toString());
          }
          script = context.compileReader(compilationScope, new StringReader(text), "expr", 1, null);
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
            SysLog.println("Expr: " + this + " finished compilation of script " + script);
        } catch (IOException ioe) {
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK)) SysLog.println(" unexpected error: " + ioe.toString());
          throw new RuntimeException("UNEXPECTED: " + ioe.toString()); // Should never occur, as we just made the reader from a String
        } catch (EvaluatorException ee) {
           // Syntax errors come out here if compilation scope is null
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK)) SysLog.println(" syntax error: " + ee.toString());
          throw new InvalidExpressionException(ee.toString());
        } catch (EcmaError ee) {
           // Syntax errors come out here if compilation scope is not null
          if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK)) SysLog.println(" syntax error: " + ee.toString());
          throw new InvalidExpressionException(ee.toString());
        }
        now = System.currentTimeMillis(); totalCompileTime += (int)(now - then); then = now;
        scriptCache.put(text, script);
        now = System.currentTimeMillis(); totalHashPutTime += (int)(now - then); then = now; totalHashEntries++;
        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
          SysLog.println("Expr: " + this + " cached script " + totalHashEntries);
      } else if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK)) {
          if (SysLog.printLevel > 3) SysLog.println("Expr: " + this + " acquired script \"" + text + "\" from cache");
          else                       SysLog.println("Expr: " + this + " acquired script " + script + " from cache");
      }
    } else {
      script = null;
    }
  }

   /** Evaluate the Expr from its Script when the Script is known to contain no references, and return the value */
  public Object evaluate() throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    return this.evaluateInScriptable(null);
  }

   /** Evaluate the Expr from its Script in the scope of the specified Scriptable and return the value */
  public Object evaluateInScriptable(Scriptable scope) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    Context context = Context.getCurrentContext();
    if (script != null && context != null) {
      try {
         // If scope is null, use the Context's empty Scriptable, constructing it if necessary
        if (scope == null) {
          scope = (Scriptable)nullScopes.get(context);
          if (scope == null) scope = context.initStandardObjects(null);
          nullScopes.put(context, scope);
        }
        value = script.exec(context, scope);
      } catch (JavaScriptException e) {
        throw new VXMLEvent("error.javascript.uncaughtevent", e);
      } catch (EcmaError e) {
        String message = e.toString();
        if (message.indexOf("ReferenceError") >= 0)
          throw new UndeclaredVariableException(message);
        else
          throw new InvalidExpressionException(message);
      }
    }
    return value;
  }

   /** Evaluate the Expr in the specified Scope from its Script and return the value */
  public Object evaluate(Scope scope) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    return evaluateInScriptable((scope != null) ? scope.scope() : null);
  }

   /** Return the Class of the Expr */
  public Class type() {
    return value != null ? value.getClass() : null;
  }

   /** Name the Expr */
  public void name(String name) {
    this.name = name;
  }

   /** Return the name of the Expr */
  public String name() {
    return this.name;
  }

   /** Return the contained script */
  public Script script() {
    return this.script;
  }

  protected void finalize() throws Throwable {
    try {
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.SCRIPT_MASK))
        SysLog.println("Expr: finalizing " + this + " containing script " + script);
    } finally {
      super.finalize();
    }
  }
}
