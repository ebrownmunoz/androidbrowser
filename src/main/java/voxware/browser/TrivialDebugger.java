package voxware.browser;

import org.w3c.dom.Element;
import org.mozilla.javascript.*;

import voxware.util.SysLog;

 /** The Trivial Debugger Implementation */
public class TrivialDebugger implements voxware.browser.Debugger {

  public TrivialDebugger() {}

   // Log the entry and return true to continue
  public boolean enter(Element element, String name, Context context, Scriptable scope) {
    if (SysLog.printLevel > 0) SysLog.println("ENTERING " + tag(element, name));
    return true;
  }

   // Log the exit and return true to continue
  public boolean leave(Element element, String name, Context context, Scriptable scope) {
    if (SysLog.printLevel > 0) SysLog.println("LEAVING " + tag(element, name));
    return true;
  }

  private String tag(Element element, String name) {
    String tag = (element != null) ? element.getTagName() : "unknown";
    return ("<" + tag + "> " + (String)(name != null ? ("\"" + name +"\"") : ""));
  }
}
