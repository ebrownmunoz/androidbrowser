package voxware.browser;

import java.net.MalformedURLException;

import org.mozilla.javascript.Context;

import voxware.util.SysLog;

public class SessionThread implements Runnable {

	private final String startingURL;
	private final String defaultSpeakerURL;
	private final int optimizationLevel;
	private final boolean jsDebug;
	private final boolean jsNoSource;
	private Recognizer recognizer;
	private AudioPlayer audioPlayer;
	private InterruptHandler interruptHandler;
	private SystemVariables systemVariables;
	private boolean preprocess;
	private boolean canonical;
	
	public SessionThread(String startingURL, String defaultSpeakerURL, 
			int optimizationLevel, boolean jsDebug, boolean jsNoSource, Recognizer recognizer,
			AudioPlayer audioPlayer, InterruptHandler interruptHandler, 
			SystemVariables systemVariables, boolean preprocess, boolean canonical) {
		this.startingURL = startingURL;
		this.defaultSpeakerURL = defaultSpeakerURL;
		this.optimizationLevel = optimizationLevel;
		this.jsDebug = jsDebug;
		this.jsNoSource = jsNoSource;
		this.recognizer = recognizer;
		this.audioPlayer = audioPlayer;
		this.interruptHandler = interruptHandler;
		this.systemVariables = systemVariables;
		this.preprocess = preprocess;
		this.canonical = canonical;
	}

	@Override
	public void run() {
		// Set up the JavaScript environment
		Context context = Context.enter();
		
		// Set version to JavaScript1.2 so that we get object-literal style printing instead of "[object Object]"
		context.setLanguageVersion(Context.VERSION_1_2);
		context.setOptimizationLevel(optimizationLevel); // pcfg.parameter("JSOptimization").intValue());
		// Remember that JavaScript debugging suppresses optimization
		context.setGeneratingDebug(jsDebug); //pcfg.parameter("JSDebug").booleanValue());
		context.setGeneratingSource(!jsNoSource); // !pcfg.parameter("JSNoSource").booleanValue());
		SysLog.println("DOMBrowser.main: The JavaScript language version is " + context. getLanguageVersion());
		SysLog.println("DOMBrowser.main: The JavaScript optimization level is " + context.getOptimizationLevel());
		SysLog.println("DOMBrowser.main: JavaScript debugging is " + (String)(context.isGeneratingDebug() ? "ON" : "OFF"));
		SysLog.println("DOMBrowser.main: The JavaScript code generator " + (String)(context.isGeneratingSource() ? "is" : "is NOT") + " generating source information");

		try {
			Session session = new Session(recognizer, audioPlayer, interruptHandler, systemVariables, preprocess);
			
			if (SysLog.printLevel > 1) SysLog.println("DOMBrowser.main: Constructed the top-level session");
			session.setTrace(canonical);
			
			session.run(startingURL, Speaker.unknown(session, defaultSpeakerURL));
		} catch (VXMLEvent e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidTag e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			Context.exit();
		}
	}

}
