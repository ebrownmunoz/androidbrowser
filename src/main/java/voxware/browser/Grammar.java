package voxware.browser;

import java.net.URL;
import java.net.MalformedURLException;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.StringBufferInputStream;  // DEPRECATED, but unavoidable until the JSGF compiler can deal with Readers

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.*;

 /** A Speech Recognition Grammar */
public class Grammar extends SuperGrammar {

   // MIME types and file extensions of standard grammar formats
  public static final int    NoType = -1;

  public static final int    VISEType = 0;
  public static final String VISEMIMEType = "x-vise";                     // Voxware proprietary recognition file type
  public static final String VISEExtension = "vgrm";                      //   Source
  public static final int    VISECType = VISEType + 1;
  public static final String VISECExtension = "rec";                      //   Compiled image

  public static final int    JSGFType = VISECType + 1;
  public static final String JSGFMIMEType = "x-jsgf";                     // Java Standard Grammar Format
  public static final String JSGFExtension = "gram";                      //   Source
  public static final int    JSGFCType = JSGFType + 1;
  public static final String JSGFCExtension = "cgram";                    //   Compiled image (proprietary)

  public static final int    XMLGType = JSGFCType + 1;
  public static final String XMLGMIMEType = "grammar+xml";                // W3C XML Speech Recognition Grammar Format
  public static final String XMLGExtension = "grxml";                     //   Source
  public static final int    XMLGCType = XMLGType + 1;
  public static final String XMLGCExtension = "cgrmxl";                   //   Compiled image (proprietary)

  public static final int    ABNFType = XMLGCType + 1;
  public static final String ABNFMIMEType = "grammar";                    // W3C Augmented BNF Speech Recognition Grammar Format
  public static final String ABNFExtension = "grm";                       //   Source
  public static final int    ABNFCType = ABNFType + 1;
  public static final String ABNFCExtension = "cgrm";                     //   Compiled image (proprietary)

  public static final int    NumTypes = ABNFCType + 1;

  public static final int    DefaultType = VISECType;

  static String[] mimeTypes =  { "",            VISEMIMEType,   JSGFMIMEType,  "",             XMLGMIMEType,  "",             ABNFMIMEType,  "" };
  static String[] extensions = { VISEExtension, VISECExtension, JSGFExtension, JSGFCExtension, XMLGExtension, XMLGCExtension, ABNFExtension, ABNFCExtension };

  public static int typeFromMIMEType(String mimeType) {
    if (mimeType == null) return NoType;
    int i = NumTypes;
    while (i-- > 0) if (mimeType.equalsIgnoreCase(mimeTypes[i])) break;
    return i;
  }

  public static int typeFromExtension(String extension) {
    if (extension == null) return NoType;
    int i = NumTypes;
    while (i-- > 0) if (extension.equalsIgnoreCase(extensions[i])) break;
    return i;
  }

  public static String mimeType(int type) {
    if (NoType < type && type < NumTypes) return mimeTypes[type];
    else                                  return null;
  }

  public static String extension(int type) {
    if (NoType < type && type < NumTypes) return extensions[type];
    else                                  return null;
  }

  private static int           instanceCount = 0;
  private static VXMLURLLoader previousURL = null;

  private Element        grammar;
  private VXMLURLLoader  url;                                             // The target URL (this or expr REQUIRED)
  private Expr           expr;                                            // An expression that evaluates to a target URL string
  private int            type = NoType;                                   // How the grammar is encoded (MIME type)
  private boolean        voice = true;                                    // Voice (true) or DTMF (false) mode
  private int            caching = CachingConverter.NULL_INDEX;
  private int            fetchhint = FetchHintConverter.NULL_INDEX;
  private int            fetchtimeout = DurationConverter.UNSPECIFIED;    // Maximum time allowed to fetch the grammar
  private String         rule;                                            // The name of the rule to activate
  private boolean        localscope;                                      // Whether the scope is "local" or not
  private Wrapper        prefetchedGrammar;                               // The grammar image if prefetched; null otherwise

   /** Construct a Grammar from a URL name */
  public Grammar(String urlString) throws MalformedURLException, VXMLEvent {
    this(urlString, CachingConverter.FAST_INDEX, FetchHintConverter.PREFETCH_INDEX);  // Throws MalformedURLException
  }

   /** Construct a Grammar from a URL name */
  public Grammar(String urlString, int caching, int fetchhint) throws MalformedURLException, VXMLEvent {
    super();
    this.caching = caching;
    this.fetchhint = fetchhint;
    url = new VXMLURLLoader(previousURL, urlString);                      // Throws MalformedURLException
  }

   /** Construct a Grammar from a <grammar> element in the DOM tree */
  Grammar(Element grammar) throws DOMException, InvalidExpressionException, MalformedURLException, VXMLEvent {
    super(grammar);
    this.setIndex(Elements.GRAMMAR_INDEX);

     // Get the attributes of the <grammar> element
    NamedNodeMap nnm = ((Node)grammar).getAttributes();
    if (nnm != null) {
      for (int j = 0; j < nnm.getLength(); j++) {
        Node attrNode = nnm.item(j);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("src"))
          url = new VXMLURLLoader(previousURL, attrNode.getNodeValue());  // Throws DOMException and MalformedURLException
        else if (nodeName.equalsIgnoreCase("expr"))
          expr = new Expr(attrNode.getNodeValue(), null);                 // Throws DOMException and InvalidExpressionException 
        else if (nodeName.equalsIgnoreCase("type"))
          type = typeFromMIMEType(attrNode.getNodeValue());               // Throws DOMException
        else if (nodeName.equalsIgnoreCase("mode"))
          voice = attrNode.getNodeValue().equalsIgnoreCase("voice");      // Throws DOMException
        else if (nodeName.equalsIgnoreCase("caching"))
          caching = ((Integer)CachingConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
        else if (nodeName.equalsIgnoreCase("fetchhint"))
          fetchhint = ((Integer)FetchHintConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
        else if (nodeName.equalsIgnoreCase("fetchtimeout"))
          fetchtimeout = ((Integer)DurationConverter.instance().convert(attrNode.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
        else if (nodeName.equalsIgnoreCase("scope"))
          localscope = !attrNode.getNodeValue().equalsIgnoreCase("document");  // And put the grammar in the Page Grammars, too. !!!
      }
    }

     // Supply an internal name if none has been given
    if (name == null)
      name = "_GRAMMAR" + Integer.toString(++instanceCount);

     // Do everything permissible at preprocessing time
    if (url != null) {
      if (prefetch()) {
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
          SysLog.println("Grammar.preprocess: prefetching new Grammar.Wrapper from \"" + url.getURL().toString() + "\"");
        prefetchedGrammar = wrapper(null);                                // Throws VXMLEvent
      } else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK)) {
        int dflt = ((Integer)BrowserProperty.value(BrowserProperty.GRAMMARFETCHHINT_INDEX)).intValue();
        SysLog.println("Grammar: no prefetching - fetchhint = " + fetchhint + " and default = " + dflt);
      }
    } else if (expr == null) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
        SysLog.println("Grammar: neither src nor expr attribute specified; assembling grammar of type \"" + type + "\" from children");
      if (type == JSGFType) {
         // JSGF grammars are NOT precompiled, because it is faster to compile them than to de-serialize their precompiled images
        prefetchedGrammar = new Wrapper(name, JSGFType, new StringBufferInputStream(source(grammar)));
      } else if (type == XMLGType || type == ABNFType) {
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
          SysLog.println("Grammar: grammar type \"" + type + "\" not supported");
        throw new VXMLEvent("error.badgrammar", "Grammar type \"" + type + "\" not supported");
      } else {
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
          SysLog.println("Grammar: ERROR - cannot assemble grammar of unknown type \"" + type + "\" from children");
        throw new VXMLEvent("error.badgrammar", "Grammar type \"" + type + "\" not known");
      }
    } else {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
        SysLog.println("Grammar: no prefetching - new Grammar has expr attribute but no src attribute");
    }
  }

   // Determine whether to prefetch
  private boolean prefetch() {
    return (fetchhint == FetchHintConverter.PREFETCH_INDEX ||
            fetchhint == FetchHintConverter.NULL_INDEX &&
            ((Integer)BrowserProperty.value(BrowserProperty.GRAMMARFETCHHINT_INDEX)).intValue() == FetchHintConverter.PREFETCH_INDEX);
  }

   // Serialize something and return the serialization in a byte[]
  private byte[] image(Object something) throws VXMLEvent {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try {
      ObjectOutputStream out = new ObjectOutputStream(baos);
      out.writeObject(something);
      out.flush();
      out.close();
    } catch (IOException e) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
        SysLog.println("Grammar.image: exception serializing -- " + e.toString());
      throw new VXMLEvent("error.grammar.badimage", e);
    }
    return baos.toByteArray();
  }

   // Assemble the grammar's source from its children
  private String source(Element node) {
    StringBuffer  sourceBuffer = new StringBuffer();
    NodeList      sourceChildren = node.getChildNodes();
    for (int i = 0; i < sourceChildren.getLength(); i++) {
      Node childNode = sourceChildren.item(i);
      String nodeName = childNode.getNodeName();
      if (nodeName.equalsIgnoreCase("#text") || nodeName.equalsIgnoreCase("#cdata-section"))
        sourceBuffer.append(childNode.getNodeValue());
    }
    return sourceBuffer.toString();
  }

  public String file() {
    return (url != null) ? url.getFile() : "unknown";
  }

  boolean modeIsVoice() {
    return voice;
  }

  boolean modeIsDTMF() {
    return !voice;
  }

   // Set the rule name
  void rule(String rule) {
    this.rule = rule;
  }

   // Return the rule name
  public String rule() {
    return rule;
  }

   // A static nested class to wrap the various flavors of grammars for caching and retrieval
  public static class Wrapper {

    // Wrappers do not refer to Grammars, because there is a one-to-many relation between them
    // Moreover, we don't want Grammars to hang around across document loads

    private String  name;
    private int     type = Grammar.NoType;
    private Object  content;
    private int     size;

    protected Wrapper( String name, int type, Object content) {
      this.name = name;
      this.type = type;
      this.content = content;
      this.size = (content instanceof byte[]) ? ((byte[])content).length : Cache.DEFAULT_SIZE_GUESS;
    }

    public String  name()                 { return name; }
    int     type()                 { return type; }
    public Object  content()              { return content; }
    int     size()                 { return size; }
    void    name(String name)      { this.name = name; }

     // Override equals(Object) method in java.lang.Object
    public boolean equals(Object wrapper) {
      return wrapper instanceof Grammar.Wrapper && this.content == ((Grammar.Wrapper)wrapper).content;
    }
  }

   /** Acquire the wrapped grammar */
    Wrapper wrapper = null;
    public Wrapper wrapper(Scope scope) throws VXMLEvent {
    int extension = NoType;

     // If there is an expr and a scope, evaluate the expression in the scope and interpret the result as the target URL
    if (expr != null && scope != null) {
      try {
        Object value = expr.evaluate(scope);                              // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
        url = value instanceof URL ? new VXMLURLLoader((URL)value) : new VXMLURLLoader(ScriptRuntime.toString(value)); // Throws MalformedURLException
      } catch (UndeclaredVariableException e) {
        throw new VXMLEvent("error.semantic.undeclared", e);
      } catch (InvalidExpressionException e) {
        throw new VXMLEvent("error.syntactic.badexpression", e);
      } catch (MalformedURLException e) {
        throw new VXMLEvent("error.badfetch.malformedurl", e);
      }
    }
     // If there is now a URL, extract the grammar name and rule name from it
    if (url != null) {
       // Extract the grammar's name from the URL
      String fileName = url.getFile();
      int    slash = fileName.lastIndexOf('/') + 1;
      int    dot = fileName.indexOf('.', slash);
      if (dot < 0) {
        name = fileName.substring(slash);         // The grammar name is everything following the last slash, if any
      } else {
        name = fileName.substring(slash, dot);    // The grammar name is everything between the last slash, if any, and the first dot following it
        extension = typeFromExtension(fileName.substring(dot + 1));
      }
       // Extract the rule's name (encoded as an "anchor" or "reference") from the URL
      rule = url.getRef();
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
        SysLog.println("Grammar.wrapper: name = \"" + name + "\", type = \"" + extension(extension) + "\", " +
                                               "rule = \"" + (String)(rule == null ? "default start rule" : rule) + "\"");
    } else if (prefetchedGrammar == null) {
       // There is no URL and no prefetched wrapper
      throw new VXMLEvent("error.badfetch.nourl");
    }

     // If the grammar was prefetched, and the URL was not computed on the fly,
     // and either the URL is null or the prefetch policy still stands, use the prefetched version
    if (prefetchedGrammar != null && expr == null && (url == null || prefetch())) {
      wrapper = prefetchedGrammar;
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
        SysLog.println("Grammar.wrapper: acquired prefetched grammar \"" + name + "\"");

     // Otherwise, get the grammar from the cache or from the URL
    } else {
       // If the caching policy is "safe", or the URL is not in the cache, acquire the URL anew
      Object cachedGrammar = null;
      int cachingPolicy = (caching != CachingConverter.NULL_INDEX) ? caching : ((Integer)BrowserProperty.value(BrowserProperty.CACHING_INDEX)).intValue();
      if (cachingPolicy == CachingConverter.SAFE_INDEX || !((cachedGrammar = DOMBrowser.cache.get(url)) instanceof Wrapper)) {
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK)) {
          if (cachingPolicy == CachingConverter.SAFE_INDEX) SysLog.println("Grammar.wrapper: caching policy is \"safe\", getting from URL");
          else if (cachedGrammar == null) SysLog.println("Grammar.wrapper: no such cache entry, getting from URL");
          else SysLog.println("Grammar.wrapper: cached object is not a Grammar.Wrapper, getting from URL");
        }
         // If the file type is unspecified, get it from the file extension, if possible, else set it to the default (VISECType)
        if (type == NoType) type = (extension == NoType) ? DefaultType : extension;
         // Get a grammar and wrap it
        wrapper = new Wrapper(name, type, url.bytes(fetchtimeout));       // Throws VXMLEvent
         // Cache the wrapped grammar
        DOMBrowser.cache.put(url, wrapper, wrapper.size());
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK)) {
          cachedGrammar = DOMBrowser.cache.get(url);
          if (cachedGrammar == null) SysLog.println("Grammar.wrapper: CACHE FAILURE - null");
          else if (!cachedGrammar.equals(wrapper)) SysLog.println("Grammar.wrapper:  CACHE FAILURE - unequal");
          else SysLog.println("Grammar.wrapper: cache holds " + DOMBrowser.cache.size() + " entries in " + DOMBrowser.cache.totalBytes() + " bytes");
        }
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
          SysLog.println("Grammar.wrapper: acquired grammar \"" + name + "\" from URL");

       // Otherwise, acquire the URL from the cache
      } else {
        wrapper = (Wrapper)cachedGrammar;
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
          SysLog.println("Grammar.wrapper: acquired grammar \"" + name + "\" from cache");
      }
    }
    if (url != null) previousURL = url;
    return wrapper;
  }
}
