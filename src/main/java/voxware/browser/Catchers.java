package voxware.browser;

import java.util.NoSuchElementException;
import java.util.Enumeration;

import voxware.browser.*;
import voxware.util.SysLog;

 /** The event Catchers for a given event
   * There MUST be one of these in the EventHandler for any ScopeElement in which the event can be thrown */
class Catchers {

  private static final int DEFAULT_NUM_CATCHERS = 5;

  int            eventCount;
  OrderedVector  catchers;    // An OrderedVector of all the Catcher objects for a given event name

  public Catchers() {
    eventCount = 0;
  }

  public void resetEventCount() {
    eventCount = 0;
  }

  public int eventCount() {
    return eventCount;
  }

  public void eventCount(int eventCount) {
    this.eventCount = eventCount;
  }

  public void insert(Catcher catcher) {
    if (catchers == null) catchers = new OrderedVector(DEFAULT_NUM_CATCHERS);
    catchers.insert(catcher);
  }

   // Get the first available Catcher with the greatest count less than or equal to this Catchers' pre-incremented eventCount
  public Catcher catcher(ScopeElement scope) throws VXMLEvent {
    return catcher(++eventCount, scope);
  }

   // Get the first available Catcher with the greatest count less than or equal to the externally supplied eventCount
  public Catcher catcher(int eventCount, ScopeElement scope) throws VXMLEvent {
    Catcher catcher = null;
    if (catchers != null) {
      int count = eventCount;
      search:
      while (count > 0) {
        Enumeration enum1 = catchers.greatestLTE(count).elements();
        while (enum1.hasMoreElements()) {
          catcher = (Catcher)(enum1.nextElement());
          try {
            if (catcher.evaluateCondition(scope)) {      // Throws UndeclaredVariableException, InvalidExpressionException, VXMLEvent
              this.eventCount = eventCount;
              break search;
            }
          } catch (UndeclaredVariableException e) {
            if (SysLog.printLevel >= 1) SysLog.println("Catchers.catcher(): ERROR - " + e.toString());
            throw new VXMLEvent("error.semantic.undeclared", e);
          } catch (InvalidExpressionException e) {
            if (SysLog.printLevel >= 1) SysLog.println("Catchers.catcher(): ERROR - " + e.toString());
            throw new VXMLEvent("error.syntactic", e);
          }
        }
         // No Catcher's cond attribute is satisfied; look for one with a smaller event count
        count = (catcher != null) ? catcher.count() - 1 : 0;
        catcher = null;
      }
    }
    return catcher;
  }
} // class Catchers
