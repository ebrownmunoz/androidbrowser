package voxware.browser;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import voxware.browser.*;
import voxware.util.SysLog;

 /** An Interrupt */
class Interrupt extends SuperGrammar implements Cloneable {

  static int instanceCount = 0;

  private String source = null;             // The name of the source of the interrupt
  private String content = null;            // The content of the interrupt
  private String level = null;
  private int delta = SystemEvent.FALLING;  // Trigger on a falling level by default

  public SystemEvent template = null;
  public boolean hard = false;              // Override Field modality

   /** Construct an Interrupt from an <interrupt> element in the DOM tree */
  public Interrupt(Element interrupt) throws InvalidTag, DOMException, NumberFormatException, IllegalArgumentException {
    super(interrupt);
    name = "_INT" + Integer.toString(++instanceCount);             // Supply an internal name

     // Get the attributes of the <interrupt> tag
    NamedNodeMap nnm = interrupt.getAttributes();
    for (int j = 0; j < nnm.getLength(); j++) {
      Node node = nnm.item(j);
      String nodeName = node.getNodeName();
      if (nodeName.equalsIgnoreCase("level")) {
        level = node.getNodeValue();                               // Throws DOMException
      } else if (nodeName.equalsIgnoreCase("sense")) {
        if (node.getNodeValue().equalsIgnoreCase("riseto"))        // Throws DOMException
          delta = SystemEvent.RISING;
      } else if (nodeName.equalsIgnoreCase("source")) {
        source = node.getNodeValue();                              // Throws DOMException
      } else if (nodeName.equalsIgnoreCase("content")) {
        content = node.getNodeValue();                             // Throws DOMException
      } else if (SysLog.printLevel > 0) {
        SysLog.println("Interrupt::Interrupt: WARNING - unexpected attribute node \"" + nodeName + "\"");
      }
    }

     // The source is REQUIRED
    if (source == null) {
      throw new InvalidTag((Node)interrupt);
    } else {
       // Construct the template during preprocessing if possible
      getTemplate();
    }
  }

  public String getSource() {
    return source;
  }

   // Return the template, constructing if if necessary
  public SystemEvent getTemplate() {
    if (template == null) {
      int index = SystemEvent.NULL_INDEX;
       // Find the longest initial dot-delimited substring that matches a SystemEvent name
      String sourceEvent = null;
      int dot = source.length();
      while (dot >= 0) {
        sourceEvent = source.substring(0, dot);
        if ((index = SystemEvent.index(sourceEvent)) != SystemEvent.NULL_INDEX) break;
        dot = sourceEvent.lastIndexOf('.', dot - 1);
      }
      if (index != SystemEvent.NULL_INDEX) {
        template = new SystemEvent(index, level, delta);  // Throws NumberFormatException, IllegalArgumentException
        if (dot < source.length()) template.setMessage(new SourcedInterrupt(source.substring(dot + 1), content));
        else                       template.setMessage(content);
        if (SysLog.printLevel > 2  && SysLog.printMaskBitsSet(SysLog.INTERRUPT_MASK) && template != null)
          SysLog.println("Interrupt::getTemplate: created " + SystemEvent.name(template.index) + " (" + index + ") template for source \"" + source + "\"");
      } else if (SysLog.printLevel > 0) {
        SysLog.println("Interrupt::getTemplate: WARNING - could not yet create a template for source \"" + source + "\"");
      }
    }
    return template;
  }

   // Override the setLink() method in SuperGrammar to set the hard boolean 
  public void setLink(Link link) {
    super.setLink(link);
    hard = link.overrideModality();
  }

   // Implementation of Cloneable interface
  public Object clone() {
    Interrupt clone = null;
    try {
      clone = (Interrupt) super.clone();
      clone.template = (SystemEvent) this.template.clone();
    } catch (Exception e) {
    }

    return clone;
  }

} // class Interrupt
