package voxware.browser;

import java.util.*;
import java.io.*;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

/** The Class that gives variables scope */
public class Scope extends Cardinal {

	private static int hashfactory = 0;

	public String prefix; // Scope variable prefix (e.g., "session", "dialog",
							// etc.)

	public static final int INITIAL_CAPACITY = 5;
	public static final int GROWTH_QUANTUM = 5;

	protected Scope parent;
	protected Cardinal child;
	protected ScriptableObject scope; // A link in the JavaScript scope chain
	private Vector initializers; // An initialization Expr or VXMLScript for
									// each variable or script in this scope
	protected Script script; // An arbitrary JavaScript script to execute when
								// entering this scope
	protected int hashcode;

	public Scope() {
		hashcode = ++hashfactory;
		prefix = null;
		parent = null;
		child = null;
		scope = null;
		initializers = null;
		script = null;
	}

	/** Make this Scope a child of the parent Scope */
	public void makeChildOf(Scope parent) {
		this.parent = parent;
		// Make a JavaScript scope object for this Scope
		scope = (ScriptableObject) parent.newObject();
		// Make the JavaScript scope object of its parent the parent of its new
		// JavaScript scope object
		scope.setParentScope(parent.scope());
	}

	/** Get the child */
	public Cardinal child() {
		return this.child;
	}

	/** Get the parent */
	public Scope parent() {
		return this.parent;
	}

	/** Have this Scope adopt an only child */
	public void adoptChild(Cardinal child) {
		this.child = child;
	}

	/** Compile an entry script for this scope */
	public void compileScript(StringReader source, String sourceName,
			int lineNumber, Object securityDomain) {
		try {
			if (SysLog.shouldLog(2, SysLog.SCRIPT_MASK))
				SysLog.println("Scope: compiling entry script");
			script = Context.getCurrentContext().compileReader(scope, source,
					sourceName, lineNumber, securityDomain);
			if (SysLog.shouldLog(2, SysLog.SCRIPT_MASK))
				SysLog.println("Scope: finished compiling entry script");
		} catch (IOException ioe) {
			throw new RuntimeException(); // Should never occur, because we just
											// made the reader from a String
		}
	}

	/** Execute the entry script for this scope */
	public Object executeScript() throws VXMLEvent {
		try {
			return (script != null) ? script.exec(Context.getCurrentContext(),
					scope) : null; // Throws JavaScriptException
		} catch (JavaScriptException e) {
			throw new VXMLEvent("error.javascript.uncaughtevent", e);
		}
	}

	/**
	 * Get this Scope's Variables (now the JavaScript scope object) (DEPRECATED)
	 */
	public ScriptableObject variables() {
		return this.scope;
	}

	/** Get this Scope's JavaScript scope object */
	public ScriptableObject scope() {
		return this.scope;
	}

	/** Give this Scope a JavaScript scope object */
	public void scope(ScriptableObject scope) {
		this.scope = scope;
	}

	/** Put a Variable into this scope */
	public Object putVariable(String name, Variable variable) {
		return makeVariable(name, variable.value(), variable.initializer());
	}

	/** Make a variable with an initializer in this scope */
	public Object makeVariable(String name, Object value, Expr initializer) {
		if (scope != null && value != null)
			setProp(scope, name, value);
		if (initializer != null) {
			if (initializers == null)
				initializers = new Vector(INITIAL_CAPACITY, GROWTH_QUANTUM);
			initializer.name(name);
			initializers.addElement(initializer);
		}
		return value;
	}

	/** Make a READONLY variable with no initializer in this scope */
	public Object makeVariable(String name, Object value) {
		makeVariable(name, value, null);
		try {
			scope.setAttributes(name, scope, ScriptableObject.READONLY);
		} catch (PropertyException pe) {
			throw new RuntimeException(pe.toString()); // Should never happen,
														// since we just created
														// this property
		}
		return value;
	}

	/** Put a VXMLScript into this scope */
	public void putScript(VXMLScript script) {
		if (initializers == null)
			initializers = new Vector(INITIAL_CAPACITY, GROWTH_QUANTUM);
		initializers.addElement(script);
	}

	/** Return a new, empty JavaScript Object */
	public Scriptable newObject() {
		return newObject(scope);
	}

	/** Return a new, empty JavaScript Object in an arbitrary scope */
	public static Scriptable newObject(Scriptable scope) {
		try {
			return (Scriptable) Context.getCurrentContext().newObject(scope);
		} catch (PropertyException e) {
			throw new RuntimeException("Scope.newObject():" + e.toString());
		} catch (NotAFunctionException e) {
			throw new RuntimeException("Scope.newObject():" + e.toString());
		} catch (JavaScriptException e) {
			throw new RuntimeException("Scope.newObject():" + e.toString());
		}
	}

	/** Return a new JavaScript Array in an arbitrary scope */
	public static Scriptable newArray(Scriptable scope, Object[] elements) {
		return (Scriptable) Context.getCurrentContext().newArray(scope,
				elements);

	}

	// Look up a name in the scope chain and return its value
	// This method is the same as ScriptRuntime.name(), except it throws
	// UndeclaredVariableException rather than ReferenceError on failure
	public Object name(String id) throws UndeclaredVariableException {
		Scriptable obj = scope;
		while (obj != null) {
			Scriptable m = obj;
			do {
				Object result = m.get(id, obj);
				if (result != Scriptable.NOT_FOUND)
					return result; // Success!
				m = m.getPrototype();
			} while (m != null);
			obj = obj.getParentScope();
		}
		throw new UndeclaredVariableException(id); // Failure!
	}

	// Look up the Scriptable containing the named property in the scope chain
	// and return it
	// This method is the same as ScriptRuntime.bind(), except it throws
	// UndeclaredVariableException rather than returning null on failure
	public Scriptable bind(String id) throws UndeclaredVariableException {
		Scriptable obj = scope;
		while (obj != null) {
			Scriptable m = obj;
			do {
				if (m.has(id, obj))
					return obj; // Success!
				m = m.getPrototype();
			} while (m != null);
			obj = obj.getParentScope();
		}
		throw new UndeclaredVariableException(id); // Failure!
	}

	// Return the value of the named property of the given Scriptable
	// This method is a simplified version of ScriptRuntime.getProp(), useable
	// when the container is known to be a Scriptable
	public static Object getProp(Scriptable container, String id)
			throws UndeclaredVariableException {
		Scriptable m = container;
		do {
			Object result = m.get(id, container);
			if (result != Scriptable.NOT_FOUND)
				return result; // Success!
			m = m.getPrototype();
		} while (m != null);
		throw new UndeclaredVariableException(id); // Failure!
	}

	// Insert the named property and its value into the given Scriptable, and
	// return its value
	// This method is a simplified version of ScriptRuntime.setProp(), useable
	// when the container is known to be a Scriptable
	public static Object setProp(Scriptable container, String id, Object value) {
		Scriptable m = container;
		do {
			if (m.has(id, container)) {
				m.put(id, container, value);
				return value;
			}
			m = m.getPrototype();
		} while (m != null);

		container.put(id, container, value);
		return value;
	}

	// Set the parent of the named Scriptable to the specified Scriptable
	// parent, and return the parent
	// This method is a simplified version of ScriptRuntime.setParent(), useable
	// when the arguments are known to be Scriptables
	public static Scriptable setParent(Scriptable obj, Scriptable parent) {
		Scriptable s = parent;
		while (s != null) {
			if (s == obj)
				throw new RuntimeException(
						"Scope.setParent(): UNEXPECTED - cyclic scope chain");
			s = s.getParentScope();
		}
		obj.setParentScope(parent);
		return parent;
	}

	// Insert the named property and its value into the appropriate Scriptable
	// in the scope chain whose head is this scope
	// The id may be a fully specified compound ECMA name, in which case the
	// full hierarchy of containing Scriptables MUST
	// exist, or the method will throw UndeclaredVariableException. The final
	// property will be created if it does not exist.
	public Object setProperty(String id, Object value)
			throws UndeclaredVariableException {
		return setProp(container(id), id.substring(id.lastIndexOf('.') + 1),
				value); // Throws UndeclaredVariableException
	}

	/**
	 * Find and return the Scriptable containing the named property in the scope
	 * chain whose head is this scope
	 */
	// The name may be a fully specified compound ECMA name. This method
	// verifies that the full hierarchy of containing
	// Scriptables exists, throwing UndeclaredVariableException otherwise. The
	// final property need not exist.
	public Scriptable container(String name) throws UndeclaredVariableException {
		StringTokenizer st = new StringTokenizer(name, ".");
		if (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (st.hasMoreTokens()) {
				Scriptable container;
				// Compound name, so find the outermost property in the scope
				// chain
				Object property = name(token); // Throws
												// UndeclaredVariableException
				if (property instanceof Scriptable
						&& property != VXMLTypes.PROTO_UNDEFINED) {
					token = st.nextToken();
					container = (Scriptable) property;
				} else {
					throw new UndeclaredVariableException(name);
				}
				// Find the container of the innermost property (even if the
				// property is not there)
				while (st.hasMoreTokens()) {
					property = getProp(container, token); // Throws
															// UndeclaredVariableException
					if (property instanceof Scriptable
							&& property != VXMLTypes.PROTO_UNDEFINED) {
						token = st.nextToken();
						container = (Scriptable) property;
					} else {
						throw new UndeclaredVariableException(name);
					}
				}
				return container;
			} else {
				// Atomic name, so just return its container (if it has one)
				return bind(token); // Throws UndeclaredVariableException
			}
		} else {
			throw new UndeclaredVariableException(name);
		}
	}

	/** Return the value of the named variable from this or a greater scope */
	public Object variable(String name) throws UndeclaredVariableException {
		return getProp(container(name),
				name.substring(name.lastIndexOf('.') + 1)); // Throws
															// UndeclaredVariableException
	}

	/**
	 * Initialize all the variables and run all the scripts in this scope in the
	 * order they were made
	 */
	public void initializeVariables() throws VXMLEvent {
		initializeVariables(null);
	}

	/**
	 * Initialize all the variables and run all the scripts in this scope in the
	 * order they were made, getting values from the given JavaScript Object if
	 * the initializer returns no value
	 */
	public void initializeVariables(Scriptable initialValues) throws VXMLEvent {
		initializeVariables(initialValues, this);
	}

	/**
	 * Initialize all the variables and run all the scripts in this scope in the
	 * order they were made, using another arbitrary scope for evaluation,
	 * getting values from the given JavaScript Object if the initializer
	 * returns no value
	 */
	public void initializeVariables(Scriptable initialValues,
			Scope evaluationScope) throws VXMLEvent {
		if (this.scope != null && initializers != null) {
			try {
				for (int i = 0; i < initializers.size(); i++) {
					Object object = initializers.elementAt(i);
					Expr initializer = (object instanceof VXMLScript) ? ((VXMLScript) object)
							.expression(evaluationScope) : (Expr) object;
					// If there is an associated variable, try to assign a value
					// to it
					if (initializer.name() != null) {
						Object value = null;
						// First try to get a value from the initialValues
						// argument
						if (initialValues != null)
							try {
								value = getProp(initialValues,
										initializer.name());
							} catch (UndeclaredVariableException e) {
							}
						// If this fails, try to get it from the variable's
						// initializer
						if (value == null)
							value = initializer.evaluate(evaluationScope); // Throws
																			// InvalidExpressionException,
																			// UndeclaredVariableException,
																			// VXMLEvent
						// If this fails, make it undefined
						if (value == null)
							value = VXMLTypes.PROTO_UNDEFINED;
						// Assign it to the variable
						setProp(this.scope, initializer.name(), value);
						// Otherwise, just evaluate the expression
					} else {
						initializer.evaluate(evaluationScope); // Throws
																// InvalidExpressionException,
																// UndeclaredVariableException,
																// VXMLEvent
					}
				}
			} catch (UndeclaredVariableException e) {
				if (SysLog.shouldLog(1))
					SysLog.println("Scope.initializeVariables(): undeclared variable \""
							+ e.getMessage() + "\"");
				throw new VXMLEvent("error.semantic.undeclared", e);
			} catch (InvalidExpressionException e) {
				throw new VXMLEvent("error.syntactic.badexpression", e);
			}
		}
	}

	/**
	 * Initialize the Scope, making all of its variables undeclared (used for
	 * anonymous Scope)
	 */
	public void initializeScope(Scope parent) {
		scope = null;
		makeChildOf(parent);
	}

	/** Override hashcode() method in Object to guarantee uniqueness */
	public int hashcode() {
		return hashcode;
	}

	/**
	 * Override equals() method in Object for consistency with overriding
	 * hashcode() method
	 */
	public boolean equals(Object object) {
		return object instanceof Scope
				&& ((Scope) object).hashcode == this.hashcode;
	}

}
