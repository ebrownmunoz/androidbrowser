package voxware.browser;

 /** A system variable */
public class SystemVariable extends Variable {

  private String  name;

   // Construct a SystemVariable
  public SystemVariable(String name) {
    super();
    this.name = name;
  }

   // Return the current value -- overrides method in Variable
  public Object value() {
    //currentValue = SystemEvent.value(name);
    return currentValue;
  }

   // Overrides method in Variable
  public void initialize() {}

   // Overrides method in Variable
  public void initialize(Scope scope) {}

   // Overrides method in Variable
  public void clear() {}

   // Overrides method in Variable
  public void setDefault() {}

   // Assign a value -- overrides method in Variable
  public void assign(Object value) {
    currentValue = value;
    //SystemEvent.setValue(name, currentValue);
  }

   // Assign the value of an Expr known not to contain References -- overrides method in Variable
  public void assign(Expr value) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent  {
    currentValue = value.evaluate(null);
    //SystemEvent.setValue(name, currentValue);
  }

   // Assign the value of an Expr -- overrides method in Variable
  public void assign(Expr value, Scope scope) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent  {
    currentValue = value.evaluate(scope);
    //SystemEvent.setValue(name, currentValue);
  }
}
