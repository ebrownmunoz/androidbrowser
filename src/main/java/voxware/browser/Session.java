package voxware.browser;

import java.util.*;
import java.net.*;
import java.io.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.InputSource;
import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.Cache;
import voxware.util.SysLog;

 /** A Browser Session */
class Session extends ScopeElement {
  public static final int    DEFAULT_MAX_SPEAKER_CACHE_ENTRIES = 20;
  public static final long   DEFAULT_MAX_SPEAKER_CACHE_BYTES = 500000;
  
  // This little bit of ugliness is copied from legacy code.
  public static int max_speaker_cache_entries = DEFAULT_MAX_SPEAKER_CACHE_ENTRIES;
  public static long max_speaker_cache_bytes = DEFAULT_MAX_SPEAKER_CACHE_BYTES;
  
  private PrintWriter out = null;           // A PrintWriter provokes a DOM tree printout
  private boolean     canonical = false;
  private boolean     initialized = false;
  private Object      subdialog;            // Whether or not this Session is a subdialog Session
  private boolean     preprocess;           // Whether to preprocess executable content
  private int         preprocessLevel = 0;
  private final Recognizer recognizer;
  private final SystemVariables systemVariables;
  private final AudioPlayer audioPlayer;
  private final InterruptHandler interruptHandler;
  

  private final Cache speakerCache;
  
   /** Construct an empty Session */
  public Session(Recognizer recognizer, AudioPlayer audioPlayer, InterruptHandler interruptHandler, SystemVariables systemVariables, boolean preprocess) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
    this(recognizer, audioPlayer, interruptHandler, systemVariables, false, preprocess);
    this.preprocess = preprocess;

  }

   /** Construct an empty Session */
  public Session(Recognizer recognizer, AudioPlayer audioPlayer, InterruptHandler interruptHandler, 
		  SystemVariables systemVariables, boolean subdialog, 
		  boolean preprocess) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
    super();
    prefix = "session";
    handler = new EventHandler();
    this.interruptHandler = interruptHandler;
    this.audioPlayer = audioPlayer;
    this.subdialog = VXMLTypes.booleanToBoolean(subdialog);
    this.preprocess = preprocess && !subdialog;
    this.recognizer = recognizer;
    this.systemVariables = systemVariables;
    this.speakerCache = new Cache(DEFAULT_MAX_SPEAKER_CACHE_ENTRIES, DEFAULT_MAX_SPEAKER_CACHE_BYTES);

	// Create a JavaScript scope and make it the base of the scope chain
	scope = (ScriptableObject)Context.getCurrentContext().initStandardObjects(null);
    Scope.setProp(scope, prefix, scope);
  } // Session(String)

   /** Initialize all the Session variables (overrides initializeVariables() in Scope) */
  public void initializeVariables() throws VXMLEvent {
    systemVariables.define(scope, scope);   // Throws VXMLEvent
    makeVariable("issubdialog", subdialog);
  }

   /** Run the top-level Session */
  public String run(String startingURI, Speaker defaultSpeaker) throws VXMLEvent {
	try {
       // Construct the starting Goto
      Goto whereToGo = new Goto(startingURI);
       // Parse the starting document. Subsequent documents are also parsed in Goto.acquireTarget()
      if (SysLog.printLevel > 0) SysLog.println("Session.run: parsing startingURI \"" + startingURI + "\"");
      whereToGo.acquireTarget(this);                  // Throws VXMLEvent
      if (SysLog.printLevel > 0) SysLog.println("Session.run: startingURI parsed");
       // Run the top-level Session
      run(whereToGo, defaultSpeaker);                 // Throws VXMLEvent, VXMLExit
      return "top-level session ran to completion";
    } catch (VXMLExit ve) {
      String reason = ScriptRuntime.toString(ve.reason());
      String log = SystemEvent.logName(SystemEvent.EXCEPTION_INDEX) + "=exit(" + reason + ")";
      SystemEvent.systemMonitor().logEvent(SystemEvent.EXCEPTION_INDEX, log);
      return reason;
    }
  }

   /** Run the Session */
  public Scriptable run(Goto whereToGo, Speaker defaultSpeaker) throws VXMLEvent, VXMLExit {
    Scriptable results = null;
    setDefaults();
    (new Application()).makeChildOf(this);  // Throws VXMLEvent
    ((ScopeElement)child).setSpeaker(defaultSpeaker);
    if (SysLog.shouldLog(1, SysLog.PREPROCESS_MASK)) SysLog.println("Session.run: preprocess start");
    preprocess(whereToGo);
    if (SysLog.shouldLog(1, SysLog.PREPROCESS_MASK)) SysLog.println("Session.run: preprocess end");
    try {
       // Interpret the preprocessed document
      if (SysLog.shouldLog(1, SysLog.PREPROCESS_MASK)) SysLog.println(1, "Session.run: interpret start");
      interpret(whereToGo, interruptHandler);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
      if (SysLog.shouldLog(1, SysLog.PREPROCESS_MASK)) SysLog.println(1, "Session.run: interpret end");
    } catch (VXMLReturn vr) {
      if (SysLog.shouldLog(1, SysLog.PREPROCESS_MASK | SysLog.SUBDIALOG_MASK))
        SysLog.println("Session.run: interpret end (==== RETURNING FROM SUBDIALOG ====)");
      results = vr.result();
    }
    return results;
  } // run(String,Speaker)

   /** Enable/Disable printing a canonical trace of the DOM tree during preprocessing */
  public void setTrace(boolean canonical) {
    this.canonical = canonical;
  }

   /** Set the Session-level defaults */
  private void setDefaults() {
     // DefaultCatcher(String event, boolean reprompt, boolean exit, String urlName, String message)
    registerCatcher(new DefaultCatcher(this, "cancel",  false, false, null, null));
    registerCatcher(new DefaultCatcher(this, "error",   false, true,  DOMBrowser.DEFAULT_ERROR_URLNAME,   "Exiting due to uncaught error"));
    registerCatcher(new DefaultCatcher(this, "help",    true,  false, DOMBrowser.DEFAULT_HELP_URLNAME,    "No help is available"));
    registerCatcher(new DefaultCatcher(this, "noinput", true,  false, null, null));
    registerCatcher(new DefaultCatcher(this, "nomatch", true,  false, DOMBrowser.DEFAULT_NOMATCH_URLNAME, "Utterance not recognized"));
  }

   /** Construct a named Expr from a <var> tag */
  public static Expr expression(Element node) throws DOMException, InvalidExpressionException, InvalidTag {
    String  name = "";
    Expr    expression;

     // Get the name of the variable (REQUIRED)
    Attr attr = node.getAttributeNode("name");
    if (attr != null) {
      name = attr.getNodeValue();                                // Throws DOMException
      if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
        SysLog.println("Session.expression: found \"name\" attribute (" + name + ")");
    } else {
      throw new InvalidTag(node);                                // Throws InvalidTag
    }

     // Get the initializing expression (OPTIONAL)
    Attr exprAttr = node.getAttributeNode("expr");
    if (exprAttr != null) {
      if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
        SysLog.println("Session.expression: found \"expr\" attribute (" + exprAttr.getNodeValue() + ")");
      expression = new Expr(exprAttr.getNodeValue(), null);      // Throws DOMException, InvalidExpressionException
    } else {
      expression = new Expr(VXMLTypes.PROTO_UNDEFINED);
    }

     // Name the Expr
    expression.name(name);

    return expression;
  }

  /** Preprocesses the Page in a Goto */
  public void preprocess(Goto whereToGo) throws VXMLEvent {
     // Save the current Application and Page in case preprocessing fails
    Application currentRoot = (Application)child;
    Page        currentPage = (Page)currentRoot.child();
     // Collect the garbage if prudent
    // GarbageCollector.getInstance().collect(((Number)BrowserProperty.value(BrowserProperty.JAVAGCTHRESHOLD_INDEX)).intValue(), ((Number)BrowserProperty.value(BrowserProperty.MINBYTESFREE_INDEX)).longValue());
    try {
      ((ScopeElement)child).adoptChild(null);  // Have the Application abandon its child Page, if any
      long before = 0;
      if (SysLog.printLevel > 0) { SysLog.println("Session.preprocess(Goto): preprocessing \"" + whereToGo.next() + "\""); before = System.currentTimeMillis(); }
       // Preprocess the document, acquiring a new root document if requested
      preprocess(whereToGo.page(), (Application)child, whereToGo);  // Throws VXMLEvent
      if (SysLog.printLevel > 0) { SysLog.println("Session.preprocess(Goto): preprocessing took " + (System.currentTimeMillis() - before) + " ms"); }
    } catch (Exception exc) {
       // Revert to the saved Application and Page and throw the Exception there
      if (child != currentRoot) child = currentRoot;
      else                      ((ScopeElement)child).adoptChild(currentPage);
      if (!(exc instanceof VXMLEvent)) exc = new VXMLEvent("error.badfetch.preprocess", exc);
      throw (VXMLEvent)exc;
    }
  }

   /** Preprocesses the specified node, recursively.
     * The "parent" argument is the ScopeElement (Session, Application, Page, Form or FormItem) */
  private void preprocess(Node node, ScopeElement parent, Goto context) throws VXMLEvent {

    // Is there anything to do?
    if (node == null) return;

    int type = node.getNodeType();
    if (parent == null)
      throw new RuntimeException("Session.preprocess: UNEXPECTED - called with null parent");
    if (SysLog.shouldLog(2, SysLog.PREPROCESS_MASK)) {
      SysLog.println();
      SysLog.println("Session.preprocess: start [Level: " + preprocessLevel + ", Parent: " + parent.getClass().getName() + "] Node: " + node.getNodeName() + " = \"" + node.getNodeValue() + "\"");
    }
    preprocessLevel++;

    try {
      switch (type) {
         // Preprocess the document
        case Node.DOCUMENT_NODE: {
            if (!canonical && SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
              SysLog.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            preprocess(((Document)node).getDocumentElement(), parent, context); // Throws VXMLEvent
            break;
          }

         // Preprocess an element and its attributes
        case Node.ELEMENT_NODE: {
            ScopeElement newParent = parent;
            String name = node.getNodeName();
            Attr attrs[] = null;
            if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK)) {
              SysLog.print("<");
              SysLog.print(name);
              attrs = sortAttributes(node.getAttributes());
              for ( int i = 0; i < attrs.length; i++ ) {
                Attr attr = attrs[i];
                SysLog.print(" " + attr.getNodeName() + "=\"" + normalize(attr.getNodeValue()) + "\"");
              }
              SysLog.print(">");
            }

            if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
              SysLog.println("Session.preprocess: Elements.index(" + name + ")=" + Elements.index(name));
            switch (Elements.index(name)) {

              case Elements.ASSIGN_INDEX: {
                   // No preprocessing
                  break;
                }

              case Elements.AUDIO_INDEX: {
                   // No preprocessing
                  break;
                }

              case Elements.BLOCK_INDEX: {
                   // Construct a Block from the <block> element
                  Block block = new Block(this, (Element)node, context, preprocess);
                   // Make it a child of the parent Form
                  block.makeChildOf(parent);                    // Throws VXMLEvent
                   // Turn off child preprocessing
                  newParent = null;
                  break;
                }

              case Elements.BREAK_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.CATCH_INDEX: {
                   // Construct a Catcher from the <catch> element
                  Catcher catcher = new Catcher(this, (Element)node, context, preprocess);
                   // Register it in the current ScopeElement
                  parent.registerCatcher(catcher);
                   // Turn off child preprocessing
                  newParent = null;
                  break;
                }

              case Elements.CHOICE_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.CLEAR_INDEX: {
                   // No preprocessing
                  break;
                }

              case Elements.DIV_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.ELSE_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.ELSEIF_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.EMP_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.ENUMERATE_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.ERROR_INDEX: {
                   // Construct a Catcher from the <error> element
                  Catcher catcher = new Catcher(this, (Element)node, context, "error", preprocess);
                   // Register it in the current ScopeElement
                  parent.registerCatcher(catcher);
                   // Turn off child preprocessing
                  newParent = null;
                  break;
                }

              case Elements.EXIT_INDEX: {
                   // No preprocessing
                  break;
                }

              case Elements.FIELD_INDEX: {
                   // Construct a Field from the <field> element
                  Field field = new Field(this, (Element)node);
                   // Make it a child of the parent Form
                  field.makeChildOf(parent);                    // Throws VXMLEvent
                   // Give all children Field scope
                  newParent = (ScopeElement)field;
                  break;
                }

              case Elements.FILLED_INDEX: {
                   // Construct a Filled from a <filled> element and register it
                  Filled filled = parent.makeFilled((Element)node, context, preprocess);  // Throws VXMLEvent
                   // Turn off child preprocessing
                  newParent = null;
                  break;
                }

              case Elements.FORM_INDEX: {
                   // Construct a Form from the <form> element
                  Form form = new Form(this, (Element)node);
                   // Make it a child of the parent Page
                  form.makeChildOf(parent);                     // Throws VXMLEvent
                   // Give all children Form scope
                  newParent = (ScopeElement)form;
                  break;
                }

              case Elements.GOTO_INDEX: {
                   // No preprocessing
                  break;
                }

              case Elements.GRAMMAR_INDEX: {
                   // Construct a Grammar from the <grammar> element
                  Grammar grammar = new Grammar((Element)node); // Throws DOMException, InvalidTag, MalformedURLException, VXMLEvent
                   // Give the Grammar the current scope
                  parent.putGrammar(grammar);
                  break;
                }

              case Elements.HELP_INDEX: {
                   // Construct a Catcher from the <help> element
                  Catcher catcher = new Catcher(this, (Element)node, context, "help", preprocess);
                   // Register it in the current ScopeElement
                  parent.registerCatcher(catcher);
                   // Turn off child preprocessing
                  newParent = null;
                  break;
                }

              case Elements.IF_INDEX: {
                   // No preprocessing
                  break;
                }

              case Elements.INITIAL_INDEX: {
                   // Construct a Initial from the <initial> element
                  Initial initial = new Initial(this, (Element)node);
                   // Make it a child of the parent Form
                  initial.makeChildOf(parent);                  // Throws VXMLEvent
                   // Give all children Initial scope
                  newParent = (ScopeElement)initial;
                  break;
                }

              case Elements.INTERRUPT_INDEX: {
                   // Construct an Interrupt from the <interrupt> element
                  Interrupt interrupt = new Interrupt((Element)node);
                   // Give the Interrupt the current scope
                  parent.putInterrupt(interrupt);
                  break;
                }

              case Elements.LINK_INDEX: {
                   // Construct a Link from the <link> element
                  Link link = new Link((Element)node, context); // Throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException
                   // Make it a child of the parent ScopeElement
                  link.makeChildOf(parent);
                   // Give all children Link scope
                  newParent = (ScopeElement)link;
                  break;
                }

              case Elements.LOG_INDEX: {
                   // Construct an unexecutable Logger from the <log> element
                  Logger logger = new Logger((Element)node, false); // Throws DOMException, InvalidExpressionException, InvalidTag, VXMLEvent
                   // Give the Logger the current scope
                  parent.putLogger(logger);
                  break;
                }

              case Elements.MENU_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.META_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.NOMATCH_INDEX: {
                   // Construct a Catcher from the <nomatch> element
                  Catcher catcher = new Catcher(this, (Element)node, context, "nomatch", preprocess);
                   // Register it in the current ScopeElement
                  parent.registerCatcher(catcher);
                   // Turn off child preprocessing
                  newParent = null;
                  break;
                }

              case Elements.NOINPUT_INDEX: {
                   // Construct a Catcher from the <noinput> element
                  Catcher catcher = new Catcher(this, (Element)node, context, "noinput", preprocess);
                   // Register it in the current ScopeElement
                  parent.registerCatcher(catcher);
                   // Turn off child preprocessing
                  newParent = null;
                  break;
                }

              case Elements.OBJECT_INDEX: {
                   // Construct a VXMLObj from the <object> element
                   // Throws DOMException, InvalidExpressionException, InvalidTag
                  VXMLObj vxmlobj = new VXMLObj(this, (Element)node);
                   // Make it a child of the parent Form
                  if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
                    SysLog.println("Session.preprocess: new VXMLObj constructed");
                  vxmlobj.makeChildOf(parent);                           // Throws VXMLEvent
                    // Give all children Object scope
                  newParent = (ScopeElement)vxmlobj;
                  break;
                }

              case Elements.PARAM_INDEX: {
                   // Must be a child of a SubFormItem
                  if (parent instanceof SubFormItem) {

                    String  paramName = null;
                    Expr    expr = null;       // Expression to compute the value of this parameter
                    String  value = null;      // String literal value for this parameter
                    String  valueType = null;  // True if URI (ref); false otherwise (not used for <subdialog>)
                    String  mimeType = null;   // The mime type (not used for <subdialog>)

                     // Get the attributes of the <param> tag
                    NamedNodeMap nnm = ((Element)node).getAttributes();
                    expr = null;
                    String deviceName = null;
                    for (int j = 0; j < nnm.getLength(); j++) {
                      Node attrNode = nnm.item(j);
                      String nodeName = attrNode.getNodeName();
                      if (nodeName.equalsIgnoreCase("name"))
                        paramName = attrNode.getNodeValue();             // Throws DOMException
                      else if (nodeName.equalsIgnoreCase("expr"))
                        expr = new Expr(attrNode.getNodeValue(), null);  // Throws DOMException, InvalidExpressionException
                      else if (nodeName.equalsIgnoreCase("value"))
                        value = attrNode.getNodeValue();                 // Throws DOMException
                      else if (nodeName.equalsIgnoreCase("valuetype"))
                        valueType = attrNode.getNodeValue();             // Throws DOMException
                      else if (nodeName.equalsIgnoreCase("type"))
                        mimeType = attrNode.getNodeValue();              // Throws DOMException
                      else if (SysLog.printLevel > 0)
                        SysLog.println("Session.preprocess: WARNING - <param> has unexpected attribute node \"" + nodeName + "\"");
                    }

                     // The "name" is required, as is "expr" or else "value"
                    if (paramName == null || (expr == null) == (value == null)) 
                      throw new InvalidTag(node);
                    if (value != null)
                      expr = new Expr(value);

                     // Put the variable into the parent's parameters object
                    ((SubFormItem)parent).makeParameter(paramName, expr);

                     // Make a READONLY shadow parameter for the type information if it is present
                    if (parent instanceof VXMLObj && (valueType != null || mimeType != null)) {
                      paramName = "$" + paramName;
                      Scriptable shadow = parent.newObject();
                      try {
                        if (valueType != null) {
                          Scope.setProp(shadow, "valuetype", valueType);
                          ((ScriptableObject)shadow).setAttributes("valuetype", parent.scope(), ScriptableObject.READONLY); // Throws PropertyException
                        }
                        if (mimeType != null) {
                          Scope.setProp(shadow, "type", mimeType);
                          ((ScriptableObject)shadow).setAttributes("type", parent.scope(), ScriptableObject.READONLY); // Throws PropertyException
                        }
                      } catch (PropertyException pe) {
                        throw new RuntimeException("Session.preprocess: " + pe.toString());  // Should never happen
                      }
                      ((SubFormItem)parent).makeParameter(paramName, shadow);
                    }

                  } else {
                    throw new VXMLEvent("error.illegalchild." + name);
                  }
                  break;
                }

              case Elements.PROMPT_INDEX: {
                   // Construct a Prompt from the <prompt> element
                  Prompt prompt = new Prompt(this, (Element)node, parent);  // Throws DOMException, InvalidExpressionException, NumberFormatException, VXMLEvent
                   // Give the Prompt the current scope
                  parent.registerPrompt(prompt);
                  break;
                }

              case Elements.PROPERTY_INDEX: {
                   // Construct a Property from a <property> element
                  String  propertyName = null;
                  Object  propertyValue = null;
                  NamedNodeMap nnm = ((Element)node).getAttributes();
                  for (int j = 0; j < nnm.getLength(); j++) {
                    Node attrNode = nnm.item(j);
                    String nodeName = attrNode.getNodeName();
                    if (nodeName.equalsIgnoreCase("name"))
                      propertyName = attrNode.getNodeValue();                             // Throws DOMException
                    else if (nodeName.equalsIgnoreCase("value"))
                      propertyValue = attrNode.getNodeValue();                            // Throws DOMException
                    else if (nodeName.equalsIgnoreCase("expr"))
                      propertyValue = new Expr(attrNode.getNodeValue(), null);            // Throws DOMException, InvalidExpressionException
                    else if (SysLog.printLevel > 0)
                      SysLog.println("Session.preprocess: WARNING - <property> has unexpected attribute node \"" + nodeName + "\"");
                  }
                  if (propertyName == null || propertyValue == null) throw new InvalidTag(node);
                  Property property = Property.newInstance(propertyName, propertyValue);
                  if (property != null) {
                     // Register the Property in the parent ScopeElement
                    parent.insertProperty(property);
                  } else if (SysLog.printLevel > 0) {
                    SysLog.println("Session.preprocess: WARNING - <property name=\"" + propertyName + "\"> does not exist");
                  }
                  break;
                }

              case Elements.PROS_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.RECORD_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.REPROMPT_INDEX: {
                   // No preprocessing
                  break;
                }

              case Elements.RETURN_INDEX: {
                   // No preprocessing
                  break;
                }

              case Elements.SAYAS_INDEX: {
                   // Not implemented
                  break;
                }

              case Elements.SCRIPT_INDEX: {
                   // Construct a VXMLScript (JavaScript) from a <script> element
                  VXMLScript script = new VXMLScript((Element)node);  // Throws DOMException, InvalidExpressionException, VXMLEvent
                   // Put the VXMLScript into its parent Scope
                  parent.putScript(script);
                  break;
                }

              case Elements.SPEAKER_INDEX: {
                   // Construct a Speaker from the <speaker> element
                  Speaker speaker = new Speaker(this, (Element)node);  // Throws DOMException, InvalidTag, MalformedURLException, VXMLEvent 
                   // Give the Speaker the current scope
                  parent.setSpeaker(speaker);
                  break;
                }

              case Elements.SUBDIALOG_INDEX: {
                   // Construct a Subdialog from the <subdialog> element
                  Subdialog subdialog = new Subdialog(this, (Element)node, context);
                   // Make it a child of the parent Form
                  if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
                    SysLog.println("Session.preprocess: new Subdialog constructed");
                  subdialog.makeChildOf(parent);                 // Throws VXMLEvent
                    // Give all children Subdialog scope
                  newParent = (ScopeElement)subdialog;
                 break;
                }

              case Elements.SUBMIT_INDEX: {
                   // No preprocessing
                  break;
                }

              case Elements.THROW_INDEX: {
                   // No preprocessing
                  break;
                }

              case Elements.VALUE_INDEX: {
                   // No preprocessing
                  break;
                }

              case Elements.VAR_INDEX: {
                   // Construct a named Expr from the <var> tag
                  Expr expression = expression((Element)node);   // Throws DOMException, InvalidExpressionException, InvalidTag
                   // Make the variable in the current scope
                  parent.makeVariable(expression.name(), null, expression);
                  break;
                }

              case Elements.VXML_INDEX: {
                   // If the parent is the application, construct a new Page from this <vxml> element
                  if (parent instanceof Application) {
                    Session session = (Session) parent.parent();
                    Page page = new Page((Element)node);
                    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
                      SysLog.println("Session.preprocess: constructed new " + page + " for element \"" + node.getNodeName() + "\"");
                     // If no root document is requested, construct a new empty Application in Session scope
                    if (page.rootURL() == null) {
                      Application application = new Application();
                      application.makeChildOf(session);
                      context.root(null);
                      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
                        SysLog.println("Session.preprocess: constructed new empty " + application);
                     // Otherwise, if a new root document is requested, construct an Application from it
                    } else if (!(page.rootURL().equals(((Application)parent).rootURL()))) {
                      Document document = context.root();
                       // Parse the root document in a subdialog only if it is different from the calling root document
                      if (document == null) {
                        VXMLURLLoader loader = new VXMLURLLoader(page.rootURL());   // Throws MalformedURLException
                        InputSource source = new InputSource(new ByteArrayInputStream(loader.bytes())); // Throws VXMLEvent
                        source.setEncoding(DOMBrowser.getBrowserEncoding());
                        if (SysLog.shouldLog(2, SysLog.PREPROCESS_MASK))
                          SysLog.println("Session.preprocess: parsing the root document \"" + page.rootURL().toString() + "\"");
                        document = DOMBrowser.parse(source);                        // Throws VXMLEvent
                      }
                       // Preprocess the root document to construct a new Application in Session scope
                      if (SysLog.shouldLog(2, SysLog.PREPROCESS_MASK))
                        SysLog.println("Session.preprocess: preprocessing root document \"" + page.rootURL().toString() + "\"");
                      preprocess(document, session, context); // Throws VXMLEvent
                      context.root(document);
                    } else {
                       // The root document does not change and is not null
                      context.isPageLevel(true);
                    }
                    parent = (Application) session.child();  // Make the Application, which may have changed, the parent
                    ((Application) parent).rootURL(page.rootURL());   // Record the new root URL in the new Application
                    page.makeChildOf(parent);
                    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
                      SysLog.println("Session.preprocess: " + session + " > " + parent + " > " + page);
                     // Give all children Page (document) scope
                    newParent = (ScopeElement)page;

                   // Otherwise, if the parent is the session, construct a new Application (root Page) from this <vxml> element
                  } else if (parent instanceof Session) {
                    Application application = new Application((Element)node);
                    application.makeChildOf(parent);
                     // Give all children Application scope
                    newParent = (ScopeElement)application;

                  } else {
                    throw new RuntimeException("Session.preprocess: UNEXPECTED - parent of Document is neither Application nor Session");
                  }
                  break;
                }

              default:
                  break;
              }

             // Preprocess any children of non-Executables
            if (newParent != null) {
              NodeList children = node.getChildNodes();
              if (children != null) {
                int len = children.getLength();
                for (int i = 0; i < len; i++) {
                  preprocess(children.item(i), newParent, context);
                }
              }
            }
            break;
          }

         // Deal with entity reference nodes
        case Node.ENTITY_REFERENCE_NODE: {
            if (canonical) {
              NodeList children = node.getChildNodes();
              if ( children != null ) {
                int len = children.getLength();
                for ( int i = 0; i < len; i++ )
                  preprocess(children.item(i), parent, context);
              }
            } else if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK)) {
              SysLog.print("&" + node.getNodeName() + ";");
            }
            break;
          }

           // Preprocess cdata sections
        case Node.CDATA_SECTION_NODE: {
            if (canonical) {
              if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
                SysLog.print(normalize(node.getNodeValue()));
            } else {
              if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
                SysLog.print("<![CDATA[" + node.getNodeValue() + "]]>");
            }
            break;
          }

           // Preprocess text
        case Node.TEXT_NODE: {
            if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
              SysLog.print(normalize(node.getNodeValue()));
            break;
          }

           // Preprocess processing instruction
        case Node.PROCESSING_INSTRUCTION_NODE: {
            if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
              SysLog.print("<?" + node.getNodeName());
            String data = node.getNodeValue();
            if ( data != null && data.length() > 0 && SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
              SysLog.print(" " + data);
            if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
              SysLog.print("?>");
            break;
          }
      }
    } catch (DOMException e) {
      throw new VXMLEvent("error.xmlparser.dom", e);
    } catch (InvalidExpressionException e) {
      throw new VXMLEvent("error.syntactic.badexpression", e);
    } catch (InvalidTag e) {
      throw new VXMLEvent("error.missingattribute." + e.node.getNodeName(), e);
    } catch (MalformedURLException e) {
      throw new VXMLEvent("error.badfetch.malformedurl", e);
    } catch (NumberFormatException e) {
      throw new VXMLEvent("error.syntactic.numberformat", e);
    }

    if (type == Node.ELEMENT_NODE && SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PREPROCESS_MASK))
      SysLog.println("</" + node.getNodeName() + ">");

    preprocessLevel--;
    if (SysLog.shouldLog(2, SysLog.PREPROCESS_MASK)) SysLog.println("Session.preprocess: end [Level: " + preprocessLevel + "]");
  } // preprocess(Node, Scope, Goto)

   /* Returns a sorted list of attributes */
  protected Attr[] sortAttributes(NamedNodeMap attrs) {
    int len = (attrs != null) ? attrs.getLength() : 0;
    Attr array[] = new Attr[len];
    for ( int i = 0; i < len; i++ ) {
      array[i] = (Attr)attrs.item(i);
    }
    for (int i = 0; i < len - 1; i++) {
      String name  = array[i].getNodeName();
      int    index = i;
      for (int j = i + 1; j < len; j++) {
        String curName = array[j].getNodeName();
        if (curName.compareTo(name) < 0 ) {
          name  = curName;
          index = j;
        }
      }
      if ( index != i ) {
        Attr temp    = array[i];
        array[i]     = array[index];
        array[index] = temp;
      }
    }
    return (array);
  } // sortAttributes(NamedNodeMap):Attr[]

   /** Normalizes the given string */
  protected String normalize(String s) {
    StringBuffer str = new StringBuffer();
    int len = (s != null) ? s.length() : 0;
    for ( int i = 0; i < len; i++ ) {
      char ch = s.charAt(i);
      switch ( ch ) {
        case '<': {
            str.append("&lt;");
            break;
          }
        case '>': {
            str.append("&gt;");
            break;
          }
        case '&': {
            str.append("&amp;");
            break;
          }
        case '"': {
            str.append("&quot;");
            break;
          }
        case '\r':
        case '\n': {
            if ( canonical ) {
              str.append("&#");
              str.append(Integer.toString(ch));
              str.append(';');
              break;
            } // else, default append char
          }
        default: {
            str.append(ch);
          }
      }
    }
    return (str.toString());
  } // normalize(String):String

   /** Interpret preprocessed VXML
     * This traverses the Application tree built by preprocess(), using the Element references into the DOM tree to get at the Elements not preprocessed */
  public Variables interpret(Goto whereToGo, InterruptHandler interruptHandler) throws VXMLEvent, VXMLExit, VXMLReturn {
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.INTERPRET_MASK))
      SysLog.println("Session.interpret: starting interpretation");
    try {
      interruptHandler.pushScope(this);
      interruptHandler.enableEvents();
      if (!initialized) {
        this.initializeVariables();     // Throws VXMLEvent
        this.executeScript();           // Throws VXMLEvent
        initialized = true;
      } else {
        SysLog.println("Session.interpret: UNEXPECTED re-initialization");
      }
      if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.VARIABLES_MASK))
        SysLog.println("Session.interpret: variables = " + (String)(scope != null ? scope.toString() : "<none>"));
      while (whereToGo.page() != null) {
        try {
          try {
             // Interpret the preprocessed document
            ((Application)child).interpret(whereToGo, interruptHandler);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
          } catch (VXMLEvent event) {
             // Catch a VXML event in Session scope
            if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.CATCH_MASK))
              SysLog.println("Session.interpret: caught VXMLEvent \"" + event.name() + "\"");
            catchEvent(event, whereToGo);                                 // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
            throw new VXMLExit("Session-level catcher for event \'" + event.name() + "\' falls through");
          } 
        } catch (Goto gt) {
          whereToGo = gt;
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK))
            SysLog.println("Session.interpret: Application level Goto caught for \"" + whereToGo.next() + "\"");
        }
      }
    } finally {
      interruptHandler.disableEvents();
      interruptHandler.popScope();
    }
    return null;
  }
  
  public Recognizer getRecognizer() {
	  return this.recognizer;
  }
  
  public SystemVariables getSystemVariables() {
	  return this.systemVariables;
  }
  
  public InterruptHandler getInterruptHandler() {
	  return this.interruptHandler;
  }

public AudioPlayer getAudioPlayer() {
	return this.audioPlayer;
}

public Cache getSpeakerCache() {
	return this.speakerCache;
}

} // class Session
