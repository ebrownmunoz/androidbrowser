package voxware.browser;

import java.io.*;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;
import java.util.NoSuchElementException;

import javax.speech.*;
import javax.speech.recognition.*;

import voxware.engine.recognition.*;
import voxware.engine.recognition.sapivise.*;
import voxware.engine.recognition.keypad.*;
import voxware.engine.recognition.mock.FileMockSpeaker;
import voxware.engine.recognition.mock.InteractiveMockSpeaker;
import voxware.engine.recognition.mock.MockSpeaker;
import voxware.engine.audioplayer.*;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.hw.event.HwControls;
import voxware.textbrowser.SapiTextRecognizer;
import voxware.util.*;

 /** A Recognizer */
public class Recognizer extends javax.speech.recognition.ResultAdapter {

	// Constants

	public static final int    DEFAULT_NHYPOTHESES = 1;
	public static final int    DEFAULT_SILMAXDWELL = 384;
	public static final int    DEFAULT_ENDMINDWELL = 448;
	public static final float  DEFAULT_INPUT_LEVEL = (float)22.5;

	public static final String  DEFAULT_SPEAKER_ROOT = "/ram";
	public static final String  DEFAULT_SPEAKER_PATH = "speakers";
	// Set this to true if VU reporting should be ON by default
	public static final boolean DEFAULT_VUONOFFSWCH = false;

	public static final long LongWait = 300000L;
	public static final long ShortWait = 60000L;
	private static final String DEFAULT_MOCK_FILE_NAME = null;

	private MockSpeaker mockSpeaker = null;


	// Set this true in environments that support speech recognition
	private boolean  speechEnabled = false;

	// Set this true in environments that support keypad recognition
	private  boolean  keypadEnabled = false;

	// Set this true in environments that support interrupts
	private boolean  interruptsEnabled = false;


	/** Enable/disable speech recognition */
	public Recognizer(InterruptHandler interruptHandler, boolean enable, String mode) {
		speechEnabled = enable;
		this.mode = mode;
		this.interruptHandler = interruptHandler;
		construct();
	}

	/** Enable/disable interrupt handling */
	public void enableInterrupts(boolean enable) {
		interruptsEnabled = enable;

	}

	/** Return true iff the speech recognizer is enabled */
	public boolean speechEnabled() {
		return speechEnabled;
	}

	/** Return true iff the keypad recognizer is enabled */
	public boolean keypadEnabled() {
		return keypadEnabled;
	}

	/** Return true iff interrupts are enabled */
	public boolean interruptsEnabled() {
		return interruptsEnabled;
	}

	/** Set the repository of speaker profiles to the specified path, creating it if necessary, and report whether successful */
	public boolean speakerDirectory(String pathName) {
		// Make the File
		java.io.File speakers = new java.io.File(pathName);
		if (!speakers.isAbsolute()) speakers = new java.io.File(DEFAULT_SPEAKER_ROOT, pathName);
		// Make the speaker repository if it doesn't exist
		if (speakers.exists() || speakers.mkdirs()) {
			// Stuff it into the recognizer, first constructing the recognizer if necessary
			this.speakers(speakers);
			return true;
		} else {
			SysLog.println("Recognizer.speakerDirectory: ERROR - mkdirs() fails for \"" + speakers.getAbsolutePath() + "\"");
			return false;
		}
	}

	/** Return the repository of speaker profiles */
	public String speakerDirectory() {
		return speakers().getAbsolutePath();
	}

	/** Register a recognition engine with the system */
	public static boolean register(String className) {
		try {
			Central.registerEngineCentral(className);
		} catch (EngineException e) {
			return false;
		}
		return true;
	}

	// A holder for (javax.speech.recognition.Grammar, ruleName) pairs
	private static class GrammarRulePair {

		private javax.speech.recognition.Grammar        grammar;
		private String                                  ruleName;

		GrammarRulePair(javax.speech.recognition.Grammar grammar, String ruleName) {
			this.grammar = grammar;
			this.ruleName = ruleName;
		}
	}

	// A Timer to implement timeouts when no speech recognizer is running
	private static class Timer implements Runnable {

		private voxware.util.Mailbox                    mailbox;
		private HashSet                                 listeners;
		private long                                    timeout;

		Timer() {
			mailbox = new Mailbox();
			listeners = new HashSet();
			timeout = 0L;
			(new Thread(this, "RecognitionTimer")).start();
		}

		public void run() {
			while (true) {
				try {
					Object message = mailbox.remove(timeout);
					if (message == null) {
						synchronized (listeners) {
							Iterator elements = listeners.iterator();
							while (elements.hasNext()) {
								Object element = elements.next();
								if (element instanceof ResultListener) ((ResultListener)element).resultRejected(null);
							}
						}
						timeout = 0L;
						if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.Timer.run: REJECTED result due to timeout");
					} else if (message instanceof Long) {
						timeout = ((Long)message).longValue();
						if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.Timer.run: set timeout to " + timeout);
					} else {
						timeout = 0L;
						SysLog.println("Recognizer.Timer.run: ignoring UNEXPECTED message");
					}
				} catch (InterruptedException ie) {
					break;
				}
			}
		}

		public void setTimeout(int timeout) {
			mailbox.insert(new Long((long)timeout));
		}

		public void addResultListener(ResultListener listener) {
			synchronized (listeners) { listeners.add(listener); }
		}

		public void removeResultListener(ResultListener listener) {
			synchronized (listeners) { listeners.remove(listener); }
		}

	}

	// Fields

	private voxware.browser.Grammar.Wrapper           currentVoiceWrapper;          // The Grammar.Wrapper containing the currently loaded voice grammar
	private ObjectSet                                 currentDTMFWrappers;          // The set of Grammar.Wrappers for the currently loaded DTMF grammars
	private ObjectSet                                 currentGrammarRules;          // The set of GrammarRulePairs for the currently loaded grammars

	private boolean                                   usingSpeech;
	private boolean                                   usingKeypad;
	private boolean                                   usingTimer;
	private String                                    mode = "voice";               // The speech recognition mode ("voice" or "text")

	private boolean                                   active;
	private boolean                                   awaitingResult;
	private javax.speech.recognition.FinalRuleResult  result;

	private javax.speech.recognition.Recognizer       speechRecognizer = null;
	private javax.speech.recognition.Recognizer       keypadRecognizer = null;
	private KeypadHandler                             keypadHandler = null;
	private final InterruptHandler                          interruptHandler;
	private Timer                                     timer = null;                 // Timer to use when no speech recognizer is in use

	private int                                       recognitionTimeout;

	private Vector                                    prompts = new Vector(10);     // The queue of associated prompts

	private java.io.File  speakers = new java.io.File(DEFAULT_SPEAKER_ROOT, DEFAULT_SPEAKER_PATH);  // The repository of speaker profiles




	// Public Methods

	/** Set the repository of speaker profiles */
	void speakers(java.io.File speakers) {
		this.speakers = speakers;
	}

	/** Return the repository of speaker profiles */
	java.io.File speakers() {
		return speakers;
	}

	/** Add a prompt to the prompt queue */
	public synchronized void addPrompt(Prompt prompt) throws VXMLEvent {
		// If the recognizer is set up, and this is the first prompt associated with it, and it allows barge-in, activate the recognizer
		if (awaitingResult && !active && prompts.isEmpty() && prompt.bargein()) activate(0, prompt.getParent());  // Throws VXMLEvent
		prompts.addElement(prompt);
	}

	/** Load all of a ScopeElement's grammars */
	private void loadGrammars(ScopeElement scope) throws VXMLEvent {
		usingSpeech = usingKeypad = false;                                            // Set in loadGrammar()
		currentGrammarRules.clear();
		ObjectSet grammars = scope.grammars();
		if (grammars != null) {
			Enumeration grammarEnum = grammars.iterator();
			while (grammarEnum.hasMoreElements()) {
				voxware.browser.Grammar grammar = (voxware.browser.Grammar)grammarEnum.nextElement();
				// Load all of the DTMF grammars
				if (grammar.modeIsDTMF() && keypadEnabled && !((Boolean)BrowserProperty.value(BrowserProperty.SUPPRESSKEYPADINPUT_INDEX)).booleanValue()) {
					loadGrammar(grammar, scope);
					// Load only the first voice grammar
				} else if (grammar.modeIsVoice() && speechEnabled && !((Boolean)BrowserProperty.value(BrowserProperty.SUPPRESSSPEECHINPUT_INDEX)).booleanValue()) {
					if (!usingSpeech) {
						loadGrammar(grammar, scope);
					} else if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK)) {
						SysLog.println("Recognizer.loadGrammar: ignoring a voice grammar other than the first");
					}
					// Complain about any unknown grammar mode
				} else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK)) {
					SysLog.println("Recognizer.loadGrammar: ignoring grammar \"" + StringLiteralSupport.stringLiteralFromUnicode(grammar.name()) + "\"");
				}
			}
		}
		usingTimer = !usingSpeech && !usingKeypad;
	}

	/** Load the specified grammar in the given scope */
	public javax.speech.recognition.Grammar loadGrammar(voxware.browser.Grammar grammar, Scope scope) throws VXMLEvent {
		javax.speech.recognition.Grammar loadedGrammar = null;
		if (grammar != null) {
			voxware.browser.Grammar.Wrapper wrapper = grammar.wrapper(scope);           // Throws VXMLEvent
			int type = wrapper.type();
			if (grammar.modeIsDTMF()) {
				if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
					SysLog.println("Recognizer.loadGrammar: grammar \"" + StringLiteralSupport.stringLiteralFromUnicode(grammar.name()) + "\" is a DTMF grammar");
				// Make sure the keypad recognizer exists
				if (keypadRecognizer == null) {
					if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
						SysLog.println("Recognizer.loadGrammar: keypad recognizer not available");
					throw new VXMLEvent("error.engine.unsupported", "keypad recognizer not available");
					// Load the DTMF recognition grammar if it's not already loaded
				} else if (!currentDTMFWrappers.contains(wrapper)) {
					if (type == voxware.browser.Grammar.JSGFType) {
						try {
							InputStream stream =
									(wrapper.content() instanceof InputStream ? (InputStream)wrapper.content() : new ByteArrayInputStream((byte[])wrapper.content()));
							loadedGrammar = ((BaseRecognizer)keypadRecognizer).loadJSGF(stream); // Throws GrammarException, IOException, EngineStateError
						} catch (IOException e) {
							throw new RuntimeException("Recognizer.loadGrammar: UNEXPECTED I/O error reading DTMF grammar \"" +
									StringLiteralSupport.stringLiteralFromUnicode(grammar.name()) + "\": " + e.toString());
						} catch (GrammarException e) {
							throw new VXMLEvent("error.grammar.badimage", e);
						} catch (EngineStateError e) {
							throw new RuntimeException("Recognizer.loadGrammar: UNEXPECTED " + e.toString());
						}
						if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.loadGrammar: loaded DTMF grammar \"" + StringLiteralSupport.stringLiteralFromUnicode(grammar.name()) + "\" into the keypad recognizer");
					} else if (type == voxware.browser.Grammar.JSGFCType) {
						try {
							loadedGrammar = keypadRecognizer.readVendorGrammar(new ByteArrayInputStream((byte[])wrapper.content()));
						} catch (IOException e) {
							throw new RuntimeException("Recognizer.loadGrammar: UNEXPECTED I/O error reading grammar \"" +
									StringLiteralSupport.stringLiteralFromUnicode(grammar.name()) + "\": " + e.toString());
						} catch (VendorDataException e) {
							throw new VXMLEvent("error.grammar.badimage", e);
						} catch (EngineStateError e) {
							throw new RuntimeException("Recognizer.loadGrammar: UNEXPECTED " + e.toString());
						}
						if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.loadGrammar: loaded DTMF grammar image \"" + StringLiteralSupport.stringLiteralFromUnicode(grammar.file()) + "\" into the keypad recognizer");
					} else {
						String message = "dtmf grammar of type \"" + voxware.browser.Grammar.extension(type) + "\" not supported";
						if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.loadGrammar: " + message);
						throw new VXMLEvent("error.grammar.unsupported", message);
					}
					currentDTMFWrappers.add(wrapper);
				} else {
					if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
						SysLog.println("Recognizer.loadGrammar: reusing old DTMF grammar \"" + StringLiteralSupport.stringLiteralFromUnicode(wrapper.name()) + "\"");
					loadedGrammar = keypadRecognizer.getRuleGrammar(wrapper.name());
				}
				currentGrammarRules.add(new GrammarRulePair(loadedGrammar, grammar.rule()));
				usingKeypad = true;
			} else if (grammar.modeIsVoice()) {
				if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
					SysLog.println("Recognizer.loadGrammar: grammar \"" + StringLiteralSupport.stringLiteralFromUnicode(grammar.name()) + "\" is a voice grammar");
				// Make sure the speech recognizer exists
				if (speechRecognizer == null) {
					if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
						SysLog.println("Recognizer.loadGrammar: speech recognizer not available");
					throw new VXMLEvent("error.engine.unsupported", "speech recognizer not available");
					// Load the VISE recognition grammar image if it's not already loaded
				} else if (currentVoiceWrapper == null || !currentVoiceWrapper.equals(wrapper)) {
					if (type == voxware.browser.Grammar.VISECType) {
						if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.loadGrammar: loading voice grammar image \"" + StringLiteralSupport.stringLiteralFromUnicode(grammar.file()) + "\" into the speech recognizer");
						try {
							if ((speechRecognizer instanceof SapiTextRecognizer)) {
								((SapiTextRecognizer)speechRecognizer).setMockSpeakerInGrammar(mockSpeaker);
							}
							loadedGrammar = speechRecognizer.readVendorGrammar(new ByteArrayInputStream((byte[])wrapper.content()));
						} catch (IOException e) {
							throw new RuntimeException("Recognizer.loadGrammar: UNEXPECTED I/O error reading grammar \"" +
									StringLiteralSupport.stringLiteralFromUnicode(grammar.name()) + "\": " + e.toString());
						} catch (VendorDataException e) {
							throw new VXMLEvent("error.grammar.badimage", e);
						} catch (EngineStateError e) {
							throw new RuntimeException("Recognizer.loadGrammar: UNEXPECTED " + e.toString());
						}
						if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.loadGrammar: loaded voice grammar image \"" + StringLiteralSupport.stringLiteralFromUnicode(grammar.file()) + "\" into the speech recognizer");
					} else {
						String message = "voice grammar of type \"" + voxware.browser.Grammar.extension(type) + "\" not supported";
						if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.loadGrammar: " + message);
						throw new VXMLEvent("error.grammar.unsupported", message);
					}
					currentVoiceWrapper = wrapper;
				} else {
					if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
						SysLog.println("Recognizer.loadGrammar: reusing old voice grammar \"" + StringLiteralSupport.stringLiteralFromUnicode(wrapper.name()) + "\"");
					loadedGrammar = speechRecognizer.getRuleGrammar(wrapper.name());
				}
				String ruleName = grammar.rule();
				if (ruleName == null && loadedGrammar instanceof SapiViseGrammar)
					ruleName = ((SapiViseGrammar)loadedGrammar).getDefaultRuleName();
				currentGrammarRules.add(new GrammarRulePair(loadedGrammar, ruleName));
				usingSpeech = true;
			} else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK)) {
				SysLog.println("Recognizer.loadGrammar: ignoring grammar \"" + StringLiteralSupport.stringLiteralFromUnicode(grammar.name()) + "\" because its mode is unknown");
			}
			// Make sure the grammar name corresponds to what the recognizer thinks it is (which may differ from grammar.name())
			wrapper.name(loadedGrammar.getName());
		}
		return loadedGrammar;
	}

	/** Recognize using the specified grammars, optionally playing the associated prompts */
	public void recognize(ScopeElement scope, boolean prompt) throws InterruptedException, VXMLEvent {

		//GarbageCollector.getInstance().collect(((Number)BrowserProperty.value(BrowserProperty.JAVAGCTHRESHOLD_INDEX)).intValue(),
		//                                       ((Number)BrowserProperty.value(BrowserProperty.MINBYTESFREE_INDEX)).longValue());

		// Set up the recognizer
		synchronized(this) {

			// Load all the ScopeElement's grammars
			loadGrammars(scope);                        // Throws VXMLEvent

			// Make sure the speaker profile is up-to-date
			if (usingSpeech) {
				SpeakerProfile profile;
				Speaker speaker = scope.speaker();
				if (speaker == null || (profile = speaker.profile(scope)) == null)
					throw new VXMLEvent("error.vise.speaker.nospeaker");
				try {
					if (speechRecognizer instanceof SapiVise) {
						ViseProperties props = (ViseProperties)speechRecognizer.getRecognizerProperties();
						props.setSpeakerPath(speakers.getAbsolutePath());         // Throws PropertyVetoException
						props.setMaxDuration(0);
					}
					// Set the current speaker to the specified speaker
					SpeakerManager sm = speechRecognizer.getSpeakerManager();   // Throws SecurityException
					if (!profile.match(sm.getCurrentSpeaker())) {
						sm.setCurrentSpeaker(profile);
						if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.recognize: changed current speaker to \"" + StringLiteralSupport.stringLiteralFromUnicode(speaker.name()) + "\"");
					} else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK)) {
						SysLog.println("Recognizer.recognize: retained current speaker \"" + StringLiteralSupport.stringLiteralFromUnicode(speaker.name()) + "\"");
					}
				} catch (PropertyVetoException e) {
					throw new VXMLEvent("error.vise.badproperty", e);
				} catch (SecurityException e) {
					throw new VXMLEvent("error.vise.speaker.nomanager", e);
				} catch (IllegalArgumentException e) {
					throw new VXMLEvent("error.vise.speaker.badprofile", e);
				}
			}

			awaitingResult = true;

			// Add the listeners to get results
			if (SysLog.printLevel > 4 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
				SysLog.println("Recognizer.recognize: adding result listeners");
			if (interruptHandler != null) interruptHandler.addResultListener(this);
			if (speechRecognizer != null) speechRecognizer.addResultListener(this);
			if (keypadRecognizer != null) keypadRecognizer.addResultListener(this);
			if (usingTimer)               timer.addResultListener(this);
		}

		// Queue the prompts
		try {
			// If there is already a prompt associated with this recognition, and it allows barge-in, activate the recognizer with no timeout
			synchronized(this) {
				if (!prompts.isEmpty() && ((Prompt)prompts.firstElement()).bargein()) activate(0, scope);  // Throws VXMLEvent
			}
			// Conditionally queue the prompts for playing (UNSYNCHRONIZED so that callbacks and result notification can occur)
			if (prompt) scope.prompts().play(this);               // Throws VXMLEvent
			// If a VXMLEvent occurs, catch it, tear down the recognizer and re-throw the event
		} catch(VXMLEvent e) {
			teardown();
			throw e;
		}

		// Boost priority and await results
		Thread ego = Thread.currentThread();
		int priority = ego.getPriority();
		ego.setPriority(Thread.MAX_PRIORITY);
		synchronized(this) {
			try {
				// Activate the recognizer with the default timeout if it is inactive and there are no prompts to activate it
				if (!active && prompts.isEmpty())
					activate(((Integer)BrowserProperty.value(BrowserProperty.TIMEOUT_INDEX)).intValue(), scope);  // Throws VXMLEvent
				// Listen
				if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK)) SysLog.println("Recognizer.recognize: awaiting result");
				while (awaitingResult) {
					long wait = 0L;
					if (awaitingResult) {
						if (SysLog.shouldLog(1, SysLog.RECOGNIZER_MASK)) {
							wait = SysLog.shouldLog(2) ? ShortWait : LongWait;
							if (wait <= recognitionTimeout) wait = (long) (1.1 * recognitionTimeout);
							SystemMemory m = new SystemMemory();
							Runtime runtime = Runtime.getRuntime();
							SysLog.println("Recognizer.recognize: awaiting result [ available heap: " + runtime.freeMemory() + "/" + runtime.totalMemory() +
									" (" + (runtime.freeMemory() * 100) / runtime.totalMemory() + "%), available memory: " + m.bytesFree() + " bytes ]");
						}
					}
					wait(wait);                                       // Throws InterruptedException
				}
			} catch (EngineStateError e) {
				throw new RuntimeException("Recognizer.recognize: UNEXPECTED " + e.toString());
			} finally {
				teardown();
				// Resume normal priority
				ego.setPriority(priority);
			}
		}
	}

	/** Receive a RESULT_ACCEPTED event */
	public synchronized void resultAccepted(ResultEvent event) throws IllegalArgumentException {
		awaitingResult = false;
		FinalRuleResult acceptedResult = (FinalRuleResult)(event.getSource());
		if (result == null || result.getResultState() != Result.ACCEPTED) {
			result = acceptedResult;
			SysLog.println("Recognizer.resultAccepted: accepting \"" + rawText(result) + "\"");
		} else {
			SysLog.println("Recognizer.resultAccepted: ignoring \"" + rawText(acceptedResult) + "\"");
		}
		notify();
		if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
			SysLog.println("Recognizer.resultAccepted: notification sent");
	}

	/** Receive a RESULT_REJECTED event */
	public synchronized void resultRejected(ResultEvent event) {
		awaitingResult = false;
		FinalRuleResult rejectedResult = (event != null) ? (FinalRuleResult)(event.getSource()) : null;
		if (result == null) {
			result = rejectedResult;
			if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
				SysLog.println("Recognizer.resultRejected");
		} else {
			if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
				SysLog.println("Recognizer.resultRejected: ignoring");
		}
		notify();
		if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
			SysLog.println("Recognizer.resultRejected: notification sent");
	}

	/** Return the raw recognition results object */
	public synchronized javax.speech.recognition.FinalRuleResult rawResult() {
		return result;
	}

	/** Clear the recognition results object */
	public synchronized void clearResult() {
		result = null;
	}

	/** Return a Variables object containing the results -- we expect the grammar
	 * to return a result of the form name1=value1&name2=value2&..., where nameN
	 * is the name of a Variable and valueN is the value to assign to it. ValueN
	 * must be a valid ECMAScript String. A result other than a valid ECMAScript
	 * String will cause this method to throw an InvalidExpressionException; a
	 * null result causes it to throw "event.noinput". */
	public synchronized Variables results(Scope scope, String name, Scriptable shadow, Goto context)
			throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent, Goto
	{
		Variables variables = null;
		try {
			if (result != null) {
				String resultString = null;
				if (result instanceof InterruptResult) {
					Interrupt interrupt = ((InterruptResult)result).interrupt();
					Object message = interrupt.template.message();
					resultString = message.toString();
					if (shadow != null) {
						String source = "interrupt." + SystemEvent.name(interrupt.template.index);
						if (message instanceof SourcedInterrupt) {
							String subsource = ((SourcedInterrupt) message).source;
							if (subsource != null) source = source + "." + subsource;
						}
						shadow.put("inputmode", shadow, source);
						shadow.put("utterance", shadow, resultString);
						shadow.put("translation", shadow, resultString);
					}
					Link link = interrupt.link();
					// If there is a link, take it
					if (link != null) {
						VXMLEvent event = link.targetEvent();
						Goto next = link.targetGoto();
						if (event != null) {
							if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
								SysLog.println("Recognizer.results: InterruptResult link throwing VXMLEvent \"" + event.name() + "\" containing \"" +
										StringLiteralSupport.stringLiteralFromUnicode(resultString) + "\"");
							throw new VXMLEvent(event.name(), resultString);
						} else if (next != null) {
							next.acquireTarget((ScopeElement)scope, context);        // Throws VXMLEvent("error.badfetch")
							if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
								SysLog.println("Recognizer.results: InterruptResult link throwing Goto");
							throw next;
						} else {
							if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
								SysLog.println("Recognizer.results: InterruptResult ERROR - bad Link");
							throw new VXMLEvent("error.link.noaction");
						}
						// Otherwise, parse the interrupt message string into its constituent name=value pairs
					} else if (resultString != null) {
						// Make sure that an empty result string is treated as a valid result
						if (resultString.length() == 0) resultString = name + "=&";
						if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.results: InterruptResult is \"" + resultString + "\"");
						// Return a Variables object constructed from the name=value pairs
						variables = new Variables(resultString, "&", '=', scope, name, true);  // Throws InvalidExpressionException, UndeclaredVariableException
					} else {
						if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.results: InterruptResult is empty");
						variables = new Variables(1);
					}
					((ScopeElement)scope).logEvent(SystemEvent.RECOGNITION_INDEX, resultString);
				} else {
					String rawText = rawText(result);
					Object pathScore;
					if (result instanceof ViseResult) {
						resultString = ((ViseResult)result).hostTrans();
						try { pathScore = VXMLTypes.intToNumber(((ViseResult)result).pathScore()); }
						catch (ResultStateError rse) { pathScore = VXMLTypes.PROTO_UNDEFINED; }
					} else {
						resultString = tagText(result);
						if (resultString == null) resultString = rawText;
						pathScore = VXMLTypes.PROTO_UNDEFINED;
					}
					if (result.getResultState() == Result.ACCEPTED && resultString != null) {
						// Make sure that an empty result string is treated as a valid result
						if (resultString.length() == 0) resultString = name + "=&";
						if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.results: Recognition result is \"" + resultString + "\"");
						// Return a Variables object constructed from the name=value pairs
						variables = new Variables(resultString, "&", '=', scope, name, true);  // Throws InvalidExpressionException, UndeclaredVariableException
					}
					String inputMode = ((result instanceof ViseResult) || (result instanceof TextResult)) ? "voice" : "dtmf";
					int nbest = result.getNumberGuesses();
					Scriptable applicationScope = (Scriptable) scope.name("application");
					Scriptable[] lastResultsArray = new Scriptable[nbest];
					for (int resultIndex = 0; resultIndex < nbest; resultIndex++) {
						ResultToken[] resultTokens = result.getAlternativeTokens(resultIndex);
						Scriptable lastResult = Scope.newObject(applicationScope);
						lastResult.put("utterance", lastResult, rawText(resultTokens));
						lastResult.put("inputmode", lastResult, inputMode);
						lastResult.put("confidence", lastResult, resultIndex == 0 ? pathScore : VXMLTypes.PROTO_UNDEFINED);
						lastResultsArray[resultIndex] = lastResult;
					}
					Scriptable lastResults = Scope.newArray(applicationScope, lastResultsArray);
					lastResults.put("utterance", lastResults, rawText);
					lastResults.put("inputmode", lastResults, inputMode);
					lastResults.put("confidence", lastResults, pathScore);
					scope.setProperty("application.lastresult$", lastResults);
					if (shadow != null) {
						shadow.put("utterance", shadow, rawText);
						shadow.put("inputmode", shadow, inputMode);
						shadow.put("confidence", shadow, pathScore);
						shadow.put("translation", shadow, resultString);
						RecognizerProperties properties = (RecognizerProperties)(speechRecognizer).getRecognizerProperties();
						// If there is training data and the result contains tokens, save the entire result object in a shadow property
						if (properties.isTrainingProvided() && result.numTokens() > 0) {
							shadow.put("result", shadow, result);
						} else {
							if (properties.isTrainingProvided()) ((FinalResult)result).releaseTrainingInfo();
							shadow.put("result", shadow, VXMLTypes.PROTO_UNDEFINED);
						}
						// If there is audio data, create an audio object, make it a property of the shadow variable and save the data in it
						if (properties.isResultAudioProvided()) {
							byte[] audioData = ((ViseResult)result).getWAV();
							((ViseResult)result).releaseAudio();
							if (audioData != null) {
								if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
									SysLog.println("Recognizer.results: returned from getWAV with " + audioData.length + " bytes");
								Scriptable audio = scope.newObject();
								shadow.put("audio", shadow, audio);
								audio.put("data", audio, audioData);
								NISTHeader header = new NISTHeader();
								Integer sampleRate = null;
								Integer sampleCount = null;
								try {
									if (header.read(new ByteArrayInputStream(audioData))) {
										NISTHeaderField field;
										field = (NISTHeaderField)header.get("sample_rate");
										if (field != null) sampleRate = (Integer)field.data();
										field = (NISTHeaderField)header.get("sample_count");
										if (field != null) sampleCount = (Integer)field.data();
									}
								} catch (IOException e) {
									throw new RuntimeException("Recognizer.results: UNEXPECTED - " + e.toString());
								}
								Object duration;
								if (sampleRate != null && sampleRate.intValue() > 0 && sampleCount != null)
									duration = new Integer((sampleCount.intValue() * 1000) / sampleRate.intValue());
								else
									duration = VXMLTypes.PROTO_UNDEFINED;
								audio.put("duration", audio, duration);
								audio.put("size", audio, new Integer(audioData.length));
							} else {
								shadow.put("audio", shadow, VXMLTypes.PROTO_UNDEFINED);
								if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
									SysLog.println("Recognizer.results: returned from getWAV with no bytes");
							}
						} else {
							shadow.put("audio", shadow, VXMLTypes.PROTO_UNDEFINED);
							if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
								SysLog.println("Recognizer.results: no result audio provided");
						}
					}
					((ScopeElement)scope).logEvent(SystemEvent.RECOGNITION_INDEX, resultString);
				}
			} else {
				if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
					SysLog.println("Recognizer.results: result is null");
				if (shadow != null) {
					shadow.put("inputmode", shadow, VXMLTypes.PROTO_UNDEFINED);
					shadow.put("utterance", shadow, VXMLTypes.PROTO_UNDEFINED);
					shadow.put("translation", shadow, VXMLTypes.PROTO_UNDEFINED);
				}
			}
			if (variables == null) {
				((ScopeElement)scope).logEvent(SystemEvent.RECOGNITION_INDEX, rawText(result));  // Log the "no-hear"
				throw new VXMLEvent("event.noinput");
			}
		} finally {
			// Flush the result
			result = null;
		}
		return variables;
	}

	/** When a prompt has finished playing out, it calls this unsynchronized wrapper */
	public void callback(Prompt prompt) throws VXMLEvent {
		// Ignore any callback that comes in after the bell (AND DO NOT CALL THE SYNCHRONIZED METHOD!)
		if (!prompts.isEmpty()) processCallback(prompt);  // Throws VXMLEvent
	}

	public float getInputGain() {
		if (speechRecognizer != null && speechRecognizer instanceof SapiVise) {
			float gain = ((SapiVise)speechRecognizer).levelGet();
			if (!Float.isNaN(gain))
				return gain;
		}
		return HwControls.getInstance().getMicGain();
	}

	public void setInputGain(float gain) {
		if (speechRecognizer != null && speechRecognizer instanceof SapiVise)
			if (((SapiVise)speechRecognizer).levelSet(gain))
				return;

		HwControls.getInstance().setMicGain(gain);
	}

	// Private Methods


	/** Instantiate the speech recognizer, the keypad recognizer and the InterruptHandler */
	private synchronized void construct() {

		// Instantiate the Timer
		if (timer == null) timer = new Timer();

		if (currentGrammarRules == null && (speechEnabled || keypadEnabled))
			currentGrammarRules = new ObjectSet();

		// Instantiate the speech recognizer if it is enabled
		if (speechRecognizer == null && speechEnabled) {
			try {
				//speechRecognizer = Central.createRecognizer(new EngineModeDesc("VISE", this.mode, Locale.US, null));
				speechRecognizer = new SapiTextRecognizer();
				if (speechRecognizer == null) SysLog.println("Recognizer.construct: WARNING - cannot find the SAPIVISE Recognizer");
				speechRecognizer.allocate();
				speechRecognizer.waitEngineState(javax.speech.Engine.ALLOCATED);
				speechRecognizer.requestFocus();
				speechRecognizer.waitEngineState(javax.speech.recognition.Recognizer.FOCUS_ON);
				if (speechRecognizer instanceof SapiVise) {
					// Set any defaults that differ from the SAPI defaults
					ViseProperties properties = (ViseProperties)speechRecognizer.getRecognizerProperties();
					try {
						properties.setNHypotheses(DEFAULT_NHYPOTHESES);
						properties.setSilMaxDwell(DEFAULT_SILMAXDWELL);
						properties.setEndMinDwell(DEFAULT_ENDMINDWELL);
						properties.setVUOnOffSwitch(DEFAULT_VUONOFFSWCH);
						setInputGain(DEFAULT_INPUT_LEVEL);
					} catch (PropertyVetoException e) {
						throw new RuntimeException("Recognizer.construct: UNEXPECTED " + e.toString());
					}
				}
				if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
					SysLog.println("Recognizer.construct: instantiated the unique JSAPI Speech Recognizer");
			} catch (Exception e) {
				SysLog.println("Recognizer.construct: WARNING - JSAPI speech recognizer NOT instantiated");
				speechRecognizer = null;
				speechEnabled = false;
			}
			// Make the speaker repository if it doesn't exist
			speakers.mkdirs();
		} else if (!speechEnabled) {
			speechRecognizer = null;
		}

		// Restore the speaker path, in the event that an applet has trashed it
		try {
			if (speechRecognizer != null && speechRecognizer instanceof SapiVise)
				((ViseProperties)speechRecognizer.getRecognizerProperties()).setSpeakerPath(speakers.getAbsolutePath());
		} catch (PropertyVetoException pve) {
			throw new RuntimeException("Recognizer.construct: UNEXPECTED " + pve.toString());
		}

		// Instantiate the keypad recognizer if it is enabled
		if (keypadRecognizer == null && keypadEnabled) {
			keypadHandler = KeypadHandler.getInstance();
			try {
				keypadRecognizer = Central.createRecognizer(new EngineModeDesc("VICE", "dtmf", null, null));
				if (keypadRecognizer == null) SysLog.println("Recognizer.construct: WARNING - cannot find the JSAPI Keypad Recognizer");
				keypadRecognizer.allocate();
				keypadRecognizer.waitEngineState(javax.speech.Engine.ALLOCATED);
				keypadHandler.addTokenListener((TokenListener)keypadRecognizer);
				keypadRecognizer.requestFocus();
				keypadRecognizer.waitEngineState(javax.speech.recognition.Recognizer.FOCUS_ON);
				if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
					SysLog.println("Recognizer.construct: instantiated the unique JSAPI Keypad Recognizer");
				currentDTMFWrappers = new ObjectSet();
			} catch (Exception e) {
				SysLog.println("Recognizer.construct: WARNING - JSAPI keypad recognizer NOT instantiated");
				keypadRecognizer = null;
				keypadEnabled = false;
			}
		} else if (!keypadEnabled) {
			keypadHandler = null;
		}
	}

	/** Activate or deactivate the recognizer and adjust the recognition timeout according to the Prompt specifications */
	private synchronized void processCallback(Prompt prompt) throws VXMLEvent {

		if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
			SysLog.println("Recognizer.callback: prompt " + prompt.toString() + " has finished");

		// Make sure there is something to process
		if (!prompts.isEmpty()) {
			int index = prompts.indexOf(prompt);
			// Sanity check (the prompt calling back must be the first prompt in the prompts queue)
			if (index != 0 && SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
				SysLog.println("Recognizer.callback: UNEXPECTED prompt mismatch (index = " + index + ")");

			// Remove the element for the prompt that just finished (and thus invoked this callback)
			if (index >= 0)
				prompts.removeElementAt(index);

			// Do something only if the recognizer is set up to recognize
			if (awaitingResult) {
				int timeout = prompt.timeout();
				if (timeout < 0) timeout = ((Integer)BrowserProperty.value(BrowserProperty.TIMEOUT_INDEX)).intValue();

				Prompt nextPrompt;
				// Try to get the next prompt associated with this recognition, if any
				try {
					nextPrompt = (Prompt)prompts.firstElement();
					// If the next prompt has no timeout attribute, propogate the current timeout
					if (nextPrompt.timeout() < 0)
						nextPrompt.timeout(timeout);
					// If the next prompt allows barge-in, make sure that recognition is active with no timeout
					if (nextPrompt.bargein()) {
						if (active) setTimeout(0);
						else activate(0, prompt.getParent());
						// Otherwise, since the next prompt does not allow barge-in, deactivate the recognizer
					} else {
						deactivate();
					}

					// There are no more prompts associated with this recognition
				} catch (NoSuchElementException e) {
					if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
						SysLog.println("Recognizer.callback: recognition timeout = " + timeout + " milliseconds");
					// Make sure the recognizer is active with the current timeout
					if (active) setTimeout(timeout);
					else activate(timeout, prompt.getParent());
				}
			}
		}
	}

	/** Set the recognition timeout */
	private void setTimeout(int timeout) {
		if (usingSpeech) {
			if (speechRecognizer instanceof SapiVise) {
				((SapiVise)speechRecognizer).setTimeout(timeout);
			}
		}
		else if (usingKeypad) ((KeypadRecognizer)keypadRecognizer).setTimeout(timeout);
		else if (usingTimer)  timer.setTimeout(timeout);
	}

	/** Activate the recognizers */
	private void activate(int timeout, ScopeElement scope) throws VXMLEvent {
		try {
			// Set the recognition timeout - ERK
			//      if      (usingSpeech) ((ViseProperties)speechRecognizer.getRecognizerProperties()).setMaxDuration(timeout);               // Throws PropertyVetoException
			//      else if (usingKeypad) ((KeypadRecognizer.Properties)keypadRecognizer.getRecognizerProperties()).setMaxDuration(timeout);  // Throws PropertyVetoException
			//      else if (usingTimer)  timer.setTimeout(timeout);
			recognitionTimeout = timeout;

			// Enable all the rules
			if (!currentGrammarRules.isEmpty()) {
				Enumeration grammarRulePairs = currentGrammarRules.iterator();
				while (grammarRulePairs.hasMoreElements()) {
					GrammarRulePair grammarRulePair = (GrammarRulePair)grammarRulePairs.nextElement();
					// If the rule name is known, enable the named rule
					if (grammarRulePair.ruleName != null) {
						((RuleGrammar)grammarRulePair.grammar).setEnabled(grammarRulePair.ruleName, true);  // Throws IllegalArgumentException
						if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.activate: grammar rule \"" + grammarRulePair.ruleName + "\" enabled");
						// Otherwise, enable all the public rules in the grammar
					} else {
						((RuleGrammar)grammarRulePair.grammar).setEnabled(true);
					}
				}
			}
			if (usingSpeech) speechRecognizer.commitChanges();                                        // Throws GrammarException, EngineStateError
			if (usingKeypad) keypadRecognizer.commitChanges();                                        // Throws GrammarException, EngineStateError
			if (keypadHandler != null) keypadHandler.enable();
		} catch (IllegalArgumentException e) {
			throw new VXMLEvent("error.grammar.nosuchrule", e);
		} catch (GrammarException e) {
			throw new VXMLEvent("error.grammar.semantics", e);
		} catch (EngineStateError e) {
			throw new RuntimeException("Recognizer.activate: UNEXPECTED " + e.toString());
		}
		active = true;
		try {
			scope.logEvent(SystemEvent.RECOGSTART_INDEX);
		} catch (Exception exc) {
			SysLog.println("Recognizer.activate: caught Exception thrown in logEvent(SystemEvent.RECOGSTART_INDEX)");
			exc.printStackTrace(SysLog.stream);
		}
		if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
			SysLog.println("Recognizer.activate: recognizer activated in scope " + scope.toString() + " (\"" + scope.name() + "\") with timeout " + timeout);
	}

	/** Deactivate the recognizers */
	private void deactivate() {
		// Stop the Timer if it is in use
		if (usingTimer) timer.setTimeout(0);
		try {
			// Disable all the rules
			if (!currentGrammarRules.isEmpty()) {
				Enumeration grammarRulePairs = currentGrammarRules.iterator();
				while (grammarRulePairs.hasMoreElements()) {
					GrammarRulePair grammarRulePair = (GrammarRulePair)grammarRulePairs.nextElement();
					// If the rule name is known, disable the named rule
					if (grammarRulePair.ruleName != null) {
						((RuleGrammar)grammarRulePair.grammar).setEnabled(grammarRulePair.ruleName, false); // Throws IllegalArgumentException
						if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
							SysLog.println("Recognizer.deactivate: grammar rule \"" + grammarRulePair.ruleName + "\" disabled");
						// Otherwise, disable all the public rules in the grammar
					} else {
						((RuleGrammar)grammarRulePair.grammar).setEnabled(false);
					}
				}
			}
			if (usingSpeech) speechRecognizer.commitChanges();                                        // Throws GrammarException, EngineStateError
			if (usingKeypad) keypadRecognizer.commitChanges();                                        // Throws GrammarException, EngineStateError
			if (keypadHandler != null) keypadHandler.disable();
		} catch (IllegalArgumentException e) {
			// This will already have been thrown in activate(); don't throw it again here
			if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
				SysLog.println("Recognizer.deactivate: caught " + e.toString());
		} catch (GrammarException e) {
			// This will already have been thrown in activate(); don't throw it again here
			if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
				SysLog.println("Recognizer.deactivate: caught " + e.toString());
		} catch (EngineStateError e) {
			// This will already have been thrown in activate(); don't throw it again here
			if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
				SysLog.println("Recognizer.deactivate: caught " + e.toString());
		}
		active = false;
		if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
			SysLog.println("Recognizer.deactivate: recognizer deactivated");
	}

	/** Restore the recognizer and the prompting mechanism to the quiescent state */
	private synchronized void teardown() {
		if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
			SysLog.println("Recognizer.teardown: entered");
		// Remove the result listeners
		if (speechRecognizer != null) speechRecognizer.removeResultListener(this);
		if (keypadRecognizer != null) keypadRecognizer.removeResultListener(this);
		if (interruptHandler != null) interruptHandler.removeResultListener(this);
		if (usingTimer)               timer.removeResultListener(this);
		if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
			SysLog.println("Recognizer.teardown: result listeners removed");
		// Cancel the remaining prompts
		prompts.removeAllElements();
		if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
			SysLog.println("Recognizer.teardown: prompts cancelled");
		// Cancel the playback of prompts
		// (new AudioPlayer()).cancel(); ERK
		if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.RECOGNIZER_MASK))
			SysLog.println("Recognizer.teardown: AudioPlayer.cancel() has returned");
		deactivate();
	}

	/** Return the concatenated tags of the first-best result, or null if none */
	private String tagText(FinalRuleResult result) {
		String textString = null;
		if (result != null) {
			try {
				String[] tags = result.getTags();
				if (tags != null && tags.length > 0) {
					StringBuffer text = new StringBuffer(tags[0]);
					for (int tagIndex = 1; tagIndex < tags.length; tagIndex++)
						text.append(' ').append(tags[tagIndex]);
					textString = text.toString();
				}
			} catch (ResultStateError e) {
				// Result is not really a FinalRuleResult, so tags are not available
			}
		}
		return textString;
	}

	/** Return the raw (spoken) text of the first-best result, or "<noinput>" if none */
	private String rawText(FinalRuleResult result) {
		StringBuffer text = new StringBuffer("");
		if (result != null) {
			int numTokens = result.numTokens();
			for (int tokenIndex = 0; tokenIndex < numTokens; tokenIndex++) {
				if (tokenIndex > 0) text.append(' ');
				text.append(result.getBestToken(tokenIndex).getSpokenText());
			}
		} else {
			text.append("<noinput>");
		}
		return text.toString();
	}

	/** Return the raw (spoken) text of the first-best result, or "<noinput>" if none */
	private String rawText(ResultToken[] tokens) {
		StringBuffer text = new StringBuffer("");
		if (tokens != null) {
			for (int tokenIndex = 0; tokenIndex < tokens.length; tokenIndex++) {
				if (tokenIndex > 0) text.append(' ');
				text.append(tokens[tokenIndex].getSpokenText());
			}
		} else {
			text.append("<noinput>");
		}
		return text.toString();
	}

	public javax.speech.recognition.Recognizer getSpeechRecognizer() {
		return this.speechRecognizer;
	}

	public void setMockSpeaker(MockSpeaker speaker) {
		this.mockSpeaker = speaker;
	}

	public void createFileMockSpeaker(String fileName) {
		this.mockSpeaker = new FileMockSpeaker((SapiTextRecognizer)speechRecognizer, fileName);
	}

	public void createInteractiveMockSpeaker(String userId, String applicationId, String language) {
		this.mockSpeaker = new InteractiveMockSpeaker((SapiTextRecognizer)speechRecognizer, userId, applicationId, language);
	}
} // class Recognizer
