package voxware.browser;

import java.net.*;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

 /** The ExecutableContent Class (Block, Catcher, Filled and ExecutableIf each contain one of these) */
public class ExecutableContent {

  private Vector executables;    // The Vector of ExecutableElements
  private final Session session;

   // CONSTRUCTORS

   // Construct an ExecutableContent without preprocessing it
  public ExecutableContent(Session session, Goto context) {
	  this.session = session;
  }

   // Construct and preprocess an ExecutableContent
  public ExecutableContent(Session session, Element element, Goto context) throws VXMLEvent {
	  this.session = session;
	  preprocess(element, context, true);                 // Throws VXMLEvent
  }

  protected ExecutableContent(Session session, Element element, Goto context, boolean recursive) throws VXMLEvent {
	  this.session = session;
	  if (recursive) preprocess(element, context, true);  // Throws VXMLEvent
  }

   // PUBLIC METHODS

   // Execute the ExecutableContent, preprocessing it if necessary (Element must underlie an Executables)
   // Return a Variables containing the names of all variables that receive defined values
  public Variables execute(Element element, ScopeElement scope, Goto context, boolean execute)
  throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
    Variables assignments = new Variables();
    return execute(element, scope, context, execute, assignments);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
  }

   // PRIVATE METHODS

   // Execute preprocessed ExecutableContent
  private Variables execute(ScopeElement scope, Goto context, boolean execute, Variables assignments)
  throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
    Enumeration elements = executables.elements();
    try {
      while (elements.hasMoreElements()) {
         // Throws Goto, InvalidExpressionException, UndeclaredVariableException, VXMLEvent, VXMLExit, VXMLReturn
        boolean proceed = ((ExecutableElement)elements.nextElement()).execute(scope, context, execute, assignments);
        if (execute && !proceed) break;
        execute = proceed;
      }
    } catch (InvalidExpressionException e) {
      if (SysLog.printLevel > 0) SysLog.println("ExecutableContent.execute(): ERROR - " + e.toString());
      throw new VXMLEvent("error.syntactic.badexpression", e);
    } catch (UndeclaredVariableException e) {
      if (SysLog.printLevel > 0) SysLog.println("ExecutableContent.execute(): ERROR - " + e.toString());
      throw new VXMLEvent("error.semantic.undeclared", e);
    } 
    return assignments;
  }

   // Execute the ExecutableContent, preprocessing it if necessary (Element must underlie an Executables)
  public Variables execute(Element element, ScopeElement scope, Goto context, boolean execute, Variables assignments)
  throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
     // Build the Vector of ExecutableElements if necessary
    if (executables == null) preprocess(element, context, false);   // Throws VXMLEvent
     // Execute it
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
      SysLog.println("ExecutableContent.execute(): executing the contents of a <" + element.getNodeName() + "> in <" + scope.getClass().getName() + "> scope");
    return execute(scope, context, execute, assignments);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
  }

   // The preprocessor
  private void preprocess(Element element, Goto context, boolean recursive) throws VXMLEvent {

    Node node = null;
    NodeList children = element.getChildNodes();

    executables = new Vector();

    try {
      if (children != null) {
        int len = children.getLength();

         // Preprocess the executable content in the element by stepping through the element's DOM subtree.
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
          SysLog.println("ExecutableContent.preprocess(): preprocessing the executable contents of a <" + element.getNodeName() + ">");
        for (int i = 0; i < len; i++) {
          node = children.item(i);
          int type = node.getNodeType();
          switch (type) {
            case Node.DOCUMENT_NODE:
              if (SysLog.printLevel > 0) SysLog.println("ExecutableContent.preprocess: UNEXPECTED - encountered  DOCUMENT_NODE");
              break;
            case Node.ELEMENT_NODE:
              String name = node.getNodeName();
              switch (Elements.index(name)) {
                case Elements.ASSIGN_INDEX:
                  executables.addElement(new ExecutableAssign((Element)node));  // Throws DOMException, InvalidExpressionException, InvalidTag 
                  break;
                case Elements.AUDIO_INDEX:
                  executables.addElement(new ExecutableAudio((Element)node));
                  break;
                case Elements.CLEAR_INDEX:
                  executables.addElement(new ExecutableClear((Element)node));   // Throws DOMException
                  break;
                case Elements.ELSE_INDEX:
                  executables.addElement(new ExecutableElse((Element)node));    // Throws DOMException, InvalidTag
                  break;
                case Elements.ELSEIF_INDEX:
                  executables.addElement(new ExecutableElseIf((Element)node));  // Throws DOMException, InvalidExpressionException, InvalidTag
                  break;
                case Elements.EXIT_INDEX:
                  executables.addElement(new ExecutableExit((Element)node));    // Throws DOMException, InvalidExpressionException, VXMLEvent
                  break;
                case Elements.GOTO_INDEX:
                  executables.addElement(new ExecutableGoto((Element)node, context));  // Throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException, VXMLEvent
                  break;
                case Elements.IF_INDEX:
                  executables.addElement(new ExecutableIf((Element)node, context, recursive));  // Throws DOMException, InvalidExpressionException, InvalidTag, VXMLEvent
                  break;
                case Elements.LOG_INDEX:
                  executables.addElement(new ExecutableLogger((Element)node));  // Throws DOMException, InvalidExpressionException, InvalidTag, VXMLEvent
                  break;
                case Elements.PROMPT_INDEX:
                  executables.addElement(new ExecutablePrompt((Element)node));  // Throws DOMException, NumberFormatException, VXMLEvent
                  break;
                case Elements.REPROMPT_INDEX:
                  executables.addElement(new ExecutableReprompt((Element)node));
                  break;
                case Elements.RETURN_INDEX:
                  executables.addElement(new ExecutableReturn((Element)node));  // Throws DOMException
                  break;
                case Elements.SCRIPT_INDEX:
                  executables.addElement(new ExecutableScript((Element)node));  // Throws DOMException, InvalidExpressionException, VXMLEvent
                  break;
                case Elements.SUBMIT_INDEX:
                  executables.addElement(new ExecutableSubmit((Element)node, context)); // Throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException, VXMLEvent
                  break;
                case Elements.THROW_INDEX:
                  executables.addElement(new ExecutableThrow((Element)node));   // Throws DOMException, InvalidTag
                  break;
                case Elements.VAR_INDEX:
                  executables.addElement(new ExecutableVar((Element)node));     // Throws DOMException, InvalidExpressionException, InvalidTag
                  break;
                default:
                  if (SysLog.printLevel > 0) SysLog.println("ExecutableContent.preprocess(): unhandled <" + name + "> element = " + node.getNodeValue());
              }
          }
        }
      }
    } catch (DOMException e) {
      if (SysLog.printLevel > 0) SysLog.println("ExecutableContent.preprocess(): ERROR - " + e.toString());
      throw new VXMLEvent("error.domerror", e);
    } catch (InvalidExpressionException e) {
      if (SysLog.printLevel > 0) SysLog.println("ExecutableContent.preprocess(): ERROR - " + e.toString());
      throw new VXMLEvent("error.syntactic.badexpression", e);
    } catch (InvalidTag e) {
      if (SysLog.printLevel > 0) SysLog.println("ExecutableContent.preprocess(): ERROR - " + e.toString());
      throw new VXMLEvent("error.missingattribute." + e.node.getNodeName(), e);
    } catch (MalformedURLException e) {
      if (SysLog.printLevel > 0) SysLog.println("ExecutableContent.preprocess(): ERROR - " + e.toString());
      throw new VXMLEvent("error.badfetch.malformedurl", e);
    } catch (NumberFormatException e) {
      if (SysLog.printLevel > 0) SysLog.println("ExecutableContent.preprocess(): ERROR - " + e.toString());
      throw new VXMLEvent("error.syntactic.numberformat", e);
    }
  }

   // EMBEDDED INTERFACES & CLASSES

   // An ExecutableElement (all implementing classes follow below)
  private interface ExecutableElement {
     // Execute the element
    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws Goto, InvalidExpressionException, UndeclaredVariableException, VXMLEvent, VXMLExit, VXMLReturn;
  }

   // An ExecutableAssign Element
  private class ExecutableAssign implements ExecutableElement {
    private String name;
    private Expr   expr;

    public ExecutableAssign(Element node) throws DOMException, InvalidExpressionException, InvalidTag {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableAssign.ExecutableAssign(): handling <assign>");
      NamedNodeMap nnm = node.getAttributes();
      for (int j = 0; j < nnm.getLength(); j++) {
        Node an = nnm.item(j);
        String nodeName = an.getNodeName();
        if (nodeName.equalsIgnoreCase("name")) {
          name = an.getNodeValue();                          // Throws DOMException
        } else if (nodeName.equalsIgnoreCase("expr")) {
          expr = new Expr(an.getNodeValue(), null);          // Throws DOMException, InvalidExpressionException
        } else {
          if (SysLog.printLevel > 0) SysLog.println("ExecutableAssign.ExecutableAssign(): WARNING - unexpected attribute node \"" + nodeName + "\"");
        }
      }
       // Both the name and the expr attributes are REQUIRED
      if (name == null || expr == null)
        throw new InvalidTag(node);                          // Throws InvalidTag
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
      if (executing) {
        Object value = expr.evaluate(scope);                 // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
        scope.setProperty(name, value);
         // Report if this assignment gives the variable a defined value
        if (value != null && value != VXMLTypes.PROTO_UNDEFINED)
          assignments.put(name, Variable.PROTO_VARIABLE);
      }
       // If executing, continue to execute
      return executing;
    }
  }

   // An ExecutableAudio Element
  private class ExecutableAudio implements ExecutableElement {
    private Element node;

    public ExecutableAudio(Element node) {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableAudio.ExecutableAudio(): handling <audio>");
      this.node = node;
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws VXMLEvent {
      if (executing) session.getAudioPlayer().play(scope, node);    // Throws VXMLEvent
       // If executing, continue to execute
      return executing;
    }
  }

   // An ExecutableClear Element
  private class ExecutableClear implements ExecutableElement {
    private String nameList;

    public ExecutableClear(Element node) throws DOMException {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableClear.ExecutableClear(): handling <clear>");
      NamedNodeMap nnm = node.getAttributes();
      nameList = null;
      for (int j = 0; j < nnm.getLength(); j++) {
        Node an = nnm.item(j);
        String nodeName = an.getNodeName();
        if (nodeName.equalsIgnoreCase("name") || nodeName.equalsIgnoreCase("namelist")) {  // "name" is DEPRECATED
          nameList = an.getNodeValue();                      // Throws DOMException
        } else {
          if (SysLog.printLevel > 0) SysLog.println("ExecutableClear.ExecutableClear(): WARNING - <clear> has unexpected attribute node \"" + nodeName + "\"");
        }
      }
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments) {
      if (executing) {
         // Search up the Scope hierarchy for the ancestral Form
        Scope formScope = scope;
        while (formScope != null && !(formScope instanceof Form))
          formScope = formScope.parent();
        if (formScope instanceof Form) {
           // If any FormItems are specified, clear them
          if (nameList != null) {
            StringTokenizer names = new StringTokenizer(nameList);
            while (names.hasMoreTokens())
              ((Form)formScope).clearFormItem(names.nextToken());
           // Otherwise, clear all FormItems in the ancestral Form
          } else {
            ((Form)formScope).clearFormItems();
          }
        }
      }
       // If executing, continue to execute
      return executing;
    }
  }

   // An ExecutableElse Element
  private class ExecutableElse implements ExecutableElement {

    public ExecutableElse(Element node) {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableElse.ExecutableElse(): handling <else>");
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments) {
       // If executing, stop; otherwise, start
      return !executing;
    }
  }

   // An ExecutableElseIf Element
  private class ExecutableElseIf implements ExecutableElement {
    private Expr condition;

    public ExecutableElseIf(Element node) throws DOMException, InvalidExpressionException, InvalidTag {
      NamedNodeMap nnm = node.getAttributes();
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableElseIf.ExecutableElseIf(): handling <elseif>");
        for (int j = 0; j < nnm.getLength(); j++) {
          Node an = nnm.item(j);
          String nodeName = an.getNodeName();
          if (nodeName.equalsIgnoreCase("cond"))
            condition = new Expr(an.getNodeValue(), null);  // Throws DOMException, InvalidExpressionException
          else if (SysLog.printLevel > 0)
            SysLog.println("ExecutableElseIf.ExecutableElseIf(): WARNING - <elseif> has unexpected attribute node \"" + nodeName + "\"");
        }
         // The conditional expression is REQUIRED
        if (condition == null) throw new InvalidTag(node);   // Throws InvalidTag
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
       // If executing, stop; otherwise, start if the conditional expression is satisfied
      return !executing && ScriptRuntime.toBoolean(condition.evaluate(scope)); // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
    }
  }

   // An ExecutableExit Element
  private class ExecutableExit implements ExecutableElement {
    private Expr expression;
    private String nameList;  // Unsupported !!!

    public ExecutableExit(Element node) throws DOMException, InvalidExpressionException, VXMLEvent {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableExit.ExecutableExit(): handling <exit>");
      NamedNodeMap nnm = node.getAttributes();
      for (int j = 0; j < nnm.getLength(); j++) {
        Node an = nnm.item(j);
        String nodeName = an.getNodeName();
        if (nodeName.equalsIgnoreCase("expr"))
          expression = new Expr(an.getNodeValue(), null );   // Throws DOMException, InvalidExpressionException
        else if (nodeName.equalsIgnoreCase("namelist"))
          throw new VXMLEvent("error.unsupported.attribute", "exit:namelist");
        else if (SysLog.printLevel > 0)
          SysLog.println("ExecutableExit.ExecutableExit(): WARNING - <exit> has unexpected attribute node \"" + nodeName + "\"");
      }
       // The expression is OPTIONAL
      if (expression == null) expression = new Expr(VXMLTypes.PROTO_UNDEFINED);
    }


    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent, VXMLExit {
      if (executing) throw new VXMLExit(expression.evaluate(scope));  // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
       // If not executing, continue not doing so
      return executing;
    }
  }

  private class ExecutableGoto implements ExecutableElement {
    private Goto target;

    public ExecutableGoto(Element node, Goto context) throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException, VXMLEvent {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableGoto.ExecutableGoto(): handling <goto>");
      NamedNodeMap nnm = node.getAttributes();
      target = new Goto(node, nnm, context);  // Throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException, VXMLEvent
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments) 
    throws Goto, VXMLEvent {
      if (executing) {
        target.acquireTarget(scope, context);                // Throws VXMLEvent
         // Force reprompt in case target is the current field item
        target.noPrompt(false);
        target.goToTarget(scope);                            // Throws Goto
      }
       // If not executing, continue not doing so
      return executing;
    }
  }

  private class ExecutableIf implements Executables, ExecutableElement {
    private Expr condition;
    private Element node;

    public ExecutableIf(Element node, Goto context, boolean recursive) throws DOMException, InvalidExpressionException, InvalidTag, VXMLEvent {
      executableContent = new ExecutableContent(session, node, context, recursive);  // Throws VXMLEvent
      this.node = node;
      NamedNodeMap nnm = node.getAttributes();
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableIf.ExecutableIf(): handling <if>");
      for (int j = 0; j < nnm.getLength(); j++) {
        Node an = nnm.item(j);
        String nodeName = an.getNodeName();
        if (nodeName.equalsIgnoreCase("cond"))
          condition = new Expr(an.getNodeValue(), null);     // Throws DOMException, InvalidExpressionException
        else if (SysLog.printLevel > 0)
          SysLog.println("ExecutableIf.ExecutableIf(): WARNING - <if> has unexpected attribute node \"" + nodeName + "\"");
      }
       // The conditional expression is REQUIRED
      if (condition == null) throw new InvalidTag(node);     // Throws InvalidTag
    }


    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws Goto, InvalidExpressionException, UndeclaredVariableException, VXMLEvent, VXMLExit, VXMLReturn {
       // Conditionally execute the content of the <if>
      if (executing) executableContent.execute(node, scope, context, ScriptRuntime.toBoolean(condition.evaluate(scope)), assignments);
       // If executing, continue to execute
      return executing;
    }

     /** Implement the Executables interface */
    private ExecutableContent executableContent;
    public ExecutableContent executableContent() {
      return executableContent;
    }

  }

  private class ExecutableLogger implements ExecutableElement {
    private Logger logger;

    public ExecutableLogger(Element node) throws DOMException, InvalidExpressionException, InvalidTag, VXMLEvent {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableLog.ExecutableLogger(): handling <log>");
      logger = new Logger((Element)node, true);           // Throws DOMException, InvalidExpressionException, InvalidTag, VXMLEvent
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws VXMLEvent {
      if (executing) logger.logEvent(scope);              // Throws VXMLEvent
       // If executing, continue to execute
      return executing;
    }
  }

  private class ExecutablePrompt implements ExecutableElement {
    private Prompt prompt;

    public ExecutablePrompt(Element node) throws DOMException, InvalidExpressionException, NumberFormatException, VXMLEvent {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutablePrompt.ExecutablePrompt(): handling <prompt>");
      prompt = new Prompt(session, (Element)node);                    // Throws DOMException, InvalidExpressionException, NumberFormatException, VXMLEvent
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws VXMLEvent {
      if (executing) prompt.play(scope);                     // Throws VXMLEvent
       // If executing, continue to execute
      return executing;
    }
  }

  private class ExecutableReprompt implements ExecutableElement {

    public ExecutableReprompt(Element node) {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableReprompt.ExecutableReprompt(): handling <reprompt>");
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments) {
      if (executing) context.noPrompt(false);
       // If executing, continue to execute
      return executing;
    }
  }

  private class ExecutableReturn implements ExecutableElement {
    private String eventName;
    private String nameList;

    public ExecutableReturn(Element node) throws DOMException {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableReturn.ExecutableReturn(): handling <return>");
      NamedNodeMap nnm = node.getAttributes();
      for (int j = 0; j < nnm.getLength(); j++) {
        Node an = nnm.item(j);
        String nodeName = an.getNodeName();
        if (nodeName.equalsIgnoreCase("event")) {
          eventName = an.getNodeValue();                     // Throws DOMException
        } else if (nodeName.equalsIgnoreCase("namelist")) {
          nameList = an.getNodeValue();                      // Throws DOMException
        } else if (SysLog.printLevel > 0) {
          SysLog.println("ExecutableReturn.ExecutableReturn(): WARNING - <throw> has unexpected attribute node \"" + nodeName + "\"");
        }
      }
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws UndeclaredVariableException, VXMLEvent, VXMLReturn {
      if (executing) {
        if (!Context.toBoolean(scope.variable("session.issubdialog"))) throw new VXMLEvent("error.semantic.badreturn");
        Scriptable result = scope.newObject();
        if (nameList != null) Variables.setProp(result, nameList, scope);  // Throws UndeclaredVariableException
        else if (eventName != null) Scope.setProp(result, "_event", eventName);
        throw new VXMLReturn(result);
      }
       // If executing, continue to execute
      return executing;
    }
  }

  private class ExecutableScript implements ExecutableElement {
    private VXMLScript script;

    public ExecutableScript(Element node) throws DOMException, InvalidExpressionException, MalformedURLException, VXMLEvent {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableScript.ExecutableScript(): handling <script>");
      script = new VXMLScript(node);                         // Throws DOMException, InvalidExpressionException, MalformedURLException, VXMLEvent
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
      if (executing) script.expression(scope).evaluate(scope);  // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
       // If executing, continue to execute
      return executing;
    }
  }

  private class ExecutableSubmit implements ExecutableElement {
    private Goto target;

    public ExecutableSubmit(Element node, Goto context) throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException, VXMLEvent {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableSubmit.ExecutableSubmit(): handling <submit>");
      NamedNodeMap nnm = node.getAttributes();
      target = new Goto(node, nnm, context);    // Throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException, VXMLEvent
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws Goto, VXMLEvent {
      if (executing) {
         // By default, submit all the explicitly named field item variables
        if (target.submit() == null) {
           // Search up the Scope hierarchy for the ancestral Form
          Scope formScope = scope;
          while (formScope != null && !(formScope instanceof Form))
            formScope = formScope.parent();
           // Make a Variables containing the names of all the explicitly named Field items
          if (formScope instanceof Form) {
            Vector fieldItems = ((Form)formScope).fieldItems();
            if (fieldItems != null && fieldItems.size() > 0) {
              Variables submit = new Variables(fieldItems.size());
              Enumeration items = fieldItems.elements();
              while (items.hasMoreElements()) {
                String fieldName = ((FieldItem)items.nextElement()).explicitName();
                if (fieldName != null) 
                  submit.put("dialog." + fieldName, Variable.PROTO_VARIABLE);
              }
              target.submit(submit);
            }
          }
        }
        target.acquireTarget(scope, context);                // Throws VXMLEvent
        target.goToTarget(scope);                            // Throws Goto
      }
       // If not executing, continue not doing so
      return executing;
    }
  }

  private class ExecutableThrow implements ExecutableElement {
    private String eventName;

    public ExecutableThrow(Element node) throws DOMException, InvalidTag {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableThrow.ExecutableThrow(): handling <throw>");
      NamedNodeMap nnm = node.getAttributes();
      for (int j = 0; j < nnm.getLength(); j++) {
        Node an = nnm.item(j);
        String nodeName = an.getNodeName();
        if (nodeName.equalsIgnoreCase("event"))
          eventName = an.getNodeValue();                     // Throws DOMException
        else if (SysLog.printLevel > 0)
          SysLog.println("ExecutableThrow.ExecutableThrow(): WARNING - <throw> has unexpected attribute node \"" + nodeName + "\"");
      }
       // The event name is REQUIRED
      if (eventName != null) eventName = Catcher.fullName(eventName);
      else throw new InvalidTag(node);                       // Throws InvalidTag
    }


    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws VXMLEvent {
      if (executing) throw new VXMLEvent(eventName);         // Throws VXMLEvent
       // If not executing, continue not doing so
      return executing;
    }
  }

  private class ExecutableVar implements ExecutableElement {
    private Expr initializer;

    public ExecutableVar(Element node) throws DOMException, InvalidExpressionException, InvalidTag {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.EXECUTABLECONTENT_MASK))
        SysLog.println("ExecutableVar.ExecutableVar(): handling <var>");
       // Construct a named expression from the <var> element
      initializer = Session.expression(node);                // Throws DOMException, InvalidExpressionException, InvalidTag
    }

    public boolean execute(ScopeElement scope, Goto context, boolean executing, Variables assignments)
    throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
      if (executing) {
        Object value = initializer.evaluate(scope);          // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
        String name = initializer.name();
        Scriptable container = scope.scope();
         // Don't replace an existing value with VXMLTypes.PROTO_UNDEFINED
        if (value == null || value == VXMLTypes.PROTO_UNDEFINED) {
          try {
            value = (container != null) ? Scope.getProp(container, name) : VXMLTypes.PROTO_UNDEFINED;
          } catch (UndeclaredVariableException e) {
            value = VXMLTypes.PROTO_UNDEFINED;
          }
        }
        Scope.setProp(container, name, value);
         // Report if this definition gives the variable a defined value
        if (value != null && value != VXMLTypes.PROTO_UNDEFINED)
          assignments.put(name, Variable.PROTO_VARIABLE);
      }
       // If executing, continue to execute
      return executing;
    }
  }
}
