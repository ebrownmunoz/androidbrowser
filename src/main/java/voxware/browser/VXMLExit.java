package voxware.browser;

import java.util.*;

public class VXMLExit extends Exception {

  private Object reason;      // A VXMLType

  public VXMLExit(Object reason) {
    super();
    this.reason = reason;
  }

  public Object reason() {
    return this.reason;
  }
}
