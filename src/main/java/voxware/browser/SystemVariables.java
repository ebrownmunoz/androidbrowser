package voxware.browser;

import java.util.*;
import java.lang.reflect.*;
import java.net.URL;

import voxware.engine.recognition.sapivise.*;
import voxware.engine.audioplayer.*;
import voxware.hw.event.HwControls;

import org.mozilla.javascript.*;

import voxware.util.*;

 /** System variables */
public class SystemVariables {

  public static       String NET_ADAPTER_HINT = "";
  public static final String MAC_ADDRESS_NAME = "macaddress";
  public static final String SYS_MEMORY_NAME  = "memory.system.stats";
  
  public static URL urlRoot;

   // SystemVariable Definition
  public static class Definition {

    static final Class[] M1Signature = { ScriptableObject.class };

    private String name;
    private Object value;                             // Expr or name of getter
    private Class  getterClass;                       // Class containing the getter method

    public Definition(String name, String getterName, Class getterClass) {
      this.name = name;
      this.value = getterName;
      this.getterClass = getterClass;
    }

    public Definition(String name, Object value) throws InvalidExpressionException {
      this.name = name;
      if (value instanceof Expr) this.value = (Expr)value;
      else                       this.value = new Expr(value);
    }

    public String name() {
      return name;
    }

    public Expr expr() {
      return (value instanceof Expr) ? (Expr)value : null;
    }

    public Method getter(String name) throws NoSuchMethodException, SecurityException {
      if (getterClass instanceof Class)
        return getterClass.getMethod((value instanceof String) ? (String)value : name, M1Signature); // Throws NoSuchMethodException, SecurityException
      else
        return null;
    }

    public Class getterClass() {
      return getterClass;
    }
  }

   // Definitions
  public Vector definitions = new Vector(116);
  
  public SystemVariables (String macAddr) {
     // SystemEvent variables
    definitions.addElement(new Definition(SystemEvent.BATTERYCHARGE_NAME,             null, SystemEvent.class));
    definitions.addElement(new Definition(SystemEvent.POWERMANAGEMENT_NAME,           null, SystemEvent.class));
    definitions.addElement(new Definition(SystemEvent.RADIOSTATUS_NAME,               null, SystemEvent.class));
    definitions.addElement(new Definition(SystemEvent.RADIOSTATUSPINGIP_NAME,         null, SystemEvent.class));
    definitions.addElement(new Definition(SystemEvent.MICROPHONEGAIN_NAME,            null, SystemEvent.class));
    definitions.addElement(new Definition(SystemEvent.SIDETONEGAIN_NAME,              null, SystemEvent.class));
    definitions.addElement(new Definition(SystemEvent.SPEAKERMUTE_NAME,               null, SystemEvent.class));
    definitions.addElement(new Definition(SystemEvent.SPEAKERVOLUME_NAME,             null, SystemEvent.class));
     // Browser properties
    definitions.addElement(new Definition(BrowserProperty.AUDIOFETCHHINT_NAME,        null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.BARGEIN_NAME,               null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.CACHING_NAME,               null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.DOCUMENTFETCHHINT_NAME,     null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.FETCHAUDIO_NAME,            null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.FETCHTIMEOUT_NAME,          null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.GRAMMARFETCHHINT_NAME,      null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.INPUTMODES_NAME,            null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.OBJECTFETCHHINT_NAME,       null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.SCRIPTFETCHHINT_NAME,       null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.SPEAKERFETCHHINT_NAME,      null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.EVENTRECURSIONLIMIT_NAME,   null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.JAVAGCTHRESHOLD_NAME,       null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.JAVAMAXPERCENTUSED_NAME,    null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.MATCHREQUIRED_NAME,         null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.MAXCONNECTRETRIES_NAME,     null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.MAXFETCHTRIES_NAME,         null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.MINBYTESFREE_NAME,          null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.MINMAXFREEBLOCKSIZE_NAME,   null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.SUPPRESSSPEECHINPUT_NAME,   null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.SUPPRESSKEYPADINPUT_NAME,   null, BrowserProperty.class));
    definitions.addElement(new Definition(BrowserProperty.TIMEOUT_NAME,               null, BrowserProperty.class));

     // TTS variables
    definitions.addElement(new Definition(Property.TTS_SPEED_NAME,                    null, SystemVariables.class));
    definitions.addElement(new Definition(Property.TTS_PITCH_NAME,                    null, SystemVariables.class));
    definitions.addElement(new Definition(Property.TTS_VOLUME_NAME,                   null, SystemVariables.class));
    definitions.addElement(new Definition(Property.TTS_ENGINE_NAME,                   null, SystemVariables.class));
    definitions.addElement(new Definition(Property.TTS_LANGUAGE_NAME,                 null, SystemVariables.class));
    definitions.addElement(new Definition(Property.TTS_VOICE_NAME,                    null, SystemVariables.class));
     // Miscellaneous variables
    try {
    	definitions.addElement(new Definition(SystemVariables.MAC_ADDRESS_NAME, macAddr));
    } catch (InvalidExpressionException e) {
    	SysLog.println ("SystemVariables: Invalid expression for macAddr");
    }
    
    definitions.addElement(new Definition(Property.SYSTEM_PRINTLEVEL_NAME,            null, SystemVariables.class));
    definitions.addElement(new Definition(Property.SYSTEM_PRINTMASK_NAME,             null, SystemVariables.class));
    definitions.addElement(new Definition(Property.SYSTEM_URLROOT_NAME,               null, SystemVariables.class));
    definitions.addElement(new Definition(SystemVariables.SYS_MEMORY_NAME,     "sysmemory", SystemVariables.class));
  }

   // Add another Definition to the definitions Vector
  public void addDefinition(Definition definition) {
    definitions.addElement(definition);
  }

   // Define all the variables having Definitions in the definitions Vector
  public void define(Scriptable scope, Scriptable scopeChain) throws VXMLEvent {
    try {
      Context context = Context.getCurrentContext();
      Enumeration variables = definitions.elements();
      while (variables.hasMoreElements()) {
        Definition definition = (Definition)variables.nextElement();
        Scriptable container = scope;
        StringTokenizer path = new StringTokenizer(definition.name, ".");
        String name = path.nextToken();
         // If the name is compound (i.e., contains '.'), descend the Object hierarchy, constructing it if necessary
        while (path.hasMoreTokens()) {
           // Walk the prototype chain looking for the named property
          Scriptable prototype = container;
          Object value = Scriptable.NOT_FOUND;
          while (value == Scriptable.NOT_FOUND && prototype != null) {
            value = prototype.get(name, container);
            prototype = prototype.getPrototype();
          }
           // If there is no such Object, build the rest of the Object hierarchy
          if (value == Scriptable.NOT_FOUND) {
            do {
              value = context.newObject(scopeChain);  // Throws PropertyException, NotAFunctionException, JavaScriptException
              container.put(name, container, value);
              container = (Scriptable)value;
              name = path.nextToken();
            } while (path.hasMoreTokens());
           // Otherwise, just descend to the next level
          } else {
            if (value instanceof Scriptable) {
              container = (Scriptable)value;
              name = path.nextToken();
            } else {
              throw new RuntimeException("SystemVariables.define(): UNEXPECTED - value not instanceof Scriptable");
            }
          }
        }
         // Define the variable as a read-only property with the associated getter or value
        try {
          Method getter = definition.getter(name);    // Throws NoSuchMethodException, SecurityException
          if (getter != null) {
             // Throws PropertyException
            ((ScriptableObject)container).defineProperty(name, null, getter, null, ScriptableObject.READONLY);
          } else {
             // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
            ((ScriptableObject)container).defineProperty(name, definition.expr().evaluateInScriptable(scope), ScriptableObject.READONLY);
          }
        } catch (NoSuchMethodException e) {
          throw new RuntimeException("SystemVariables.define(): UNEXPECTED - failure defining \"" + definition.name + "\" (" + e.toString() + "\"");
        } catch (InvalidExpressionException e) {
          throw new VXMLEvent("error.syntactic.badexpression", e);
        } catch (UndeclaredVariableException e) {
          throw new VXMLEvent("error.semantic.undeclared", e);
        }
      }
    } catch (SecurityException e) {
      throw new RuntimeException("SystemVariables.define(): UNEXPECTED - " + e.toString());
    } catch (JavaScriptException e) {
      throw new RuntimeException("SystemVariables.define(): UNEXPECTED - " + e.toString());
    }
  }

  
   // Getters for TTS parameters

  public static Object speed(ScriptableObject ignored) {
    return VXMLTypes.floatToNumber(VoxwareText.getSpeed());
  }

  public static Object pitch(ScriptableObject ignored) {
    return VXMLTypes.floatToNumber(VoxwareText.getPitch());
  }

  public static Object volume(ScriptableObject ignored) {
    return VXMLTypes.floatToNumber(VoxwareText.getVolume());
  }

  public static Object engine(ScriptableObject ignored) {
    return VoxwareText.getEngine();
  }

  public static Object language(ScriptableObject ignored) {
    return VoxwareText.getLanguage();
  }

  public static Object voice(ScriptableObject ignored) {
    return VoxwareText.getVoice();
  }

   // Getters for miscellaneous parameters

  public static Object macaddress(ScriptableObject ignored) {
    Object address = System.getProperty("hw.mac");
    if (address == null) address = HwControls.getInstance().getMacAddr(NET_ADAPTER_HINT);
    if (address == null) address = VXMLTypes.PROTO_UNDEFINED;
    return address;
  }

  public static Object printlevel(ScriptableObject ignored) {
    return VXMLTypes.intToNumber(SysLog.printLevel);
  }

  public static Object printmask(ScriptableObject ignored) {
    return VXMLTypes.longToNumber(SysLog.printMask);
  }

  public static Object urlroot(ScriptableObject ignored) {
    return getUrlRoot();
  }
  
  public static URL getUrlRoot() {
	  return SystemVariables.urlRoot;
  }
  
  public static void setUrlRoot(URL root) {
	  SystemVariables.urlRoot = root;
  }

  public static Object sysmemory(ScriptableObject scope) {
    ScriptableObject stats = (ScriptableObject)Scope.newObject(scope);
    SystemMemory memory = new SystemMemory();
    stats.put("totalbytes", stats, VXMLTypes.longToNumber(memory.totalBytes));
    stats.put("bytesfree", stats, VXMLTypes.longToNumber(memory.bytesFree));
    stats.put("percentused", stats, VXMLTypes.intToNumber(memory.percentUsed));
    stats.put("maxfreeblocksize", stats, VXMLTypes.longToNumber(memory.maxFreeBlockSize));
    return stats;
  }
}
