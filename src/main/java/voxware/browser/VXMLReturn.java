package voxware.browser;

import java.util.*;
import org.mozilla.javascript.*;

public class VXMLReturn extends Exception {

  private Scriptable result;

  public VXMLReturn(Scriptable result) {
    super();
    this.result = result;
  }

  public Scriptable result() {
    return this.result;
  }
}
