package voxware.browser;

import java.util.*;
import org.mozilla.javascript.*;

import voxware.browser.*;

 // A null Converter
class NullConverter implements Converter {

  private static NullConverter converter = null;

  public static NullConverter instance() {
    if (converter == null) converter = new NullConverter();
    return converter;
  }

  public Object convert(Object value) throws VXMLEvent {
    return value;
  }
}
