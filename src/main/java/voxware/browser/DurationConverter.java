package voxware.browser;

import java.util.*;
import org.mozilla.javascript.*;

import voxware.browser.*;

 // A singleton Converter for time designators
public class DurationConverter implements Converter {

  public static final int UNSPECIFIED = -1;  // Any duration less than zero is considered unspecified

  private static DurationConverter converter = null;

  public static DurationConverter instance() {
    if (converter == null) converter = new DurationConverter();
    return converter;
  }

  public Object convert(Object value) throws VXMLEvent {
    int duration = UNSPECIFIED;
    if (value instanceof String) {
      int index;
      if ((index = ((String)value).indexOf('m')) > 0)
         // Interpret what precedes the 'm' as an integer number of milliseconds
        duration = ScriptRuntime.toInt32(((String)value).substring(0, index).trim());
      else if ((index = ((String)value).indexOf('s')) > 0)
         // Interpret what precedes the 's' as a float number of seconds
        duration = (int)(1000.0 * ScriptRuntime.toNumber(((String)value).substring(0, index).trim()));
      else
         // Interpret the string as an integer number of milliseconds
        duration = ScriptRuntime.toInt32(((String)value).trim());
    } else {
       // Try to convert the value to an integer number of milliseconds
      duration = ScriptRuntime.toInt32(value);
    }
    if (duration < 0) duration = UNSPECIFIED;
    return new Integer(duration);
  }
}
