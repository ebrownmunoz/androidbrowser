package voxware.browser;

import java.util.Vector;
import java.util.Enumeration;

import voxware.browser.*;

 /** The Loggers for a Scope **/
class Loggers extends Vector {

  public Loggers() {
    super();
  }

  public void addLogger(Logger logger) {
    addElement(logger);
  }

   // Get a Logger to log the indexed event, or null if there is none
  public synchronized Logger getLogger(int eventIndex, Scope scope) throws VXMLEvent {
    Logger logger = null;
    Enumeration loggers = elements();
    while (logger == null && loggers.hasMoreElements()) {
      logger = (Logger)loggers.nextElement();
      if (!logger.shouldLogEvent(eventIndex, scope)) logger = null;                   // Throws VXMLEvent
    }
    return logger;
  }

   // Say whether to log the indexed event
  public synchronized boolean shouldLogEvent(int eventIndex, Scope scope) throws VXMLEvent {
    boolean shouldLog = false;
    Enumeration loggers = elements();
    while (!shouldLog && loggers.hasMoreElements())
      shouldLog = ((Logger)loggers.nextElement()).shouldLogEvent(eventIndex, scope);  // Throws VXMLEvent
    return shouldLog;
  }

   // Say whether to log the named event
  public synchronized boolean shouldLogEvent(String eventName, Scope scope) throws VXMLEvent {
    return shouldLogEvent(SystemEvent.index(eventName), scope);                       // Throws VXMLEvent
  }
} // class Loggers
