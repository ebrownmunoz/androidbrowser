/**
 * A DOM-based VXML browser for Pacer Voxware XML
 *
 * Based on the DOMWriter code from I.B.M.
 */

package voxware.browser;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.mozilla.javascript.Context;

import voxware.engine.audioplayer.AsyncAudioPlayer;
import voxware.engine.audioplayer.VoxwareTextAudioPlayer;
import voxware.engine.recognition.mock.FileMockSpeaker;
import voxware.engine.recognition.mock.MockSpeaker;
import voxware.hw.event.HwControls;
import voxware.security.key.Verifier;
import voxware.util.*;
import voxware.util.PCFG.Parameter.EvaluationException;
import voxware.vjml.*;


/** A VXML browser */
public class DOMBrowser {

	// Constants
	public static final String  splash = "Voxware VoiceXML Browser";
	public static final String  copyright = "(c) Voxware, Inc. 1999-2011";
	public static final String  version = "3.5.10-10";
	public static final String  DEFAULT_CLASSFILE_EXTENSION = ".class";
	public static final String  DEFAULT_CLASSLOADER = "voxware.browser.DefaultClassLoader";
	public static final String  DEFAULT_PARSER_NAME = "voxware.browser.DOMParser";
	public static final String  DEFAULT_ERROR_URLNAME = null;
	public static final int     DEFAULT_JAVASCRIPT_OPTIMIZATION = 0;
	public static final String  DEFAULT_HELP_URLNAME = null;
	public static final String  DEFAULT_NOMATCH_URLNAME = null;
	public static final long    DEFAULT_MAX_CACHE_BYTES = (long)2097152;
	public static final int     DEFAULT_MAX_CACHE_ENTRIES = 50;
	public static final String  DEFAULT_SPEECH_ENGINE_CENTRAL_CLASSNAME = "voxware.engine.recognition.sapivise.SapiViseEngineCentral";
	public static final String  DEFAULT_KEYPAD_ENGINE_CENTRAL_CLASSNAME = "voxware.engine.recognition.keypad.KeypadEngineCentral";
	public static final String  DEFAULT_RECOGNITION_MODE = "voice";
	public static final int     DEFAULT_LOGLEVEL = 0;
	public static final long    DEFAULT_LOGMASK = 0x0;
	public static final String  DEFAULT_TEST_PROTOCOL_NAME = "auto";
	
	
	
	private static ExecutorService threadPool;
	
	public static CountDownLatch latch; // wait until all threads finish before exiting.
	
	// Static Fields

	public static Cache         cache = null;
	public static Debugger      debugger = null;              // The voxware.browser.Debugger
	public static Vector        classFileExtensions;          // The list of the class file extensions
	public static Vector        classLoaders = new Vector();  // The list of voxware.browser.ClassLoader Objects
	public static Vector        xmlParsers = new Vector();    // The list of voxware.browser.DOMParserWrapper Classes


	// Data

	private static String       PRINTWRITER_ENCODING = "UTF8";

	private static final String       JAVA_SUPPORTED_ENCODINGS[] = {
		"Default",
		"8859_1", "8859_2", "8859_3", "8859_4", "8859_5", "8859_6", "8859_7", "8859_8", "8859_9",
		"Cp037", "Cp273", "Cp277", "Cp278", "Cp280", "Cp284", "Cp285", "Cp297", "Cp420", "Cp424", "Cp437", "Cp500",
		"Cp737", "Cp775", "Cp838", "Cp850", "Cp852", "Cp855", "Cp856", "Cp857", "Cp860", "Cp861", "Cp862", "Cp863",
		"Cp864", "Cp865", "Cp866", "Cp868", "Cp869", "Cp870", "Cp871", "Cp874", "Cp875", "Cp918", "Cp921", "Cp922",
		"Cp930", "Cp933", "Cp935", "Cp937", "Cp939", "Cp942", "Cp948", "Cp949", "Cp950", "Cp964", "Cp970", "Cp1006",
		"Cp1025", "Cp1026", "Cp1046", "Cp1097", "Cp1098", "Cp1112", "Cp1122", "Cp1123", "Cp1124", "Cp1250", "Cp1251",
		"Cp1252", "Cp1253", "Cp1254", "Cp1255", "Cp1256", "Cp1257", "Cp1258", "Cp1381", "Cp1383", "Cp33722", "MS874",
		"DBCS_ASCII", "DBCS_EBCDIC", "EUC", "EUCJIS", "GB2312", "GBK", "ISO2022CN_CNS", "ISO2022CN_GB",
		"JIS", "JIS0208", "KOI8_R", "KSC5601","MS874", "SJIS", "SingleByte", "Big5", "CNS11643",
		"MacArabic", "MacCentralEurope", "MacCroatian", "MacCyrillic", "MacDingbat", "MacGreek",
		"MacHebrew", "MacIceland", "MacRoman", "MacRomania", "MacSymbol", "MacThai", "MacTurkish",
		"MacUkraine", "SJIS", "Unicode", "UnicodeBig", "UnicodeLittle", "UTF8"
	};

	private static boolean      canonical;
	private static boolean      logRedirected = false;

	// Static Variable (mainly for SessionCreation)

	private static String bootURI;
	private static String speakerURL; 
	private static String speakerDirectory; 
	private static Vector vxmlDefinitions;
	private static boolean preprocess;
	private static boolean speechEnabled;
	private static boolean interruptsEnabled;
	private static boolean traces;
	private static boolean callExit;
	private static String[] scriptNames;
	private static int optimizationLevel; // pcfg.parameter("JSOptimization").intValue());
	private static boolean jsDebug;
	private static boolean jsNoSource;
	private static boolean interactive = false;
	private static String userId = null;
	private static String applicationId = null;
	private static String macAddr = null;

	// Constructors

	private DOMBrowser(boolean canonical) throws UnsupportedEncodingException {
		this(getBrowserEncoding(), canonical);
	}

	public DOMBrowser(String encoding, boolean canonical) throws UnsupportedEncodingException {
		this.canonical = canonical;
	}


	// Static Methods

	public static String getBrowserEncoding() {
		return PRINTWRITER_ENCODING;
	}

	public static void setBrowserEncoding(String encoding) {
		PRINTWRITER_ENCODING = encoding;
	}

	public static boolean isValidJavaEncoding(String encoding) {
		for (int i = 0; i < JAVA_SUPPORTED_ENCODINGS.length; i++)
			if (encoding.equals(JAVA_SUPPORTED_ENCODINGS[i])) return (true);
		return false;
	}

	private static void printValidJavaEncodings() {
		SysLog.println("Valid encodings are (case sensitive):");
		SysLog.print("   ");
		for (int i = 0; i < JAVA_SUPPORTED_ENCODINGS.length; i++) {
			SysLog.print(JAVA_SUPPORTED_ENCODINGS[i] + " ");
			if (i % 7 == 0) {
				SysLog.println();
				SysLog.print("   ");
			}
		}
		SysLog.println();
	}


	/**------------------------------------------------------------*
	 * If name is a well-formed URL, acquire the content and return
	 * it in a ByteArrayInputStream; otherwise, try to open a file
	 * named name and return the FileInputStream; otherwise throw a
	 * FileNotFoundException.
	 *------------------------------------------------------------**/
	public static InputStream openConfigFile(String name) throws FileNotFoundException, IOException {
		InputStream instream;
		
		if (name == null) throw new FileNotFoundException("null file name");
		try {
			HttpURLConnection connection = (HttpURLConnection) 
					new URL(SystemVariables.getUrlRoot(), name).openConnection();
			instream = new BufferedInputStream(connection.getInputStream());
			
		} catch (MalformedURLException e) {
			// It's not a URL, so assume it's just a file name
			return new FileInputStream(name);                       // Throws FileNotFoundException
		}
		// It's a URL, so get its content
		return instream ;
	}

	private static void readPCFG(String[] argv) throws IllegalArgumentException, EvaluationException, MalformedURLException {
		PCFG pcfg = initializePcfg();

		// Enable Cookies
		PCFG.Parameter help = pcfg.helpParameter(argv);
		if (argv.length == 0 || help != null) {
			printUsage(pcfg, help == pcfg.parameter("XHelp"));
			return;
		}
		// Get the parameters from the command line
		pcfg.scanCommandLine(argv);
		if (pcfg.parameter("CommandLineScan").booleanValue() || pcfg.parameter("XQuit").booleanValue() || SysLog.printLevel > 2)
			SysLog.println("DOMBrowser.main: scanned command line:\n" + pcfg.parametersToString());

		redirectLog(pcfg);
		SysLog.println(splash + " " + version + " " + copyright);

		// Scan the configuration files specified on the command line, in the order of their appearance
		Object configFileList = pcfg.parameter("CfgFile").value();
		if (configFileList != null && configFileList instanceof Vector) {
			Enumeration configFiles = ((Vector)configFileList).elements();
			while (configFiles.hasMoreElements()) {
				String configFileName = (String)configFiles.nextElement();
				try {
					if (SysLog.printLevel > 0) SysLog.println("DOMBrowser.main: Scanning the configuration file \"" + configFileName + "\"");
					pcfg.scanConfigFileSelectively(openConfigFile(configFileName));
					SysLog.println("DOMBrowser.main: Scanned the configuration file \"" + configFileName + "\"");
				} catch (FileNotFoundException e) {
					SysLog.println("DOMBrowser.main: ERROR - configuration file \"" + configFileName + "\" + not found");
					return;
				} catch (IOException e) {
					SysLog.println("DOMBrowser.main: ERROR - error reading configuration file \"" + configFileName + "\"");
					return;
				}
			}
		}

		// Scan the configuration file specified in the environment variable named in CfgEnv
		try {
			PCFG.Parameter configParameter = pcfg.parameter("CfgEnv");
			if (configParameter.value() != null) {
				String configEnvName = pcfg.parameter("CfgEnv").stringValue();
				pcfg.scanConfigFile(openConfigFile(configEnvName));
				SysLog.println("DOMBrowser.main: Scanned the primary configuration file specified in " + configEnvName);
			} else {
				SysLog.println("DOMBrowser.main: No primary configuration file specified");
			}
		} catch (FileNotFoundException e) {
			SysLog.println("DOMBrowser.main: No primary configuration file found");
		} catch (IOException e) {
			SysLog.println("DOMBrowser.main: ERROR - error reading the primary configuration file");
			return;
		}

		redirectLog(pcfg);

		SysLog.setPrintLevel(pcfg.parameter("LogLevel").intValue());
		SysLog.setPrintMask(pcfg.parameter("LogMask").longValue());
		SysLog.setShowStamp(pcfg.parameter("TimeStampLog").booleanValue());

		if (pcfg.parameter("ChronoLog").booleanValue()) {
			SysLog.setShowStamp();
			SysLog.setPrintMaskBits(SysLog.GOTO_MASK | SysLog.PREPROCESS_MASK | SysLog.RECOGNIZER_MASK | SysLog.SCRIPT_MASK | SysLog.URLLOADER_MASK);
			if (SysLog.getPrintLevel() < 2) SysLog.setPrintLevel(2);
		}

		SysTimer.granularity(pcfg.parameter("TimerGranularity").intValue());

		if (pcfg.parameter("NoVXMLIO").booleanValue()) {
			pcfg.parameter("NoAudioOutput").setInitialValue(Boolean.TRUE);
			pcfg.parameter("NoInterrupts").setInitialValue(Boolean.TRUE);
			pcfg.parameter("NoKeypadRecognition").setInitialValue(Boolean.TRUE);
			pcfg.parameter("NoSpeechRecognition").setInitialValue(Boolean.TRUE);
		}

		SystemVariables.NET_ADAPTER_HINT = pcfg.parameter("NetworkAdapterHint").stringValue();

		DOMBrowser.bootURI = pcfg.parameter("BootURI").stringValue();
		SystemVariables.setUrlRoot(new URL(pcfg.parameter("URLRoot").stringValue()));

		// Variables
		String   encoding           =  pcfg.parameter("VXMLEncoding").stringValue();
		long     maxCacheBytes      =  pcfg.parameter("MaxURLCacheBytes").longValue();
		int      maxCacheEntries    =  pcfg.parameter("MaxURLCacheEntries").intValue();
		DOMBrowser.preprocess         = !pcfg.parameter("NoPreprocessing").booleanValue(); // Preprocess executable content by default
		boolean  audioOutEnabled    = !pcfg.parameter("NoAudioOutput").booleanValue();
		DOMBrowser.interruptsEnabled  = !pcfg.parameter("NoInterrupts").booleanValue();
		boolean  keypadEnabled      = !pcfg.parameter("NoKeypadRecognition").booleanValue();
		DOMBrowser.speechEnabled      = !pcfg.parameter("NoSpeechRecognition").booleanValue();
		DOMBrowser.speakerURL         =  pcfg.parameter("Speaker").stringValue();
		DOMBrowser.callExit           = !pcfg.parameter("NoExit").booleanValue();
		DOMBrowser.traces             =  pcfg.parameter("Traces").booleanValue();
		DOMBrowser.interactive		  =  pcfg.parameter("Interactive").booleanValue();

		// Get the class file extensions
		PCFG.Parameter classFileExtParameter = pcfg.parameter("ClassFileExtension");
		classFileExtensions = (Vector)classFileExtParameter.value();
		if (classFileExtensions != null) {
			// If extensions have been added, remove the default
			if (classFileExtParameter.configured() && !classFileExtensions.isEmpty())
				classFileExtensions.remove(0);
		}

		// Load and instantiate the voxware.browser.ClassLoaders
		PCFG.Parameter classLoaderParameter = pcfg.parameter("ClassLoader");
		Vector classLoaderNames = (Vector)classLoaderParameter.value();
		if (classLoaderNames != null) {
			// If ClassLoaders have been added, move the default ClassLoader to the end of the list
			if (classLoaderParameter.configured() && !classLoaderNames.isEmpty()) 
				classLoaderNames.addElement(classLoaderNames.remove(0));
			Enumeration classLoaderNameEnum = classLoaderNames.elements();
			while (classLoaderNameEnum.hasMoreElements()) {
				String classLoaderName = (String)classLoaderNameEnum.nextElement();
				try {
					classLoaders.addElement((voxware.browser.ClassLoader)Class.forName(classLoaderName).newInstance());
					SysLog.println("DOMBrowser.main: voxware.browser.ClassLoader \"" + classLoaderName + "\" instantiated");
				} catch (ClassNotFoundException e) {
					SysLog.println("DOMBrowser.main: WARNING - cannot find voxware.browser.ClassLoader \"" + classLoaderName + "\" (" + e.toString() + ")");
				} catch (InstantiationException e) {
					SysLog.println("DOMBrowser.main: WARNING - cannot instantiate voxware.browser.ClassLoader \"" + classLoaderName + "\" (" + e.toString() + ")");
				} catch (IllegalAccessException e) {
					SysLog.println("DOMBrowser.main: WARNING - cannot access voxware.browser.ClassLoader \"" + classLoaderName + "\" (" + e.toString() + ")");
				} catch (Exception e) {
					SysLog.println("DOMBrowser.main: WARNING - cannot load and instantiate voxware.browser.ClassLoader \"" + classLoaderName + "\" (" + e.toString() + ")");
				}
			}
		}

		// Define the Java system properties listed in the "DefineJavaProperty" parameter
		Object javaDefinitions = pcfg.parameter("DefineJavaProperty").value();
		if (javaDefinitions != null && javaDefinitions instanceof Vector) {
			Properties properties = System.getProperties();
			Enumeration definitions = ((Vector)javaDefinitions).elements();
			String name = null;
			Object value = null;
			try {
				while (definitions.hasMoreElements()) {
					String definition = (String)definitions.nextElement();
					int equalsIndex = definition.indexOf('=');
					if (equalsIndex < 0) equalsIndex = definition.length();
					name = definition.substring(0, equalsIndex).trim();
					value = (equalsIndex + 1 < definition.length()) ? (Object)definition.substring(equalsIndex + 1).trim() : null;
					if (value == null) {
						properties.remove(name);
						SysLog.println("DOMBrowser.main: removed Java System property \"" + name + "\"");
					} else {
						properties.put(name, value);
						SysLog.println("DOMBrowser.main: defined Java System property \"" + name + "\" as \"" + value + "\"");
					}
				}
			} catch (Exception e) {
				SysLog.println("DOMBrowser.main: ERROR - cannot define \"" + name + "\" as \"" + value + "\" due to " + e.toString());
				return;
			}
			System.setProperties(properties);
		}

		canonical = pcfg.parameter("CanonicalXML").booleanValue();

		// String speakerDirectory = pcfg.parameter("SpeakerDirectory").stringValue(); // ERK
		DOMBrowser.speakerDirectory = null;

		BrowserProperty.value(BrowserProperty.MAXFETCHTRIES_INDEX, (Integer)pcfg.parameter("MaxURLRequests").value());

		// This little bit of ugliness (static variables set here) is from legacy code.
		Session.max_speaker_cache_bytes = pcfg.parameter("MaxSpkrCacheBytes").longValue();
		Session.max_speaker_cache_entries = pcfg.parameter("MaxSpkrCacheEntries").intValue();

		// Set the VXML encoding
		if (isValidJavaEncoding(encoding)) {
			setBrowserEncoding(encoding);
		} else {
			SysLog.println("DOMBrowser.main: ERROR - \"" + encoding + "\" is not a valid encoding");
			printValidJavaEncodings();
			return;
		}

		SysLog.println("DOMBrowser.main: System architecture is " + System.getProperty("os.arch"));
		SysLog.println("DOMBrowser.main: Operating system is " + System.getProperty("os.name") + " " + System.getProperty("os.version"));
		SysLog.println("DOMBrowser.main: Default file encoding is " + System.getProperty("file.encoding"));
		SysLog.println("DOMBrowser.main: Running with the following parameter settings:\n" + pcfg.parametersToString());

		if (pcfg.parameter("XQuit").booleanValue()) {
			SysLog.println("DOMBrowser.main: Exiting by request after configuration" );
			return;
		}

		Expr.constructScriptCache(pcfg.parameter("MaxScriptCacheEntries").intValue());


		// Start up the background garbage collection thread
		//      if (pcfg.parameter("Scavenge").booleanValue())
		//        new Thread(GarbageCollector.getInstance(), "Scavenger").start();
		// Construct the browser's cache
		cache = new Cache(maxCacheEntries, maxCacheBytes);

		// Set the values of the VXML properties listed in the "Property" parameter
		Object propertyList = pcfg.parameter("Property").value();
		if (propertyList != null && propertyList instanceof Vector) {
			Enumeration properties = ((Vector)propertyList).elements();
			String name = null;
			Object value = null;
			try {
				while (properties.hasMoreElements()) {
					String assignment = (String)properties.nextElement();
					int equalsIndex = assignment.indexOf('=');
					if (equalsIndex < 0) equalsIndex = assignment.length();
					name = assignment.substring(0, equalsIndex).trim();
					value = (equalsIndex + 1 < assignment.length()) ? (Object)assignment.substring(equalsIndex + 1).trim() : (Object)VXMLTypes.PROTO_UNDEFINED;
					Property property = Property.newInstance(name, value);
					if (property != null) {
						property.enter(null);     // Throws InvalidExpressionException, PropertyVetoException, UndeclaredVariableException, VXMLEvent
						SysLog.println("DOMBrowser.main: set property \"" + name + "\" to \"" + value + "\"");
					} else {
						SysLog.println("DOMBrowser.main: WARNING - property \"" + name + "\" does not exist");
					}
				}
			} catch (Exception e) {
				SysLog.println("DOMBrowser.main: ERROR - cannot set property \"" + name + "\" to \"" + value + "\" due to " + e.toString());
				return;
			}
		}     

		// Define the session variables listed in the "Define" parameter
		DOMBrowser.vxmlDefinitions = (Vector)pcfg.parameter("Define").value();

		int numScripts = pcfg.numUnknownArgs();
		
		if (interactive) {
			
			if (pcfg.numUnknownArgs() != 3)
			{
				printUsage(pcfg, false);
				System.exit(-1);
			}
			
			DOMBrowser.userId = argv[pcfg.indexOfUnknownArg(0)];
			DOMBrowser.applicationId = argv[pcfg.indexOfUnknownArg(1)];
			DOMBrowser.macAddr = argv[pcfg.indexOfUnknownArg(2)];
		} else {
			DOMBrowser.scriptNames = new String[numScripts]; 
			for (int jjj = 0 ; jjj < pcfg.numUnknownArgs(); jjj++) {
				DOMBrowser.scriptNames[jjj] = argv[pcfg.indexOfUnknownArg(jjj)];
			}
		}
		
		optimizationLevel =  pcfg.parameter("JSOptimization").intValue();
		// Remember that JavaScript debugging suppresses optimization
		jsDebug = pcfg.parameter("JSDebug").booleanValue();
		jsNoSource = pcfg.parameter("JSNoSource").booleanValue();
	}
	
	private static void startInteractive (String bootURI, 
			Vector vxmlDefinitions, boolean preprocess, boolean speechEnabled, 
			boolean interruptsEnabled, boolean traces) throws FileNotFoundException, IOException, InvalidTag, VXMLEvent {



		DOMBrowser.latch = new CountDownLatch(1);
		threadPool = new ThreadPoolExecutor(1, 1, 1, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

		SessionThread sthread = createNewInteractiveSessionThread(bootURI, 
				DOMBrowser.userId, DOMBrowser.applicationId, "en-US", vxmlDefinitions, DOMBrowser.macAddr, 
				"B0", preprocess, speechEnabled, interruptsEnabled, traces);

		threadPool.execute(sthread);



		try {
			DOMBrowser.latch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Main

	public static void main(String argv[]) {

		System.out.println("DOMBrowser.main: invoked");

		System.setProperty("gnu.gcj.runtime.NameFinder.demangle", "false");
		System.setProperty("gnu.gcj.runtime.NameFinder.use_addr2line", "false");

		try {

			readPCFG(argv);
			
			// TODO start session here! CreateNewSessionThread
			
		} catch (PCFG.Parameter.EvaluationException e) {
			SysLog.println("DOMBrowser.main: threw " + e.toString());
			Throwable thrown = e.throwable();
			if (traces) {
				if (thrown != null) thrown.printStackTrace(SysLog.stream);
				e.printStackTrace(SysLog.stream);
			}
			return;
		} catch (Throwable t) {
			SysLog.println("DOMBrowser.main: caught Throwable " + t.toString());
			if (traces) t.printStackTrace(SysLog.stream);
			return;
		} finally {
			if (Expr.totalHashEntries > 0 && Expr.totalScriptCount > 0) {
				SysLog.println("DOMBrowser.main: compiled " + Expr.totalHashEntries + " of " +  Expr.totalScriptCount + " scripts in [ get: " +
						Expr.totalHashGetTime + ", compile: " + Expr.totalCompileTime + ", put: " + Expr.totalHashPutTime + " ] ms (<" + 
						Expr.totalHashGetTime / Expr.totalScriptCount + ", " +
						Expr.totalCompileTime / Expr.totalHashEntries + ", " +
						Expr.totalHashPutTime / Expr.totalHashEntries + "> ms / script)");
			}
			if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK) && Goto.totalURLAcquisitions > 0) {
				SysLog.println("DOMBrowser.main: acquired and parsed " + Goto.totalURLAcquisitions + " VXML pages in [acq: " +
						Goto.totalAcquisitionTime + ", parse: " + Goto.totalVXMLParsingTime + " ] ms (<" +
						Goto.totalAcquisitionTime / Goto.totalURLAcquisitions + ", " +
						Goto.totalVXMLParsingTime / Goto.totalURLAcquisitions + "> ms / page)");
			}
			// Call exit() to shut down all the descendent threads (and the JVM!)
			if (callExit) System.exit(0);
		}
	}
	
	private static SessionThread createNewFileSessionThread(String bootURI, String scriptFile,
			Vector vxmlDefinitions, String macAddr, String tag,
			boolean preprocess, boolean speechEnabled, boolean interruptsEnabled, boolean traces) {

		InterruptHandler interruptHandler = new InterruptHandler();
		
		Recognizer recognizer = new Recognizer(interruptHandler, speechEnabled, DEFAULT_RECOGNITION_MODE);

		recognizer.createFileMockSpeaker(scriptFile);
		
		return createNewSessionThread(bootURI, interruptHandler, recognizer, vxmlDefinitions, macAddr, tag, preprocess, speechEnabled, interruptsEnabled, traces);

	}
	
	private static SessionThread createNewInteractiveSessionThread(String bootURI, String userId,
			String applicationId, String language, Vector vxmlDefinitions, String macAddr, String tag,
			boolean preprocess, boolean speechEnabled, boolean interruptsEnabled, boolean traces) {
		InterruptHandler interruptHandler = new InterruptHandler();
		
		Recognizer recognizer = new Recognizer(interruptHandler, speechEnabled, DEFAULT_RECOGNITION_MODE);

		recognizer.createInteractiveMockSpeaker(userId, applicationId, language);
		
		return createNewSessionThread(bootURI, interruptHandler, recognizer, vxmlDefinitions, macAddr, tag, preprocess, speechEnabled, interruptsEnabled, traces);

	}
			

	private static SessionThread createNewSessionThread(String bootURI, InterruptHandler interruptHandler, 
			Recognizer recognizer,
			Vector vxmlDefinitions, String macAddr, String tag,
			boolean preprocess, boolean speechEnabled, boolean interruptsEnabled, boolean traces) {

		SessionThread sessionThread = null;

		AsyncAudioPlayer asyncAudioPlayer = new AsyncAudioPlayer(tag);
		AudioPlayer audioPlayer = new AudioPlayer(asyncAudioPlayer);

		SystemVariables systemVariables = new SystemVariables(macAddr);



		// Enable/disable interrupt handling
		recognizer.enableInterrupts(interruptsEnabled);

		if (vxmlDefinitions != null ) {
			Enumeration definitions = ((Vector)vxmlDefinitions).elements();
			String name = null;
			Object value = null;
			try {
				while (definitions.hasMoreElements()) {
					String definition = (String)definitions.nextElement();
					int equalsIndex = definition.indexOf('=');
					if (equalsIndex < 0) equalsIndex = definition.length();
					name = definition.substring(0, equalsIndex).trim();
					value = (equalsIndex + 1 < definition.length()) ? (Object)definition.substring(equalsIndex + 1).trim() : (Object)VXMLTypes.PROTO_UNDEFINED;
					Property property = Property.newInstance(name, value);
					if (property != null) {
						property.enter(null);     // Throws InvalidExpressionException, PropertyVetoException, UndeclaredVariableException, VXMLEvent
						SysLog.println("DOMBrowser.main: defined property \"" + name + "\" as \"" + value + "\"");
					} else {
						// It's not an existing Property, so create a SystemVariables.Definition for it
						systemVariables.addDefinition(new SystemVariables.Definition(name, value)); // Throws InvalidExpressionException
						SysLog.println("DOMBrowser.main: defined \"" + name + "\" as \"" + value + "\"");
					}
				}
			} catch (Exception e) {
				SysLog.println("DOMBrowser.main: ERROR - cannot define \"" + name + "\" as \"" + value + "\" due to " + e.toString());
				return null;
			}
		}

		try {
			long before = System.currentTimeMillis();
			if (SysLog.printLevel > 0) SysLog.println("DOMBrowser.main: Starting the top-level session");
			// Run the top-level session
			sessionThread = new SessionThread(bootURI, speakerURL, optimizationLevel, jsDebug, jsNoSource, 
					recognizer, audioPlayer, interruptHandler, systemVariables, preprocess, canonical);
			
		} catch (Exception exc) {
			if (traces) exc.printStackTrace(System.err);
			SysLog.println("DOMBrowser.main: caught " + exc.toString());
		}

		return sessionThread;
	}

	/** Print the usage */
	private static void printUsage(PCFG pcfg, boolean showhidden) {
		SysLog.println(splash + " " + version + " " + copyright);
		SysLog.println("Usage: java voxware.browser.DOMBrowser [options] [bootURI]");
		SysLog.println("");
		SysLog.println(pcfg.optionsToString("<option>", "<default value>", "<description>", showhidden, "  ", "\n  "));
	}

	/** Parse an InputSource */
	public static Document parse(InputSource source) throws VXMLEvent {
		Document  document  = null;
		// Try each of the registered parsers until one is found to work
		Enumeration parsers = xmlParsers.elements();
		while (parsers.hasMoreElements()) {
			Class parser = (Class)parsers.nextElement();
			try {
				document = ((DOMParserWrapper)parser.newInstance()).parse(source);  // Throws IllegalAccessException, InstantiationException, VXMLEvent
				break;
			} catch (InstantiationException e) {
				SysLog.println("DOMBrowser.parse: WARNING - cannot instantiate " + parser.getName());
				if (!parsers.hasMoreElements()) throw new VXMLEvent("error.xmlparser.instantiation", e, parser.getName());
			} catch (IllegalAccessException e) {
				SysLog.println("DOMBrowser.parse: WARNING - cannot access " + parser.getName());
				
				if (!parsers.hasMoreElements()) throw new VXMLEvent("error.xmlparser.access", e, parser.getName());
			} catch (VXMLEvent e) {
				SysLog.println("DOMBrowser.parse: WARNING - " + parser.getName() + " threw " + e.toString());
				if (!parsers.hasMoreElements()) throw e;
			} catch (Exception e) {
				SysLog.println("DOMBrowser.parse: WARNING - " + parser.getName() + " threw " + e.toString());
				if (!parsers.hasMoreElements()) throw new VXMLEvent("error.xmlparser", e, parser.getName());
			}
		}
		return document;
	}

	/**
	 * Constructs a PCFG object for parsing the command line.
	 * @return
	 * @throws EvaluationException
	 */
	private static PCFG initializePcfg() throws EvaluationException {
		PCFG.Parameter[] parameters = {
				//                 Name                    Type                     Description                                           Option
				new PCFG.Parameter("AuxiliaryClass",        PCFG.STRP | PCFG.List,  "Load and run an auxiliary class",                   "-aclass"),

				new PCFG.Parameter("BootURI",               PCFG.STRP,              "The URI of the first page to load",                "-booturi",
				Goto.DEFAULT_BOOT_URL),
				new PCFG.Parameter("CanonicalXML",          PCFG.TRUEP | PCFG.Veil, "Produce canonical XML output when tracing",      "-canonical"),

				new PCFG.Parameter("CfgFile",               PCFG.STRP | PCFG.List,  "The name of a configuration file",                 "-cfgfile"),

				new PCFG.Parameter("ChronoLog",             PCFG.TRUEP | PCFG.Veil, "Generate chronological log",                     "-chronolog"),

				new PCFG.Parameter("ClassFileExtension",    PCFG.STRP | PCFG.List,  "The file extension for class files",                  "-cext",
								DEFAULT_CLASSFILE_EXTENSION),
				new PCFG.Parameter("ClassLoader",           PCFG.STRP | PCFG.List,  "Register a ClassLoader",                           "-cloader",
										DEFAULT_CLASSLOADER),
				new PCFG.Parameter("CommandLineScan",       PCFG.TRUEP | PCFG.Veil, "Display the results of the command line scan", "-cmdlinescan"),

				new PCFG.Parameter("Debugger",              PCFG.STRP,              "Register a browser Debugger class",               "-debugger"),

				new PCFG.Parameter("DefineJavaProperty",    PCFG.STRP | PCFG.List,  "Define a Java System property",                      "-djava"),

				new PCFG.Parameter("Define",                PCFG.STRP | PCFG.List,  "Define a read-only session variable",               "-define"),

				new PCFG.Parameter("VXMLEncoding",          PCFG.STRP,              "VXML encoding",                                   "-encoding",
												"UTF8"),
				new PCFG.Parameter("CfgEnv",                PCFG.STRP,              "The configuration environment variable",       "-environment",
														PCFG.ConfigFileEnv),
				new PCFG.Parameter("JSDebug",               PCFG.TRUEP | PCFG.Veil, "Generate JavaScript debugging information",          "-jsdbg"),

				new PCFG.Parameter("JSNoSource",            PCFG.TRUEP,             "Suppress source information in Script classes",     "-jsnsrc"),

				new PCFG.Parameter("JSOptimization",        PCFG.INTP,              "The JavaScript optimization level (-1 to 9)",        "-jsopt",
				new Integer(DEFAULT_JAVASCRIPT_OPTIMIZATION)),
				new PCFG.Parameter("KeypadRecognizer",      PCFG.STRP | PCFG.List,  "Register a keypad EngineCentral class",       "-keypadengine",
				DEFAULT_KEYPAD_ENGINE_CENTRAL_CLASSNAME),
				new PCFG.Parameter("SpeechRecognizer",      PCFG.STRP | PCFG.List,  "Register a speech EngineCentral class",       "-speechengine",
				DEFAULT_SPEECH_ENGINE_CENTRAL_CLASSNAME),
				new PCFG.Parameter("TimerGranularity",      PCFG.INTP | PCFG.Veil,  "Timer granularity (1 default, 10 highest)",    "-granularity",
				new Integer(1)),
				new PCFG.Parameter("Help",                  PCFG.HELPP,             "Display the basic help screen",                       "-help"),
				new PCFG.Parameter("LogLevel",              PCFG.INTP,              "Level of logging detail (default is 0)",                "-ll",
				new Integer(DEFAULT_LOGLEVEL)),
				new PCFG.Parameter("LogMask",               PCFG.LONGP | PCFG.Hex,  "Bit mask to enable selective logging",                "-mask",
				new Long(DEFAULT_LOGMASK)),
				new PCFG.Parameter("MaxURLCacheBytes",      PCFG.LONGP,             "Maximum number of bytes in the URL cache",             "-mcb",
				new Long(DEFAULT_MAX_CACHE_BYTES)),
				new PCFG.Parameter("MaxURLCacheEntries",    PCFG.INTP,              "Maximum number of entries in the URL cache",           "-mce",
				new Integer(DEFAULT_MAX_CACHE_ENTRIES)),
				new PCFG.Parameter("MaxURLRequests",        PCFG.INTP,              "Maximum number of URL acquisition requests",     "-mrequests",
				new Integer(BrowserProperty.MAXFETCHTRIES_DEFAULT.intValue())),
				new PCFG.Parameter("MaxScriptCacheEntries", PCFG.INTP,              "Maximum number of entries in the script cache",   "-mscripts",
				new Integer(Expr.DEFAULT_MAX_CACHE_ENTRIES)),
				new PCFG.Parameter("MaxSpkrCacheBytes",     PCFG.LONGP,             "Maximum number of bytes in the speaker cache",        "-mscb",
				new Long(Session.DEFAULT_MAX_SPEAKER_CACHE_BYTES)),
				new PCFG.Parameter("MaxSpkrCacheEntries",   PCFG.INTP,              "Maximum number of entries in the speaker cache",      "-msce",
				new Integer(Session.DEFAULT_MAX_SPEAKER_CACHE_ENTRIES)),
				new PCFG.Parameter("NoAudioOutput",         PCFG.TRUEP,             "Run without audio output",                          "-naudio"),

				new PCFG.Parameter("NetworkAdapterHint",    PCFG.STRP,              "Network adapter hint",                             "-nethint"),

				new PCFG.Parameter("NoExit",                PCFG.TRUEP | PCFG.Veil, "Do not call exit() when shutting down",              "-nexit"),

				new PCFG.Parameter("NoInterrupts",          PCFG.TRUEP,             "Run without interrupt handling",               "-ninterrupts"),

				new PCFG.Parameter("NoKeypadRecognition",   PCFG.TRUEP,             "Run without keypad recognition",                   "-nkeypad"),

				new PCFG.Parameter("NoPreprocessing",       PCFG.TRUEP,             "Delay preprocessing of executable content", "-npreprocessing"),

				new PCFG.Parameter("NoSpeechRecognition",   PCFG.TRUEP,             "Run without speech recognition",              "-nrecognition"),

				new PCFG.Parameter("NoVXMLIO",              PCFG.TRUEP | PCFG.Veil, "Run without VXML I/O ( = -na -ni -nk -nr)",           "-nvio"),

				new PCFG.Parameter("XMLParser",             PCFG.STRP | PCFG.List,  "Specify a DOM parser wrapper by name",               "-parser",
				DEFAULT_PARSER_NAME),
				new PCFG.Parameter("Property",              PCFG.STRP | PCFG.List,  "Set the value of a VXML property",                "-property"),
				new PCFG.Parameter("RecognitionMode",       PCFG.STRP,              "Recognition mode (\"voice\" or \"text\")",           "-rmode",
						DEFAULT_RECOGNITION_MODE),
			    new PCFG.Parameter("Scavenge",              PCFG.TRUEP,             "Enable background garbage collection",            "-scavenge"),

				new PCFG.Parameter("Speaker",               PCFG.STRP,              "Specify the default .voi file by name",            "-speaker",
				Speaker.UNKNOWN_SPEAKER_URL),
				new PCFG.Parameter("SpeakerDirectory",      PCFG.STRP,              "Specify the speaker directory by name",         "-sdirectory",
				""),
				new PCFG.Parameter("SysLog",                PCFG.STRP,              "The path to the system logger",                     "-syslog"),
				new PCFG.Parameter("TimeStampLog",          PCFG.TRUEP,             "Insert timestamps into the log",                 "-timestamp"),
				new PCFG.Parameter("Traces",                PCFG.TRUEP | PCFG.Veil, "Print stack traces",                                 "-trace"),
				new PCFG.Parameter("Interactive",           PCFG.TRUEP,             "Take responses from stdin",                    "-interactive"),
				new PCFG.Parameter("LicenseKey",            PCFG.STRP,              "The Voxware license key",                            "-vlkey"),
				new PCFG.Parameter("URLRoot",               PCFG.STRP,              "The default URL prefix",                           "-urlroot"),
				new PCFG.Parameter("XQuit",                 PCFG.TRUEP | PCFG.Veil, "Quit after processing configuration files",          "-Xquit"),
				new PCFG.Parameter("XHelp",                 PCFG.HELPP | PCFG.Veil, "Display help screen including hidden parameters",    "-Xhelp"
						)
		};

		SysLog.initialize(System.out);
		SysTimer.initialize(System.out);

		SysLog.setPrintLevel(DEFAULT_LOGLEVEL);
		SysLog.setPrintMask(DEFAULT_LOGMASK);

		System.out.println("DOMBrowser.main: constructing PCFG");

		PCFG pcfg = new PCFG(parameters);

		return pcfg;
	}

	/** 
	 * Acquire a Class from a URL or from the classpath
	 * Used by Applets
	 * */
	public static Class loadClass(String source, int fetchtimeout) throws ClassNotFoundException {
		Class klass = null;
		// Try each of the registered loaders until one is found to work
		Enumeration classLoaderEnum = classLoaders.elements();
		while (klass == null && classLoaderEnum.hasMoreElements()) {
			klass = ((voxware.browser.ClassLoader)classLoaderEnum.nextElement()).loadClass(source, classFileExtensions, fetchtimeout);
		}
		if (klass == null) throw new ClassNotFoundException("Cannot load Class for \"" + source + "\"");
		return klass;
	}

	/** Redirect SysLog */
	private static void redirectLog(PCFG pcfg) {
		// Do this only once
		if (!logRedirected) {
			PCFG.Parameter sysLogParameter = pcfg.parameter("SysLog");
			if (sysLogParameter.configured()) {
				String path = sysLogParameter.stringValue();
				try {
					if (path.indexOf(':') > 0) {
						// Launch a VJML client and pipe SysLog to it
						PipedOutputStream pipe = new PipedOutputStream();
						SysLog.println("DOMBrowser.redirectLog: redirecting SysLog to " + path);
						new SysLogThread("tSysLog", pipe, path).start();                      // Throws IOException, VJMLNameConflictException
						SysLog.println("DOMBrowser.redirectLog: redirecting SysLog to " + path);
						SysLog.initialize(new PrintStream(pipe, true));                       // Throws IOException, SecurityException
						SysLog.println("DOMBrowser.redirectLog: redirecting SysLog to " + path);
					} else {
						SysLog.initialize(path, "browser");
					}
					logRedirected = true;
				} catch (SecurityException e) {
					SysLog.println("DOMBrowser.redirectLog: WARNING - insufficient privilege to open system log file \"" + path + "\"");
				} catch (IOException e) {
					SysLog.println("DOMBrowser.redirectLog: WARNING - cannot open system log \"" + path + "\" due to " + e.toString());
				} catch (VJMLNameConflictException e) {
					SysLog.println("DOMBrowser.redirectLog: WARNING - cannot launch VJML SysLog thread due to " + e.toString());
				}
			}
		}
	}

	private static class SysLogThread extends Thread {

		public static final int LineAllocation = 200;

		private PipedInputStream pipe;
		private VJML             vjml;
		private VJMLChannel      channel;

		private SysLogThread(String name, PipedOutputStream pipe, String server) throws IOException, VJMLNameConflictException {
			super(name);
			this.pipe = new PipedInputStream(pipe);               // Throws IOException
			VJMLAttributes attributes = new VJMLAttributes();
			attributes.numPriorities(1);
			int colon = server.indexOf(':');
			if (colon == server.length() - 1)
				server = server.substring(0, colon);
			else
				attributes.defaultReceivePort(Integer.parseInt(server.substring(colon + 1)));
			vjml = new VJML(attributes);
			channel = vjml.channel("SysLog_" + Integer.toHexString((new Random()).nextInt()), server);  // Throws VJMLNameConflictException
		}

		public void run() {
			int character;
			ByteArrayOutputStream line = new ByteArrayOutputStream(LineAllocation);
			try {
				do {
					character = pipe.read();                            // Throws IOException
					line.write(character < 0 ? '\n' : character);
					// If we've accumulated an entire line, send it to the server
					if (character == '\n' || character < 0) {
						channel.putMessage(new VJMLMessage(line.toString(), 0));
						line.reset();
					}
				} while (character >= 0);
			} catch (IOException e) {
				channel.putMessage(new VJMLMessage("DOMBrowser.SysLogThread.run: exiting due to " + e.toString() + "\n", 0));
			}
		}
	}
}
