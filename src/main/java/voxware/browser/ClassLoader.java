package voxware.browser;

import java.util.Vector;

 /** The voxware.browser.ClassLoader Interface */
public interface ClassLoader {
  /**--------------------------------------------------------------------------------*
   * Return Class if loaded, null otherwise
   * @param source 
   *   a String containing the name of the source of the Class
   * @param classFileExtensions
   *   a Vector of Strings; sources ending in one of these will be treated as URLs
   * @param fetchtimeout
   *   an int specifying the maximum fetch time in milliseconds
   * @return 
   *   the loaded Class or <code>null</code> if none loaded
   * @exception ClassNotFoundException
   *   normally not thrown, except to disable subsequent voxware.browser.ClassLoaders;
   *   return <code>null</code> instead
   *--------------------------------------------------------------------------------**/
  public Class loadClass(String source, Vector classFileExtensions, int fetchtimeout)
  throws ClassNotFoundException;
}
