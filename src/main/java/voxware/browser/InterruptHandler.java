package voxware.browser;

import java.util.*;
import javax.speech.recognition.*;
import voxware.hw.event.*;

import voxware.util.SysLog;

 /** The Interrupt Handler */
public class InterruptHandler implements HwEventListener {

   // Instantiate this singleton class by calling the static method InterruptHandler.getInstance()

  private HwEventSource source = null;
  private Stack scope = new Stack();
  private  Vector resultListeners = null;

  private int hwReferenceCount = 0;


   // Instantiate the InterruptHandler -- may safely be called any number of times
  public InterruptHandler() {
      source = HwEventSource.getInstance();
  }

   // Enable the hardware events
  public synchronized void enableEvents() {
    if (hwReferenceCount++ == 0) {
//      source.addHwBatteryChargeListener(this);
//      source.addHwBatteryLowListener(this);
//      source.addHwRadioStatusListener(this);
//      source.addHwStandbyListener(this);
    }
  }

   // Disable the hardware events
  public synchronized void disableEvents() {
    if (hwReferenceCount > 0 && --hwReferenceCount == 0) {
//      source.removeHwBatteryChargeListener(this);
//      source.removeHwBatteryLowListener(this);
//      source.removeHwRadioStatusListener(this);
//      source.removeHwStandbyListener(this);
    }
  }

   // Push the current scope, discarding any pending interrupts
  public synchronized void pushScope(Scope scope) {
    ((ScopeElement)scope).clearInterrupts();
    this.scope.push(scope);
  }

   // Pop the current scope, discarding any pending interrupts
  public synchronized void popScope() {
    ((ScopeElement)this.scope.pop()).clearInterrupts();
  }

   // The generic interrupt method
  public void genericInterrupt(int eventIndex, Object message) {
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.INTERRUPT_MASK))
      SysLog.println("InterruptHandler::genericInterrupt(): called for \"" + SystemEvent.name(eventIndex) + "\" (" + eventIndex + ") event");
    SystemEvent event = new SystemEvent(eventIndex);
    event.setMessage(message);
    interrupt(event);
  }

   // The system monitor interrupt method
  public void sysMonitorInterrupt(String message) {
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.INTERRUPT_MASK))
      SysLog.println("InterruptHandler::sysMonitorInterrupt(): called");
    genericInterrupt(SystemEvent.SYSMONITOR_INDEX, message);
  }

   // The HwEventListener interface

  public void eventArrived(ButtonHwEvent hwEvent) {
    // Do nothing -- DTMF handles these
  }

  public void eventArrived(BatteryChargeHwEvent hwEvent) {
    int newLevel = hwEvent.getCharge();
    SystemEvent event = new SystemEvent(SystemEvent.BATTERYCHARGE_INDEX, newLevel);
    event.setMessage(VXMLTypes.intToNumber(newLevel));
    interrupt(event);
  }

  public void eventArrived(BatteryLowHwEvent hwEvent) {
    SystemEvent event = new SystemEvent(SystemEvent.BATTERYLOW_INDEX);
    event.setMessage(VXMLTypes.booleanToBoolean(true));
    interrupt(event);
  }

  public void eventArrived(RadioStatusHwEvent hwEvent) {
    int newLevel = hwEvent.getStatus();
    SystemEvent event = new SystemEvent(SystemEvent.RADIOSTATUS_INDEX, newLevel);
    event.setMessage(VXMLTypes.intToNumber(newLevel));
    interrupt(event);
  }

  public void eventArrived(StandbyHwEvent hwEvent) {
    int newLevel = hwEvent.getDirection();
    SystemEvent event = new SystemEvent(SystemEvent.STANDBY_INDEX, newLevel);
    event.setMessage(VXMLTypes.intToNumber(newLevel));
    interrupt(event);
  }

   // Conditionally log the event, then determine if it is an interrupt and what to do with it if it is
  private void interrupt(SystemEvent event) {
    ScopeElement currentScope = (ScopeElement)scope.peek();
     // Conditionally log the event
    try {
      try {
        currentScope.logEvent(event.index, event.message().toString());  // Throws VXMLEvent
      } catch (VXMLEvent e) {
        currentScope.logEvent(SystemEvent.EXCEPTION_INDEX, e.toString());
      }
    } catch (VXMLEvent e) {
       // Throwing an exception should never fail
      throw new RuntimeException("InterruptHandler.interrupt: UNEXPECTED " + e.toString());
    }
     // If interrupts are enabled ...
    if (resultListeners != null) {
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.INTERRUPT_MASK))
        SysLog.println("InterruptHandler.interrupt: reporting " + SystemEvent.name(event.index) + " (" + event.index + ") interrupt");
       // If this event is an interrupt, send it to the resultListeners
      Interrupt interrupt = currentScope.reportInterrupt(event);
      if (interrupt != null) reportResult(interrupt);
     // Otherwise, queue it iff it is an interrupt
    } else {
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.INTERRUPT_MASK))
        SysLog.println("InterruptHandler.interrupt: recording " + SystemEvent.name(event.index) + " (" + event.index + ") interrupt");
      currentScope.recordInterrupt(event);
    }
  }

   // Report an interrupt to all ResultListeners
  private void reportResult(Interrupt interrupt) {
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.INTERRUPT_MASK))
      SysLog.println("InterruptHandler.reportResult: reporting " + SystemEvent.name(interrupt.template.index) + " (" + interrupt.template.index + ") interrupt");
    InterruptResult interruptResult = new InterruptResult(interrupt);
    interruptResult.setResultState(Result.ACCEPTED);
    ResultEvent result = new ResultEvent(interruptResult, interrupt.template.index);
     // Clone the resultListeners to allow modification without possibility of a deadlock while reporting results
    Enumeration listeners = null;
    synchronized (this) {
      if (resultListeners != null) listeners = ((Vector) resultListeners.clone()).elements();
    }
    if (listeners != null)
      while (listeners.hasMoreElements()) ((ResultListener)listeners.nextElement()).resultAccepted(result);
  }

   // Add a resultListener
  public void addResultListener(ResultListener resultListener) {
    synchronized (this) {
      if (resultListeners == null) resultListeners = new Vector();
      resultListeners.addElement(resultListener);
    }
     // Send the oldest pending interrupt, if any, to the resultListeners
    Interrupt interrupt = ((ScopeElement)scope.peek()).oldestInterrupt();
    if (interrupt != null) reportResult(interrupt);
  }

   // Remove a resultListener
  public void removeResultListener(ResultListener resultListener) {
    synchronized (this) {
      if (resultListeners != null && resultListeners.removeElement(resultListener)) {
        if (resultListeners.size() == 0) resultListeners = null;
      } else if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.INTERRUPT_MASK)) {
        SysLog.println("InterruptHandler.removeResultListener: ERROR - can't remove ResultListener");
      }
    }
  }

} // class InterruptHandler
