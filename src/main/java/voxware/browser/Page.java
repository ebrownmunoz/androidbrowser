package voxware.browser;

import java.util.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.*;

 /** A Page (document)
   * A document's parent is an Application and its children are Forms */
class Page extends ScopeElement {

  protected Element      page;
  protected String       rootURL;
  protected boolean      fastcache;
  protected String       version;     // Currently ignored

   /** Construct a childless Page */
  public Page(Element page) throws DOMException {
    super();
    prefix = "document";

    this.page = page;
    this.setIndex(Elements.VXML_INDEX);

    handler = new EventHandler();
    grammars = new ObjectSet();
    children = new HashVector();

    speaker = null;

     // Construct it from the <vxml> element if there is one
    if (page != null) {
       /* Get the URL of the root document */
      Attr attr = page.getAttributeNode("application");
      rootURL = (attr != null) ? attr.getNodeValue() : null;                        // Throws DOMException
       /* Get the caching policy for the document */
      attr = page.getAttributeNode("caching");
      fastcache = (attr == null || !attr.getNodeValue().equalsIgnoreCase("safe"));  // Throws DOMException
       /* Get the VXML version (REQUIRED) */
      attr = page.getAttributeNode("version");
      version = (attr != null) ? attr.getNodeValue() : "1.0";                       // Throws DOMException
    } else {
      rootURL = null;
      fastcache = true;
    }
  }

   /** Construct a childless Page */
  public Page() throws DOMException {
    this(null);
  }

  public String rootURL() {
    return rootURL;
  }

  public void rootURL(String rootURL) {
    this.rootURL = rootURL;
  }

   /** Interpret the document */
  public Variables interpret(Goto whereToGo, InterruptHandler interruptHandler) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
     // This method never returns normally; it exits only via an Exception
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.INTERPRET_MASK))
      SysLog.println("Page.interpret: starting to interpret \"" + whereToGo.next() + "\"");
    try {
      interruptHandler.pushScope(this);
      modifyProperties();
      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.enter(page, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request entering <vxml> " + name);
      this.initializeVariables();                         // Throws VXMLEvent
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.VARIABLES_MASK))
        SysLog.println("Page.interpret: variables = " + scope.toString());
      this.executeScript();                               // Throws VXMLEvent
      Form form = null;
       // If directed to a particular form by the Goto, go there
      if (whereToGo.form() != null) {
        form = (Form)children.get(whereToGo.form());
       // Otherwise, go to the first form
      } else {
        try {
          form = (Form)children.firstElement();
        } catch (NoSuchElementException nsee) {
          form = null;
        }
      }
      while (true) {
        try {
          try {
            if (form == null) {
              if (SysLog.printLevel > 0) {
                if (whereToGo.form() != null)
                  SysLog.println("Page.interpret: ERROR -- no target form for goto \"" + whereToGo.form() + "\" on page");
                else
                  SysLog.println("Page.interpret: ERROR -- no form on page");
              }
              throw new VXMLEvent("error.badfetch.notargetform", whereToGo.form() != null ? whereToGo.form() : "<null>");
            }
            form.interpret(whereToGo, interruptHandler);  // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
          } catch (VXMLEvent event) {
             // Catch a VXML event in document scope
            if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.CATCH_MASK))
              SysLog.println("Page.interpret: <vxml> caught VXMLEvent \"" + event.name() + "\"");
            catchEvent(event, whereToGo);                 // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
            throw new VXMLExit("Document-level catcher for event \'" + event.name() + "\' falls through");
          }
          throw new VXMLExit("Form exited without goto");
        } catch (Goto gt) {
           // If it's not a form level Goto, just re-throw it
          if (!gt.isFormLevel()) throw gt;
          whereToGo = gt;
          if (whereToGo.form() != null) {
            if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK))
              SysLog.println("Page.interpret: Form level Goto caught for \"" + whereToGo.form() + "\"");
            form = (Form)children.get(whereToGo.form());
          } else {
            try {
              if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GOTO_MASK))
                SysLog.println("Page.interpret: Form level Goto caught for \"<null>\"");
              form = (Form)children.firstElement();
            } catch (NoSuchElementException nsee) {
              form = null;
            }
          }
        }
      }
    } finally {
      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.leave(page, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request leaving <vxml> " + name);
      restoreProperties();
      interruptHandler.popScope();
    }
  }
} // class Page
