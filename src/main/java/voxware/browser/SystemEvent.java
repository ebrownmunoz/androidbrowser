 /** SYSTEM EVENTS **/

package voxware.browser;

import java.util.*;
import voxware.hw.event.*;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

 /** The SystemEvent Class **/
public class SystemEvent implements Cloneable {

   // Constants

  public static final int    FALLING = -1;
  public static final int    RISING = 1;
  public static final int    DEFAULT_SENSE = FALLING;

  public static final int    NO_LEVEL = -1;
  public static final int    DEFAULT_LEVEL = 0;

  public static final int    NO_INDEX = -1;
  public static final String UNKNOWN_NAME = "unknown";
  public static final int    NULL_INDEX = 0;

  public static final int    BATTERYCHARGE_INDEX = NULL_INDEX + 1;
  public static final String BATTERYCHARGE_NAME = "batterycharge";
  public static Object batterycharge(ScriptableObject ignored) { return value(BATTERYCHARGE_INDEX); }

  public static final int    BATTERYLOW_INDEX = BATTERYCHARGE_INDEX + 1;
  public static final String BATTERYLOW_NAME = "batterylow";

  public static final int    KEYPRESS_INDEX = BATTERYLOW_INDEX + 1;
  public static final String KEYPRESS_NAME = "keypress";

  public static final int    POWERMANAGEMENT_INDEX = KEYPRESS_INDEX + 1;
  public static final String POWERMANAGEMENT_NAME = "powermanagement";
  public static Object powermanagement(ScriptableObject ignored) { return value(POWERMANAGEMENT_INDEX); }

  public static final int    RADIOSTATUS_INDEX = POWERMANAGEMENT_INDEX + 1;
  public static final String RADIOSTATUS_NAME = "radiostatus";
  public static final int    RADIOSTATUS_SAMPLE_INTERVAL = 1500;  // Milliseconds (should be less than PING_SEC in jniDrv.c)
  public static Object radiostatus(ScriptableObject ignored) { return value(RADIOSTATUS_INDEX); }

  public static final int    RADIOSTATUSPINGIP_INDEX = RADIOSTATUS_INDEX + 1;
  public static final String RADIOSTATUSPINGIP_NAME = "radiostatuspingip";
  public static Object radiostatuspingip(ScriptableObject ignored) { return value(RADIOSTATUSPINGIP_INDEX); }

  public static final int    MICROPHONEGAIN_INDEX = RADIOSTATUSPINGIP_INDEX + 1;
  public static final String MICROPHONEGAIN_NAME = "microphonegain";
  public static Object microphonegain(ScriptableObject ignored) {
	  return value(MICROPHONEGAIN_INDEX); 
  }

  public static final int    SIDETONEGAIN_INDEX = MICROPHONEGAIN_INDEX + 1;
  public static final String SIDETONEGAIN_NAME = "sidetonegain";
  public static Object sidetonegain(ScriptableObject ignored) { return value(SIDETONEGAIN_INDEX); }

  public static final int    SPEAKERMUTE_INDEX = SIDETONEGAIN_INDEX + 1;
  public static final String SPEAKERMUTE_NAME = "speakermute";
  public static Object speakermute(ScriptableObject ignored) { return value(SPEAKERMUTE_INDEX); }

  public static final int    SPEAKERVOLUME_INDEX = SPEAKERMUTE_INDEX + 1;
  public static final String SPEAKERVOLUME_NAME = "speakervolume";
  public static Object speakervolume(ScriptableObject ignored) { return value(SPEAKERVOLUME_INDEX); }

  public static final int    STANDBY_INDEX = SPEAKERVOLUME_INDEX + 1;
  public static final String STANDBY_NAME = "standby";

  public static final int    RECOGSTART_INDEX = STANDBY_INDEX + 1;
  public static final String RECOGSTART_NAME = "recogstart";

  public static final int    RECOGNITION_INDEX = RECOGSTART_INDEX + 1;
  public static final String RECOGNITION_NAME = "recognition";

  public static final int    AUDIOOUTPUT_INDEX = RECOGNITION_INDEX + 1;
  public static final String AUDIOOUTPUT_NAME = "audiooutput";

  public static final int    URLREQUEST_INDEX = AUDIOOUTPUT_INDEX + 1;
  public static final String URLREQUEST_NAME = "urlrequest";

  public static final int    SYSMONITOR_INDEX = URLREQUEST_INDEX + 1;
  public static final String SYSMONITOR_NAME = "sysmonitor";
  public static final String SYSMONITOR_SYNONYM = "sysmon";

  public static final int    STREAM_INDEX = SYSMONITOR_INDEX + 1;
  public static final String STREAM_NAME = "stream";

  public static final int    APPSERVER_INDEX = STREAM_INDEX + 1;
  public static final String APPSERVER_NAME = "appserver";
  public static final String APPSERVER_SYNONYM = "appsrv";

  public static final int    EXCEPTION_INDEX = APPSERVER_INDEX + 1;
  public static final String EXCEPTION_NAME = "exception";

  public static final int    NUM_SYSEVENTS = EXCEPTION_INDEX + 1;

  private static class EventDescriptor {

    private EventDescriptor(String name, String logName, int currentLevel) {
      this.name = name;
      this.logName = logName;
      this.currentLevel = currentLevel;
    }

    private String          name;

    private String       logName;
    private void      setLogName(String name) { logName = name; }

    private int     currentLevel;
    private void setCurrentLevel(int level)   { currentLevel = level; }
  }

  private static Hashtable eventIndex = new Hashtable(NUM_SYSEVENTS * 2);
  private static Vector eventDescriptor = new Vector(NUM_SYSEVENTS);

  static {

     // Initialize SystemEvent.eventIndex, SystemEvent.name and SystemEvent.logName for all the system events

    HwControls controls = HwControls.getInstance();
    eventDescriptor.setSize(NUM_SYSEVENTS);

    eventDescriptor.set(NULL_INDEX, new EventDescriptor(null, null, NO_LEVEL));

    eventIndex.put(BATTERYCHARGE_NAME, new Integer(BATTERYCHARGE_INDEX));
    eventDescriptor.set(BATTERYCHARGE_INDEX, new EventDescriptor(BATTERYCHARGE_NAME, BATTERYCHARGE_NAME, controls.getBatteryCharge()));

    eventIndex.put(BATTERYLOW_NAME, new Integer(BATTERYLOW_INDEX));
    eventDescriptor.set(BATTERYLOW_INDEX, new EventDescriptor(BATTERYLOW_NAME, BATTERYLOW_NAME, DEFAULT_LEVEL));

    eventIndex.put(KEYPRESS_NAME, new Integer(KEYPRESS_INDEX));
    eventDescriptor.set(KEYPRESS_INDEX, new EventDescriptor(KEYPRESS_NAME, KEYPRESS_NAME, ButtonHwEvent.BUTTON_RELEASED));

    eventIndex.put(POWERMANAGEMENT_NAME, new Integer(POWERMANAGEMENT_INDEX));
    eventDescriptor.set(POWERMANAGEMENT_INDEX, new EventDescriptor(POWERMANAGEMENT_NAME, POWERMANAGEMENT_NAME, NO_LEVEL));

    eventIndex.put(RADIOSTATUS_NAME, new Integer(RADIOSTATUS_INDEX));
    eventDescriptor.set(RADIOSTATUS_INDEX, new EventDescriptor(RADIOSTATUS_NAME, RADIOSTATUS_NAME, controls.getRadioStatus()));

    eventIndex.put(RADIOSTATUSPINGIP_NAME, new Integer(RADIOSTATUSPINGIP_INDEX));
    eventDescriptor.set(RADIOSTATUSPINGIP_INDEX, new EventDescriptor(RADIOSTATUSPINGIP_NAME, RADIOSTATUSPINGIP_NAME, NO_LEVEL));

    eventIndex.put(MICROPHONEGAIN_NAME, new Integer(MICROPHONEGAIN_INDEX));
    eventDescriptor.set(MICROPHONEGAIN_INDEX, new EventDescriptor(MICROPHONEGAIN_NAME, MICROPHONEGAIN_NAME, NO_LEVEL));

    eventIndex.put(SIDETONEGAIN_NAME, new Integer(SIDETONEGAIN_INDEX));
    eventDescriptor.set(SIDETONEGAIN_INDEX, new EventDescriptor(SIDETONEGAIN_NAME, SIDETONEGAIN_NAME, NO_LEVEL));

    eventIndex.put(SPEAKERMUTE_NAME, new Integer(SPEAKERMUTE_INDEX));
    eventDescriptor.set(SPEAKERMUTE_INDEX, new EventDescriptor(SPEAKERMUTE_NAME, SPEAKERMUTE_NAME, NO_LEVEL));

    eventIndex.put(SPEAKERVOLUME_NAME, new Integer(SPEAKERVOLUME_INDEX));
    eventDescriptor.set(SPEAKERVOLUME_INDEX, new EventDescriptor(SPEAKERVOLUME_NAME, SPEAKERVOLUME_NAME, NO_LEVEL));

    eventIndex.put(STANDBY_NAME, new Integer(STANDBY_INDEX));
    eventDescriptor.set(STANDBY_INDEX, new EventDescriptor(STANDBY_NAME, STANDBY_NAME, StandbyHwEvent.OUT_OF_STANDBY));

    eventIndex.put(RECOGSTART_NAME, new Integer(RECOGSTART_INDEX));
    eventDescriptor.set(RECOGSTART_INDEX, new EventDescriptor(RECOGSTART_NAME, RECOGSTART_NAME, NO_LEVEL));

    eventIndex.put(RECOGNITION_NAME, new Integer(RECOGNITION_INDEX));
    eventDescriptor.set(RECOGNITION_INDEX, new EventDescriptor(RECOGNITION_NAME, RECOGNITION_NAME, NO_LEVEL));

    eventIndex.put(AUDIOOUTPUT_NAME, new Integer(AUDIOOUTPUT_INDEX));
    eventDescriptor.set(AUDIOOUTPUT_INDEX, new EventDescriptor(AUDIOOUTPUT_NAME, AUDIOOUTPUT_NAME, NO_LEVEL));

    eventIndex.put(URLREQUEST_NAME, new Integer(URLREQUEST_INDEX));
    eventDescriptor.set(URLREQUEST_INDEX, new EventDescriptor(URLREQUEST_NAME, URLREQUEST_NAME, NO_LEVEL));

    eventIndex.put(SYSMONITOR_NAME, new Integer(SYSMONITOR_INDEX));
    eventIndex.put(SYSMONITOR_SYNONYM, new Integer(SYSMONITOR_INDEX));
    eventDescriptor.set(SYSMONITOR_INDEX, new EventDescriptor(SYSMONITOR_NAME, SYSMONITOR_NAME, DEFAULT_LEVEL));

    eventIndex.put(STREAM_NAME, new Integer(STREAM_INDEX));
    eventDescriptor.set(STREAM_INDEX, new EventDescriptor(STREAM_NAME, STREAM_NAME, DEFAULT_LEVEL));

    eventIndex.put(APPSERVER_NAME, new Integer(APPSERVER_INDEX));
    eventIndex.put(APPSERVER_SYNONYM, new Integer(APPSERVER_INDEX));
    eventDescriptor.set(APPSERVER_INDEX, new EventDescriptor(APPSERVER_NAME, APPSERVER_NAME, DEFAULT_LEVEL));

    eventIndex.put(EXCEPTION_NAME, new Integer(EXCEPTION_INDEX));
    eventDescriptor.set(EXCEPTION_INDEX, new EventDescriptor(EXCEPTION_NAME, EXCEPTION_NAME, NO_LEVEL));
  }

   // Fields

  public int index;
  public int level;
  public int delta;
  public Object message;

   // Constructors

   // Construct a SystemEvent from an event index and a level
  public SystemEvent(int index, int level) {
    EventDescriptor event = (EventDescriptor) eventDescriptor.elementAt(index);
    if (event != null) {
      this.index = index;
      this.level = level;
      this.delta = level - event.currentLevel;
       // Set the new level for this type of event
      event.setCurrentLevel(level);
    }
  }

   // Construct a SystemEvent from an event index for an event without an associated level
  public SystemEvent(int index) {
    this.index = index;
    this.level = DEFAULT_LEVEL;
    this.delta = DEFAULT_SENSE;
  }

   // Construct a SystemEvent for the indexed event from a level description String and a delta
  public SystemEvent(int index, String levelString, int delta) throws NumberFormatException, IllegalArgumentException {
    this.index = index;
    level = NO_LEVEL;
    switch (index) {
      case BATTERYCHARGE_INDEX:
         // Translate levelString to a percentage batterycharge level; pass the delta through
        level = (new Integer(levelString)).intValue();         // Throws NumberFormatException
        if (level < 0) level = 0;
        else if (level > 100) level = 100;
        this.delta = delta;
        break;
      case BATTERYLOW_INDEX:
         // Ignore the levelString and the delta
        level = DEFAULT_LEVEL;
        this.delta = DEFAULT_SENSE;
        break;
      case KEYPRESS_INDEX:
         // Translate levelString to a keypress level; calculate the delta
        if (levelString.equalsIgnoreCase("pressed")) {
          level = ButtonHwEvent.BUTTON_PRESSED;
          this.delta = ButtonHwEvent.BUTTON_PRESSED - ButtonHwEvent.BUTTON_RELEASED;
        } else if (levelString.equalsIgnoreCase("released")) {
          level = ButtonHwEvent.BUTTON_RELEASED;
          this.delta = ButtonHwEvent.BUTTON_RELEASED - ButtonHwEvent.BUTTON_PRESSED;
        } else {
          throw new IllegalArgumentException(levelString);     // Throws IllegalArgumentException
        }
        break;
      case RADIOSTATUS_INDEX:
         // Translate levelString to a radiostatus level; pass the delta through
        if (levelString.equalsIgnoreCase("offline"))
          level = RadioStatusHwEvent.RADIO_STATUS_OFFLINE;
        else if (levelString.equalsIgnoreCase("searching"))
          level = RadioStatusHwEvent.RADIO_STATUS_SEARCHING;
        else if (levelString.equalsIgnoreCase("weak"))
          level = RadioStatusHwEvent.RADIO_STATUS_WEAK;
        else if (levelString.equalsIgnoreCase("medium"))
          level = RadioStatusHwEvent.RADIO_STATUS_MEDIUM;
        else if (levelString.equalsIgnoreCase("strong"))
          level = RadioStatusHwEvent.RADIO_STATUS_STRONG;
        else
          throw new IllegalArgumentException(levelString);     // Throws IllegalArgumentException
        this.delta = delta;
        break;
      case STANDBY_INDEX:
         // Translate levelString to a standby level; calculate the delta
        if (levelString.equalsIgnoreCase("tostandby")) {
          level = StandbyHwEvent.GO_TO_STANDBY;
          this.delta = StandbyHwEvent.GO_TO_STANDBY - StandbyHwEvent.OUT_OF_STANDBY;
        } else if (levelString.equalsIgnoreCase("fromstandby")) {
          level = StandbyHwEvent.OUT_OF_STANDBY;
          this.delta = StandbyHwEvent.OUT_OF_STANDBY - StandbyHwEvent.GO_TO_STANDBY;
        } else {
          throw new IllegalArgumentException(levelString);     // Throws IllegalArgumentException
        }
        break;
      case SystemEvent.SYSMONITOR_INDEX:
      case SystemEvent.STREAM_INDEX:
      case SystemEvent.APPSERVER_INDEX:
         // Ignore the levelString and the delta
        level = DEFAULT_LEVEL;
        this.delta = DEFAULT_SENSE;
        break;
      default:
         // Ignore the levelString and the delta
        level = DEFAULT_LEVEL;
        this.delta = DEFAULT_SENSE;
        break;
    }
  }

   // Methods

   // Determine if this SystemEvent satisfies the requirements embodied in another
  public boolean satisfies(SystemEvent template) {
     // Return true iff the source is the same and the sense is the same and the level is sufficient and the message or source is the same
    return (index == template.index && delta * template.delta > 0 && delta * (level - template.level) >= 0) &&
           (template.message == null ||
            message instanceof String && ((String) message).equals(template.message) ||
            message instanceof SourcedInterrupt && ((SourcedInterrupt) message).satisfies(template.message));
  }

   // Set the message object
  public void setMessage(Object message) {
    this.message = message;
  }

   // Get the message object
  public Object message() {
    return this.message;
  }

   // Static Methods

   // Add a new event type and return its index; returns NO_INDEX if the event type already exists
  public static int addType(String name) {
    Integer index = (Integer) eventIndex.get(name);
    if (index == null) {
      index = new Integer(eventDescriptor.size());
      eventIndex.put(name, index);
      eventDescriptor.addElement(new EventDescriptor(name, name, NO_LEVEL));
    } else {
      index = new Integer(NO_INDEX);
    }
    return index.intValue();
  }

   // Get the index of the named system event
  public static int index(String name) {
    Integer iindex = (Integer) eventIndex.get(name);
    return (iindex == null) ? NULL_INDEX : iindex.intValue();
  }

   // Get the name of the indexed system event
  public static String name(int index) {
    return (index > NULL_INDEX && index < eventDescriptor.size()) ? ((EventDescriptor)eventDescriptor.elementAt(index)).name : UNKNOWN_NAME;
  }

   // Get the logName of the indexed system event
  public static String logName(int index) {
    return (index > NULL_INDEX && index < eventDescriptor.size()) ? ((EventDescriptor)eventDescriptor.elementAt(index)).logName : null;
  }

   // Get the logName of the named system event
  public static String logName(String name) {
    return logName(SystemEvent.index(name));
  }

   // Set the logName of the indexed system event
  public static void setLogName(int index, String newLogName) {
    if (index > NULL_INDEX && index < eventDescriptor.size()) ((EventDescriptor)eventDescriptor.elementAt(index)).setLogName(newLogName);
  }

   // Set the logName of the named system event
  public static void setLogName(String name, String newLogName) {
    setLogName(SystemEvent.index(name), newLogName);
  }

   // Get the SystemMonitor (DEPRECATED)
  public static synchronized SystemMonitor systemMonitor() {
    return Logger.getSysMonitor();
  }

   // Set the SystemMonitor (DEPRECATED)
  public static synchronized void setSystemMonitor(SystemMonitor monitor) {
    Logger.setSysMonitor(monitor);
  }

   // Get the AppServer (DEPRECATED)
  public static synchronized SystemMonitor appServer() {
    return Logger.getAppServer();
  }

   // Set the AppServer (DEPRECATED)
  public static synchronized void setAppServer(SystemMonitor monitor) {
    Logger.setAppServer(monitor);
  }

   // Get the current value of the indexed system variable as a VXMLType
  public static Object value(int index) {
    switch(index) {
      case NULL_INDEX:
        return null;
      case BATTERYCHARGE_INDEX:
        return VXMLTypes.intToNumber(HwControls.getInstance().getBatteryCharge());
      case POWERMANAGEMENT_INDEX:
        return VXMLTypes.booleanToBoolean(HwControls.getInstance().getPowerMgmt());
      case RADIOSTATUS_INDEX:
        return VXMLTypes.intToNumber(HwControls.getInstance().getRadioStatus());
      case RADIOSTATUSPINGIP_INDEX:
        return HwEventSource.getInstance().getPingIpAddr();
      case MICROPHONEGAIN_INDEX:
    	  // return VXMLTypes.floatToNumber(voxware.browser.Recognizer.getInputGain());
    	  return VXMLTypes.floatToNumber(1.0f);
      case SIDETONEGAIN_INDEX:
        return VXMLTypes.floatToNumber(HwControls.getInstance().getSidetoneGain());
      case SPEAKERMUTE_INDEX:
        //return VXMLTypes.booleanToBoolean(AudioPlayer.getMute());
    	  return false;
      case SPEAKERVOLUME_INDEX:
        {
           // Try to get the gain from AudioPlayer first; if this fails, try HwControls.
          float gain =  1.0f; //AudioPlayer.getGain();
          if (Float.isNaN(gain)) gain = HwControls.getInstance().getOutputVolume();
          return VXMLTypes.floatToNumber(gain);
        }
      default:
        return VXMLTypes.PROTO_UNDEFINED;
    }
  }

   // Get the current value of the named system variable as a VXMLType
  public static Object value(String name) {
    return value(index(name));
  }

   // Set the value of the indexed system variable
  public static void setValue(int index, Object value) {
    switch(index) {
      case POWERMANAGEMENT_INDEX:
        HwControls.getInstance().setPowerMgmt(ScriptRuntime.toBoolean(value));
        return;
      case RADIOSTATUSPINGIP_INDEX:
        HwEventSource.getInstance().setPingIpAddr(ScriptRuntime.toString(value));
        return;
       case MICROPHONEGAIN_INDEX:
        // voxware.browser.Recognizer.setInputGain((float)ScriptRuntime.toNumber(value));
        return;
      case SIDETONEGAIN_INDEX:
        HwControls.getInstance().setSidetoneGain((float)ScriptRuntime.toNumber(value));
        return;
      case SPEAKERMUTE_INDEX:
        // AudioPlayer.setMute(ScriptRuntime.toBoolean(value));
        return;
      case SPEAKERVOLUME_INDEX:
        // if (!AudioPlayer.setGain((float)ScriptRuntime.toNumber(value)))
        //  HwControls.getInstance().setOutputVolume((float)ScriptRuntime.toNumber(value));
        return;
      default:
        return;
    }
  }

   // Set the value of the named system variable
  public static void setValue(String name, Object value) {
    setValue(index(name), value);
  }

   // Wait for radiostatus to equal or exceed RADIO_STATUS_WEAK
  public static int waitForRadioStatus(int timeout) {
    return waitForRadioStatus(RadioStatusHwEvent.RADIO_STATUS_WEAK, timeout);
  }

   // Wait for radiostatus to equal or exceed a specified floor value; returns time remaining to timeout
   // A timeout not exceeding RADIOSTATUS_SAMPLE_INTERVAL incurs no latency when out of range; the function returns zero immediately.
   // A timeout longer than this causes the function to sample getRadioStatus() at the RADIOSTATUS_SAMPLE_INTERVAL until the radio
   // status improves (in which case the function returns the remaining timeout) or the remaining timeout no longer exceeds
   // RADIOSTATUS_SAMPLE_INTERVAL (in which case the function returns zero).
  public static int waitForRadioStatus(int statusFloor, int timeout) {
    HwControls hardware = HwControls.getInstance();
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.RADIOSTATUS_MASK))
      SysLog.println("SystemEvent.waitForRadioStatus(): radiostatus = " + hardware.getRadioStatus());
    long end = System.currentTimeMillis() + timeout;
    int status;
    while ((status = hardware.getRadioStatus()) < statusFloor && timeout > RADIOSTATUS_SAMPLE_INTERVAL) {
      if (SysLog.printLevel > 5 && SysLog.printMaskBitsSet(SysLog.RADIOSTATUS_MASK))
        SysLog.println("SystemEvent.waitForRadioStatus(): status = " + hardware.getRadioStatus() + "; remaining = " + timeout);
      try {
        Thread.sleep((long)RADIOSTATUS_SAMPLE_INTERVAL);
      } catch (InterruptedException e) {}
      timeout = (int)(end - System.currentTimeMillis());
    }
    if (status < statusFloor || timeout < 0) timeout = 0;
    return timeout;
  }

  public Object clone() {
    Object retval = null;
    try {
      retval = super.clone();
    } catch (Exception e) {
    }

    return retval;
  }

} // public class SystemEvent
