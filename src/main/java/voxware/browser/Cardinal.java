package voxware.browser;

import java.util.*;

import voxware.browser.*;

 /** Base class for any object that can represent a VXML element */
class Cardinal {

  int index;

  public Cardinal() {
    this.index = Elements.NULL_INDEX;
  }

  public Cardinal(int index) {
    this.index = index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public int index() {
    return this.index;
  }

} // class Cardinal

