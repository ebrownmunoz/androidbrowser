 /** SYSTEM MONITOR INTERFACE **/

package voxware.browser;

 /** The SystemMonitor Interface **/
public interface SystemMonitor {
  public void logEvent(int index, String text);
}
