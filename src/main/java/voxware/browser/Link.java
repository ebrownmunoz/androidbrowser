package voxware.browser;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import voxware.browser.*;
import voxware.util.SysLog;

 /** A Link
   * A Link's parent is a Page, Form or FormItem, and it has no children */
class Link extends ScopeElement {

  private Element      link;
  private Goto         targetGoto;                // The goto to execute if this link is satisfied
  private VXMLEvent    targetEvent;               // The VXML event to throw if this link is satisfied
  private boolean      override = false;          // Override Field modality?

   /** Construct a childless, orphan Link from a <link> element in a DOM tree */
  public Link(Element link, Goto context) throws InvalidTag, DOMException, InvalidExpressionException, NumberFormatException, VXMLEvent {
    super();

    this.link = link;
    this.setIndex(Elements.LINK_INDEX);

     // Get the attributes of the <link> tag
    NamedNodeMap nnm = link.getAttributes();
    Node node;

     // Get the mode, if any
    if ((node = nnm.getNamedItem("mode")) != null && node.getNodeValue().equalsIgnoreCase("hard")) // Throws DOMException
      override = true;
    else
      override = false;

     // Get the target event, if any
    if ((node = nnm.getNamedItem("event")) != null) {
      targetEvent = new VXMLEvent(node.getNodeValue());  // Throws DOMException
    } else if ((node = nnm.getNamedItem("next")) != null || (node = nnm.getNamedItem("expr")) != null) {
      targetGoto = new Goto(link, nnm, context);  // Throws DOMException, InvalidExpressionException, InvalidTag, NumberFormatException, VXMLEvent
    } else {
      if (SysLog.printLevel > 0) SysLog.println("Link.Link(): ERROR -- no target specified");
      throw new InvalidTag(link);
    }
  }

   /** Make the Link a child of a ScopeElement WITHOUT HAVING THE PARENT ADOPT IT */
  public void makeChildOf(ScopeElement parent) {
    this.parent = parent;
     // Inherit the parent's JavaScript scope
    scope = parent.scope();
     // Inherit the parent's Grammars
    grammars = parent.grammars();
     // Inherit the parent's Interrupts
    interrupts = parent.interrupts();
  }

   /** Put a grammar into the parent ScopeElement's Grammars object and give it this Link */
  public void putGrammar(SuperGrammar grammar) {
    grammar.setLink(this);
    ((ScopeElement)parent).putGrammar(grammar);
  }

   /** Put an interrupt into the parent ScopeElement's Interrupts object and give it this Link */
  public void putInterrupt(Interrupt interrupt) {
    interrupt.setLink(this);
    ((ScopeElement)parent).putInterrupt(interrupt);
  }

  public void setTargetGoto(Goto targetGoto) {
    this.targetGoto = targetGoto;
    targetEvent = null;
  }

  public Goto targetGoto() {
    return targetGoto;
  }

  public void setTargetEvent(VXMLEvent targetEvent) {
    targetGoto = null;
    this.targetEvent = targetEvent;
  }

  public VXMLEvent targetEvent() {
    return targetEvent;
  }

  public void overrideModality(boolean override) {
    this.override = override;
  }

  public boolean overrideModality() {
    return override;
  }
} // class Link
