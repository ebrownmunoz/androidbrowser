package voxware.browser;

import java.util.Enumeration;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.ObjectSet;
import voxware.util.SysLog;
import voxware.util.SysTimer;

 /** A Field
   * A Field's parent is a Form and it has no children */
class Field extends FormItem implements FieldItem {

  private String       type;           // The type of the Field grammar
  private String       slot;           // The name of the grammar field that should fill this field, if different from this field's name
  private boolean      modal = true;   // Overrides VXML default !!!
  private int          timeout = DurationConverter.UNSPECIFIED;       // Recognition timeout in milliseconds (DEPRECATED)
  private Scriptable   shadow;         // The name$ shadow variable to contain raw input

   /** Construct a childless, orphan Field from a <field> element in a DOM tree */
  public Field(Session session, Element field) throws InvalidExpressionException, InvalidTag, DOMException, VXMLEvent {
    super(session, field);          // Throws DOMException, InvalidExpressionException
    
    prefix = "dialog";
    this.setIndex(Elements.FIELD_INDEX);

    handler = new EventHandler();
    grammars = new ObjectSet();
    prompts = new Prompts();

    modal = false;

     // The Field name is REQUIRED
    if (name == null) throw new InvalidTag((Node)field);
     // Get the grammar type (optional)
    Attr attr = field.getAttributeNode("type");
    if (attr != null) type = attr.getNodeValue();                     // Throws DOMException
     // Get the slot name
    attr = field.getAttributeNode("slot");
    if (attr != null) slot = attr.getNodeValue();                     // Throws DOMException
     // Get the "modal" boolean
    attr = field.getAttributeNode("modal");
    if (attr != null) modal = ((Boolean)BooleanConverter.instance().convert(attr.getNodeValue())).booleanValue(); // Throws DOMException, VXMLEvent
     // Get the "noinput" timeout                                     // DEPRECATED
    attr = field.getAttributeNode("timeout");
    if (attr != null) timeout = ((Integer)DurationConverter.instance().convert(attr.getNodeValue())).intValue(); // Throws DOMException, VXMLEvent
  }

   /** FieldItem Implementation */
  public String explicitName() {
    return name;  // Every Field is explicitly named
  }

   /** Make this a child of a ScopeElement (Form) (extends makeChildOf() in FormItem to inherit the parent's JavaScript scope) */
  public void makeChildOf(ScopeElement parent) throws VXMLEvent {
     // Inherit the parent's JavaScript scope 
    scope = parent.scope();
     // Have the parent adopt this child
    super.makeChildOf(parent);  // Throws VXMLEvent
     // Construct a shadow variable
    shadow = newObject();
    scope.put(name + "$", scope, shadow);
    shadow.put("confidence", shadow, VXMLTypes.PROTO_UNDEFINED);
  }

   /** Override the reportInterrupt() method in ScopeElement to handle field modality */
  public Interrupt reportInterrupt(SystemEvent event) {
     // The field interrupts are always active in the field
    Interrupt interrupt = (interrupts != null) ? interrupts.reportEvent(event, false) : null;
     // If the field is modal, higher level interrupts are active only if hard
    return (interrupt != null || parent == null) ? interrupt : ((ScopeElement)parent).reportInterrupt(event, modal);
  }

   /** Override the oldestInterrupt() method in ScopeElement to handle field modality */
  public Interrupt oldestInterrupt() {
     // The field interrupts are always active in the field
    Interrupt interrupt = (interrupts != null) ? interrupts.oldestEvent(false) : null;
     // If the field is modal, higher level interrupts are active only if hard
    return (interrupt != null || parent == null) ? interrupt : ((ScopeElement)parent).oldestInterrupt(modal);
  }

   /** Interpret the Field */
  public Variables interpret(Goto whereToGo, InterruptHandler interruptHandler) throws Goto, VXMLEvent, VXMLExit, VXMLReturn {
     // Implement the contents of "if(a field is chosen)" from Appendix C of the VXML Spec
     // Clear the guard variable, in case this visit is the result of a form item goto
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.INTERPRET_MASK))
      SysLog.println("Field.interpret: starting to interpret <field> \"" + name + "\"");
    Scope.setProp(scope, name, VXMLTypes.PROTO_UNDEFINED);
    Variables retVars = null;
    try {
      interruptHandler.pushScope(this);
      modifyProperties();

      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.enter(element, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request entering <field> " + name);

       // Acquire the Recognizer
      voxware.browser.Recognizer recognizer = session.getRecognizer();
      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.INTERPRET_MASK))
        SysLog.println("Field.interpret: acquired recognizer " + recognizer.toString());

       // Recognize using this scope's speaker and grammars, optionally queueing up the appropriate prompt(s) and playing them
      try {
        recognizer.recognize(this, !whereToGo.noPrompt() || (whereToGo.lastScope() != this.hashcode()));  // Throws InterruptedException, VXMLEvent
      } catch (InterruptedException ie) {
        recognizer.clearResult();
      }

       // Process the recognition results
      try {
        retVars = recognizer.results(this, name, shadow, whereToGo);  // Throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent
        if (retVars != null) {
          boolean singleton = retVars.size() == 1;
           // If result is not a single variable and cannot give the guard variable a value, throw an "event.nomatch" and return nothing
          if (((Boolean)BrowserProperty.value(BrowserProperty.MATCHREQUIRED_INDEX)).booleanValue() && 
              !singleton && !retVars.containsKey(name) && (slot == null || slot != null && !retVars.containsKey(slot))) {
            retVars = null;
            throw new VXMLEvent("event.nomatch");
           // Otherwise, assign the values returned by recognition to the appropriate variables
          } else {
            String srcName;
            String varName;
            Enumeration names = retVars.keys();
            while (names.hasMoreElements()) {
              srcName = (String)names.nextElement();
              Object value = retVars.getByName(srcName).value();
               // If this is the only variable or the slot variable, assign it to the guard variable
              if (singleton || (slot != null && slot.equals(srcName))) {
                varName = name;
                 // The guard variable must occur in retVars for <filled> to work properly
                if (srcName != name) retVars.put(name, value);
               // Otherwise, assign it to the eponymous variable (should we perhaps ignore it? !!!)
              } else {
                varName = srcName;
              }
              setProperty(varName, value);                            // Throws UndeclaredVariableException
            }
             // Set the guard variables of all the Initials to true
            Enumeration initials = ((Form)parent).initials().elements();
            while (initials.hasMoreElements())
              setProperty(((Initial)initials.nextElement()).name(), Boolean.TRUE);
          }
        } else {
          if (SysLog.printLevel > 0) SysLog.println("Field.interpret: ERROR - recognizer returns null Variables");
        }
      } catch (UndeclaredVariableException e) {
        if (SysLog.printLevel > 0) SysLog.println("Field.interpret: ERROR - " + e.toString());
        throw new VXMLEvent("error.semantic.undeclared", e);
      } catch (InvalidExpressionException e) {
        if (SysLog.printLevel > 0) SysLog.println("Field.interpret: ERROR - " + e.toString());
        throw new VXMLEvent("error.syntactic.badexpression", e);
      }

     // Catch any VXMLEvent in this Field's scope
    } catch (VXMLEvent event) {
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.CATCH_MASK))
        SysLog.println("Field.interpret: <field> \"" + name + "\" caught VXMLEvent \"" + event.name() + "\"");
      retVars = catchEvent(event, whereToGo);                         // Throws Goto, VXMLEvent, VXMLExit, VXMLReturn
    } finally {
      if (DOMBrowser.debugger != null && !DOMBrowser.debugger.leave(element, name, Context.getCurrentContext(), scope()))
        throw new VXMLExit("Exit by request leaving <field> " + name);
      restoreProperties();
      interruptHandler.popScope();
    }
    return retVars;
  }
} // class Field
