package voxware.browser;

import java.lang.*;
import java.util.Hashtable;

 /** Elements - a Hashtable of node type indices hashed by node name */
public class Elements {

  // Constants

  public static final int     NULL_INDEX      = 0;

   // Names and indices of all the standard VXML Elements
  public static final int     ASSIGN_INDEX    = NULL_INDEX + 1;
  public static final String  ASSIGN_NAME     = "assign";
  public static final int     AUDIO_INDEX     = ASSIGN_INDEX + 1;
  public static final String  AUDIO_NAME      = "audio";
  public static final int     BLOCK_INDEX     = AUDIO_INDEX + 1;
  public static final String  BLOCK_NAME      = "block";
  public static final int     BREAK_INDEX     = BLOCK_INDEX + 1;
  public static final String  BREAK_NAME      = "break";
  public static final int     CATCH_INDEX     = BREAK_INDEX + 1;
  public static final String  CATCH_NAME      = "catch";
  public static final int     CHOICE_INDEX    = CATCH_INDEX + 1;
  public static final String  CHOICE_NAME     = "choice";
  public static final int     CLEAR_INDEX     = CHOICE_INDEX + 1;
  public static final String  CLEAR_NAME      = "clear";
  public static final int     DIV_INDEX       = CLEAR_INDEX + 1;
  public static final String  DIV_NAME        = "div";
  public static final int     ELSE_INDEX      = DIV_INDEX + 1;
  public static final String  ELSE_NAME       = "else";
  public static final int     ELSEIF_INDEX    = ELSE_INDEX + 1;
  public static final String  ELSEIF_NAME     = "elseif";
  public static final int     EMP_INDEX       = ELSEIF_INDEX + 1;
  public static final String  EMP_NAME        = "emp";
  public static final int     ENUMERATE_INDEX = EMP_INDEX + 1;
  public static final String  ENUMERATE_NAME  = "enumerate";
  public static final int     ERROR_INDEX     = ENUMERATE_INDEX + 1;
  public static final String  ERROR_NAME      = "error";
  public static final int     EXIT_INDEX      = ERROR_INDEX + 1;
  public static final String  EXIT_NAME       = "exit";
  public static final int     FIELD_INDEX     = EXIT_INDEX + 1;
  public static final String  FIELD_NAME      = "field";
  public static final int     FILLED_INDEX    = FIELD_INDEX + 1;
  public static final String  FILLED_NAME     = "filled";
  public static final int     FORM_INDEX      = FILLED_INDEX + 1;
  public static final String  FORM_NAME       = "form";
  public static final int     GOTO_INDEX      = FORM_INDEX + 1;
  public static final String  GOTO_NAME       = "goto";
  public static final int     GRAMMAR_INDEX   = GOTO_INDEX + 1;
  public static final String  GRAMMAR_NAME    = "grammar";
  public static final int     HELP_INDEX      = GRAMMAR_INDEX + 1;
  public static final String  HELP_NAME       = "help";
  public static final int     IF_INDEX        = HELP_INDEX + 1;
  public static final String  IF_NAME         = "if";
  public static final int     INITIAL_INDEX   = IF_INDEX + 1;
  public static final String  INITIAL_NAME    = "initial";
  public static final int     LINK_INDEX      = INITIAL_INDEX + 1;
  public static final String  LINK_NAME       = "link";
  public static final int     MENU_INDEX      = LINK_INDEX + 1;
  public static final String  MENU_NAME       = "menu";
  public static final int     META_INDEX      = MENU_INDEX + 1;
  public static final String  META_NAME       = "meta";
  public static final int     NOMATCH_INDEX   = META_INDEX + 1;
  public static final String  NOMATCH_NAME    = "nomatch";
  public static final int     NOINPUT_INDEX   = NOMATCH_INDEX + 1;
  public static final String  NOINPUT_NAME    = "noinput";
  public static final int     OBJECT_INDEX    = NOINPUT_INDEX + 1;
  public static final String  OBJECT_NAME     = "object";
  public static final int     PARAM_INDEX     = OBJECT_INDEX + 1;
  public static final String  PARAM_NAME      = "param";
  public static final int     PROMPT_INDEX    = PARAM_INDEX + 1;
  public static final String  PROMPT_NAME     = "prompt";
  public static final int     PROPERTY_INDEX  = PROMPT_INDEX + 1;
  public static final String  PROPERTY_NAME   = "property";
  public static final int     PROS_INDEX      = PROPERTY_INDEX + 1;
  public static final String  PROS_NAME       = "pros";
  public static final int     RECORD_INDEX    = PROS_INDEX + 1;
  public static final String  RECORD_NAME     = "record";
  public static final int     REPROMPT_INDEX  = RECORD_INDEX + 1;
  public static final String  REPROMPT_NAME   = "reprompt";
  public static final int     RETURN_INDEX    = REPROMPT_INDEX + 1;
  public static final String  RETURN_NAME     = "return";
  public static final int     SAYAS_INDEX     = RETURN_INDEX + 1;
  public static final String  SAYAS_NAME      = "sayas";
  public static final int     SCRIPT_INDEX    = SAYAS_INDEX + 1;
  public static final String  SCRIPT_NAME     = "script";
  public static final int     SUBDIALOG_INDEX = SCRIPT_INDEX + 1;
  public static final String  SUBDIALOG_NAME  = "subdialog";
  public static final int     SUBMIT_INDEX    = SUBDIALOG_INDEX + 1;
  public static final String  SUBMIT_NAME     = "submit";
  public static final int     THROW_INDEX     = SUBMIT_INDEX + 1;
  public static final String  THROW_NAME      = "throw";
  public static final int     VALUE_INDEX     = THROW_INDEX + 1;
  public static final String  VALUE_NAME      = "value";
  public static final int     VAR_INDEX       = VALUE_INDEX + 1;
  public static final String  VAR_NAME        = "var";
  public static final int     VXML_INDEX      = VAR_INDEX + 1;
  public static final String  VXML_NAME       = "vxml";

   // Names and indices of the VWXML extensions
  public static final int     INTERRUPT_INDEX = VXML_INDEX + 1;
  public static final String  INTERRUPT_NAME  = "interrupt";
  public static final int     LOG_INDEX       = INTERRUPT_INDEX + 1;
  public static final String  LOG_NAME        = "log";
  public static final int     SPEAKER_INDEX   = LOG_INDEX + 1;
  public static final String  SPEAKER_NAME    = "speaker";

  public static final int     NUM_NODE_TYPES  = SPEAKER_INDEX;

  // Variables

  public static Hashtable  hash = new Hashtable(NUM_NODE_TYPES);

  // Static Initializer

  static {
     // Initialize Elements.hash with all the standard VXML Elements
    hash.put(ASSIGN_NAME, new Integer(ASSIGN_INDEX));
    hash.put(AUDIO_NAME, new Integer(AUDIO_INDEX));
    hash.put(BLOCK_NAME, new Integer(BLOCK_INDEX));
    hash.put(BREAK_NAME, new Integer(BREAK_INDEX));
    hash.put(CATCH_NAME, new Integer(CATCH_INDEX));
    hash.put(CHOICE_NAME, new Integer(CHOICE_INDEX));
    hash.put(CLEAR_NAME, new Integer(CLEAR_INDEX));
    hash.put(DIV_NAME, new Integer(DIV_INDEX));
    hash.put(ELSE_NAME, new Integer(ELSE_INDEX));
    hash.put(ELSEIF_NAME, new Integer(ELSEIF_INDEX));
    hash.put(EMP_NAME, new Integer(EMP_INDEX));
    hash.put(ENUMERATE_NAME, new Integer(ENUMERATE_INDEX));
    hash.put(ERROR_NAME, new Integer(ERROR_INDEX));
    hash.put(EXIT_NAME, new Integer(EXIT_INDEX));
    hash.put(FIELD_NAME, new Integer(FIELD_INDEX));
    hash.put(FILLED_NAME, new Integer(FILLED_INDEX));
    hash.put(FORM_NAME, new Integer(FORM_INDEX));
    hash.put(GOTO_NAME, new Integer(GOTO_INDEX));
    hash.put(GRAMMAR_NAME, new Integer(GRAMMAR_INDEX));
    hash.put(HELP_NAME, new Integer(HELP_INDEX));
    hash.put(IF_NAME, new Integer(IF_INDEX));
    hash.put(INITIAL_NAME, new Integer(INITIAL_INDEX));
    hash.put(LINK_NAME, new Integer(LINK_INDEX));
    hash.put(MENU_NAME, new Integer(MENU_INDEX));
    hash.put(META_NAME, new Integer(META_INDEX));
    hash.put(NOMATCH_NAME, new Integer(NOMATCH_INDEX));
    hash.put(NOINPUT_NAME, new Integer(NOINPUT_INDEX));
    hash.put(OBJECT_NAME, new Integer(OBJECT_INDEX));
    hash.put(PARAM_NAME, new Integer(PARAM_INDEX));
    hash.put(PROMPT_NAME, new Integer(PROMPT_INDEX));
    hash.put(PROPERTY_NAME, new Integer(PROPERTY_INDEX));
    hash.put(PROS_NAME, new Integer(PROS_INDEX));
    hash.put(RECORD_NAME, new Integer(RECORD_INDEX));
    hash.put(REPROMPT_NAME, new Integer(REPROMPT_INDEX));
    hash.put(RETURN_NAME, new Integer(RETURN_INDEX));
    hash.put(SAYAS_NAME, new Integer(SAYAS_INDEX));
    hash.put(SCRIPT_NAME, new Integer(SCRIPT_INDEX));
    hash.put(SUBDIALOG_NAME, new Integer(SUBDIALOG_INDEX));
    hash.put(SUBMIT_NAME, new Integer(SUBMIT_INDEX));
    hash.put(THROW_NAME, new Integer(THROW_INDEX));
    hash.put(VALUE_NAME, new Integer(VALUE_INDEX));
    hash.put(VAR_NAME, new Integer(VAR_INDEX));
    hash.put(VXML_NAME, new Integer(VXML_INDEX));

     // Add the VWXML extensions
    hash.put(INTERRUPT_NAME, new Integer(INTERRUPT_INDEX));
    hash.put(LOG_NAME, new Integer(LOG_INDEX));
    hash.put(SPEAKER_NAME, new Integer(SPEAKER_INDEX));
  }

   /* Get the index of an Element with the given name */
  public static int index(String name) {
    int index;
    Integer iindex = (Integer) hash.get(name);

    if (iindex == null) index = NULL_INDEX;
    else                index = iindex.intValue();
    return index;
  }
}
