package voxware.browser;

import java.util.*;

import org.mozilla.javascript.*;

import voxware.browser.*;
import voxware.util.SysLog;

public class BrowserProperty extends Property {

   // Browser properties
  public static final int NULL_INDEX = 0;
  public static final int DEFAULT_FETCH_TIMEOUT = 500;

  public static final int AUDIOFETCHHINT_INDEX = 1;
  public static final String AUDIOFETCHHINT_NAME = "audiofetchhint";
  public static final Integer AUDIOFETCHHINT_DEFAULT = new Integer(FetchHintConverter.PREFETCH_INDEX);
  public static Object audiofetchhint(ScriptableObject ignored) { return FetchHintConverter.convert(((Number)value(AUDIOFETCHHINT_INDEX)).intValue()); }

  public static final int BARGEIN_INDEX = AUDIOFETCHHINT_INDEX + 1;
  public static final String BARGEIN_NAME = "bargein";
  public static final Boolean BARGEIN_DEFAULT = new Boolean(true);
  public static Object bargein(ScriptableObject ignored) { return value(BARGEIN_INDEX); }

  public static final int CACHING_INDEX = BARGEIN_INDEX + 1;
  public static final String CACHING_NAME = "caching";
  public static final Integer CACHING_DEFAULT = new Integer(CachingConverter.FAST_INDEX);
  public static Object caching(ScriptableObject ignored) { return CachingConverter.convert(((Number)value(CACHING_INDEX)).intValue()); }

  public static final int DOCUMENTFETCHHINT_INDEX = CACHING_INDEX + 1;
  public static final String DOCUMENTFETCHHINT_NAME = "documentfetchhint";
  public static final Integer  DOCUMENTFETCHHINT_DEFAULT = new Integer(FetchHintConverter.SAFE_INDEX);
  public static Object documentfetchhint(ScriptableObject ignored) { return FetchHintConverter.convert(((Number)value(DOCUMENTFETCHHINT_INDEX)).intValue()); }

  public static final int FETCHAUDIO_INDEX = DOCUMENTFETCHHINT_INDEX + 1;
  public static final String FETCHAUDIO_NAME = "fetchaudio";
  public static final String FETCHAUDIO_DEFAULT = "";
  public static Object fetchaudio(ScriptableObject ignored) { return value(FETCHAUDIO_INDEX); }

  public static final int FETCHTIMEOUT_INDEX = FETCHAUDIO_INDEX + 1;
  public static final String FETCHTIMEOUT_NAME = "fetchtimeout";

  public static final Integer FETCHTIMEOUT_DEFAULT = new Integer(DEFAULT_FETCH_TIMEOUT);
  public static Object fetchtimeout(ScriptableObject ignored) { return value(FETCHTIMEOUT_INDEX); }

  public static final int GRAMMARFETCHHINT_INDEX = FETCHTIMEOUT_INDEX + 1;
  public static final String GRAMMARFETCHHINT_NAME = "grammarfetchhint";
  public static final Integer GRAMMARFETCHHINT_DEFAULT = new Integer(FetchHintConverter.PREFETCH_INDEX);
  public static Object grammarfetchhint(ScriptableObject ignored) { return FetchHintConverter.convert(((Number)value(GRAMMARFETCHHINT_INDEX)).intValue()); }

  public static final int INPUTMODES_INDEX = GRAMMARFETCHHINT_INDEX + 1;
  public static final String INPUTMODES_NAME = "inputmodes";
  public static final String INPUTMODES_DEFAULT = "voice";
  public static Object inputmodes(ScriptableObject ignored) { return value(INPUTMODES_INDEX); }

  public static final int OBJECTFETCHHINT_INDEX = INPUTMODES_INDEX + 1;
  public static final String OBJECTFETCHHINT_NAME = "objectfetchhint";
  public static final Integer OBJECTFETCHHINT_DEFAULT = new Integer(FetchHintConverter.PREFETCH_INDEX);
  public static Object objectfetchhint(ScriptableObject ignored) { return FetchHintConverter.convert(((Number)value(OBJECTFETCHHINT_INDEX)).intValue()); }

  public static final int SCRIPTFETCHHINT_INDEX = OBJECTFETCHHINT_INDEX + 1;
  public static final String SCRIPTFETCHHINT_NAME = "scriptfetchhint";
  public static final Integer SCRIPTFETCHHINT_DEFAULT = new Integer(FetchHintConverter.PREFETCH_INDEX);
  public static Object scriptfetchhint(ScriptableObject ignored) { return FetchHintConverter.convert(((Number)value(SCRIPTFETCHHINT_INDEX)).intValue()); }

  public static final int SPEAKERFETCHHINT_INDEX = SCRIPTFETCHHINT_INDEX + 1;
  public static final String SPEAKERFETCHHINT_NAME = "speakerfetchhint";
  public static final Integer SPEAKERFETCHHINT_DEFAULT = new Integer(FetchHintConverter.PREFETCH_INDEX);
  public static Object speakerfetchhint(ScriptableObject ignored) { return FetchHintConverter.convert(((Number)value(SPEAKERFETCHHINT_INDEX)).intValue()); }

  public static final int TIMEOUT_INDEX = SPEAKERFETCHHINT_INDEX + 1;
  public static final String TIMEOUT_NAME = "timeout";
  public static final Integer TIMEOUT_DEFAULT = new Integer(60000);
  public static Object timeout(ScriptableObject ignored) { return value(TIMEOUT_INDEX); }

  public static final String MAXNBEST_NAME = "maxnbest";  // VXML standard alias for vise.numhypotheses
  public static final int NUM_ALIASES = 1;

   // Proprietary browser properties
  public static final int EVENTRECURSIONLIMIT_INDEX = TIMEOUT_INDEX + 1;
  public static final String EVENTRECURSIONLIMIT_NAME = "eventrecursionlimit";
  public static final Number EVENTRECURSIONLIMIT_DEFAULT = new Integer(100);
  public static Object eventrecursionlimit(ScriptableObject ignored) { return value(EVENTRECURSIONLIMIT_INDEX); }

  public static final int JAVAGCTHRESHOLD_INDEX = EVENTRECURSIONLIMIT_INDEX + 1;
  public static final String JAVAGCTHRESHOLD_NAME = "memory.java.gcthreshold";
  public static final Number JAVAGCTHRESHOLD_DEFAULT = new Integer(79);
  public static Object gcthreshold(ScriptableObject ignored) { return value(JAVAGCTHRESHOLD_INDEX); }

  public static final int JAVAMAXPERCENTUSED_INDEX = JAVAGCTHRESHOLD_INDEX + 1;
  public static final String JAVAMAXPERCENTUSED_NAME = "memory.java.maxpercentused";
  public static final Number JAVAMAXPERCENTUSED_DEFAULT = new Integer(100);
  public static Object maxpercentused(ScriptableObject ignored) { return value(JAVAMAXPERCENTUSED_INDEX); }

  public static final int MINBYTESFREE_INDEX = JAVAMAXPERCENTUSED_INDEX + 1;
  public static final String MINBYTESFREE_NAME = "memory.system.minbytesfree";
  public static final Number MINBYTESFREE_DEFAULT = new Integer(0);
  public static Object minbytesfree(ScriptableObject ignored) { return value(MINBYTESFREE_INDEX); }

  public static final int MINMAXFREEBLOCKSIZE_INDEX = MINBYTESFREE_INDEX + 1;
  public static final String MINMAXFREEBLOCKSIZE_NAME = "memory.system.minmaxfreeblocksize";
  public static final Number MINMAXFREEBLOCKSIZE_DEFAULT = new Integer(0);
  public static Object minmaxfreeblocksize(ScriptableObject ignored) { return value(MINMAXFREEBLOCKSIZE_INDEX); }

  public static final int MAXCONNECTRETRIES_INDEX = MINMAXFREEBLOCKSIZE_INDEX + 1;
  public static final String MAXCONNECTRETRIES_NAME = "maxconnectretries";
  public static final Number MAXCONNECTRETRIES_DEFAULT = new Integer(0);
  public static Object maxconnectretries(ScriptableObject ignored) { return value(MAXCONNECTRETRIES_INDEX); }

  public static final int MAXFETCHTRIES_INDEX = MAXCONNECTRETRIES_INDEX + 1;
  public static final String MAXFETCHTRIES_NAME = "maxfetchtries";
  public static final Number MAXFETCHTRIES_DEFAULT = new Integer(1);
  public static Object maxfetchtries(ScriptableObject ignored) { return value(MAXFETCHTRIES_INDEX); }

  public static final int MATCHREQUIRED_INDEX = MAXFETCHTRIES_INDEX + 1;
  public static final String MATCHREQUIRED_NAME = "matchrequired";
  public static final Boolean MATCHREQUIRED_DEFAULT = new Boolean(true);
  public static Object matchrequired(ScriptableObject ignored) { return value(MATCHREQUIRED_INDEX); }

  public static final int SUPPRESSSPEECHINPUT_INDEX = MATCHREQUIRED_INDEX + 1;
  public static final String SUPPRESSSPEECHINPUT_NAME = "suppressspeechinput";
  public static final Boolean SUPPRESSSPEECHINPUT_DEFAULT = new Boolean(false);
  public static Object suppressspeechinput(ScriptableObject ignored) { return value(SUPPRESSSPEECHINPUT_INDEX); }

  public static final int SUPPRESSKEYPADINPUT_INDEX = SUPPRESSSPEECHINPUT_INDEX + 1;
  public static final String SUPPRESSKEYPADINPUT_NAME = "suppresskeypadinput";
  public static final Boolean SUPPRESSKEYPADINPUT_DEFAULT = new Boolean(false);
  public static Object suppresskeypadinput(ScriptableObject ignored) { return value(SUPPRESSKEYPADINPUT_INDEX); }

  public static final int NUM_PROPERTIES = SUPPRESSKEYPADINPUT_INDEX + 1;

   // An Array of browser properties
  private static Object[] properties = new Object[NUM_PROPERTIES];
  private static Hashtable indices = new Hashtable(NUM_PROPERTIES);

  static {
    properties[NULL_INDEX] = null;

    indices.put(AUDIOFETCHHINT_NAME, new Integer(AUDIOFETCHHINT_INDEX));
    properties[AUDIOFETCHHINT_INDEX] = AUDIOFETCHHINT_DEFAULT;

    indices.put(BARGEIN_NAME, new Integer(BARGEIN_INDEX));
    properties[BARGEIN_INDEX] = BARGEIN_DEFAULT;

    indices.put(CACHING_NAME, new Integer(CACHING_INDEX));
    properties[CACHING_INDEX] = CACHING_DEFAULT;

    indices.put(DOCUMENTFETCHHINT_NAME, new Integer(DOCUMENTFETCHHINT_INDEX));
    properties[DOCUMENTFETCHHINT_INDEX] = DOCUMENTFETCHHINT_DEFAULT;

    indices.put(FETCHAUDIO_NAME, new Integer(FETCHAUDIO_INDEX));
    properties[FETCHAUDIO_INDEX] = FETCHAUDIO_DEFAULT;

    indices.put(FETCHTIMEOUT_NAME, new Integer(FETCHTIMEOUT_INDEX));
    properties[FETCHTIMEOUT_INDEX] = FETCHTIMEOUT_DEFAULT;

    indices.put(GRAMMARFETCHHINT_NAME, new Integer(GRAMMARFETCHHINT_INDEX));
    properties[GRAMMARFETCHHINT_INDEX] = GRAMMARFETCHHINT_DEFAULT;

    indices.put(INPUTMODES_NAME, new Integer(INPUTMODES_INDEX));
    properties[INPUTMODES_INDEX] = INPUTMODES_DEFAULT;

    indices.put(OBJECTFETCHHINT_NAME, new Integer(OBJECTFETCHHINT_INDEX));
    properties[OBJECTFETCHHINT_INDEX] = OBJECTFETCHHINT_DEFAULT;

    indices.put(SCRIPTFETCHHINT_NAME, new Integer(SCRIPTFETCHHINT_INDEX));
    properties[SCRIPTFETCHHINT_INDEX] = SCRIPTFETCHHINT_DEFAULT;

    indices.put(SPEAKERFETCHHINT_NAME, new Integer(SPEAKERFETCHHINT_INDEX));
    properties[SPEAKERFETCHHINT_INDEX] = SPEAKERFETCHHINT_DEFAULT;

    indices.put(TIMEOUT_NAME, new Integer(TIMEOUT_INDEX));
    properties[TIMEOUT_INDEX] = TIMEOUT_DEFAULT;

    indices.put(EVENTRECURSIONLIMIT_NAME, new Integer(EVENTRECURSIONLIMIT_INDEX));
    properties[EVENTRECURSIONLIMIT_INDEX] = EVENTRECURSIONLIMIT_DEFAULT;

    indices.put(JAVAGCTHRESHOLD_NAME, new Integer(JAVAGCTHRESHOLD_INDEX));
    properties[JAVAGCTHRESHOLD_INDEX] = JAVAGCTHRESHOLD_DEFAULT;

    indices.put(JAVAMAXPERCENTUSED_NAME, new Integer(JAVAMAXPERCENTUSED_INDEX));
    properties[JAVAMAXPERCENTUSED_INDEX] = JAVAMAXPERCENTUSED_DEFAULT;

    indices.put(MINBYTESFREE_NAME, new Integer(MINBYTESFREE_INDEX));
    properties[MINBYTESFREE_INDEX] = MINBYTESFREE_DEFAULT;

    indices.put(MINMAXFREEBLOCKSIZE_NAME, new Integer(MINMAXFREEBLOCKSIZE_INDEX));
    properties[MINMAXFREEBLOCKSIZE_INDEX] = MINMAXFREEBLOCKSIZE_DEFAULT;

    indices.put(MAXCONNECTRETRIES_NAME, new Integer(MAXCONNECTRETRIES_INDEX));
    properties[MAXCONNECTRETRIES_INDEX] = MAXCONNECTRETRIES_DEFAULT;

    indices.put(MAXFETCHTRIES_NAME, new Integer(MAXFETCHTRIES_INDEX));
    properties[MAXFETCHTRIES_INDEX] = MAXFETCHTRIES_DEFAULT;

    indices.put(MATCHREQUIRED_NAME, new Integer(MATCHREQUIRED_INDEX));
    properties[MATCHREQUIRED_INDEX] = MATCHREQUIRED_DEFAULT;

    indices.put(SUPPRESSSPEECHINPUT_NAME, new Integer(SUPPRESSSPEECHINPUT_INDEX));
    properties[SUPPRESSSPEECHINPUT_INDEX] = SUPPRESSSPEECHINPUT_DEFAULT;

    indices.put(SUPPRESSKEYPADINPUT_NAME, new Integer(SUPPRESSKEYPADINPUT_INDEX));
    properties[SUPPRESSKEYPADINPUT_INDEX] = SUPPRESSKEYPADINPUT_DEFAULT;
  }

   // Get the index of the named browser property
  public static int index(String name) {
    Integer iindex = (Integer) indices.get(name);
    return (iindex == null) ? NULL_INDEX : iindex.intValue();
  }

   // Get the value of the indexed browser property
  public static Object value(int index) {
    return (NULL_INDEX < index && index < NUM_PROPERTIES) ? properties[index] : null;
  }

   // Set the value of the indexed browser property
  public static void value(int index, Object value) {
    if (NULL_INDEX < index && index < NUM_PROPERTIES) properties[index] = value;
  }

   // Get the value of the named browser property
  public static Object value(String name) {
    return value(index(name));
  }

   // Set the value of the named browser property
  public static void value(String name, Object value) {
    value(index(name), value);
  }

  private Object    oldValue;

  public BrowserProperty(Object value, Integer index) {
    super(value, index);
  }

  public BrowserProperty(Object value, Integer index, Converter converter) throws VXMLEvent {
    super(value, index, converter);
  }

  public void enter(Scope scope) throws InvalidExpressionException, UndeclaredVariableException, VXMLEvent {
    oldValue = properties[index];
    properties[index] = (newValue instanceof Expr) ? converter.convert(((Expr)newValue).evaluate(scope)) : newValue;
    if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.PROPERTY_MASK))
      SysLog.println("BrowserProperty.enter: property " + index + " set to " + properties[index].toString());
  }

  public void leave() {
    if (oldValue != null) {
      properties[index] = oldValue;
      if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.PROPERTY_MASK))
        SysLog.println("BrowserProperty.leave: property " + index + " restored to " + oldValue.toString());
      oldValue = null;
    }
  }
}
