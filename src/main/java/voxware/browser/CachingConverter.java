package voxware.browser;

import java.util.*;
import org.mozilla.javascript.*;

 // A singleton Converter for caching directives
public class CachingConverter implements Converter {

  public static final int    NULL_INDEX = 0;
  public static final String NULL_STRING = "";
  public static final int    FAST_INDEX = NULL_INDEX + 1;
  public static final String FAST_STRING = "fast";
  public static final int    SAFE_INDEX = FAST_INDEX + 1;
  public static final String SAFE_STRING = "safe";

  public static final int NUM_CACHSPECS = SAFE_INDEX + 1;

  private static CachingConverter converter = null;

  public static CachingConverter instance() {
    if (converter == null) converter = new CachingConverter();
    return converter;
  }

  public Object convert(Object value) throws VXMLEvent {
    int index = FAST_INDEX;
    if (value instanceof String) {
      if (((String)value).equalsIgnoreCase(FAST_STRING))
        index = FAST_INDEX;
      else if (((String)value).equalsIgnoreCase(SAFE_STRING))
        index = SAFE_INDEX;
    } else {
      index = ScriptRuntime.toInt32(value);
    }
    if (NULL_INDEX < index && index < NUM_CACHSPECS)
      return new Integer(index);
    else
      throw new VXMLEvent("error.attribute.badvalue");
  }

  public static String convert(int index) {
    switch(index) {
      case FAST_INDEX: return FAST_STRING;
      case SAFE_INDEX: return SAFE_STRING;
      default:         return NULL_STRING;
    }
  }
}
