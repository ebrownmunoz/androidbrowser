// DataInputStream able to read data with little-endian byte order

package voxware.voxfile;

import java.io.*;

public class VoxInputStream extends DataInputStream {

   // Constructor
  public VoxInputStream(InputStream is) {
    super(is);
  }

   // Instance Methods

   // Signed 16 bits wide swapped and put into Java short
  public short readSInt() throws IOException {
    short i = readShort(); // throws IOException, EOFException
    return (short)(0xff & (i>>8) | 0xff00 & (i<<8));
  }
   // Unsigned 16 bits wide swapped and put into Java int
  public int readUns() throws IOException {
    int i = readUnsignedShort(); // throws IOException, EOFException
    return (0xff & (i>>8) | 0xff00 & (i<<8));
  }
   // Signed 32 bits wide swapped and put into Java int
  public int readSLong() throws IOException {
    int i = readInt(); // throws IOException, EOFException
    return (0xff & (i>>24) | 0xff00 & (i>>8) | 0xff0000 & (i<<8) | 0xff000000 & (i<<24));
  }
   // Unsigned 32 bits wide swapped and put into Java long
  public long readULong() throws IOException {
    int i = readInt(); // throws IOException, EOFException
    return 0xffffffffL & (long)(0xff & (i>>24) | 0xff00 & (i>>8) | 0xff0000 & (i<<8) | 0xff000000 & (i<<24));
  }
}
