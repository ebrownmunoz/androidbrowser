// VocabularyTranslation -> VoxBlock

package voxware.voxfile;

import java.io.*;

public class VocabularyTranslation extends VoxBlock {

   // Instance Fields and their access method

  private int    usNumberWords;
  public  int    getNumberWords() {return usNumberWords;}
  public  void   setNumberWords(int i) {usNumberWords = i;}

  private int    usNumberDiscreteWords;
  public  int    getNumberDiscreteWords() {return usNumberDiscreteWords;}
  public  void   setNumberDiscreteWords(int i) {usNumberDiscreteWords = i;}

  private LenOff loVocabularyName;
  public  LenOff getVocabularyName() {return loVocabularyName;}
  public  void   setVocabularyName(LenOff lo) {loVocabularyName = lo;}

  private LenOff loHRFInitiator;
  public  LenOff getHRFInitiator() {return loHRFInitiator;}
  public  void   setHRFInitiator(LenOff lo) {loHRFInitiator = lo;}

  private LenOff loHRFSeparator;
  public  LenOff getHRFSeparator() {return loHRFSeparator;}
  public  void   setHRFSeparator(LenOff lo) {loHRFSeparator = lo;}

  private LenOff loHRFTerminator;
  public  LenOff getHRFTerminator() {return loHRFTerminator;}
  public  void   setHRFTerminator(LenOff lo) {loHRFTerminator = lo;}

  private LenOff loVRFInitiator;
  public  LenOff getVRFInitiator() {return loVRFInitiator;}
  public  void   setVRFInitiator(LenOff lo) {loVRFInitiator = lo;}

  private LenOff loVRFSeparator;
  public  LenOff getVRFSeparator() {return loVRFSeparator;}
  public  void   setVRFSeparator(LenOff lo) {loVRFSeparator = lo;}

  private LenOff loVRFTerminator;
  public  LenOff getVRFTerminator() {return loVRFTerminator;}
  public  void   setVRFTerminator(LenOff lo) {loVRFTerminator = lo;}

  private LenOff loDRFInitiator;
  public  LenOff getDRFInitiator() {return loDRFInitiator;}
  public  void   setDRFInitiator(LenOff lo) {loDRFInitiator = lo;}

  private LenOff loDRFSeparator;
  public  LenOff getDRFSeparator() {return loDRFSeparator;}
  public  void   setDRFSeparator(LenOff lo) {loDRFSeparator = lo;}

  private LenOff loDRFTerminator;
  public  LenOff getDRFTerminator() {return loDRFTerminator;}
  public  void   setDRFTerminator(LenOff lo) {loDRFTerminator = lo;}

  private int    usYesChoiceID;
  public  int    getYesChoiceID() {return usYesChoiceID;}
  public  void   setYesChoiceID(int i) {usYesChoiceID = i;}

  private int    usNoChoiceID;
  public  int    getNoChoiceID() {return usNoChoiceID;}
  public  void   setNoChoiceID(int i) {usNoChoiceID = i;}

  private int    usExitChoiceID;
  public  int    getExitChoiceID() {return usExitChoiceID;}
  public  void   setExitChoiceID(int i) {usExitChoiceID = i;}

  private VocabularyEntry[] veaWords;
  public  VocabularyEntry[] getWords() {return veaWords;}

   // Constructors
   // Package scope for read()
  VocabularyTranslation(VoxFile voxFile) {
	  super(voxFile);
	  blockID = VOCABULARY_TRANS;}

  // Public
  public VocabularyTranslation(VoxFile voxFile, int vocabularyID, int wordCount) {
	  super(voxFile);
	  blockID = VOCABULARY_TRANS;
    classID = vocabularyID;
    if (wordCount <= 0 || wordCount > 2 * Short.MAX_VALUE) 
      throw new IllegalArgumentException("VocabularyTranslation: Bad Word Count");
    veaWords = new VocabularyEntry[wordCount];
    usNumberWords = wordCount;
  }

  public VocabularyTranslation(VoxFile voxFile, int vocabularyID, VocabularyEntry[] vea) {
	  super(voxFile);
	  blockID = VOCABULARY_TRANS;
    classID = vocabularyID;
    veaWords = vea;
    usNumberWords = veaWords.length;
    if (usNumberWords == 0 || usNumberWords > 2 * Short.MAX_VALUE) 
      throw new IllegalArgumentException("VocabularyTranslation: Bad Word Count");
  }

   // Instance methods

   // Package scope
  long bodySize() {return 2*2 + 10*LenOff.bodySize() + 3*2 + veaWords.length * VocabularyEntry.bodySize();}

   // Package scope. blockID must have been read !!!
  VocabularyTranslation read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      readBlockHeader(vis);
      // Read object body
      usNumberWords = vis.readUns();
      if (usNumberWords == 0) 
        throw new VoxFormatException("VocabularyTranslation.read: NumberWords == 0");
      usNumberDiscreteWords = vis.readUns();
      if (usNumberDiscreteWords > usNumberWords) 
        throw new VoxFormatException("VocabularyTranslation.read: DiscreteWords > NumberWords");
      loVocabularyName = LenOff.read(vis);
      loHRFInitiator = LenOff.read(vis);
      loHRFSeparator = LenOff.read(vis);
      loHRFTerminator = LenOff.read(vis);
      loVRFInitiator = LenOff.read(vis);
      loVRFSeparator = LenOff.read(vis);
      loVRFTerminator = LenOff.read(vis);
      loDRFInitiator = LenOff.read(vis);
      loDRFSeparator = LenOff.read(vis);
      loDRFTerminator = LenOff.read(vis);
      usYesChoiceID = vis.readUns();
      if (usYesChoiceID > usNumberWords)
        throw new VoxFormatException("VocabularyTranslation.read: YesChoiceID > NumberWords");
      usNoChoiceID = vis.readUns();
      if (usNoChoiceID > usNumberWords)
        throw new VoxFormatException("VocabularyTranslation.read: NoChoiceID > NumberWords");
      usExitChoiceID = vis.readUns();
      if (usExitChoiceID > usNumberWords) 
        throw new VoxFormatException("VocabularyTranslation.read: ExitChoiceID > NumberWords");
      veaWords = new VocabularyEntry[usNumberWords];
      for (int i = 0; i < usNumberWords; i++)
        veaWords[i] = VocabularyEntry.read(getVoxFile(), vis);
    } catch (EOFException e) {
      throw new VoxFormatException("VocabularyTranslation.read: Unexpected EOF");
    }
    return this;
  }

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    if (usNumberDiscreteWords > usNumberWords) 
      throw new VoxFormatException("VocabularyTranslation.write: DiscreteWords > NumberWords");
    if (usYesChoiceID > usNumberWords) 
      throw new VoxFormatException("VocabularyTranslation.write: YesChoiceID > NumberWords");
    if (usNoChoiceID > usNumberWords) 
      throw new VoxFormatException("VocabularyTranslation.write: NoChoiceID > NumberWords");
    if (usExitChoiceID > usNumberWords) 
      throw new VoxFormatException("VocabularyTranslation.write: ExitChoiceID > NumberWords");
    writeBlockHeader(vos);
    vos.writeUns(usNumberWords);
    vos.writeUns(usNumberDiscreteWords);
    loVocabularyName.write(vos);
    loHRFInitiator.write(vos);
    loHRFSeparator.write(vos);
    loHRFTerminator.write(vos);
    loVRFInitiator.write(vos);
    loVRFSeparator.write(vos);
    loVRFTerminator.write(vos);
    loDRFInitiator.write(vos);
    loDRFSeparator.write(vos);
    loDRFTerminator.write(vos);
    vos.writeUns(usYesChoiceID);
    vos.writeUns(usNoChoiceID);
    vos.writeUns(usExitChoiceID);
    for (int i = 0; i < usNumberWords; i++)
      if (veaWords[i] == null)
        throw new VoxFormatException("VocabularyTranslation.write: null Vocabulary Entry #" + i);
      else
        veaWords[i].write(vos);
  }

  public String toString() {
    StringBuffer buffer = new StringBuffer(super.toString() + "VocabularyTranslation:\n");
    buffer.append("Vocabulary: \"" + getText(loVocabularyName) + "\", #Words: " + usNumberWords + ", #Discrete Words: " + usNumberDiscreteWords +
                  ", YesChoiceID: " + usYesChoiceID + ", NoChoiceID: " + usNoChoiceID + ", ExitChoiceID: " + usExitChoiceID + "\n");
    for (int i = 0; i < usNumberWords; i++) {
      if (veaWords[i] == null) throw new NullPointerException("VocabularyTranslation.toString: null Vocabulary Entry #" + i);
      buffer = StringFormatter.appendTo(buffer, "Word ", -4, String.valueOf(i + 1));
      buffer.append(veaWords[i]);
    }
    return buffer.toString() + "\n";
  }
}
