// Concrete VoxAdd subclass facilitating English language support

package voxware.voxfile;

import java.util.*;
import java.io.*;

public class VoxAddEN extends VoxAdd {

  /** Exception Dictionary File to be found in Knowledge Base directory */
  public static String ExceptionDictionaryFileName = "custom.dic";
  /** General Dictionary File to be found in Knowledge Base directory */
  public static String GeneralDictionaryFileName   = "pronounc.dic";
  /** General Dictionary default orthography delimiter */
  public static char   GENERAL_DICTIONARY_ORTHO_DELIMITER = ' ';
  /** General Dictionary default encoding is standard Java Properties file encoding. Another valid encoding is "UTF-8" */
  public static String GENERAL_DICTIONARY_ENCODING = null;

  /** Neural Net File File to be found in Knowledge Base directory */
  public static String NeuralNetFileName           = "pronounc.net";

  static Phone[] english = {
    new Phone("SIL", "*", "*", SPECS, 1),
    new Phone("AA", ">aa aa aa<", "aa", VOWEL, 3),
    new Phone("AE", ">ae ae ae<", "ae", VOWEL, 3),
    new Phone("AH", ">ah ah ah<", "ah", VOWEL, 3),
    new Phone("AO", ">ao ao ao<", "ao", VOWEL, 3),
    new Phone("AW", ">aw Aw aW aw<", "aw", DIPHT, 4),
    new Phone("AX", ">ax ax<", "ax", SCHWA, 2),
    new Phone("AY", ">ay Ay aY ay<", "ay", DIPHT, 4),
    new Phone("B", "^b<b b<", "b", STOPS, 2),
    new Phone("BD", "^b<", "b", FLAPS, 1),
    new Phone("CH", "^t<ch ch ch<", "ch", AFRIC, 3),
    new Phone("D", "^d<d d<", "d", STOPS, 2),
    new Phone("DD", "^d<", "d", FLAPS, 1),
    new Phone("DH", "dh dh<", "dh", VFRIC, 2),
    new Phone("DX", "dx<", "d", FLAPS, 1),
    new Phone("EH", ">eh eh eh<", "eh", VOWEL, 3),
    new Phone("ER", ">er er er<", "er", VOWEL, 3),
    new Phone("EY", ">ey Ey eY ey<", "ey", DIPHT, 4),
    new Phone("F", "f f<", "f", UFRIC, 2),
    new Phone("G", "^g<g g<", "g", STOPS, 2),
    new Phone("GD", "^g<", "g", FLAPS, 1),
    new Phone("HH", "hh hh<", "hh", GLIDE, 2),
    new Phone("IH", ">ih ih ih<", "ih", VOWEL, 3),
    new Phone("IX", ">ix ix<", "ix", SCHWA, 2),
    new Phone("IY", ">iy iy iy<", "iy", VOWEL, 3),
    new Phone("JH", "^d<jh jh jh<", "jh", AFRIC, 3),
    new Phone("K", "^k<k k<", "k", STOPS, 2),
    new Phone("KD", "^k<", "k", FLAPS, 1),
    new Phone("L", ">l l<", "l", GLIDE, 2),
    new Phone("M", "m m<", "m", NASAL, 2),
    new Phone("N", "n n<", "n", NASAL, 2),
    new Phone("NG", "ng ng<", "ng", NASAL, 2),
    new Phone("OW", ">ow Ow oW ow<", "ow", DIPHT, 4),
    new Phone("OY", ">oy Oy oY oy<", "oy", DIPHT, 4),
    new Phone("P", "^p<p p<", "p", STOPS, 2),
    new Phone("PD", "^p<", "p", FLAPS, 1),
    new Phone("R", ">r r<", "r", GLIDE, 2),
    new Phone("S", "s s<", "s", UFRIC, 2),
    new Phone("SH", "sh sh<", "sh", UFRIC, 2),
    new Phone("T", "^t<t t<", "t", STOPS, 2),
    new Phone("TD", "^t<", "t", FLAPS, 1),
    new Phone("TH", "th th<", "th", UFRIC, 2),
    new Phone("UH", ">uh uh uh<", "uh", VOWEL, 3),
    new Phone("UW", ">uw uw uw<", "uw", VOWEL, 3),
    new Phone("V", "v v<", "v", VFRIC, 2),
    new Phone("W", ">w w<", "w", SEMIV, 2),
    new Phone("Y", ">y y<", "y", SEMIV, 2),
    new Phone("Z", "z z<", "z", VFRIC, 2),
    new Phone("ZH", "zh zh<", "zh", VFRIC, 2)
  };

  private ExceptionDictionary exceptionDictionary;
  private NeuralNet nnet;

  // Constructors
  public VoxAddEN(String kbPath) {
    super(english);
    language = "EN";
    // Do dictionaries and NeuralNet
    exceptionDictionary = new ExceptionDictionary(kbPath + "/" + ExceptionDictionaryFileName);
    try {
      if (GENERAL_DICTIONARY_ENCODING == null) {
        // Don't construct VoxProperties directly here to avoid double loading of huge number of pairs
        generalDictionary = new Properties();
        generalDictionary.load(new FileInputStream(kbPath + "/" + GeneralDictionaryFileName));
      } else {
        // Make it with specified encoding
        generalDictionary = new VoxProperties(new FileInputStream(kbPath + "/" + GeneralDictionaryFileName), GENERAL_DICTIONARY_ENCODING);
      }
      nnet = new NeuralNet(new FileInputStream(kbPath + "/" + NeuralNetFileName));
    } catch (IOException ioe) {
      throw new IllegalArgumentException("VoxAddEN: " + ioe.getMessage());
    }
  }

  public VoxAddEN(String kbPath, String gender, int feVersion) {
    this(kbPath);
    if (!FrontEndVersion.isValidFEVersion(feVersion)) throw new IllegalArgumentException("VoxAddEN: Bad Front End Version " + feVersion);
    String componentFileName = "unit" + feVersion + ".voi";
    if (gender.charAt(0) == 'm' || gender.charAt(0) == 'M')      componentFileName = "m" + componentFileName;
    else if (gender.charAt(0) == 'f' || gender.charAt(0) == 'F') componentFileName = "f" + componentFileName;
    else throw new IllegalArgumentException("VoxAddEN: Gender String \"" + gender + "\" does not start with 'f' or 'm'");
    try {
      load(new FileInputStream(kbPath + "/" + componentFileName));
    } catch (IOException ioe) {
      throw new IllegalArgumentException("VoxAddEN: " + ioe.getMessage());
    } catch (VoxFormatException vfe) {
      throw new IllegalArgumentException("VoxAddEN: " + vfe.getMessage());
    }
  }

  /**
   * Overrides superclass version of orthography transformation into component sequence.
   * Handles pronunciation exception, numbers, abbreviations e.t.c here.
   * Returns componentString. If supplied String[3] report != null report[0] will contain expanded orthography,
   * report[1] - phoneString, report[2] - componentString. Appearance of "+" in any of strings means whole or
   * a part of a string came frome Exception Dictionary. Similarly "*" means General Dictionary involvement.
   */
  public String componentsFromOrthography(String ortho, String[] report) throws VoxFormatException {
    String       orthoString, phoneString, componentString; // to fill report
    int          index;
    StringBuffer tmpReport = report == null ? null: new StringBuffer("");
    boolean      inDictionary;

    // Clean up report if any
    if (report != null) Arrays.fill(report, null);

    // Run ortho through ExceptionDictionary
    if ((index = exceptionDictionary.wordIndex(ortho)) >= 0) {
      // It is there. The most detailed translation has preference
      componentString = exceptionDictionary.getComponents(index);
      phoneString     = exceptionDictionary.getPhones(index);
      orthoString     = exceptionDictionary.getOrthography(index);
      if (componentString != null || phoneString != null) {
        if (componentString == null) {
          componentString = componentsFromPhones(phoneString);
          if (report != null) report[2] = componentString;
        } else {
          if (report != null) report[2] = "+++ " + componentString;
        }
        if (report != null) {
          if (orthoString != null) report[0] = "+++ " + orthoString;
          if (phoneString != null) report[1] = "+++ " + phoneString;
        }
        return componentString;
      } else {
        // Only orthoString supplied. It replaces original ortho as a target for further processing
        ortho = orthoString;
        if (tmpReport != null) tmpReport.append("+++ ");
      }
    }
    // Orthography may contain special chars, abbreviations, alphanumerics ... .
    // Translate it and run each token through ExceptionDictionary.
    // Only 1 level of recursion here, so let each token deliver either "simple"
    // orthoString or more detailed transcription along with it.
    ortho = translate(ortho);
    StringBuffer buf = new StringBuffer(200);
    StringTokenizer words = new StringTokenizer(ortho);
    while (words.hasMoreTokens()) {
      String word = words.nextToken();
      if ((index = exceptionDictionary.wordIndex(word)) >= 0 && (orthoString = exceptionDictionary.getOrthography(index)) != null) {
        buf.append(orthoString + " ");
        if (tmpReport != null) tmpReport.append("[+" + orthoString);
      } else {
        buf.append(word + " ");
        if (tmpReport != null) tmpReport.append("[" + word);
      }
      if (tmpReport != null) tmpReport.append("] ");
    }
    ortho = buf.toString();
    if (tmpReport != null) report[0] = tmpReport.toString();
    // Now build the phone string. Run each ortho token through ExceptionDictionary
    buf.setLength(0);
    if (tmpReport != null) tmpReport.setLength(0);
    StringBuffer tmp = new StringBuffer(200);
    words = new StringTokenizer(ortho);
    while (words.hasMoreTokens()) {
      String word = words.nextToken();
      if (((index = exceptionDictionary.wordIndex(word)) >= 0 || (index = exceptionDictionary.orthoIndex(word)) >= 0) &&
          (phoneString = exceptionDictionary.getPhones(index)) != null) {
        buf.append(phoneString + " ");
        if (tmpReport != null) tmpReport.append("[+" + phoneString);
      } else { // Go get translation from engine
        inDictionary = phonesFromOrthography(word, tmp);
        buf.append(tmp);
        buf.append(" ");
        if (tmpReport != null) {
          tmpReport.append(inDictionary ? "[*" : "[");
          tmpReport.append(tmp);
        }
        tmp.setLength(0);
      }
      if (tmpReport != null) tmpReport.append("] ");
    }
    phoneString = buf.toString();
    if (tmpReport != null) report[1] = tmpReport.toString();
    // Now build the component string.
    componentString = componentsFromPhones(phoneString);
    if (report != null) report[2] = componentString;
    return componentString;
  }

  /**
   * Returns input converted to upper case
   */
  String preprocess(String ortho) {
    return ortho.toUpperCase();
  }

  /**
   * Returns input if inDictionary == true, calls for Neural Net service otherwise.
   */
  String applyRules(String input, boolean inDictionary) throws VoxFormatException {
    if (inDictionary) return input;
    // Eliminate some symbols from input string
    String set = "_'-,.()/:;?!`";
    StringBuffer sb = new StringBuffer(64);
    for (int i = 0; i < input.length(); i++) {
      char c = input.charAt(i);
      if (set.indexOf(c) < 0)
        sb.append(c);
    }
    String s = sb.toString();
    return nnet.textToPhones(s);
  }

  /**
   * Replaces with a space anything that is not a alpha, digit, period,'&' sign, '%', or apostrophe.
   * Adds a space after a '.', inserts a space before '.' if it is preceded with digit.
   * Adds a space before and after a '&', then handles alpha-numeric mixture and numbers.
   */
  String translate(String in) throws VoxFormatException {
    StringBuffer buf = new StringBuffer(200);
    int          i, j, l = in.length();
    char         c;
    for (i = 0; i < l; i++) {
      c = in.charAt(i);
      if (!Character.isLetterOrDigit(c) && c != '.' && c != '&' && c != '%' && c != '\'') {
        buf.append(" ");
      } else if (c == '.' || c == '%') {
        // If after digit insert space
        if (i > 0 && Character.isDigit(in.charAt(i - 1)))
          buf.append(" ");
        buf.append(c);
        buf.append(" ");
      } else if (c == '&') {
        buf.append(" & ");
      } else {
        buf.append(c);
      }
    }
    in = buf.toString().trim();
    buf.setLength(0);
    // Break tokens like "SIM3000" to "SIM 3000". Convert digit only tokens into numbers
    StringTokenizer tokens = new StringTokenizer(in);
    while (tokens.hasMoreTokens()) {
      String token = tokens.nextToken();
      l = token.length();
      boolean state = Character.isDigit(token.charAt(0));
      for (i = 0, j = 0; true; i++) {
        boolean test = Character.isDigit(token.charAt(i));
        // Do we have a state change
        if (test != state) {
          buf.append((state ? toNumerals(token.substring(j, i)) : token.substring(j, i)) + " ");
          state = test;
          j = i;
        }
        // At the end of a string, including case of single char token
        if (i == l - 1) {
          buf.append((state ? toNumerals(token.substring(j)) : token.substring(j)) + " ");
          break;
        }
      }
    }
    return buf.toString().trim();
  }

  /**
   * Converts only simple digit strings.
   */
  String toNumerals(String in) throws VoxFormatException {
    int val = Integer.parseInt(in);
    switch(val) {
      case    0: return "zero";
      case    1: return "one";
      case    2: return "two";
      case    3: return "three";
      case    4: return "four";
      case    5: return "five";
      case    6: return "six";
      case    7: return "seven";
      case    8: return "eight";
      case    9: return "nine";
      case   10: return "ten";
      case   11: return "eleven";
      case   12: return "twelve";
      case   13: return "thirteen";
      case   14: return "fourteen";
      case   15: return "fifteen";
      case   16: return "sixteen";
      case   17: return "seventeen";
      case   18: return "eighteen";
      case   19: return "nineteen";
      case   20: return "twenty";
      case   30: return "thirty";
      case   40: return "forty";
      case   50: return "fifty";
      case   60: return "sixty";
      case   70: return "seventy";
      case   80: return "eighty";
      case   90: return "ninety";
      case  100: return "one hundred";
      case 1000: return "one thousand";
      default: throw new VoxFormatException("VoxAddEN.toNumerals: Unsuported number " + val);
    }
  }
}
