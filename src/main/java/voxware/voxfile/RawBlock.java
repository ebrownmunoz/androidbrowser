package voxware.voxfile;

import java.io.IOException;
import java.io.EOFException;
import java.util.zip.CRC32;

public class RawBlock extends VoxBlock {
  byte[] body;

  public RawBlock(VoxFile voxFile) {
	  super(voxFile);
  }

  long bodySize() {
    if (body == null)
      return 0;
    else
      return (long) body.length;
  }

  public long getBodySize() {
    return bodySize();
  }

  public void read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      //!! Read the blockID
      blockID = vis.readUns();
      readBlockHeader(vis);
      
      // Allocate body and read
      body = new byte[(int)blockSize];
      /*
      System.out.println("DEBUG: blockSize " + blockSize);
      System.exit(0); */
      
      int offset = 0;
      int nread = 0;

      while (offset < blockSize && (nread = vis.read(body, offset, (int)(blockSize - offset))) >= 0) {
        offset += nread;
      }
    } catch (EOFException e) {
      throw new VoxFormatException("RawBlock.read: Unexpected EOF");
    }
  }

  public long checksum() {
    if (body == null || body.length == 0)
      return 0;

    CRC32 crc32 = new CRC32();
    crc32.update(body, 0, body.length);
    return crc32.getValue();
  }
}