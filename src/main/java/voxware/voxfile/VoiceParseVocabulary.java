// VoiceParseVocabulary -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class VoiceParseVocabulary extends ParseTable {

   // Constructor for VoxBlock.read()
  VoiceParseVocabulary(VoxFile voxFile) {
	  super(voxFile);
	  blockID = VOICE_PARSE_VOCAB;}

   // Constructors for Java Convert writers. Should check arguments consistency.
  public VoiceParseVocabulary(VoxFile voxFile, int vocabularyID) {
    super(voxFile, vocabularyID, null, null);
    blockID = VOICE_PARSE_VOCAB;
  }

  public VoiceParseVocabulary(VoxFile voxFile, int vocabularyID, long[] offsets, int[] data) {
    super(voxFile, vocabularyID, offsets, data);
    blockID = VOICE_PARSE_VOCAB;
  }

  public String toString() {
    return super.toString() + "VoiceParseVocabulary: State Count = " + ulaOffsets.length + "\n\n";
  }
}
