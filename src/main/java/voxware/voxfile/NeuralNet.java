// NeuralNet converts orthography into phonetic transcription

package voxware.voxfile;

import java.io.*;
import java.util.*;

public class NeuralNet {
   // Constants
  static final int NUM_INPUT           = 189;
  static final int NUM_HIDDEN          = 70;
  static final int NUM_BIAS            = 1;
  static final int NUM_OUTPUT          = 58;
  static final int NUMALPHAS           = 27;
  static final int NUMPHONES           = 64;
  static final int WINDOWSIZE          = 7;
  static final String INPUT_UNIT_NAME  = "Input";
  static final String OUTPUT_UNIT_NAME = "Output";
  static final String HIDDEN_UNIT_NAME = "Hidden";
  static final String BIAS_UNIT_NAME   = "Bias";

  static String alphabets = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  // Actual no. of phones used == numOutput
  static String[] phones = {
    "AA", "AE", "AH", "AO", "AW", "AX", "AY", "B", "CH", "D",
    "DD", "DH", "DX", "EH", "ER", "EY", "F",  "G", "HH", "IH",
    "IX", "IY", "JH", "K",  "KD", "L",  "M",  "N", "NG", "OW",
    "OY", "P",  "PD", "R",  "S",  "SH", "SIL","T", "TD", "TH",
    "UH", "UW", "V",  "W",  "Y",  "Z",  "ZH",
    "AEK", "AEKD", "AXK", "AXKD", "IHKD", "KS", "KZ", "YUW", "YUH",
    "AXS", "AXZ",
    null
  };

  // Nested classes
  static class Unit {
    // Instance Fields
    String name;         // Name of this unit
    float  totalInput;   // The summed input to the unit
    float  output;       // The unit's output
    float  target;       // The target output of the unit
    float  outputDeriv;  // The sum of the backprop errors
    float  inputDeriv;   // The derivative value
    Link[] incomingLink; // counter: numIncoming
  }

  static class Link {
    // Instance Fields
    String name;
    float  weight;
    Unit   preUnit;
    Unit   postUnit;
  }

  // NeuralNet Instance Fields
  Unit[]    input        = new Unit[NUM_INPUT];
  Unit[]    output       = new Unit[NUM_OUTPUT];
  Unit[]    hidden       = new Unit[NUM_HIDDEN];
  Unit[]    bias         = new Unit[NUM_BIAS];
  Sigmoid   sigmoid      = new Sigmoid(0.0, 0.0, 1.0, 1.0);
  float[][] alphaVectors = new float[NUMALPHAS][NUMALPHAS];

  // Constructor
  NeuralNet(InputStream is) {
    int      i, j;
    String   line;
    String[] sa;
    Link     link;
    Unit     u;

    // Input units initialization
    for (i = 0; i < input.length; i++) {
      input[i] = new Unit();
      input[i].name = INPUT_UNIT_NAME + "." + i;
    }
    // Bias unit is special
    bias[0] = new Unit();
    bias[0].name = "Bias";
    bias[0].output = 1.0F;
    // Hidden units initialization
    for (i = 0; i < hidden.length; i++) {
      hidden[i] = new Unit();
      hidden[i].name = HIDDEN_UNIT_NAME + "." + i;
      hidden[i].incomingLink = new Link[NUM_INPUT + 1]; // Plus 1 bias unit
    }
    // Output units initialization
    for (i = 0; i < output.length; i++) {
      output[i] = new Unit();
      output[i].name = OUTPUT_UNIT_NAME + "." + i;
      output[i].incomingLink = new Link[NUM_HIDDEN + 1]; // Plus 1 bias unit
    }
    // Digest the file, providing links
    BufferedReader br = new BufferedReader(new InputStreamReader(is));
    try {
      line = br.readLine(); // Line with Version and total number of links
      // (1) Bias -> Hidden, followed by Bias -> Output
      readLinks(br, bias, hidden);
      readLinks(br, bias, output);
      // (2) Input -> Hidden, grouped by Hidden
      readLinks(br, input, hidden);
      // (3) Hidden -> Output, grouped by Output
      readLinks(br, hidden, output);
    } catch (IOException ioe) {
      throw new IllegalArgumentException("NeuralNet: " + ioe.getMessage());
    }
    // Initialze alphaVectors, used for formating
    for (i = 0; i < NUMALPHAS; i++)
      alphaVectors[i][i] = 1.0F;
  }

  private void readLinks(BufferedReader br, Unit[] left,  Unit[] right) throws IOException {
    int      i, j;
    String   line;
    String[] sa;
    Link     link;
    Unit     pre, post;
    boolean  leftIsBias = left[0].name.equals("Bias");

    // Left -> Right, grouped by Right
    for (i = 0; i < right.length; i++) {
      post = right[i];
      for (j = 0; j < left.length; j++) {
        pre = left[j];
        if ((line = br.readLine()) == null) throw new IOException("Unexpected EOF");
        StringTokenizer tokens = new StringTokenizer(line);
        if (!(tokens.countTokens() > 3)) throw new IOException("line \"" + line + "\" has too few tokens");
        String token = tokens.nextToken();
        if (!token.equals(pre.name)) throw new IOException("preUnit must have name\"" + pre.name + "\" rather than \"" + token + "\"");
        tokens.nextToken();
        token = tokens.nextToken();
        if (!token.startsWith(post.name)) throw new IOException("postUnit name must start with \"" + post.name + "\" in \"" + token + "\"");
        link = new Link();
        link.weight = Float.parseFloat(tokens.nextToken());
        link.preUnit = pre;
        link.postUnit = post;
        // Incoming links for this post unit
        if (leftIsBias) post.incomingLink[0] = link;
        else            post.incomingLink[j + 1] = link;
      }
    }
  }

  public String textToPhones(String text) throws VoxFormatException {
    int i, j, len = text.length();
    // Format
    float[][] inputVector = formatInput(text);
    // Convert
    float[][] outputVector = new float[len][output.length];
    for (i = 0; i < len; i++) {
      float[] actIn = inputVector[i];
      float[] actOut = outputVector[i];
      for (j = 0; j < input.length; j++)
        input[j].output = actIn[j];
      for (j = 0; j < hidden.length; j++) {
        dotProductCombIn(hidden[j]);
        hidden[j].output = sigmoid.getSigmoid(hidden[j].totalInput);
      }
      for (j = 0; j < output.length; j++) {
        dotProductCombIn(output[j]);
        output[j].output = sigmoid.getSigmoid(output[j].totalInput);
        actOut[j] = output[j].output;
      }
    }
    // Return translation
    return translateOutput(outputVector);
  }

  // Calculates the dot product of all incoming links for a unit and stores it in the totalInput field of the unit.
  private void dotProductCombIn(Unit unit) {
    float total = 0.0F;
    for (int i = 0; i < unit.incomingLink.length; i++) {
      Link link = unit.incomingLink[i];
      total += link.weight * link.preUnit.output;
    }
    unit.totalInput = total;
  }

  // Input and Output formatting
  private float[][] formatInput(String text) throws VoxFormatException {
    int   i, j, k, n, len = text.length();
    int[] index = new int[len + 6];
    // Pad word with 3 initial #'s
    index[0] = index[1] = index[2] = 0;
    // Put letter indexes in
    for (i = 0, j = 3; i < len; i++) {
      char c = text.charAt(i);
      if ((k = alphabets.indexOf(c)) < 1) throw new VoxFormatException("Illegal Character '" + String.valueOf(c) + "' !");
      index[j++] = k;
    }
    // Pad word with 3 final #'s
    index[j++] = 0;
    index[j++] = 0;
    index[j] = 0;
    float[][] iv = new float[len][input.length];
    // Each letter in the word
    for (i = 0; i < len; i++) {
      float[] act = iv[i];
      // Each window -- group of input values. WINDOWSIZE * NUMALPHAS == NUM_INPUT == act.length == input.length
      for (j = 0, n = 0; j < WINDOWSIZE; j++)
        for (k = 0; k < NUMALPHAS; k++)
          act[n++] = alphaVectors[index[i + j]][k];
    }
    return iv;
  }

  private String translateOutput(float[][] outVec) {
    int i, phid, len = outVec.length;
    int[] hyp = new int[len];

    // Each phone in the word
    for (i = 0; i < len; i++) {
      float[] act = outVec[i];
      float prevVal = 0.0F;
      for (phid = 0; phid < act.length; phid++)
        if (act[phid] > prevVal) {
          hyp[i] = phid;
          prevVal = act[phid];
        }
    }

    StringBuffer sb = new StringBuffer(100);
    for (i = 0; i < len; i++) {
      String phone = phones[hyp[i]];
      if (phone.equals("SIL"))   continue;
      else if (phone.equals("AEK"))  sb.append("AE K ");
      else if (phone.equals("AEKD")) sb.append("AE KD ");
      else if (phone.equals("AXK"))  sb.append("AX K ");
      else if (phone.equals("AXKD")) sb.append("AX KD ");
      else if (phone.equals("AXS"))  sb.append("AX S ");
      else if (phone.equals("AXZ"))  sb.append("AX Z ");
      else if (phone.equals("IHKD")) sb.append("IH KD ");
      else if (phone.equals("KS"))   sb.append("K S ");
      else if (phone.equals("YAX"))  sb.append("Y AX ");
      else if (phone.equals("YUH"))  sb.append("Y UH ");
      else if (phone.equals("YUW"))  sb.append("Y UW ");
      else                           sb.append(phone + " ");
    }
    String s = sb.toString();
    return s;
  }
}
