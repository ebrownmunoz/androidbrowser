// VoxDictionary -> VoxBlock

package voxware.voxfile;

import java.io.*;

public class VoxDictionary extends VoxBlock {

	// Instance Fields and their access method

	private DictionaryEntry[] deaWords;
	public  DictionaryEntry[] getWords() {return deaWords;}

	public  int getWordCount() {return deaWords.length;}

	// Constructors
	// Package scope for read()
	VoxDictionary(VoxFile voxFile) {
		super(voxFile);
		blockID = DICT;}

	// Public
	public VoxDictionary(VoxFile voxFile, DictionaryEntry[] dea) {
		super(voxFile);
		blockID = DICT;
		classID = 1;  // Per convert out_dict()
		deaWords = dea;
		if (deaWords.length == 0 || deaWords.length > 2 * Short.MAX_VALUE) throw new IllegalArgumentException("VoxDictionary: Bad Word Count");
	}

	// Instance methods

	// Package scope
	long bodySize() {return 2 + deaWords.length * DictionaryEntry.bodySize();}

	// Package scope. blockID must have been read !!!
	VoxDictionary read(VoxInputStream vis) throws IOException, VoxFormatException {
		try {
			readBlockHeader(vis);
			// Read object body
			int n = vis.readUns();
			if (n == 0 || n > 2 * Short.MAX_VALUE) throw new VoxFormatException("VoxDictionary.read: Bad Word Count");
			deaWords = new DictionaryEntry[n];
			for (int i = 0; i < n; i++)
				deaWords[i] = DictionaryEntry.read(voxFile, vis);
		} catch (EOFException e) {
			throw new VoxFormatException("VoxDictionary.read: Unexpected EOF");
		}
		return this;
	}

	void write(VoxOutputStream vos) throws IOException, VoxFormatException {
		writeBlockHeader(vos);
		vos.writeUns(deaWords.length);
		for (int i = 0; i < deaWords.length; i++)
			deaWords[i].write(vos);
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer(super.toString() + "VoxDictionary: #Words: " + deaWords.length + "\n");
		for (int i = 0; i < deaWords.length; i++) {
			buffer = StringFormatter.appendTo(buffer, -4, String.valueOf(i + 1), ".");
			buffer.append(deaWords[i]);
		}
		return buffer.toString() + "\n";
	}
}
