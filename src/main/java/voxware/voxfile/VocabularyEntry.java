// VocabularyEntry - component of VocabularyTranslation block

package voxware.voxfile;

import java.io.*;

public class VocabularyEntry {

   // Class Methods. Package scope.
  static VocabularyEntry read(VoxFile voxFile, VoxInputStream vis) throws IOException, VoxFormatException {
    VocabularyEntry entry = new VocabularyEntry(voxFile);
    try {
      entry.usMultipleTemplateID = vis.readUns();
      if (entry.usMultipleTemplateID == 0) throw new VoxFormatException("VocabularyEntry.read: wordId == 0");
      entry.ubDiscreteWord = vis.readByte();
      entry.loWordName = LenOff.read(vis);
      entry.loHostTranslation = LenOff.read(vis);
      entry.loVoiceTranslation = LenOff.read(vis);
      entry.loDisplayTranslation = LenOff.read(vis);
    } catch (EOFException e) {
      throw new VoxFormatException("VocabularyEntry.read: Unexpected EOF");
    }
    return entry;
  }

   // Instance Fields and their access method
  
  private VoxFile voxFile;
  
  private int    usMultipleTemplateID;
  public  int    getMultipleTemplateID() {return usMultipleTemplateID;}
  public  void   setMultipleTemplateID(int i) {usMultipleTemplateID = i;}

  private int    ubDiscreteWord;
  public  int    getDiscreteWord() {return ubDiscreteWord;}
  public  void   setDiscreteWord(int i) {ubDiscreteWord = i;}

  private LenOff loWordName;
  public  LenOff getWordName() {return loWordName;}
  public  void   setWordName(LenOff lo) {loWordName = lo;}

  private LenOff loHostTranslation;
  public  LenOff getHostTranslation() {return loHostTranslation;}
  public  void   setHostTranslation(LenOff lo) {loHostTranslation = lo;}

  private LenOff loVoiceTranslation;
  public  LenOff getVoiceTranslation() {return loVoiceTranslation;}
  public  void   setVoiceTranslation(LenOff lo) {loVoiceTranslation = lo;}

  private LenOff loDisplayTranslation;
  public  LenOff getDisplayTranslation() {return loDisplayTranslation;}
  public  void   setDisplayTranslation(LenOff lo) {loDisplayTranslation = lo;}

   // Constructor
  public  VocabularyEntry(VoxFile voxFile) {
	  this.voxFile = voxFile;
  } 

   // Package scope
  static long bodySize() {return 2 + 1 + 4 * LenOff.bodySize();}

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    if (usMultipleTemplateID == 0) throw new VoxFormatException("VocabularyEntry.write: wordId == 0");
    vos.writeUns(usMultipleTemplateID);
    vos.writeByte(ubDiscreteWord);
    loWordName.write(vos);
    loHostTranslation.write(vos);
    loVoiceTranslation.write(vos);
    loDisplayTranslation.write(vos);
  }

  public String toString () {
    StringBuffer buffer = StringFormatter.appendTo("(", -4, String.valueOf(usMultipleTemplateID), ");");
    buffer.append("  word name:            " + voxFile.getText(loWordName) + "\n");
    buffer.append("                  host    translation:  " + voxFile.getText(loHostTranslation) + "\n");
    buffer.append("                  voice   translation:  " + voxFile.getText(loVoiceTranslation) + "\n");
    buffer.append("                  display translation:  " + voxFile.getText(loDisplayTranslation) + "\n");
    return buffer.toString();
  }
}
