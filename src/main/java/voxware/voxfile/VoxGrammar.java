// VoxGrammar -> VoxBlock

package voxware.voxfile;

import java.io.*;

public class VoxGrammar extends VoxBlock {

   // Instance Fields and their access method

  private int    usVocabulary;
  public  int    getVocabulary() {return usVocabulary;}
  public  void   setVocabulary(int i) {usVocabulary = i;}

  private int    usDefaultCRTemplate;
  public  int    getDefaultCRTemplate() {return usDefaultCRTemplate;}
  public  void   setDefaultCRTemplate(int i) {usDefaultCRTemplate = i;}

  private int    usDefaultHRTemplate;
  public  int    getDefaultHRTemplate() {return usDefaultHRTemplate;}
  public  void   setDefaultHRTemplate(int i) {usDefaultHRTemplate = i;}

  private LenOff loGrammarName;
  public  LenOff getGrammarName() {return loGrammarName;}
  public  void   setGrammarName(LenOff lo) {loGrammarName = lo;}

  private int    usDefaultVRTemplate;
  public  int    getDefaultVRTemplate() {return usDefaultVRTemplate;}
  public  void   setDefaultVRTemplate(int i) {usDefaultVRTemplate = i;}

  private int    usDefaultDRTemplate;
  public  int    getDefaultDRTemplate() {return usDefaultDRTemplate;}
  public  void   setDefaultDRTemplate(int i) {usDefaultDRTemplate = i;}

   // Constructors
  VoxGrammar(VoxFile voxFile) {
	super(voxFile);
    blockID = GRAMMAR;
  }

  public VoxGrammar(VoxFile voxFile, int grammarID) {
	super(voxFile);
    blockID = GRAMMAR;
    classID = grammarID;
  }

   // Instance methods

   // Package scope
  long bodySize() {return 15;} // 3*2 + 5 + 2*2

   // Package scope. blockID must have been read !!!
  VoxGrammar read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      readBlockHeader(vis);
      // Early check on the fixed length object
      if (blockSize != bodySize()) throw new VoxFormatException("VoxGrammar.read: expected size = 15, but read " + blockSize);
      // Read object body
      usVocabulary = vis.readUns();
      usDefaultCRTemplate = vis.readUns();
      usDefaultHRTemplate = vis.readUns();
      loGrammarName = LenOff.read(vis);
      usDefaultVRTemplate = vis.readUns();
      usDefaultDRTemplate = vis.readUns();
    } catch (EOFException e) {
      throw new VoxFormatException("VoxGrammar.read: Unexpected EOF");
    }
    return this;
  }

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    writeBlockHeader(vos);
    vos.writeUns(usVocabulary);
    vos.writeUns(usDefaultCRTemplate);
    vos.writeUns(usDefaultHRTemplate);
    loGrammarName.write(vos);
    vos.writeUns(usDefaultVRTemplate);
    vos.writeUns(usDefaultDRTemplate);
  }

  public String toString () {
    return super.toString() + "VoxGrammar:\n" +
           "Grammar: " + getText(loGrammarName) + "; Vocabulary Class: " + usVocabulary + "\n" +
           "Default Control Response Template: " + usDefaultCRTemplate + "; Default Host Response Template: " + usDefaultHRTemplate + "\n" +
           "Default Voice Response Template: " + usDefaultVRTemplate + "; Default Display Response Template: " + usDefaultDRTemplate + "\n\n";
  }
}
