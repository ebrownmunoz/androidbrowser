// VoxGraph -> VoxBlock

package voxware.voxfile;

import java.io.*;

public class VoxGraph extends VoxBlock {

   // Instance Fields and their access method

  private int   usGraphSize; // usGraphSize = 3 + 3*usNumberOfArcs + wordCount == 3 + usaArcs.length == (blockSize - 2) / 2
  public  int   getGraphSize() {return usGraphSize;}

  private int   usGrammarID;
  public  int   getGrammarID() {return usGrammarID;}

  private int   usNumberOfArcs;
  public  int   getNumberOfArcs() {return usNumberOfArcs;}

  private int   usNumberOfNodes;
  public  int   getNumberOfNodes() {return usNumberOfNodes;}

  private int[] usaArcs;
  public  int[] getArcs() {return usaArcs;}

   // Constructors
   // Package scope for read()
  VoxGraph(VoxFile voxFile) {
	  super(voxFile);
	  blockID = GRAPH;}

   // Public
  public VoxGraph(VoxFile voxFile, int grammarID, int numberOfArcs, int numberOfNodes, int totalWordsOnArcsCount, int[] arcs) {
    super(voxFile);
    blockID = GRAPH;
    classID = grammarID;
    usGraphSize = 3 + 3*numberOfArcs + totalWordsOnArcsCount;
    usGrammarID = grammarID;
    usNumberOfArcs = numberOfArcs;
    usNumberOfNodes = numberOfNodes;
    usaArcs = arcs;
    if (arcs.length < usGraphSize - 3) throw new IllegalArgumentException("VoxGraph: arcs array too short");
  }

   // Instance methods

   // Package scope
  long bodySize() {return 2 * (usGraphSize + 1);}

   // Package scope. blockID must have been read !!!
  VoxGraph read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      readBlockHeader(vis);
      // Read object body
      usGraphSize = vis.readUns();
      if (blockSize != 2 * (usGraphSize + 1)) throw new VoxFormatException("VoxGraph.read: Block Size and Graph Size mismatch");
      usGrammarID = vis.readUns();
      usNumberOfArcs = vis.readUns();
      usNumberOfNodes = vis.readUns();
      usaArcs = new int[usGraphSize - 3];
      for (int i = 0; i < usaArcs.length; i++)
        usaArcs[i] = vis.readUns();
    } catch (EOFException e) {
      throw new VoxFormatException("VoxGraph.read: Unexpected EOF");
    }
    return this;
  }

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    writeBlockHeader(vos);
    vos.writeUns(usGraphSize);
    vos.writeUns(usGrammarID);
    vos.writeUns(usNumberOfArcs);
    vos.writeUns(usNumberOfNodes);
    for (int i = 0; i < usGraphSize - 3; i++)
      vos.writeUns(usaArcs[i]);
  }

  public String toString() {
    return super.toString() + "VoxGraph:\n" +
           "Grammar #" + usGrammarID + "; Graph Size: " + usGraphSize + "; Arcs: " + usNumberOfArcs + "; Nodes: " + usNumberOfNodes +
           "; Word Instances: " + (usGraphSize - 3 * usNumberOfArcs - 3) + "\n\n";
  }
}
