// Voice File clustering service

package voxware.voxfile;

import java.util.*;
import java.io.*;

public class VoxCluster extends ArrayList {
  /**
   * Instance of this class must be filled with Voice Files using ArrayList constructors or add() method
   * before any public method is called. The Voice Files may be represented by byte[], InputStream, or
   * fully qualified FileNameString.
   */

  /** Settable parameters limiting number of K-Means iterations, and random seeding cluster selections */
  public static int MAX_ITERATIONS = 10;
  public static int MAX_RANDOM_TRY = 8;

  /** Constructors */
  public VoxCluster(Collection c) { super(c); }
  public VoxCluster(int capacity) { super(capacity); }

  // Nested class defining everything we need to extract from personal voice files
  class Pers {
    private int        gend;       // gender
    private VoxPattern silPat;     // long silence model
    private VoxPattern wordPat;    // word model

    Pers(int g, VoxPattern s, VoxPattern w) {
      gend = g;
      silPat = s;
      wordPat = w;
    }
    int        gender()  {return gend;}
    VoxPattern silence() {return silPat;}
    VoxPattern word()    {return wordPat;}
  }

  // Nested class defining Cluster
  class Cluster {
    // Fields
    private Pers[]  source; // Contains all objects to be clustered
    private int[][] matrix; // Matrix of distances between source objects
    private int[]   objIdx; // Array of indexes of source objects - members of cluster
    private int     numIdx; // Number of indexes in the objIdx == number of cluster members
    private int     centrum; // Index of cluster center object in the source array
    private int     cluSum; // Sum of distances from all members to center
    // Constructor. Does not check consistency of parameters. distmat must be created and computed for src;
    // cntr must be valid index for src.
    Cluster(Pers[] src, int[][] distmat, int cntr) {
      source = src;
      matrix = distmat;
      centrum = cntr;
      objIdx = new int[source.length];
    }
    // Methods
    int size() { return numIdx; }

    int center() { return centrum; }

    int sum() { return cluSum; }

    void clear() {
      objIdx[0] = centrum;
      numIdx = 1;
      cluSum = 0;
    }

    // Returns distance from object to cluster. Note: matrix[n][n] == 0.
    // For now the simplest: distance from object to cluster center.
    int distance(int idx) { return matrix[centrum][idx]; }

    // Adds new element if it is not the center to cluster.
    void add(int idx) {
      if (idx != centrum) {
        objIdx[numIdx++] = idx;
        cluSum += matrix[centrum][idx];
      }
    }

    // Finds new cluster center. Returns newly computed cluSum. Note: matrix[n][n] == 0.
    // Current center and value of cluSum for current center is a starting point
    int modify() {
      int i, j, sum;
      for (i = 1; i < numIdx; i++) { // consider objIdx[i] as potential new center
        for (j = 0, sum = 0; j < numIdx; j++)
          sum += matrix[objIdx[i]][objIdx[j]];
        if (sum < cluSum) {
          cluSum = sum;
          centrum = objIdx[i];
        }
      }
      return cluSum;
    }
  }

  /**
   * Extracts silence and firstWord models from Voice Files accumulated in this object. Computes output silence as average.
   * For each firstWord and gender selects subset of models containing fair share of specified number of clusters nClusters, 
   * which approximate all models in the best possible way. Saves silence and selected models as Voice File with multiple
   * word templates in OutputStream os.
   */
  public int cluster(VoxProperties firstWordNames, int nClusters, OutputStream os) throws IOException, VoxFormatException {
    VoxFile      vf = null;             // Current and the last personal user voice file
    UserPatterns sp = null;             // Current and the last silence pattern block
    VoxPattern[] spa = null;            // Current and the last silence pattern array
    UserPatterns up = null;             // Current and the last user pattern block
    VoxPattern[] vpa = null;            // Current and the last user pattern array
    Object       o = null;
    int          feVersion = -1, firstGender = -1, i, j, nPers;
    boolean      gendermix = false; // Becomes true if we have gender mix

    // Are personal voice files and first words given ?
    if (isEmpty()) throw new VoxFormatException("VoxCluster.cluster: Empty list of Personal Voice Files");
    if (firstWordNames.size() == 0) throw new VoxFormatException("VoxCluster.cluster: Empty list of First Words");
    ArrayList extracts = new ArrayList(2 * size()); // For case of 2 first words per user as for "sign on".
    int[] sum = new int[VoxPattern.TEMPLATE_DIMENSION]; // accumulator of silence vectors
    Iterator   iter = iterator();
    int silenceCount = 0;
    while (iter.hasNext()) {
      InputStream is = null;
      o = iter.next();
      if (o == null) continue;
      if (o instanceof byte[]) {
        is = new ByteArrayInputStream((byte[])o);
      } else if (o instanceof String) {
        is = new FileInputStream((String)o);
      } else if (o instanceof InputStream) {
        is = (InputStream)o;
      } else {
        throw new VoxFormatException("VoxCluster.cluster: Don't know how to make InputStream from object of <" + o.getClass().getName() + "> class");
      }
      vf = new VoxFile();
      vf.read(is);
      if (!vf.isVoiceFile()) throw new VoxFormatException("VoxCluster.cluster: Personal File is NOT a Voice File");
      if (feVersion == -1) feVersion = vf.getFrontEndVersion();
      else if (feVersion != vf.getFrontEndVersion())
        throw new VoxFormatException("VoxCluster.cluster: Incompatible FEVersions " + feVersion + " and " + vf.getFrontEndVersion());
      // Get pers info
      int g = vf.getGender();
      if (firstGender == -1) firstGender = g;
      else if (g != firstGender) gendermix = true;
      // Silences. We will take first silence model ms1, which is the same as long silence.
      sp = vf.getSilencePatterns();
      if (sp == null) throw new VoxFormatException("VoxCluster.cluster: Malformed Personal Voice File lacks Silence Patterns");
      spa = sp.getPatterns();
      // Accumulate silence using unsigned math
      byte[] silTemplate = spa[0].getTemplate().getBytes();
      for (j = 0; j < VoxPattern.TEMPLATE_DIMENSION; j++) {
        i = silTemplate[j];
        sum[j] += (i & 0xff);
      }
      silenceCount++;
      // Get non silence UserPatterns block and patterns
      up = vf.getUserPatterns();
      vpa = up.getPatterns();
      // Extact models for words specified as first, put them along with gender and silence into extracts.
      for (i = 0; i < vpa.length; i++)
        if (firstWordNames.containsKey(vpa[i].getWordName()))
          extracts.add(new Pers(g, spa[0], vpa[i]));
    } // vf, sp, spa, up of the last person will be a skeleton of output voice file
    if (extracts.size() == 0) return 0; // There is nothing to cluster
    // Compute averaged silence model rounding positive components to nearest integer
    byte[] silTemplate = spa[0].getTemplate().getBytes();
    for (j = 0; j < VoxPattern.TEMPLATE_DIMENSION; j++) {
      i = (((sum[j] << 1) / silenceCount) + 1) >> 1;
      if (i > 255) i = 255; // Shouldn't happen
      silTemplate[j] = (byte)i;
    }
    // Copy this silence model into all output voice file silence models
    for (i = 1; i < spa.length; i++)
      if (spa[i] != null)
        System.arraycopy(silTemplate, 0, spa[i].getTemplate().getBytes(), 0, VoxPattern.TEMPLATE_DIMENSION);
    // Word Model clustering
    ArrayList centroids = new ArrayList(nClusters); // Storage for VoxPatterns of built cluster centroids
    int nClustersLeft = nClusters; // Number of clusters left for remaining extracts elements
    int nClustersReserved = firstWordNames.size(); // We want to reserve at least 1 cluster for each first word and gender
    if (gendermix) nClustersReserved *= 2;
    ArrayList persWork = new ArrayList(extracts.size()); // Here we move a subset of extracts elements having the same wordName and gender
    while (!extracts.isEmpty() && nClustersLeft > 0) {
      nPers = extracts.size();
      if (nPers <= nClustersLeft) {
        // No further clustering needed. Remaining models from extracts become centroids
        iter = extracts.iterator();
        while (iter.hasNext())
          centroids.add(((Pers)iter.next()).word());
        break;
      }
      persWork.clear();
      iter = extracts.iterator();
      o = iter.next();
      int currentGender = ((Pers)o).gender();
      String currentWord = ((Pers)o).word().getWordName();
      // Move the object from extracts to persWork
      persWork.add(o);
      iter.remove();
      while (iter.hasNext()) {
        o = iter.next();
        if (currentGender == ((Pers)o).gender() && currentWord.equals(((Pers)o).word().getWordName())) {
          persWork.add(o);
          iter.remove();
        }
      }
      // Convert ArrayList to Pers[]
      Pers[] persSrc = new Pers[persWork.size()];
      persSrc = (Pers[])persWork.toArray(persSrc);
      // Determine how many clusters to allocate for this subset
      j = nClustersLeft - nClustersReserved + 1; // This max that can be allocated on this step
      if (j < 1) j = 1;
      i = (persSrc.length * nClustersLeft) / nPers;
      if (i < 1) i = 1;
      if (i > j) i = j;
// Debugging piece !!!
//    System.out.println("VoxCluster.cluster: Clustering " + persSrc.length + " models of word " + currentWord + " and " + currentGender + " gender to " + i + " clusters");
      // Do actual clustering of the subset.
      ArrayList ret = doClusters(persSrc, i);
      centroids.addAll(ret);
      nClustersLeft -= ret.size();
      if (nClustersReserved > 1) nClustersReserved--;
    }
    // Convert centroids to VoxPattern[]. Set up user pattern block.
    vpa = new VoxPattern[centroids.size()];
    vpa = (VoxPattern[])centroids.toArray(vpa);
    up.setPatterns(vpa);
    up.setWildcardSum(0);
    up.setWildcardSumSquares(0);
    up.setWildcardCount(0);
    // Maintain original voice file train scheme.
    if (gendermix) up.setGender(UserPatterns.GENDER_UNISEX);
    vf.write(os);
    return up.getNumberPatterns();
  }

  /**
   * The same as above, except that firstWordNames are given in a collection.
   */
  public void cluster(Collection firstWordNames, int nClusters, OutputStream os) throws IOException, VoxFormatException {
    cluster(new VoxProperties(firstWordNames), nClusters, os);
  }

  private ArrayList doClusters(Pers[] src, int nClust) {
    ArrayList ret = new ArrayList(nClust);
    int       numEl = src.length;
    int       i, j, k, d, dClust, totalScore, optScore;

    if (numEl <= nClust) {
      // No clustering needed. Move given models to ret
      for (i = 0; i < numEl; i++)
        ret.add(src[i].word());
      return ret;
    }
    // Compute distortion matrix. Assume symmetry. Diagonal elements remain 0.
    int[][] distmat = new int[numEl][numEl];
    for (i = 0; i < numEl; i++)
      for (j = 0; j < i; j++)
        distmat[j][i] = distmat[i][j] = dist(src[i], src[j]);

// Debugging piece !!! Tested O'K
//  for (i = 0; i < numEl; i++)
//    for (j = 0; j < numEl; j++)
//      distmat[i][j] = dist(src[i], src[j]);
//  Check symmetry
//  for (i = 0; i < numEl; i++) {
//    if (distmat[i][i] != 0) System.out.println("VoxCluster.doClusters: d[i][i] = " + distmat[i][i] + " for index " + i + " rather than 0");
//    for (j = 0; j < i; j++)
//      if (distmat[i][j] != distmat[j][i]) System.out.println("VoxCluster.doClusters: d[i][j] != d[j][i] for indexes " + i + " and " + j);
//  }

    // Allocate arrays of Cluster objects
    Cluster[] optClusters = new Cluster[nClust];
    Cluster[] clusters = new Cluster[nClust];
    // Seed optCluster array with the first no more than nClust uniq src elements as centers.
    for (i = 0, dClust = 0; i < numEl && dClust < nClust; i++) {
      // Make sure this candidate is not the exact match to the already selected cluster centers
      for (j = 0; j < dClust; j++)
        if (optClusters[j].distance(i) == 0)
          break;
      // If no match found add new cluster
      if (j == dClust) optClusters[dClust++] = new Cluster(src, distmat, i);
    }
    // If dClust < nClust, we had so many duplicates (e.g. generic) among models in src, that no further clustering is needed
    if (dClust == nClust) {
      optScore = kMeans(numEl, optClusters);
      // Test MAX_RANDOM_TRY random seeding cluster sets
      Random rand = new Random();
      for (k = 0; k < MAX_RANDOM_TRY; k++) {
        // Fill in test clusters array with uniq random indexes from range [0, numEl-1], checking nonzero distances
        for (dClust = 0; dClust < nClust;) {
          i = rand.nextInt(numEl);
          for (j = 0; j < dClust; j++)
            if (clusters[j].distance(i) == 0)
              break;
          if (j == dClust) clusters[dClust++] = new Cluster(src, distmat, i);
        }
// Debugging piece !!!
//    System.out.print("  Seeds:");
//    for (j = 0; j < nClust; j++)
//      System.out.print(" " + clusters[j].center());
//    System.out.println("");

        totalScore = kMeans(numEl, clusters);
        if (totalScore < optScore) {
          optScore = totalScore;
          System.arraycopy(clusters, 0, optClusters, 0, nClust);
// Debugging piece !!!
//        System.out.println("  Better seeding with the score " + totalScore);
        }
      }
    }
    // Either converged, or exhausted iterations, or had too many duplicates
    // Collect all cluster center models in returned collection. Modify dwells, sums, counts
    for (j = 0; j < dClust; j++) {
// Debugging piece !!!
//    System.out.println("Cluster " + optClusters[j].center() + " covers " + optClusters[j].size() + " models");
      VoxPattern vp = src[optClusters[j].center()].word();
      vp.setSum(0);
      vp.setSquares(0);
      vp.setCount(0);
      vp.setTrainingCount(0);
      vp.setTrainingSavedCount(0);
      byte[] dwells = vp.getDwells().getBytes();
      int nKernels = vp.getDwells().getNumElements();
      // Expand dwells as much as possible, for models now become speaker independent,
      // and because we computed dist below assuming unconstrained dwells
      int sumMaxDwells = 0;
      for (k = 0, i = 0; k < nKernels; k++) {
        dwells[i++] = 1;
        d = (int)dwells[i++] & 0xff;
        sumMaxDwells += d;
      }
      // This much can add to each maxdwell so sum of them does not exceed 255 limit
      d = (255 - sumMaxDwells) / nKernels;
      for (k = 0; k < nKernels; k++)
        dwells[2*k + 1] += (byte)d;
      ret.add(vp);
    }
    return ret;
  }

  // K-Means algorithm clustering numEl object against array of clasters. Returns total score
  private int kMeans(int numEl, Cluster[] clusters) {
    int       i, j, k, d, min, iteration, oldTotal, newTotal = 0, nClust = clusters.length;
    for (iteration = 0, oldTotal = Integer.MAX_VALUE; iteration < MAX_ITERATIONS; iteration++) {
      // Reset clusters
      for (j = 0; j < nClust; j++)
        clusters[j].clear();
      // Classify each model as belonging to the closest cluster with price min. Accumulate prices in the newTotal
      for (i = 0, newTotal = 0; i < numEl; i++) {
        for (j = 0, min = Integer.MAX_VALUE, k = -1; j < nClust; j++) {
          if ((d = clusters[j].distance(i)) < min) {
            min = d;
            k = j;
          }
        }
        // Add src[i] to the closest cluster. If i is the center of clusters[k], min == 0, and add() does nothing
        clusters[k].add(i);
        newTotal += min;
      }

// Debugging piece !!!
//    for (j = 0, d = 0; j < nClust; j++)
//      d += clusters[j].sum();
//    System.out.println("    VoxCluster.kMeans: On iteration " + iteration + " after classification the total = " + newTotal + "; Sum = " + d);
//    System.out.println("    VoxCluster.kMeans: On iteration " + iteration + " after classification the total = " + newTotal);
      // Check convergency
      if (newTotal >= oldTotal) break; // We are done
      oldTotal = newTotal;
      // Modify cluster centers on basis of newly collected elements. Accumulate cluster scores in the newTotal
      for (j = 0, newTotal = 0; j < nClust; j++)
        newTotal += clusters[j].modify();
// Debugging piece !!!
//    System.out.println("    VoxCluster.kMeans: On iteration " + iteration + " after modification the total = " + newTotal);
      // Check convergency
      if (newTotal >= oldTotal) break; // We are done
      oldTotal = newTotal;
    }
    return newTotal;
  }

  // Distortion computed by means of dynamic programming alignment of word utterances.
  // Corresponding silences are used for boundary matching.
  private int dist(Pers x, Pers y) {
    int    i, j, xoffset, yoffset, min, cnt;
    int    jSize = VoxPattern.TEMPLATE_DIMENSION;             // Jin dimension
    int    xLen  = x.word().getTemplate().getNumElements();   // x's numKernels
    byte[] xTem  = x.word().getTemplate().getBytes();         // x's word template
    byte[] xSil  = x.silence().getTemplate().getBytes();      // x's silence template
    int    yLen  = y.word().getTemplate().getNumElements();   // y's numKernels
    byte[] yTem  = y.word().getTemplate().getBytes();         // y's word template
    byte[] ySil  = y.silence().getTemplate().getBytes();      // y's silence template

    // Compute distances between templates' kernels and their own silence
    int[]  xTemToSilLap = new int[xLen];
    for (i = 0, xoffset = 0; i < xLen; i++, xoffset += jSize)
      xTemToSilLap[i] = lap(xSil, 0, xTem, xoffset);
    int[]  yTemToSilLap = new int[yLen];
    for (j = 0, yoffset = 0; j < yLen; j++, yoffset += jSize)
      yTemToSilLap[j] = lap(ySil, 0, yTem, yoffset);
    // Arrays of DP scores and passed node counters
    int[] newScores = new int[xLen + 2];
    int[] oldScores = new int[xLen + 2];
    int[] newNodes = new int[xLen + 2];
    int[] oldNodes = new int[xLen + 2];
    // Initialize 0-th row matching all x-kernels against its own x-silence
    newScores[0] = newNodes[0] = 0;
    // Include terminating silence in loop. For the last node any value greater than previous will do, which makes it never appear on optimal path
    for (i = 0; i <= xLen; i++) {
      newScores[i + 1] = newScores[i] + ((i < xLen) ? xTemToSilLap[i] : 1);
      newNodes[i + 1] = newNodes[i] + 1;
    }
    // For each real y-kernel row, and than terminating x-silence
    for (j = 0, yoffset = 0; j <= yLen; j++, yoffset += jSize) {
      // Swap arrays
      int[] tmp = oldScores;
      oldScores = newScores;
      newScores = tmp;
      tmp       = oldNodes;
      oldNodes  = newNodes;
      newNodes  = tmp;
      // Start new scores row matching current y-kernel with y-silence, except the last node set to 1, which makes it never appear on optimal path
      newScores[0] = oldScores[0] + ((j < yLen) ? yTemToSilLap[j] : 1);
      newNodes[0] = oldNodes[0] + 1;
      // For each real x-kernel column, and than terminating y-silence
      for (i = 0, xoffset = 0; i <= xLen; i++, xoffset += jSize) {
        // Find conditionally optimal source node.
        // Taking diagonal first precludes appearance of right angle pieces of trajectory on optimal path
        min = oldScores[i];
        cnt = oldNodes[i];
        // Compare it to horizontal
        if (newScores[i] < min) {
          min = newScores[i];
          cnt = newNodes[i];
        }
        // Than vertical
        if (oldScores[i + 1] < min) {
          min = oldScores[i + 1];
          cnt = oldNodes[i + 1];
        }
        // Now add contribution of the (i+1,j+1) node to the conditionally optimal path
        if (i == xLen && j == yLen) {
          // We reached the end. Return the normalized score rounded to the nearest positive integer
          return ((min << 1) / cnt + 1) >> 1;
        } else {
          newScores[i + 1] = min + ((i < xLen) ? ((j < yLen) ? lap(yTem, yoffset, xTem, xoffset) : xTemToSilLap[i]) : yTemToSilLap[j]);
          newNodes[i + 1] = cnt + 1;
        }
      }
    }
    // Should not reach this point, but compiler coplains
    return 0;
  }

  // Laplacian distance between portions of byte arrays specified by corresponding offsets
  private static int lap(byte[] f, int foffs, byte[] s, int soffs) {
    int sum = 0, first, second;
    for (int i = 0; i < VoxPattern.TEMPLATE_DIMENSION; i++) {
      first  = (int)f[foffs++] & 0xff;
      second = (int)s[soffs++] & 0xff;
      sum += (first > second) ? first - second : second - first;
    }
    return sum;
  }

  // Main
  public static void main(String argv[]) {

    if (argv.length != 4) {
      System.out.println("Usage: java VoxCluster VoiceFilesListFileName WordsListFileName DestinationVoiceFileName NumberOfClusters");
      return;
    }
    
    try {
      VoxProperties vpFiles = new VoxProperties(new FileInputStream(argv[0]));
      VoxProperties vpWords = new VoxProperties(new FileInputStream(argv[1]));
      int nClusters = Integer.parseInt(argv[3]);
      if (vpFiles.size() > 0 && vpWords.size() > 0 && nClusters > 0) {
        VoxCluster vc = new VoxCluster(vpFiles.size());
        Enumeration e = vpFiles.propertyNames();
        while (e.hasMoreElements())
          vc.add(e.nextElement());
        int n = vc.cluster(vpWords, nClusters, new FileOutputStream(argv[2]));
        System.out.println("Built " + n + " clusters for " + vpFiles.size() + " users");
      } else {
        System.out.println("Both file and word lists must not be empty, and NumberOfClusters must be positive");
      }
    } catch (Throwable t) {
      System.out.println("VoxCluster.main: caught Throwable " + t.toString());
      t.printStackTrace();
    }
  }
}
