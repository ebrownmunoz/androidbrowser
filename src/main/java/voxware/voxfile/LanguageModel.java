// LanguageModel -> VoxBlock

package voxware.voxfile;

import java.io.*;

public class LanguageModel extends VoxBlock {

	// Instance Fields and their access method
	private int    usVocabulary;
	public  int    getVocabulary() {return usVocabulary;}
	public  void   setVocabulary(int i) {usVocabulary = i;}

	private int    usFirstSilence;
	public  int    getFirstSilence() {return usFirstSilence;}
	public  void   setFirstSilence(int i) {usFirstSilence = i;}

	private long   ulNumUnigrams;
	public  long   getNumUnigrams() {return ulNumUnigrams;}
	public  void   setNumUnigrams(long l) {ulNumUnigrams = l;}

	private long   ulNumBigrams;
	public  long   getNumBigrams() {return ulNumBigrams;}
	public  void   setNumBigrams(long l) {ulNumBigrams = l;}

	private int    usNumNodes;
	public  int    getNumNodes() {return usNumNodes;}
	public  void   setNumNodes(int i) {usNumNodes = i;}

	private int    slLanguageWeight;
	public  int    getLanguageWeight() {return slLanguageWeight;}
	public  void   setLanguageWeight(int i) {slLanguageWeight = i;}

	private int    slUnigramWeight;
	public  int    getUnigramWeight() {return slUnigramWeight;}
	public  void   setUnigramWeight(int i) {slUnigramWeight = i;}

	private int    slWordInsertPenalty;
	public  int    getWordInsertPenalty() {return slWordInsertPenalty;}
	public  void   setWordInsertPenalty(int i) {slWordInsertPenalty = i;}

	private int    slPhoneInsertPenalty;
	public  int    getPhoneInsertPenalty() {return slPhoneInsertPenalty;}
	public  void   setPhoneInsertPenalty(int i) {slPhoneInsertPenalty = i;}

	private int    slSilInsertPenalty;
	public  int    getSilInsertPenalty() {return slSilInsertPenalty;}
	public  void   setSilInsertPenalty(int i) {slSilInsertPenalty = i;}

	private int    usStartingWIID;
	public  int    getStartingWIID() {return usStartingWIID;}
	public  void   setStartingWIID(int i) {usStartingWIID = i;}

	private int    usEndingWIID;
	public  int    getEndingWIID() {return usEndingWIID;}
	public  void   setEndingWIID(int i) {usEndingWIID = i;}

	private Unigram[] uaUnigrams;
	public  Unigram[] getUnigrams() {return uaUnigrams;}

	// Constructors
	// Package scope for read()
	LanguageModel(VoxFile voxFile) {
		super(voxFile);
		blockID = FLM;
	}

	// Public
	public LanguageModel(VoxFile voxFile, int grammarID, Unigram[] unigrams) {
		super(voxFile);
		blockID = FLM;
		classID = grammarID;
		uaUnigrams = unigrams;
		ulNumUnigrams = unigrams.length;
	}

	// Instance methods

	// Package scope
	long bodySize() {
		long l = 5*2 + 7*4; // 5 of V_Uns, 2 of V_ULong, 5 of V_Long
		for (int i = 0; i < uaUnigrams.length; i++)
			l += uaUnigrams[i].bodySize();
		return l;
	}

	// Package scope. blockID must have been read !!!
	LanguageModel read(VoxInputStream vis) throws IOException, VoxFormatException {
		try {
			readBlockHeader(vis);
			// Read object body
			usVocabulary = vis.readUns();
			usFirstSilence = vis.readUns();
			ulNumUnigrams = vis.readULong();
			ulNumBigrams = vis.readULong();
			usNumNodes = vis.readUns();
			slLanguageWeight = vis.readSLong();
			slUnigramWeight = vis.readSLong();
			slWordInsertPenalty = vis.readSLong();
			slPhoneInsertPenalty = vis.readSLong();
			slSilInsertPenalty = vis.readSLong();
			usStartingWIID = vis.readUns();
			usEndingWIID = vis.readUns();
			uaUnigrams = new Unigram[(int)ulNumUnigrams];
			for (int i = 0; i < uaUnigrams.length; i++)
				uaUnigrams[i] = Unigram.read(vis);
		} catch (EOFException e) {
			throw new VoxFormatException("LanguageModel.read: Unexpected EOF");
		}
		return this;
	}

	void write(VoxOutputStream vos) throws IOException, VoxFormatException {
		writeBlockHeader(vos);
		vos.writeUns(usVocabulary);
		vos.writeUns(usFirstSilence);
		vos.writeULong(ulNumUnigrams);
		vos.writeULong(ulNumBigrams);
		vos.writeUns(usNumNodes);
		vos.writeSLong(slLanguageWeight);
		vos.writeSLong(slUnigramWeight);
		vos.writeSLong(slWordInsertPenalty);
		vos.writeSLong(slPhoneInsertPenalty);
		vos.writeSLong(slSilInsertPenalty);
		vos.writeUns(usStartingWIID);
		vos.writeUns(usEndingWIID);
		for (int i = 0; i < uaUnigrams.length; i++)
			uaUnigrams[i].write(vos);
	}

	public String toString() {
		return super.toString() + "LanguageModel:\n" +
				"Vocabulary: " + usVocabulary + "; #Unigrams: " + ulNumUnigrams + "; #Bigrams: " + ulNumBigrams + "; #Nodes: " + usNumNodes + "\n\n";
	}
}
