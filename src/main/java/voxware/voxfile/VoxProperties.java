  /**
   * VoxProperties - subclass of java.util.Properties with several extra constructors.
   * Facilitates Case insensitive search of word names, converting keys to upper case and putting
   * original keys (or values when nonempty) into values,  when ignoreCase == true..
   * Overrides constructor with Properties parameter, which means NO "defaults" properties are supported.
   */

package voxware.voxfile;

import java.util.*;
import java.io.*;

public class VoxProperties extends Properties {

  public VoxProperties(String key, boolean ignoreCase) {
    super();
    if (key != null) {
      if (ignoreCase) setProperty(key.toUpperCase(), key);
      else            setProperty(key, key);
    }
  }

  public VoxProperties(String key) {
    this(key, false);
  }

  public VoxProperties(String[] keys, boolean ignoreCase) {
    super();
    if (keys != null)
      for (int i = 0; i < keys.length; i++)
        if (keys[i] != null) {
          if (ignoreCase) setProperty(keys[i].toUpperCase(), keys[i]);
          else            setProperty(keys[i], keys[i]);
        }
  }

  public VoxProperties(String[] keys) {
    this(keys, false);
  }

  public VoxProperties(Collection keys, boolean ignoreCase) {
    super();
    if (keys != null) {
      Iterator   i = keys.iterator();
      Object o;
      while (i.hasNext())
        if ((o = i.next()) instanceof String) {
          String key = (String)o;
          if (ignoreCase) setProperty(key.toUpperCase(), key);
          else            setProperty(key, key);
        }
    }
  }

  public VoxProperties(Collection keys) {
    this(keys, false);
  }

  public VoxProperties(Properties p, boolean ignoreCase) {
    super();
    if (p.size() > 0) {
      Enumeration e = p.propertyNames();
      while (e.hasMoreElements()) {
        String src = (String)e.nextElement();
        String dst = p.getProperty(src);
        if (src.length() == 0) continue;
        if (dst.length() == 0) dst = src;
        if (ignoreCase) setProperty(src.toUpperCase(), dst);
        else            setProperty(src, dst);
      }
    }
  }

  public VoxProperties(Properties p) {
    this(p, false);
  }

  /**
   * Creates and loads VoxProperties with specified encoding of keys and values.
   * If charSetName == null uses standard Properties load function, otherwise
   * uses first " ", than '=', and the last ':', as key separators.
   * Line containing single word starting with '=' or ':' is ignored.
   */
  public VoxProperties(InputStream is, String charSetName, boolean ignoreCase) {
    super();
    try {
      if (charSetName == null) {
        Properties p = new Properties();
        p.load(is);
        if (p.size() > 0) {
          Enumeration e = p.propertyNames();
          while (e.hasMoreElements()) {
            String src = (String)e.nextElement();
            String dst = p.getProperty(src);
            if (src.length() == 0) continue;
            if (dst.length() == 0) dst = src;
            if (ignoreCase) setProperty(src.toUpperCase(), dst);
            else            setProperty(src, dst);
          }
        }
      } else {
        String line, left, right;
        int    i;

        BufferedReader br = new BufferedReader(new InputStreamReader(is, charSetName));
        while ((line = br.readLine()) != null) {
          line = line.trim();
          if (line.length() == 0) continue;
          if ((i = line.indexOf(' ')) < 0 && (i = line.indexOf('=')) < 0 && (i = line.indexOf(':')) < 0) {
            left = line;
            right = line;
          } else {
            left = line.substring(0, i).trim();
            // If the first non-white space char is '=' or ':', and no ' ' after, it is single word comment line
            if (left.length() == 0) continue;
            right = line.substring(i + 1).trim();
            if (right.length() == 0) right = left;
          }
          if (ignoreCase) setProperty(left.toUpperCase(), right);
          else            setProperty(left, right);
        }
      }
    } catch (IOException ioe) {
      throw new IllegalArgumentException("VoxProperties: " + ioe.getMessage());
    }
  }

  public VoxProperties(InputStream is, String charSetName) {
    this(is, charSetName, false);
  }

  public VoxProperties(InputStream is, boolean ignoreCase) {
    this(is, null, ignoreCase);
  }

  public VoxProperties(InputStream is) {
    this(is, null, false);
  }
}
