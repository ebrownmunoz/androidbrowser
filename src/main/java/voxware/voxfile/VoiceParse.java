// VoiceParse -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class VoiceParse extends ParseTable {

   // Constructor for VoxBlock.read()
  VoiceParse(VoxFile voxFile) {
	  super(voxFile);
    blockID = VOICE_PARSE;
  }

   // Constructor for Java Convert writers. Should check arguments consistency.
  public VoiceParse(VoxFile voxFile, int grammarID) {
    super(voxFile, grammarID, null, null);
    blockID = VOICE_PARSE;
  }

  public VoiceParse(VoxFile voxFile, int grammarID, long[] offsets, int[] data) {
    super(voxFile, grammarID, offsets, data);
    blockID = VOICE_PARSE;
  }

  public String toString() {
    return super.toString() + "VoiceParse: State Count = " + ulaOffsets.length + "\n\n";
  }
}
