// VoxUnknown -> VoxBlock

package voxware.voxfile;

import java.io.*;

public class VoxUnknown extends VoxBlock {

   // Instance Fields and their access method
  private byte[] body;

   // Package scope. Only VoxBlock.read() calls it
  VoxUnknown(VoxFile voxFile, int uBlockID) {
	  super(voxFile);
	  blockID = uBlockID;}

   // Package scope. blockID must have been read !!!
  VoxUnknown read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      readBlockHeader(vis);
      body = new byte[(int)blockSize];
      if (blockSize > 0) vis.readFully(body);
    } catch (EOFException e) {
      throw new VoxFormatException("VoxUnknown.read: Unexpected EOF");
    }
    return this;
  }

   // Package scope
  long bodySize() {return body.length;}

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    writeBlockHeader(vos);
    if (blockSize > 0) vos.write(body);
  }

  public String toString () {
    return super.toString() + "VoxUnknown\n\n";
  }
}
