// VoxFile - Rec/Voi files manipulation service

package voxware.voxfile;

import java.util.*;
import java.io.*;

public class VoxFile {

	// Constants
	// Class Fields

	// Package scope. Defaults to Convert word encoding. Legacy Voice File may use "ISO-8859-1" as had old WConvert
	static String WORD_ENCODING = "UTF-8";

	// Class Methods
	// Instance Fields

	private VoxText textBlock; // The main text block. Referenced by other blocks.

	private ArrayList    blocks;     // Block List
	public  ArrayList    getBlocks() { return blocks; }

	private Iterator     iterator;   // Support for rewind, getNextBlock methods

	private boolean      isVoice;
	public  boolean      isVoiceFile() { return isVoice; }

	public  boolean      isRecFile() { return getVocabularyTranslation() != null; }

	private UserPatterns userPatterns;
	public  UserPatterns getUserPatterns() { return userPatterns; }

	private UserPatterns silencePatterns;
	public  UserPatterns getSilencePatterns() { return silencePatterns; }

	private int          gender;
	public  int          getGender() { return gender; }

	private int          feVersion;
	public  int          getFrontEndVersion() { return feVersion; }

	private int          trainScheme;
	public  int          getTrainScheme() { return trainScheme; }

	private VocabularyTranslation vocabularyTranslation;
	public  VocabularyTranslation getVocabularyTranslation() { return vocabularyTranslation; }

	private ArrayList    grammars;     // Grammar List for grammar names in script printing
	public  ArrayList    getGrammars() { return grammars; }

	// Constructors. Defaults to 300 blocks - reasonable for recfile with less than 100 grammars
	public VoxFile() {
		this(300);
	}

	public VoxFile(int numBlocks) {
		blocks = new ArrayList(numBlocks);
		grammars = new ArrayList(100);
	}

	// Instance Methods
	public void read(InputStream is) throws IOException, VoxFormatException {
		VoxInputStream vis = new VoxInputStream(is);
		boolean endOfFile = false;

		blocks.clear();
		iterator = null;
		try {
			while(!endOfFile) {
				try {
					Object block = VoxBlock.readBlock(this, vis); // Throws EOFException, IOException, VoxFormatException
					blocks.add(block);
					// Some insurance
					if (blocks.size() == 1 && !(block instanceof VoxKey))
						throw new VoxFormatException("VoxFile.read: First block is not VoxKey");
					if (blocks.size() == 2 && !(block instanceof VoxVersion))
						throw new VoxFormatException("VoxFile.read: Second block is not VoxVersion");
					if (block instanceof UserPatterns && ((UserPatterns)block).getClassID() == 0) {
						silencePatterns = (UserPatterns)block;
					} else if (block instanceof UserPatterns && ((UserPatterns)block).getClassID() > 0) {
						isVoice = true;
						userPatterns = (UserPatterns)block;
						gender = userPatterns.getGender();
						trainScheme = userPatterns.getTrainScheme();
						VoxParameter[] pars = userPatterns.getParameters();
						for (int i = 0; i < pars.length; i++)
							if (pars[i].getNumber() == VoxParameter.FEVERSION)
								feVersion = pars[i].getValue();
					} else if (block instanceof VocabularyTranslation) {
						vocabularyTranslation = (VocabularyTranslation)block;
					} else if (block instanceof VoxGrammar) {
						grammars.add(block);
					}
				} catch (EOFException e) {
					endOfFile = true;
				}
			}
		} finally {
			vis.close();
			rewind();
		}
	}

	public void read(String fileName) throws FileNotFoundException, IOException, VoxFormatException {
		InputStream is = new FileInputStream(fileName); // Throws FileNotFoundException
		read(is);
	}

	public void addBlock(VoxBlock block) {
		blocks.add(block);
	}

	public int getBlockCounter() {
		return blocks.size();
	}

	public void rewind() {
		if (blocks.size() > 0)
			iterator = blocks.iterator();
	}

	public Object getNextBlock() {
		if (iterator != null && iterator.hasNext())
			return iterator.next();

		return null;
	}

	public String getText(LenOff loff) {
		return textBlock.getText(loff);
	}

	public void write(OutputStream os) throws IOException, VoxFormatException {
		VoxOutputStream vos = new VoxOutputStream(os);
		rewind();
		if (iterator == null) throw new VoxFormatException("VoxFile.write: VoxFile is empty");
		try {
			while (iterator.hasNext())
				((VoxBlock)iterator.next()).write(vos); // Throws IOException, VoxFormatException
		} finally {
			vos.close();
		}
	}

	public void write(String fileName) throws FileNotFoundException, IOException, VoxFormatException {
		OutputStream os = new FileOutputStream(fileName); // Throws FileNotFoundException
		write(os);
	}

	// Main. Dumps VoxFile
	public static void main(String argv[]) {

		if (argv.length == 0) {
			System.out.println("Usage: java voxware.voxfile.VoxFile fileName [wordName]");
			return;
		}

		VoxFile vf = new VoxFile();

		boolean gotException = false;
		try {
			vf.read(argv[0]);
			// Try to print whatever was able to read
			VoxBlock vb;
			while ((vb = (VoxBlock)vf.getNextBlock()) != null) {
				byte[] ba;
				if (vb instanceof UserPatterns && argv.length > 1)
					ba = ((UserPatterns)vb).toString(argv[1]).getBytes("UTF-8");
				else if (vb instanceof VoxScript)
					ba = ((VoxScript)vb).toString(vf).getBytes("UTF-8");
				else
					ba = vb.toString().getBytes("UTF-8");
				System.out.write(ba, 0, ba.length);
			}
		} catch (Throwable t) {
			System.out.println("VoxFile: caught Throwable " + t.toString() + ", reading file " + argv[0]);
			t.printStackTrace();
			gotException = true;
		}
	}

	public void setTextBlock(VoxText textBlock) {
		this.textBlock = textBlock;	
	}
}
