// VoxBlock - generic block in VoxFile

package voxware.voxfile;

import java.util.*;
import java.io.*;

public class VoxBlock {

   // Constants

   // Block ID's definitions
   // System Class
  public static final int KEY =                  100;
  public static final int VERSION =              101;

   // Application Class
  public static final int IDENT =                200;
  public static final int TEXT =                 201;
  public static final int RESPONSE_TEMPLATE =    202;
  public static final int CONTROL_TEMPLATE =     203;
//    public static final int SWITCH =               204;
//    public static final int PHONETICS =            205;
//    public static final int VPF =                  206;
//    public static final int DPF =                  207;
//    public static final int HELP =                 208;
//    public static final int KEY_MAP =              209;
  public static final int DICT =                 210;

   // Vocabulary Class
  public static final int USER_PATTERNS =        300;  // TR_UPAT
  public static final int VOCABULARY_TRANS =     301;
  public static final int SCRIPT =               302;
//    public static final int RECON =                303;
  public static final int CONTROL_PARSE_VOCAB =  304;
  public static final int VOICE_PARSE_VOCAB =    305;
  public static final int HOST_PARSE_VOCAB =     306;
  public static final int DISPLAY_PARSE_VOCAB =  307;

   // Grammar Class
  public static final int GRAMMAR =              400;
  public static final int GRAPH =                401;
  public static final int GRAMMAR_TRANS =        402;
  public static final int CONTROL_PARSE =        403;
  public static final int VOICE_PARSE =          404;
  public static final int HOST_PARSE =           405;
  public static final int DISPLAY_PARSE =        406;
  public static final int FLM =                  410;
//    public static final int FUGRAM =               411;
//    public static final int FBGRAM =               412;

  public static final int FILE_OVERHEAD =          6;    // sizeof(blockSize) + sizeof(checksum)
  public static final int MAX_BLOCK_SIZE =   2097152;    // 2M per DCV
  
  protected VoxFile voxFile;

   // Class Methods
  public String getText(LenOff loff) {
	  
	return voxFile.getText(loff);
  }

  public static Object readBlock(VoxFile voxFile, VoxInputStream vis) throws IOException, VoxFormatException {
     // Read Block Header
    int  uBlockID = vis.readUns(); // Throws EOFException here if clean end file reached
     // Dispatch
    switch(uBlockID) {

      case KEY:
        return (new VoxKey(voxFile)).read(vis);

      case VERSION:
        return (new VoxVersion(voxFile)).read(vis);

      case TEXT:
        VoxText textBlock = (new VoxText(voxFile)).read(vis);
    	  
    	voxFile.setTextBlock(textBlock);
        return textBlock;

      case USER_PATTERNS:
        return (new UserPatterns(voxFile)).read(vis);

      case IDENT:
        return (new VoxIdent(voxFile)).read(vis);

      case RESPONSE_TEMPLATE:
        return (new ResponseTemplate(voxFile)).read(vis);

      case CONTROL_TEMPLATE:
        return (new ControlTemplate(voxFile)).read(vis);

      case CONTROL_PARSE:
        return (new ControlParse(voxFile)).read(vis);

      case VOICE_PARSE:
        return (new VoiceParse(voxFile)).read(vis);

      case HOST_PARSE:
        return (new HostParse(voxFile)).read(vis);

      case DISPLAY_PARSE:
        return (new DisplayParse(voxFile)).read(vis);

      case CONTROL_PARSE_VOCAB:
        return (new ControlParseVocabulary(voxFile)).read(vis);

      case VOICE_PARSE_VOCAB:
        return (new VoiceParseVocabulary(voxFile)).read(vis);

      case HOST_PARSE_VOCAB:
        return (new HostParseVocabulary(voxFile)).read(vis);

      case DISPLAY_PARSE_VOCAB:
        return (new DisplayParseVocabulary(voxFile)).read(vis);

      case VOCABULARY_TRANS:
        return (new VocabularyTranslation(voxFile)).read(vis);

      case GRAMMAR:
        return (new VoxGrammar(voxFile)).read(vis);

      case GRAMMAR_TRANS:
        return (new GrammarTranslation(voxFile)).read(vis);

      case GRAPH:
        return (new VoxGraph(voxFile)).read(vis);

      case SCRIPT:
        return (new VoxScript(voxFile)).read(vis);

      case FLM:
        return (new LanguageModel(voxFile)).read(vis);

      case DICT:
        return (new VoxDictionary(voxFile)).read(vis);

      default:
        return (new VoxUnknown(voxFile, uBlockID)).read(vis);
    }
  }

   // Instance Fields
  protected int  blockID;   // Immutable after construction, so only getter provided
  protected int  classID;
  protected long blockSize; // For internal read/write
  private   int  checksum;  // Unused. Preserved if comes from read, otherwise 0

   // Constructor
  public VoxBlock(VoxFile voxFile) {
	  this.voxFile = voxFile;
  }
  
  public VoxFile getVoxFile() {
	  return this.voxFile;
  }

   // Instance Methods
  public int  getBlockID() { return blockID; }
  public int  getClassID() { return classID; }
  public void setClassID(int id) { classID = id; }

   // Package scope
  long bodySize() {return 0;} // generic block is empty

  void readBlockHeader(VoxInputStream vis) throws VoxFormatException, IOException {
     // blockID must have been read !!!
    classID = vis.readUns();
    blockSize = vis.readULong() - FILE_OVERHEAD;
    if (blockSize > MAX_BLOCK_SIZE) throw new VoxFormatException("VoxBlock.readBlockHeader: blockSize > 2M");
    checksum = vis.readUns();
  }

  void writeBlockHeader(VoxOutputStream vos) throws VoxFormatException, IOException {
    vos.writeUns(blockID);
    vos.writeUns(classID);
    blockSize = bodySize();
    if (blockSize > MAX_BLOCK_SIZE) throw new VoxFormatException("VoxBlock.writeBlockHeader: blockSize > 2M");
    if (blockSize == 0) throw new VoxFormatException("VoxBlock.writeBlockHeader: Empty Block");
    vos.writeULong(blockSize + FILE_OVERHEAD);
    vos.writeUns(checksum);
  }

  void write(VoxOutputStream vos) throws VoxFormatException, IOException {
    throw new VoxFormatException("VoxBlock.write: Generic write() must be overridden");
  }

  public String toString() {
    return "ID = " + blockID + "; Class = " + classID + "; Size = " + blockSize + "; ";
  }
}
