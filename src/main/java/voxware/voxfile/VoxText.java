// VoxText -> VoxBlock

package voxware.voxfile;

import java.io.*;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

/* VoxText
 * This implementation is bi-modal.  If a text block is read in, a byte array
 * is used to store the text and to access the characters.  The other mode
 * is utilized when building a new text block from scratch in memory (e.g. convert).
 * TextBlockEntries are added to a LinkedHashMap, and the LenOff's are 
 * computed by traversing the entires in the map.
 */

public class VoxText extends VoxBlock {
  private byte[] buffer = null;
  private String image = null;
  private LinkedHashMap entries;
  private TextBlockEntry previousEntry;
  private static LenOff nullLenOff;

  static {
    nullLenOff = new LenOff(0, 0);
  }

   // Instance Fields and their access method
  private int    counter; // Actual number of bytes so far

   // Constructor
  public VoxText(VoxFile voxFile) {
	super(voxFile);
    blockID = TEXT;
    entries = new LinkedHashMap();
    counter = 0; // Empty
  }

   // Package scope. blockID must have been read !!!
  VoxText read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      readBlockHeader(vis);
      counter = (int)blockSize;
      buffer = new byte[counter];
      image = null;
      if (counter > 0)
        vis.readFully(buffer, 0, counter);
    } catch (EOFException e) {
      throw new VoxFormatException("VoxText.read: Unexpected EOF");
    }
    return this;
  }

  public String getText(LenOff lo) {
    String result = null;

    if (buffer != null) {
      int  len = lo.getLen();
      long off = lo.getOffset();
      if (len + off > counter) throw new IndexOutOfBoundsException("VoxText.text: Out of Bounds");
      try {
        return new String(buffer, (int)off, len, VoxFile.WORD_ENCODING);
      } catch (UnsupportedEncodingException e) {
        return "VoxText.text: UnsupportedEncodingExceptionCaught";
      }
    } else {
      Object key = null;
      Set keySet = entries.keySet();
      for (Iterator eit = keySet.iterator(); eit.hasNext(); ) {
        key = eit.next();
        TextBlockEntry tbe = (TextBlockEntry) entries.get(key);

        if (tbe.getOffset() < lo.getOffset()) {
          continue;
        } else if (tbe.getOffset() == lo.getOffset()) {
          if (tbe.getLen() == lo.getLen())
            result = tbe.getString();
          break;
        } else if (tbe.getOffset() > lo.getOffset()) {
          break;
        }
      }
    }
    return result;
  }

  public TextBlockEntry getEntry(String s) {
    return (TextBlockEntry) entries.get(s);
  }

  public LenOff getLenOff(String s) {
    if (s == null)
      return nullLenOff;

    if (buffer != null) {
      if (image == null) {
        try {
          image = new String(buffer, VoxFile.WORD_ENCODING);
        } catch (UnsupportedEncodingException e) {
          e.printStackTrace();
          return null;
        }
      }

      int offset = image.indexOf(s);
      if (offset == -1)
        return nullLenOff;
      else
        return new LenOff(s.length(), offset);
    } else {
      TextBlockEntry entry = (TextBlockEntry) entries.get(s);

      /*!! CAV */
      if (entry == null) {
        return nullLenOff;
      } else {
        return (LenOff) entry;
      }
    }
  }

   // Converts String to byte[] appends content to the baText, reallocating if needed, returns LenOff of added content
  public LenOff addText(String s) throws VoxFormatException {
    LenOff result = nullLenOff;

    if (s == null || s.length() == 0)
      return result;

    if (buffer != null) {
      byte[] ba;

      // Clear the image String when new text is added, lazily reconstruct later
      image = null;

      try {
        ba = s.getBytes(VoxFile.WORD_ENCODING);
      } catch (UnsupportedEncodingException e) {
        throw new VoxFormatException("VoxText.addText: UnsupportedEncodingException caught");
      }
      if (ba.length > LenOff.MAX_LEN) throw new VoxFormatException("VoxText.addText: string too long");
      if (counter + ba.length > buffer.length) {
        byte[] newBuffer = new byte[2 * buffer.length];
        System.arraycopy(buffer, 0, newBuffer, 0, counter);
        buffer = newBuffer;
      }
      System.arraycopy(ba, 0, buffer, counter, ba.length);
      long offset = counter;
      counter += ba.length; // Exceeding 2M limit on blockSize unlikely
      result = new LenOff(ba.length, offset);
    } else {
      TextBlockEntry value = new TextBlockEntry(s);

      // Hash TextBlockEntry using s as key
      if (!entries.containsKey(s)) {
        int size = entries.size();
        if (previousEntry == null) {
          value.setOffset(0);
        } else {
          // Compute offset: lest element offset + last element size
          value.setOffset(previousEntry.getLen() + previousEntry.getOffset());
        }
        previousEntry = value;
        TextBlockEntry entry = (TextBlockEntry) entries.put(s, value);

        if (entry == null) {
          return (LenOff) value;
        } else {
          return (LenOff) entry;
        }
      } else {
        TextBlockEntry tbe = (TextBlockEntry) entries.get(s);
        return (LenOff) tbe;
      }
    }
  
    return result;
  }

   // Package scope
  long bodySize() {return counter;}

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    if (buffer != null) {
      writeBlockHeader(vos);
      // must be blockSize == counter thru bodySize() call in writeBlockHeader
      if (blockSize > 0)
        vos.write(buffer, 0, (int)blockSize);
    } else {
      /* first pass is for counting */
      counter = 0;
      Object key = null;
      TextBlockEntry tbe = null;
      Set keySet = entries.keySet();
      for (Iterator eit = keySet.iterator(); eit.hasNext(); ) {
        key = eit.next();
        tbe = (TextBlockEntry) entries.get(key);
        counter += tbe.getLen();
      }

      writeBlockHeader(vos);
      for (Iterator eit = keySet.iterator(); eit.hasNext(); ) {
        key = eit.next();
        tbe = (TextBlockEntry) entries.get(key);
        byte[] bytes = tbe.getString().getBytes(VoxFile.WORD_ENCODING);
        vos.write(bytes, 0, bytes.length);
      }
    }
  }

  public String toString () {
    return super.toString() + "VoxText\n\n";
  }
}
