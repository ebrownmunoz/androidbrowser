// VoxPattern - component of UserPatterns - word profile

package voxware.voxfile;

import java.io.*;
import java.util.Arrays;

public class VoxPattern {

   // Constants

  public static final int MAX_KERNELS =          42;
  public static final int TEMPLATE_DIMENSION =   16;
  public static final int WORD_MODEL_DIMENSION =  2;

   // Class Methods. Package scope. classID == 0 -> silences, class > 0 -> regular words
  static VoxPattern read(VoxInputStream vis, int classID) throws IOException, VoxFormatException {
    VoxPattern entry = new VoxPattern();
    try {
      entry.ulSum = vis.readULong();
      entry.ulSquares = vis.readULong();
      entry.usCount = vis.readUns();
      entry.usTrainingCount = vis.readUns();
      entry.usTrainingSavedCount = vis.readUns();
    } catch (EOFException e) {
      throw new VoxFormatException("VoxPattern.read: Unexpected EOF");
    }
    int maxSize = (classID == 0) ? 1 : MAX_KERNELS;
    entry.vbDwells = VoxBytes.read(vis, maxSize, WORD_MODEL_DIMENSION);
    entry.vbTemplate = VoxBytes.read(vis, maxSize, TEMPLATE_DIMENSION);
    if (entry.vbDwells.getNumElements() != entry.vbTemplate.getNumElements())
      throw new VoxFormatException("VoxPattern.read: Model and Template mismatch");
    entry.vnWordName = VoxName.read(vis);
    if (entry.vnWordName.getNameLen() == 0)
      throw new VoxFormatException("VoxPattern.read: Empty Word Name");
    return entry;
  }

   // Instance Fields and their access method
  private long     ulSum;
  public  long     getSum() {return ulSum;}
  public  void     setSum(long sum) {ulSum = sum;}

  private long     ulSquares;
  public  long     getSquares() {return ulSquares;}
  public  void     setSquares(long squares) {ulSquares = squares;}

  private int      usCount;
  public  int      getCount() {return usCount;}
  public  void     setCount(int count) {usCount = count;}

  private int      usTrainingCount;
  public  int      getTrainingCount() {return usTrainingCount;}
  public  void     setTrainingCount(int count) {usTrainingCount = count;}

  private int      usTrainingSavedCount;
  public  int      getTrainingSavedCount() {return usTrainingSavedCount;}
  public  void     setTrainingSavedCount(int count) {usTrainingSavedCount = count;}

  private VoxBytes vbDwells;
  public  VoxBytes getDwells() {return vbDwells;}
  public  void     setDwells(VoxBytes dwells) throws VoxFormatException {
    if (dwells.getNumElements() == 0) throw new VoxFormatException("VoxPattern.setDwells: Empty dwells");
    if (dwells.getDimension() != vbDwells.getDimension()) throw new VoxFormatException("VoxPattern.setDwells: Bad dimension");
    vbDwells = dwells;
  }

  private VoxBytes vbTemplate;
  public  VoxBytes getTemplate() {return vbTemplate;}
  public  void     setTemplate(VoxBytes template) throws VoxFormatException {
    if (template.getNumElements() == 0) throw new VoxFormatException("VoxPattern.setTemplate: Empty template");
    if (template.getDimension() != vbTemplate.getDimension()) throw new VoxFormatException("VoxPattern.setTemplate: Bad dimension");
    vbTemplate = template;
  }

  private VoxName  vnWordName;
  public  byte[]   getWordBytes() {return vnWordName.getBytes();}
  public  String   getWordName() {return vnWordName.toString();}
  public  void     setWordName(String s) throws VoxFormatException {
    if (s.length() == 0) throw new VoxFormatException("VoxPattern.setWordName: Empty Word Name");
    vnWordName.setName(s);
  }

  public  int      getNumberOfKernels() throws VoxFormatException {
    int n = vbDwells.getNumElements();
    if (n != vbTemplate.getNumElements()) throw new VoxFormatException("VoxPattern.getNumberOfKernels: Model and Template mismatch");
    return n;
  }

   // Constructors
  private VoxPattern() {} // For use in read()

  public VoxPattern(int nKernels, String wordName) {
    if (nKernels <= 0 || nKernels > MAX_KERNELS) throw new IllegalArgumentException("VoxPattern: Number of Kernels must be in [1, " + MAX_KERNELS + "] range");
    if (wordName.length() == 0) throw new IllegalArgumentException("VoxPattern: Empty Word Name");
    vbDwells = new VoxBytes(nKernels, WORD_MODEL_DIMENSION);
    vbTemplate = new VoxBytes(nKernels, TEMPLATE_DIMENSION);
    vnWordName = new VoxName(0, wordName);
  }

   // Package scope
  long bodySize() {return 2*4 + 3*2 + vbDwells.bodySize() + vbTemplate.bodySize() + vnWordName.bodySize();}

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
     // All reasonable checks
    if (vbDwells.getNumElements() != vbTemplate.getNumElements()) throw new VoxFormatException("VoxPattern.write: Model and Template mismatch");
    if ((vbDwells.getBytes())[0] == 0) throw new VoxFormatException("VoxPattern.write: Uninitialized dwells"); // Must be minDwell > 0
    if (vnWordName.getNameLen() == 0) throw new VoxFormatException("VoxPattern.write: Empty Word Name");
    vos.writeULong(ulSum);
    vos.writeULong(ulSquares);
    vos.writeUns(usCount);
    vos.writeUns(usTrainingCount);
    vos.writeUns(usTrainingSavedCount);
    vbDwells.write(vos);
    vbTemplate.write(vos);
    vnWordName.write(vos);
  }

  public String toString () {
     // Word, counts, training mean
    StringBuffer buffer = StringFormatter.appendTo(14, vnWordName.toString(), ";");
    buffer = StringFormatter.appendTo(buffer, "  #K: ", -2, String.valueOf(vbDwells.getNumElements()), ";");
    buffer = StringFormatter.appendTo(buffer, "  WC: ", -4, String.valueOf(usCount), ";");
    buffer = StringFormatter.appendTo(buffer, "  TC: ", -4, String.valueOf(usTrainingCount), ";");
    buffer = StringFormatter.appendTo(buffer, "  M: ", -4, String.valueOf((usCount > 0) ? ulSum/usCount : 0), "\n");
     // Dwells
    buffer.append("    ");
    byte[] dwells = vbDwells.getBytes();
    for (int k = 0; k < dwells.length; k += WORD_MODEL_DIMENSION) {
      buffer.append(dwells[k]);
      buffer.append("/" + dwells[k+1] + " ");
    }
     // Templates
    byte[] t = vbTemplate.getBytes();
    for (int k = 0; k < t.length; k++)
      buffer = StringFormatter.appendTo(buffer, (k % TEMPLATE_DIMENSION == 0) ? "\n      " : " ", -3, String.valueOf(0xff & (int)t[k]));
    buffer.append("\n\n");
    return buffer.toString();
  }
}
