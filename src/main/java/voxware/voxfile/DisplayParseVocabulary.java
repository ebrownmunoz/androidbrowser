// DisplayParseVocabulary -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class DisplayParseVocabulary extends ParseTable {

   // Constructor for VoxBlock.read()
  DisplayParseVocabulary(VoxFile voxFile) {
	  super(voxFile); 
	  blockID = DISPLAY_PARSE_VOCAB;}

   // Constructor for Java Convert writers. Should check arguments consistency.
  public DisplayParseVocabulary(VoxFile voxFile, int vocabularyID) {
    super(voxFile, vocabularyID, null, null);
    blockID = DISPLAY_PARSE_VOCAB;
  }

  public DisplayParseVocabulary(VoxFile voxFile, int vocabularyID, long[] offsets, int[] data) {
    super(voxFile, vocabularyID, offsets, data);
    blockID = DISPLAY_PARSE_VOCAB;
  }

  public String toString() {
    return super.toString() + "DisplayParseVocabulary: State Count = " + ulaOffsets.length + "\n\n";
  }
}
