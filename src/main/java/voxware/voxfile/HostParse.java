// HostParse -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class HostParse extends ParseTable {

   // Constructor for VoxBlock.read()
  HostParse(VoxFile voxFile) {
	  super(voxFile);
    blockID = HOST_PARSE;
  }

   // Constructors for Java Convert writers. Should check arguments consistency.
  public HostParse(VoxFile voxFile, int grammarID) {
    super(voxFile, grammarID, null, null);
    blockID = HOST_PARSE;
  }

  public HostParse(VoxFile voxFile, int grammarID, long[] offsets, int[] data) {
    super(voxFile, grammarID, offsets, data);
    blockID = HOST_PARSE;
  }

  public String toString() {
    return super.toString() + "HostParse: State Count = " + ulaOffsets.length + "\n\n";
  }
}
