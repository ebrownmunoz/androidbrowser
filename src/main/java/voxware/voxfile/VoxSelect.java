// Word model selection service

package voxware.voxfile;

import java.io.*;
import java.util.*;

import voxware.util.PCFG;

public class VoxSelect {

  /**
   * Method selects from srcVoiFile all word models with wordNames coming as "keys" from VoxProperties parameter,
   * and put them into dstVoiFile with wordNames as corresponding "values". Parameter ignoreCase must be the same,
   * which was used to construct VoxProperties object "names". When ignoreCase == true, search is case
   * insensitive. When clean == true, removes all traces of training, including TrainScheme from dstVoiceFile header 
   * and models. Returns list of word names for which word models were not found or null if all words are covered.
   */
  public static String[] selectWords(VoxProperties names, boolean ignoreCase, InputStream srcVoiFile, OutputStream dstVoiFile, boolean clean)
                                                                                                       throws IOException, VoxFormatException {
    int    i;
    String srcName, dstName;

    if (names.size() == 0) return null;
    // Source VoiceFile
    VoxFile vf = new VoxFile();
    vf.read(srcVoiFile);
    if (!vf.isVoiceFile()) throw new VoxFormatException("VoxSelect: srcVoiFile is not a Voice File");
    UserPatterns up = vf.getUserPatterns();
    // To compute some header items
    long wildcardSum = 0;
    long wildcardSumSquares = 0;
    int  wildcardCount = 0;
    // To construct return array
    Properties found = new Properties();
    // Do patterns
    VoxPattern[] vpa = up.getPatterns();
    for (i = 0; i < vpa.length; i++) {
      srcName = vpa[i].getWordName();
      dstName = names.getProperty(ignoreCase ? srcName.toUpperCase() : srcName);
      if (dstName == null) {
        // Not among needed. Get rid of this pattern
        vpa[i] = null;
      } else {
        // Put in possibly new name
        vpa[i].setWordName(dstName);
        if (clean) {
          vpa[i].setSum(0);
          vpa[i].setSquares(0);
          vpa[i].setCount(0);
          vpa[i].setTrainingCount(0);
          vpa[i].setTrainingSavedCount(0);
        } else {
          wildcardSum += vpa[i].getSum();
          wildcardSumSquares += vpa[i].getSquares();
          wildcardCount += vpa[i].getCount();
        }
        // Put it among those found
        found.setProperty(dstName, "");
      }
    }    
    if (up.getNumberPatterns() > 0) {
      up.setWildcardSum(wildcardSum);
      up.setWildcardSumSquares(wildcardSumSquares);
      up.setWildcardCount(wildcardCount);
      if (clean) up.setTrainScheme(UserPatterns.TRAIN_SCHEME_INCRM);
      // Write it
      vf.write(dstVoiFile);
    }
    // Construct missing words array
    i = names.size() - found.size();
    if (i == 0) return null;
    String[] missingWords = new String[i];
    // Iterate over all dstNames
    Enumeration e = names.elements();
    i = 0;
    while (e.hasMoreElements()) {
      dstName = (String)e.nextElement();
      if (!found.containsKey(dstName))
        missingWords[i++] = dstName;
    }
    if (i < missingWords.length) {
      // Multiple templates for some words
      if (i == 0) return null;
      String[] shorter  = new String[i];
      System.arraycopy(missingWords, 0, shorter, 0, i);
      missingWords = shorter;
    }
    return missingWords;
  }


  // Overloading
  /** Selection without renaming */
  public static String[] selectWords(String[] words, boolean ignoreCase, InputStream src, OutputStream dst, boolean clean) throws IOException, VoxFormatException {
    return selectWords(new VoxProperties(words, ignoreCase), ignoreCase, src, dst, clean);
  }

  /** Selection without renaming from collection rather than String[] */
  public static String[] selectWords(Collection words, boolean ignoreCase, InputStream src, OutputStream dst, boolean clean) throws IOException, VoxFormatException {
    return selectWords(new VoxProperties(words, ignoreCase), ignoreCase, src, dst, clean);
  }

  /**
   * Method selects word models from "src" Voice File according to list of words coming from a Rec File or
   * Properties File. The latter allows to rename selected words. Selected models are saved in "dst" Voice File.
   * When clean == true, removes all traces of training, including TrainScheme from "dst" header and models.
   * If word has multiple templates all are included in "dst". Each line of Properties File has format:
   * srcWordName[=dstWordName], where srcWordName is used for (optionally case insensitive) search of a word model
   * in the "src", and optional dstWordName becomes the word name of the model in the "dst". For encoding rules for
   * a Properties File see Java documentation on Properties class. If "dst" == null, the backed up "src" will be
   * the destination. Returns list of word names for which word models were not found or null if all words are covered.
   */

  public static String[] selectWords(String recOrProp, boolean ignoreCase, String src, String dst, boolean clean) throws IOException, VoxFormatException {
    if (dst == null) {
      String tmp = src + "~";
      (new File(src)).renameTo(new File(tmp));
      dst = src;
      src = tmp;
    }
    // If we are given RecFile populate word lists from it
    try {
      String[] saWordNames = VoxReport.allWords(recOrProp); // throws VoxFormatException or IOException if it is not RecFile
      return selectWords(saWordNames, ignoreCase, new FileInputStream(src), new FileOutputStream(dst), clean);
    } catch (VoxFormatException vfe) {
    } catch (IOException ie) {
    }

    // Try Properties File
    return selectWords(new VoxProperties(new FileInputStream(recOrProp), ignoreCase), ignoreCase, new FileInputStream(src), new FileOutputStream(dst), clean);
  }

  /**
   * Method selects those word models from "src" Voice File, which have training count == 0.
   * Selected models are saved in "dst" Voice File. Sets TrainScheme to TRAIN_SCHEME_INCRM.
   * Returns the number of untrained words.
   */

  public static int selectUntrainedWords(InputStream srcVoiFile, OutputStream dstVoiFile) throws IOException, VoxFormatException {
     // Source VoiceFile
    VoxFile vf = new VoxFile();
    vf.read(srcVoiFile);
    if (!vf.isVoiceFile()) throw new VoxFormatException("VoxSelect.selectUntrainedWords: Source File is not a Voice File");
    UserPatterns up = vf.getUserPatterns();
     // Do patterns
    VoxPattern[] vpa = up.getPatterns();
    for (int i = 0; i < vpa.length; i++) {
      if (vpa[i].getCount() > 0) {
         // Not needed. Get rid of this pattern
        vpa[i] = null;
      } else {
         // Just in case these items are not already 0
        vpa[i].setSum(0);
        vpa[i].setSquares(0);
        vpa[i].setTrainingCount(0);
        vpa[i].setTrainingSavedCount(0);
      }        
    }
    if (up.getNumberPatterns() > 0) {
      up.setWildcardSum(0);
      up.setWildcardSumSquares(0);
      up.setWildcardCount(0);
      up.setTrainScheme(UserPatterns.TRAIN_SCHEME_INCRM);
      // Write it
      vf.write(dstVoiFile);
    }
    return up.getNumberPatterns();
  }
                                              
  /**
   * Method selects those word models from "src" Voice File, which have training count > 0.
   * Selected models are saved in "dst" Voice File. Preserves training statistics and scheme.
   * Returns the number of trained words.
   */

  public static int selectTrainedWords(InputStream srcVoiFile, OutputStream dstVoiFile) throws IOException, VoxFormatException {
     // Source VoiceFile
    VoxFile vf = new VoxFile();
    vf.read(srcVoiFile);
    if (!vf.isVoiceFile()) throw new VoxFormatException("VoxSelect.selectUntrainedWords: Source File is not a Voice File");
    UserPatterns up = vf.getUserPatterns();
     // To compute some header items
    long wildcardSum = 0;
    long wildcardSumSquares = 0;
    int  wildcardCount = 0;
     // Do patterns
    VoxPattern[] vpa = up.getPatterns();
    for (int i = 0; i < vpa.length; i++) {
      if (vpa[i].getCount() == 0) {
         // Not needed. Get rid of this pattern
        vpa[i] = null;
      } else {
        wildcardSum += vpa[i].getSum();
        wildcardSumSquares += vpa[i].getSquares();
        wildcardCount += vpa[i].getCount();
      }        
    }
    if (up.getNumberPatterns() > 0) {
      up.setWildcardSum(wildcardSum);
      up.setWildcardSumSquares(wildcardSumSquares);
      up.setWildcardCount(wildcardCount);
      // Write it
      vf.write(dstVoiFile);
    }
    return up.getNumberPatterns();
  }
                                              
  // Main
  public static void main(String argv[]) {
    try {
      PCFG.Parameter[] parameters = {
         //                 Name                   Type                   Description                                     Option        Default
        new PCFG.Parameter("InputVoiceFile",       PCFG.STRP,             "Input Voice File Name (REQUIRED!)",            "-i"),
        new PCFG.Parameter("OutputVoiceFile",      PCFG.STRP,             "Output Voice File Name",                       "-o"),
        new PCFG.Parameter("RecFile",              PCFG.STRP,             "Recognizer File Name",                         "-r"),
        new PCFG.Parameter("PropertiesFile",       PCFG.STRP,             "Properties File Name(SrcWord[=DstWord])",      "-p"),
        new PCFG.Parameter("Help",                 PCFG.HELPP,            "Display the help screen",                      "-h"),
        new PCFG.Parameter("CaseInsensitive",      PCFG.TRUEP,            "Case insensitive search of words",             "-c"),
        new PCFG.Parameter("TrainedWords",         PCFG.TRUEP,            "Trained words only",                           "-t"),
        new PCFG.Parameter("UntrainedWords",       PCFG.TRUEP,            "Untrained words only",                         "-u"),
        new PCFG.Parameter("ZeroTrainingData",     PCFG.TRUEP,            "Zero training statistics",                     "-z")
      };
      PCFG pcfg = new PCFG(parameters);
      PCFG.Parameter help = pcfg.helpParameter(argv);
      if (argv.length == 0 || help != null) usage(pcfg); // Inform & Exit
       // Get the parameters from the command line
      pcfg.scanCommandLine(argv);
      if (pcfg.numUnknownArgs() > 0) {
        System.out.println("VoxSelect: ERROR - unidentifiable argument on the command line: " + pcfg.unknownArgsToString());
        usage(pcfg);
      }
       // Must have Input File Name
      if (!pcfg.configured("InputVoiceFile")) {
        System.out.println("VoxSelect: ERROR - Input Voice File Name is not specified");
        usage(pcfg);
      }
      String inpFN = pcfg.parameter("InputVoiceFile").stringValue();
      String outFN = pcfg.configured("OutputVoiceFile") ? pcfg.parameter("OutputVoiceFile").stringValue() : null;
      String recFN = pcfg.configured("RecFile") ? pcfg.parameter("RecFile").stringValue() : null;
      String proFN = pcfg.configured("PropertiesFile") ? pcfg.parameter("PropertiesFile").stringValue() : null;
      boolean iCase = pcfg.configured("CaseInsensitive");
      boolean tnly = pcfg.configured("TrainedWords");
      boolean unly = pcfg.configured("UntrainedWords");
      boolean zero = pcfg.configured("ZeroTrainingData");
       // Minimum for trying
      if (!tnly && !unly && recFN == null && proFN == null) {
        System.out.println("VoxSelect: ERROR - Recognizer File Name, or Properties File Name, or -t, or -u option REQUIRED!");
        usage(pcfg);
      }
      if (tnly && unly) {
        System.out.println("VoxSelect: ERROR - Only one of -t, or -u options can be specified");
        usage(pcfg);
      }
       // Backup inpFN, if outFN is not provided
      if (outFN == null) {
        String tmp = inpFN + "~";
        (new File(inpFN)).renameTo(new File(tmp));
        outFN = inpFN;
        inpFN = tmp;
      }
      if (unly) {
        int n = selectUntrainedWords(new FileInputStream(inpFN), new FileOutputStream(outFN));
        System.out.println("VoxSelect: Found " + n + " untrained word models");
      } else if (tnly) {
        int n = selectTrainedWords(new FileInputStream(inpFN), new FileOutputStream(outFN));
        System.out.println("VoxSelect: Found " + n + " trained word models");
      } else {
        String[] list = null;
        if (recFN != null) {
          String[] saWordNames = VoxReport.allWords(recFN);
          list = selectWords(saWordNames, iCase, new FileInputStream(inpFN), new FileOutputStream(outFN), zero);
        } else {
          list = selectWords(new VoxProperties(new FileInputStream(proFN), iCase), iCase, new FileInputStream(inpFN), new FileOutputStream(outFN), zero);
        }
        if (list == null) {
          System.out.println("VoxSelect: All words have been found");
        } else {
          System.out.println("VoxSelect: Missing " + list.length + " words:");
          for (int i = 0; i < list.length; i++)
            System.out.println(list[i]);
        }
      }
    } catch (Throwable t) {
      System.out.println("VoxSelect: caught Throwable " + t.toString());
      t.printStackTrace();
    }
  }

  private static void usage(PCFG pcfg) {
    System.out.println("");
    System.out.println("Usage: java voxware.voxfile.VoxSelect [options]");
    System.out.println("");
    System.out.println(pcfg.optionsToString("<option>", "<default value>", "<description>", false, "  ", "\n  "));
    System.exit(1);
  }
}
