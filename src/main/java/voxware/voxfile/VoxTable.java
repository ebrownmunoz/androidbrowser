// VoxTable -> VoxBlock

package voxware.voxfile;

import java.io.*;

class VoxTable extends VoxBlock {   // Instance Fields. Package scope
  long[] ulaOffsets;
  int[]  usaData;

  // Accessors
  public int getStateCount() {
    return ulaOffsets.length;
  }

  public long[] getStateOffsets() {
    return ulaOffsets;
  }

  public void setStateOffsets(long[] stateOffsets) {
    ulaOffsets = stateOffsets;
  }

  public int[] getStateData() {
    return usaData;
  }

  public void setStateData(int[] stateData) {
    usaData = stateData;
  }

   // Constructors. Subclasses must setup blockID
  VoxTable(VoxFile voxFile) {
	  super(voxFile);
  }

  VoxTable(VoxFile voxFile, long[] offsets, int[] data) {
    super(voxFile);
    ulaOffsets = offsets;
    usaData = data;
  }

   // Instance methods
  long bodySize() {
    return 2 + 4 + 4*ulaOffsets.length + 2*usaData.length;
  }

   // blockID must have been read !!!
  VoxTable read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      int  i, len, usCount;
      long ulOffsetToData;

      readBlockHeader(vis);
      usCount = vis.readUns();
      if (usCount == 0) throw new VoxFormatException("VoxTable.read: Elemements Count == 0");
      ulOffsetToData = vis.readULong();
      if (ulOffsetToData != 6 + usCount*4) throw new VoxFormatException("VoxTable.read: Bad Offset to Data");
      len = (int)(blockSize - ulOffsetToData);
      if (len <= 0 || len % 2 != 0) throw new VoxFormatException("VoxTable.read: Bad Data Size");
      len /= 2;
      ulaOffsets = new long[usCount];
      for (i = 0; i < usCount; i++)
        ulaOffsets[i] = vis.readULong();
      usaData = new int[len];
      for (i = 0; i < len; i++)
        usaData[i] = (int)vis.readUns();
    } catch (EOFException e) {
      throw new VoxFormatException("VoxTable.read: Unexpected EOF");
    }
    return this;
  }

   // Mostly for Java Convert Writers. Test copying too
  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    int  i, usCount = ulaOffsets.length;
    long ulOffsetToData = 6 + usCount*4;

    writeBlockHeader(vos); // Computes this.bodySize(), updating ubNumberParameters, usNumberPatterns
    vos.writeUns(usCount);
    vos.writeULong(ulOffsetToData);
    for (i = 0; i < usCount; i++)
      vos.writeULong(ulaOffsets[i]);
    for (i = 0; i < usaData.length; i++)
      vos.writeUns(usaData[i]);
  }
}
