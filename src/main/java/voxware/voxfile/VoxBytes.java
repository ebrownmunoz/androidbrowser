// VoxBytes: 2-dimensional byte array as 1-dimensional one for templates and dwells.

package voxware.voxfile;

import java.io.*;

public class VoxBytes {

   // Class Methods. Package scope
  static VoxBytes read(VoxInputStream vis, int maxSize, int dimension) throws IOException, VoxFormatException {
    try {
      int n = vis.readUnsignedByte();
      if (n > maxSize) throw new VoxFormatException("VoxBytes.read: numElements > maxSize");
      if (n == 0) throw new VoxFormatException("VoxBytes.read: numElements == 0");
      VoxBytes vb = new VoxBytes(n, dimension);
      vis.readFully(vb.baBytes);
      return vb;
    } catch (EOFException e) {
      throw new VoxFormatException("VoxBytes.read: Unexpected EOF");
    }
  }

   // Instance Fields and their access method
  private int numElements;
  public int getNumElements() { return numElements; }
  private int elDimension;
  public int getDimension() { return elDimension; }
  private byte[] baBytes;
  public byte[] getBytes() { return baBytes; }

   // Constructors
  public VoxBytes(int num, int dimension) {
    numElements = num;
    elDimension = dimension;
    baBytes = new byte[numElements * elDimension];
  }

   // Package scope
  long bodySize() {return 1 + baBytes.length;}

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    vos.writeByte(numElements);
    vos.write(baBytes);
  }
}
