// VoxParameter - component of UserPaternes - describes DSP setting the block was created with

package voxware.voxfile;

import java.io.*;

public class VoxParameter {
   // Constants

   // Currenly used enumerations from viseparm.h
  public static final int VERSION          = 12;
  public static final int JINSCALE         = 27;
  public static final int NOISEBITMASK     = 28;
  public static final int FRAMESHIFT       = 30;
  public static final int JINNUMCOMPONENTS = 31;
  public static final int FRAMEWIDTH       = 32;
  public static final int SAMPLERATE       = 33;
  public static final int SIGBITS          = 34;
  public static final int AUDIOLEVEL       = 35;
   // The only one which makes sense to keep
  public static final int FEVERSION        = 38;

   // Class Methods. Package scope
  static VoxParameter read(VoxInputStream vis) throws IOException, VoxFormatException {
    VoxParameter vp = new VoxParameter();
    try {
      vp.ubNumber = vis.readByte();
      vp.usValue = vis.readUns();
      vp.ubControl = vis.readByte();
    } catch (EOFException e) {
      throw new VoxFormatException("VoxParameter.read: Unexpected EOF");
    }
    return vp;
  }

  static long bodySize() {return 4;} // size of V_Byte + V_Uns + V_Byte

   // Instance Fields and their access methods

  private byte ubNumber;
  public  int  getNumber() {return 0xff & (int)ubNumber;} // Unsigned byte returned

  private int  usValue;
  public  int  getValue() {return usValue;}
  public  void setValue(int v) {usValue = v;}

  private byte ubControl;
  public int getControl() {return 0xff & (int)ubControl;} // Unsigned byte returned

   // Constructors
  public VoxParameter() {
  }

  public VoxParameter(int num, int val, int con) {
    ubNumber = (byte)num;
    usValue = val;
    ubControl = (byte)con;
  }

  public VoxParameter(int feVersion) {this(FEVERSION, feVersion, 1);}

   // Instance methods

  void write(VoxOutputStream vos) throws IOException {
    vos.writeByte(ubNumber);
    vos.writeUns(usValue);
    vos.writeByte(ubControl);
  }

  public String toString () {
    StringBuffer buffer = StringFormatter.appendTo(-4, String.valueOf(getNumber()));
    buffer = StringFormatter.appendTo(buffer, -8, String.valueOf(getValue()));
    buffer = StringFormatter.appendTo(buffer, -8, String.valueOf(getControl()), "\n");
    return buffer.toString();
  }
}
