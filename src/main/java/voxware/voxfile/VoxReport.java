// Missing or available words reporting service

package voxware.voxfile;

import java.io.*;
import java.util.*;

public class VoxReport {

  public static boolean isWellFormed(InputStream is) {
    VoxFile vf = new VoxFile();
    try {
      vf.read(is);
      return true;
    } catch (IOException ie) {
      return false;
    } catch (VoxFormatException vfe) {
      return false;
    }
  }

  public static boolean isVoiceFile(InputStream is) {
    VoxFile vf = new VoxFile();
    try {
      vf.read(is);
      return vf.isVoiceFile();
    } catch (IOException ie) {
      return false;
    } catch (VoxFormatException vfe) {
      return false;
    }
  }

  public static boolean isRecFile(VoxFile vf) {
    return vf.getVocabularyTranslation() != null;
  }

  public static boolean isRecFile(InputStream is) {
    VoxFile vf = new VoxFile();
    try {
      vf.read(is);
      return vf.getVocabularyTranslation() != null;
    } catch (IOException ie) {
      return false;
    } catch (VoxFormatException vfe) {
      return false;
    }
  }

  /**
   * If the only argument is a recFile, returns list of unique word names in the file. To get multiple
   * templating info use VoxFile on this recFile. If the only argument is a voiceFile, returns list
   * of all word names in the file. If both are given, returns list of word names in the recFile not
   * found in the voiFile. If all words are covered, returns null.
   */
  public static String[] missingWords(VoxFile recOrVoiFile, VoxFile voiFile) throws VoxFormatException {
    int          i, n, uniqWordCount, index;
    VoxBlock     vb;
    UserPatterns up;
    VoxPattern[] vpa;
    String[]     allWords;

    // Dissect first file
    if (recOrVoiFile.isVoiceFile() && voiFile != null) throw new VoxFormatException("VoxReport: Expected RecFile as first argument");
    if (recOrVoiFile.isVoiceFile()) {
      // Report all words available in this file
      up = recOrVoiFile.getUserPatterns();
      vpa = up.getPatterns();
      allWords = new String[vpa.length];
      for (i = 0; i < vpa.length; i++)
        allWords[i] = vpa[i].getWordName();
      return allWords;
    }

    // First file is RecFile. Dissect it
    VocabularyTranslation vt = recOrVoiFile.getVocabularyTranslation();
    if (vt == null) throw new VoxFormatException("VoxReport: Malformed RecFile lacks VocabularyTranslation block");
    VocabularyEntry[] vea = vt.getWords();
    allWords = new String[vea.length];
    for (i = 0, uniqWordCount = 0; i < vea.length; i++)
      if (vea[i].getMultipleTemplateID() == i + 1)
        allWords[uniqWordCount++] = recOrVoiFile.getText(vea[i].getWordName()); // This is first appearance of the word
    vt = null;
    vea = null;

    // Trim array
    if (uniqWordCount < allWords.length) {
      String[] shorter = new String[uniqWordCount];
      System.arraycopy(allWords, 0, shorter, 0, uniqWordCount);
      allWords = shorter;
    }

    if (voiFile == null) return allWords;

    // VoiceFile turn
    if (!voiFile.isVoiceFile()) throw new VoxFormatException("VoxReport: Expected VoiceFile as second argument");
    up = voiFile.getUserPatterns();
    vpa = up.getPatterns();
    if (vpa.length == 0) return allWords; // May have empty voice file
    Hashtable htPats = new Hashtable(2 * vpa.length);
    for (i = 0; i < vpa.length; i++)
      htPats.put(vpa[i].getWordName(), vpa[i]);
    // Count words from allWords not present in table
    for (i = 0, n = 0; i < uniqWordCount; i++)
      if (htPats.containsKey(allWords[i])) allWords[i] = null;
      else                                 n++;
    
    if (n == 0) return null; // All words are covered

    // Allocate and fill in list
    String[] list = new String[n];
    for (i = 0, n = 0; i < uniqWordCount; i++)
      if (allWords[i] != null)
        list[n++] = allWords[i];

    return list;
  }

  // Overloading

  public static String[] allWords(VoxFile voxFile) throws IOException, VoxFormatException {
    return missingWords(voxFile, null);
  }

  public static String[] missingWords(InputStream recOrVoiStream, InputStream voiStream) throws IOException, VoxFormatException {
    VoxFile recOrVoiFile = null;
    if (recOrVoiStream != null) {
      recOrVoiFile = new VoxFile();
      recOrVoiFile.read(recOrVoiStream);
    }
    VoxFile voiFile = null;
    if (voiStream != null) {
      voiFile = new VoxFile();
      voiFile.read(voiStream);
    }
    return missingWords(recOrVoiFile, voiFile);
  }

  public static String[] allWords(InputStream recOrVoiStream) throws IOException, VoxFormatException {
    return missingWords(recOrVoiStream, null);
  }

  public static String[] missingWords(String recFileName, String voiFileName) throws IOException, VoxFormatException {
    InputStream recFile = new FileInputStream(recFileName);
    return missingWords(recFile, voiFileName == null ? null : new FileInputStream(voiFileName));
  }

  public static String[] allWords(String voxFileName) throws IOException, VoxFormatException {
    return missingWords(voxFileName, null);
  }

   // Test Main
  public static void main(String argv[]) {

    if (argv.length == 0) {
      System.out.println("Usage: java VoxReport [RecFileName] [VoiceFileName]");
      return;
    }

    String[] list = null;
    try {
      if (argv.length == 1) list = allWords(argv[0]);
      else                  list = missingWords(argv[0], argv[1]);

      if (list == null) {
        System.out.println("VoxReport: All words are present in Voice File");
      } else {
        System.out.println("VoxReport: There are " + list.length + " words " + (argv.length == 1 ? "in the File:" : "missing:"));
        for (int i = 0; i < list.length; i++)
          System.out.println(list[i]);
      }
    } catch (Throwable t) {
      System.out.println("VoxReport: caught Throwable " + t.toString());
      t.printStackTrace();
    }
  }
}
