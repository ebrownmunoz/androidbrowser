// VoxIdent -> VoxBlock

package voxware.voxfile;

import java.io.*;

public class VoxIdent extends VoxBlock {

   // Instance Fields and their access method
  private LenOff loApplicationName;
  public  LenOff getApplicationName() {return loApplicationName;}
  public  void   setApplicationName(LenOff lo) {loApplicationName = lo;}

  private int    usHostStartMsg;
  public  int    getHostStartMsg() {return usHostStartMsg;}
  public  void   setHostStartMsg(int i) {usHostStartMsg = i;}

  private int    usVoiceStartMsg;
  public  int    getVoiceStartMsg() {return usVoiceStartMsg;}
  public  void   setVoiceStartMsg(int i) {usVoiceStartMsg = i;}

  private int    ubVARControlCode;
  public  int    getVARControlCode() {return ubVARControlCode;}
  public  void   setVARControlCode(int i) {ubVARControlCode = i;}

  private LenOff loVersionName;
  public  LenOff getVersionName() {return loVersionName;}
  public  void   setVersionName(LenOff lo) {loVersionName = lo;}

  private long   ulVersionKey;
  public  long   getVersionKey() {return ulVersionKey;}
  public  void   setVersionKey(long l) {ulVersionKey = l;}

  private int    usDisplayStartMsg;
  public  int    getDisplayStartMsg() {return usDisplayStartMsg;}
  public  void   setDisplayStartMsg(int i) {usDisplayStartMsg = i;}

  private int    usFirstGrammar;
  public  int    getFirstGrammar() {return usFirstGrammar;}
  public  void   setFirstGrammar(int i) {usFirstGrammar = i;}

   // Constructor
  public VoxIdent(VoxFile voxFile) {
	super(voxFile);
    blockID = IDENT;
  }

   // Instance methods

   // Package scope
  long bodySize() {return 23;} // 5 + 2*2 + 1 + 5 + 4 + 2*2

   // Package scope. blockID must have been read !!!
  VoxIdent read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      readBlockHeader(vis);
      // Early check on the fixed length object
      if (blockSize != bodySize()) throw new VoxFormatException("VoxIdent.read: expected size = 23, but read " + blockSize);
      // Read object body
      loApplicationName = LenOff.read(vis);
      usHostStartMsg = vis.readUns();
      usVoiceStartMsg = vis.readUns();
      ubVARControlCode = vis.readUnsignedByte();
      loVersionName = LenOff.read(vis);
      ulVersionKey = vis.readULong();
      usDisplayStartMsg = vis.readUns();
      usFirstGrammar = vis.readUns();
    } catch (EOFException e) {
      throw new VoxFormatException("VoxIdent.read: Unexpected EOF");
    }
    return this;
  }

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    writeBlockHeader(vos);
    loApplicationName.write(vos);
    vos.writeUns(usHostStartMsg);
    vos.writeUns(usVoiceStartMsg);
    vos.writeByte(ubVARControlCode);
    loVersionName.write(vos);
    vos.writeULong(ulVersionKey);
    vos.writeUns(usDisplayStartMsg);
    vos.writeUns(usFirstGrammar);
  }
  
  public String getApplicationNameString() {
	  return getText(loApplicationName);
  }

  public String toString () {
    return super.toString() + "VoxIdent:\n" +
           "Application: " + getText(loApplicationName) + "; Version: " + getText(loVersionName) +
           "VAR Control Code: " + ubVARControlCode + "; Version Key: " + ulVersionKey + "; First Grammar: " + usFirstGrammar + "\n" +
           "Host Start: " + usHostStartMsg  + "; Voice Start: " + usVoiceStartMsg + "; Display Start: " + usDisplayStartMsg + "\n\n";
  }
}
