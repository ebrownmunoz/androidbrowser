// ControlParseVocabulary -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class ControlParseVocabulary extends ParseTable {

   // Constructor for VoxBlock.read()
  ControlParseVocabulary(VoxFile voxFile) {
	  super(voxFile);
	  blockID = CONTROL_PARSE_VOCAB;}

   // Constructor for Java Convert writers. Should check arguments consistency.
  public ControlParseVocabulary(VoxFile voxFile, int vocabularyID, long[] offsets, int[] data) {
    super(voxFile, vocabularyID, offsets, data);
    blockID = CONTROL_PARSE_VOCAB;
  }

  public String toString() {
    return super.toString() + "ControlParseVocabulary: State Count = " + ulaOffsets.length + "\n\n";
  }
}
