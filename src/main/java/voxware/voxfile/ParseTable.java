// ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class ParseTable extends VoxTable {

   // Constructors. Subclasses must setup blockID
  ParseTable(VoxFile voxFile) {
	  super(voxFile);
  }

  ParseTable(VoxFile voxFile, int classId, long[] offsets, int[] data) {
    super(voxFile, offsets, data);
    classID = classId;
  }
}
