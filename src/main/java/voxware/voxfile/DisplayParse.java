// DisplayParse -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class DisplayParse extends ParseTable {

   // Constructor for VoxBlock.read()
  DisplayParse(VoxFile voxFile) {
	  super(voxFile);
    blockID = DISPLAY_PARSE;
  }

   // Constructors for Java Convert writers. Should check arguments consistency.
  public DisplayParse(VoxFile voxFile, int grammarID) {
    super(voxFile, grammarID, null, null);
    blockID = DISPLAY_PARSE;
  }

  public DisplayParse(VoxFile voxFile, int grammarID, long[] offsets, int[] data) {
    super(voxFile, grammarID, offsets, data);
    blockID = DISPLAY_PARSE;
  }

  public String toString() {
    return super.toString() + "DisplayParse: State Count = " + ulaOffsets.length + "\n\n";
  }
}
