// Voice File concatenation service

package voxware.voxfile;

import java.util.*;
import java.io.*;

public class VoxCat {

  /**
   * destination will have all the models from the first, plus those models from the
   * second, which the first didn't have. If the first and the second have different trainScheme
   * the destination will have TRAIN_SCHEME_MIXED. If the first and the second have different gender
   * && force == true, the destination will have GENDER_UNISEX, otherwise exception is thrown,
   * as is always the case if feVersions in the first and the second are different.
   * Returns number of word models added to  destination Voice File.
   */

  public static int cat(InputStream first, InputStream second, OutputStream destination, boolean force) throws IOException, VoxFormatException {
    int i, j, n;

    VoxCat vc = new VoxCat(first);
    VoxFile vf1 = vc.getHost();
    VoxFile vf2 = new VoxFile();
    vf2.read(second);
    // Compatibility
    if (!vf2.isVoiceFile()) throw new VoxFormatException("VoxCat: Second argument must be Voice File");
    if (vf1.getFrontEndVersion() != vf2.getFrontEndVersion()) throw new VoxFormatException("VoxCat: Incompatible Front End Version in source Voice Files");
    if (vf1.getGender() != vf2.getGender() && !force) throw new VoxFormatException("VoxCat: Concatenation of Voice Files of different Gender must be forced");
    // Get UserPatterns from both
    UserPatterns up1 = vf1.getUserPatterns();
    UserPatterns up2 = vf2.getUserPatterns();
    // All modifications are applied to the first now
    if (up1.getGender() != up2.getGender()) up1.setGender(UserPatterns.GENDER_UNISEX);
    if (up1.getTrainScheme() != up2.getTrainScheme()) up1.setTrainScheme(UserPatterns.TRAIN_SCHEME_MIXED );
    VoxPattern[] vpa1 = up1.getPatterns();
    VoxPattern[] vpa2 = up2.getPatterns();
    n = vpa1.length;
    VoxPattern[] vpa = new VoxPattern[n + vpa2.length];
    // Fill it up
    System.arraycopy(vpa1, 0, vpa, 0, n);
    for (i = 0; i < vpa2.length; i++)
      if (!vc.containsWord(vpa2[i].getWordName()))
        vpa[n++] = vpa2[i];
    n -= vpa1.length;
    // Put patterns in place
    up1.setPatterns(vpa);
    up1.setWildcardSum(0);
    up1.setWildcardSumSquares(0);
    up1.setWildcardCount(0);
    // Write destination
    vf1.write(destination);
    
    return n;
  }

  // Overloading
  public static int cat(InputStream first, InputStream second, OutputStream destination) throws IOException, VoxFormatException {
    return cat(first, second, destination, false);
  }

  /** If the destination == null, the backed up first will be the destination. */
  public static int cat(String first, String second, String destination, boolean force) throws IOException, VoxFormatException {
    if (destination == null) {
      String tmp = first + "~";
      (new File(first)).renameTo(new File(tmp));
      destination = first;
      first = tmp;
    }
    return cat(new FileInputStream(first), new FileInputStream(second), new FileOutputStream(destination), force);
  }

  public static int cat(String first, String second, String destination) throws IOException, VoxFormatException {
    return cat(first, second, destination, false);
  }

  public static int cat(String first, String second, boolean force) throws IOException, VoxFormatException {
    return cat(first, second, null, force);
  }

  public static int cat(String first, String second) throws IOException, VoxFormatException {
    return cat(first, second, null, false);
  }

  // Instance Fields
  private VoxFile      hostVoxFile;         // VoxFile accumulating VoxPattern object from multiple sources
  private UserPatterns userPatterns;        // Non silence User Pattern block of hostVoxFile
  private Hashtable    htPats;              // Storage of accumulated, renamed VoxPattern objects
  
  /**
   * Default Constructor
   */
  public VoxCat() {
    htPats = new Hashtable(1024);
  }

  /**
   * Constructor with host Voice VoxFile as parameter
   */
  public VoxCat(VoxFile vf) {
    hostVoxFile = vf;
    initialize();
  }

  /**
   * Constructor with host Voice Stream as parameter
   */
  public VoxCat(InputStream is) {
    try {
      hostVoxFile = new VoxFile();
      hostVoxFile.read(is);
    } catch (IOException ioe) {
      throw new IllegalArgumentException(ioe.getMessage());
    } catch (VoxFormatException vfe) {
      throw new IllegalArgumentException(vfe.getMessage());
    }
    initialize();
  }

  protected void initialize() {
    if (!hostVoxFile.isVoiceFile()) throw new IllegalArgumentException("VoxCat: Given InputStream is not a Voice File");
    userPatterns = hostVoxFile.getUserPatterns();
    VoxPattern[] vpa = userPatterns.getPatterns();
    htPats = new Hashtable(2 * vpa.length);
    for (int i = 0; i < vpa.length; i++) htPats.put(vpa[i].getWordName(), vpa[i]);
  }

  /**
   * Returns host VoxFile if any, null otherwise
   */
  public VoxFile getHost() {
    return hostVoxFile;
  }

  /**
   * Returns true if containns word model for specified word, false otherwise
   */
  public boolean containsWord(String wordName) {
    return htPats.containsKey(wordName);
  }

  /**
   * Adds to the object first (in case of multiple template) model from Voice File is, if can find its wordName, using
   * case sensitive search, among keys of "names". The "names" must have been created with single parameter constructor!
   */
  public String[] cat(VoxFile vf, VoxProperties names, String suffix) throws VoxFormatException {
    int     i;
    String  wordName;

    if (names.size() == 0) return null;
    if (!vf.isVoiceFile()) throw new VoxFormatException("VoxCat.cat: Given InputStream is not a Voice File");
    UserPatterns up = vf.getUserPatterns();
    if (hostVoxFile == null) {
      // First addition defines Front End Version, Silences, and gender to some extent
      hostVoxFile = vf;
      userPatterns = up;
    } else {
      // Check compatibility
      if (vf.getFrontEndVersion() != hostVoxFile.getFrontEndVersion()) throw new VoxFormatException("VoxCat.cat: Incompatible Front End");
      if (up.getGender() != userPatterns.getGender()) userPatterns.setGender(UserPatterns.GENDER_UNISEX);
      if (up.getTrainScheme() != userPatterns.getTrainScheme()) userPatterns.setTrainScheme(UserPatterns.TRAIN_SCHEME_MIXED );
    }
    // Do patterns
    VoxPattern[] vpa = up.getPatterns();
    for (i = 0; i < vpa.length && names.size() > 0; i++) {
      wordName = vpa[i].getWordName();
      if (names.containsKey(wordName)) {
        if (suffix != null) vpa[i].setWordName(wordName + suffix);
        htPats.put(vpa[i].getWordName(), vpa[i]);
        names.remove(wordName);
      }
    }
    // Construct missing words array
    i = names.size();
    if (i == 0) return null;
    String[] missingWords = new String[i];
    // Collect all keys remaining
    Enumeration e = names.keys();
    i = 0;
    while (e.hasMoreElements())
      missingWords[i++] = (String)e.nextElement();
    return missingWords;
  }

  /**
   * Convenience method that consumes an InputStream rather than a VoxFile
   */
  public String[] cat(InputStream is, VoxProperties names, String suffix) throws IOException, VoxFormatException {
    if (names.size() == 0) return null;
    VoxFile vf = new VoxFile();
    vf.read(is);
    return cat(vf, names, suffix);
  }

  /**
   * Adds to the object first (in case of multiple template) model from Voice File vf, if can find its wordName, using
   * case sensitive search, among strings of "words" array.
   */
  public String[] cat(VoxFile vf, String[] words, String suffix) throws IOException, VoxFormatException {
    return cat(vf, new VoxProperties(words), suffix);
  }

  /**
   * Convenience method that consumes an InputStream rather than a VoxFile
   */
  public String[] cat(InputStream is, String[] words, String suffix) throws IOException, VoxFormatException {
    return cat(is, new VoxProperties(words), suffix);
  }

  /**
   * Adds to the object first (in case of multiple template) model from Voice File vfg, if can find its wordName, using
   * case sensitive search, among strings of "words" collection.
   */
  public String[] cat(VoxFile vf, Collection words, String suffix) throws IOException, VoxFormatException {
    return cat(vf, new VoxProperties(words), suffix);
  }

  /**
   * Convenience method that consumes an InputStream rather than a VoxFile
   */
  public String[] cat(InputStream is, Collection words, String suffix) throws IOException, VoxFormatException {
    return cat(is, new VoxProperties(words), suffix);
  }

  public void write(OutputStream os) throws IOException, VoxFormatException {
    if (hostVoxFile == null || htPats == null) return;
    Collection pats = htPats.values();
    if (pats.size() == 0) return;
    VoxPattern[] vpa = new VoxPattern[pats.size()];
    vpa = (VoxPattern[])pats.toArray(vpa);
    userPatterns.setPatterns(vpa);
    userPatterns.setWildcardSum(0);
    userPatterns.setWildcardSumSquares(0);
    userPatterns.setWildcardCount(0);
    hostVoxFile.write(os);
  }

  // Main
  public static void main(String argv[]) {
    String  destination = null;
    boolean force = false;

    if (argv.length < 2 || argv.length > 4) {
      System.out.println("Usage: java VoxCat FirstVoiceFileName SecondVoiceFileName [DestinationVoiceFileName] [force]");
      return;
    }
    if (argv.length == 3) {
      if (argv[2].equalsIgnoreCase("force"))
        force = true;
      else
        destination = argv[2];
    } else if (argv.length == 4) {
      if (!argv[3].equalsIgnoreCase("force")) {
        System.out.println("VoxCat: Don't understand last argunent: " + argv[3]);
        return;
      }
      destination = argv[2];
      force = true;
    }

    try {
      int n = cat(argv[0], argv[1], destination, force);
      if (n == 0) System.out.println("VoxCat: File " + argv[0] + " already had all the word models available in File " + argv[1]);
      else        System.out.println("VoxCat: File " + (destination == null ? argv[0] : destination) + " has now " + n + " more word models");
    } catch (Throwable t) {
      System.out.println("VoxCat: caught Throwable " + t.toString());
      t.printStackTrace();
    }
  }
/*
  public static void main(String argv[]) {
    String first   = "m900si.voi";
    String second  = "f900si.voi";
    String suffix1 = "__m";
    String suffix2 = "__f";
    String out     = "Qoo.voi";

    try {
      String[] words1 = VoxReport.allWords(first);
      String[] words2 = VoxReport.allWords(second);
      HashSet male = new HashSet(100);
      HashSet fema = new HashSet(100);
      for (int i = 0; i < 11; i++) {
        male.add(words1[i]);
        fema.add(words2[i]);
      }
      male.add("stranger");
      fema.add("unknown");
      VoxCat vc = new VoxCat();
      String[] list = vc.cat(new FileInputStream(first), male, suffix1);
      if (list != null)
        for (int i = 0; i < list.length; i++)
          System.out.println(list[i]);
      System.out.println("--------------------------");
      list = vc.cat(new FileInputStream(second), fema, suffix2);
      if (list != null)
        for (int i = 0; i < list.length; i++)
          System.out.println(list[i]);
      vc.write(new FileOutputStream(out));
    } catch (Throwable t) {
      System.out.println("VoxCat: caught Throwable " + t.toString());
      t.printStackTrace();
    }
  }
*/
}
