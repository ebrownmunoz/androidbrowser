// ResponseTemplate -> TemplateTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class ResponseTemplate extends TemplateTable {

   // Constructor for Java Convert writers. Should check arguments consistency.
  public ResponseTemplate(VoxFile voxFile) {
	  super(voxFile);
  blockID = RESPONSE_TEMPLATE;
  }

  public ResponseTemplate(VoxFile voxFile, long[] offsets, int[] data) {
    super(voxFile, offsets, data);
    blockID = RESPONSE_TEMPLATE;
  }

  public String toString() {
    return super.toString() + "ResponseTemplate: Template Count = " + ulaOffsets.length + "\n\n";
  }
}
