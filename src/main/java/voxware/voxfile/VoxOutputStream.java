// DataOutputStream able to write data with little-endian byte order

package voxware.voxfile;

import java.io.*;

public class VoxOutputStream extends DataOutputStream {

   // Constructor
  public VoxOutputStream(OutputStream os) {
    super(os);
  }

   // Instance Methods

   // Low 16 bits swapped and put into stream
  public void writeUns(int i) throws IOException {
    writeShort(0xff & (i>>8) | 0xff00 & (i<<8));
  }
   // 32 bits swapped and put into stream
  public void writeSLong(int i) throws IOException {
    writeInt(0xff & (i>>24) | 0xff00 & (i>>8) | 0xff0000 & (i<<8) | 0xff000000 & (i<<24));
  }
   // Low 32 bits swapped and put into stream
  public void writeULong(long l) throws IOException {
    int i = (int)l;
    writeInt(0xff & (i>>24) | 0xff00 & (i>>8) | 0xff0000 & (i<<8) | 0xff000000 & (i<<24));
  }
}
