package voxware.security.password;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.UnsupportedEncodingException;

public class Password {

  public static final String DefaultAlgorithm = "MD5";
  public static final int DefaultRadix = 36;

  protected String algorithm;
  protected int radix;
  protected String hashing;

  public static String hash(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    return hash(password, DefaultAlgorithm, DefaultRadix);
  }

  public static String hash(String password, String algorithm, int radix) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    MessageDigest digest = java.security.MessageDigest.getInstance(algorithm);
    byte[] bytes = password.getBytes("UTF8");
    digest.update(bytes, 0, bytes.length);
    return new BigInteger(digest.digest()).abs().toString(radix);
  }

  public Password(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    this(password, DefaultAlgorithm, DefaultRadix);
  }

  public Password(String password, String algorithm, int radix) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    this.algorithm = algorithm;
    this.radix = radix;
    hashing = hash(password, algorithm, radix);
  }

  public String getPassword() throws NoSuchAlgorithmException, UnsupportedEncodingException {
    return hashing;
  }

  public String setPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    hashing = hash(password, this.algorithm, this.radix);
    return hashing;
  }
}
