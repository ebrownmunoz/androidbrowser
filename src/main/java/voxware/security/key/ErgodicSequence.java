package voxware.security.key;

import java.util.Random;
import java.security.NoSuchAlgorithmException;

public class ErgodicSequence {

  private int length;

  private int modulus;
  private int random;
  private int multiplier;

  public static void main(String[] args) throws NoSuchAlgorithmException {
    // Usage: ErgodicSequence [ trials [ length ] ]
    int trials = args.length > 0 ? Integer.parseInt(args[0]) : 10;
    int length = args.length > 1 ? Integer.parseInt(args[1]) : voxware.security.key.Encryption.uidLength * 8;
    int failures = 0;
    for (int trial = 0; trial < trials; trial++) {
      long seed = voxware.security.key.Encryption.seed(); // Throws NoSuchAlgorithmException
      int[] sequence = new ErgodicSequence(length, seed).sequence();
      System.out.println("Sequence " + (trial + 1) + " of length " + length + " for seed " + Long.toHexString(seed) + ":");
      System.out.print(sequence[0]);
      for (int i = 1; i < length; i++) {
        System.out.print(", " + sequence[i]);
        for (int j = 0; j < i; j++) {
          if (sequence[j] == sequence[i]) {
            System.out.print(": NOT ERGODIC! (sequence[" + i + "] == " + sequence[i] + " == sequence[" + j + "]");
            failures++;
            i = length;
            break;
          }
        }
      }
      System.out.println("");
    }
    System.out.println("There were " + failures + " failures");
  }

  /*
  private double modulus;
  private double random;
  private double multiplier;

  ErgodicSequence(int length, long seed) {
    this.length = length;
    double exponent = StrictMath.ceil(StrictMath.log(length) / StrictMath.log(2.0) + 2);
    modulus = StrictMath.pow(2, exponent);
    Random generator = new Random(seed);
    random = StrictMath.floor(generator.nextDouble() * (modulus - 1));
    if (random % 2 == 0) random++;
    multiplier = 8 * (StrictMath.floor(generator.nextDouble() * (StrictMath.pow(2, exponent - 3) - 2)) + 1) - 3;
  }
  */

  ErgodicSequence(int length, long seed) {
    this.length = length;
    for (modulus = 4; modulus < length; modulus *= 2);
    modulus *= 4;
    Random generator = new Random(seed);
    random = generator.nextInt(modulus);
    if (random % 2 == 0) random = (random + 1) % modulus;
    multiplier = 8 * (generator.nextInt(modulus / 8) + (modulus / 16)) - 3;
  }

  int next() {
    random = (multiplier * random) % modulus;
    int index = random / 4;
    return (index < length) ? index : next();
  }

  int[] sequence() {
    int[] sequence = new int[length];
    for (int index = 0; index < length; index++) sequence[index] = next();
    return sequence;
  }
}
