package voxware.security.key;

import java.math.BigInteger;

public class Verifier {

  public static void main(String[] args) {
    if (args.length != 2) {
      System.out.println("Usage: voxware.security.key.Verifier uid key");
    } else {
      try {
        if (verify(args[0], args[1])) {
          System.out.println(args[1] + " is a valid key for uid " + args[0]);
        } else {
          System.out.println(args[1] + " is NOT a valid key for uid " + args[0]);
        }
      } catch (NumberFormatException e) {
        System.out.println(e.getMessage());
      }
    }
  }

  public static boolean verify(String uidString, String keyString) throws NumberFormatException {
    long uid = 0L;
    try {
      if (uidString.length() > voxware.security.key.Encryption.uidLength * 2) throw new NumberFormatException();
      uid = Long.parseLong(uidString, 16);
    } catch (NumberFormatException e) {
      throw new NumberFormatException(uidString + " is not a well-formed uid");
    }
    long key = 0L;
    try {
      if (keyString.length() > voxware.security.key.Encryption.keyLength * 2) throw new NumberFormatException();
      key = new BigInteger(keyString, 16).longValue();
    } catch (NumberFormatException e) {
      throw new NumberFormatException(keyString + " is not a well-formed key");
    }
    return verify(uid, key);
  }

  public static boolean verify(long uid, long key) {
    return (uid == voxware.security.key.Encryption.uid(key));
  }
}
