package voxware.security.key;

import java.security.NoSuchAlgorithmException;

public class Generator {

  public static void main(String[] args) throws NoSuchAlgorithmException {
    if (args.length != 1) {
      System.out.println("Usage: voxware.security.key.Generator <macaddress>");
    } else {
      String uidString = args[0];
      int dot = 0;
       // The following is in lieu of String.replaceAll(), which was unavailable in Java prior to 1.4
      while ((dot = uidString.indexOf('.', dot)) >= 0 || (dot = uidString.indexOf(':', dot)) >= 0)
        uidString = uidString.substring(0, dot) + uidString.substring(dot + 1);
      try {
        if (uidString.length() > voxware.security.key.Encryption.uidLength * 2) throw new NumberFormatException();
        long uid = Long.parseLong(uidString, 16);
        long key = voxware.security.key.Encryption.key(uid); // Throws NoSuchAlgorithmException
        String keyString = Long.toHexString(key);
        while (keyString.length() < voxware.security.key.Encryption.keyLength * 2) keyString = "0" + keyString;
        System.out.println("A valid key for uid " + Long.toHexString(uid) + " is " + keyString);
      } catch (NumberFormatException e) {
        System.out.println(uidString + " is not a well-formed uid");
      }
    }
  }
}
