package voxware.cas.framework;

import java.util.Map;

import voxware.cas.framework.exceptions.CommandException;


public interface CommandHandler {
  void execute(Command command, Map model) throws CommandException;
  Object createEvent(Command command, Map model) throws CommandException;
}
