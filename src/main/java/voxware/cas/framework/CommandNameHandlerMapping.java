package voxware.cas.framework;

import java.util.Map;
import java.util.HashMap;

public class CommandNameHandlerMapping {
  Map commandMap = null;

  public Map getCommandMap() {
    return commandMap;
  }

  public void setCommandMap(Map commandMap) {
    this.commandMap = commandMap;
  }

  public CommandHandler getHandler(Object cmd) {
    CommandHandler handler = null;

    String cmdName = cmd.getClass().getName();
    handler = (CommandHandler) commandMap.get(cmdName);
    return handler;
  }

  public CommandHandler getHandler(String cmdName) {
    CommandHandler handler = null;

    handler = (CommandHandler) commandMap.get(cmdName);
    return handler;
  }
}

  
