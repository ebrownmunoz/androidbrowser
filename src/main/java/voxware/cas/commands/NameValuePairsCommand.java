package voxware.cas.commands;

import voxware.cas.framework.Command;
import voxware.cas.framework.NameValuePair;
import voxware.cas.framework.exceptions.*;

import java.util.StringTokenizer;

public class NameValuePairsCommand implements Command {

  public String CommandPairName = "cmd";
  public String PairDelimiter   = "&";

  String command = null;
  NameValuePair[] nameValuePairs = null;
  int index = 0;

  public NameValuePairsCommand() {
  }

  public NameValuePairsCommand(String string) throws IllegalArgumentException {
    fromString(string);
  }

  public NameValuePairsCommand(String string, int size) throws IllegalArgumentException {
    fromString(string, size);
  }

  public String getCommand() {
    return command;
  }

  public void setCommand(String command) {
    this.command = command;
  }

  public NameValuePair[] getNameValuePairs() {
    return nameValuePairs;
  }

  public void setNameValuePairs(NameValuePair[] nameValuePairs) {
    this.nameValuePairs = nameValuePairs;
  }

  public void add(NameValuePair nameValuePair) {
    nameValuePairs[index++] = nameValuePair;
  }

  public String toString() {
    StringBuffer string = new StringBuffer();
    if (command != null) string.append(CommandPairName).append('=').append(command);
    if (nameValuePairs != null) {
      for (int i = 0; i < nameValuePairs.length; i++)
        string.append(PairDelimiter).append(nameValuePairs[i].name()).append('=').append(nameValuePairs[i].value());
    }
    return string.toString();
  }

  public void fromString(String string) throws IllegalArgumentException {
    fromString(string, -1);
  }

  protected void fromString(String string, int size) throws IllegalArgumentException {
    if (string != null && string.length() > 0) {
      StringTokenizer st = new StringTokenizer(string, PairDelimiter);
      index = 0;
       // Get the command
      if (st.hasMoreTokens()) {
        String token = st.nextToken();
        int indexOfEquals = token.indexOf('=');
        if (indexOfEquals < 0 || token.substring(0, indexOfEquals).equals(CommandPairName)) {
          command = token.substring(indexOfEquals + 1);
        } else {
          throw new IllegalArgumentException(string);
        }
         // Get the name/value pairs
        if (size < 0) size = st.countTokens();
        nameValuePairs = size > 0 ? new NameValuePair[size] : null;
        while (st.hasMoreTokens() && index < size) {
          token = st.nextToken();
          indexOfEquals = token.indexOf('=');
          if (indexOfEquals > 0) {
            nameValuePairs[index++] = new NameValuePair(token.substring(0, indexOfEquals), token.substring(indexOfEquals + 1));
          } else {
            throw new IllegalArgumentException(string);
          }
        }
      } else {
        command = null;
        nameValuePairs = null;
      }
    }
  }

}
