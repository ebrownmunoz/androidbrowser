/**
 * Connection.java - An implementation of URLConnection for obtaining
 * a byte[] from an embedded class
 *
 * Author: dvetter@voxware.com
 *
 * This implementation of HttpURLConnection is based on the March 29, 1999
 * version of gnu.gcj.protocol.http.Connection authored by Warren Levy
 * <warrenl@cygnus.com>.
 **/

package voxware.net.protocol.auto;

import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLConnection;
import java.net.ProtocolException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import voxware.util.ByteArrayWrapper;
import voxware.util.SysLog;

public class Connection extends URLConnection {

  private byte[] bytes;

  public Connection(URL url) { super(url); }

   // Overrides default method in URLConnection
  public long getLastModified() { return System.currentTimeMillis(); }

   // Overrides default method in URLConnection
  public String getHeaderField(String name) {
    if (name.equalsIgnoreCase("content-length") && bytes != null) return Integer.toString(bytes.length);
    return null;
  }

   // Implements abstract method in URLConnection
  public void connect() throws IOException {
    if (!connected) {
      try {
        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.URLCONNECTION_MASK))
          SysLog.println("voxware.net.protocol.auto.Connection.connect(): url.getHost() = \"" + url.getHost() + "\"");
        Class klass = Class.forName(url.getHost());
        Constructor ctor = klass.getConstructor(new Class[0]);
        bytes = ((ByteArrayWrapper) ctor.newInstance(null)).getBytes();
        connected = true;
      } catch (Exception exc) {
        if (SysLog.printLevel > 0 && SysLog.printMaskBitsSet(SysLog.URLCONNECTION_MASK))
          SysLog.println("voxware.net.protocol.auto.Connection.connect(): threw " + exc);
        throw (IOException) (new IOException().initCause(exc));
      }
    }
  }

   // Implements abstract method in URLConnection
  public void disconnect() {
    bytes = null;
    connected = false;
  }

   // Overrides default method in URLConnection
  public InputStream getInputStream() throws IOException {
    if (!connected) connect();
    if (!doInput) throw new ProtocolException("Can't open InputStream when doInput is false");
    return new ByteArrayInputStream(bytes);
  }

}
