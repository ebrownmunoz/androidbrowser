
package voxware.net.protocol.http;
import java.net.*;
import java.io.*;
import java.util.*;

public class VoxwareSocket extends java.net.Socket {
    public VoxwareSocket(InetAddress address, int port, int connectTimeout) throws IOException {
      super(address, port);


      setSoTimeout(connectTimeout);
    }
  }
