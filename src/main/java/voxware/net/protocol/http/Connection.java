/**
 * Connection.java - An implementation of HttpURLConnection
 *
 * Author: dvetter@voxware.com
 *
 * This implementation of HttpURLConnection is based on the March 29, 1999
 * version of gnu.gcj.protocol.http.Connection authored by Warren Levy
 * <warrenl@cygnus.com>. It adds connectTimeout and socketTimeout fields
 * and their associated access methods, as well as a private static
 * extension to java.net.Socket that supports connect timeouts
 **/

package voxware.net.protocol.http;

import java.net.*;
import java.io.*;
import java.util.Map;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;

public class Connection extends HttpURLConnection {

  protected Socket socket = null;
  private static Hashtable defRequestProperties = new Hashtable();
  private Hashtable requestProperties;
  private Hashtable hdrHash = new Hashtable();
  private Vector headers = null;
  private BufferedInputStream bufferedIn;
  private int socketTimeout = 0;
  private int connectTimeout = 0;

  private static int proxyPort = 80;
  private static boolean proxyInUse = false;
  private static String proxyHost = null;


  static {
     // Recognize some networking properties listed at
     // http://java.sun.com/j2se/1.4/docs/guide/net/properties.html.
    String port = null;
    proxyHost = System.getProperty("http.proxyHost");
    if (proxyHost != null) {
      proxyInUse = true;
      if ((port = System.getProperty("http.proxyPort")) != null) {
        try {
          proxyPort = Integer.parseInt(port);
        } catch (Throwable t) {}
      }
    }
  }

  public Connection(URL url) {
    super(url);
    requestProperties = (Hashtable) defRequestProperties.clone();
  }

  public int getConnectTimeout() {
    return connectTimeout;
  }

  public void setConnectTimeout(int connectTimeout) {
    this.connectTimeout = connectTimeout;
  }

  public int getSocketTimeout() throws SocketException {
    if (socket != null) socketTimeout = socket.getSoTimeout();
    return socketTimeout;
  }

  public void setSocketTimeout(int socketTimeout) throws SocketException {
    this.socketTimeout = socketTimeout;
    if (socket != null) socket.setSoTimeout(this.socketTimeout);
  }

   // Overrides method in URLConnection
  public static void setDefaultRequestProperty(String key, String value) {
    defRequestProperties.put(key, value);
  }

   // Overrides method in URLConnection
  public static String getDefaultRequestProperty(String key)
  {
    return (String) defRequestProperties.get(key);
  }

  // Override method in URLConnection
  public void setRequestProperty(String key, String value) {
    if (connected) throw new IllegalAccessError("Connection already established.");
    requestProperties.put(key, value);
  }

   // Overrides method in URLConnection
  public String getRequestProperty(String key) {
    if (connected) throw new IllegalAccessError("Connection already established.");
    return (String) requestProperties.get(key);
  }

   // Implementation of abstract method
  public void connect() throws IOException {
     // Ignore if already connected
    if (connected) return;

     // Get address and port number
    int port;
    InetAddress destAddr = null;
    if (proxyInUse) {
      destAddr = InetAddress.getByName(proxyHost);
      port = proxyPort;
    } else {
      destAddr = InetAddress.getByName(url.getHost());
      if ((port = url.getPort()) == -1) port = 80;
    }
     // Open socket and output stream
    socket = new VoxwareSocket(destAddr, port, connectTimeout);
    socket.setSoTimeout(socketTimeout);

    PrintWriter out = new PrintWriter(socket.getOutputStream());

     // Send request including any request properties that were set
    out.print(getRequestMethod() + " " + url.getFile() + " HTTP/1.1\r\n");
    out.print("Host: " + url.getHost() + ":" + port + "\r\n");
    Enumeration reqKeys = requestProperties.keys();
    Enumeration reqVals = requestProperties.elements();
    while (reqKeys.hasMoreElements())
      out.print(reqKeys.nextElement() + ": " + reqVals.nextElement() + "\r\n");
    out.print("\r\n");
    out.flush();
    connected = true;
  }

   // Implementation of abstract method
  public void disconnect() {
    if (socket != null) {
      try {
        socket.close();
      } catch (IOException x) {}
      socket = null;
    }
    connected = false;
    headers = null;
  }

  public boolean usingProxy() {
    return proxyInUse;
  }

   // Overrides default method in URLConnection
  public InputStream getInputStream() throws IOException {
    if (!connected) connect();
    if (headers == null) getHttpHeaders();
    if (!doInput) throw new ProtocolException("Can't open InputStream when doInput is false");
    return bufferedIn;
  }

   // Overrides default method in URLConnection
  public OutputStream getOutputStream() throws IOException {
    if (!connected) connect();
    if (!doOutput) throw new ProtocolException("Can't open OutputStream when doOutput is false");
    return socket.getOutputStream();
  }

   // Overrides default method in URLConnection
  public String getHeaderField(String name) {
    try {
      if (!connected) connect();
      if (headers == null) getHttpHeaders();
    } catch (IOException x) {
      return null;
    }
    return (String) hdrHash.get(name.toLowerCase());
  }

   // Overrides default method in URLConnection
  public Map getHeaderFields() {
    try {
      if (!connected) connect();
      if (headers == null) getHttpHeaders();
    } catch (IOException x) {
      return null;
    }
    return hdrHash;
  }

   // Overrides default method in URLConnection
  public String getHeaderField(int n) {
    try {
      if (!connected) connect();
      if (headers == null) getHttpHeaders();
    } catch (IOException x) {
      return null;
    }
    return (n < headers.size()) ? getField((String) headers.elementAt(n)) : null;
  }

  private String getField(String str) {
    if (str == null) return null;
    int index = str.indexOf(':');
    return (index >= 0) ? str.substring(index + 1).trim() : str;
  }

   // Overrides default method in URLConnection
  public String getHeaderFieldKey(int n) {
    try {
      if (!connected) connect();
      if (headers == null) getHttpHeaders();
    } catch (IOException x) {
      return null;
    }
    return (n < headers.size()) ? getFieldKey((String) headers.elementAt(n)) : null;
  }

  private String getFieldKey(String str) {
    if (str == null) return null;
    int index = str.indexOf(':');
    return (index >= 0) ? str.substring(0, index) : null;
  }

  private void getHttpHeaders() throws IOException {

     /* Originally tried using a BufferedReader here to take advantage of
      * the readLine method and avoid the following, but the buffer read
      * past the end of the headers so the first part of the content was lost.
      * It is probably more robust than it needs to be, e.g. the byte[]
      * is unlikely to overflow and a '\r' should always be followed by a '\n',
      * but it is better to be safe just in case. */

    bufferedIn = new BufferedInputStream(socket.getInputStream());
    if (headers == null) headers = new Vector();

    int buflen = 100;
    byte[] buf = new byte[buflen];
    String line = "";
    boolean gotnl = false;
    byte[] ch = new byte[1];
    ch[0] = (byte) '\n';

    while (true) {
       // Check for leftover byte from non-'\n' after a '\r'
      if (ch[0] != '\n')line = line + '\r' + new String(ch, 0, 1);

      int i;
      for (i = 0; i < buflen; i++) {
        buf[i] = (byte) bufferedIn.read();
        // System.out.println("i " + i + " buf[i] " + (char)buf[i]);
        if (buf[i] == -1) throw new IOException("Malformed HTTP header");
        if (buf[i] == '\r') {
          bufferedIn.read(ch, 0, 1);
          if (ch[0] == '\n') gotnl = true;
          break;
        }
      }
      line = line + new String(buf, 0, i);

       // A CRLF terminates a header entry; keep looping and appending to the line until one is found.
      if (gotnl) {
         // A zero length entry signals the end of the headers
        if (line.length() == 0) break;
         // Store the header and reinitialize for next cycle
        // System.out.println("header line: \"" + line + "\"");
        headers.addElement(line);
        String key = getFieldKey(line);
        if (key != null) hdrHash.put(key.toLowerCase(), getField(line));
        line = "";
        ch[0] = (byte)'\n';
        gotnl = false;
      }
    }
  }
}



