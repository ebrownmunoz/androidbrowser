package voxware.net.cookies;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import voxware.util.SysLog;

public class VoxwareCookieJar extends HashMap implements CookieJar, Serializable {

  /*****************************************************************************
   *
   *  This implementation of the CookieJar interface maintains a Map, keyed
   *  by IP address and port number, whose elements are CookieSets.
   *
   *****************************************************************************/

  public VoxwareCookieJar() {
    super();
  }

  /*****************************************************************************
   *
   *  Return a CookieSet[] of length Cookie.MaxValidVersion + 1, indexed by
   *  the version number. Calling this removes all stale Cookies in the
   *  given host's CookieSet from the CookieJar.
   *
   *****************************************************************************/

  public CookieSet[] getCookies(String host, String domain, String path, int port) {
     // Construct a template Cookie
    Cookie template = new Cookie();
    template.setDomain(domain);
    template.setPath(path);
    template.setPort(new int[] { port });
    template.setMaxAge(-1);
     // Construct the hashkey
    String key = host + ":" + Integer.toString(port);
     // Select the CookieSets
    CookieSet cached = (CookieSet)get(key);
    if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.URLCONNECTION_MASK)) {
      int numCookies = cached != null ? cached.size() : 0;
      SysLog.println("VoxwareCookieJar.getCookies: found " + numCookies + " cookie" + (String)(numCookies == 1 ? "" : "s") + " for host \"" + key + "\"");
    }
    CookieSet[] selection = new VoxwareCookieSet[Cookie.MaxValidVersion + 1];
    for (int version = 0; version <= Cookie.MaxValidVersion; version++) {
      template.setVersion(version);
      selection[version] = (cached != null) ? cached.select(template, true) : null;
      if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.URLCONNECTION_MASK)) {
        int numCookies = selection[version] != null ? selection[version].size() : 0;
        SysLog.println("VoxwareCookieJar.getCookies: found " + numCookies + " type " + version + " cookie" + (String)(numCookies == 1 ? "" : "s") +
                              " for host \"" + key + "\" matching domain \"" + domain + "\", path \"" + path + "\" and port " + port + ":");
        if (selection[version] != null && selection[version].size() > 0) SysLog.println("  " + selection[version].toString());
      }
    }
    return selection;
  }

  /*****************************************************************************
   *
   *  Return a CookieSet of all unstale cookies in the CookieJar that match
   *  the given template. Calling this removes all stale Cookies from the
   &  CookieJar.
   *
   *****************************************************************************/

  public CookieSet getCookies(Cookie template) {
    CookieSet selection = new VoxwareCookieSet();
    Collection cookieSets = values();
    if (cookieSets != null) {
       // Iterate over the hosts' CookieSets, selecting from each
      Iterator hosts = cookieSets.iterator();
      while (hosts.hasNext()) {
        CookieSet cached = (CookieSet)hosts.next();
        if (cached != null) selection.addAll(cached.select(template, true));
      }
    }
    return selection;
  }

  /*****************************************************************************
   *
   *  Insert all of the cookies in the CookieSet into the CookieJar.
   *
   *****************************************************************************/

  public void addCookies(String host, int port, CookieSet cookies) {
    String key = host + ":" + (String)((port < 0) ? "80" : Integer.toString(port));
    CookieSet cache = (CookieSet)get(key);
    if (cache == null) put(key, cookies);
    else               cache.addAll(cookies);
    if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.URLCONNECTION_MASK)) {
      int numCookies = cookies.size();
      SysLog.println("VoxwareCookieJar.addCookies: added " + numCookies + " cookie" + (String)(numCookies == 1 ? "" : "s") + " for host \"" + key + "\":");
      Iterator iterator = cookies.iterator();
      for (int i = 0; iterator.hasNext(); i++) SysLog.print(" Cookie[" + i + "]: " + ((Cookie)iterator.next()).toString() + "\n");
    }
  }
}
