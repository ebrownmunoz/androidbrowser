package voxware.net.cookies;

import java.io.Reader;
import java.io.PushbackReader;
import java.io.StringReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class VoxwareCookieMaker implements CookieMaker {

  private static final int    EndOfReader = -1;
  private static final char   Quotes = '\"';
  private static final char   Equals = '=';
  private static final char   Escape = '\\';
  private static final char   CookieDelimiter = ',';
  private static final char   AttributeDelimiter = ';';
  private static final String NameDelimiters = "=;,";
  private static final String ValueDelimiters = ";,";
  private static final String ListDelimiter = ",";

   // Parse the name field out of a Reader, leaving the Reader at the start of the next name
  private String name(PushbackReader reader) throws IOException {
    StringBuffer buffer = new StringBuffer();
    int character = reader.read();
    while (character != EndOfReader && NameDelimiters.indexOf(character) < 0) {
      buffer.append((char)character);
      character = reader.read();
    }
    String name = buffer.toString().trim();

    if (character != EndOfReader) reader.unread(character);
    return (name.length() > 0) ? name : null;
  }

   // Parse the value field out of a Reader, leaving the Reader at the start of the next name
  private String value(PushbackReader reader) throws IOException {

    int          character;
    StringBuffer buffer = new StringBuffer();
    String       value;

     // Skip over initial whitespace
    while ((character = reader.read()) == ' ' || character == '\t');
    if (character == Quotes) {
       // Value is quoted
      while ((character = reader.read()) != Quotes && character != EndOfReader) {
        buffer.append((char)character);
        if (character == Escape) {
          if ((character = reader.read()) == EndOfReader) break;
          else buffer.append((char)character);
        }
      }
       // Return the value string exactly as is, whether empty or not
      value = (character == Quotes) ? buffer.toString() : null;
       // Skip to the next delimiter
      while (character != EndOfReader && ValueDelimiters.indexOf(character) < 0) character = reader.read();
    } else {
       // Value is not quoted
      while (character != EndOfReader && ValueDelimiters.indexOf(character) < 0) {
        buffer.append((char)character);
        character = reader.read();
      }
      value = buffer.toString().trim();
       // If the value String is empty, return null
      if (value.length() == 0) value = null;
    }

    if (character != EndOfReader) reader.unread(character);
    return value;
  }

   // Skip past the next occurence of the specified delimiter outside of quotes
  private void skip(PushbackReader reader, char delimiter) throws IOException {
    int character;
    while (true) {
      character = reader.read();
      while (character != EndOfReader && character != Quotes && character != delimiter);
      if (character == Quotes) {
        while ((character = reader.read()) != Quotes && character != EndOfReader)
          if (character == Escape && (character = reader.read()) == EndOfReader) break;
        if (character == EndOfReader) break;
      } else {
        break;
      }
    }
  }

   /// CookieMaker Implementation ///

  public CookieSet makeCookies(String type, String recipes, String path, String domain, int[] port) {

    boolean setCookie2 = type.equalsIgnoreCase("Set-Cookie2");
    PushbackReader reader = new PushbackReader(new StringReader(recipes));
    CookieSet cookies = new VoxwareCookieSet();

    try {

       // Parse a concatenation of cookie recipes
      while (true) {

        int character;
        String name;
        String value;

         // Parse a cookie recipe
        if ((name = name(reader)) == null || name.length() == 0) {
           // Recipe has no name; skip over it
          skip(reader, CookieDelimiter);
        } else if ((character = reader.read()) == CookieDelimiter) {
           // Premature end of recipe
        } else if (character == EndOfReader) {
          break;
        } else if (character == AttributeDelimiter || (value = value(reader)) == null) {
           // Recipe has no value; skip over it
          skip(reader, CookieDelimiter);
        } else {
          Cookie cookie = new Cookie(name);
          cookie.setValue(value);

           // Parse any attributes, ignoring those that do not conform to specification
          Integer version = null;
          Integer maxAge  = null;
          Boolean discard = null;
          Boolean secure  = null;
          while ((character = reader.read()) == AttributeDelimiter) {
            if ((name = name(reader)) == null || name.length() == 0) {
               // Attribute has no name; ignore the value
              value(reader);
            } else {
              if ((character = reader.read()) != Equals) {
                reader.unread(character);
                value = null;
              } else {
                value = value(reader);
              }
              name = name.toLowerCase();
              if (name.equals("version")) {
                 // Obligatory value must be a token or quoted string that parses to a non-negative integer
                if (version == null) version = integer(value);
              } else if (name.equals("comment")) {
                 // Obligatory value may be a token or quoted string
                if (cookie.getComment() == null) cookie.setComment(value);
              } else if (name.equals("domain")) {
                 // Obligatory value may be a token or quoted string
                if (cookie.getDomain() == null) cookie.setDomain(value);
              } else if (name.equals("path")) {
                 // Obligatory value may be a token or quoted string
                if (cookie.getPath() == null) cookie.setPath(value);
              } else if (name.equals("max-age")) {
                 // Obligatory value must be a token or quoted string that parses to a non-negative integer
                if (maxAge == null) maxAge = integer(value);
              } else if (name.equals("secure")) {
                 // Must have no value
                if (secure == null && value == null) secure = Boolean.TRUE;
              } else if (setCookie2 && name.equals("discard")) {
                 // Must have no value
                if (discard == null && value == null) discard = Boolean.TRUE;
              } else if (setCookie2 && name.equals("commenturl")) {
                 // Obligatory value must be a quoted string
                if (cookie.getCommentURL() == null) cookie.setCommentURL(value);
              } else if (setCookie2 && name.equals("port")) {
                 // Optional value must be a quoted string containing a comma-separated list of strings that parse to integers
                if (cookie.getPort() == null) {
                  int[] portArray;
                  if (value != null) {
                    StringTokenizer tokens = new StringTokenizer(value, ListDelimiter);
                    ArrayList ports = new ArrayList();
                    Integer portNumber;
                    while (tokens.hasMoreTokens()) {
                      portNumber = integer(tokens.nextToken());
                      if (portNumber != null) ports.add(portNumber);
                    }
                    portArray = new int[ports.size()];
                    for (int i = 0; i < portArray.length; i ++) {
                      portArray[i] = ((Integer)ports.get(i)).intValue();
                    }
                  } else {
                     // An empty Port attribute defaults to the port argument
                    portArray = port;
                  }
                  cookie.setPort(portArray);
                }
              }
            }
          } // End of attribute parsing loop

           // Check the cookie for well-formedness

          if (cookie.getName() == null ||
              cookie.getValue() == null ||
              setCookie2 && version == null ||
              version != null && version.intValue() < 0 ||
              path != null && !cookie.pathMatches(path) ||
              domain != null && !cookie.domainMatches(domain) ||
              port != null && !cookie.portMatches(port)) {
             // Reject the cookie as ill-formed
            cookie = null;
          } else {
            if (version != null) cookie.setVersion(version.intValue());
            if (maxAge != null)  cookie.setMaxAge(maxAge.intValue());
             // Unless Max-Age is specified, the default is to discard the cookie when the session ends
            if (discard == null) cookie.setDiscard(maxAge == null);
            else                 cookie.setDiscard(discard.booleanValue());
            if (secure != null)  cookie.setSecure(secure.booleanValue());
          }

          if (cookie != null) cookies.add(cookie);
          if (character == EndOfReader) break;
        }
      } // End of cookie recipe parsing loop

    } catch (IOException e) {
      throw new RuntimeException("Unexpected: " + e);
    }

    return cookies;
  }

  private Integer integer(String string) {
    Integer integer = null;
    if (string != null) {
      try {
        integer = new Integer(string.trim());
      } catch (NumberFormatException e) {
        integer = null;
      }
    }
    return integer;
  }
}
