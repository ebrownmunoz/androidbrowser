package voxware.net.cookies;

import java.util.Iterator;

public class CookieMakerTest {

  private static CookieMaker cookieMaker = new VoxwareCookieMaker();

  public static void main(String[] args) {

    StringBuffer cmdLine = new StringBuffer("");

    if (args.length > 0) {
      cmdLine.append(args[0]);
      for (int i = 1; i < args.length; i++) cmdLine.append(' ').append(args[i]);
    }

    String recipe = cmdLine.toString();
    int colon = recipe.indexOf(":");
    String key;
    if (colon < 0 || !(key = cmdLine.substring(0, colon).trim()).equalsIgnoreCase("Set-Cookie") && !key.equalsIgnoreCase("Set-Cookie2")) {
      System.out.println("Invalid header key");
    } else if (cmdLine.length() > colon + 1) {

      recipe = cmdLine.substring(colon + 1);
      System.out.println("Recipe = " + recipe);

      CookieSet cookies = cookieMaker.makeCookies(key, recipe, null, null, new int[] { 8080 });

      int numCookies = cookies.size();
      System.out.println("Yields " + numCookies + " cookie" + (String)(numCookies == 1 ? ":" : "s:"));

      Iterator iterator = cookies.iterator();
      int i = 0;
      while (iterator.hasNext()) System.out.println(" Cookie[" + i++ + "]: " + ((Cookie)iterator.next()).toString());
    }
  }
}
