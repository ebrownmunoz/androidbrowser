package voxware.net.cookies;

import java.io.Serializable;
import java.util.SortedSet;

public interface CookieSet extends SortedSet, Serializable {

  /*****************************************************************************
   *
   *  This interface characterizes a SortedSet of Cookies. A typical
   *  implementor might extend TreeSet.
   *
   *****************************************************************************/

  /*****************************************************************************
   *
   *  Override the generic add(Object) method in Set to REPLACE a Cookie in the
   *  CookieSet with the given Cookie if the latter equals the former and has
   *  the same or a higher version number. Like its progenitor, this method
   *  returns true iff it modifies the CookieSet.
   *
   *  See Cookie.equals(Cookie template) for the semantics of equals.
   *
   *****************************************************************************/
  public boolean add(Cookie cookie);

  /*****************************************************************************
   *
   *  Override the generic addAll(Collection) method in Set to REPLACE Cookies
   *  with equal ones having the same or higher version number. Like its
   *  progenitor, this method returns true iff it modifies the CookieSet.
   *
   *  See Cookie.equals(Cookie template) for the semantics of equals.
   *
   *****************************************************************************/
  public boolean addAll(CookieSet cookies);

  /*****************************************************************************
   *
   *  Return a CookieSet containing only those Cookies that match the given
   *  Cookie template.
   *
   *  See Cookie.match(Cookie template)
   *
   *****************************************************************************/
  public CookieSet select(Cookie template);

  /*****************************************************************************
   *
   *  Return a CookieSet containing only those Cookies that match the
   *  given Cookie template, optionally discarding any that are stale.
   *
   *  See Cookie.match(Cookie template)
   *
   *****************************************************************************/
  public CookieSet select(Cookie template, boolean cull);

  /*****************************************************************************
   *
   *  Return a String representing the CookieSet in a format appropriate for
   *  sending in a Cookie request header.
   *
   *  The format of the String, as specified in RFC 2965 Section 3.3.4 (and
   *  earlier in the now obsolete RFC 2109), is:
   *  
   *    cookies  =  cookie-value 1*((";" | ",") cookie-value)
   *
   *  where it is presupposed that the cookie-value string for each constituent
   *  cookie can be obtained by invoking that cookie's toString() method
   *  (see voxware.net.cookies.Cookie).
   *  
   *  NOTE: This is exactly the same as the syntax specified in the earlier
   *  (now obsolete) RFC 2109. FOR BACKWARDS COMPATIBILITY, THE SEPARATOR
   *  SHOULD BE ";", not ",".
   *  
   *****************************************************************************/
  public String toString();
}
