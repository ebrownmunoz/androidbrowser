package voxware.net.cookies;

import java.io.Serializable;

public interface CookieJar extends Serializable {

  /*****************************************************************************
   *
   *  This interface characterizes a repository for cookies of all types
   *  and validity. Typically its implementor will maintain a Map, keyed
   *  by IP address and port number, whose values are CookieSets.
   *
   *****************************************************************************/

  /*****************************************************************************
   *
   *  Return a CookieSet[] indexed by the version number. The length of the
   *  returned CookieSet[] is always Cookie.MaxValidVersion + 1;
   *
   *****************************************************************************/

  public CookieSet[] getCookies(String host, String domain, String path, int port);

  /*****************************************************************************
   *
   *  Return a CookieSet of all cookies in the CookieJar that match the given
   *  template. See voxware.net.cookies.Cookie for the semantics of matching.
   *
   *****************************************************************************/

  public CookieSet getCookies(Cookie template);

  /*****************************************************************************
   *
   *  Insert all of the valid cookies in the CookieSet into the CookieJar. If
   *  URL is null, all cookies in the CookieSet are considered valid.
   *
   *****************************************************************************/

  public void addCookies(String host, int port, CookieSet cookies);
}
