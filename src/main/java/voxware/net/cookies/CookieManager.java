package voxware.net.cookies;

import java.io.*;
import java.net.*;

public interface CookieManager {

  /*****************************************************************************
   *
   *  Per RFC 2965 Section 9.1, for backwards compatibility:
   *
   *  Existing cookie implementations, based on the Netscape specification,
   *  use the Set-Cookie (not Set-Cookie2) header. User agents that receive in
   *  the same response both a Set-Cookie and Set-Cookie2 response header for
   *  the same cookie MUST discard the Set-Cookie information and use only the
   *  Set-Cookie2 information. Furthermore, a user agent MUST assume, if it
   *  receives a Set-Cookie2 response header, that the sending server complies
   *  with this document and will understand Cookie request headers that also
   *  follow this specification.
   *
   *  New cookies MUST replace both equivalent old- and new-style cookies. That
   *  is, if a user agent that follows both this specification and Netscape's
   *  original specification receives a Set-Cookie2 response header, and the
   *  NAME, Domain and Path attributes equal those of a Netscape-style cookie,
   *  the Netscape-style cookie MUST be discarded, and the user agent MUST
   *  retain only the cookie adhering to this specification.
   *
   *  Older user agents that do not understand this specification, but that do
   *  understand Netscape's original specification, will not recognize the
   *  Set-Cookie2 response header and will receive and send cookies according to
   *  the older specification. A user agent that supports both this specification
   *  and Netscape-style cookies SHOULD send a Cookie request header that follows
   *  the older Netscape specification if it receives the cookie in a Set-Cookie
   *  response header and not in a Set-Cookie2 response header. However, it
   *  SHOULD send the following request header as well:
   *
   *     Cookie2: $Version="1"
   *
   *  The Cookie2 header advises the server that the user agent understands
   *  new-style cookies. If the server understands new-style cookies, as well,
   *  it SHOULD continue the stateful session by sending a Set- Cookie2 response
   *  header, rather than Set-Cookie. A server that does not understand
   *  new-style cookies will simply ignore the Cookie2 request header.
   *
   *****************************************************************************/

  /*****************************************************************************
   *
   *  Get/Set this CookieManager's CookieJar
   *
   *  See voxware.net.cookies.CookieJar. Not that since a CookieJar is
   *  Serializable, this pair of access methods provides a way to save the
   *  the CookieJar to non-volatile storage and restore it later.
   *
   *****************************************************************************/
  public CookieJar getCookieJar();
  public void      setCookieJar(CookieJar cookieJar);

  /*****************************************************************************
   *
   *  Get/Set this CookieManager's CookieMaker
   *
   *  See voxware.net.cookies.CookieMaker
   *
   *****************************************************************************/
  public CookieMaker getCookieMaker();
  public void        setCookieMaker(CookieMaker cookieMaker);

  /*****************************************************************************
   *
   *  Get cookie(s) from the response header and cache them locally
   *  
   *  Per RFC 2965 Section 3.3.2 (and earlier in the now obsolete RFC 2901],
   *  to prevent possible security or privacy violations, a user agent rejects
   *  a Set-Cookie2 (Set-Cookie) according to the rules below. The goal of the
   *  rules is to try to limit the set of servers for which a cookie is valid,
   *  based on the values of the Path, Domain, and Port attributes and the
   *  request-URI, request-host and request-port.
   *
   *  A user agent rejects a Set-Cookie2 (SHALL NOT store its information) if
   *  the Version attribute is missing [RFC 2965], but accepts a Set-Cookie with
   *  a missing Version attribute, defaulting the version to zero [RFC 2109].
   *  Moreover, a user agent rejects a cookie if any of the following is true of
   *  the attributes explicitly present in the Set-Cookie2 response header:
   *
   *     *  The value for the Path attribute is not a prefix of the
   *        request-URI.
   *  
   *     *  The value for the Domain attribute contains no embedded dots,
   *        and the value is not .local.
   *  
   *     *  The effective host name that derives from the request-host does
   *        not domain-match the Domain attribute.
   *  
   *     *  The request-host is a HDN (not IP address) and has the form HD,
   *        where D is the value of the Domain attribute, and H is a string
   *        that contains one or more dots.
   *  
   *     *  The Port attribute has a "port-list", and the request-port is
   *        not in the list [Set-Cookie2].
   *  
   *  Examples:
   *  
   *     *  A Set-Cookie2 from request-host y.x.foo.com for Domain=.foo.com
   *        would be rejected, because H is y.x and contains a dot.
   *  
   *     *  A Set-Cookie2 from request-host x.foo.com for Domain=.foo.com
   *        would be accepted.
   *  
   *     *  A Set-Cookie2 with Domain=.com or Domain=.com., will always be
   *        rejected, because there is no embedded dot.
   *  
   *     *  A Set-Cookie2 with Domain=ajax.com will be accepted, and the
   *        value for Domain will be taken to be .ajax.com, because a dot
   *        gets prepended to the value.
   *  
   *     *  A Set-Cookie2 with Port="80,8000" will be accepted if the
   *        request was made to port 80 or 8000 and will be rejected
   *        otherwise.
   *  
   *     *  A Set-Cookie2 from request-host example for Domain=.local will
   *        be accepted, because the effective host name for the request-
   *        host is example.local, and example.local domain-matches .local.
   *  
   *  Per RFC 2965 Section 3.3.3, if a user agent receives a Set-Cookie2
   *  response header whose NAME is the same as that of a cookie it has
   *  previously stored, THE NEW COOKIE SUPERSEDES THE OLD when both
   *
   *     *  The old and new Domain attribute values compare equal, using a
   *        case-insensitive string-compare; AND
   *
   *     *  The old and new Path attribute values string-compare equal
   *        (case-sensitive).
   *
   *  However, if the Set-Cookie2 has a value for Max-Age of zero, the
   *  (old and new) cookie is discarded. Otherwise a cookie persists
   *  (resources permitting) until whichever of the following happens first,
   *  then gets discarded:
   *
   *     *  The Max-Age attribute is exceeded; OR
   *
   *     *  The Discard attribute is set, the user agent terminates the
   *        session.
   *
   *  Because user agents have finite space in which to store cookies, they MAY
   *  also discard older cookies to make space for newer ones, using, for
   *  example, a least-recently-used algorithm, along with constraints on the
   *  maximum number of cookies that each origin server may set.
   *  
   *  If a Set-Cookie2 response header includes a Comment attribute, the user
   *  agent SHOULD store that information in a human-readable form with the
   *  cookie and SHOULD display the comment text as part of a cookie inspection
   *  user interface.
   *
   *  If a Set-Cookie2 response header includes a CommentURL attribute, the user
   *  agent SHOULD store that information in a human-readable form with the
   *  cookie, or, preferably, SHOULD allow the user to follow the http_URL link
   *  as part of a cookie inspection user interface.
   *  
   *  User agents SHOULD allow the user to control cookie destruction, but they
   *  they MUST NOT extend the cookie's lifetime beyond that controlled by the
   *  Discard and Max-Age attributes. An infrequently-used cookie may function
   *  as a "preferences file" for network applications, and a user may wish to
   *  keep it even if it is the least-recently-used cookie. One possible
   *  implementation would be an interface that allows the permanent storage of
   *  a cookie through a checkbox (or, conversely, its immediate destruction).
   *  
   *****************************************************************************/
  public void getCookies(URL url, URLConnection urlConnection);

  /*****************************************************************************
   *
   *  Set request properties for the cached cookie(s)
   *
   *  As specified in RFC 2965 (and earlier in the now obsolete RFC 2109),
   *  the format of the request properties is:
   *
   *    cookie          =  "Cookie:" cookie-version [(";" | ",") cookies]
   *    cookie-version  =  "$Version" "=" value
   *
   *  where it is assumed that the cookies string can be obtained
   *  by invoking the toString() method on the desired CookieSet
   *  (see voxware.net.cookies.CookieSet)
   *
   *  NOTE: This is exactly the same as the syntax specified in the earlier
   *  (now obsolete) RFC 2109. FOR BACKWARDS COMPATIBILITY, THE SEPARATOR
   *  SHOULD BE ";", not ",".
   *  
   *  The value of the Version attribute MUST be the value from the Version
   *  attribute of the corresponding Set-Cookie or Set-Cookie2 response header.
   *  Otherwise the Version is 0 (SAME AS RFC 2109).
   *
   *  The following rules determine which cookies are incorporated:
   * 
   *     Domain Selection
   *        The origin server's effective host name must domain-match the
   *        Domain attribute of the cookie, if any.
   * 
   *     Port Selection
   *        There are three possible behaviors, depending on the
   *        Cookie's Port attribute:
   * 
   *        1. If a Cookie's port-list is null, the port portion of the URL
   *           is ignored.
   * 
   *        2. If a Cookie has an empty port-list, the port portion of the
   *           URL must match the request-port the Cookie was received from.
   *           (In practice, this is the same as 3 below, since an empty Port
   *           attribute defaults to a list having this one entry.)
   * 
   *        3. If a Cookie has a non-empty port-list, the port portion of the
   *           URL must match one of the entries in the list.
   * 
   *     Path Selection
   *        The path portion of the given URL must path-match the Path
   *        attribute of the cookie. This means that the Path attribute must
   *        match a prefix of the path portion of the URL.
   * 
   *     Max-Age Selection
   *        The cookies must not have expired.
   *
   *  If multiple cookies satisfy the criteria above, they are ordered in the
   *  Cookie header such that those with more specific Path attributes precede
   *  those with less specific. Ordering with respect to other attributes is
   *  unspecified. (SAME AS RFC 2109)
   *  
   *****************************************************************************/
  public void setCookies(URL url, URLConnection urlConnection);
}
