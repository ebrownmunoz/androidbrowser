package voxware.net.cookies;

import java.io.Serializable;
import java.util.TreeSet;
import java.util.Iterator;

public class VoxwareCookieSet extends TreeSet implements CookieSet, Serializable {

   /// CookieSet Implementation ///

  public boolean add(Cookie cookie) {
    boolean success = super.add(cookie);
    if (!success) {
      Cookie element = (Cookie)this.tailSet(cookie).first();
      if (cookie.getVersion() >= element.getVersion()) {
         // It's cheaper to graft than to delete and insert
        cookie.copyInto(element);
        success = true;
      }
    }
    return success;
  }

  public boolean addAll(CookieSet cookies) {
    boolean success = false;
    if (cookies != null) {
      Iterator iterator = cookies.iterator();
      while (iterator.hasNext()) success = success || add((Cookie)iterator.next());
    }
    return success;
  }

  public CookieSet select(Cookie template) {
    CookieSet selections = new VoxwareCookieSet();
    Iterator iterator = iterator();
    while (iterator.hasNext()) {
      Cookie selection = (Cookie)iterator.next();
      if (selection.matches(template)) selections.add(selection);
    }
    return selections;
  }

  public CookieSet select(Cookie template, boolean cull) {
    long currentTimeMillis = cull ? System.currentTimeMillis() : Long.MIN_VALUE;
    CookieSet selections = new VoxwareCookieSet();
    Iterator iterator = iterator();
    while (iterator.hasNext()) {
      Cookie selection = (Cookie)iterator.next();
      if (cull && selection.isStale(currentTimeMillis)) iterator.remove();
      else if (selection.matches(template)) selections.add(selection);
    }
    return selections;
  }

  public String toString() {
    StringBuffer cookies = new StringBuffer();
    Iterator iterator = iterator();
    if (iterator.hasNext()) cookies.append(((Cookie)iterator.next()).toString());
    while (iterator.hasNext()) cookies.append("; ").append(((Cookie)iterator.next()).toString());
    return cookies.toString();
  }
}
