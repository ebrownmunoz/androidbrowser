package voxware.net.cookies;

import java.io.*;
import java.net.*;

public class VoxwareCookieManager implements CookieManager {

  private CookieJar      cookieJar = new VoxwareCookieJar();
  public  CookieJar   getCookieJar()                          { return cookieJar; }
  public  void        setCookieJar(CookieJar cookieJar)       { this.cookieJar = cookieJar; }

  private CookieMaker    cookieMaker = new VoxwareCookieMaker();
  public  CookieMaker getCookieMaker()                        { return cookieMaker; }
  public  void        setCookieMaker(CookieMaker cookieMaker) { this.cookieMaker = cookieMaker; }

  public VoxwareCookieManager() {
  }

   /// CookieManager Implementation ///

   // Get cookie(s) from the response header and cache them locally
  public void getCookies(URL url, URLConnection urlConnection) {
    if (url != null && urlConnection != null) {
      int[] port = new int[] { url.getPort() };
      if (port[0] < 0) port[0] = 80;
      String headerFieldKey;
      for (int field = 1; (headerFieldKey = urlConnection.getHeaderFieldKey(field)) != null; field++) {
        if (headerFieldKey.equalsIgnoreCase("Set-Cookie") || headerFieldKey.equalsIgnoreCase("Set-Cookie2")) {
          CookieSet cookies = cookieMaker.makeCookies(headerFieldKey, urlConnection.getHeaderField(field), url.getPath(), url.getHost(), port);
          cookieJar.addCookies(url.getHost(), port[0], cookies);
        }
      }
    }
  }

   // Set request properties for the cached cookie(s)
  public void setCookies(URL url, URLConnection urlConnection) {
    if (url != null && urlConnection != null) {
      int port = url.getPort();
      if (port < 0) port = 80;
      CookieSet[] cookies = cookieJar.getCookies(url.getHost(), url.getHost(), url.getPath(), port);
      StringBuffer value = null;
      for (int version = 0; version < cookies.length; version++) {
        if (cookies[version] != null) {
           // Stamp with the lowest version number
          if (value == null) value = new StringBuffer("$Version=\"").append(version).append('\"');
          value.append("; ").append(cookies[version].toString());
        }
        if (value != null) urlConnection.addRequestProperty("Cookie", value.toString());
      }
      if (cookies[0] != null) urlConnection.addRequestProperty("Cookie2", "$Version=\"1\"");
    }
  }
}
