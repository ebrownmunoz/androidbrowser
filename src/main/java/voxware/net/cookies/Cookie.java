package voxware.net.cookies;

import java.io.Serializable;
import voxware.util.SysLog;

public class Cookie implements Comparable, Serializable {

  /*****************************************************************************
   *
   *  This is the fundamental cookie class. It provides bean-style access
   *  methods to all of the cookie attributes, methods to determine the
   *  similarity and equality of cookies, and a toString() method that
   *  serializes a cookie in a format appropriate for incorporation in a
   *  request header.
   *
   *  There is an unfortunate asymmetry in the voxware.net .cookies package
   *  in that this class does not do de-serialization, which, for reasons of
   *  efficiency, is relegated to the CookieMaker interface.
   *
   *****************************************************************************/


  private String        name;
  public  String     getName()                     { return this.name; }
  public  void       setName(String name)          { this.name = name; }

  private String        value;
  public  String     getValue()                    { return this.value; }
  public  void       setValue(String value)        { this.value = value; }

  private String        comment;
  public  String     getComment()                  { return this.comment; }
  public  void       setComment(String comment)    { this.comment = comment; }

  private String        commentURL;
  public  String     getCommentURL()               { return this.commentURL; }
  public  void       setCommentURL(String comment) { this.commentURL = comment; }

  private String        domain;
  public  String     getDomain()                   { return this.domain; }
  public  void       setDomain(String domain)      { this.domain = domain; }

  private String        path;
  public  String     getPath()                     { return this.path; }
  public  void       setPath(String path)          { this.path = path; }

  private int[]         port;
  public  int[]      getPort()                     { return this.port; }
  public  void       setPort(int[] port)           { this.port = port; }

  private boolean       discard = true;
  public  void       setDiscard(boolean discard)   { this.discard = discard; }
  public  boolean shouldDiscard()                  { return this.discard; }

  private boolean       secure = false;
  public  void       setSecure(boolean secure)     { this.secure = secure; }
  public  boolean     isSecure()                   { return this.secure; }

  public static final int MaxValidVersion = 1;
  private int           version = 0;
  public  int        getVersion()                  { return this.version; }
  public  void       setVersion(int version)       { this.version = version; }

  public static final int MaxMaxAge = Integer.MAX_VALUE;
  private int           maxAge;
  public  int        getMaxAge()                   { return this.maxAge; }
  public  void       setMaxAge(int maxAge)         { this.maxAge = maxAge; deathTime = System.currentTimeMillis() + (long)maxAge * 1000L; }

  private long deathTime;

  public Cookie() {
    setMaxAge(MaxMaxAge);
  }

  public Cookie(String name) {
    this();
    this.name = name;
  }

  /*****************************************************************************
   *
   *  A domain String domain-matches a Cookie's Domain attribute iff the latter
   *  is null or is equal (case-insensitive) to the former.
   *  NB: THIS IS A PARTIAL IMPLEMENTATION THAT WILL WORK IN ONLY IN
   *  ENVIRONMENTS WHERE THE DOMAIN IS ALWAYS AN IP ADDRESS.
   *
   *****************************************************************************/
  public boolean domainMatches(String domain) {
    return this.domain == null || this.domain.equalsIgnoreCase(domain);
  }

  /*****************************************************************************
   *
   *  A path path-matches a Cookie's Path attribute iff the latter is null or is
   *  a prefix (case-sensitive) of the former.
   *
   *****************************************************************************/
  public boolean pathMatches(String path) {
    return this.path == null || path.startsWith(this.path) && (path.length() == this.path.length() || path.charAt(this.path.length()) == '/');
  }

  /*****************************************************************************
   *
   *  A port[] port-matches a Cookie's Port attribute iff the latter is
   *  null or contains all of the elements of the former.
   *
   *****************************************************************************/
  public boolean portMatches(int[] port) {
     // If no port is specified in the cookie, any will do
    if (this.port != null && port != null) {
      for (int i = 0; i < port.length; i++) {
        int j;
        for (j = 0; j < this.port.length; j++) if (port[i] == this.port[j]) break;
        if (j == this.port.length) return false;
      }
    }
    return true;
  }

  public boolean isStale() {
    return isStale(System.currentTimeMillis());
  }

  public boolean isStale(long currentTimeMillis) {
    if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.URLCONNECTION_MASK)) {
      if (currentTimeMillis > deathTime) {
        SysLog.println("Cookie.isStale: true, because currentTimeMillis (" + currentTimeMillis + ") > deathTime (" + deathTime + ")");
      } else {
        SysLog.println("Cookie.isStale: false, because currentTimeMillis (" + currentTimeMillis + ") <= deathTime (" + deathTime + ")");
      }
    }
    return (currentTimeMillis > deathTime);
  }

  /*****************************************************************************
   *
   *  Return true iff this Cookie matches the given template Cookie.
   *
   *  The following rules, motivated by the requirements of RFC 2965 (and of
   *  the now obsolete RFC 2109), determine the results of a match:
   *
   *     Name
   *        If the template Cookie's name is non-null, this Cookie's name
   *        must equal (case-insensitive) it.
   *
   *     Value
   *        If the template Cookie's value is non-null, this Cookie's value
   *        must equal (case-sensitive) it.
   * 
   *     Domain
   *        If both this Cookie and the template Cookie have non-null Domain
   *        attributes, the template's Domain must domain-match the Domain
   *        attribute of this Cookie.
   * 
   *     Path
   *        If both this Cookie and the template Cookie have non-null Path
   *        attributes, this Cookie's Path must equal (case-sensitive) a prefix
   *        of the template Cookie's Path.
   * 
   *     Port
   *        If both this Cookie and the template Cookie have non-null Port
   *        attributes, every port number in the template's Port list must
   *        also be in this Cookie's Port list.
   * 
   *     Max-Age
   *        If the template's Max-Age attribute is not negative, it must equal
   *        this Cookie's Max-Age.
   *
   *     Version
   *        If the template's Version attribute is not negative, it must equal
   *        this Cookie's Version.
   *
   *****************************************************************************/
  public boolean matches(Cookie template) {
     // For efficiency, compare version first
    if (SysLog.printLevel > 3 && SysLog.printMaskBitsSet(SysLog.URLCONNECTION_MASK)) {
      if        (!(template.version < 0                      || version == template.version)) {
        SysLog.println("Cookie.matches: fails because version (" + version + ") != template.version (" + template.version + ")");
      } else if (!(template.domain == null || domain == null || domainMatches(template.domain))) {
        SysLog.println("Cookie.matches: fails because domain (\"" + domain + "\") does not domain-match template.domain (\"" + template.domain + "\")");
      } else if (!(template.path == null   || path == null   || pathMatches(template.path))) {
        SysLog.println("Cookie.matches: fails because path (\"" + path + "\") does not path-match template.path (\"" + template.path + "\")");
      } else if (!(template.port == null   || port == null   || portMatches(template.port))) {
        SysLog.println("Cookie.matches: fails because port (" + portToString() + ") does not port-match template.port (" + template.portToString() + ")");
      } else if (!(template.name == null   || name == null   || name.equalsIgnoreCase(template.name))) {
        SysLog.println("Cookie.matches: fails because name (\"" + name + "\") does not equal template.name (\"" + template.name + "\")");
      } else if (!(template.value == null  || value == null  || value.equalsIgnoreCase(template.value))) {
        SysLog.println("Cookie.matches: fails because value (\"" + value + "\") does not equal template.value (\"" + template.value + "\")");
      } else if (!(template.maxAge < 0                       || maxAge == template.maxAge)) {
        SysLog.println("Cookie.matches: fails because maxAge (" + maxAge + ") != template.maxAge (" + template.maxAge + ")");
      } else {
        SysLog.println("Cookie.matches: succeeds");
        return true;
      }
      return false;
    } else {
      return (template.version < 0                      || version == template.version) &&
             (template.domain == null || domain == null || domainMatches(template.domain)) &&
             (template.path == null   || path == null   || pathMatches(template.path)) &&
             (template.port == null   || port == null   || portMatches(template.port)) &&
             (template.name == null   || name == null   || name.equalsIgnoreCase(template.name)) &&
             (template.value == null  || value == null  || value.equalsIgnoreCase(template.value)) &&
             (template.maxAge < 0                       || maxAge == template.maxAge);
    }
  }

  public String portToString() {
    StringBuffer ports = new StringBuffer("{ ");
    if (port.length > 0) ports.append(port[0]);
    for (int i = 1; i < port.length; i++) ports.append(", " + port[i]);
    ports.append(" }");
    return ports.toString();
  }

  /*****************************************************************************
   *
   *  Return true iff this Cookie equals the given Cookie.
   *
   *  Per RFC 2965 Section 3.3.3, if a user agent receives a Set-Cookie2
   *  response header whose NAME is the same as that of a cookie it has
   *  previously stored, THE NEW COOKIE SUPERSEDES THE OLD when both
   *
   *     *  The old and new Domain attribute values compare equal, using a
   *        case-insensitive string-compare; AND
   *
   *     *  The old and new Path attribute values string-compare equal
   *        (case-sensitive).
   *
   *****************************************************************************/
  public boolean equals(Cookie other) {
    return (other.name == null   && name == null   || name.equalsIgnoreCase(other.name)) &&
           (other.domain == null && domain == null || domain.equalsIgnoreCase(other.domain)) &&
           (other.path == null   && path == null   || path.equals(other.path));
  }

  /*****************************************************************************
   *
   *  This implementation of the Comparable interface establishes a natural
   *  ordering of Cookies in which those with more specific Path attributes
   *  precede those with less specific. It is "consistent with equals", in that
   *  it returns 0 iff this.equals(that).
   *
   *****************************************************************************/
  public int compareTo(Object that) throws ClassCastException {
    return compareTo((Cookie)that);
  }

  /*****************************************************************************
   *
   *  This method establishes a natural ordering of Cookies in which those with
   *  more specific Path attributes precede those with less specific. It is
   *  "consistent with equals", in that it returns 0 iff this.equals(that).
   *
   *  Per RFC 2965 Section 3.3.4, if multiple cookies are sent in the same
   *  Cookie header, they must be ordered in this fashion. Ordering with
   *  respect to other attributes (e.g., Domain) is unspecified.
   *
   *****************************************************************************/
  public int compareTo(Cookie that) {
     // For consistency with equals, return zero if this and that are equal
    if (this.equals(that)) {
      return 0;
    } else if (this.path == null) {
      return +1;
    } else if (that.path == null) {
      return -1;
    } else {
      int comparison = that.path.compareTo(this.path);
       // For consistency with equals, return zero only if this and that are equal
      return (comparison == 0) ? -1 : comparison;
    }
  }

  /*****************************************************************************
   *
   *  Copies all of this Cookie's fields into the target Cookie.
   *  Does nothing if the target Cookie is null.
   *
   *****************************************************************************/
  public void copyInto(Cookie target) {
    if (target != null) {
      target.name = name;
      target.value = value;
      target.comment = comment;
      target.commentURL = commentURL;
      target.domain = domain;
      target.path = path;
      target.port = port;
      target.discard = discard;
      target.secure = secure;
      target.version = version;
      target.maxAge = maxAge;
      target.deathTime = deathTime;
    }
  }

  /*****************************************************************************
   *
   *  Return a String representing the cookie in the appropriate format for
   *  sending as part of a Cookie request header.
   *
   *  The format of the String, as specified in RFC 2965 Section 3.3.4 (and
   *  earlier in the now obsolete RFC 2109), is:
   *  
   *    cookie-value  =  NAME "=" VALUE [";" path] [";" domain] [";" port]
   *    NAME          =  attr
   *    VALUE         =  value
   *    path          =  "$Path" "=" value
   *    domain        =  "$Domain" "=" value
   *    port          =  "$Port" [ "=" <"> value <"> ]
   *  
   *  NOTE: This is exactly the same as the syntax specified in RFC 2109,
   *  with the addition of the optional port attribute, which will be null
   *  in cookies derived from Set-Cookie response headers.
   *  
   *  The value of the path attribute MUST be the value from the Path
   *  attribute, if one was present, of the corresponding Set-Cookie2 response
   *  header. Otherwise the attribute SHOULD be omitted from the Cookie request
   *  header. (SAME AS RFC 2109)
   *  
   *  The value of the domain attribute MUST be the value from the Domain
   *  attribute, if one was present, of the corresponding Set-Cookie2 response
   *  header. Otherwise the attribute SHOULD be omitted from the Cookie request
   *  header. (SAME AS RFC 2109)
   *  
   *  The port attribute of the Cookie request header MUST mirror the Port
   *  attribute, if one was present, in the corresponding Set-Cookie2 response
   *  header. That is, the port attribute MUST be present if the Port attribute
   *  was present in the Set-Cookie2 header, and it MUST have the same value,
   *  if any. Otherwise, if the Port attribute was absent from the Set-Cookie2
   *  header, the attribute likewise MUST be omitted from the Cookie request
   *  header. (NEW, but consistent with RFC 2109)
   *  
   *  Note that there is neither a Comment nor a CommentURL attribute in the
   *  Cookie request header corresponding to the ones in the Set- Cookie2
   *  response header. The user agent does not return the comment information
   *  to the origin server. (SAME AS RFC 2109)
   *
   *  Note (from RFC 2965 Section 10.1.3): 
   *
   *  In Netscape's original proposal, the values in attribute-value pairs did
   *  not accept quoted strings.  Consequently, a user agent should only use
   *  quotes around values in Cookie headers when the cookie's versions are all
   *  compliant with this specification (Version 1) or later.
   *
   *****************************************************************************/
  public String toString() {
    StringBuffer cookie = new StringBuffer(name);
    if (version > 0) {
      cookie.append("=\"").append(value).append('\"');
      if (path != null) cookie.append("; ").append("$Path=\"").append(path).append('\"');
      if (domain != null) cookie.append("; ").append("$Domain=\"").append(domain).append('\"');
      if (port != null && port.length > 0) {
        cookie.append("; ").append("$Port= \"").append(Integer.toString(port[0]));
        for (int i = 1; i < port.length; i++) cookie.append(", ").append(Integer.toString(port[0]));
        cookie.append('\"');
      }
    } else {
      cookie.append("=").append(value);
      if (path != null) cookie.append("; ").append("$Path=").append(path);
      if (domain != null) cookie.append("; ").append("$Domain=").append(domain);
    }
    return cookie.toString();
  }
}
