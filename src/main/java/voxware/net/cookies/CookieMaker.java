package voxware.net.cookies;

public interface CookieMaker {

  /*****************************************************************************
   *
   *  Construct a CookieSet from a recipe. This CookieSet factory is the
   *  fundamental de-serialization method. As such, it is the normal source
   *  of CookieSets and Cookies.
   *
   *  In a typical implementation, the recipe will be the value string of a
   *  "Set-Cookie" (RFC 2109 4.2.2) or "Set-Cookie2" (RFC 2965 Section 3.2.2)
   *  response header. The type will be the corresponding key ("Set-Cookie"
   *  or "Set-Cookie2").
   *
   *  Per RFC 2965 Section 3.2.2, the syntax for the Set-Cookie2 response header
   *  is
   *  
   *     set-cookie      =       "Set-Cookie2:" cookies
   *     cookies         =       1#cookie
   *     cookie          =       NAME "=" VALUE *(";" set-cookie-av)
   *     NAME            =       attr
   *     VALUE           =       value
   *     set-cookie-av   =       "Comment" "=" value
   *                     |       "CommentURL" "=" <"> http_URL <">
   *                     |       "Discard"
   *                     |       "Domain" "=" value
   *                     |       "Max-Age" "=" value
   *                     |       "Path" "=" value
   *                     |       "Port" [ "=" <"> portlist <"> ]
   *                     |       "Secure"
   *                     |       "Version" "=" 1*DIGIT
   *     portlist        =       1#portnum
   *     portnum         =       1*DIGIT
   *  
   *  NOTE: This is identical to the syntax specified in RFC 2109 Section 4.2.2,
   *  with the addition of the "CommentURL", "Discard" and "Port" attributes, so
   *  we do not need to treat the two cases separately.
   *  
   *  Informally, the Set-Cookie2 response header comprises the token
   *  Set-Cookie2:, followed by a comma-separated list of one or more cookies.
   *  Each cookie begins with a NAME=VALUE pair, followed by zero or more
   *  semi-colon-separated attribute-value pairs. The syntax for attribute-value
   *  pairs was shown earlier. The specific attributes and the semantics of
   *  their values follows. The NAME=VALUE attribute- value pair MUST come first
   *  in each cookie. The others, if present, can occur in any order. If an
   *  attribute appears more than once in a cookie, the client SHALL use only
   *  the value associated with the FIRST appearance of the attribute; a client
   *  MUST ignore values after the first.
   *
   *  The NAME of a cookie MAY be the same as one of the attributes in this
   *  specification. However, because the cookie's NAME must come first in a
   *  Set-Cookie2 response header, the NAME and its VALUE cannot be confused
   *  with an attribute-value pair.
   *  
   *     NAME=VALUE
   *        REQUIRED.  The name of the state information ("cookie") is NAME,
   *        and its value is VALUE.  NAMEs that begin with $ are reserved and
   *        MUST NOT be used by applications.
   *  
   *        The VALUE is opaque to the user agent and may be anything the
   *        origin server chooses to send, possibly in a server-selected
   *        printable ASCII encoding.  "Opaque" implies that the content is of
   *        interest and relevance only to the origin server.  The content
   *        may, in fact, be readable by anyone that examines the Set-Cookie2
   *        header.
   *  
   *     Comment=value
   *        OPTIONAL.  Because cookies can be used to derive or store private
   *        information about a user, the value of the Comment attribute
   *        allows an origin server to document how it intends to use the
   *        cookie.  The user can inspect the information to decide whether to
   *        initiate or continue a session with this cookie.  Characters in
   *        value MUST be in UTF-8 encoding. [RFC2279]
   *  
   *     CommentURL="http_URL"
   *        OPTIONAL.  Because cookies can be used to derive or store private
   *        information about a user, the CommentURL attribute allows an
   *        origin server to document how it intends to use the cookie.  The
   *        user can inspect the information identified by the URL to decide
   *        whether to initiate or continue a session with this cookie.
   *  
   *     Discard
   *        OPTIONAL.  The Discard attribute instructs the user agent to
   *        discard the cookie unconditionally when the user agent terminates.
   *  
   *     Domain=value
   *        OPTIONAL.  The value of the Domain attribute specifies the domain
   *        for which the cookie is valid.  If an explicitly specified value
   *        does not start with a dot, the user agent supplies a leading dot.
   *  
   *     Max-Age=value
   *        OPTIONAL.  The value of the Max-Age attribute is delta-seconds,
   *        the lifetime of the cookie in seconds, a decimal non-negative
   *        integer.  To handle cached cookies correctly, a client SHOULD
   *        calculate the age of the cookie according to the age calculation
   *        rules in the HTTP/1.1 specification [RFC2616].  When the age is
   *        greater than delta-seconds seconds, the client SHOULD discard the
   *        cookie.  A value of zero means the cookie SHOULD be discarded
   *        immediately.
   *  
   *     Path=value
   *        OPTIONAL.  The value of the Path attribute specifies the subset of
   *        URLs on the origin server to which this cookie applies.
   *  
   *     Port[="portlist"]
   *        OPTIONAL.  The Port attribute restricts the port to which a cookie
   *        may be returned in a Cookie request header.  Note that the syntax
   *        REQUIREs quotes around the OPTIONAL portlist even if there is only
   *        one portnum in portlist.
   *  
   *     Secure
   *        OPTIONAL.  The Secure attribute (with no value) directs the user
   *        agent to use only (unspecified) secure means to contact the origin
   *        server whenever it sends back this cookie, to protect the
   *        confidentially and authenticity of the information in the cookie.
   *  
   *        The user agent (possibly with user interaction) MAY determine what
   *        level of security it considers appropriate for "secure" cookies.
   *        The Secure attribute should be considered security advice from the
   *        server to the user agent, indicating that it is in the session's
   *        interest to protect the cookie contents.  When it sends a "secure"
   *        cookie back to a server, the user agent SHOULD use no less than
   *        the same level of security as was used when it received the cookie
   *        from the server.
   *  
   *     Version=value
   *        REQUIRED.  The value of the Version attribute, a decimal integer,
   *        identifies the version of the state management specification to
   *        which the cookie conforms.  For this specification, Version=1
   *        applies.
   *  
   *  Interpreting Set-Cookie [RFC 2109 4.3.1] and Set-Cookie2 [RFC 2965 3.3.1]
   *  
   *     The user agent keeps separate track of state information that arrives
   *     via Set-Cookie and Set-Cookie2 response headers from each origin server
   *     (as distinguished by name or IP address and port).  The user agent MUST
   *     [Set-Cookie2 only] ignore attribute-value pairs whose attribute it does
   *     not recognize. The user agent applies these defaults for optional
   *     attributes that are missing:
   *  
   *     Discard [Set-Cookie2 only] The default behavior is dictated by the
   *             presence or absence of a Max-Age attribute.
   *  
   *     Version [Set-Cookie] Defaults to Version 0 behavior as originally
   *             specified by Netscape.
   *             [Set-Cookie2] No default; the version MUST be present.
   *  
   *     Domain  Defaults to the effective request-host.  (Note that because
   *             there is no dot at the beginning of effective request-host,
   *             the default Domain can only domain-match itself.)
   *  
   *     Max-Age The default behavior is to discard the cookie when the user
   *             agent exits.
   *  
   *     Path    [Set-Cookie] Defaults to the path of the request URL that
   *             generated the Set-Cookie response, up to, but NOT including,
   *             the right-most /.
   *             [Set-Cookie2] Defaults to the path of the request URL that
   *             generated the Set-Cookie2 response, up to AND including the
   *             right-most /.
   *  
   *     Port    The default is null, signifying that the cookie MAY be returned
   *             to any request-port.
   *  
   *     Secure  If absent, the user agent MAY send the cookie over an insecure
   *             channel.
   *  
   *  To prevent possible security or privacy violations, a user agent
   *  rejects a cookie (SHALL NOT store its information) according to rules
   *  below (as set forth for Set-Cookie in RFC 2109 Section 4.3.2 and for
   *  Set-Cookie2 in RFC 2965 Section 3.3.2).  The goal of the rules is to
   *  try to limit the set of servers for which a cookie is valid, based on the
   *  values of the Path, Domain, and [Set-Cookie2 only] Port attributes and the
   *  request-URI, request-host and request-port.
   *  
   *    *  [Set-Cookie2 only] The Version attribute is missing.
   *       For Set-Cookie, a missing Version attribute defaults to 0.
   *   
   *    *  The value for the Path attribute is not a prefix of path portion
   *       of the request URL.
   *  
   *    *  [Set-Cookie] The value for the Domain attribute contains no
   *       embedded dots or does not start with a dot.
   *       [Set-Cookie2] The value for the Domain attribute contains no
   *       embedded dots and is not .local.
   *  
   *    *  The effective host name that derives from the request-host does
   *       not domain-match the Domain attribute.
   *  
   *    *  The request-host is a HDN (not IP address) and has the form HD,
   *       where D is the value of the Domain attribute, and H is a string
   *       that contains one or more dots.
   *  
   *    *  [Set-Cookie2 only] The Port attribute has a "port-list", and the
   *       request-port was not in the list. The exception to this is an empty
   *       Port attribute, which defaults to the request-port.
   *
   *  For efficiency, this filtering should be incorporated into the makeCookies()
   *  method. It should not make a cookie if
   *
   *    *  [Set-Cookie2 only] The Version attribute is missing.
   *       However, for Set-Cookie, a missing Version attribute defaults to 0;
   *
   *    *  The path argument non-null and does not path-match the candidate
   *       cookie's Path attribute;
   *
   *    *  The domain argument is non-null and does not domain-match the
   *       candidate cookie's Domain attribute;
   *
   *    *  The port argument is non-null and does not port-match the candidate
   *       cookie's Port attribute. However, if the candidate's Port attribute
   *       is empty (but non-null), it is set to the port argument.
   *
   *  See voxware.net.cookies.Cookie for definitions of the matching methods.
   *  
   *****************************************************************************/
  public CookieSet makeCookies(String type, String recipe, String path, String domain, int[] port);
}
