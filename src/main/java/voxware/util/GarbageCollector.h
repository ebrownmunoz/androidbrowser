#ifndef __voxware_util_GarbageCollector__
#define __voxware_util_GarbageCollector__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace util
    {
      class GarbageCollector;
    }
  }
}

class voxware::util::GarbageCollector : public ::java::lang::Object
{
private:
  GarbageCollector ();
public: // actually package-private
  virtual jboolean isCollectionImminent (jint);
public:
  static ::voxware::util::GarbageCollector *getInstance ();
  virtual void run ();
  virtual void collect ();
  virtual void collectInBackground ();
  virtual void collect (jboolean);
  virtual jboolean collect (jint);
  virtual jboolean collect (jint, jlong);
  virtual jboolean collect (jint, jboolean);
  virtual jboolean collect (jlong, jlong);
  virtual jboolean collectInBackground (jlong, jlong);
  virtual jboolean collect (jlong, jlong, jboolean);
  virtual jboolean collect (jint, jlong, jlong);
  virtual jboolean collectInBackground (jint, jlong, jlong);
  virtual jboolean collect (jint, jlong, jlong, jboolean);
  virtual jboolean countAndCollect ();
  virtual jboolean countAndCollectInBackground ();
  virtual jboolean countAndCollect (jboolean);
private:
  void requestCollection (jboolean);
  void performCollection ();
public:
  static jint TriggerCount;
private:
  jboolean __attribute__((aligned(__alignof__( ::java::lang::Object ))))  collectable;
  static jboolean running;
  static ::voxware::util::GarbageCollector *garbageCollector;
  static jint count;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_util_GarbageCollector__ */
