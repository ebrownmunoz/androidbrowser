package voxware.util;

 // Access to the environment variables
public class Environment {

  private static boolean nativeLoaded = false;
  static {
    try {
      // LibraryLoader.loadLibrary("jutil");
      nativeLoaded = true;
    } catch(UnsatisfiedLinkError e) {
      SysLog.println("voxware.util.Environment: ERROR - jutil library does not exist");
    } catch (SecurityException e) {
      SysLog.println("voxware.util.Environment: ERROR - jutil library load fails due to: " + e.toString());
    }
  }

  private static native String  getenv(String name);
  private static native boolean setenv(String name, String value);

  /**------------------------------------------------------------*
   * Get the value of an environment variable
   *------------------------------------------------------------**/
  public static String getEnvironment(String name) {
    return nativeLoaded ? getenv(name) : null;
  }

  /**------------------------------------------------------------*
   * Set the value of an environment variable
   *------------------------------------------------------------**/
  public static boolean setEnvironment(String name, String value) {
    return nativeLoaded ? setenv(name, value) : false;
  }
}
