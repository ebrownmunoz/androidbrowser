package voxware.util;

import java.util.*;
	
 // A Cache
public class Cache extends Dictionary {

  public static final int    DEFAULT_MAX_ENTRIES = 20;
  public static final int    DEFAULT_SIZE_GUESS  = 10000;
  public static final double DEFAULT_LOAD_FACTOR = 0.65;

   // Inner static interface to generalize removal
  public static interface Remover {
    public void remove(Object element);
  }

  private Hashtable  map;
  private Entry      mru;         // The most recently used end of the linked age list
  private Entry      lru;         // The least recently used end of the linked age list
  private int        maxEntries;
  private long       totalBytes;
  private long       maxBytes;
  private long       maxAge;
  private int        sizeGuess;

  protected class Entry {
    private Object  key;
    private Object  element;
    private int     size;
    private long    birthTime;
    private long    maxAge;
    private Remover remover;
    private Entry   previous;
    private Entry   next;

    public Entry() {
      this(null, null, 0, 0, null);
    }

    public Entry(Object key, Object element, int size, long maxAge, Remover remover) {
      this.key = key;
      this.element = element;
      birthTime = System.currentTimeMillis();
      this.size = size;
      this.maxAge = maxAge;
      this.remover = remover;
    }

    public Object key() {
      return key;
    }

    public Object element() {
      return element;
    }

    public int size() {
      return this.size;
    }

    public long maxAge() {
      return this.maxAge;
    }

    public long age() {
      return System.currentTimeMillis() - birthTime;
    }

    public boolean dead() {
      return maxAge > 0 && age() > maxAge;
    }

    public Entry previous() {
      return previous;
    }

    public Entry next() {
      return next;
    }

    public void link(Entry previous, Entry next) {
      this.previous = previous;
      if (previous != null) previous.next = this;
      this.next = next;
      if (next != null) next.previous = this;
    }

    public void unlink() {
      if (previous != null) previous.next = next;
      if (next != null) next.previous = previous;
    }
  }

   // Inner helper class to support key enumeration
  protected class KeyEnumerator implements java.util.Enumeration {
    Entry entry;
    public KeyEnumerator() { entry = mru.next(); }
    public boolean hasMoreElements() { return entry != null; }
    public Object nextElement() {
      if (entry == null) throw new java.util.NoSuchElementException();
      Object key = entry.key();
      entry = entry.next();
      return key;
    }
  }

   // Inner helper class to support element enumeration
  protected class ElementEnumerator implements java.util.Enumeration {
    Entry entry;
    public ElementEnumerator() { entry = mru.next(); }
    public boolean hasMoreElements() { return entry != null; }
    public Object nextElement() {
      if (entry == null) throw new java.util.NoSuchElementException();
      Object element = entry.element();
      entry = entry.next();
      return element;
    }
  }

  public Cache() {
    this(DEFAULT_MAX_ENTRIES, Long.MAX_VALUE);
  }

  public Cache(long maxBytes) {
    this(DEFAULT_MAX_ENTRIES, maxBytes);
  }

  public Cache(int maxEntries) {
    this(maxEntries, Long.MAX_VALUE);
  }

  public Cache(int maxEntries, long maxBytes) {
     // Setting maxEntries < 1 turns off limits on the number of entries
    if (maxEntries < 1) {
      this.maxEntries = Integer.MAX_VALUE;
      maxEntries = DEFAULT_MAX_ENTRIES;
    } else {
      this.maxEntries = maxEntries;
    }
    map = new Hashtable((int)Math.ceil((double)maxEntries / DEFAULT_LOAD_FACTOR), (float)DEFAULT_LOAD_FACTOR);
     // Setting maxBytes < 1 turns off limits on the number of bytes
    this.maxBytes = (maxBytes < 1) ? Long.MAX_VALUE : maxBytes;
    maxAge = Long.MAX_VALUE;
    sizeGuess = DEFAULT_SIZE_GUESS;
     // Construct eternal null head and tail Entries to facilitate housekeeping
     // These do not go in the Hashtable, so they do not contribute to the size of the Cache
    mru = new Entry();
    lru = new Entry();
    mru.link(null, lru);
    lru.link(mru, null);
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.CACHE_MASK))
      SysLog.println("Cache: constructed a cache with maxBytes = " + this.maxBytes + " and maxEntries = "+ this.maxEntries);
  }

   // Set the maximum number of entries the cache can hold
  public void maxEntries(int maxEntries) {
     // Setting maxEntries < 1 turns off limits on number of entries
    this.maxEntries = (maxEntries < 1) ? Integer.MAX_VALUE : maxEntries;
    downSize();
  }

   // Return the maximum number of entries the cache can hold
  public int maxEntries() {
    return this.maxEntries;
  }

   // Set the maximum number of bytes the cache can hold
  public void maxBytes(long maxBytes) {
     // Setting maxBytes < 1 turns off limits on number of bytes
    this.maxBytes = (maxBytes < 1) ? Long.MAX_VALUE : maxBytes;
    downSize();
  }

   // Return the maximum number of bytes the cache can hold
  public long maxBytes() {
    return this.maxBytes;
  }

   // Set the maximum age of a cache Entry in milliseconds
  public void maxAge(long maxAge) {
     // Setting maxAge < 1 turns off limits on the age
    this.maxAge = (maxAge < 1) ? Long.MAX_VALUE : maxAge;
  }

   // Return the maximum age of a cache Entry in milliseconds
  public long maxAge() {
    return this.maxAge;
  }

   // Return the total number of bytes currently cached
  public long totalBytes() {
    return this.totalBytes;
  }

   // Set the size to assign to entries whose size is unknown
  public void sizeGuess(int sizeGuess) {
    this.sizeGuess = sizeGuess;
  }

   // Return the size assigned to entries whose size is unknown
  public int sizeGuess() {
    return this.sizeGuess;
  }


  // Dictionary Implementation

   // Enumerate the elements in the Cache in ascending order by age
  public Enumeration elements() {
    return new ElementEnumerator();
  }

   // Return the element corresponding to the given key, or null if none or expired
   // Acquiring an element gives it MRU status
  public synchronized Object get(Object key) {
    Object element = null;
    if (key != null) {
      Entry entry = (Entry)map.get(key);
      if (entry != null) {
        if (!entry.dead() && entry.age() <= maxAge) {
          element = entry.element();
           // Make the entry the mru
          entry.unlink();
          entry.link(mru, mru.next());
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.CACHE_MASK))
            SysLog.println("Cache.get: got entry \"" + element.toString() + "\" for key \"" + key.toString() + "\"");
        } else {
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.CACHE_MASK))
            SysLog.println("Cache.get: entry for key \"" + key.toString() + "\" has expired");
          remove(entry.key());
        }
      } else {
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.CACHE_MASK))
          SysLog.println("Cache.get: no entry for key \"" + key.toString() + "\"");
      }
    } else {
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.CACHE_MASK))
        SysLog.println("Cache.get: null key");
    }
    return element;
  }

   // Report whether the cache is empty
  public boolean isEmpty() {
    return map.isEmpty();
  }

   // Enumerate the keys in ascending order by age
  public Enumeration keys() {
    return new KeyEnumerator();
  }

   // Insert an element into the cache, returning a reference to any element already present with the same key
   // The new entry has MRU status
  public synchronized Object put(Object key, Object value) { return put(key, value, sizeGuess, 0, null); }
  public synchronized Object put(Object key, Object value, int size) { return put(key, value, size, 0, null); }
  public synchronized Object put(Object key, Object value, long maxAge) { return put(key, value, sizeGuess, maxAge, null); }
  public synchronized Object put(Object key, Object value, Remover remover) { return put(key, value, sizeGuess, 0, remover); }
  public synchronized Object put(Object key, Object value, int size, long maxAge) { return put(key, value, size, maxAge, null); }
  public synchronized Object put(Object key, Object value, int size, Remover remover) { return put(key, value, size, 0, remover); }
  public synchronized Object put(Object key, Object value, long maxAge, Remover remover) { return put(key, value, sizeGuess, maxAge, remover); }
  public synchronized Object put(Object key, Object value, int size, long maxAge, Remover remover) {
    Object element = null;
    if (key != null) {
      Entry entry = new Entry(key, value, size, maxAge, remover);
      entry.link(mru, mru.next());
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.CACHE_MASK))
        SysLog.println("Cache.put: inserted entry \"" + value.toString() + "\" for key \"" + key.toString() + "\"");
      entry = (Entry)map.put(key, entry);
      totalBytes += size;
       // If the new entry replaces an old one, unlink the old
      if (entry != null) {
        entry.unlink();
         // If the new value is different from the old, and the old entry has a Remover, call remove() on it
        if (!entry.element().equals(value)) {
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.CACHE_MASK))
            SysLog.println("Cache.put: new entry for key \"" + key.toString() + "\" replaces old");
          if (entry.remover != null) entry.remover.remove(element);
        } else if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.CACHE_MASK)) {
          SysLog.println("Cache.put: new entry for key \"" + key.toString() + "\" is same as old");
        }
        totalBytes -= entry.size();
        element = entry.element();
      }
       // If the cache is now overfull, remove lrus
      downSize();
    }
    return element;
  }

   // Remove the element corresponding to a given key from the cache, returning it if not expired
  public synchronized Object remove(Object key) {
    Object element = null;
    if (key != null) {
      Entry entry = (Entry)map.remove(key);
      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.CACHE_MASK))
        SysLog.println("Cache.remove: removed entry for key \"" + key.toString() + "\"");
      if (entry != null) {
        if (!entry.dead() && entry.age() <= maxAge)
          element = entry.element();
        entry.unlink();
        if (entry.remover != null)
          entry.remover.remove(element);
        totalBytes -= entry.size();
      }
    }
    return element;
  }

   // Return the number of entries currently in the cache
  public int size() {
    return map.size();
  }

   // Cull the cache of superannuated elements
  public synchronized void cull() {
    Entry entry = mru.next();
    while (entry != null)
      if (entry.dead()) remove(entry.key());
  }

   // Remove lrus until the cache is no longer overfull
  private synchronized void downSize() {
    while (map.size() > maxEntries || totalBytes > maxBytes) {
      Entry entry = lru.previous();
      remove(entry.key());
    }
  }
}
