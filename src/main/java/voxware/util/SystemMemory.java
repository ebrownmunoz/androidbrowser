package voxware.util;

import java.io.FileInputStream;
import java.util.StringTokenizer;

 // Access to system memory statistics
public class SystemMemory {

  private static boolean nativeLoaded = false;
  static {
    try {
      // LibraryLoader.loadLibrary("jutil");
      nativeLoaded = true;
    } catch(UnsatisfiedLinkError e) {
      SysLog.println("voxware.util.SystemMemory: ERROR - jutil library does not exist");
    } catch (SecurityException e) {
      SysLog.println("voxware.util.SystemMemory: ERROR - jutil library load fails due to: " + e.toString());
    }
  }


  public long  totalBytes = 0;
  public long  bytesFree = 0;
  public int   percentUsed = 0;
  public long  maxFreeBlockSize = 0;
  public long  totalVirtualBytes = 0;
  public long  virtualBytesFree = 0;

  private void updateAllocationStats() {
	  Runtime rt = Runtime.getRuntime();
	  this.totalBytes = rt.totalMemory();
	  totalVirtualBytes = this.totalBytes;
	  this.bytesFree = rt.freeMemory();
	  this.virtualBytesFree = this.bytesFree;	  
  }

  /**
   * An instance of SystemMemory contains the memory statistics current at construction time
   * You cannot change or update the statistics except by constructing another instance
   */
  public SystemMemory() {

    /* The following system prop may or MAY NOT be implemented */
    String osname;
    if (((osname = System.getProperty("os.name")) != null) &&  osname.equalsIgnoreCase("linux")) {
        parseMemInfo();
    } else {
      updateAllocationStats();
    }
  }
    
  public long totalBytes() {
    return totalBytes;
  }

  public long bytesFree() {
    return bytesFree;
  }

  public int  percentUsed() {
    return percentUsed;
  }

  public long maxFreeBlockSize() {
    return maxFreeBlockSize;
  }

  public long totalPhysicalBytes() {
    return totalBytes;
  }

  public long physicalBytesFree() {
    return bytesFree;
  }

  public long totalVirtualBytes() {
    return totalVirtualBytes;
  }

  public long virtualBytesFree() {
    return virtualBytesFree;
  }

  private void parseMemInfo() {
    try {
      byte[] memInfoBytes = new byte[256];
      (new FileInputStream("/proc/meminfo")).read(memInfoBytes);                      // Throws FileNotFoundException, IOException
      StringTokenizer memInfo = new StringTokenizer(new String(memInfoBytes));
      while (memInfo.hasMoreTokens() && !memInfo.nextToken().equals("Mem:"));
      if (memInfo.hasMoreTokens()) totalBytes = Long.parseLong(memInfo.nextToken());  // Throws NumberFormatException
      if (memInfo.hasMoreTokens()) memInfo.nextToken();
      if (memInfo.hasMoreTokens()) bytesFree = Long.parseLong(memInfo.nextToken());   // Throws NumberFormatException
      if (totalBytes > 0) percentUsed = (int)(((totalBytes - bytesFree) * 100) / totalBytes);
      maxFreeBlockSize = bytesFree;
    } catch (Exception e) {
      SysLog.println("SystemMemory.parseMemInfo: WARNING - unexpected exception " + e.toString());
    }
  }
}
