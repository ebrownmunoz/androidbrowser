package voxware.util;

import java.io.UnsupportedEncodingException;

public class NISTHeaderField {

  private static int InitialStringSize = 80;

  private static final String  TypeDelimiter     = " -";
  private static final String  CommentDelimiter  = " ;";
  private static final char    IntegerType       = 'i';
  private static final char    RealType          = 'r';
  private static final char    StringType        = 's';
  private static final char    BlankCharacter    = ' ';

  private String  name;
  private Object  data;
  private String  comment;

  public NISTHeaderField(String name, Object data) {
    this(name, data, null);
  }

  public NISTHeaderField(String name, Object data, String comment) {
    this.name = name;
    this.data = data;
    this.comment = comment;
  }

   // Set the name
  public void name(String name) {
    this.name = name;
  }

   // Return the name
  public String name() {
    return this.name;
  }

   // Set the data
  public void data(Object data) {
    this.data = data;
  }

   // Return the data
  public Object data() {
    return this.data;
  }

   // Set the comment
  public void comment(String comment) {
    this.comment = comment;
  }

   // Return the comment
  public String comment() {
    return this.comment;
  }

   // Return a String representation of the field
  public String toString() {
    try { return toString("ISO-8859-1"); } catch (UnsupportedEncodingException uee) {}
    return null;
  }
  

   // Return a String representation of the field
  public String toString(String charsetName) throws UnsupportedEncodingException {
    StringBuffer buffer = new StringBuffer(InitialStringSize);
    String dataString = data.toString();
    if (data instanceof Integer)
      buffer.append(name).append(TypeDelimiter).append(IntegerType).append(BlankCharacter).append(dataString);
    else if (data instanceof Float)
      buffer.append(name).append(TypeDelimiter).append(RealType).append(BlankCharacter).append(dataString);
    else
      buffer.append(name).append(TypeDelimiter).append(StringType).append(dataString.getBytes(charsetName).length).append(BlankCharacter).append(dataString);
    if (comment != null)
      buffer.append(CommentDelimiter).append(comment);
    return buffer.toString();
  }

   // Return an image of the field as an array of bytes using the default encoding
  public byte[] bytes() {
    return this.toString().getBytes();
  }

   // Return an image of the field as an array of bytes using the specified encoding
  public byte[] bytes(String charsetName) throws UnsupportedEncodingException {
    return (charsetName != null) ? this.toString(charsetName).getBytes(charsetName) : bytes();
  }

   // Parse a String into a NISTHeaderField, returning null if the String is ill-formed
   // The String must be of the form: "<name> -t[length] <value>[ ;<comment>]", where <name> is an arbitrary sequence of
   // characters (not containing the sequence TypeDelimiter), t is one of the characters StringType, IntegerType or
   // RealType, length is a string representation of the integral length of the value field (optional), <value> is a
   // valid string representation of the value (not containing the String CommentDelimiter) and <comment> is any
   // sequence of characters.
  public static NISTHeaderField parse(String line) {
    NISTHeaderField field = null;
    int endOfName = line.indexOf(TypeDelimiter);
    if (endOfName > 0) {
      int  typeIndex = endOfName + TypeDelimiter.length();
      char typeChar = line.charAt(typeIndex);
      int  startOfValue = line.indexOf(BlankCharacter, typeIndex) + 1;
      if (startOfValue > 0) {
        int endOfValue = line.indexOf(CommentDelimiter, startOfValue);
        int startOfComment = -1;
        if (endOfValue < 0) endOfValue = line.length();
        else                startOfComment = endOfValue + CommentDelimiter.length();
        if (typeChar == StringType) {
          field = new NISTHeaderField(line.substring(0, endOfName), line.substring(startOfValue, endOfValue));
        } else if (typeChar == IntegerType) {
          field = new NISTHeaderField(line.substring(0, endOfName), new Integer(line.substring(startOfValue, endOfValue)));
        } else if (typeChar == RealType) {
          field = new NISTHeaderField(line.substring(0, endOfName), new Float(line.substring(startOfValue, endOfValue)));
        }
        if (startOfComment > 0 && field != null) field.comment(line.substring(startOfComment));
      }
    }
    return field;
  }
}
