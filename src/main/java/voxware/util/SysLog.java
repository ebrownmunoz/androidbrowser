package voxware.util;
	
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SysLog {

   // ALWAYS refer to PrintMasks symbolically, since the values may change

   // Browser PrintMasks
  public static final long AUDIOPLAYER_MASK       = 1L;                            // 0x00000000001
  public static final long CATCH_MASK             = AUDIOPLAYER_MASK << 1;         // 0x00000000002
  public static final long EXECUTABLECONTENT_MASK = CATCH_MASK << 1;               // 0x00000000004
  public static final long FILLED_MASK            = EXECUTABLECONTENT_MASK << 1;   // 0x00000000008
  public static final long FORM_MASK              = FILLED_MASK << 1;              // 0x00000000010
  public static final long GOTO_MASK              = FORM_MASK << 1;                // 0x00000000020
  public static final long GRAMMAR_MASK           = GOTO_MASK << 1;                // 0x00000000040
  public static final long HARDWARE_MASK          = GRAMMAR_MASK << 1;             // 0x00000000080
  public static final long INTERPRET_MASK         = HARDWARE_MASK << 1;            // 0x00000000100
  public static final long INTERRUPT_MASK         = INTERPRET_MASK << 1;           // 0x00000000200
  public static final long OBJECT_MASK            = INTERRUPT_MASK << 1;           // 0x00000000400
  public static final long PREPROCESS_MASK        = OBJECT_MASK << 1;              // 0x00000000800
  public static final long PROPERTY_MASK          = PREPROCESS_MASK << 1;          // 0x00000001000
  public static final long RADIOSTATUS_MASK       = PROPERTY_MASK << 1;            // 0x00000002000
  public static final long RECOGNIZER_MASK        = RADIOSTATUS_MASK << 1;         // 0x00000004000
  public static final long SCRIPT_MASK            = RECOGNIZER_MASK << 1;          // 0x00000008000
  public static final long SPEAKER_MASK           = SCRIPT_MASK << 1;              // 0x00000010000
  public static final long SUBDIALOG_MASK         = SPEAKER_MASK << 1;             // 0x00000020000
  public static final long VARIABLES_MASK         = SUBDIALOG_MASK << 1;           // 0x00000040000

   // JSAPI PrintMasks
  public static final long JSAPI_MASK             = 1L << 20;                      // 0x00000100000
  public static final long SAPIVISE_MASK          = JSAPI_MASK << 1;               // 0x00000200000
  public static final long KEYPAD_MASK            = SAPIVISE_MASK << 1;            // 0x00000400000

   // VJAP PrintMask
  public static final long VJAP_MASK              = 1L << 24;                      // 0x00001000000

   // VJML PrintMask
  public static final long VJML_MASK              = 1L << 25;                      // 0x00002000000

   // Utility PrintMasks
  public static final long CACHE_MASK             = 1L << 32;                      // 0x00100000000
  public static final long ALLOCATION_MASK        = CACHE_MASK << 1;               // 0x00200000000
  public static final long NISTHEADER_MASK        = ALLOCATION_MASK << 1;          // 0x00400000000
  public static final long PCFG_MASK              = NISTHEADER_MASK << 1;          // 0x00800000000
  public static final long URLCONNECTION_MASK     = PCFG_MASK << 1;                // 0x01000000000
  public static final long URLLOADER_MASK         = URLCONNECTION_MASK << 1;       // 0x02000000000
  public static final long UTILITY_MASK           = URLLOADER_MASK << 1;           // 0x04000000000
  public static final long XMLPARSER_MASK         = UTILITY_MASK << 1;             // 0x08000000000

   // Defaults
  public static final String DefaultDirectory = "/var/voxware/log";
  public static final String DefaultBaseName  = "syslog";
  public static final String DefaultExtension = "log";
  public static final int    DefaultMaxFiles  = 3;
  public static final int    DefaultMaxLines  = 1500;

   // If uninitialized, log to System.out without pruning
  public static PrintStream stream = System.out;

  public static int         printLevel;
  public static long        printMask;
  public static boolean     showStamp = false;

  public static String      directory = DefaultDirectory;
  public static String      baseName  = DefaultBaseName;
  public static String      extension = DefaultExtension;
  public static int         maxFiles  = DefaultMaxFiles;
  public static int         maxLines  = Integer.MAX_VALUE;

  private static int numLines = 0;
  private static int numFiles = 0;
  
   // Timestamp Formatting
  private static final String DEFAULT_DATE_TIME_FORMAT = "HH:mm:ss:SSS";  
  private static String dateTimeFormat = DEFAULT_DATE_TIME_FORMAT;
  private static DateFormat dateFormatter;

  static { dateFormatter = new SimpleDateFormat(dateTimeFormat); }

  private static void checkStream() {
    if (stream == null || numLines > maxLines) {
      numLines = 0;

      FileOutputStream fos = null;
      String filePath = directory + "/" + baseName + "." + numFiles + "." + extension;
      try {
        fos = new FileOutputStream(filePath);
      } catch (FileNotFoundException fnfe) {
        String error = "SysLog FATAL ERROR: could not create \"" + filePath + "\"";
        if (stream != null) stream.println(error);
        else System.err.println(error);
        System.exit(1);
      }
      if (stream != null) {
        stream.println("Closing this logfile; opening new logfile \"" + filePath + "\"");
        stream.close();
      }
      stream = new PrintStream(fos);
      
       // Retain maxFiles
      int oldest = numFiles - (maxFiles + 1);
       // Retain the zeroth segment to preserve access to the startup parameters and version number
      if (oldest >= 1) {
        String oldFilePath = directory + "/" + baseName + "." + oldest + "." + extension;
        File oldestFile = new File(oldFilePath);
        oldestFile.delete();
        stream.println("Deleted oldest retained logfile \"" + oldFilePath + "\"");
      }
      numFiles++;
    }
  }

  public static void initialize(PrintStream ps) {
    stream = (ps == null) ? System.out : ps;
    maxLines = Integer.MAX_VALUE;
  }

  public static void initialize(String dir, String name, String ext) {
    stream = null;
    directory = (dir == null) ? DefaultDirectory : dir;
    baseName = (name == null) ? DefaultBaseName : name;
    extension = (ext == null) ? DefaultExtension : ext;
    maxFiles = DefaultMaxFiles;
    maxLines = DefaultMaxLines;
  }

  public static void initialize(String path, String defaultBaseName) {
     // Parse the pathname
    java.io.File file = (new java.io.File(path)).getAbsoluteFile();
    String directory = null;
    String baseName = defaultBaseName;
    String extension = null;
    if (file.isDirectory()) {
      directory = file.getPath();
    } else {
      directory = file.getParent();
      String fileName = file.getName();
      int dot = fileName.lastIndexOf(".");
      if (dot < 0) {
        baseName = fileName;
      } else {
        extension = fileName.substring(dot + 1);
        if (dot > 0) baseName = fileName.substring(0, dot);
      }
    }
    SysLog.initialize(directory, baseName, extension);
  }

  public static void initialize(String path) {
    initialize(path, null);
  }

  public static void setMaxFiles(int mf)   { maxFiles = mf; }
  public static int  getMaxFiles()         { return maxFiles; }

  public static void setMaxLines(int ml)   { maxLines = ml; }
  public static int  getMaxLines()         { return maxLines; }

  public static void setPrintLevel(int pl) { printLevel = pl; }
  public static int  getPrintLevel()       { return printLevel; }

  public static void setPrintMask(long pm) { printMask = pm; }
  public static long getPrintMask()        { return printMask; }

  public static void setPrintMaskBits(long pm) { printMask |= pm; }

  public static void clearPrintMaskBits(long pm) { printMask &= ~pm; }

  public static boolean printMaskBitsSet(long pm) { return (printMask & pm ^ pm) == 0; }

  public static boolean printMaskBitsClear(long pm) { return (printMask & pm) == 0; }

  public static void    setShowStamp()           { showStamp = true; }
  public static void  clearShowStamp()           { showStamp = false; }
  public static void    setShowStamp(boolean st) { showStamp = st; }
  public static boolean getShowStamp()           { return showStamp; }

  public static boolean shouldLog(int prio, long pm) { return prio <= printLevel && printMaskBitsSet(pm); }
  public static boolean shouldLog(int prio)          { return prio <= printLevel; }
  public static boolean shouldLog(long pm)           { return printMaskBitsSet(pm); }

   /// Print methods

  public static synchronized void print(Object o) {
    checkStream();
    stream.print(o);
  }

  public static synchronized void print(int prio, Object o) {
    if (prio <= printLevel) {
      checkStream();
      stream.print(o);
    }
  }

  public static synchronized void print(String str) {
    checkStream();
    stream.print(str);
  }

  public static synchronized void print(int prio, String str) {
    if (prio <= printLevel) {
      checkStream();
      stream.print(str);
    }
  }

   /// Println methods

  public static synchronized void println() {
    checkStream();
    stream.println("");
    ++numLines;
  }

  public static synchronized void println(Object o) {
    checkStream();
    if (showStamp) stream.println(dateFormatter.format(new Date()) + "[] " + o);
    else           stream.println(o);
    ++numLines;
  }

  public static synchronized void printlnNoStamp(Object o) {
    checkStream();
    stream.println(o);
    ++numLines;
  }

  public static synchronized void println(Object o, boolean stamp) {
    checkStream();
    if (stamp && showStamp) stream.println(dateFormatter.format(new Date()) + "[] " + o);
    else                    stream.println(o);
    ++numLines;
  }

  public static synchronized void println(int prio, Object o) {
    if (prio <= printLevel) {
      checkStream();
      if (showStamp) stream.println(dateFormatter.format(new Date()) + " [" + prio + "] " + o);
      else           stream.println(o);
      ++numLines;
    }
  }

  public static synchronized void println(String str) {
    checkStream();
    if (showStamp) stream.println(dateFormatter.format(new Date()) + "[] " + str);
    else           stream.println(str);
    ++numLines;
  }

  public static synchronized void printlnNoStamp(String str) {
    checkStream();
    stream.println(str);
    ++numLines;
  }

  public static synchronized void println(String str, boolean stamp) {
    checkStream();
    if (stamp && showStamp) stream.println(dateFormatter.format(new Date()) + "[] " + str);
    else                    stream.println(str);
    ++numLines;
  }

  public static synchronized void println(int prio, String str) {
    if (prio <= printLevel) {
      checkStream();
      if (showStamp) stream.println(dateFormatter.format(new Date()) + " [" + prio + "] " + str);
      else           stream.println(str);
      ++numLines;
    }
  }

   // Print a list of the active threads
  public static void printThreads(String prefix) {
    checkStream();
    Thread[] threads = new Thread[Thread.activeCount() + 5];
    int numThreads = Thread.enumerate(threads);
    StringBuffer threadList = new StringBuffer(prefix != null ? prefix : "").append("Active Threads:\n");
    for (int index = 0; index < threads.length && index < numThreads; index++) {
      if (threads[index] != null) threadList.append(" [").append(index).append("] ").append(threads[index].getName()).append('\n');
    }
    stream.print(threadList.toString());
    numLines += numThreads + 1;
  }
}
