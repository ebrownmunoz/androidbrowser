package voxware.util;

import java.io.Serializable;
import java.util.*;

 /** An ObjectSet
   * This is a Java 1.1 implementation of the Java 1.2 Set interface
   * A Set is an unordered collection of distinct objects.  This implementation,
   * though based on Vector, is not an extension of Vector, since that would make
   * it too easy to violate the uniqueness criterion on elements. Besides, Vector
   * exports many methods not in the Set interface */

public class ObjectSet implements Serializable {

  private Vector set;

  public ObjectSet() {
    set = new Vector();
  }

  protected ObjectSet(int capacity) {
    set = new Vector(capacity);
  }

  public ObjectSet(ObjectSet set) {
    this(set.size());
    this.addAll(set);
  }

   // Add the argument element to the set if it is not already there, returning true iff this changes the set
  public synchronized boolean add(Object element) {
    if (!set.contains(element)) {
      set.addElement(element);
      return true;
    } else {
      return false;
    }
  }

   // Add to the set all of the elements in the argument set that are not already there, returning true iff this changes the set
  public synchronized boolean addAll(ObjectSet set) {
    boolean changed = false;
    Enumeration elements = set.iterator();
    while (elements.hasMoreElements())
      changed = this.add(elements.nextElement()) || changed;
    return changed;
  }

   // Empty the set
  public void clear() {
    set.removeAllElements();
  }

   // Return true iff the set contains the argument element
  public boolean contains(Object element) {
    return set.contains(element);
  }

   // Return true iff the argument set is a subset of the set
  public synchronized boolean containsAll(ObjectSet set) {
    boolean containsAll = true;
    Enumeration elements = set.iterator();
    while (containsAll && elements.hasMoreElements())
      containsAll = this.set.contains(elements.nextElement());
    return containsAll;
  }

   // Return true iff there are no elements in the set
  public boolean isEmpty() {
    return set.isEmpty();
  }

   // Enumerate the elements in the set
  public Enumeration iterator() {
    return set.elements();
  }

   // Remove the argument element, returning true iff this changes the set
  public boolean remove(Object element) {
    return set.removeElement(element);
  }

   // Remove all the elements shared with the argument, returning true iff this changes the set
  public synchronized boolean removeAll(ObjectSet set) {
    boolean changed = false;
    Enumeration elements = set.iterator();
    while (elements.hasMoreElements())
      changed = this.set.removeElement(elements.nextElement()) || changed;
    return changed;
  }

   // Retain only the elements shared with the argument, returning true iff this changes the set
  public synchronized boolean retainAll(ObjectSet set) {
    ObjectSet unshared = new ObjectSet();
    Enumeration elements = this.set.elements();
    while (elements.hasMoreElements()) {
      Object element = elements.nextElement();
      if (!set.contains(element)) unshared.add(element);
    }
    return removeAll(unshared);
  }

   // Return the number of elements in the set
  public int size() {
    return set.size();
  }

   // Create an array of the elements in the set
  public synchronized Object[] toArray() {
    Object[] array = new Object[set.size()];
    set.copyInto(array);
    return array;
  }

   // Fill in an array with the elements in the set, creating a larger array if necessary and null-terminating it if possible
  public synchronized Object[] toArray(Object[] array) {
    int size = set.size();
    if (array.length < size) array = new Object[size];
    set.copyInto(array);
    if (array.length > size) array[size] = null;
    return array;
  }
}
