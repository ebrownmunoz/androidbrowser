package voxware.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Hashtable;

/**
 * A class of static methods for loading and initializing dynamic libraries.
 *
 * Call LibraryLoader.loadLibrary() in place of System.loadLibrary()
 */
public class LibraryLoader {

  /**
   * The FoundationLibrary, if any, is loaded before all other dynamically loaded libraries.
   * In common with all dynamically loaded libraries that require some initialization,
   * the FoundationLibrary must export a one-argument static boolean initialization method for
   * LibraryLoader to call when it loads the FoundationLibrary. If any initialization must occur
   * to set the stage for other libraries to load and run properly, it must occur in
   * FoundationLibrary's initialization method. If the FoundationLibrary does not export such
   * a method, LibraryLoader assumes that no such initialization is required.
   *
   * An initialization method's single argument is a String specifying the name of the library's
   * logfile, that is, the file to which the library should log messages. In the case of the
   * FoundationLibrary, you can override the logfile name by defining an environment variable,
   * BASELIB_LOGPATH. This definition takes precedence over all other means of specifying
   * the logfile.
   */
  public static final String FoundationLogFilePropertyName = "voxware.util.logfile";
  public static final String FoundationLogFileDefaultName = "jutil.log.txt";
  public static final String FoundationLibraryName = null;
  public static final String FoundationLibaryInitializer = "voxware.util.Initializer.initialize";

  /**
   * The default logfile name is "/temp/<libraryName>.log". To override this, call the three-argument
   * version of LibraryLoader.loadLibrary with the desired file name, or define a System property
   * named "voxware.<libraryName>.logfile". E.g., putting "-Dvoxware.foolib.logfile=/oblivion/claptrap.fog"
   * on the Java command line will redirect library foolib's output from /temp/foolib.log to
   * /oblivion/claptrap.fog. Similarly, "-Dvoxware.foolib.logfile=stdout" redirects it to stdout.
   */
  public static final String DefaultLogFileDirectory = "/temp/";
  public static final String DefaultLogFileExtension = ".log.txt";

   // A Hashtable of the names of all successfully loaded libraries hashed by name
  private static Hashtable libraries = new Hashtable();

  public static String foundationLogFileName = FoundationLogFileDefaultName;

  static {
    if (FoundationLibraryName != null) {
      if (!foundationLogFileName.startsWith("/") && !foundationLogFileName.startsWith("\\")) foundationLogFileName = DefaultLogFileDirectory + foundationLogFileName;
      foundationLogFileName = System.getProperty(FoundationLogFilePropertyName, foundationLogFileName);
      if (!loadLibrary(FoundationLibraryName, FoundationLibaryInitializer, foundationLogFileName))
        SysLog.println("LibraryLoader: ERROR - foundation library \"" + FoundationLibraryName + "\" NOT loaded");
    } else {
      SysLog.println("LibraryLoader: INFO - no foundation library specified");
    }
  }

  /**
   * Load the given library without an initializer and without a logfile
   * @param libraryName the name of the library to load
   *
   * @return true iff the library loads successfully
   */

  public static boolean loadLibrary(String libraryName) {
    return loadLibrary(libraryName, null, null);
  }

  /**
   * Load and initialize the given library to log to its default logfile
   * @param libraryName the name of the library to load
   * @param initializerName the fully specified name of the initialization method, or null if none
   *
   * @return true iff the library loads successfully
   */
  public static boolean loadLibrary(String libraryName, String initializerName) {
    return loadLibrary(libraryName, initializerName, DefaultLogFileDirectory + libraryName + DefaultLogFileExtension);
  }

  /**
   * Load and initialize the given library to log to the specified logfile.
   *
   * Every library that requires initialization must export a one-argument static String initialization method
   * for LibraryLoader.loadLibrary() to call. The initializer's single argument is a String specifying the name
   * of the logfile; if the argument is null, the library should log to stdout. The initializer returns a String
   * specifying the actual logfile, which may differ from that specified (see above).
   *
   * If no initialization method is specified, LibraryLoader assumes that no initialization is required.
   *
   * @param libraryName the name of the library to load
   * @param initializerName the fully specified name of the initialization method, or null if none
   * @param logFileName the name of the log file for this library
   *
   * @return true iff the library loads and initializes successfully
   */
  public static boolean loadLibrary(String libraryName, String initializerName, String logFileName) {
    boolean success = true;
    if (libraries.put(libraryName, libraryName) == null) {
      success = false;
      try {
         // Load the library - noop for gcj
        success = true;
         // If an initializer is specified, call it
        if (initializerName != null) {
          success = false;
          int dot = initializerName.lastIndexOf('.');
          if (dot < 0) {
            SysLog.println("LibraryLoader.loadLibrary: ERROR - initializer \"" + initializerName + "\" for \"" + libraryName + "\" has ill-formed name");
          } else {
            String initializerClassName = initializerName.substring(0, dot);
            String initializerMethodName = initializerName.substring(dot + 1);
            try {
              Method initializer = Class.forName(initializerClassName).getMethod(initializerMethodName, new Class[] { String.class });
               // A system property "voxware.<libraryName>.logfile" overrides the default logfile name
              String pLogFileName = System.getProperty("voxware." + libraryName + ".logfile");
              if (pLogFileName != null)
                logFileName = pLogFileName.equalsIgnoreCase("stdout") ? null : pLogFileName;
              if ((logFileName = (String)initializer.invoke(null, new Object[] { logFileName })) != null) {
                SysLog.println("LibraryLoader.loadLibrary: INFO - initializer \"" + initializerName + "\" for \"" + libraryName + "\" succeeds");
                if (logFileName != null) SysLog.println("LibraryLoader.loadLibrary: INFO - " + libraryName + " is logging to " + logFileName);
                success = true;
              } else {
                SysLog.println("LibraryLoader.loadLibrary: ERROR - initializer \"" + initializerName + "\" for \"" + libraryName + "\" fails");
              }
            } catch (NoSuchMethodException e) {
              SysLog.println("LibraryLoader.loadLibrary: ERROR - initializer \"" + initializerName + "\" for \"" + libraryName + "\" not found");
            } catch (InvocationTargetException e) {
              SysLog.println("LibraryLoader.loadLibrary: ERROR - exception thrown in initializer \"" + initializerName + "\" for \"" + libraryName + "\": " + e.getTargetException().toString());
            } catch (ClassNotFoundException e) {
              SysLog.println("LibraryLoader.loadLibrary: ERROR - initialization class \"" + initializerClassName + "\" for \"" + libraryName + "\" not found");
            } catch (Exception e) {
              SysLog.println("LibraryLoader.loadLibrary: ERROR - exception invoking the initializer \"" + initializerName + "\" for \"" + libraryName + "\": " + e.toString());
            }
          }
        } else {
          SysLog.println("LibraryLoader.loadLibrary: INFO - no initializer specified for \"" + libraryName + "\"");
        }
      } catch(UnsatisfiedLinkError e) {
        SysLog.println("LibraryLoader.loadLibrary: WARNING - library \"" + libraryName + "\" does not exist");
      } catch (SecurityException e) {
        SysLog.println("LibraryLoader.loadLibrary: WARNING - library \"" + libraryName + "\" failed to load due to: " + e.toString());
      }
    }

    return success;
  }
}
