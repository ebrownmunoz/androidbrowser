package voxware.util;

import java.io.Serializable;
import java.util.*;

 /** A HashVector
   * This is a Vector of objects, hashed by name, so you can find an object
   * quickly by name using get(String), or use the Vector methods to access
   * the objects in the order they were originally put() in. */

public class HashVector extends Vector implements Serializable {

  protected Hashtable nameHash;

  public HashVector() {
    super();
    nameHash = new Hashtable();
  }

  public HashVector(int initialCapacity) {
    super(initialCapacity);
    nameHash = new Hashtable(initialCapacity);
  }

   // If there is already an element by this name, return it and do nothing
  public Object put(String name, Object child) {
    Object old = nameHash.put(name, child);
    if (old != null)
      nameHash.put(name, old);  // Put the old one back
    else
      super.addElement(child);
    return old;
  }

  public Object get(String name) {
    return nameHash.get(name);
  }

  public Object remove(String name) {
    Object old = nameHash.remove(name);
    if (old != null) super.removeElement(old);
    return old;
  }

  public void removeContent(Object element) {
    if (super.removeElement(element)) {
       // Find the element in the Hashtable and remove it
      Enumeration keys = nameHash.keys();
      while (keys.hasMoreElements()) {
        String key = (String)keys.nextElement();
        if (nameHash.get(key) == element) {
          nameHash.remove(key);
          break;
        }
      }
    }
  }

  public Object removeYoungest() {
    Object youngest = null;
    try {
      youngest = super.lastElement();
      removeContent(youngest);
    } catch (NoSuchElementException e) {}
    return youngest;
  }

  public Object removeOldest() {
    Object oldest = null;
    try {
      oldest = super.firstElement();
      removeContent(oldest);
    } catch (NoSuchElementException e) {}
    return oldest;
  }

  public Enumeration keys() {
    return nameHash.keys();
  }

  public boolean containsKey(String name) {
    return nameHash.containsKey(name);
  }
} // class HashVector

