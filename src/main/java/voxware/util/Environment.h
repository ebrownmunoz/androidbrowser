#ifndef __voxware_util_Environment__
#define __voxware_util_Environment__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace util
    {
      class Environment;
    }
  }
}

class voxware::util::Environment : public ::java::lang::Object
{
private:
  static ::java::lang::String *getenv (::java::lang::String *);
  static jboolean setenv (::java::lang::String *, ::java::lang::String *);
public:
  static ::java::lang::String *getEnvironment (::java::lang::String *);
  static jboolean setEnvironment (::java::lang::String *, ::java::lang::String *);
  Environment ();
private:
  static jboolean nativeLoaded;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_util_Environment__ */
