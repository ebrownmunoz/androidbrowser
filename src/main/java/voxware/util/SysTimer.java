package voxware.util;
	
import java.io.*;

public class SysTimer {

  public static final int MAXIMUM_GRANULARITY = 10;

  public static PrintStream stream;
  public static int         granularity = 0;

  private static long splits[] = new long[MAXIMUM_GRANULARITY];
  
  public static void initialize(PrintStream printStream) {
    stream = printStream;
    granularity(1);
  }

  public static void granularity(int granularity) {
    if (granularity > MAXIMUM_GRANULARITY) granularity = MAXIMUM_GRANULARITY;
    long seed = (SysTimer.granularity == 0) ? System.currentTimeMillis() : splits[SysTimer.granularity - 1];
    for (int split = SysTimer.granularity; split < granularity; split++)
      splits[split] = seed;
    SysTimer.granularity = granularity;
  }

  public static int granularity() {
    return granularity;
  }

  public static void stamp(int split) {
    if (split < granularity) {
      long now = System.currentTimeMillis();
      long elapsed = now - splits[split];
      while (split < granularity)
        splits[split++] = now;
    }
  }

  public static void stamp(int split, String pre, String post) {
    if (split < granularity) {
      long now = System.currentTimeMillis();
      long elapsed = now - splits[split];
      stream.println(pre + elapsed + post);
      while (split < granularity)
        splits[split++] = now;
    }
  }
}