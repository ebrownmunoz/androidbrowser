#ifndef __voxware_util_Registrar__
#define __voxware_util_Registrar__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace util
    {
      class Registrar;
    }
  }
}

class voxware::util::Registrar : public ::java::lang::Object
{
private:
  static ::java::lang::Object *getreg (::java::lang::String *, ::java::lang::String *);
  static jboolean setreg (::java::lang::String *, ::java::lang::String *, ::java::lang::String *);
public:
  static ::java::lang::Object *getRegistry (::java::lang::String *, ::java::lang::String *);
  static jboolean setRegistry (::java::lang::String *, ::java::lang::String *, ::java::lang::String *);
  static ::java::lang::String *stringValue (::java::lang::Object *);
  Registrar ();
  static ::java::lang::String *ListSeparator;
private:
  static jboolean nativeLoaded;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_util_Registrar__ */
