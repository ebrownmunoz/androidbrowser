package voxware.util;

import java.io.UTFDataFormatException;

public class UTF8Support {

  /**
   * Utilities for converting between UTF-8 Strings and Unicode Strings
   */

   // Return the Unicode String representation of the given UTF-8 String
  public static String unicodeFromUTF8(String utf8) throws UTFDataFormatException {
    return appendUnicodeForUTF8(new StringBuffer(utf8.length()), utf8).toString();
  }

   // Append the Unicode String representation of the given UTF-8 String to the given StringBuffer
  public static StringBuffer appendUnicodeForUTF8(StringBuffer buffer, String utf8) throws UTFDataFormatException {
    int length = utf8.length();
    int unicode;
    for (int index = 0; index < length; index++) {
      int code = (int) utf8.charAt(index);
      if (code < 0x80) {
         // One-byte UTF-8 encoding
        unicode = code;
      } else if (code < 0xC0) {
        throw new UTFDataFormatException();
      } else if (code < 0xE0) {
         // Two-byte UTF-8 encoding
        unicode = ((code & 0x1F) << 6);
        code = (int) utf8.charAt(++index);
        if (code < 0x80 || code >= 0xC0) throw new UTFDataFormatException();
        else unicode = unicode | (code & 0x3F);
      } else {
         // Three-byte UTF-8 encoding
        unicode = ((code & 0xF) << 12);
        code = (int) utf8.charAt(++index);
        if (code < 0x80 || code >= 0xC0) throw new UTFDataFormatException();
        else unicode = unicode | ((code & 0x3F) << 6);
        code = (int) utf8.charAt(++index);
        if (code < 0x80 || code >= 0xC0) throw new UTFDataFormatException();
        else unicode = unicode | (code & 0x3F);
      }
      buffer.append((char) unicode);
    }
    return buffer;
  }

   // Return the UTF-8 String representation of the given Unicode String
  public static String utf8FromUnicode(String unicode) {
    return appendUTF8ForUnicode(new StringBuffer(unicode.length()), unicode).toString();
  }

   // Append the UTF-8 String representation of the given Unicode String to the given StringBuffer
  public static StringBuffer appendUTF8ForUnicode(StringBuffer buffer, String unicode) {
    int length = unicode.length();
    for (int index = 0; index < length; index++) {
      int code = (int) unicode.charAt(index);
      if (code < 0x80) {
         // One-byte UTF-8 encoding
        buffer.append((char) code);
      } else if (code < 0x800) {
         // Two-byte UTF-8 encoding
        buffer.append((char) ((code >> 6) & 0x1F | 0xC0));
        buffer.append((char) ((code) & 0x3F | 0x80));
      } else {
         // Three-byte UTF-8 encoding
        buffer.append((char) ((code >> 12) & 0x0F | 0xE0));
        buffer.append((char) ((code >> 6) & 0x3F | 0x80));
        buffer.append((char) ((code) & 0x3F | 0x80));
      }
    }
    return buffer;
  }

}
