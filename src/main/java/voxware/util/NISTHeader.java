package voxware.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
	
 // A NISTHeader
public class NISTHeader extends Hashtable {

  public static String CharsetName = "UTF-8";

  public  static final int     QuantumLength         = 8;

  private static final int     LabelFieldLength      = QuantumLength;    // MUST be QuantumLength
  private static final String  NISTLabel             = "NIST_1A";        // Length MUST be QuantumLength - 1;
  private static final int     LengthFieldOffset     = LabelFieldLength;
  private static final int     LengthFieldLength     = QuantumLength;    // MUST be QuantumLength
  private static final int     DefaultQuantaPerChunk = 128;
  private static final String  Terminator            = "end_head";
  private static final int     PaddingCharacter      = ' ';

  private int quantaPerChunk;  // The number of quanta in a header chunk (the header is written in chunks)
  private int length;          // The number of bytes in the header (as read or written)

   // Construct an empty NISTHeader
  public NISTHeader() {
    quantaPerChunk = DefaultQuantaPerChunk;
  }

   // Construct an empty NISTHeader with the given number of quanta per header chunk
  public NISTHeader(int quantaPerChunk) {
    if (quantaPerChunk < 1) quantaPerChunk = 1;
    this.quantaPerChunk = quantaPerChunk;
  }

   // If the given InputStream starts with a NIST header, return the header's length; otherwise, return zero
   // If successful, the stream pointer is left pointing to the start of the first header field
  public static int length(InputStream stream) throws IOException {
    byte[]  headerHeader = new byte[LabelFieldLength + LengthFieldLength];
    int     headerLength = 0;
    if (stream.read(headerHeader) == LabelFieldLength + LengthFieldLength)  // Throws IOException
      headerLength = length(headerHeader);
    return headerLength;
  }

   // If the given byte[] starts with a NIST header, return the header's length; otherwise, return zero
  public static int length(byte[] buffer) {
    int     headerLength = 0;
    if ((new String(buffer, 0, LabelFieldLength)).startsWith(NISTLabel) && buffer[LabelFieldLength - 1] == '\n') {
      String lengthString = new String(buffer, LengthFieldOffset, LengthFieldLength - 1);
      int    lengthStringStart = lengthString.lastIndexOf(PaddingCharacter);
      lengthStringStart = (lengthStringStart < 0) ? 0 : lengthStringStart + 1;
      headerLength = Integer.parseInt(lengthString.substring(lengthStringStart));
      if (headerLength <= 0 || headerLength % QuantumLength != 0) headerLength = 0;
    }
    return headerLength;
  }

   // Set the number of quanta in a header chunk
  public void quantaPerChunk(int quantaPerChunk) {
    if (quantaPerChunk < 1) quantaPerChunk = 1;
    this.quantaPerChunk = quantaPerChunk;
  }

   // Get the number of quanta in a header chunk
  public int quantaPerChunk() {
    return this.quantaPerChunk;
  }

   // Return the length of the header in bytes
  public int length() {
    return length;
  }

   // Read the header from an InputStream, returning true iff successful
   // If successful, the stream pointer is left pointing to the start of data following the well-formed header,
   // and length() will return the number of header bytes read from the stream
  public boolean read(InputStream stream) throws IOException {
    boolean success = false;
    length = length(stream);
    if (length > 0) {
      success = true;
      int bodyLength = length - LabelFieldLength - LengthFieldLength;
      if (bodyLength > 0) {
        byte[] headerBody = new byte[bodyLength];
        success = (stream.read(headerBody) == bodyLength && parse(headerBody)); // Throws IOException
      }
    }
    return success;
  }

   // Return a Vector of all the comment Strings in the header
  public Vector comments() {
    Vector comments = null;
    Enumeration fields = elements();
    while (fields.hasMoreElements()) {
      NISTHeaderField field = (NISTHeaderField)fields.nextElement();
      String comment = field.comment();
      if (comment != null) {
        if (comments == null) comments = new Vector();
        comments.addElement(comment);
      }
    }
    return comments;
  }

   // Change the name of a field in the header
  public boolean changeFieldName(String oldName, String newName) {
    boolean success = false;
    NISTHeaderField field = (NISTHeaderField)get(oldName);
    if (field != null) {
      remove(oldName);
      field.name(newName);
      put(newName, field);
      success = true;
    }
    return success;
  }

   // Add a field to the header, returning the previous one, if any
  public NISTHeaderField addField(String name, Object data) {
    NISTHeaderField field = new NISTHeaderField(name, data);
    return (NISTHeaderField)this.put(name, field);
  }

   // Stream all the fields in the header to a PrintStream
  public void print(PrintStream stream) {
    stream.println("The number of fields in the header is " + this.size());
    Enumeration fields = elements();
    while (fields.hasMoreElements()) {
      stream.println(((NISTHeaderField)fields.nextElement()).toString());
    }
  }

   // A private convenience class
  private class NISTHeaderOutputStream extends ByteArrayOutputStream {

    public NISTHeaderOutputStream(int initialCapacity) {
      super(initialCapacity);
    }

    public void writeLength() {
      String length = Integer.toString(count);
      int lengthSize = length.length();
      System.arraycopy(length.getBytes(), 0, buf, LengthFieldOffset + LengthFieldLength - lengthSize - 1, lengthSize);
    }

    public void write(byte[] bytes) {
      this.write(bytes, 0, bytes.length);
      this.write('\n');
    }

    public void fill(int fillChar, int length) {
      while (--length > 0) this.write(fillChar);
      if (length == 0) this.write('\n');
    }

    public void pad(int padChar, int quantum) {
      int remainder = count % quantum;
      if (remainder > 0) fill(padChar, quantum - remainder);
    }
  }

   // Return an image of the header as a ByteArrayOutputStream
   // After calling this, a call to length() will return the number of header bytes written 
  public ByteArrayOutputStream bytes() {
    NISTHeaderOutputStream stream = new NISTHeaderOutputStream(DefaultQuantaPerChunk * QuantumLength);
     // Stamp it with the NIST label
    stream.write(NISTLabel.getBytes());
     // Leave room for the length
    stream.fill(PaddingCharacter, LengthFieldLength);
    try {
       // Write the fields
      Enumeration fields = elements();
      while (fields.hasMoreElements()) stream.write(((NISTHeaderField)fields.nextElement()).bytes(CharsetName));
    } catch (UnsupportedEncodingException uee) {}
     // Terminate the header
    stream.write(Terminator.getBytes());
    stream.pad(PaddingCharacter, quantaPerChunk * QuantumLength);
     // Record its length
    stream.writeLength();
    length = stream.size();
    return stream;
  }

   // Parse the image of a header
  private boolean parse(byte[] body) {
    boolean success = true;
    try {
      int startOfLine = 0;
      int endOfLine = -1;
      while (startOfLine < body.length) {
        startOfLine = ++endOfLine;
        if (body.length - startOfLine <= Terminator.length()) return false;
        while (endOfLine < body.length && body[endOfLine] != '\n') endOfLine++;
        if (endOfLine == body.length) return false;
        if (endOfLine > startOfLine) {
          String line = new String(body, startOfLine, endOfLine - startOfLine, CharsetName);
          if (line.startsWith(Terminator)) return success;
          NISTHeaderField field = NISTHeaderField.parse(line);
          if (field != null) put(field.name(), field);
          else success = false;
        }
      }
    } catch (UnsupportedEncodingException uee) {
      success = false;
    }
    return success;
  }
}

