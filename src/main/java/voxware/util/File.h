#ifndef __voxware_util_File__
#define __voxware_util_File__

#pragma interface

#include <java/io/File.h>

extern "Java"
{
  namespace voxware
  {
    namespace util
    {
      class File;
    }
  }
}

class voxware::util::File : public ::java::io::File
{
private:
  jboolean performSetWriteable (::java::lang::String *, jboolean);
public:
  File (::voxware::util::File *, ::java::lang::String *);
  File (::java::lang::String *);
  File (::java::lang::String *, ::java::lang::String *);
  virtual jboolean copyTo (::voxware::util::File *);
  virtual jboolean copyTo (::voxware::util::File *, jboolean, jboolean);
  virtual jboolean delete$ ();
  virtual jboolean remove ();
  virtual ::java::lang::StringBuffer *list (jint, jboolean, jboolean, jboolean, ::java::lang::String *);
public:  // actually protected
  virtual void list (::java::lang::StringBuffer *, ::java::lang::String *, jint, jint, jint, jboolean, jboolean, jboolean);
  virtual jint maxNameLength (jint, jboolean);
public:
  virtual jboolean setWriteable (jboolean);
  static jint MaxDirectoryDepth;
  static jint MaxSizeFieldLength;
  static ::java::lang::String *NewLine;
  static ::java::lang::String *DirectorySeparator;
  static jint DirectorySeparatorLength;
private:
  static jboolean nativeLoaded;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_util_File__ */
