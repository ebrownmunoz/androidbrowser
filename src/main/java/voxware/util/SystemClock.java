package voxware.util;

import java.util.Date;

 // Access Windows CE system clock
public class SystemClock {

  private static native boolean getSystemTime(Date date);
  private static native boolean setSystemTime(Date date);

  private static boolean nativeLoaded = false;
  static {
    try {
      //LibraryLoader.loadLibrary("jutil");
      nativeLoaded = true;
    } catch(UnsatisfiedLinkError e) {
      SysLog.println("voxware.util.SystemClock: ERROR - jutil library does not exist");
    } catch (SecurityException e) {
      SysLog.println("voxware.util.SystemClock: ERROR - jutil library load fails due to: " + e.toString());
    }
  }

   // Get a Date representation of the current CE system time
  public static Date getTime() {
    Date date = new Date();
    if (getSystemTime(date)) {
      return date;
    } else {
      return null;
    }
  }

   // Set the CE system clock from the given Date
  public static boolean setTime(Date date) {
    return setSystemTime(date);
  }
}

