package voxware.util;

 // Access to the registry
public class Registrar {

  public static String ListSeparator = ", ";

  private static boolean nativeLoaded = false;
  static {
    try {
      //LibraryLoader.loadLibrary("jutil");
      nativeLoaded = true;
    } catch(UnsatisfiedLinkError e) {
      SysLog.println("voxware.util.Registrar: ERROR - jutil library does not exist");
    } catch (SecurityException e) {
      SysLog.println("voxware.util.Registrar: ERROR - jutil library load fails due to: " + e.toString());
    }
  }

  private static native Object  getreg(String key, String name);
  private static native boolean setreg(String key, String name, String value);

  /**------------------------------------------------------------*
   * Get the value of an entry in the registry
   *------------------------------------------------------------**/
  public static Object getRegistry(String key, String name) {
    return nativeLoaded ? getreg(key, name) : null;
  }

  /**------------------------------------------------------------*
   * Set the value of an entry in the registry
   *------------------------------------------------------------**/
  public static boolean setRegistry(String key, String name, String value) {
    return nativeLoaded ? setreg(key, name, value) : false;
  }

  /**------------------------------------------------------------*
   * Return the String representation of a registry value
   *------------------------------------------------------------**/
  public static String stringValue(Object value) {
    String stringValue = null;
    if (value != null) {
      if (value instanceof String) {
         // REG_SZ (1)
        stringValue = (String)value;
      } else if (value instanceof byte[]) {
         // REG_BINARY (3)
        byte[] bytes = (byte[])value;
        StringBuffer buffer = new StringBuffer(3 * bytes.length);
        for (int index = 0; index < bytes.length; index++) {
          if (index > 0) buffer.append(' ');
          String hex = Integer.toHexString((int)bytes[index] & 0xff);
          if (hex.length() == 1) buffer.append('0');
          buffer.append(hex);
        }
        stringValue = buffer.toString();
      } else if (value instanceof Integer) {
         // REG_DWORD (4)
        stringValue = ((Integer)value).toString();
      } else if (value instanceof String[]) {
         // REG_MULTI_SZ (4)
        String[] strings = (String[])value;
        StringBuffer buffer = new StringBuffer();
        for (int index = 0; index < strings.length; index++) {
          if (index > 0) buffer.append(ListSeparator);
          buffer.append(strings[index]);
        }
        stringValue = buffer.toString();
      } else {
        stringValue = value.toString();
      }
    }
    return stringValue;
  }

}
