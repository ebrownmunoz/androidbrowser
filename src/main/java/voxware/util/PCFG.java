package voxware.util;

import java.io.*;
/*
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
 */
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

public class PCFG {

  public static final String AssignmentOperator  = "=";       // The assignment operator
  public static final char   CommentCharacter = ';';          // Introduces comments in configuration files
  public static final char   NameTerminator = ':';            // Terminates names in configuration files

  public static final String DefaultQuoteCharacters = "\"\'"; // A list of the characters that can optionally delimit value fields in a config file
  public static final char   DefaultEscapeCharacter = '\\';   // A character to escape quotes in quoted strings in a config file
  public static final String ConfigFileEnv = "VOXWARE_PCFG";  // The name of the default environment variable containing the config file name
  public static final String ListSeparator = ", ";            // Separates strings in string lists

   // PCFG parameter types
  public static final int TypeSize = 8;
  public static final int Type     = 0xff;                    // The portion of the type field containing the type index
  public static final int NOTYPEP  = 0;
  public static final int CHARP    = NOTYPEP + 1;
  public static final int BYTEP    = CHARP + 1;
  public static final int SHORTP   = BYTEP + 1;
  public static final int INTP     = SHORTP + 1;
  public static final int LONGP    = INTP + 1;
  public static final int FLOATP   = LONGP + 1;
  public static final int DOUBLEP  = FLOATP + 1;
  public static final int STRP     = DOUBLEP + 1;
  public static final int DATEP    = STRP + 1;
  public static final int BOOLP    = DATEP + 1;
  public static final int TRUEP    = BOOLP + 1;
  public static final int FALSEP   = TRUEP + 1;
  public static final int HELPP    = FALSEP + 1;

   // PCFG Type Names
  public static final String[] TypeNames =
    { "NOTYPEP", "CHARP", "BYTEP", "SHORTP", "INTP", "LONGP", "FLOATP", "DOUBLEP", "STRP", "DATEP", "BOOLP", "TRUEP", "FALSEP", "HELPP" };

   // A six-bit subfield specifying the base of the number system to use when displaying the parameter
  public static final int BaseOffset = TypeSize;
  public static final int BaseSize = 6;
  public static final int Base     = 0x3f << BaseOffset;
  public static final int Octal    = 0x08 << BaseOffset;
  public static final int Decimal  = 0x0a << BaseOffset;
  public static final int Hex      = 0x10 << BaseOffset;

   // A one-bit subfield specifying whether the parameter is hidden
  public static final int VeilOffset = BaseSize + BaseOffset;
  public static final int VeilSize = 1;
  public static final int Veil     = 0x01 << VeilOffset;

   // A one-bit subfield specifying whether the parameter is a list
  public static final int ListOffset = VeilSize + VeilOffset;
  public static final int ListSize = 1;
  public static final int List     = 0x01 << ListOffset;

   // A one-bit subfield specifying whether the parameter is modifiable
  public static final int FixedOffset = ListSize + ListOffset;
  public static final int FixedSize = 1;
  public static final int Fixed     = 0x01 << FixedOffset;

   // The date/time formatter
/*public static DateFormat dateTime = DateFormat.getDateTimeInstance(); */

  /**------------------------------------------------------------*
   * Get the DateFormat
   *------------------------------------------------------------**/
/*public static DateFormat getDateFormat() {
    return dateTime;
  } */

  /**------------------------------------------------------------*
   * Return the Date as a date/time String
   *------------------------------------------------------------**/
/*public static String dateString(Date date) {
    return dateTime.format(date);
  } */

  /**------------------------------------------------------------*
   * Return the long as a date/time String
   *------------------------------------------------------------**/
/*public static String dateString(long date) {
    return dateTime.format(new Date(date));
  } */

  /**------------------------------------------------------------*
   * Decode a String as a long
   * This is a surrogate for Long.decode(), which is inexplicably
   * lacking in Java 1.1
   *------------------------------------------------------------**/
  public static Long decodeLong(String string) throws NumberFormatException {
    long result = 0;
    boolean negative = false;
    if (string != null) {
      string = string.trim();
      if (string.startsWith("-")) {
        negative = true;
        string = string.substring(1).trim();
      }
      if (string.startsWith("0x") || string.startsWith("0X"))
        result = Long.parseLong(string.substring(2), 16);     // Throws NumberFormatException
      else if (string.startsWith("0"))
        result = Long.parseLong(string, 8);                   // Throws NumberFormatException
      else
        result = Long.parseLong(string);                      // Throws NumberFormatException
    } else {
      throw new NumberFormatException("PCFG.decodeLong: null argument");
    }
    return new Long(negative ? -result : result);
  }

   // A PCFG Command Line Tokenizer
  public static class Tokenizer {

    private String[] tokens;

    public Tokenizer(String string) {
      StringTokenizer tokenizer = new StringTokenizer(string);
       // Collect all the tokens into a String[]
      Vector vector = new Vector(tokenizer.countTokens());
      while (tokenizer.hasMoreTokens()) {
        String token = tokenizer.nextToken();
         // If the next token begins with a quote character, assemble a compound token from the tokens in quotes
        if (token.startsWith("\"") || token.startsWith("\'")) {
          String quote = token.substring(0, 1);
          StringBuffer buffer = new StringBuffer(token.substring(1));
          while (tokenizer.hasMoreTokens()) {
            buffer.append(" ");
            token = tokenizer.nextToken();
            if (token.endsWith(quote)) {
              buffer.append(token.substring(0, token.length() - 1));
              break;
            } else {
              buffer.append(token);
            }
          }
          token = buffer.toString();
        }
        vector.addElement(token);
      }
      tokens = new String[vector.size()];
      vector.copyInto(tokens);
    }

     // Return an array of all the tokens
    public String[] tokens() {
        return tokens;
    }

     // Return an array of all but the first first tokens
    public String[] tokens(int first) {
      return tokens(first, tokens.length);
    }

     // Return an array of all but the first first and last (tokens.length - last) tokens
    public String[] tokens(int first, int last) {
      String[] subarray = null;
      if (0 <= first && first <= last && last <= tokens.length) {
        subarray = new String[last - first];
        if (first < tokens.length) System.arraycopy(tokens, first, subarray, 0, last - first);
      } else {
        subarray = new String[0];
      }
      return subarray;
    }

     // Return the nth token
    public String token(int n) {
      return (0 <= n && n < tokens.length) ? tokens[n] : null;
    }

     // Return the first token (normally the command name)
    public String command() {
      return (tokens.length > 0) ? tokens[0] : null;
    }

     // Return an array of all but the first token (normally the array of arguments)
    public String[] arguments() {
      return tokens(1, tokens.length);
    }
  }

   // A PCFG Assignment (a helper class to parse assignment strings ("target = source"))
  public static class Assignment {

    private String  rhs;
    private String  lhs;
    private boolean hasRHS;

    public Assignment(String assignment) {
      this(assignment, AssignmentOperator);
    }

    public Assignment(String assignment, String operator) {
      assignment = assignment.trim();
      int equals = assignment.indexOf(operator);
       // If there is no equals, the lhs is the string and the rhs is null
      if (equals < 0) {
        lhs = assignment;
        rhs = null;
        hasRHS = false;
       // Else, if the string ends in equals, the lhs is the string without the equals, and the rhs is the empty string
      } else if (equals == assignment.length() - 1) {
        lhs = assignment.substring(0, assignment.length() - 1).trim();
        rhs = "";
        hasRHS = false;
       // Otherwise, the lhs is everything before the equals, and the rhs is everything after
      } else {
        lhs = assignment.substring(0, equals).trim();
        rhs = assignment.substring(equals + operator.length()).trim();
        hasRHS = true;
      }
    }

    public String  rhs()    { return rhs; }
    public String  lhs()    { return lhs; }
    public boolean hasRHS() { return hasRHS; }
  }

   // A PCFG parameter
  public static class Parameter {

     // A PCFG parameter evaluator
    public interface Evaluator {
      public void evaluate(PCFG.Parameter parameter, Object value, boolean initial) throws PCFG.Parameter.EvaluationException;
    }

      // A catch-all Exception class for wrapping exceptions thrown in evaluators
    public static class EvaluationException extends Exception {
      private Throwable throwable;
      public EvaluationException(Throwable throwable) { this.throwable = throwable; }
      public Throwable throwable() { return this.throwable; }
    }

     // The default scalar parameter evaluator
    public static class ScalarEvaluator implements PCFG.Parameter.Evaluator {
      public void evaluate(PCFG.Parameter parameter, Object value, boolean initial) throws PCFG.Parameter.EvaluationException {
        parameter.value = value;
      }
    }

     // The default list parameter evaluator
    public static class ListEvaluator implements PCFG.Parameter.Evaluator {
      public void evaluate(PCFG.Parameter parameter, Object value, boolean initial) throws PCFG.Parameter.EvaluationException {
         // If the new value is not null, add it to the list, constructing the list if necessary
        if (value != null) {
          if (parameter.value == null) parameter.value = new Vector();
          ((Vector)parameter.value).addElement(value);
         // Else if the parameter is not configured, nullify it; otherwise, ignore it
        } else if (!parameter.configured()) {
          parameter.value = null;
        }
      }
    }

     // Default Evaluators (may be changed to change the default behavior of setValue())

    public static Evaluator DefaultScalarEvaluator = new ScalarEvaluator();
    public static Evaluator DefaultListEvaluator   = new ListEvaluator();

     // Fields

    public  String     name;                // Parameter name               (Obligatory)
    public  int        type;                // Parameter type               (Obligatory)
    public  Object     value;               // The value of the parameter   (Optional)
    public  String     description;         // Documentation string         (Optional)
    public  String     option;              // Command line option name     (Optional)
    protected int      minOptionLength;     // The length of the shortest unambiguous option name
    protected boolean  configured;          // True iff the parameter has been found on the command line or in a configuration file
    protected Evaluator evaluator;          // The interface whose evaluate() method assigns a value to the parameter

    /**------------------------------------------------------------*
     * Construct an uninitialized Parameter without a description,
     * whose name and command line option are the same
     *------------------------------------------------------------**/
    public Parameter(String name, int type) throws PCFG.Parameter.EvaluationException {
      this(name, type, null, name, null, null);
    }

    /**------------------------------------------------------------*
     * Construct an uninitialized Parameter with description
     * The description argument may be null
     *------------------------------------------------------------**/
    public Parameter(String name, int type, String description) throws PCFG.Parameter.EvaluationException {
      this(name, type, description, null, null, null);
    }

    /**------------------------------------------------------------*
     * Construct an uninitialized Parameter with description and evaluator class
     * The description and evaluator class arguments may be null
     *------------------------------------------------------------**/
    public Parameter(String name, int type, String description, Evaluator evaluator) throws PCFG.Parameter.EvaluationException {
      this(name, type, description, null, null, evaluator);
    }

    /**------------------------------------------------------------*
     * Construct an uninitialized Parameter with description and command line option
     * The description and option arguments may be null
     *------------------------------------------------------------**/
    public Parameter(String name, int type, String description, String option) throws PCFG.Parameter.EvaluationException {
      this(name, type, description, option, null, null);
    }

    /**------------------------------------------------------------*
     * Construct an uninitialized Parameter with description, command line option and evaluator class
     * All except the name and type arguments may be null
     *------------------------------------------------------------**/
    public Parameter(String name, int type, String description, String option, Evaluator evaluator) throws PCFG.Parameter.EvaluationException {
      this(name, type, description, option, null, evaluator);
    }

    /**------------------------------------------------------------*
     * Construct an initialized Parameter with description and command line option
     * All except the name and type arguments may be null
     *------------------------------------------------------------**/
    public Parameter(String name, int type, String description, String option, Object value) throws PCFG.Parameter.EvaluationException {
      this(name, type, description, option, value, null);
    }

    /**------------------------------------------------------------*
     * Construct an initialized Parameter with description, command line option and evaluator class.
     * All except the name and type arguments may be null
     *------------------------------------------------------------**/
    public Parameter(String name, int type, String description, String option, Object value, Evaluator evaluator) throws PCFG.Parameter.EvaluationException {
      if (name == null) throw new IllegalArgumentException("Parameter name is null");
      this.name = name;
      if ((type & Base) == 0) type = type | Decimal;          // The display base defaults to decimal
      this.type = type;
      if (evaluator != null)
        this.evaluator = evaluator;
      else
        this.evaluator = this.isList() ? DefaultListEvaluator : DefaultScalarEvaluator;
      this.description = description;
      this.option = option;
      this.minOptionLength = (option != null) ? option.length() : Integer.MAX_VALUE;
      this.setInitialValue(value);                            // Throws PCFG.Parameter.EvaluationException
    }

    /**------------------------------------------------------------*
     * Set the evaluator class
     *------------------------------------------------------------**/
    public void setEvaluator(Evaluator evaluator) {
      this.evaluator = evaluator;
    }

    /**------------------------------------------------------------*
     * Get the evaluator class
     *------------------------------------------------------------**/
    public Evaluator evaluator() {
      return this.evaluator;
    }

    /**------------------------------------------------------------*
     * Return the name of the parameter
     *------------------------------------------------------------**/
    public String name() {
      return name;
    }

    /**------------------------------------------------------------*
     * Return the type of the parameter
     *------------------------------------------------------------**/
    public int type() {
      return (type & PCFG.Type);
    }

    /**------------------------------------------------------------*
     * Return true iff the parameter is veiled
     *------------------------------------------------------------**/
    public boolean veiled() {
      return (type & PCFG.Veil) == PCFG.Veil;
    }

    /**------------------------------------------------------------*
     * Return true iff the parameter is a list type
     *------------------------------------------------------------**/
    public boolean isList() {
      return (type & PCFG.List) == PCFG.List;
    }

    /**------------------------------------------------------------*
     * Return true iff the parameter is unmodifiable
     *------------------------------------------------------------**/
    public boolean fixed() {
      return (type & PCFG.Fixed) == PCFG.Fixed;
    }

    /**------------------------------------------------------------*
     * Return the base of the display number system
     *------------------------------------------------------------**/
    public int base() {
      return (type & PCFG.Base) >> PCFG.BaseOffset;
    }

    /**------------------------------------------------------------*
     * Return the option name with optional characters in square
     * brackets
     *------------------------------------------------------------**/
    public String option() {
      if (option == null || minOptionLength >= option.length()) {
        return option;
      } else {
        StringBuffer buffer = new StringBuffer(option);
        buffer.insert(minOptionLength, '[').append(']');
        return buffer.toString();
      }
    }

    /**------------------------------------------------------------*
     * Return the description of the parameter
     *------------------------------------------------------------**/
    public String description() {
      return description;
    }

    /**------------------------------------------------------------*
     * Return the status of the parameter
     *------------------------------------------------------------**/
    public boolean configured() {
      return configured;
    }

    /**------------------------------------------------------------*
     * Return the value of the parameter
     *------------------------------------------------------------**/
    public Object value() {
      return value;
    }

    /**------------------------------------------------------------*
     * Return the value of the parameter as a string
     *------------------------------------------------------------**/
    public String stringValue() {

      String string;

      if (value != null) {
        if (!isList() || !(value instanceof Vector)) {
          string = stringValue(value);
        } else {
          StringBuffer buffer = new StringBuffer();
          Enumeration  elements = ((Vector)value).elements();
          if (elements.hasMoreElements()) {
            buffer.append(stringValue(elements.nextElement()));
            while (elements.hasMoreElements()) buffer.append(ListSeparator).append(stringValue(elements.nextElement()));
          }
          string = buffer.toString();
        }
      } else {
        string = "";
      }

      return string;
    }

    private String stringValue(Object object) {
      if (object instanceof Integer || object instanceof Long || object instanceof Byte || object instanceof Short) {
        StringBuffer result = new StringBuffer();
        int radix = base();
        if (radix == 10)      result.append("");
        else if (radix == 8)  result.append("0");
        else if (radix == 16) result.append("0x");
        else                  result.append(Integer.toString(radix)).append("r");
        return result.append(Long.toString(((Number)object).longValue(), radix)).toString();
   /* } else if (object instanceof Date) {
	  return dateTime.format((Date)object); */
      } else {
        return object.toString();
      }
    }

    /**------------------------------------------------------------*
     * Return the value of the parameter as a char
     *------------------------------------------------------------**/
    public char charValue() throws PCFG.Parameter.EvaluationException {
      if (value instanceof Character) {
        return ((Character)value).charValue();
      } else if (value instanceof Number) {
        return (char)((Number)value).longValue();
      } else {
        String string = stringValue();
        if (string.length() > 0) return string.charAt(0);
        else throw new PCFG.Parameter.EvaluationException(new NumberFormatException());
      } 
    }

    /**------------------------------------------------------------*
     * Return the value of the parameter as a byte
     *------------------------------------------------------------**/
    public byte byteValue() throws PCFG.Parameter.EvaluationException {
      if (value instanceof Number) {
        return ((Number)value).byteValue();
      } else if (value instanceof Character) {
        return (byte)((Character)value).charValue();
      } else try {
        return Byte.parseByte(stringValue());                 // Throws NumberFormatException
      } catch (NumberFormatException e) {
        throw new PCFG.Parameter.EvaluationException(e);
      }
    }

    /**------------------------------------------------------------*
     * Return the value of the parameter as a short
     *------------------------------------------------------------**/
    public short shortValue() throws PCFG.Parameter.EvaluationException {
      if (value instanceof Number) {
        return ((Number)value).shortValue();
      } else if (value instanceof Character) {
        return (short)((Character)value).charValue();
      } else try {
        return Short.parseShort(stringValue());               // Throws NumberFormatException
      } catch (NumberFormatException e) {
        throw new PCFG.Parameter.EvaluationException(e);
      }
    }

    /**------------------------------------------------------------*
     * Return the value of the parameter as an int
     *------------------------------------------------------------**/
    public int intValue() throws PCFG.Parameter.EvaluationException {
      if (value instanceof Number) {
        return ((Number)value).intValue();
      } else if (value instanceof Character) {
        return (int)((Character)value).charValue();
      } else try {
        return Integer.parseInt(stringValue());               // Throws NumberFormatException
      } catch (NumberFormatException e) {
        throw new PCFG.Parameter.EvaluationException(e);
      }
    }

    /**------------------------------------------------------------*
     * Return the value of the parameter as a long
     *------------------------------------------------------------**/
    public long longValue() throws PCFG.Parameter.EvaluationException {
      if (value instanceof Number) {
        return ((Number)value).longValue();
      } else if (value instanceof Character) {
        return (long)((Character)value).charValue();
   /* } else if (value instanceof Date) {
        return ((Date)value).getTime(); */
      } else try {
        return Long.parseLong(stringValue());                 // Throws NumberFormatException
      } catch (NumberFormatException e) {
        throw new PCFG.Parameter.EvaluationException(e);
      }
    }

    /**------------------------------------------------------------*
     * Return the value of the parameter as a float
     *------------------------------------------------------------**/
    public float floatValue() throws PCFG.Parameter.EvaluationException {
      if (value instanceof Number) {
        return ((Number)value).floatValue();
      } else if (value instanceof Character) {
        return (float)((Character)value).charValue();
      } else try {
     // return Float.parseFloat(stringValue());               // Throws NumberFormatException
     //  Float.parseFloat() does not exist in Java 1.1, so we use the following circumlocution:
        return Float.valueOf(stringValue()).floatValue();     // Throws NumberFormatException
      } catch (NumberFormatException e) {
        throw new PCFG.Parameter.EvaluationException(e);
      }
    }

    /**------------------------------------------------------------*
     * Return the value of the parameter as a double
     *------------------------------------------------------------**/
    public double doubleValue() throws PCFG.Parameter.EvaluationException {
      if (value instanceof Number) {
        return ((Number)value).doubleValue();
      } else if (value instanceof Character) {
        return (double)((Character)value).charValue();
      } else try {
     // return Double.parseDouble(stringValue());             // Throws NumberFormatException
     //  Double.parseDouble() does not exist in Java 1.1, so we use the following circumlocution:
        return Double.valueOf(stringValue()).doubleValue();   // Throws NumberFormatException
      } catch (NumberFormatException e) {
        throw new PCFG.Parameter.EvaluationException(e);
      }
    }

    /**------------------------------------------------------------*
     * Return the value of the parameter as a boolean
     *------------------------------------------------------------**/
    public boolean booleanValue() throws PCFG.Parameter.EvaluationException {
      if (value instanceof Boolean) {
        return ((Boolean)value).booleanValue();
      } else if (value instanceof Number) {
        return (((Number)value).doubleValue() != 0.0);
      } else try {
        return getBoolean(stringValue()).booleanValue();      // Throws IllegalArgumentException
      } catch (IllegalArgumentException e) {
        throw new PCFG.Parameter.EvaluationException(e);
      }
    }

    /**------------------------------------------------------------*
     * Return the value of the parameter as a Date
     *------------------------------------------------------------**/
/*  public Date dateValue() throws PCFG.Parameter.EvaluationException {
      if (value instanceof Date) {
        return (Date)value;
      } else try {
        return dateTime.parse(stringValue());                 // Throws ParseException
      } catch (ParseException e) {
        throw new PCFG.Parameter.EvaluationException(e);
      }
    } */

    /**------------------------------------------------------------*
     * Convert the argument to the appropriate type and assign it
     * as the value of the parameter. This does nothing unless the
     * parameter is unconfigured (has not yet received a value from
     * the command line or a configuration file, though it may have
     * a default value assigned by the constructor) or is an
     * extendable (non-fixed) list.
     *------------------------------------------------------------**/
    public void setValue(Object object) throws PCFG.Parameter.EvaluationException {
      setValue(object, false);                                // Throws PCFG.Parameter.EvaluationException
    }

    /**------------------------------------------------------------*
     * Convert the argument to the appropriate type and assign it
     * as the initial value of the parameter. This succeeds and
     * marks the parameter as configured even if it is fixed.
     *------------------------------------------------------------**/
    public void setInitialValue(Object object) throws PCFG.Parameter.EvaluationException {
      setValue(object, true);                                 // Throws PCFG.Parameter.EvaluationException
    }

    /**------------------------------------------------------------*
     * Convert the argument to the appropriate type and assign
     * it as the value of the parameter. If initializing, this
     * succeeds and marks the parameter as configured even if it
     * is fixed; otherwise, it works only if the parameter is
     * unconfigured (has not yet received a value from the
     * command line or a configuration file, though it may have
     * a default value assigned by the constructor) or is an
     * extendable (non-fixed) list.
     *------------------------------------------------------------**/
    private void setValue(Object object, boolean initial) throws PCFG.Parameter.EvaluationException {

       // Do nothing unless (a) initializing or (b) the parameter is not configured or (c) the parameter is an extendable list
      if (initial || !configured || isList() && !fixed()) {

        Object newValue;
        int    ptype = type();

        try {

           // If the new value object is null, just set the parameter to its undefaulted initial value
          if (object == null) {
            if      (ptype == BOOLP || ptype == TRUEP || ptype == HELPP) newValue = new Boolean(false);
            else if (ptype == FALSEP)                                    newValue = new Boolean(true);
            else                                                         newValue = null;

           // Otherwise, convert the object to a string and parse it to construct the appropriate type of value
          } else {
            String string = object.toString();
            switch (ptype) {
              case CHARP:
                newValue = new Character(string.charAt(0));
                break;
              case BYTEP:
                newValue = Byte.decode(string);               // Throws NumberFormatException
                break;
              case SHORTP:
                newValue = Short.decode(string);              // Throws NumberFormatException
                break;
              case INTP:
                newValue = Integer.decode(string);            // Throws NumberFormatException
                break;
              case LONGP:
             // newValue = Long.decode(string);               // Throws NumberFormatException
             // Inexplicably, Long.decode() is missing in Java 1.1, so we have to roll our own:
                newValue = PCFG.decodeLong(string);           // Throws NumberFormatException
                break;
              case FLOATP:
                newValue = new Float(string);                 // Throws NumberFormatException
                break;
              case DOUBLEP:
                newValue = new Double(string);                // Throws NumberFormatException
                break;
              case STRP:
                newValue = scanEnvironment(string);
                break;
              // case DATEP:
              // newValue = dateTime.parse(string);            // Throws ParseException
              // break;
              case BOOLP:
              case TRUEP:
              case FALSEP:
              case HELPP:
                newValue = getBoolean(string);                // Throws IllegalArgumentException
                break;
              default:
                if (SysLog.printLevel > 0) SysLog.println("PCFG.Parameter.setValue: unknown parameter type " + ptype);
                throw new IllegalArgumentException("Invalid PCFG parameter type " + ptype);
            }
          }
        } catch (Exception e) {
          throw new PCFG.Parameter.EvaluationException(e);
        }

         // Evaluate the parameter
        if (initial) value = null;
        this.evaluator.evaluate(this, newValue, initial);     // Throws PCFG.Parameter.EvaluationException
      }
       // Mark the parameter as configured unless this is the initial evaluation and the parameter is not fixed
      configured = !initial || fixed();
    }

    /**------------------------------------------------------------*
     * Convert the String argument to a Boolean
     *------------------------------------------------------------**/
    private Boolean getBoolean(String string) throws IllegalArgumentException {
      if (string.equalsIgnoreCase("on") || string.equalsIgnoreCase("1") ||
          string.equalsIgnoreCase("t") || string.equalsIgnoreCase("true") ||
          string.equalsIgnoreCase("y") || string.equalsIgnoreCase("yes"))
        return new Boolean(true);
      else
      if (string.equalsIgnoreCase("off") || string.equalsIgnoreCase("0") ||
          string.equalsIgnoreCase("f") || string.equalsIgnoreCase("false") ||
          string.equalsIgnoreCase("n") || string.equalsIgnoreCase("no"))
        return new Boolean(false);
      else
        throw new IllegalArgumentException("Invalid boolean value \"" + string + "\"");
    }

    /**------------------------------------------------------------*
     * Return a String detailing the command line option, optionally
     * showing its value and description, empty if option is null.
     * If equals is non-null and showValue is true, separate the
     * option and value strings with equals. If optionFieldLength is
     * greater than the length of the option name, right-justify the
     * option name in a field optionFieldLength characters long. If
     * showValue is true and valueFieldLength is greater the length
     * of the string representation of the value, left-justify the
     * value in a field valueFieldLength characters long. Otherwise,
     * just separate the fields with single spaces.
     *------------------------------------------------------------**/
    public String toString(int optionFieldLen, boolean showValue, int valueFieldLen, boolean showDescription, String equals) {
      StringFormatter.Field[] fields = {
        new StringFormatter.Field(-optionFieldLen, option(), equals),
        new StringFormatter.Field(valueFieldLen, showValue ? stringValue() : null, " "),
        new StringFormatter.Field(showDescription ? description : null)
      };
      return option() == null ? "" : new StringFormatter(fields).toString();
    }

    /**------------------------------------------------------------*
     * Return a String detailing the parameter, optionally showing
     * its command line option and description. Right-justify the
     * name in its field; left-justify the option and value in
     * their fields. If equals is non-null, prefix it to the value;
     * otherwise all separators are blanks.
     *------------------------------------------------------------**/
    public String toString(int nameFieldLen, boolean showOption, int optionFieldLen, int valueFieldLen, boolean showDescription, String equals) {
      StringFormatter.Field[] fields = {
        new StringFormatter.Field(-nameFieldLen, name, " "),
        new StringFormatter.Field(optionFieldLen, showOption ? (option() == null ? "" : option()) : null, equals),
        new StringFormatter.Field(valueFieldLen, stringValue(), " "),
        new StringFormatter.Field(showDescription ? description : null)
      };
      return new StringFormatter(fields).toString();
    }

    /**------------------------------------------------------------*
     * Return a String representation of the parameter
     *------------------------------------------------------------**/
    public String toString() {
      StringBuffer buffer = new StringBuffer(super.toString());
      buffer.append("[").append(name).append(ListSeparator);
      buffer.append(TypeNames[type()]);
      if (isList())     buffer.append(" | List");
      if (base() == 16) buffer.append(" | Hex");
      if (veiled())     buffer.append(" | Veiled");
      buffer.append(ListSeparator).append(stringValue()).append(ListSeparator);
      buffer.append(description == null ? "" : description).append(ListSeparator);
      buffer.append(option == null ? "" : option()).append(ListSeparator);
      buffer.append(configured ? "Configured" : "Unconfigured").append("]");
      return buffer.toString();
    }
  }

   // Fields
  private Parameter[]  parameters;
  private Hashtable    nameHash;
  private int[]        helps;                                     // Array of indices of HELPP parameters
  private int          maxNameLength;                             // The length of the longest parameter name
  private int          maxOptionLength;                           // The length of the longest option name (including brackets indicating optionality)
  private String[]     arguments;                                 // The command line
  private Vector       unknowns;                                  // Vector of indices of unidentifiable command line arguments
  private String       quoteCharacters = DefaultQuoteCharacters;
  private char         escapeCharacter = DefaultEscapeCharacter;


  /**------------------------------------------------------------*
   * Construct a PCFG from an array of PCFG parameters
   *-------------------------------------------------------------*/
  public PCFG(Parameter[] parameters) {

    Parameter parameter;
    int       length;
    Vector    help = new Vector();                            // Vector to accumulate the indices of HELPP type parameters

    this.parameters = parameters;
    unknowns = new Vector();
    nameHash = new Hashtable(parameters.length * 2);

     // Hash all the parameters by name
    for (int index = 0; index < parameters.length; index++) {
      parameter = parameters[index];
      parameter.configured = false;
      nameHash.put(parameter.name, parameter);
    }

     // Derive the global properties
    for (int index = 0; index < parameters.length; index++) {
      parameter = parameters[index];
      parameter.minOptionLength = ambiguityLength(index) + 1;
      length = (parameter.option != null) ? parameter.option().length() : 0;
      if (length > maxOptionLength) maxOptionLength = length;
      length = parameter.name.length();
      if (length > maxNameLength) maxNameLength = length;
      if (parameter.type() == HELPP) help.addElement(new Integer(index));
    }

     // Convert the Vector of indices of HELPP type parameters to an array
    helps = new int[help.size()];
    for (int index = 0; index < help.size(); index++)
      helps[index] = ((Integer)help.elementAt(index)).intValue();
  }

  /**------------------------------------------------------------*
   * Set the quote and escape characters to use in config files
   *------------------------------------------------------------**/
  public void setQuotes(String quoteCharacters, char escapeCharacter) {
    this.quoteCharacters = quoteCharacters;
    this.escapeCharacter = escapeCharacter;
  }

  /**------------------------------------------------------------*
   * Return the length of the longest parameter name
   *------------------------------------------------------------**/
  public int maxNameLength() {
    return maxNameLength;
  }

  /**------------------------------------------------------------*
   * Return the length of the longest option String, including
   * if present the square brackets that indicate optionality
   *------------------------------------------------------------**/
  public int maxOptionLength() {
    return maxOptionLength;
  }

  /**------------------------------------------------------------*
   * Return the PCFG.Parameter corresponding to the first HELPP
   * option on the command line, or null if none.
   *------------------------------------------------------------**/
  public PCFG.Parameter helpParameter(String argv[]) {
     // Search through the argument array, looking for a HELPP option
    for (int argi = 0; argi < argv.length; argi++)
      for (int hi = 0; hi < helps.length; hi++) {
        PCFG.Parameter parameter = parameters[helps[hi]];
        if (startsWith(parameter.option, argv[argi]))
          return parameter;
      }
    return null;
  }

  /**------------------------------------------------------------*
   * Evaluate the parameters by scanning the entire command line
   *------------------------------------------------------------**/
  public void scanCommandLine(String argv[]) throws PCFG.Parameter.EvaluationException {
    scanCommandLine(argv, 0, 0);
  }

  /**------------------------------------------------------------*
   * Evaluate the parameters by scanning the command line,
   * ignoring the first ignoreFirst elements and the last
   * ignoreLast elements
   *------------------------------------------------------------**/
  public void scanCommandLine(String argv[], int ignoreFirst, int ignoreLast) throws PCFG.Parameter.EvaluationException {

    int        argi;        // Argument index
    int        pi;          // Parameter index
    Parameter  parameter;

     // Retain a reference to the command line
    arguments = argv;

     // Search through the argument array, ignoring the specified number of leading and trailing arguments, looking for options and their values
    for (argi = ignoreFirst; argi < argv.length - ignoreLast; argi++) {

       // Find the first parameter whose option name starts with the arg string
      parameter = null;
      for (pi = 0; pi < parameters.length; pi++) {
        if (startsWith(parameters[pi].option, argv[argi])) {
           // Make sure there is no ambiguity
          if (argv[argi].length() >= parameters[pi].minOptionLength)
            parameter = parameters[pi++];
          break;
        }
      }

       // If we've found a uniquely matching unconfigured parameter, give it a value
      if (parameter != null) {
         // Assign inherent values to types that have them
        int ptype = parameter.type();
        if (ptype == TRUEP || ptype == HELPP) {
          parameter.setValue("TRUE");
        } else if (ptype == FALSEP) {
          parameter.setValue("FALSE");
        } else if (argi + 1 < argv.length - ignoreLast) {
           // See if the next item on the command line is an option name
          for (pi = 0; pi < parameters.length; pi++)
            if (startsWith(parameters[pi].option, argv[argi + 1])) break;
           // If not, consider it to be a value and assign it to the parameter
          if (pi == parameters.length) parameter.setValue(argv[++argi]);  // Throws PCFG.Parameter.EvaluationException
           // Otherwise, nullify the parameter, clearing its default value, if any
          else                         parameter.setValue(null);          // Throws PCFG.Parameter.EvaluationException
        } else {
           // As we haven't assigned a value to the parameter, nullify it, clearing its default value, if any
          parameter.setValue(null);                                       // Throws PCFG.Parameter.EvaluationException
        }

       // Otherwise, record the argument as unidentifiable
      } else {
        unknowns.addElement(new Integer(argi));
      }
    }
  }

  /**------------------------------------------------------------*
   * Evaluate the parameters by scanning a configuration file,
   * ignoring unrecognized parameters.
   *------------------------------------------------------------**/
  public void scanConfigFileSelectively(InputStream file) throws IOException, IllegalArgumentException, PCFG.Parameter.EvaluationException {
    scanConfigFile(file, true, null);
  }

  /**------------------------------------------------------------*
   * Evaluate the parameters by scanning a configuration file,
   * warning of unrecognized parameters.
   *------------------------------------------------------------**/
  public void scanConfigFile(InputStream file) throws IOException, IllegalArgumentException, PCFG.Parameter.EvaluationException {
    scanConfigFile(file, false, null);
  }

   // Scanning Constants
  private static final int EndOfFile = -1;

  /**------------------------------------------------------------*
   * Evaluate the parameters by scanning a configuration file,
   * optionally ignoring unrecognized parameters.
   *------------------------------------------------------------**/
  public void scanConfigFile(InputStream file, boolean ignore, String encoding) throws IOException, IllegalArgumentException, PCFG.Parameter.EvaluationException {

    int       character;
    Parameter parameter;
    String    valueString;

    PushbackReader reader = new PushbackReader((encoding == null) ? new InputStreamReader(file) : new InputStreamReader(file, encoding), 2);

    while (true) {

      parameter = parseName(reader, ignore);      // Throws IOException, IllegalArgumentException
      valueString = parseValue(reader);           // Throws IOException, IllegalArgumentException

      if (parameter != null) {
        int       ptype = parameter.type();
        if      (ptype == TRUEP || ptype == HELPP)  parameter.setValue("TRUE");
        else if (ptype == FALSEP)                   parameter.setValue("FALSE");
        else                                        parameter.setValue(valueString);  // Throws PCFG.Parameter.EvaluationException
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PCFG_MASK))
          SysLog.println("PCFG.scanConfigFile: " + parameter.toString());
      }

       // Skip to the end of the line
      while ((character = reader.read()) != '\n' && character != EndOfFile);
      if (character == EndOfFile) break;
    }
  }

  /**------------------------------------------------------------*
   * Parse a name string from the Reader and return the valid
   * Parameter for the name. If the name string is empty
   * or if it is unknown and ignore is specified, return null;
   * if it is unknown and ignore is not specified, throw an
   * IllegalArgumentException. Upon return, the Reader points to
   * the first character following the name or name terminator, or
   * to a comment character if the parameter is null.
   *------------------------------------------------------------**/
  private Parameter parseName(PushbackReader reader, boolean ignore) throws IOException, IllegalArgumentException {

    int          character;
    StringBuffer buffer = new StringBuffer();
    String       string;
    Parameter    result = null;

     // Skip over initial whitespace
    while ((character = reader.read()) == ' ' || character == '\t');
    while (character != NameTerminator && character != CommentCharacter && character != '\n' && character != EndOfFile) {
      buffer.append((char)character);
      character = reader.read();
    }
    if (character != NameTerminator) reader.unread(character);
    string = buffer.toString().trim();
    if (string.length() == 0) {
       // Treat an empty parameter as a comment
      reader.unread(CommentCharacter);
    } else {
      result = (Parameter)nameHash.get(string);
      if (result == null) {
        if (ignore) {
           // Treat an unknown parameter as a comment
          if (SysLog.printLevel > 0) SysLog.println("PCFG_scanConfigFile: WARNING - treating unknown parameter \"" + string + "\" as a comment");
          reader.unread(CommentCharacter);
        } else {
          throw new IllegalArgumentException("PCFG_scanConfigFile: Invalid parameter name \"" + string + "\"");
        }
      }
    }

    return result;
  }

  /**------------------------------------------------------------*
   * Return a value string from the Reader. Return the quoted
   * empty String ("") as itself; return the unquoted empty
   * String as null. Upon return, the Reader points to the first
   * character following the value string or its enclosing quotes.
   *------------------------------------------------------------**/
  private String parseValue(PushbackReader reader) throws IOException, IllegalArgumentException {

    int          character;
    StringBuffer buffer = new StringBuffer();
    String       result;

     // Skip over initial whitespace
    while ((character = reader.read()) == ' ' || character == '\t');
    int quoteIndex = quoteCharacters.length();
    while (--quoteIndex >= 0) if (character == quoteCharacters.charAt(quoteIndex)) break;
    if (quoteIndex >= 0) {
       // Value is quoted
      char quoteCharacter = quoteCharacters.charAt(quoteIndex);
      while ((character = reader.read()) != quoteCharacter && character != EndOfFile) {
        if (character == escapeCharacter && (character = reader.read()) == EndOfFile) {
           // Escape character has no character to escape
          reader.unread(character);
          throw new IllegalArgumentException("PCFG_scanConfigFile: Ill-formed value \"" + buffer.toString() + "\"");
        } else {
          buffer.append((char)character);
        }
      }
       // Return the result string exactly as is, whether empty or not
      result = buffer.toString();
    } else {
       // Value is not quoted
      while (character != CommentCharacter && character != '\n' && character != EndOfFile) {
        buffer.append((char)character);
        character = reader.read();
      }
      reader.unread(character);
      result = buffer.toString().trim();
       // If the result String is empty, return null
      if (result.length() == 0) result = null;
    }

    return result;
  }

  /**------------------------------------------------------------*
   * Return a reference to the named parameter
   *------------------------------------------------------------**/
  public Parameter parameter(String name) throws IllegalArgumentException {
    Object parameter = nameHash.get(name);
    if (parameter == null) throw new IllegalArgumentException("Invalid parameter name \"" + name + "\"");
    return (Parameter)parameter;
  }

  /**------------------------------------------------------------*
   * Set the status of the named parameter
   *------------------------------------------------------------**/
  public void setStatus(String name, boolean configured) throws IllegalArgumentException {
    Object parameter = nameHash.get(name);
    if (parameter == null) throw new IllegalArgumentException("Invalid parameter name \"" + name + "\"");
    ((Parameter)parameter).configured = configured;
  }

  /**------------------------------------------------------------*
   * Return the status of the named parameter
   *------------------------------------------------------------**/
  public boolean configured(String name) throws IllegalArgumentException {
    Object parameter = nameHash.get(name);
    if (parameter == null) throw new IllegalArgumentException("Invalid parameter name \"" + name + "\"");
    return ((Parameter)parameter).configured;
  }

  /**------------------------------------------------------------*
   * Return the number of unknown arguments on the command line
   *------------------------------------------------------------**/
  public int numUnknownArgs() {
    return unknowns.size();
  }

  /**------------------------------------------------------------*
   * Return the n-th unknown command line argument
   *------------------------------------------------------------**/
  public String unknownArg(int n) throws ArrayIndexOutOfBoundsException {
    return arguments[indexOfUnknownArg(n)];                       // Throws ArrayIndexOutOfBoundsException
  }

  /**------------------------------------------------------------*
   * Return the position on the command line of the n-th
   * unknown argument
   *------------------------------------------------------------**/
  public int indexOfUnknownArg(int n) throws ArrayIndexOutOfBoundsException {
    return ((Integer)unknowns.elementAt(n)).intValue();
  }

  /**------------------------------------------------------------*
   * Return a list of the unknown arguments
   *------------------------------------------------------------**/
  public String unknownArgsToString() throws ArrayIndexOutOfBoundsException {
    return unknownArgsToString(false, null, null);
  }

  /**------------------------------------------------------------*
   * Return a list of the unknown arguments, optionally showing
   * their indices. Prepend the prefix to the first argument and
   * the separator to each subsequent argument.
   *------------------------------------------------------------**/
  public String unknownArgsToString(boolean showIndices, String prefix, String separator) throws ArrayIndexOutOfBoundsException {
    boolean first = true;
    StringBuffer buffer = new StringBuffer("");

    if (prefix == null) prefix = "";
    if (separator == null) separator = ListSeparator;

    Enumeration args = unknowns.elements();
    while (args.hasMoreElements()) {
      int index = ((Integer)args.nextElement()).intValue();
      if (first) {
        buffer.append(prefix);
        first = false;
      } else {
        buffer.append(separator);
      }
      if (showIndices) buffer.append("[").append(index).append("] ");
      buffer.append(arguments[index]);
    }
    return buffer.toString();
  }

  /**------------------------------------------------------------*
   * Return a String detailing the command line options with
   * their descriptions
   *------------------------------------------------------------**/
  public String optionsToString() {
    return optionsToString(false, false, true);
  }

  /**------------------------------------------------------------*
   * Return a String detailing the command line options with
   * their values and descriptions
   *------------------------------------------------------------**/
  public String optionsAndValuesToString() {
    return optionsToString(true, false, true);
  }

  /**------------------------------------------------------------*
   * Return a String detailing the command line options,
   * optionally showing their values, their descriptions, and the
   * veiled options
   *------------------------------------------------------------**/
  public String optionsToString(boolean showValues, boolean showVeiled, boolean showDescriptions) {
    return optionsToString(showValues, showVeiled, showDescriptions, "   ", "\n");
  }

  /**------------------------------------------------------------*
   * Return a String detailing the command line options,
   * optionally showing their values, their descriptions, and the
   * veiled options. Prepend the prefix to the first parameter's
   * substring and prepend the separator to each subsequent
   * parameter's substring.
   *------------------------------------------------------------**/
  public String optionsToString(boolean showValues, boolean showVeiled, boolean showDescriptions, String prefix, String separator) {
     // If we are showing values and descriptions, get the length of the longest value string
    int maxValueLength = (showValues && showDescriptions) ? maxValueLength(showVeiled, true) : 0;
    return optionsToString(maxOptionLength, showValues, maxValueLength, showVeiled, showDescriptions, prefix, separator);
  }

  /**------------------------------------------------------------*
   * Return a labelled String detailing the command line options,
   * optionally showing their values, their descriptions, and the
   * veiled options. Prepend the prefix to the first parameter's
   * substring and prepend the separator to each subsequent
   * substring. This flavor of optionsToString inserts a label at
   * the beginning of the String, with the components of the label
   * justified appropriately for the fields they label. Values are
   * not shown if the value label is null; descriptions are not
   * shown if the description label is null.
   *------------------------------------------------------------**/
  public String optionsToString(String optionLabel, String valueLabel, String descriptionLabel, boolean showVeiled, String prefix, String separator) {
    if (optionLabel == null) optionLabel = "";
     // Compute the length of the option field (since the label may be longer than any option)
    int optionFieldLength = maxOptionLength();
    if (optionFieldLength < optionLabel.length()) optionFieldLength = optionLabel.length();
     // If we are showing values and descriptions, get the length of the longest value string
    int valueFieldLength = 0;
    if (valueLabel != null && descriptionLabel != null) {
      valueFieldLength = maxValueLength(showVeiled, true);
      if (valueFieldLength < valueLabel.length()) valueFieldLength = valueLabel.length();
    }
    StringFormatter.Field[] fields = {
      new StringFormatter.Field(prefix, ""),
      new StringFormatter.Field(-optionFieldLength, optionLabel, " = "),
      new StringFormatter.Field(valueFieldLength, valueLabel, " "),
      new StringFormatter.Field(descriptionLabel)
    };
    StringBuffer buffer = new StringFormatter(fields).toStringBuffer();
    buffer.append(optionsToString(optionFieldLength, valueLabel != null, valueFieldLength, showVeiled, descriptionLabel != null, separator, separator));
    return buffer.toString();
  }

  /**------------------------------------------------------------*
   * Return a String detailing the command line options,
   * optionally showing their values, their descriptions, and the
   * veiled options. Prepend the prefix to the first parameter's
   * substring and prepend the separator to each subsequent
   * parameter's substring. This flavor of optionsToString allows
   * adjusting the lengths of the option and value fields.
   *------------------------------------------------------------**/
  public String optionsToString(int optionFieldLength, boolean showValues, int valueFieldLength,
                                boolean showVeiled, boolean showDescriptions, String prefix, String separator) {
    Parameter    parameter;
    StringBuffer buffer = new StringBuffer("");

     // Unless we are showing values and descriptions, set the length of the value field to zero to avoid appending trailing blanks
    if (!(showValues && showDescriptions)) valueFieldLength = 0;

    boolean first = true;
    for (int pi = 0; pi < parameters.length; pi++) {
      parameter = parameters[pi];
       // Print only the parameters that have command line options
      if ((!parameter.veiled() || showVeiled) && parameter.option() != null) {
        if (first) {
          first = false;
          if (prefix != null) buffer.append(prefix);
        } else if (separator != null) {
          buffer.append(separator);
        }
        buffer.append(parameter.toString(optionFieldLength, showValues, valueFieldLength, showDescriptions, " = "));
      }
    }
    return buffer.toString();
  }

  /**------------------------------------------------------------*
   * Return a String listing the unveiled parameters on successive
   * lines without their command line options and descriptions
   *------------------------------------------------------------**/
  public String parametersToString() {
    return parametersToString(false, false, false);
  }

  /**------------------------------------------------------------*
   * Return a String listing the parameters on successive lines,
   * optionally showing the command line options, the
   * descriptions, and the hidden parameters
   *------------------------------------------------------------**/
  public String parametersToString(boolean showOptions, boolean showVeiled, boolean showDescriptions) {
    return parametersToString(showOptions, showVeiled, showDescriptions, "  ", "\n  ");
  }

  /**------------------------------------------------------------*
   * Return a String listing the parameters, optionally showing
   * the command line options, the descriptions, and the hidden
   * parameters. Prepend the prefix to the first parameter's
   * substring and prepend the separator to each subsequent
   * substring.
   *------------------------------------------------------------**/
  public String parametersToString(boolean showOptions, boolean showVeiled, boolean showDescriptions, String prefix, String separator) {
     // If we are showing descriptions, get the length of the longest value string
    int maxValueLength = showDescriptions ? maxValueLength(showVeiled, false) : 0;
    return parametersToString(maxNameLength, showOptions, maxOptionLength, maxValueLength, showVeiled, showDescriptions, prefix, separator);
  }

  /**------------------------------------------------------------*
   * Return a labelled String listing the parameters, optionally
   * showing the command line options, the descriptions, and the
   * hidden parameters. Prepend the prefix the first parameter's
   * substring and prepend the separator to each subsequent
   * parameter's substring. This flavor of parametersToString
   * inserts a label at the beginning of the String, with the
   * components of the label justified in the same way as the
   * fields they label. Options are suppressed if the option
   * label is null; descriptions are suppressed if the
   * description label is null.
   *------------------------------------------------------------**/
  public String parametersToString(String nameLabel, String optionLabel, String valueLabel, String descriptionLabel,
                                   boolean showVeiled, String prefix, String separator) {
     // The name field must have a label
    if (nameLabel == null) nameLabel = "<Parameter>";
     // Compute the length of the name field (since the label may be longer than any name)
    int nameLength = maxNameLength();
    if (nameLength < nameLabel.length()) nameLength = nameLabel.length();
     // Compute the length of the option field (since the label may be longer than any option)
    int optionLength = 0;
    if (optionLabel != null) {
      optionLength = maxOptionLength();
      if (optionLength < optionLabel.length()) optionLength = optionLabel.length();
    }
     // The value field must have a label
    if (valueLabel == null) valueLabel = "<Value>";
     // If we are showing descriptions, get the length of the longest value string
    int valueLength = 0;
    if (descriptionLabel != null) {
      valueLength = maxValueLength(showVeiled, false);
      if (valueLength < valueLabel.length()) valueLength = valueLabel.length();
    }
    StringFormatter.Field[] fields = {
      new StringFormatter.Field(prefix, ""),
      new StringFormatter.Field(-nameLength, nameLabel, " "),
      new StringFormatter.Field(-optionLength, optionLabel, " = "),
      new StringFormatter.Field(valueLength, valueLabel, " "),
      new StringFormatter.Field(descriptionLabel)
    };
    StringBuffer buffer = new StringFormatter(fields).toStringBuffer();
    buffer.append(parametersToString(nameLength, optionLabel != null, -optionLength, valueLength, showVeiled, descriptionLabel != null, separator, separator));
    return buffer.toString();
  }

  /**------------------------------------------------------------*
   * Return a String listing the parameters, optionally showing
   * the command line options, the descriptions, and the hidden
   * parameters. Prepend the prefix the first parameter's
   * substring and prepend the separator to each subsequent
   * parameter's substring. This flavor of parametersToString
   * allows adjusting the lengths of the name, option and value
   * fields.
   *------------------------------------------------------------**/
  public String parametersToString(int nameFieldLength, boolean showOptions, int optionFieldLength, int valueFieldLength,
                                   boolean showVeiled, boolean showDescriptions, String prefix, String separator) {
    Parameter    parameter;
    StringBuffer buffer = new StringBuffer("");

     // Unless we are showing descriptions, set the length of the value field to zero to avoid appending training blanks
    if (!showDescriptions) valueFieldLength = 0;

    boolean first = true;
    for (int pi = 0; pi < parameters.length; pi++) {
      parameter = parameters[pi];
      if (!parameter.veiled() || showVeiled) {
        if (first) {
          first = false;
          if (prefix != null) buffer.append(prefix);
        } else if (separator != null) {
          buffer.append(separator);
        }
        buffer.append(parameter.toString(nameFieldLength, showOptions, optionFieldLength, valueFieldLength, showDescriptions, " = "));
      }
    }
    return buffer.toString();
  }

  /**------------------------------------------------------------*
   * Return the length of the longest value string
   *------------------------------------------------------------**/
  public int maxValueLength(boolean includeVeiled, boolean optionsOnly) {
    int maxValueLength = 0;
    for (int pi = 0; pi < parameters.length; pi++) {
      Parameter parameter = parameters[pi];
      if ((!parameter.veiled() || includeVeiled) && !(optionsOnly && parameter.option == null)) {
        int valueLength = parameter.stringValue().length();
        if (maxValueLength < valueLength) maxValueLength = valueLength;
      }
    }
    return maxValueLength;
  }

  public static final char env = '$';   // The character that introduces an environment variable reference
  public static final char bra = '{';   // The (optional) start of an environment variable name (immediately following an env char)
  public static final char ket = '}';   // The end of an environment variable name (obligatory if the name started with a bra)

  /**------------------------------------------------------------*
   * Substitute environment values into a string
   *------------------------------------------------------------**/
  public static String scanEnvironment(String string) {

    StringBuffer buffer = new StringBuffer(100);
    StringBuffer variable = null;

    char character;
    int  index = 0;

    while (index < string.length()) {
      character = string.charAt(index++);
      if (character == env && index < string.length()) {
        character = string.charAt(index++);
         // "$$" is "$"
        if (character == env) {
          buffer.append(character);
        } else {
          if (variable == null) variable = new StringBuffer(100);
          else variable.setLength(0);
           // Treat any bracketed sequence of characters as the name of an environment variable
          if (character == bra) {
            while (index < string.length()) {
              character = string.charAt(index++);
              if (character != ket)
                variable.append(character);
              else
                break;
            }
           // Otherwise, the name must be a sequence of alphanumeric characters and underscores, terminated by anything else
          } else {
            while (true) {
              if (Character.isLetterOrDigit(character) || character == '_') {
                variable.append(character);
                if (index < string.length()) character = string.charAt(index++);
                else break;
              } else {
                index--;
                break;
              }
            }
          }
          String environment = variable.length() > 0 ? Environment.getEnvironment(variable.toString()) : null;
           // If variable is not the name of an environment variable, assume it is the name of a Java system property
          if (environment == null) environment = System.getProperty(variable.toString());
          if (environment != null)
            buffer.append(environment);
          else if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.PCFG_MASK))
            SysLog.println("PCFG.scanEnvironment: found no definition for \"" + variable.toString() + "\"");
        }
      } else {
        buffer.append(character);
      }
    }

    return(buffer.toString());
  }

  /**-------------------------------------------------------------------------*
   * Case INSENSITIVE String comparison of the first prefix.length() characters
   * of the two String arguments
   *-------------------------------------------------------------------------**/
  private boolean startsWith(String string, String prefix) {
    return string != null && prefix != null && string.regionMatches(true, 0, prefix, 0, prefix.length());
  }

  /**------------------------------------------------------------*
   * Return the index of the first distinguishing character in
   * the nth parameter's option name
   *------------------------------------------------------------**/
  private int ambiguityLength(int n) {

    int  first;
    int  index = 0;

    for (int pi = 0; pi < parameters.length; pi++) {
      if (pi != n) {
        first = matchLength(parameters[pi].option, parameters[n].option);
        if (first > index) index = first;
      }
    }

    return(index);
  }

  /**-------------------------------------------------------------------------*
   * Case INSENSITIVE string comparison, returning the length of the common
   * prefix
   *-------------------------------------------------------------------------**/
  private int matchLength(String string1, String string2) {

    int  length = 0;

    if (string1 != null && string2 != null) {
      int size = string1.length() < string2.length() ? string1.length() : string2.length();
      while (length < size) {
        char c1 = Character.toUpperCase(Character.toLowerCase(string1.charAt(length)));
        char c2 = Character.toUpperCase(Character.toLowerCase(string2.charAt(length)));
        if (c1 != c2) break;
        length++;
      }
    }

    return(length);
  }
}
