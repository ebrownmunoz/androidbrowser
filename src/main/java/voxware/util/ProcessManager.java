package voxware.util;

 // Manipulate processes
public class ProcessManager {

  private static native boolean killProcess(String processName);
  private static native boolean processExists(String processName);

  private static boolean nativeLoaded = false;
  static {
    try {
      // LibraryLoader.loadLibrary("jutil");
      nativeLoaded = true;
    } catch(UnsatisfiedLinkError e) {
      SysLog.println("voxware.util.ProcessManager: ERROR - jutil library does not exist");
    } catch (SecurityException e) {
      SysLog.println("voxware.util.ProcessManager: ERROR - jutil library load fails due to: " + e.toString());
    }
  }

   // Kill all processes with the given name, returning true iff any exist
  public static boolean kill(String processName) {
    return killProcess(processName);
  }

   // Return true iff processes with the given name exist
  public static boolean exists(String processName) {
    return processExists(processName);
  }
}

