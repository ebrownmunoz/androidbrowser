package voxware.util;

public interface ByteArrayWrapper {
  byte[] getBytes();
}
