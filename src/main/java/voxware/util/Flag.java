package voxware.util;

 // A Flag
public class Flag {

  private boolean flag;
  private Mailbox receiver;
  private Object  message;
  private int     priority;

  public Flag() {
  }

  public Flag(Mailbox receiver, Object message) {
    this(receiver, message, 0);
  }

  public Flag(Mailbox receiver, Object message, int priority) {
    this.receiver = receiver;
    this.message = message;
    this.priority = priority;
  }

  public void receiver(Mailbox receiver, Object message) {
    this.receiver(receiver, message, 0);
  }

  public void receiver(Mailbox receiver, Object message, int priority) {
    this.receiver = receiver;
    this.message = message;
    this.priority = priority;
  }

   // Return the state of the flag
  public boolean raised() {
    return flag;
  }

   // Return the state of the flag and raise it, optionally sending a message
  public synchronized boolean raise() {
    boolean wasRaised = flag;
    flag = true;
    if (receiver != null) receiver.insert(message, priority);
    return wasRaised;
  }

   // Raise the flag, optionally sending a message, and wait forever for it to be lowered
   // Return true if the flag is ever lowered, false otherwise
  public synchronized boolean raiseAndWait() {
    boolean success = true;
    flag = true;
    if (receiver != null) receiver.insert(message, priority);
    try {
      wait();
    } catch (InterruptedException e) {
      success = false;
    }
    return success;
  }

   // Raise the flag, optionally sending a message, and wait a while for it to be lowered
   // Return true if the flag is lowered before the timeout period expires, false otherwise
  public synchronized boolean raiseAndLinger(long timeout) throws IllegalArgumentException {
    boolean success = true;
    flag = true;
    if (receiver != null) receiver.insert(message, priority);
    try {
      wait(timeout);
    } catch (InterruptedException e) {
      success = false;
    }
    flag = false;   // In case the receiver failed to lower it
    return success;
  }

   // Raise the flag, optionally sending a message, and wait a while for it to be lowered
   // Return true if the flag is lowered before the timeout period expires, false otherwise
  public synchronized boolean raiseAndLinger(long timeout, int nanos) throws IllegalArgumentException {
    boolean success = true;
    flag = true;
    if (receiver != null) receiver.insert(message, priority);
    try {
      wait(timeout, nanos);
    } catch (InterruptedException e) {
      success = false;
    }
    success = success && !flag;
    flag = false;   // In case the receiver failed to lower it
    return success;
  }

   // Return the state of the flag and lower it
  public synchronized boolean lower() {
    boolean wasRaised = flag;
    flag = false;
    notify();
    return wasRaised;
  }
}
