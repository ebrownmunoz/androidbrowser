#ifndef __voxware_util_SystemMemory__
#define __voxware_util_SystemMemory__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace util
    {
      class SystemMemory;
    }
  }
}

class voxware::util::SystemMemory : public ::java::lang::Object
{
private:
  void updateAllocationStats ();
public:
  SystemMemory ();
  virtual jlong totalBytes () { return totalBytes__; }
  virtual jlong bytesFree () { return bytesFree__; }
  virtual jint percentUsed () { return percentUsed__; }
  virtual jlong maxFreeBlockSize () { return maxFreeBlockSize__; }
  virtual jlong totalPhysicalBytes () { return totalBytes__; }
  virtual jlong physicalBytesFree () { return bytesFree__; }
  virtual jlong totalVirtualBytes () { return totalVirtualBytes__; }
  virtual jlong virtualBytesFree () { return virtualBytesFree__; }
private:
  void parseMemInfo ();
  static jboolean nativeLoaded;
public:
  jlong __attribute__((aligned(__alignof__( ::java::lang::Object ))))  totalBytes__;
  jlong bytesFree__;
  jint percentUsed__;
  jlong maxFreeBlockSize__;
  jlong totalVirtualBytes__;
  jlong virtualBytesFree__;

  static ::java::lang::Class class$;
};

#endif /* __voxware_util_SystemMemory__ */
