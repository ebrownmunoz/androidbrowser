package voxware.util;

import java.io.File;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipBall {

  private static final int BUFFER_SIZE = 8192;

   /** Return an Array of all the Files in a directory whose names satisfy the given FilenameFilter */
  public static File[] listFilesAsArray(File directory, FilenameFilter filter, boolean recurse) {
    Collection files = listFiles(directory, filter, recurse);
    return (File[]) files.toArray(new File[0]);
  }

   /** Return a List of all the Files in a directory whose names satisfy the given FilenameFilter */
  public static Collection listFiles(File directory, FilenameFilter filter, boolean recurse) {
    Vector files = new Vector();
	
    File[] entries = directory.listFiles();
    System.out.println("ZipBall.listFiles: entries.length = " + entries.length);
    for (int f = 0; f < entries.length; f++) {
      File entry = (File) entries[f];

       // If there is no filter or the filter accepts the file / directory, add it to the list
      if (filter == null || filter.accept(directory, entry.getName())) {
        System.out.println("ZipBall.listFiles: add entry \"" + entry + "\"");
        files.add(entry);
      }
		
       // If the file is a directory and the recurse flag is set, recurse into the directory
      if (recurse && entry.isDirectory()) {
        System.out.println("ZipBall.listFiles: add ALL entries for \"" + entry + "\"");
        files.addAll(listFiles(entry, filter, recurse));
      }
    }
	
    // Return collection of files
    return files;		
  }

   /** Zip the given byte[] in an entry at the given path and return the zipball as a byte[] */
  public static byte[] zipBytes(String path, byte[] bytes) throws IOException {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    ZipOutputStream zipper = new ZipOutputStream(stream);
    int slash = 1;
    while ((slash = path.indexOf(File.separatorChar, slash) + 1) > 0) {
      zipper.putNextEntry(new ZipEntry(path.substring(0, slash)));
      zipper.closeEntry();
    }
    zipper.putNextEntry(new ZipEntry(path));
    zipper.write(bytes, 0, bytes.length);
    zipper.closeEntry();
    zipper.close();
    return stream.toByteArray();
  }

   /** Convert the given content String to a byte[] using the specified encoding and zip it in an entry at the given path */
  public static byte[] zipString(String path, String content, String encoding) throws IOException {
    return zipBytes(path, content.getBytes(encoding));
  }

   /** Convert the given content String to a byte[] using UTF8 encoding and zip it in an entry at the given path */
  public static byte[] zipString(String path, String content) throws IOException {
    return zipBytes(path, content.getBytes("utf8"));
  }

   /** Unzip the named entry from the given byte[] and return it as a byte[] */
  public static byte[] unzipBytes(String name, byte[] bytes) throws IOException {
    ZipInputStream unzipper = new ZipInputStream(new ByteArrayInputStream(bytes));
    ZipEntry entry = null;
    while ((entry = unzipper.getNextEntry()) != null) {
      if (name == null || name.equals(entry.getName())) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] buffer = new byte[BUFFER_SIZE];
        int numBytesRead;
        while ((numBytesRead = unzipper.read(buffer, 0, buffer.length)) > 0) stream.write(buffer, 0, numBytesRead);
        unzipper.closeEntry();
        unzipper.close();
        stream.close();
        return stream.toByteArray();
      }
    }
    return null;
  }

  private ZipFile     zipFile;
  private InputStream inputStream;
  private SortedSet   dirsMade;
  private String      basePath;
  private String      zipBase;

  private ByteArrayOutputStream byteStream;
  private ZipOutputStream       zipper;

  public ZipBall() {}

   /** Open a ZipBall to accumulate ZipEntries */
  public void open() {
    byteStream = new ByteArrayOutputStream();
    zipper = new ZipOutputStream(byteStream);
    dirsMade = new TreeSet();
  }

   /** Close the ZipBall and return its image */
  public byte[] close() throws IOException {
    zipper.close();
    return byteStream.toByteArray();
  }

   /** Zip the given byte[] in an entry at the given path */
  public void addBytes(String path, byte[] bytes) throws IOException {
    int slash = 1;
    while ((slash = path.indexOf(File.separatorChar, slash) + 1) > 0) {
      String dirName = path.substring(0, slash);
      if (!dirsMade.contains(dirName)) {
        zipper.putNextEntry(new ZipEntry(dirName));
        zipper.closeEntry();
        dirsMade.add(dirName);
      }
    }
    zipper.putNextEntry(new ZipEntry(path));
    zipper.write(bytes, 0, bytes.length);
    zipper.closeEntry();
  }

   /** Convert the given content String to a byte[] using the specified encoding and zip it in an entry at the given path */
  public void addString(String path, String content, String encoding) throws IOException {
    addBytes(path, content.getBytes(encoding));
  }

   /** Convert the given content String to a byte[] using UTF8 encoding and zip it in an entry at the given path */
  public void addString(String path, String content) throws IOException {
    addBytes(path, content.getBytes("utf8"));
  }

   /** Create a zipfile containing the given directory hierarchy */
  public void create(String zipName, String srcDir) throws Exception {
    if (zipName == null || srcDir == null)
      throw new Exception("Invalid arguments to create zip file");
    if (srcDir.indexOf(File.separator) < 0)
      throw new Exception("ERROR Zip ball source path must be ABSOLUTE");

    try {
      FileOutputStream fos = new FileOutputStream(zipName);
      ZipOutputStream zos = new ZipOutputStream(fos);

      basePath = srcDir.substring(0, srcDir.lastIndexOf(File.separator));
      zipBase = srcDir.substring(srcDir.lastIndexOf(File.separator)+1);
      System.out.println("ZipBall.create: basePath = \"" + basePath + "\" zipBase = \"" + zipBase + "\"");
      zipDir(srcDir, srcDir, zos);

      zos.flush();
      zos.finish();
      zos.close();
      fos.close();
    } catch (IOException e) {
      SysLog.println("ZipBall.create: EXCEPTION -- " + e.getMessage());
      throw e;
    }
  }

   /** Extract all of the ZipEntries from a ZipFile into a given directory */
  public void extract(String fileName, String destDir) throws Exception {
    dirsMade = new TreeSet();
    inputStream = null;
    zipFile = new ZipFile(fileName);
    Enumeration all = zipFile.entries();
    while (all.hasMoreElements()) {
      try {
        getFile((ZipEntry) all.nextElement(), destDir);
      } catch (IOException err) {
        SysLog.println("ZipBall.extract: IO Error (" + err + ")");
        throw err;
      }
    }
  }

   /** Extract all of the ZipEntries from a ZipInputStream into a given directory */
  public void extract(ZipInputStream stream, String destDir) throws Exception {
    dirsMade = new TreeSet();
    this.inputStream = stream;
    zipFile = null;
    ZipEntry entry = null;
    while ((entry = stream.getNextEntry()) != null) {
      try {
        getFile(entry, destDir);
      } catch (IOException err) {
        SysLog.println("ZipBall.extract: IO Error (" + err + ")");
        throw err;
      }
    }
  }

  private boolean     warnedMkdir = false;
  private byte[]      buffer;

   /** Extract a ZipEntry from the zip ball */
  protected void getFile(ZipEntry e, String destDir) throws IOException {
    String zipName = e.getName();
    if (zipName.startsWith(File.separator)) {
      if (!warnedMkdir) {
	       if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.UTILITY_MASK))
          SysLog.println("ZipBall.getFile: ignoring absolute paths");
        warnedMkdir = true;
      }
      zipName = zipName.substring(1);
    }

     // Directory creation
    int lastSlashIdx = zipName.lastIndexOf(File.separatorChar);
    if (lastSlashIdx > 0) {
      String dirName = destDir + File.separator + zipName.substring(0, lastSlashIdx);
      if (!dirsMade.contains(dirName)) {
        File d = new File(dirName);
         // If it already exists as a dir, don't do anything
        if (!(d.exists() && d.isDirectory())) {
           // Try to create the directory, warn on failure
	         if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.UTILITY_MASK))
            SysLog.println("ZipBall.getFile: creating directory \"" + dirName + "\"");
          if (!d.mkdirs())
            SysLog.println("ZipBall.getFile: WARNING -- unable to mkdir \"" + dirName + "\"");
          dirsMade.add(dirName);
        }
      }
    }

     // If entry ends with File.separator, directory creation is all that's needed
    if (zipName.endsWith(File.separator))
      return;

    zipName = destDir + File.separator + zipName;
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.UTILITY_MASK))
      SysLog.println("ZipBall.getFile: creating \"" + zipName + "\"");
    FileOutputStream outputStream = new FileOutputStream(zipName);
    if (zipFile != null) inputStream = zipFile.getInputStream(e);
    int n = 0;
    if (buffer == null) buffer = new byte[BUFFER_SIZE];
    while ((n = inputStream.read(buffer, 0, buffer.length)) > 0) outputStream.write(buffer, 0, n);
    if (zipFile != null) inputStream.close();
    else ((ZipInputStream) inputStream).closeEntry();
    outputStream.close();
  }

  private void zipDir(String currentDir, String srcDir, ZipOutputStream zos) throws IOException {
    File dirObj = new File(currentDir);
    if (dirObj.exists() == true) {
      if (dirObj.isDirectory() == true) {
        File[] fileList = dirObj.listFiles();
        String path = dirObj.getAbsolutePath();
        path = path.substring(basePath.length()+1);

        System.out.println("ZipBall.zipDir: creating directory \"" + path + "\"");
        ZipEntry zipEntry = new ZipEntry(path);
        zos.putNextEntry(zipEntry);

        for (int i = 0; i < fileList.length; i++) {
          if (fileList[i].isDirectory()) {
            zipDir(fileList[i].getPath(), srcDir, zos);
          } else if (fileList[i].isFile()) {
            zipFile(fileList[i].getPath(), zos, srcDir);
          }
        }
      } else {
        System.out.println("ZipBall.zipDir: ERROR -- \"" + srcDir + "\" is not a directory");
      }
    } else {
      System.out.println("ZipBall.zipDir: ERROR -- directory \"" + srcDir + "\" not found");
    }
  }

  private void zipFile(String filePath, ZipOutputStream zos, String srcDir) throws IOException {
    File file = new File(filePath);
    String path;

    path = file.getAbsolutePath();
    String tmpDir = srcDir + File.separator;

    // Strip srcdir from absolute path but prefix w/zipBase
    path = path.substring(path.indexOf(tmpDir) + tmpDir.length());
    path = zipBase + File.separator + path;

    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.UTILITY_MASK))
      SysLog.println("ZipBall.zipFile: archiving " + filePath + ": " + path);

    FileInputStream fis = new FileInputStream(filePath);
    BufferedInputStream bis = new BufferedInputStream(fis);

    ZipEntry zipEntry = new ZipEntry(path);
    zos.putNextEntry(zipEntry);

    int byteCount;
    if (buffer == null) buffer = new byte[BUFFER_SIZE];
    while ((byteCount = bis.read(buffer, 0, buffer.length)) > -1)
      zos.write(buffer, 0, byteCount);
  }

}
