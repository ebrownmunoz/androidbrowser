package voxware.util;

public class RegistryEntry {

  public static int MaxValueLength = 512;
  public static int MaxRootLength  = 5;
  public static int MaxPathLength  = 512;
  public static int MaxNameLength  = 256;

  public static String UntypedTypeName = "<untyped>";
  public static String ByteArrayTypeName = "hex:";
  public static String LongTypeName = "dword:";
  public static String StringArrayTypeName = "array:";

  public static char   PathSeparator = '/';
  public static char   RegistryPathSeparator = '\\';
  public static String DefaultArrayDelimiter = ",";
  public static String RegistryEntryDelimiter = "\n";

  public RegistryEntry() {}

  /**-------------------------------------------------------------------------------*
   * Construct a new RegistryEntry from the given full path. If path ends with a
   * PATH_Separator character, the entry will be a directory (key); otherwise, it
   * will be a named value. The type is ignored when constructing a directory, since
   * directories are not typed.
   *-------------------------------------------------------------------------------**/
  public RegistryEntry(String path) throws MalformedRegistryEntryException {
    parse(path);
  }

  /**-------------------------------------------------------------------------------*
   * Construct a new RegistryEntry from the given components.
   * If name is null, the new RegistryEntry is a directory.
   *-------------------------------------------------------------------------------**/
  public RegistryEntry(String root, String path, String name) throws MalformedRegistryEntryException {
    this(root, path, name, null);
  }

  /**-------------------------------------------------------------------------------*
   * Construct a new RegistryEntry with a value from the given components.
   * If name is null, the new RegistryEntry is a directory.
   *-------------------------------------------------------------------------------**/
  public RegistryEntry(String root, String path, String name, Object value) throws MalformedRegistryEntryException {
    if (root != null) setRoot(root);
    if (path != null) setPath(path);
    setName(name);
    setValue(value);
  }

  /**-------------------------------------------------------------------------------*
   * Construct a new directory RegistryEntry from the given components.
   *-------------------------------------------------------------------------------**/
  public RegistryEntry(String root, String path) throws MalformedRegistryEntryException {
    this(root, path, null, null);
  }

   /// Methods to read and write the underlying physical registry

  public native boolean read();
  public native boolean write();
  public native boolean remove();
  public native boolean isRootKey(String candidate);

  protected native int getType();

   /// Access methods

  protected String    root;
  public String    getRoot() { return root; }
  public void      setRoot(String root) throws MalformedRegistryEntryException {
    if (isRootKey(root)) this.root = root;
    else throw new MalformedRegistryEntryException("invalid root \"" + root + "\"");
  }

  protected String    path;
  public String    getPath() { return path; }
  public void      setPath(String path) {
    this.path = localize(path);
  }

  protected String    name;
  public String    getName() { return name; }
  public void      setName(String name) throws MalformedRegistryEntryException {
    if (name == null || name.indexOf(PathSeparator) < 0) this.name = name;
    else throw new MalformedRegistryEntryException("invalid name \"" + name + "\"");
  }

  protected Object   value;
  public Object   getValue() { return value; }
  public void     setValue(Object value) throws MalformedRegistryEntryException {
    if (value == null) {
      this.value = null;
    } else if (isValidType(value)) {
      this.value = (value instanceof Number && !(value instanceof Long)) ? new Long(((Number) value).longValue()) : value;
    } else {
      SysLog.println("RegistryEntry.setValue: invalid value type \"" + value.getClass().getName() + "\"");
      throw new MalformedRegistryEntryException("invalid value type \"" + value.getClass().getName() + "\"");
    }
  }
  public String          getStringValue()             { return (String) value; }
  public Long            getLongValue()               { return (Long) value; }
  public String[]        getStringArrayValue()        { return (String[]) value; }
  public byte[]          getByteArrayValue()          { return (byte[]) value; }
  public RegistryEntry[] getRegistryEntryArrayValue() { return (RegistryEntry[]) value; }

  public boolean isDirectory() { return isKey(); }
  public boolean isKey() { return name == null; }

   /// Display Methods

  public StringBuffer appendTo(StringBuffer buffer) throws MalformedRegistryEntryException { return appendTo(buffer, RegistryEntryDelimiter); }
  public StringBuffer appendTo(StringBuffer buffer, String separator) throws MalformedRegistryEntryException {
    buffer.append(root).append(PathSeparator).append(generalize(path)).append(PathSeparator);
    if (name != null) {
      buffer.append(name).append(" = ");
      if (value instanceof Long) {
        buffer.append(LongTypeName).append(value.toString());
      } else if (value instanceof String) {
        buffer.append(value);
      } else if (value instanceof String[]) {
        buffer.append(StringArrayTypeName);
        String[] array = (String[]) value;
        if (array.length > 0) {
          buffer.append(array[0]);
          for (int index = 1; index < array.length; index++) buffer.append(DefaultArrayDelimiter).append(array[index]);
        }
      } else if (value instanceof byte[]) {
        buffer.append(ByteArrayTypeName);
        byte[] array = (byte[]) value;
        if (array.length > 0) {
          buffer.append(ubyte(array[0]));
          for (int index = 1; index < array.length; index++) buffer.append(DefaultArrayDelimiter).append(ubyte(array[index]));
        }
      } else {
        throw new MalformedRegistryEntryException("invalid value type \"" + (value != null ? value.getClass().getName() : "<null>") + "\"");
      }
    } else if (value instanceof RegistryEntry[]) {
      RegistryEntry[] array = (RegistryEntry[]) value;
      for (int index = 0; index < array.length; index++) {
        buffer.append(separator);
        array[index].appendTo(buffer, separator);
      }
    } else {
      throw new MalformedRegistryEntryException("invalid key type \"" + value.getClass().getName() + "\"");
    }
      
    return buffer;
  }

  public String toString() {
    String string = null;
    try {
      string = appendTo(new StringBuffer(MaxPathLength)).toString();
    } catch(MalformedRegistryEntryException mree) {
    }

    return string;
  }
  
  public String toString(String separator) {
    String string = null;
    try {
      string = appendTo(new StringBuffer(MaxPathLength), separator).toString();
    } catch(MalformedRegistryEntryException mree) {
    }
    
    return string;
  }

  protected String ubyte(byte value) { return (Integer.toHexString(((int) value) & 0x00ff | 0x0f00).substring(1)); }
  protected String localize(String path) { return path.replace(PathSeparator, RegistryPathSeparator); }
  protected String generalize(String path) { return path.replace(RegistryPathSeparator, PathSeparator); }
  protected void parse(String path) {}

  /**------------------------------------------------------------*
   * Validate the type of a candidate value
   *------------------------------------------------------------**/
  public boolean isValidType(Object value) {
    return (value instanceof Number || value instanceof String ||
            value instanceof String[] || value instanceof byte[] || value instanceof RegistryEntry[]);
  }
}
