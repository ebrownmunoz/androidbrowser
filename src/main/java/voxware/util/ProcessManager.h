#ifndef __voxware_util_ProcessManager__
#define __voxware_util_ProcessManager__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace util
    {
      class ProcessManager;
    }
  }
}

class voxware::util::ProcessManager : public ::java::lang::Object
{
private:
  static jboolean killProcess (::java::lang::String *);
  static jboolean processExists (::java::lang::String *);
public:
  static jboolean kill (::java::lang::String *);
  static jboolean exists (::java::lang::String *);
  ProcessManager ();
private:
  static jboolean nativeLoaded;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_util_ProcessManager__ */
