package voxware.util;

public class MalformedRegistryEntryException extends Exception {

  public MalformedRegistryEntryException() { super(); }
  public MalformedRegistryEntryException(String message) { super(message); }

}

