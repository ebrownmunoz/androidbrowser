#ifndef __voxware_util_Initializer__
#define __voxware_util_Initializer__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace util
    {
      class Initializer;
    }
  }
}

class voxware::util::Initializer : public ::java::lang::Object
{
public:
  static ::java::lang::String *initialize (::java::lang::String *);
  Initializer ();

  static ::java::lang::Class class$;
};

#endif /* __voxware_util_Initializer__ */
