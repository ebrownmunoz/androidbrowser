package voxware.util;

public class Initializer {

  /**
   * The static initialization method for library jutil
   */
  public static native String initialize(String logFileName);

}
