package voxware.util.DHCP;

import java.io.*;
import java.net.*;
import java.util.StringTokenizer;

/**
 * A DHCP Message
 * Based on edu.bucknell.net.JDHCP.DHCPMessage
 * Created Feb 22, 2003 (DCV)
 */

public class DHCPMessage extends Object {

  public static final int DefaultMaximumSize = 576;

   // DHCP Message Types
  
  public static final int DISCOVER = 1;
  public static final int OFFER = 2;
  public static final int REQUEST = 3;
  public static final int DECLINE = 4;
  public static final int ACK = 5;
  public static final int NAK = 6;
  public static final int RELEASE = 7;
  public static final int INFORM = 8;
  public static String[] types = new String[] { null, "DISCOVER", "OFFER", "REQUEST", "DECLINE", "ACK", "NAK", "RELEASE", "INFORM" };

   // Default Destination Ports
  public static int DefaultClientPort = 68;     // Default DHCP client port
  public static int DefaultServerPort = 67;     // Default DHCP server port

  public static InetAddress BroadcastAddress = null; 
  static {
    if (BroadcastAddress == null) {
      try {
        BroadcastAddress = InetAddress.getByName("255.255.255.255");
      } catch (UnknownHostException e) {}
    }
  }

   // Default Destination InetAddress
  public static InetAddress DefaultDestination = BroadcastAddress;


   // DHCP Message Fields

  private byte        op;                         // The op code (1 in messages sent by a client; 2 in messages sent by a server)
  private byte        htype;                      // The hardware address type, aka link-layer or MAC address type (ethernet address type is 1)
  private byte        hlen;                       // The length of the hardware address in the chaddr field (6 for ethernet addresses)
  private byte        hops;                       // The number of relay agents that have forwarded this message
  private int         xid;                        // Transaction identifier, used by a client to match responses with previously transmitted requests
  private short       secs;                       // Elapsed time in seconds since the client began the configuration process
  private short       flags;                      // Flags (e.g., if the lsb, or broadcast bit, is set to 1, then replies must be broadcast)
  private byte[]      ciaddr = new byte[4];       // The client's IP address, set by the client when it has confirmed that its IP address is valid
  private byte[]      yiaddr = new byte[4];       // The client's (your) IP address, set by the server to inform the client of the client's IP address
  private byte[]      siaddr = new byte[4];       // The IP address of the next server for the client to use in the configuration process
  private byte[]      giaddr = new byte[4];       // Relay agent (gateway) IP, filled in by the relay agent with the address of the receiving interface
  private byte[]      chaddr = new byte[16];      // The client's hardware (link-layer) address
  private byte[]      sname  = new byte[64];      // The (optional) hostname of the next server for the client to use in the configuration process
  private byte[]      file   = new byte[128];     // The (optional) name of the file (e.g. OS image) for the client to request from the next server

  private DHCPOptions optionTable = null;         // Internal representaton of the options section of a DHCP message

  private int         port;                       // The destination port
  private InetAddress dest;                       // InetAddress of the destination host

  /**
   * Construct an empty DHCPMessage object
   * @param dest the destination InetAddress
   * @param port the destination port number
   */
  public DHCPMessage()                           { this(DefaultDestination, DefaultServerPort); }
  public DHCPMessage(InetAddress dest)           { this(dest, DefaultServerPort); }
  public DHCPMessage(int port)                   { this(DefaultDestination, port); }
  public DHCPMessage(InetAddress dest, int port) {
    initialize();
    this.dest = dest;
    this.port  = port;
  }

  /**
   * Copy constructor
   * Construct a new DHCPMessage by copying message
   * @param message the DHCPMessage to copy
   * @param dest the destination InetAddress
   * @param port the destination port number
   */
  public DHCPMessage(DHCPMessage message)                             { this(message, DefaultDestination, DefaultServerPort); }
  public DHCPMessage(DHCPMessage message, InetAddress dest)           { this(message, dest, DefaultServerPort); }
  public DHCPMessage(DHCPMessage message, int port)                   { this(message, DefaultDestination, port); }
  public DHCPMessage(DHCPMessage message, InetAddress dest, int port) {
    initialize();
    this.dest   = dest;
    this.port   = port;
    this.op     = message.op;
    this.htype  = message.htype;
    this.hlen   = message.hlen;
    this.hops   = message.hops;
    this.xid    = message.xid;
    this.secs   = message.secs;
    this.flags  = message.flags;
    this.ciaddr = message.ciaddr;
    this.yiaddr = message.yiaddr;
    this.siaddr = message.siaddr;
    this.giaddr = message.giaddr;
    this.chaddr = message.chaddr;
    this.sname  = message.sname;
    this.file   = message.file;
    optionTable.internalize(message.getOptions()); 
  }

  /**
   * Construct a DHCPMessage object and initialize it from the given byte[]
   * @param bytes the byte[] to initialize DHCPMessage object
   * @param dest the destination InetAddress
   * @param port the destination port number
   */
  public DHCPMessage(byte[] bytes)                             { this(bytes, DefaultDestination, DefaultServerPort); }
  public DHCPMessage(byte[] bytes, InetAddress dest)           { this(bytes, dest, DefaultServerPort); }
  public DHCPMessage(byte[] bytes, int port)                   { this(bytes, DefaultDestination, port); }
  public DHCPMessage(byte[] bytes, InetAddress dest, int port) {
    initialize();
    internalize(bytes);
    this.dest = dest;
    this.port = port;
    
  }

  /**
   * Construct a DHCPMessage object and initialize it from the given DataInputStream
   * @param stream the DataInputStream to initialize DHCPMessage object
   * @param dest the destination InetAddress
   * @param port the destination port number
   */
  public DHCPMessage(DataInputStream stream)                             { this(stream, DefaultDestination, DefaultServerPort); }
  public DHCPMessage(DataInputStream stream, InetAddress dest)           { this(stream, dest, DefaultServerPort); }
  public DHCPMessage(DataInputStream stream, int port)                   { this(stream, DefaultDestination, port); }
  public DHCPMessage(DataInputStream stream, InetAddress dest, int port) {
    initialize();
    internalize(stream);
    this.dest = dest;
    this.port = port;
  }

  /*
   * Initialize fields in this DHCPMessage
   * Initializes the options array from linked list form
   */
  private void initialize() {
    optionTable = new DHCPOptions();
  }
  
  /**
   * Convert this DHCPMessage object into a byte[] and return it
   * @return a byte[] representing this DHCPMessage object
   */
  public synchronized byte[] externalize() {
    ByteArrayOutputStream outBStream = new ByteArrayOutputStream();
    DataOutputStream outStream = new DataOutputStream(outBStream);
    try {
      outStream.writeByte(op);
      outStream.writeByte(htype);
      outStream.writeByte(hlen);
      outStream.writeByte(hops);
      outStream.writeInt(xid);
      outStream.writeShort(secs);
      outStream.writeShort(flags);
      outStream.write(ciaddr, 0, 4);
      outStream.write(yiaddr, 0, 4);
      outStream.write(siaddr, 0, 4);
      outStream.write(giaddr, 0, 4);
      outStream.write(chaddr, 0, 16);
      outStream.write(sname, 0, 64);
      outStream.write(file, 0, 128);
      byte[] options = new byte[312];
      if (optionTable == null) initialize();
      options = optionTable.externalize();
      outStream.write(options, 0, 312);
    } catch (IOException e) {
      System.err.println(e);
    }

     // Extract the byte array from the Stream
    byte data[] = outBStream.toByteArray ();

    return data;
  }

  /**
   * Read a byte[] containing a DHCP message into this DHCPMessage object and return this
   * @return this DHCPMessage object
   * @param bytes byte[] to convert to a DHCPMessage object
   */
  public synchronized DHCPMessage internalize(byte[] bytes) {
    ByteArrayInputStream inBStream = new ByteArrayInputStream(bytes, 0, bytes.length );
    DataInputStream      stream  = new DataInputStream(inBStream);
    return internalize(stream);
  }

  /**
   * Read a DataInputStream containing a DHCP message into the DHCPMessage object and return this
   * @return the DHCPMessage object
   * @param stream the DataInputStream containing a DHCP message
   */
  public synchronized DHCPMessage internalize(DataInputStream stream) {
    try {
      op    = stream.readByte();
      htype = stream.readByte();
      hlen  = stream.readByte();
      hops  = stream.readByte();
      xid   = stream.readInt();
      secs  = stream.readShort();
      flags = stream.readShort();
      stream.readFully(ciaddr, 0, 4);
      stream.readFully(yiaddr, 0, 4);
      stream.readFully(siaddr, 0, 4);
      stream.readFully(giaddr, 0, 4);
      stream.readFully(chaddr, 0, 16);
      stream.readFully(sname, 0, 64);
      stream.readFully(file, 0, 128);
      byte[]  options = new byte[312];
      stream.readFully(options, 0, 312);
      if (optionTable == null) initialize();
      optionTable.internalize(options);
    } catch (IOException e) {
      System.err.println(e);
    }

    return this;
  }

  /**************************************************************/
  /* Methods for setting DHCPMessage fields
  /**************************************************************/

  /**
   * Set message Op code / message type
   * @param op message Op code / message type
   */
  public void setOp(byte op) {
    this.op = op;
  }

  public static final byte RequestOperation = (byte)1;
  public static final byte ReplyOperation   = (byte)2;

  public void setRequest() { setOp(RequestOperation); }
  public void setReply()   { setOp(ReplyOperation);   }

  /**
   * Set hardware (aka link-layer or MAC) address type
   * @param htype hardware address type
   */
  public void setHtype(byte htype) {
    this.htype = htype;
  }

  public static final byte EthernetMACAddressType = (byte)1;

  /**
   * Set hardware address length
   * @param hlen hardware address length
   */
  public void setHlen(byte hlen) {
    this.hlen = hlen;
  }

  public static final byte EthernetMACAddressLength = (byte)6;

  /**
   * Set hops field
   * @param hops hops field
   */
  public void setHops(byte hops) {
    this.hops = hops;
  }

  /**
   * Set transaction ID
   * @param xid transactionID
   */
  public void setXid(int xid) {
    this.xid = xid;
  }

  /**
   * Set seconds elapsed since client began address acquisition or renewal process
   * @param secs seconds elapsed since client began address acquisition or renewal process
   */
  public void setSecs(short secs) {
    this.secs = secs;
  }

  /**
   * Set flags field
   * @param flags flags field
   */
  public void setFlags(short flags) {
    this.flags = flags;
  }

  /**
   * Set client IP address
   * The client sets this when it has confirmed that its IP address is valid
   * @param ciaddr client IP address
   */
  public void setCiaddr(byte[] ciaddr) {
    this.ciaddr = ciaddr;
  }

  /**
   * Set 'your' (client) IP address
   * The server sets this to inform the client of the client's IP address
   * @param yiaddr client IP address
   */
  public void setYiaddr(byte[] yiaddr) {
    this.yiaddr = yiaddr;
  }

  /**
   * Set address of next server to use in the configuration process
   * @param siaddr address of next server
   */
  public void  setSiaddr(byte[] siaddr) {
    this.siaddr = siaddr;
  }

  /**
   * Set relay agent (aka gateway) IP address
   * @param giaddr relay agent IP address
   */
  public void setGiaddr(byte[] giaddr) {
    this.giaddr = giaddr;
  }

  /**
   * Set client hardware (aka link-layer or MAC) address
   * @param chaddr client hardware address
   */
  public void setChaddr(byte[] chaddr) {
    this.chaddr = chaddr;
  }

  /**
   * Set optional server host name
   * @param sname server host name
   */
  public void setSname(byte[] sname) {
    this.sname = sname;
  }

  /**
   * Set boot file name
   * @param file boot file name
   */
  public void setFile(byte[] file) {
    this.file = file;
  }

  /**
   * Set message destination port
   * @param port port on message destination host
   */
  public void setPort(int port) {
    this.port = port;
  }

  /**
   * Set message destination IP
   * @param host hostname or string representation of message destination IP address
   */
  public void setDestinationHost(String host) {
    try {
      this.dest = InetAddress.getByName(host);
    } catch (Exception e) {
      System.err.println(e);
    }
  }

  /**************************************************************
   * Methods for getting the values of DHCPMessage fields
   **************************************************************/
  
  /**
   * Get message Opcode/message type
   */
  public byte getOp() {
    return op;
  }

  /**
   * Get hardware address type
   */
  public byte getHtype() {
    return htype;
  }

  /**
   * Get hardware address length
   */
  public byte getHlen() {
    return hlen ;
  }

  /**
   * Get hops field
   */
  public byte getHops() {
    return hops;
  }

  /**
   * Get transaction ID
   */
  public int getXid() {
    return xid;
  }

  /**
   * Get seconds elapsed since client began address acquisition or renewal process
   */
  public short getSecs() {
    return secs;
  }

  /**
   * Get flags field
   */
  public short getFlags() {
    return flags;
  }


  /**
   * Get client IP address
   */
  public byte[] getCiaddr() {
    return ciaddr;
  }

  /**
   * Get 'your' (client) IP address
   */
  public byte[] getYiaddr() {
    return yiaddr;
  }

  /**
   * Get address of next server to use in bootstrap
   */
  public byte[] getSiaddr() {
    return siaddr;
  }

  /**
   * Get relay agent IP address
   */
  public byte[] getGiaddr() {
    return giaddr;
  }

  /**
   * Get client harware address
   */
  public byte[] getChaddr() {
    return chaddr;
  }

  /**
   * Get optional server host name
   */
  public byte[] getSname() {
    return sname;
  }

  /**
   * Get boot file name
   */
  public byte[] getFile() {
    return file;
  }

  /**
   * Get all options.
   * @return a byte array containing options 
   */
  public byte[] getOptions() {
    if (optionTable == null) initialize();
    return optionTable.externalize();
  }

  /**
   * Get destination port
   * @return the destination port 
   */
  public int getPort() {
    return this.port;
  }

  /**
   * Get message destination hostname
   * @return a string representing the destination hostname
   * destination server 
   */
  public String getDestinationAddress() {
    return dest.getHostAddress();
  }

  /**
   * Set the given DHCP option in this DHCPMessage. If the option already exists then remove old option and insert a new one.
   * @param inOptNum  option number
   * @param inOptionData option data
   */
  public void setOption(int inOptNum, byte[] inOptionData) {
    optionTable.setOption((byte)inOptNum, inOptionData);
  }

  /**
   * Return the specified DHCP option if it is present, null otherwise
   * @param inOptNum  option number
   */
  public byte[] getOption(int inOptNum) {
    if (optionTable == null) initialize();
    return optionTable.getOption((byte) inOptNum);
  }

  /**
   * Removes the specified DHCP option
   * @param inOptNum  option number
   */
  public void removeOption(int inOptNum) {
    if (optionTable == null) initialize();
    optionTable.removeOption((byte)inOptNum);
  }

  /**
   * Report whether or not the given option number is present
   * @return true iff the option is specified
   * @param inOptNum  option number
   */
  public boolean optionIsSet(int inOptNum) {
    if (optionTable == null) initialize();
    return optionTable.contains((byte)inOptNum);
  }

  // ------------------------------------------------------------------
  // Static Utility Methods
  // ------------------------------------------------------------------

  /**
   * Convert a byte[] to a dot-punctuated decimal String, treating the bytes as unsigned
   * @param bytes a byte[]
   * @return a dot-punctuated decimal String
   */
  public static String unsignedBytesToString(byte[] bytes) {
    return unsignedBytesToString(bytes, 0, bytes.length, 10, '.');
  }

  /**
   * Convert the first length bytes of a byte[] to a separator-punctuated String with the given radix, treating the bytes as unsigned
   * @param bytes a byte[]
   * @param length the number of bytes to convert
   * @param radix the radix of the converted byte
   * @param separator a character to insert between the converted bytes
   * @return a separator-punctuated String with the given radix
   */
  public static String unsignedBytesToString(byte[] bytes, int length, int radix, char separator) {
    return unsignedBytesToString(bytes, 0, length, radix, separator);
  }

  /**
   * Convert length bytes of a byte[], starting at start, to a separator-punctuated String with the given radix, treating the bytes as unsigned
   * @param bytes a byte[]
   * @param start the starting index
   * @param length the number of bytes to convert
   * @param radix the radix of the converted byte
   * @param separator a character to insert between the converted bytes
   * @return a separator-punctuated String with the given radix
   */
  public static String unsignedBytesToString(byte[] bytes, int start, int length, int radix, char separator) {
    StringBuffer string = new StringBuffer();
    if (length > 0 && start < bytes.length) {
      string.append(unsignedByteToInt(bytes[start]));
      if (start + length > bytes.length) length = bytes.length - start;
      for (int i = 1; i < length; i++) string.append(separator).append(Integer.toString(unsignedByteToInt(bytes[start + i]), radix));
    }
    return string.toString();
  }

  /**
   * Convert a dot-punctuated decimal String to a byte[], treating the bytes as unsigned
   * @param string a dot-punctuated decimal String
   * @return a byte[]
   */
  public static byte[] stringToUnsignedBytes(String string) {
    return stringToUnsignedBytes(string, 10, '.');
  }

  /**
   * Convert a separator-punctuated String with the given radix to a byte[], treating the bytes as unsigned
   * The resulting byte[] will have whatever length is required to represent the contents of the String
   * @param string a separator-punctuated String with the given radix
   * @param radix the radix of the bytes in the String
   * @param separator the character separating the bytes in the string
   * @return a byte[]
   */
  public static byte[] stringToUnsignedBytes(String string, int radix, char separator) {
    StringTokenizer tokens = new StringTokenizer(string, (new Character(separator)).toString());
    byte[]          bytes = new byte[tokens.countTokens()];
    int i = 0;
    while (tokens.hasMoreTokens()) bytes[i++] = (byte)Integer.parseInt(tokens.nextToken(), radix);
    return bytes;
  }

  /**
   * Convert a separator-punctuated String with the given radix to a byte[length], treating the bytes as unsigned
   * If the String represents more than length bytes, only the first length bytes are converted
   * If the String represents less than length bytes, the subsequent bytes in byte[] are zeroed
   * @param string a separator-punctuated String with the given radix
   * @param length the size of the resulting byte[]
   * @param radix the radix of the bytes in the String
   * @param separator the character separating the bytes in the string
   * @return a byte[]
   */
  public static byte[] stringToUnsignedBytes(String string, int length, int radix, char separator) {
    StringTokenizer tokens = new StringTokenizer(string, (new Character(separator)).toString());
    byte[]          bytes = new byte[length];
    int i = 0;
    while (i < length && tokens.hasMoreTokens()) bytes[i++] = (byte)Integer.parseInt(tokens.nextToken(), radix);
    while (i < length) bytes[i++] = 0;
    return bytes;
  }

  /**
   * Convert a byte[] to an int, treating the bytes as unsigned
   * @param bytes a byte[]
   * @return an int
   */
  public static int unsignedBytesToInt(byte[] bytes) {
    int result = 0;
    for (int i = 0; i < bytes.length; i++) result = result * 256 + (int)(unsignedByteToInt(bytes[i]));
    return result;
  }

  /**
   * Convert a byte[] to an long, treating the bytes as unsigned
   * @param bytes a byte[]
   * @return a long
   */
  public static long unsignedBytesToLong(byte[] bytes) {
    long result = 0;
    for (int i = 0; i < bytes.length; i++) result = result * 256 + (long)(unsignedByteToInt(bytes[i]));
    return result;
  }

  /**
   * Convert a byte to an int, treating the byte as unsigned
   * @param ubyte the byte
   * @return the int
   */
  public static int unsignedByteToInt(byte ubyte) {
    return (int)ubyte & 0xff;
  }

  /**
   * Convert a short to an int, treating the short as unsigned
   * @param ushort the short
   * @return the int
   */
  public static int unsignedShortToInt(short ushort) {
    return (int)ushort & 0xffff;
  }

  /**
   * Return a String representing the DHCPMessage
   * @param prefix a String to precede each line in the resulting String
   * @return String representation of the DHCPMessage
   */
  public String toString(String prefix) {
    StringBuffer buffer = new StringBuffer(super.toString());
    buffer.append(":\n");
    buffer.append(prefix).append("op     = ");
    buffer.append(op == 1 ? "client->server" : op == 2 ? "server->client" : Integer.toString(unsignedByteToInt(op))).append('\n');
    buffer.append(prefix).append("htype  = ").append(htype == 1 ? "ethernet" : Integer.toString(unsignedByteToInt(htype))).append('\n');
    buffer.append(prefix).append("hlen   = ").append(unsignedByteToInt(hlen)).append('\n');
    buffer.append(prefix).append("hops   = ").append(unsignedByteToInt(hops)).append('\n');
    buffer.append(prefix).append("xid    = ").append(xid).append('\n');
    buffer.append(prefix).append("secs   = ").append(unsignedShortToInt(secs)).append('\n');
    buffer.append(prefix).append("flags  = ").append(unsignedShortToInt(flags)).append('\n');
    buffer.append(prefix).append("ciaddr = ").append(unsignedBytesToString(ciaddr)).append('\n');
    buffer.append(prefix).append("yiaddr = ").append(unsignedBytesToString(yiaddr)).append('\n');
    buffer.append(prefix).append("siaddr = ").append(unsignedBytesToString(siaddr)).append('\n');
    buffer.append(prefix).append("giaddr = ").append(unsignedBytesToString(giaddr)).append('\n');
    buffer.append(prefix).append("chaddr = ").append(unsignedBytesToString(chaddr, unsignedByteToInt(hlen), 16, ':')).append('\n');
    buffer.append(prefix).append("sname  = ").append(new String(sname)).append('\n');
    buffer.append(prefix).append("file   = ").append(new String(file)).append('\n');
    buffer.append(prefix).append("options:\n").append(optionTable.toString(prefix + prefix));
    return buffer.toString();
  }

  /**
   * Return a String representing the DHCPMessage
   * Overrides toString() method in Object
   * @return String representation of the DHCPMessage
   */
  public String toString() {
    return toString("");
  }
}
