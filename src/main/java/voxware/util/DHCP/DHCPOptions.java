package voxware.util.DHCP;

import java.util.*;

/**
 * A Hashtable of the options for a DHCP message, hashed by option code
 * Based on edu.bucknell.net.JDHCP.DHCPOptions
 * Created Feb 22, 2003 (DCV)
 */
public class DHCPOptions extends Hashtable {

   // The separator for String lists
  public static String separator = ", ";

   // The length of an unlimited lease
  public static final long UnlimitedLease = 0x0ffffffffL;

   // DHCP Option Codes

   // DHCP-Specific Options
  public static final int Pad                   = 0;   // (Has no length field) Padding (ignored when the options field is interpreted)
  public static final int VendorSpecificOptions = 43;  // (n bytes)  Information interpreted according to the client vendor type (VendorClassId)
  public static final int RequestedAddress      = 50;  // (4 bytes)  The IP address that a client requests when unsure that its current address is valid
  public static final int LeaseTime             = 51;  // (4 bytes)  The (unsigned) duration of the lease on an address assigned to the client
  public static final int OptionOverload        = 52;  // (1 byte)   Bits 0 and 1 specify respectively that the file and/or sname fields contain options
  public static final int MessageType           = 53;  // (1 byte)   The type of message (DHCPDISCOVER, DHCPOFFER, DHCPREQUEST, etc.)
  public static final int ServerIdentifier      = 54;  // (4 bytes)  The IP address of the server involved in the DHCP transaction
  public static final int ParameterRequestList  = 55;  // (n bytes)  A list of DHCP option codes for options requested by the client
  public static final int Message               = 56;  // (n bytes)  A message for the recipient to log or report to the user
  public static final int MaximumMessageSize    = 57;  // (2 bytes)  An unsigned quantity not less than DHCPMessage.DefaultMaximumSize
  public static final int LeaseRenewalTime      = 58;  // (4 bytes)  The (unsigned) time (T1) when the client can begin trying to extend its lease
  public static final int RebindingTime         = 59;  // (4 bytes)  The (unsigned) time (T2) when the client must seek a new server to extend its lease
  public static final int VendorClassId         = 60;  // (n bytes)  Vendor type and configuration
  public static final int ClientIdentifier      = 61;  // (n bytes)  A byte[] used by servers in place of htype and chaddr
  public static final int TFTPServerName        = 66;  // (n bytes)  The TFTP server for the client to use (when the sname field has been suborned by options)
  public static final int BootFileName          = 67;  // (n bytes)  The boot file for the client to use (when the file field has been suborned by options)
  public static final int End                   = 255; // (Has no length field)  The end of a message's options segment

   // Host Configuration Options
  public static final int TimeOffset            = 2;   // (4 bytes)  The (signed) offset from UTC that the client should use
  public static final int HostName              = 12;  // (n bytes)  The client's name, optionally qualified with the local domain name
  public static final int BootFileSize          = 13;  // (2 bytes)  The (unsigned) number of 512-byte blocks in the client's bootfile
  public static final int MeritDumpFile         = 14;  // (n bytes)  The name of a file where the client should dump a core image if it crashes
  public static final int DomainName            = 15;  // (n bytes)  The name of the client's domain for resolving names in the Domain Name System (DNS)
  public static final int SwapServer            = 16;  // (4 bytes)  The IP address of a server to provide swap space for the client
  public static final int RootPathName          = 17;  // (n bytes)  The full pathname of the directory being used as the client's root disk partition
  public static final int ExtensionsPath        = 18;  // (n bytes)  The full pathname of a file that contains additional DHCP options for the client
  public static final int UserClass             = 77;  // (n bytes)  Textual information the client can use to identify the type of user or application
  public static final int ClientFQDN            = 81;  // (n bytes)  The client's Fully Qualified Domain Name

   // TCP/IP Stack Configuration Options
  public static final int SubnetMask            = 1;   // (4 bytes)  The subnet mask for the interface to which the DHCP message is delivered
  public static final int Routers               = 3;   // (4n bytes) A list of n IP addresses of default routers for a client to add to its routing table
  public static final int Forward               = 19;  // (1 byte)   A boolean telling the client whether to forward IP datagrams between its interfaces
  public static final int ForwardNonlocal       = 20;  // (1 byte)   A boolean telling the client whether to forward datagrams with nonlocal source routes
  public static final int PolicyFilter          = 21;  // (8n bytes) A list of n address filters to apply to datagrams with nonlocal source routes
  public static final int MaxReassemblySize     = 22;  // (2 bytes)  The size of the largest fragmented IP datagram the client is prepared to reassemble
  public static final int DefaultIPTimeToLive   = 23;  // (1 byte)   The default TTL for the client to put in the header of the datagrams it transmits
  public static final int PMTUAgeingTimeout     = 24;  // (4 bytes)  The (unsigned) timeout in seconds for the client to use to age PMTU values
  public static final int PMTUPlateauTable      = 25;  // (2n bytes) A table of n unsigned 16-bit MTU sizes for use by the PMTU mechanism
  public static final int InterfaceMTU          = 26;  // (2 bytes)  The MTU for an interface
  public static final int AllSubnetsLocal       = 27;  // (1 byte)   A boolean telling the client whether to assume that all subnets have the same MTU
  public static final int BroadcastAddress      = 28;  // (4 bytes)  The IP broadcast address for the network to which a configured interface is attached
  public static final int DiscoverMask          = 29;  // (1 byte)   A boolean telling the client whether to use ICMP to get the subnet mask for an interface
  public static final int SupplySubnetMask      = 30;  // (1 byte)   A boolean telling the client whether to respond to ICMP address mask request messages
  public static final int DiscoverRouter        = 31;  // (1 byte)   A boolean telling the client whether to initiate the router discovery protocol
  public static final int SolicitationAddress   = 32;  // (4 bytes)  The IP address the client should use to transmit router discovery protocol messages
  public static final int StaticRoutes          = 33;  // (8n bytes) A list of n IP address pairs for the client to add to its routing table
  public static final int EncapsulateTrailers   = 34;  // (1 byte)   A boolean telling the client whether to negotiate using trailers by using ARP
  public static final int ARPCacheTimeout       = 35;  // (4 bytes)  The (unsigned) lifetime in seconds of entries in the ARP cache
  public static final int EthernetEncapsulation = 36;  // (1 byte)   A code selecting the type of encapsulation for IP datagrams
  public static final int DefaultTCPTimeToLive  = 37;  // (1 byte)   The default TTL that the TCP layer should use when sending TCP segments
  public static final int TCPKeepaliveInterval  = 38;  // (4 bytes)  The (unsigned) time in seconds that the client should wait before a keepalive segment
  public static final int TCPKeepaliveGarbage   = 39;  // (1 byte)   A boolean telling the client whether to send a garbage byte in keepalive segments

   // Service Parameter Options
  public static final int TimeServers           = 4;   // (4n bytes) A list of n IP addresses of RFC 868 time servers available to the client
  public static final int NameServers           = 5;   // (4n bytes) A list of n IP addresses of Internet Name Servers available to the client
  public static final int DomainNameServers     = 6;   // (4n bytes) A list of n IP addresses of Domain Name Service (DNS) servers available to the client
  public static final int LogServers            = 7;   // (4n bytes) A list of n IP addresses of logging servers available to the client
  public static final int QuotesServers         = 8;   // (4n bytes) A list of n IP addresses of quote of the day servers available to the client
  public static final int LPRServers            = 9;   // (4n bytes) A list of n IP addresses of Line Printer Protocol (LPR) servers available to the client
  public static final int ImpressServers        = 10;  // (4n bytes) A list of n IP addresses of Impress print servers available to the client
  public static final int RLPServers            = 11;  // (4n bytes) A list of n IP addresses of Resource Location Protocol servers available to the client
  public static final int NISDomainName         = 40;  // (n bytes)  The name of the client's NIS domain
  public static final int NISServers            = 41;  // (4n bytes) A list of n IP addresses of NIS servers available to the client
  public static final int NTPServers            = 42;  // (4n bytes) A list of n IP addresses of Network Time Protocol (NTP) servers available to the client
  public static final int WINSServers           = 44;  // (4n bytes) A list of n IP addresses of Windows Internet Naming Service servers available
  public static final int NBDDServers           = 45;  // (4n bytes) A list of n IP addresses of NetBIOS over datagram distribution servers available
  public static final int NetBIOSNodeType       = 46;  // (1 byte)   A code specifying the type of NetBIOS node to which the client should configure itself
  public static final int NetBIOSScope          = 47;  // (n bytes)  A string of characters specifying the NetBIOS over TCP/IP scope for the client
  public static final int XFontServers          = 48;  // (4n bytes) A list of n IP addresses of X Window System Font Servers available to the client
  public static final int XDisplayManagers      = 49;  // (4n bytes) A list of n IP addresses of X Window System Display Managers available to the client
  public static final int NetWareIPDomainName   = 62;  // (n bytes)  The name of the NetWare/IP domain for the client
  public static final int NetWareIPInformation  = 63;  // (n bytes)  Additional NetWare/IP information for the client
  public static final int NISPlusDomainName     = 64;  // (n bytes)  The name of the client's NIS+ domain
  public static final int NISPlusServers        = 65;  // (4n bytes) A list of n IP addresses of NIS+ version 2 servers available to the client
  public static final int MobileIPHomeAgents    = 68;  // (4n bytes) A list of n IP addresses of mobile IP protocol home agents available to the client
  public static final int SMTPServers           = 69;  // (4n bytes) A list of n IP addresses of SMTP servers available to the client
  public static final int POP3Servers           = 70;  // (4n bytes) A list of n IP addresses of Post Office Protocol (POP3) servers available to the client
  public static final int NNTPServers           = 71;  // (4n bytes) A list of n IP addresses of Network News Transport Protocol (NNTP) servers available
  public static final int WWWServers            = 72;  // (4n bytes) A list of n IP addresses of World Wide Web (HTTP) servers available to the client
  public static final int FingerServers         = 73;  // (4n bytes) A list of n IP addresses of RFC 1288 finger protocol servers available to the client
  public static final int IRCServers            = 74;  // (4n bytes) A list of n IP addresses of Internet Relay Chat (IRC) servers available to the client
  public static final int StreetTalkServers     = 75;  // (4n bytes) A list of n IP addresses of StreetTalk servers available to the client
  public static final int StreetTalkDAServers   = 76;  // (4n bytes) A list of n IP addresses of StreetTalk Directory Assistance servers available
  public static final int SLPDirectoryAgents    = 78;  // (4n + 1 bytes) A list of n IP addresses of Service Location Protocol servers available + a flag
  public static final int SLPScopes             = 79;  // (n + 1 bytes) A comma-delimited list of the scopes the client's SLP agent should use + a flag
  public static final int RelayAgentInfo        = 82;  // (n bytes)  Information about a DHCP client from a DHCP relay agent to a DHCP server
  public static final int NDSServers            = 85;  // (4n bytes) A list of n IP addresses of NetWare Directory Services (NDS) servers available
  public static final int NDSTreeName           = 86;  // (n bytes)  A UTF-8 encoded string containing the name of the NDS tree available to the client
  public static final int NDSContext            = 87;  // (n bytes)  A UTF-8 encoded string containing the initial NDS context for the client
  public static final int Authentication        = 90;  // (n bytes)  Information for authenticating the identity of DHCP clients and servers
  public static final int UAPServers            = 98;  // (4n bytes) A list of n IP addresses of User Authentication Protocol (UAP) servers available
  public static final int NameServicePriorities = 117; // (2n bytes) A list of DHCP option codes specifying the order in which to search name services
  public static final int SubnetSelection       = 118; // (4 bytes)  An IP address in the subnet from which the client wants to be assigned an address

  public static String[] names = new String[256];
  static {
    names[Pad] = "Pad";
    names[VendorSpecificOptions] = "VendorSpecificOptions";
    names[RequestedAddress] = "RequestedAddress";
    names[LeaseTime] = "LeaseTime";
    names[OptionOverload] = "OptionOverload";
    names[MessageType] = "MessageType";
    names[ServerIdentifier] = "ServerIdentifier";
    names[ParameterRequestList] = "ParameterRequestList";
    names[Message] = "Message";
    names[MaximumMessageSize] = "MaximumMessageSize";
    names[LeaseRenewalTime] = "LeaseRenewalTime";
    names[RebindingTime] = "RebindingTime";
    names[VendorClassId] = "VendorClassId";
    names[ClientIdentifier] = "ClientIdentifier";
    names[TFTPServerName] = "TFTPServerName";
    names[BootFileName] = "BootFileName";
    names[End] = "End";
    names[TimeOffset] = "TimeOffset";
    names[HostName] = "HostName";
    names[BootFileSize] = "BootFileSize";
    names[MeritDumpFile] = "MeritDumpFile";
    names[DomainName] = "DomainName";
    names[SwapServer] = "SwapServer";
    names[RootPathName] = "RootPathName";
    names[ExtensionsPath] = "ExtensionsPath";
    names[UserClass] = "UserClass";
    names[ClientFQDN] = "ClientFQDN";
    names[SubnetMask] = "SubnetMask";
    names[Routers] = "Routers";
    names[Forward] = "Forward";
    names[ForwardNonlocal] = "ForwardNonlocal";
    names[PolicyFilter] = "PolicyFilter";
    names[MaxReassemblySize] = "MaxReassemblySize";
    names[DefaultIPTimeToLive] = "DefaultIPTimeToLive";
    names[PMTUAgeingTimeout] = "PMTUAgeingTimeout";
    names[PMTUPlateauTable] = "PMTUPlateauTable";
    names[InterfaceMTU] = "InterfaceMTU";
    names[AllSubnetsLocal] = "AllSubnetsLocal";
    names[BroadcastAddress] = "BroadcastAddress";
    names[DiscoverMask] = "DiscoverMask";
    names[SupplySubnetMask] = "SupplySubnetMask";
    names[DiscoverRouter] = "DiscoverRouter";
    names[SolicitationAddress] = "SolicitationAddress";
    names[StaticRoutes] = "StaticRoutes";
    names[EncapsulateTrailers] = "EncapsulateTrailers";
    names[ARPCacheTimeout] = "ARPCacheTimeout";
    names[EthernetEncapsulation] = "EthernetEncapsulation";
    names[DefaultTCPTimeToLive] = "DefaultTCPTimeToLive";
    names[TCPKeepaliveInterval] = "TCPKeepaliveInterval";
    names[TCPKeepaliveGarbage] = "TCPKeepaliveGarbage";
    names[TimeServers] = "TimeServers";
    names[NameServers] = "NameServers";
    names[DomainNameServers] = "DomainNameServers";
    names[LogServers] = "LogServers";
    names[QuotesServers] = "QuotesServers";
    names[LPRServers] = "LPRServers";
    names[ImpressServers] = "ImpressServers";
    names[RLPServers] = "RLPServers";
    names[NISDomainName] = "NISDomainName";
    names[NISServers] = "NISServers";
    names[NTPServers] = "NTPServers";
    names[WINSServers] = "WINSServers";
    names[NBDDServers] = "NBDDServers";
    names[NetBIOSNodeType] = "NetBIOSNodeType";
    names[NetBIOSScope] = "NetBIOSScope";
    names[XFontServers] = "XFontServers";
    names[XDisplayManagers] = "XDisplayManagers";
    names[NetWareIPDomainName] = "NetWareIPDomainName";
    names[NetWareIPInformation] = "NetWareIPInformation";
    names[NISPlusDomainName] = "NISPlusDomainName";
    names[NISPlusServers] = "NISPlusServers";
    names[MobileIPHomeAgents] = "MobileIPHomeAgents";
    names[SMTPServers] = "SMTPServers";
    names[POP3Servers] = "POP3Servers";
    names[NNTPServers] = "NNTPServers";
    names[UAPServers] = "UAPServers";
    names[WWWServers] = "WWWServers";
    names[FingerServers] = "FingerServers";
    names[IRCServers] = "IRCServers";
    names[StreetTalkServers] = "StreetTalkServers";
    names[StreetTalkDAServers] = "StreetTalkDAServers";
    names[SLPDirectoryAgents] = "SLPDirectoryAgents";
    names[SLPScopes] = "SLPScopes";
    names[RelayAgentInfo] = "RelayAgentInfo";
    names[NDSServers] = "NDSServers";
    names[NDSTreeName] = "NDSTreeName";
    names[NDSContext] = "NDSContext";
    names[Authentication] = "Authentication";
    names[NameServicePriorities] = "NameServicePriorities";
    names[SubnetSelection] = "SubnetSelection";
  }

  /**
   * An entry in the options table
   */
  private class Option {

    byte   code;
    byte   length;
    byte[] value;

    public Option(byte code, byte length, byte[] value) {
      this.code = code;
      this.length = length;
      this.value = value;
    }

    public String toString() {

      String valueString = null;
      StringBuffer buffer = null;

      switch (code) {
        case MessageType:
           // Value is a DHCP message type
          valueString = DHCPMessage.types[DHCPMessage.unsignedBytesToInt(value)];
          break;
        case Forward:
        case ForwardNonlocal:
        case AllSubnetsLocal:
        case DiscoverMask:
        case SupplySubnetMask:
        case DiscoverRouter:
        case EncapsulateTrailers:
        case TCPKeepaliveGarbage:
           // Value is a boolean
          valueString = value[0] == 0 ? "false" : "true";
          break;
        case RequestedAddress:
        case ServerIdentifier:
        case SwapServer:
        case SubnetMask:
        case BroadcastAddress:
        case SolicitationAddress:
        case SubnetSelection:
           // Value is an IP address
          valueString = DHCPMessage.unsignedBytesToString(value);
          break;
        case LeaseTime:
        case LeaseRenewalTime:
        case RebindingTime:
        case ARPCacheTimeout:
        case TCPKeepaliveInterval:
           // Value is an unsigned 32-bit duration in seconds
          valueString = Long.toString(DHCPMessage.unsignedBytesToLong(value)) + " seconds";
          break;
        case TimeOffset:
           // Value is a signed 32-bit time offset
          valueString = Long.toString(DHCPMessage.unsignedBytesToInt(value)) + " seconds";
          break;
        case MaximumMessageSize:
        case BootFileSize:
        case MaxReassemblySize:
        case InterfaceMTU:
           // Value is an unsigned 16-bit quantity
          valueString = Integer.toString(DHCPMessage.unsignedBytesToInt(value));
          break;
        case Message:
        case VendorClassId:
        case TFTPServerName:
        case BootFileName:
        case HostName:
        case MeritDumpFile:
        case DomainName:
        case RootPathName:
        case ExtensionsPath:
        case UserClass:
        case ClientFQDN:
        case NISDomainName:
        case NetBIOSScope:
        case NetWareIPDomainName:
        case NetWareIPInformation:
        case NISPlusDomainName:
        case RelayAgentInfo:
        case Authentication:
           // Value is a string
          valueString = new String(value);
          break;
        case PMTUPlateauTable:
           // Value is a list of unsigned 16-bit quantities
          buffer = new StringBuffer("{ ");
          if (value.length > 1) {
            byte[] bytes = new byte[2];
            int i = 0;
            do {
              System.arraycopy(value, i, bytes , 0, 2);
              buffer.append(Integer.toString(DHCPMessage.unsignedBytesToInt(bytes)));
              i += 2;
              if (i < value.length - 1) buffer.append(separator);
              else break;
            } while (true);
          }
          valueString = buffer.append(" }").toString();
          break;
        case ParameterRequestList:
        case NameServicePriorities:
           // Value is a list of DHCP option codes
          buffer = new StringBuffer("{ ");
          if (value.length > 0) {
            buffer.append(names[DHCPMessage.unsignedByteToInt(value[0])]);
            for (int i = 1; i < value.length; i++) buffer.append(separator).append(names[DHCPMessage.unsignedByteToInt(value[i])]);
          }
          valueString = buffer.append(" }").toString();
          break;
        case Routers:
        case TimeServers:
        case NameServers:
        case DomainNameServers:
        case LogServers:
        case QuotesServers:
        case LPRServers:
        case ImpressServers:
        case RLPServers:
        case NISServers:
        case NTPServers:
        case WINSServers:
        case NBDDServers:
        case XFontServers:
        case XDisplayManagers:
        case NISPlusServers:
        case MobileIPHomeAgents:
        case SMTPServers:
        case POP3Servers:
        case NNTPServers:
        case WWWServers:
        case FingerServers:
        case IRCServers:
        case StreetTalkServers:
        case StreetTalkDAServers:
        case NDSServers:
        case UAPServers:
           // Value is a list of IP addresses
          buffer = new StringBuffer("{ ");
          if (value.length > 3) {
            int i = 0;
            do {
              buffer.append(DHCPMessage.unsignedBytesToString(value, i, 4, 10, '.'));
              i += 4;
              if (i < value.length - 3) buffer.append(separator);
              else break;
            } while (true);
          }
          valueString = buffer.append(" }").toString();
          break;
        case PolicyFilter:
        case StaticRoutes:
           // Value is a list of pairs of IP addresses
          buffer = new StringBuffer("{ ");
          if (value.length > 7) {
            int i = 0;
            do {
              buffer.append('[').append(DHCPMessage.unsignedBytesToString(value, i, 4, 10, '.'));
              buffer.append(separator).append(DHCPMessage.unsignedBytesToString(value, i + 4, 4, 10, '.')).append(']');
              i += 8;
              if (i < value.length - 7) buffer.append(separator);
              else break;
            } while (true);
          }
          valueString = buffer.append(" }").toString();
          break;
        default:
          valueString = DHCPMessage.unsignedBytesToString(value, value.length, 10, '|');
      }
      return(names[code] + ": length = " + length + ", value = " + valueString);
    }
  }

  public DHCPOptions() {
    super();
  }

  /**
   * Remove the specified option
   * @param code the option code
   */
  public void removeOption(byte code) {
    this.remove(new Byte(code));
  }


  /*
   * Report whether the option has a value
   * @param code the option code
   * @return true if option has a value, false otherwise
   */
  public boolean contains(byte code) {
    return this.containsKey(new Byte(code));
  }

  /**
   * Report whether the options table is empty
   * @return true if there are no options set, otherwise false
   */
  public boolean isEmpty() {
    return this.isEmpty();
  }

  /**
   * Get the value of option
   * @param code the option code
   * @return byte[] containing the value of the option, or null if option is not set
   */
  public byte[] getOption(byte code) {
    if (this.contains(code)) {
      DHCPOptions.Option option = (DHCPOptions.Option) this.get(new Byte(code));
      return option.value;
    } else {
      return null;
    }
  }

  /**
   * Set the value of an option
   * @param code the option code
   * @param value the new value of the option
   */
  public void setOption(byte code, byte[] value) {
    DHCPOptions.Option opt = new DHCPOptions.Option(code, (byte) value.length, value);
    this.put(new Byte(code), opt);
  }

  /**
   * Read an options byte[] into the options table
   * @param optionsArray  the byte[] containing the options
   */
  public void internalize(byte[] optionsArray) {
     // Skip the magic cookie
    int    pos = 4;
    byte   code;
    byte   length;
    byte[] value;
    while (optionsArray[pos] != (byte)End) {
      code = optionsArray[pos++];
      length = optionsArray[pos++];
      value = new byte[length];
      System.arraycopy(optionsArray, pos, value, 0, length);
      setOption(code, value);
      pos += length;
    }
  }

  /**
   * Return a byte[] representing the contents of the options table
   * @return  byte[] representation of options table
   */
  public byte[] externalize() {
    byte[] options = new byte[312];
     // Insert the magic cookie
    options[0] = (byte)99;
    options[1] = (byte)130;
    options[2] = (byte)83;
    options[3] = (byte)99;
    int position = 4;
    for(Enumeration e = this.elements(); e.hasMoreElements(); ) {
      DHCPOptions.Option option = (DHCPOptions.Option) e.nextElement();
      options[position++] = option.code;
      options[position++] = option.length;
      System.arraycopy(option.value, 0, options, position, option.length);
      position += option.length;
    }
    options[position] = (byte)End;
    return options;
  }

  /**
   * Return a String representing the contents of the options table
   * @return String representation of options table
   */
  public String toString(String prefix) {
    StringBuffer buffer = new StringBuffer();
    Enumeration options = elements();
    while (options.hasMoreElements()) buffer.append(prefix).append(((Option)options.nextElement()).toString()).append('\n');
    return buffer.toString();
  }

  /**
   * Return a String representing the contents of the options table
   * Overrides toString() method in Hashtable
   * @return String representation of options table
   */
  public String toString() {
    return toString("");
  }
}
