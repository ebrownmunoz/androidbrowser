package voxware.util.DHCP;

import java.net.*;

/**
 * A DatagramSocket for sending and receiving DHCP Messages
 * Based on edu.bucknell.net.JDHCP.DHCPSocket
 * Created Feb 22, 2003 (DCV)
 */
public class DHCPSocket extends DatagramSocket  {

  public static final int DefaultPacketSize    = 1500;      // Default Maximum Transfer Unit (MTU) for ethernet
  public static final int DefaultSocketTimeout = 3000;      // 3 seconds

  protected int packetSize = DefaultPacketSize;

  /** 
   * Construct a DHCPSocket on a specific port on the local machine
   * @param port  the port to bind
   */
  public DHCPSocket(int port) throws SocketException {
    super(port);
    setSoTimeout(DefaultSocketTimeout);
  }

  /**
   * Set the MTU for this socket
   * @param size integer representing desired MTU
   */
  public void setMTU(int size) {
    packetSize = size;
  }

  /**
   * Return the MTU for this socket
   * @return the Maximum Transfer Unit for this socket
   */
  public int getMTU() {
    return packetSize;
  }

  /**
   * Send the given DHCPMessage object in a datagram to a given host
   * @param message  the DHCPMessage to send
   */
  public synchronized void send(DHCPMessage message) throws java.io.IOException {
    byte[] data = message.externalize();
    InetAddress dest = null;
    try {
      dest = InetAddress.getByName(message.getDestinationAddress());
    } catch (UnknownHostException e) {}
    send(new DatagramPacket(data, data.length, dest, message.getPort()));
  }

  /** 
   * Receive a datagram containing a DHCP message into the given DHCPMessage object
   * @return true if message is received, false if timeout or other I/O exception occurs  
   * @param message  the DHCPMessage object to receive the message
   */
  public synchronized boolean receive(DHCPMessage message)  {
    try {
      DatagramPacket incoming = new DatagramPacket(new byte[packetSize], packetSize);
       // Block on receive for SO_TIMEOUT
      receive(incoming);
      message.internalize(incoming.getData());
    } catch (java.io.IOException e) {
      return false;
    }
    return true;
  }
}
