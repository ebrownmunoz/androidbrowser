package voxware.util;

import java.io.*;
import java.util.Date;

/**------------------------------------------------------------*
 * A java.io.File with additional methods for manipulating the
 * underlying physical file object, if any
 *------------------------------------------------------------**/

public class File extends java.io.File {

  private native boolean performSetWriteable(String path, boolean writeable);

  public static int     MaxDirectoryDepth  = 64;
  public static int     MaxSizeFieldLength = 10;
  public static String  NewLine = System.getProperty("line.separator");
  public static String  DirectorySeparator = System.getProperty("file.separator");
  public static int     DirectorySeparatorLength = DirectorySeparator.length();

  private static boolean nativeLoaded = false;
  static {
    try {
      // LibraryLoader.loadLibrary("jutil");
      nativeLoaded = true;
    } catch(UnsatisfiedLinkError e) {
      SysLog.println("voxware.util.File: ERROR - jutil library does not exist");
    } catch (SecurityException e) {
      SysLog.println("voxware.util.File: ERROR - jutil library load fails due to: " + e.toString());
    }
  }

   // Construct a new File from a parent abstract pathname and a child pathname string
  public File(File parent, String child) {
    super(parent, child);
  }

   // Construct a new File by converting the given pathname string into an abstract pathname.
  public File(String pathname) {
    super(pathname);
  }

   // Construct a new File from a parent pathname string and a child pathname string
  public File(String parent, String child) {
    super(parent, child);
  }

   // Copy this file or directory, including any files or directories it may contain, and return true iff successful
  public boolean copyTo(File target) throws IllegalArgumentException, IOException, SecurityException {
    return copyTo(target, false, false);
  }

   // Copy this file or directory, including any files or directories it may contain, and return true iff successful
  public boolean copyTo(File target, boolean ifYounger, boolean keepTime) throws IllegalArgumentException, IOException, SecurityException {
    boolean copied = true;
    if (this.exists()) {
       // If this is a directory, copy it first, then copy its contents
      if (this.isDirectory()) {
        boolean preexisting;
        if (target.exists()) {
          if (!target.isDirectory()) throw new IllegalArgumentException(target.getCanonicalPath() + " is not a directory");
          preexisting = true;
        } else {
          target.mkdirs();
          preexisting = false;
        }
        String[] directory = this.list();
        for (int entry = 0; entry < directory.length; entry++)
          copied = copied && new voxware.util.File(this, directory[entry]).copyTo(new File(target, directory[entry]), ifYounger, keepTime);
           // Throws IllegalArgumentException, IOException, SecurityException
        // if (!preexisting && keepTime) target.setLastModified(this.lastModified());  // (1.2)
      } else if (!ifYounger || this.lastModified() > target.lastModified()) {
         // If necessary, create all the directories in the path to the target file
        (new File(target.getParent())).mkdirs();                      // Throws SecurityException
        try {
           // Acquire a byte[] from this file
          FileInputStream inputStream = new FileInputStream(this);    // Throws FileNotFoundException, SecurityException
          byte[] bytes = new byte[(int)this.length()];
          inputStream.read(bytes);                                    // Throws IOException
           // Write it out to the target file
          FileOutputStream outputStream = new FileOutputStream(target); // Throws FileNotFoundException, SecurityException
          outputStream.write(bytes);                                  // Throws IOException
          // if (keepTime) target.setLastModified(this.lastModified());  // Throws IllegalArgumentException, SecurityException (1.2)
          inputStream.close();
          outputStream.close();
          if (SysLog.printLevel > 0)
            SysLog.println("voxware.util.File.copy: copied " + this.getCanonicalPath() + " to " + target.getCanonicalPath());  // Throws IOException, Security Exception
        } catch (FileNotFoundException e) {
          copied = false;
          SysLog.println("voxware.util.File.copy: WARNING - cannot open or create target file " + target.getCanonicalPath() + " to copy to");  // Throws IOException, SecurityException
        }
      }
    }
    return copied;
  }

   // Delete this file or directory, including any files or directories it may contain
  public boolean delete() throws SecurityException {
    boolean deleted = true;
    try {
       // If this is a directory, delete its contents first
      if (this.isDirectory()) {
        String[] directory = this.list();
        for (int entry = 0; entry < directory.length; entry++)
          deleted = deleted && new voxware.util.File(this, directory[entry]).delete();  // Throws IOException, SecurityException
      }
       // Delete this
      if (!super.delete()) {                                          // Throws IOException, SecurityException
        deleted = false;
        SysLog.println("voxware.util.File.delete: WARNING - could not delete " + this.getCanonicalPath()); // Throws IOException, SecurityException
      } else if (SysLog.printLevel > 0) {
        SysLog.println("voxware.util.File.delete: deleted " + this.getCanonicalPath()); // Throws IOException, SecurityException
      }
    } catch (IOException e) {
      deleted = false;
    }
    return deleted;
  }

   // Remove this file or directory, including any files or directories it may contain
   // This synonym for delete() is necessary when using JS LiveConnect, since "delete" is a reserved word in JS
  public boolean remove() throws SecurityException {
    return delete();
  }

   // If this is a directory, return a list of all the files and directories in it; otherwise, just return the file name
  public StringBuffer list(int indentation, boolean size, boolean time, boolean recursive, String separator) throws IOException, SecurityException {
    StringBuffer buffer = null;
    if (this.exists()) {
      String name = this.getCanonicalPath();
      if (this.isDirectory()) name += DirectorySeparator;
      StringFormatter.Field[] fields = {
        new StringFormatter.Field(name),                              // Throws IOException, SecurityException
        new StringFormatter.Field(-MaxSizeFieldLength, size ? Long.toString(this.length()) : null, " "),
        new StringFormatter.Field(time ? new Date(this.lastModified()).toString() : null)
      };
      buffer = new StringFormatter(fields).toStringBuffer();
       // If the this is a directory, tack on the list of its contents
      if (this.isDirectory()) this.list(buffer, separator, indentation, this.maxNameLength(0, recursive), 1, size, time, recursive);
    }
    return buffer;
  }

  protected void list(StringBuffer buffer, String separator, int indentation, int length, int level, boolean size, boolean time, boolean recursive)
  throws IOException, SecurityException {
    String[] directory = this.list();
    if (directory != null) {
      for (int index = 0; index < directory.length; index++) {
        voxware.util.File entry = new voxware.util.File(this, directory[index]);  // Throws IOException, SecurityException
        String name = entry.getName();
        if (entry.isDirectory()) name += DirectorySeparator;
        StringFormatter.Field[] fields = {
          new StringFormatter.Field(-(indentation * level), ""),
          new StringFormatter.Field(length, name),                      // Throws IOException, SecurityException
          new StringFormatter.Field(-MaxSizeFieldLength, size ? Long.toString(entry.length()) : null, " "),
          new StringFormatter.Field(time ? new Date(entry.lastModified()).toString() : null)
        };
        new StringFormatter(fields).appendTo(buffer.append(separator));
         // If recursion is requested and this is a directory, tack on the list of its contents
        if (recursive && entry.isDirectory()) entry.list(buffer, separator, indentation, length, level + 1, size, time, recursive);
      }
    }
  }

   // Return the length of the longest file name in the hierarchy
  protected int maxNameLength(int maxLength, boolean recursive) throws IOException, SecurityException {
    String[] directory = this.list();
    if (directory != null) {
      for (int index = 0; index < directory.length; index++) {
        voxware.util.File entry = new voxware.util.File(this, directory[index]);  // Throws IOException, SecurityException
        int length = entry.getName().length();
         // Allow for appending the path separator
        if (entry.isDirectory()) length += DirectorySeparatorLength;
        if (length > maxLength) maxLength = length;
        if (recursive && entry.isDirectory()) maxLength = entry.maxNameLength(maxLength, recursive);
      }
    }
    return maxLength;
  }

  /* return true if file writeability set, false otherwise */
  public boolean setWriteable(boolean writeable) {
    return performSetWriteable(getPath(), writeable);
  }
}
