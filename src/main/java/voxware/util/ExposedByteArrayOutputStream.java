package voxware.util;

import java.io.ByteArrayOutputStream;

public class ExposedByteArrayOutputStream extends ByteArrayOutputStream implements ByteArrayWrapper {

  public ExposedByteArrayOutputStream()         { super(); }
  public ExposedByteArrayOutputStream(int size) { super(size); }

  public byte[] getBytes() { return buf; }
}
