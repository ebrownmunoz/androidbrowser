#ifndef __voxware_util_RegistryEntry__
#define __voxware_util_RegistryEntry__

#pragma interface

#include <gcj/array.h>

extern "Java"
{
  namespace voxware
  {
    namespace util
    {
      class RegistryEntry;
    }
  }
}

class voxware::util::RegistryEntry : public ::java::lang::Object
{
public:
  RegistryEntry ();
  RegistryEntry (::java::lang::String *);
  RegistryEntry (::java::lang::String *, ::java::lang::String *, ::java::lang::String *);
  RegistryEntry (::java::lang::String *, ::java::lang::String *, ::java::lang::String *, ::java::lang::Object *);
  RegistryEntry (::java::lang::String *, ::java::lang::String *);
  virtual jboolean read ();
  virtual jboolean write ();
  virtual jboolean remove ();
  virtual jboolean isRootKey (::java::lang::String *);
public:  // actually protected
  virtual jint getType ();
public:
  virtual ::java::lang::String *getRoot () { return root; }
  virtual void setRoot (::java::lang::String *);
  virtual ::java::lang::String *getPath () { return path; }
  virtual void setPath (::java::lang::String *);
  virtual ::java::lang::String *getName () { return name; }
  virtual void setName (::java::lang::String *);
  virtual ::java::lang::Object *getValue () { return value; }
  virtual void setValue (::java::lang::Object *);
  virtual ::java::lang::String *getStringValue ();
  virtual ::java::lang::Long *getLongValue ();
  virtual JArray< ::java::lang::String *> *getStringArrayValue ();
  virtual jbyteArray getByteArrayValue ();
  virtual JArray< ::voxware::util::RegistryEntry *> *getRegistryEntryArrayValue ();
  virtual jboolean isDirectory ();
  virtual jboolean isKey ();
  virtual ::java::lang::StringBuffer *appendTo (::java::lang::StringBuffer *);
  virtual ::java::lang::StringBuffer *appendTo (::java::lang::StringBuffer *, ::java::lang::String *);
  virtual ::java::lang::String *toString ();
  virtual ::java::lang::String *toString (::java::lang::String *);
public:  // actually protected
  virtual ::java::lang::String *ubyte (jbyte);
  virtual ::java::lang::String *localize (::java::lang::String *);
  virtual ::java::lang::String *generalize (::java::lang::String *);
  virtual void parse (::java::lang::String *) { }
public:
  virtual jboolean isValidType (::java::lang::Object *);
  static jint MaxValueLength;
  static jint MaxRootLength;
  static jint MaxPathLength;
  static jint MaxNameLength;
  static ::java::lang::String *UntypedTypeName;
  static ::java::lang::String *ByteArrayTypeName;
  static ::java::lang::String *LongTypeName;
  static ::java::lang::String *StringArrayTypeName;
  static jchar PathSeparator;
  static jchar RegistryPathSeparator;
  static ::java::lang::String *DefaultArrayDelimiter;
  static ::java::lang::String *RegistryEntryDelimiter;
public:  // actually protected
  ::java::lang::String * __attribute__((aligned(__alignof__( ::java::lang::Object )))) root;
  ::java::lang::String *path;
  ::java::lang::String *name;
  ::java::lang::Object *value;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_util_RegistryEntry__ */
