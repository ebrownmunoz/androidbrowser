package voxware.hw;

import java.io.UnsupportedEncodingException;

public class Device {
  private static final String DFLT_LIBRARY = "/opt/voxware/lib/voxconfig.dll";
  private static final String DFLT_FUNCTION = "invoke";

  private native boolean natInitialize();
  private native byte[] natInvoke(String libname, String fcn, byte[] input);

  private static Device device;
  private Log log;

  private Device() {
    log = new Log(this.getClass().getName(), Log.DEBUG, System.out);

    if (!natInitialize()) {
      throw new UnsatisfiedLinkError("Failed to initialize hw Device layer");
    }
  }

  public synchronized static Device getInstance() {
    if (device == null) {
      device = new Device();
    }

    return device;
  }

  public static String invoke(String input) {
    return invoke(DFLT_LIBRARY, DFLT_FUNCTION, input);
  }

  public static String invoke(String library, String function, String input) {
    Device device = getInstance();

    String output = null;
    try {
      byte[] bytes = device.invoke(library, function, (input != null) ? input.getBytes("UTF8") : null);
      if (bytes != null) {
        output = new String(bytes, "UTF8");
      }
    } catch (UnsupportedEncodingException uee) {
      uee.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return output;
  }

 public static byte[] invoke(String library, String function, byte[] input) {
    Device device = getInstance();

    byte[] output = null;
    try {
      output = device.invoke(library, function, input, 0);
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    return output;
  }

  public byte[] invoke(String library, String function, byte[] input, int duncel) {
    byte[] ret;
    if (input == null) {
      input = new byte[0];
    }

    /* ret is null when there is an error */
    ret = natInvoke(library, function, input);

    if (ret == null) {
      if (log.isLoggable(Log.DEBUG))
        log.debug("Device.invoke: FAILED - null returned");
				}

    return ret;
  }
}