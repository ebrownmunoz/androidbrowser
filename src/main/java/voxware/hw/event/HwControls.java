/**
 * State and controls for the client hardware
 */

package voxware.hw.event;

import voxware.util.LibraryLoader;
import voxware.util.SysLog;

public class HwControls {

  public static final String LibraryName = "hw";
  public static final String LibraryInitializer = "voxware.hw.event.Initializer.initialize";
  public static final String XVoxOEMString = "XVOX"; // This MUST correspond to the definition in vbx/include/platform.h

  static boolean loadLibrary() {
    // return LibraryLoader.loadLibrary(HwControls.LibraryName, HwControls.LibraryInitializer, null);
	  return true; // ERK
  }

  private static boolean nativeLoaded = HwControls.loadLibrary();
  private static HwControls controls = null;

  protected HwControls() {
     // Following dummy assignments make sure that referenced classes are initialized,
     // and their static fields/methods can be accessed safely from CNI code.
    int i = RadioStatusHwEvent.RADIO_STATUS_STRONG;
  }

 /**
  * Return the sole instance of the hardware controls, constructing it if necessary
  * @return HwControls - controls for client hardware
  */
  public static HwControls getInstance() {
    if (controls == null) controls = new HwControls();
    return(controls);
  }

  synchronized public String getMacAddr(String adapterHint) {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.getMacAddr(" + adapterHint + "): called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.getMacAddr(" + adapterHint + "): called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(HWC_getMacAddr(adapterHint));
    else              return("00:00:00:00:00:00");
  }

  synchronized public String getOEMString() {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.getOEMString(): called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.getOEMString(): called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(HWC_getOEMString());
    else              return("Unavailable");
  }

  synchronized public int getBatteryCharge() {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.getBatteryCharge: called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.getBatteryCharge: called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(HWC_getBatteryCharge());
    else              return(100);
  }

  synchronized public int getRadioStatus() {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.getRadioStatus: called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.getRadioStatus: called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(HWC_getRadioStatus());
    else              return(RadioStatusHwEvent.RADIO_STATUS_STRONG);
  }

  synchronized public void setPowerMgmt(boolean state) {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.setPowerMgmt(" + state + "): called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.setPowerMgmt(" + state + "): called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) HWC_setPowerMgmt(state);
  }

  synchronized public boolean getPowerMgmt() {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.getPowerMgmt: called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.getPowerMgmt: called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(HWC_getPowerMgmt());
    else              return(false);
  }

  synchronized public int flashUpdate(String path) {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.flashUpdate(" + path + "): called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.flashUpdate(" + path + "): called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(InroadFlashUpdate(path));
    else              return(0);
  }

  public int ping(String ipaddr) {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.ping(" + ipaddr + "): called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.ping(" + ipaddr + "): called").printStackTrace(SysLog.stream);
    }
    return (HWC_ping(ipaddr));
  }

  synchronized public int programFlash(String path, int offset) {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.programFlash(" + path + ", " + offset + "): called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.programFlash(" + path + ", " + offset + "): called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(InroadProgramFlash(path, offset));
    else              return(0);
  }

  synchronized public int programPMU(String path) {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.programPMU(" + path + "): called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.programPMU(" + path + "): called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(InroadProgramPMU(path));
    else              return(0);
  }

  synchronized public float getMicGain() {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.getMicGain: called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.getMicGain: called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(HWC_getMicGain());
    else              return(Float.NaN);
  }

   // Range for now is [0.,42.5]dB, with step 1.5dB
  synchronized public void setMicGain(float gain) {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.setMicGain(" + gain + "): called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.setMicGain(" + gain + "): called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) HWC_setMicGain(gain);
  }

  synchronized public float getOutputVolume() {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.getOutputVolume: called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.getOutputVolume: called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(HWC_getOutputVolume());
    else              return(Float.NaN);
  }

   // Range is [0.,46.5]dB, with step 1.5dB
  synchronized public void setOutputVolume(float volume) {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.setOutputVolume(" + volume + "): called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.setOutputVolume(" + volume + "): called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) HWC_setOutputVolume(volume);
  }

  synchronized public float getSidetoneGain() {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.getSidetoneGain: called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.getSidetoneGain: called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(HWC_getSidetoneGain());
    else              return(Float.NaN);
  }

   // Range for now is [-94.5, 0.]dB, with step -1.5dB
  synchronized public void setSidetoneGain(float gain) {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwControls.setSidetoneGain(" + gain + "): called");
      // if (SysLog.printLevel > 3) new Throwable("HwControls.setSidetoneGain(" + gain + "): called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) HWC_setSidetoneGain(gain);
  }

   // Platform management
  private String  HWC_getOEMString(){
	  return "";
  };

   // Futz with the network
  private int     HWC_ping(String ipaddr) {
	  return 1;
  };
  private String  HWC_getMacAddr(String adapterHint) {
 	  return "00:00:00:00:00:01"; // ERK
  };

   // The get status routines
  private  int     HWC_getBatteryCharge() {
	  return 50;
  };
  
  private int     HWC_getRadioStatus() {
	  return RadioStatusHwEvent.RADIO_STATUS_STRONG;
  };

   // Power management
  private void    HWC_setPowerMgmt(boolean state) {
	 
  };
  private boolean HWC_getPowerMgmt() {
	  return false;
  };

   // Audio gain management
  private float   HWC_getMicGain() {
	  return 50.0f;
  };
  private void    HWC_setMicGain(float gain) {
	  
  };
  private float   HWC_getOutputVolume() {
	  return 1.0f;
  };
  
  private void    HWC_setOutputVolume(float volume) {
	  
  }; 
  private float   HWC_getSidetoneGain() {
	  return 1.0f;
  };
  
  private void    HWC_setSidetoneGain(float att) {
	  
  };

   // Flash management
  private int     InroadFlashUpdate(String path) {
	  return 1;
  };
  private int     InroadProgramFlash(String path, int offset) {
	  return 1;
  };
  private int     InroadProgramPMU(String path) {
	  return 1;
  };
}
