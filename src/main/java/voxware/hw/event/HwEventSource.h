#ifndef __voxware_hw_event_HwEventSource__
#define __voxware_hw_event_HwEventSource__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace hw
    {
      namespace event
      {
        class RawHwEvent;
        class StandbyHwEvent;
        class RadioStatusHwEvent;
        class BatteryLowHwEvent;
        class BatteryChargeHwEvent;
        class ButtonHwEvent;
        class HwEventListener;
        class HwEventSource;
      }
    }
  }
}

class voxware::hw::event::HwEventSource : public ::java::lang::Object
{
public:  // actually protected
  HwEventSource ();
public:
  static ::voxware::hw::event::HwEventSource *getInstance ();
  virtual void addHwBatteryChargeListener (::voxware::hw::event::HwEventListener *);
  virtual void removeHwBatteryChargeListener (::voxware::hw::event::HwEventListener *);
  virtual void addHwBatteryLowListener (::voxware::hw::event::HwEventListener *);
  virtual void removeHwBatteryLowListener (::voxware::hw::event::HwEventListener *);
  virtual void addHwRadioStatusListener (::voxware::hw::event::HwEventListener *);
  virtual void removeHwRadioStatusListener (::voxware::hw::event::HwEventListener *);
  virtual void addHwButtonListener (::voxware::hw::event::HwEventListener *, jint);
  virtual void removeHwButtonListener (::voxware::hw::event::HwEventListener *, jint);
  virtual void addHwMicStatusListener (::voxware::hw::event::HwEventListener *);
  virtual void removeHwMicStatusListener (::voxware::hw::event::HwEventListener *);
  virtual void addHwStandbyListener (::voxware::hw::event::HwEventListener *);
  virtual void removeHwStandbyListener (::voxware::hw::event::HwEventListener *);
  virtual void setPingIpAddr (::java::lang::String *);
  virtual ::java::lang::String *getPingIpAddr ();
  virtual void enableEvents ();
  virtual void disableEvents ();
  virtual void run ();
public:  // actually protected
  virtual void notifyButtonListeners (::voxware::hw::event::ButtonHwEvent *);
  virtual void notifyBatteryChargeListeners (::voxware::hw::event::BatteryChargeHwEvent *);
  virtual void notifyBatteryLowListeners (::voxware::hw::event::BatteryLowHwEvent *);
  virtual void notifyRadioStatusListeners (::voxware::hw::event::RadioStatusHwEvent *);
  virtual void notifyStandbyListeners (::voxware::hw::event::StandbyHwEvent *);
private:
  void HWE_getEvent (::voxware::hw::event::RawHwEvent *);
  void HWE_setPingIpAddr (::java::lang::String *);
  ::java::lang::String *HWE_getPingIpAddr ();
  void HWE_enable ();
  void HWE_disable ();
  static jboolean nativeLoaded;
  static ::voxware::hw::event::HwEventSource *source;
  ::java::util::Vector * __attribute__((aligned(__alignof__( ::java::lang::Object )))) batteryChargeListeners;
  ::java::util::Vector *batteryLowListeners;
  ::java::util::Vector *radioStatusListeners;
  JArray< ::java::util::Vector *> *buttonListeners;
  ::java::util::Vector *micStatusListeners;
  ::java::util::Vector *standbyListeners;
  jint batteryCharge;
  jint radioStatus;
  jintArray buttonState;
  jint micStatus;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_hw_event_HwEventSource__ */
