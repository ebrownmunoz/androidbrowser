package voxware.hw.event;

public class Initializer {

  /**
   * The static initialization method for library hw
   */
  public static String initialize(String logFileName) {
    return logFileName != null ? logFileName : "current library logfile";
  }

}
