/**
 * Adapter class for the HwEvent listener interface
 */

package voxware.hw.event;

public abstract class HwEventAdapter implements HwEventListener {
  public void eventArrived(ButtonHwEvent e) {};
  public void eventArrived(BatteryChargeHwEvent e) {};
  public void eventArrived(BatteryLowHwEvent e) {};
  public void eventArrived(RadioStatusHwEvent e) {};
  public void eventArrived(MicStatusHwEvent e) {};
  public void eventArrived(StandbyHwEvent e) {};
}
