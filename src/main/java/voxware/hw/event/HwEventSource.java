/**
 * Client hardware event source
 */

package voxware.hw.event;

import java.util.Vector;
import voxware.util.SysLog;

public class HwEventSource implements Runnable {

  private static boolean nativeLoaded = HwControls.loadLibrary();
  private static HwEventSource source = null;

  private Vector batteryChargeListeners = new Vector(4);
  private Vector batteryLowListeners = new Vector(4);
  private Vector radioStatusListeners = new Vector(4);
  private Vector buttonListeners[] = new Vector[] {  
    new Vector(4), new Vector(4),
    new Vector(4), new Vector(4), 
    new Vector(4), new Vector(4), 
    new Vector(4), new Vector(4)
  };
  private Vector micStatusListeners = new Vector(4);
  private Vector standbyListeners = new Vector(4);

  private int batteryCharge;
  private int radioStatus;
  private int buttonState[];
  private int micStatus;

  protected HwEventSource() {
    if (nativeLoaded) new Thread (this, "tJHwEvent").start();
     // Following dummy assignments make sure that referenced classes are initialized,
     // and their static fields/methods can be accessed safely from CNI code.
    int i = ButtonHwEvent.BUTTON_PRESSED;
    i = RadioStatusHwEvent.RADIO_STATUS_STRONG;
  }

 /**
  * Return the sole instance of the hardware event source,
  * constructing it if necessary
  * @return HwEventSource - source of client hardware events
  */
  public static HwEventSource getInstance() {
    if (source == null) source = new HwEventSource();
    return (source);
  }

 /**
  * Add a listener to the event source
  * @param HwEventListener  listener to add
  */
  public synchronized void addHwBatteryChargeListener(HwEventListener l) {
    if (!batteryChargeListeners.contains(l))
      batteryChargeListeners.addElement(l);
  }

  public synchronized void removeHwBatteryChargeListener(HwEventListener l) {
    batteryChargeListeners.removeElement(l);
  }

  public synchronized void addHwBatteryLowListener(HwEventListener l) {
    if (!batteryLowListeners.contains(l))
      batteryLowListeners.addElement(l);
  }

  public synchronized void removeHwBatteryLowListener(HwEventListener l) {
    batteryLowListeners.removeElement (l);
  }

  public synchronized void addHwRadioStatusListener(HwEventListener l) {
    if (!radioStatusListeners.contains(l))
      radioStatusListeners.addElement(l);
  }

  public synchronized void removeHwRadioStatusListener(HwEventListener l) {
    radioStatusListeners.removeElement(l);
  }

  public synchronized void addHwButtonListener(HwEventListener l, int i) {
    if (!buttonListeners[i].contains(l))
      buttonListeners[i].addElement(l);
  }

  public synchronized void removeHwButtonListener(HwEventListener l, int i) {
    buttonListeners[i].removeElement(l);
  }

  public synchronized void addHwMicStatusListener(HwEventListener l) {
    if (!micStatusListeners.contains(l))
      micStatusListeners.addElement(l);
  }

  public synchronized void removeHwMicStatusListener(HwEventListener l) {
    micStatusListeners.removeElement(l);
  }

  public synchronized void addHwStandbyListener(HwEventListener l) {
    if (!standbyListeners.contains(l))
      standbyListeners.addElement(l);
  }

  public synchronized void removeHwStandbyListener(HwEventListener l) {
    standbyListeners.removeElement(l);
  }

  public void setPingIpAddr(String ipAddr) {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwEventSource.setPingIpAddr(" + ipAddr + "): called");
      // if (SysLog.printLevel > 3) new Throwable("HwEventSource.setPingIpAddr(" + ipAddr + "): called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) HWE_setPingIpAddr(ipAddr);
  }

  public String getPingIpAddr() {
    if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
      SysLog.println("HwEventSource.getPingIpAddr: called");
      // if (SysLog.printLevel > 3) new Throwable("HwEventSource.getPingIpAddr: called").printStackTrace(SysLog.stream);
    }
    if (nativeLoaded) return(HWE_getPingIpAddr());
    else              return("");
  }

  public void enableEvents() {
    if (nativeLoaded) HWE_enable();
  }
    
  public void disableEvents() {
    if (nativeLoaded) HWE_disable();
  }

  public void run() {

     // This thread runs only if the raw hardware event source has been initialized

    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK))
      SysLog.println("HwEventSource.run: running");

    HWE_enable();

     // Construct a raw event to use to get events from the hardware
    RawHwEvent rawEvent = new RawHwEvent();
//    while (true) {
//
//      if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK))
//        SysLog.println("HwEventSource.run: waiting for an event");
//
//       // Wait for the hardware to generate a raw event
//      HWE_getEvent(rawEvent);
//
//       // Notify the appropriate listeners of the event
//      if (rawEvent.getType() == RawHwEvent.BUTTON) {
//        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK))
//          SysLog.println("HwEventSource.run: got a Button event [" + rawEvent.getButtonIndex() + ", " + rawEvent.getButtonState() + "]");
//        ButtonHwEvent event = new ButtonHwEvent(this, rawEvent);
//        notifyButtonListeners(event);
//      } else if (rawEvent.getType() == RawHwEvent.BATTERY_CHARGE) {
//        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK))
//          SysLog.println("HwEventSource.run: got a BatteryCharge event [" + rawEvent.getBatteryCharge() + "]");
//        BatteryChargeHwEvent event = new BatteryChargeHwEvent(this, rawEvent);
//        notifyBatteryChargeListeners(event);
//      } else if (rawEvent.getType() == RawHwEvent.BATTERY_LOW) {
//        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK))
//          SysLog.println("HwEventSource.run: got a BatteryLow event");
//        BatteryLowHwEvent event = new BatteryLowHwEvent(this, rawEvent);
//        notifyBatteryLowListeners(event);
//      } else if (rawEvent.getType() == RawHwEvent.RADIO_STATUS) {
//        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK))
//          SysLog.println("HwEventSource.run: got a RadioStatus event [" + rawEvent.getRadioStatus() + "]");
//        RadioStatusHwEvent event = new RadioStatusHwEvent(this, rawEvent);
//        notifyRadioStatusListeners(event);
//      } else if (rawEvent.getType() == RawHwEvent.MIC_STATUS) {
//        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK))
//          SysLog.println("HwEventSource.run: got a MicStatus event [" + rawEvent.getMicStatus() + "]");
//      } else if (rawEvent.getType() == RawHwEvent.STANDBY_DIR) {
//        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK))
//          SysLog.println("HwEventSource.run: got a Standby event [" + rawEvent.getStandbyDirection() + "]");
//        StandbyHwEvent event = new StandbyHwEvent(this, rawEvent);
//        notifyStandbyListeners(event);
//      } else if (rawEvent.getType() == RawHwEvent.NULL_EVENT) {
//        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK))
//          SysLog.println("HwEventSource.run: got a Null event");
//      } else {
//        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.HARDWARE_MASK)) {
//          SysLog.println("HwEventSource.run: got an unknown event");
//          // if (SysLog.printLevel > 3) new Throwable("HwEventSource.run: got an unknown event").printStackTrace(SysLog.stream);
//        }
//      }
//    }
  }

  protected void notifyButtonListeners(ButtonHwEvent e) {
    int id = e.getButtonID();
    Vector copy;

    synchronized(this) {
      copy = (Vector) buttonListeners[id].clone();
    }
	
    int count = copy.size();
    for (int i = 0; i < count; i++) {
      HwEventListener l = (HwEventListener) copy.elementAt(i);
      l.eventArrived (e);
    }
  }

  protected void notifyBatteryChargeListeners(BatteryChargeHwEvent e) {
    Vector copy;

    synchronized(this) {
      copy = (Vector) batteryChargeListeners.clone();
    }
	
    int count = copy.size();
    for (int i = 0; i < count; i++) {
      HwEventListener l = (HwEventListener) copy.elementAt (i);
      l.eventArrived (e);
    }
  }

  protected void notifyBatteryLowListeners(BatteryLowHwEvent e) {
    Vector copy;

    synchronized(this) {
      copy = (Vector) batteryLowListeners.clone();
    }
	
    int count = copy.size();
    for (int i = 0; i < count; i++) {
      HwEventListener l = (HwEventListener) copy.elementAt(i);
      l.eventArrived(e);
    }
  }

  protected void notifyRadioStatusListeners(RadioStatusHwEvent e) {
    Vector copy;

    synchronized (this) {
      copy = (Vector) radioStatusListeners.clone();
    }
	
    int count = copy.size();
    for (int i = 0; i < count; i++) {
      HwEventListener l = (HwEventListener) copy.elementAt(i);
      l.eventArrived (e);
    }
  }

  protected void notifyStandbyListeners(StandbyHwEvent e) {
    Vector copy;

    synchronized(this) {
      copy = (Vector) standbyListeners.clone();
    }
	
    int count = copy.size();
    for (int i = 0; i < count; i++) {
      HwEventListener l = (HwEventListener) copy.elementAt(i);
      l.eventArrived(e);
    }
  }

 /**
  * Retrieve a raw hardware event. This method will block until an event
  * occurs. A "blank" event is passed in to be modified. This approach avoids
  * complications of instantiating objects on the native side and dealing
  * with the reference on the Java side.
  * @param	RawHwEvent	event occurred
  */
  private  void   HWE_getEvent(RawHwEvent rawEvent) {
	  System.err.println("native HWE_getEvent called (and ignored");
  };
    
  /** Set the ping address (radio status monitoring) */
  private  void   HWE_setPingIpAddr(String ipaddr){
	  System.err.println("native HWE_drnyPingIpAddr called (and ignored");
  };

  /** Get the ping address (radio status monitoring) */
  private  String HWE_getPingIpAddr(){
	  System.err.println("native HWE_getPingIpAddr called (and ignored");
	  return "";
  };

  /** Enable the occurence of hardware events */
  private  void   HWE_enable(){
	  System.err.println("native HWE_getEnable called (and ignored");
  };

  /** Disable the occurence of hardware events */
  private  void   HWE_disable(){
	  System.err.println("native HWE_getDisable called (and ignored");
  };

  /* Test Code, for non-native environment */
  /*
  private static Random rand = new Random();

  private HwEvent HWE_getEvent() {
    HwEvent e;
    switch (Math.abs(rand.nextInt()) % 4) {
      case 0: {
        e = new BatteryChargeHwEvent(this, (Math.abs(rand.nextInt()) % 100));
        break;
      }
      case 1: {
        e = new BatteryStatusHwEvent(this, (Math.abs(rand.nextInt()) % 4));
        break;
      }
      case 2: {
        e = new RadioStatusHwEvent(this, (Math.abs(rand.nextInt()) % 4));
        break;
      }
      case 3: {
        e = new MicStatusHwEvent(this, (Math.abs(rand.nextInt()) % 2));
        break;
      }
      default: {
        e = new MicStatusHwEvent(this, 0);
        break;
      }
    }
    return (e);
  }

  private void HWE_enable() {};
  private void HWE_disable() {};
  */
}
