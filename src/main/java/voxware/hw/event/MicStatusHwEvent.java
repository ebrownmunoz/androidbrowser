/**
 * A Pacer hardware microphone status event. Represents a notice from the
 * Pacer hardware of a new microphone status
 */

package voxware.hw.event;

import java.util.EventObject;

public class MicStatusHwEvent extends EventObject {
  public static final int MIC_STATUS_ON = 1;
  public static final int MIC_STATUS_OFF = 2;

  private int status;

  public MicStatusHwEvent(Object source, int s) {
    super(source);
    status = s;
  }

  MicStatusHwEvent(Object source, RawHwEvent rawEvent) {
    super(source);
    status = rawEvent.getMicStatus();
  }

 /**
  * Return this event's status member
  * @return  int     MIC_STATUS_ON or MIC_STATUS_OFF
  */
  public int getStatus() {
    return(status);
  }
}
