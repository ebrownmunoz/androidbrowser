#ifndef __voxware_hw_event_RawHwEvent__
#define __voxware_hw_event_RawHwEvent__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace hw
    {
      namespace event
      {
        class RawHwEvent;
      }
    }
  }
}

class voxware::hw::event::RawHwEvent : public ::java::lang::Object
{
public: // actually package-private
  RawHwEvent ();
public:
  virtual jint getType () { return type; }
  virtual jint getBatteryCharge () { return charge; }
  virtual jint getRadioStatus () { return status; }
  virtual jint getMicStatus () { return status; }
  virtual jint getButtonIndex () { return index; }
  virtual jint getButtonState () { return state; }
  virtual jint getStandbyDirection () { return direction; }
  static const jint NULL_EVENT = 0L;
  static const jint BUTTON = 1L;
  static const jint BATTERY_CHARGE = 2L;
  static const jint RADIO_STATUS = 3L;
  static const jint MIC_STATUS = 4L;
  static const jint STANDBY_DIR = 5L;
  static const jint BATTERY_LOW = 6L;
public:  // actually protected
  jint __attribute__((aligned(__alignof__( ::java::lang::Object ))))  type;
  jint status;
  jint charge;
  jint index;
  jint state;
  jint direction;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_hw_event_RawHwEvent__ */
