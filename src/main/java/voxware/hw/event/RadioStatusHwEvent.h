#ifndef __voxware_hw_event_RadioStatusHwEvent__
#define __voxware_hw_event_RadioStatusHwEvent__

#pragma interface

#include <java/util/EventObject.h>

extern "Java"
{
  namespace voxware
  {
    namespace hw
    {
      namespace event
      {
        class RadioStatusHwEvent;
        class RawHwEvent;
      }
    }
  }
}

class voxware::hw::event::RadioStatusHwEvent : public ::java::util::EventObject
{
public:
  RadioStatusHwEvent (::java::lang::Object *, jint);
public: // actually package-private
  RadioStatusHwEvent (::java::lang::Object *, ::voxware::hw::event::RawHwEvent *);
public:
  virtual jint getStatus () { return status; }
  static const jint RADIO_STATUS_OFFLINE = 0L;
  static const jint RADIO_STATUS_SEARCHING = 1L;
  static const jint RADIO_STATUS_WEAK = 2L;
  static const jint RADIO_STATUS_MEDIUM = 3L;
  static const jint RADIO_STATUS_STRONG = 4L;
private:
  jint __attribute__((aligned(__alignof__( ::java::util::EventObject ))))  status;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_hw_event_RadioStatusHwEvent__ */
