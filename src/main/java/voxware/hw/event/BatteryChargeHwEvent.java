/**
 *
 * A Pacer hardware battery charge event. Represents a notice from the
 * Pacer battery of a new charge level.
 */

package voxware.hw.event;

import java.util.EventObject;

public class BatteryChargeHwEvent extends EventObject {
  public static final int BATTERY_CHARGE_UNKNOWN = -1;
  private int charge;

  public BatteryChargeHwEvent(Object source, int charge) {
    super(source);
    this.charge = charge;
  }

  BatteryChargeHwEvent(Object source, RawHwEvent rawEvent) {
    super(source);
    charge = rawEvent.getBatteryCharge();
  }

 /**
  * Return the charge associated with this event as an integer from 0 to 100
  *
  * @return  int     Current battery charge (0-100, -1 => unknown/constant)
  */
  public int getCharge() {
    return (charge);
  }
}
