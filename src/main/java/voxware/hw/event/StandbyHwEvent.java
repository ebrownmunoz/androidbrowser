/**
 * A Pacer hardware standby event.  Represents a notice from the
 * Pacer hardware that the standby state is changing. 
 */

package voxware.hw.event;

import java.util.EventObject;

public class StandbyHwEvent extends EventObject {
  public static final int GO_TO_STANDBY = 1;
  public static final int OUT_OF_STANDBY = 2;

  private int direction;

  public StandbyHwEvent(Object source, int d) {
    super(source);
    direction = d;
  }

  StandbyHwEvent(Object source, RawHwEvent rawEvent) {
    super (source);
    direction = rawEvent.getStandbyDirection();
  }

 /**
  * Return the direction associated with this event
  * @return  int     GO_TO_STANDBY or OUT_OF_STANDBY
  */
  public int getDirection() {
    return (direction);
  }
}
