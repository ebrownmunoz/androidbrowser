#ifndef __voxware_hw_event_HwControls__
#define __voxware_hw_event_HwControls__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace hw
    {
      namespace event
      {
        class HwControls;
      }
    }
  }
}

class voxware::hw::event::HwControls : public ::java::lang::Object
{
public: // actually package-private
  static jboolean loadLibrary ();
public:  // actually protected
  HwControls ();
public:
  static ::voxware::hw::event::HwControls *getInstance ();
  virtual ::java::lang::String *getMacAddr (::java::lang::String *);
  virtual ::java::lang::String *getOEMString ();
  virtual jint getBatteryCharge ();
  virtual jint getRadioStatus ();
  virtual void setPowerMgmt (jboolean);
  virtual jboolean getPowerMgmt ();
  virtual jint flashUpdate (::java::lang::String *);
  virtual jint ping (::java::lang::String *);
  virtual jint programFlash (::java::lang::String *, jint);
  virtual jint programPMU (::java::lang::String *);
  virtual jfloat getMicGain ();
  virtual void setMicGain (jfloat);
  virtual jfloat getOutputVolume ();
  virtual void setOutputVolume (jfloat);
  virtual jfloat getSidetoneGain ();
  virtual void setSidetoneGain (jfloat);
private:
  ::java::lang::String *HWC_getOEMString ();
  jint HWC_ping (::java::lang::String *);
  ::java::lang::String *HWC_getMacAddr (::java::lang::String *);
  jint HWC_getBatteryCharge ();
  jint HWC_getRadioStatus ();
  void HWC_setPowerMgmt (jboolean);
  jboolean HWC_getPowerMgmt ();
  jfloat HWC_getMicGain ();
  void HWC_setMicGain (jfloat);
  jfloat HWC_getOutputVolume ();
  void HWC_setOutputVolume (jfloat);
  jfloat HWC_getSidetoneGain ();
  void HWC_setSidetoneGain (jfloat);
  jint InroadFlashUpdate (::java::lang::String *);
  jint InroadProgramFlash (::java::lang::String *, jint);
  jint InroadProgramPMU (::java::lang::String *);
public:
  static ::java::lang::String *LibraryName;
  static ::java::lang::String *LibraryInitializer;
  static ::java::lang::String *XVoxOEMString;
private:
  static jboolean nativeLoaded;
  static ::voxware::hw::event::HwControls *controls;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_hw_event_HwControls__ */
