/**
 * Listener interface for Pacer hardware events
 */

package voxware.hw.event;

public interface HwEventListener {
  public void eventArrived(ButtonHwEvent e);
  public void eventArrived(BatteryChargeHwEvent e);
  public void eventArrived(BatteryLowHwEvent e);
  public void eventArrived(RadioStatusHwEvent e);
  public void eventArrived(StandbyHwEvent e);
}
