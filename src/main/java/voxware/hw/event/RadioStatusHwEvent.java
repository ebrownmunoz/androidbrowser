/**
 * A Pacer radio status event. Represents a notice from the Pacer
 * unit of a new radio status
 */

package voxware.hw.event;

import java.util.EventObject;

public class RadioStatusHwEvent extends EventObject {

  public static final int RADIO_STATUS_OFFLINE = 0;
  public static final int RADIO_STATUS_SEARCHING = 1;
  public static final int RADIO_STATUS_WEAK = 2;
  public static final int RADIO_STATUS_MEDIUM = 3;
  public static final int RADIO_STATUS_STRONG = 4;

  private int status;

  public RadioStatusHwEvent(Object source, int status) {
    super(source);
    this.status = status;
  }

  RadioStatusHwEvent(Object source, RawHwEvent rawEvent) {
    super(source);
    status = rawEvent.getRadioStatus();
  }

 /**
  * Return the status associated with this event
  *
  * @return  int     RADIO_STATUS_[OFFLINE,SEARCHING,WEAK,MEDIUM,STRING]
  */
  public int getStatus() {
    return(status);
  }
}
