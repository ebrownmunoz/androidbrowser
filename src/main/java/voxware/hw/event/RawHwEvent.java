/**
 * A single class that can contain all information needed to describe any type
 * of Pacer hardware event
 */

package voxware.hw.event;

public class RawHwEvent {
    
  public static final int NULL_EVENT = 0;
  public static final int BUTTON = 1;
  public static final int BATTERY_CHARGE = 2;
  public static final int RADIO_STATUS = 3;
  public static final int MIC_STATUS = 4;
  public static final int STANDBY_DIR = 5;
  public static final int BATTERY_LOW = 6;

  protected int type;
  protected int status;
  protected int charge;
  protected int index;
  protected int state;
  protected int direction;

  RawHwEvent() {
    type = NULL_EVENT;
  }
    
  public int getType() {
    return(type);
  }
    
  // @@ perhaps all of these should check to make sure that "this" is
  // not a NULL_EVENT event or of the wrong type.
  public int getBatteryCharge() {
    return(charge);
  }
    
  public int getRadioStatus() {
    return(status);
  }
    
  public int getMicStatus() {
    return(status);
  }
    
  public int getButtonIndex() {
    return(index);
  }
    
  public int getButtonState() {
    return(state);
  }
    
  public int getStandbyDirection() {
    return(direction);
  }
}
