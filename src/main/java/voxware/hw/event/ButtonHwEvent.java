/**
 * A Pacer hardware button event. Represents a notice from the Pacer
 * hardware that a particular button was either pressed or released.
 */

package voxware.hw.event;

import java.util.EventObject;

public class ButtonHwEvent extends EventObject {
  public static final int BUTTON_PRESSED = 0;
  public static final int BUTTON_RELEASED = 1;

  private int buttonID;
  private int state;

  public ButtonHwEvent(Object source, int i, int s) {
    super(source);
    buttonID = i;
    state = s;
  }

  ButtonHwEvent(Object source, RawHwEvent rawEvent) {
    super(source);
    buttonID = rawEvent.getButtonIndex();
    state = rawEvent.getButtonState();
  }

 /**
  * Return the button ID of the event
  * @return  int     Button ID, 0-7
  */
  public int getButtonID() {
    return (buttonID);
  }

 /**
  * Return the state of the button
  * @return  int     BUTTON_PRESSED or BUTTON_RELEASED
  */
  public int getState() {
    return(state);
  }
}
