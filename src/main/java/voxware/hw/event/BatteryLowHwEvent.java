/**
 * A Pacer hardware low charge event. Represents a notice from the Pacer
 * battery that a battery low warning has occurred.
 */

package voxware.hw.event;

import java.util.EventObject;

public class BatteryLowHwEvent extends EventObject {
  public BatteryLowHwEvent(Object source) {
    super(source);
  }

  BatteryLowHwEvent(Object source, RawHwEvent rawEvent) {
    super(source);
  }
}
