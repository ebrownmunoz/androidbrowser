#ifndef __voxware_hw_event_ButtonHwEvent__
#define __voxware_hw_event_ButtonHwEvent__

#pragma interface


extern "Java"
{
  namespace voxware
  {
    namespace hw
    {
      namespace event
      {
        class ButtonHwEvent;
        class RawHwEvent;
      }
    }
  }
}

class voxware::hw::event::ButtonHwEvent : public ::java::util::EventObject
{
public:
  ButtonHwEvent (::java::lang::Object *, jint, jint);
public: // actually package-private
  ButtonHwEvent (::java::lang::Object *, ::voxware::hw::event::RawHwEvent *);
public:
  virtual jint getButtonID () { return buttonID; }
  virtual jint getState () { return state; }
  static const jint BUTTON_PRESSED = 0L;
  static const jint BUTTON_RELEASED = 1L;
private:
  jint __attribute__((aligned(__alignof__( ::java::util::EventObject ))))  buttonID;
  jint state;
public:

  static ::java::lang::Class class$;
};

#endif /* __voxware_hw_event_ButtonHwEvent__ */
