package voxware.hw;

import java.io.PrintStream;

public class Log {
  public static final int ERROR = 1;
  public static final int WARN = 2;
  public static final int INFO = 3;
  public static final int DEBUG = 4;

  private String className;
  private int level;
  private PrintStream printStream;

  /* Default constructor has some.. defaults */
  public Log() {
    this.className = new String("default");
    this.level = INFO;
    this.printStream = System.out;
  }

  public Log(String className) {
    this.className = className;
  }

  public Log(String className, int level, PrintStream printStream) {
    this.className = className;
    this.level = level;
    this.printStream = printStream;
  }

  public Log(Log that) {
    this.className = that.className;
    this.level = that.level;
    this.printStream = that.printStream;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }
  
  public boolean isLoggable(int level) {
    return level <= this.level;
  }

  public PrintStream getPrintStream() {
    return printStream;
  }

  public void setPrintStream(PrintStream printStream) {
    this.printStream = printStream;
  }

  /* log output methods */
  public void error(String msg) {
    if (level >= ERROR)
      printStream.println(msg);
  }

  public void warn(String msg) {
    if (level >= WARN)
      printStream.println(msg);
  }

  public void info(String msg) {
    if (level >= INFO)
      printStream.println(msg);
  }

  public void debug(String msg) {
    if (level >= DEBUG)
      printStream.println(msg);
  }
}