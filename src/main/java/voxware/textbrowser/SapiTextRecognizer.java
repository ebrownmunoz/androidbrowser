package voxware.textbrowser;

import java.util.List;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.speech.EngineException;
import javax.speech.EngineStateError;
import javax.speech.VendorDataException;
import javax.speech.recognition.Grammar;
import javax.speech.recognition.RecognizerModeDesc;
import javax.speech.recognition.RecognizerProperties;
import javax.speech.recognition.Result;
import javax.speech.recognition.RuleGrammar;
import javax.speech.recognition.SpeakerManager;
import javax.speech.recognition.SpeakerProfile;

import voxware.engine.recognition.BaseGrammar;
import voxware.engine.recognition.BaseRecognizer;
import voxware.engine.recognition.BaseResult;
import voxware.engine.recognition.BaseRuleGrammar;
import voxware.engine.recognition.mock.FileMockSpeaker;
import voxware.engine.recognition.mock.MockSpeaker;
import voxware.engine.recognition.mock.NotifiableByMocks;
import voxware.engine.recognition.sapivise.RawResult;
import voxware.engine.recognition.sapivise.SapiTextGrammar;
import voxware.engine.recognition.sapivise.SapiViseGrammar;
import voxware.engine.recognition.sapivise.TextRecognizerProperties;
import voxware.engine.recognition.sapivise.TextSpeakerManager;
import voxware.engine.recognition.sapivise.ViseResult;
import voxware.util.SysLog;

public class SapiTextRecognizer extends BaseRecognizer implements NotifiableByMocks {

	private BaseRuleGrammar currentGrammar;
	List<String> speakers = new ArrayList<>();
	SpeakerProfile currentSpeaker;
	private MockSpeaker mockSpeaker;
	private final RecognizerProperties recognizerProperties = new TextRecognizerProperties();
	
	
	public SapiTextRecognizer() {
		super();
	}

	public SapiTextRecognizer(MockSpeaker mockSpeaker, RecognizerModeDesc mode, boolean reloadAll) {
		super(mode, reloadAll);
	}
	
	  /** 
	   * Override getSpeakerManager() in BaseRecognizer to get a ViseSpeakerManager
	   * FROM javax.speech.recognition.Recognizer
	   */
	  public SpeakerManager getSpeakerManager() {
	    SpeakerManager manager = new TextSpeakerManager(this);
	    return manager;
	  }
	  
	  public void allocateEngine() throws EngineException {
		  super.allocateEngine();
	  }
	  
	  /** 
	   * Override readVendorGrammar() in BaseRecognizer to read a SapiViseGrammar
	   * FROM javax.speech.recognition.Recognizer
	   */
	  public Grammar readVendorGrammar(InputStream input) throws VendorDataException, IOException, EngineStateError {
	    checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);           // Throws EngineStateError

	    synchronized (this) { // Guard mockSpeaker
	    	SapiTextGrammar stgrammar = new SapiTextGrammar(this, input);                // Throws IOException
	    	stgrammar.setMockSpeaker(mockSpeaker);
	    	currentGrammar = stgrammar;
	    }
	     // Remove and delete any older grammar already on the list under the same name
	    RuleGrammar oldG = null;
	    if ((oldG = retrieveGrammar(currentGrammar.getName())) != null) {
	      if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.SAPIVISE_MASK))
	        SysLog.println("SapiVise.readVendorGrammar: deleting old version of grammar \"" + currentGrammar.getName() + "\"");
	      deleteRuleGrammar(oldG);
	      ((SapiTextGrammar)oldG).deallocate();
	    }
	    oldG = null;

	     // Store the new grammar in this Recognizer
	    storeGrammar((RuleGrammar)currentGrammar);

	    return currentGrammar;
	  }
	  
	  public boolean readVendorSpeakerProfile(String spkrName, byte[] voiData) {
		    speakers.add(spkrName);
		    if (currentSpeaker == null) {
		    	currentSpeaker = new SpeakerProfile();
		        currentSpeaker.setName(spkrName);
		    }
		    return true;
	  }
	  
	  public void setCurrentSpeaker(SpeakerProfile profile) {
		  this.currentSpeaker = profile;
	  }
	  
	  public SpeakerProfile getCurrentSpeaker() {
		  return this.currentSpeaker;
	  }
	  
	   // Utility function to forward a speech result string to the appropriate grammar
	  public void notifyResult(RawResult rawresult) {
	    BaseGrammar G = (BaseGrammar)getRuleGrammar(rawresult.grammarName);

	    if (G == null) {
	      System.err.println("SapiVise.notifyResult: ERROR -- UNKNOWN GRAMMAR FOR RESULT " + rawresult.grammarName);
	      return;
	    }
	    
	    BaseResult result = new BaseResult(G, rawresult.nbestArray, false);
	    
	    this.resultCreated(result);
	    result.grammarFinalized(this, G);
	    
	    if (rawresult.accepted) {
	        result.setResultState(Result.ACCEPTED);
	        result.resultAccepted(this, G);
	      } else {
	        result.setResultState(Result.REJECTED);
	        result.resultRejected(this, G);
	      }	    
	  }

	// syncrhonized to guard mock Speaker
	public  synchronized void setMockSpeakerInGrammar(MockSpeaker mockSpeaker) {
		this.mockSpeaker = mockSpeaker;
		
	}
	
	@Override
	public RecognizerProperties getRecognizerProperties() {
		return this.recognizerProperties;
	}
}
