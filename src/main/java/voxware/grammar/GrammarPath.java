package voxware.grammar;

import java.io.Serializable;

 // A GrammarPath is a reference to the tail GrammarPath leading to or from it via a path element
public class GrammarPath implements Cloneable, Serializable {

  public GrammarPathElement  element;  // This path element (GrammarToken, GrammarTag or GrammarRuleResult)
  public GrammarPath         tail;     // The GrammarPath leading to/from here

  public GrammarPath() {
  }

  public GrammarPath(GrammarPath tail) {
    this.tail = tail;
  }

  public GrammarPath(GrammarPath tail, GrammarPathElement element) {
    this.tail = tail;
    this.element = element;
  }

   // Return the tail GrammarPath
  public GrammarPath tail() {
    return tail;
  }

   // Set the tail GrammarPath
  public void tail(GrammarPath tail) {
    this.tail = tail;
  }

   // Return the path element
  public GrammarPathElement element() {
    return element;
  }

   // Set the path element
  public void element(GrammarPathElement element) {
    this.element = element;
  }

   // Append the argument GrammarPath to the tail of this one
  public GrammarPath append(GrammarPath path) {
    end().tail(path);
    return this;
  }

   // Append this GrammarPath to the tail of the argument
  public GrammarPath prepend(GrammarPath path) {
    path.end().tail(this);
    return path;
  }

   // Return the end of the tail of this GrammarPath
  public GrammarPath end() {
    if (tail != null) return tail.end();
    else              return this;
  }

   // Return the reverse of this GrammarPath
  public GrammarPath reverse() {
    return reverse(null);
  }

   // Return the reverse of this GrammarPath with the given tail appended
  protected GrammarPath reverse(GrammarPath tail) {
    GrammarPath toFrom = this.tail;
    this.tail = tail;
    if (element != null) element.reverse();
    if (toFrom != null) return toFrom.reverse(this);
    else                return this;
  }

   // Clone this GrammarPath
  public Object clone() {
    try {
      GrammarPath clone = (GrammarPath)super.clone();
      if (tail != null) clone.tail((GrammarPath)tail.clone());
      return clone;
    } catch (CloneNotSupportedException e) {
      throw new RuntimeException("GrammarPath.clone: UNEXPECTED " + e.toString());
    }
  }

  public String toString() {
    String elementString = element != null ? element.toString() : "";
    if (tail != null) {
      String tailString = tail.toString();
      StringBuffer stringBuffer = new StringBuffer(elementString);
      if (stringBuffer.length() > 0 && tailString.length() > 0) stringBuffer.append(' ');
      return stringBuffer.append(tailString).toString();
    } else {
      return elementString;
    }
  }

  public String toReverseString() {
    String elementString = element != null ? element.toReverseString() : "";
    if (tail != null) {
      String tailString = tail.toReverseString();
      StringBuffer stringBuffer = new StringBuffer(tailString);
      if (stringBuffer.length() > 0 && elementString.length() > 0) stringBuffer.append(' ');
      return stringBuffer.append(elementString).toString();
    } else {
      return elementString;
    }
  }
}
