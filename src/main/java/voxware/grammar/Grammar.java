package voxware.grammar;

import java.io.Serializable;

import java.util.Enumeration;
import java.util.Hashtable;

import voxware.util.HashVector;

public class Grammar implements Serializable {

  public static GrammarToken NULL_TOKEN = new GrammarToken("NULL_TOKEN");
  public static GrammarToken GARBAGE_TOKEN = new GrammarToken("GARBAGE_TOKEN");

  private static int tokenCount = 0;

  protected Hashtable  rules;
  protected HashVector tokens;
  protected Hashtable  aliases;
  protected String     root;                        // The name (id) of the root rule

  public Grammar() {
    rules = new Hashtable();
    tokens = new HashVector();
    token(NULL_TOKEN);                              // Always token 0
    token(GARBAGE_TOKEN);                           // Always token 1
  }

   // Add a GrammarRule, returning any existing rule having the same name, or null if none
  public GrammarRule rule(GrammarRule rule) {
    rule.grammar(this);
    return (GrammarRule)rules.put(rule.name(), rule);
  }

  // Get a GrammarRule by name, constructing it if necessary
  public GrammarRule rule(String name) {
    GrammarRule rule = (GrammarRule)rules.get(name);
    if (rule == null) {
      rule = new GrammarRule(name, this);
      rule(rule);
    }
    return rule;
  }

   // Add a GrammarToken, returning any existing token having the same name, or null if none
  public GrammarToken token(GrammarToken token) {
    token.id = tokenCount;
    GrammarToken oldToken = (GrammarToken)tokens.put(token.name, token);
    if (oldToken == null) tokenCount++;
    return oldToken;
  }

   // Get a GrammarToken by id
  public GrammarToken token(int id) {
    return (GrammarToken)tokens.elementAt(id);
  }

   // Get a GrammarToken by name, constructing it if necessary
  public GrammarToken token(String name) {
    GrammarToken token = (GrammarToken)tokens.get(name);
    if (token == null) {
      token = new GrammarToken(name);
      token(token);
    }
    return token;
  }

   // Get a GrammarToken's id by name
  public int tokenId(String name) throws XMLGrammarException {
    GrammarToken token = (GrammarToken)tokens.get(name);
    if (token == null) throw new XMLGrammarException("\"" + name + "\" is not a valid token");
    return token.id;
  }

   // Return the root GrammarRule
  public GrammarRule root() {
    return (GrammarRule)rules.get(root);
  }

   // Return the Hashtable of GrammarRules
  public Hashtable rules() {
    return rules;
  }

   // Return the HashVector of GrammarTokens
  public HashVector tokens() {
    return tokens;
  }

   // Construct the closures of all the GrammarNodes in all the GrammarRules
  protected void close() {
    Enumeration ruleEnum = rules.elements();
    while (ruleEnum.hasMoreElements()) ((GrammarRule)ruleEnum.nextElement()).close();
  }

   // Return a String representation of the Grammar's tokens
  public String tokensToString(String delimiter, String indentation) {
    StringBuffer tokenString = new StringBuffer(Integer.toString(tokens.size()));
    tokenString.append(" Tokens:");
    Enumeration tokenEnum = tokens.elements();
    while (tokenEnum.hasMoreElements())
      tokenString.append(((GrammarToken)tokenEnum.nextElement()).toString(delimiter, indentation));
    return tokenString.toString();
  }

   // Return a String representation of the Grammar's rules
  public String rulesToString(String delimiter, String indentation) {
    StringBuffer ruleString = new StringBuffer(Integer.toString(rules.size()));
    ruleString.append(" Rules:");
    delimiter += indentation;
    Enumeration ruleEnum = rules.elements();
    while (ruleEnum.hasMoreElements())
      ruleString.append(((GrammarRule)ruleEnum.nextElement()).toString(delimiter, indentation));
    return ruleString.toString();
  }

   // Return a String representation of the entire Grammar
  public String toString() {
    StringBuffer grammarString = new StringBuffer();
    grammarString.append(tokensToString("\n", "  ")).append("\n");
    grammarString.append(rulesToString("\n", "  ")).append("\n");
    return grammarString.toString();
  }
}
