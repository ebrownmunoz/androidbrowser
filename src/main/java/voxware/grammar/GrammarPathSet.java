package voxware.grammar;

import java.io.Serializable;

import java.util.Enumeration;
import java.util.Hashtable;

import voxware.util.ObjectSet;

 /** A GrammarPathSet
   * This is a Java 1.1 implementation of the Java 1.2 Set interface, with extensions
   * specific to GrammarPathToNodes. A Set is an unordered collection of distinct objects.
   * Two GrammarPathToNodes are considered equivalent if their terminal GrammarNodes are 
   * identical, so the set will never contain two paths ending in the same GrammarNode. */

public class GrammarPathSet extends Hashtable implements Serializable {

  public GrammarPathSet() {
    super();
  }

  protected GrammarPathSet(int capacity) {
    super(capacity);
  }

  public GrammarPathSet(GrammarPathSet set) {
    this(set.size());
    this.addAll(set);
  }

   // Set Implementation

   // Add the argument GrammarPathToNode to the set and return true iff the set does not contain a GrammarPathToNode with the same terminal GrammarNode
  public boolean add(GrammarPathToNode path) {
    GrammarPathToNode old = (GrammarPathToNode)this.put(path.terminus(), path);
    if (old != null) {
      this.put(old.terminus(), old);
      return false;
    } else {
      return true;
    }
  }

   // Add to the set all of the paths ending in nodes not already there, returning true iff this changes the set
  public boolean addAll(GrammarPathSet set) {
    boolean changed = false;
    Enumeration paths = set.iterator();
    while (paths.hasMoreElements())
      changed = this.add((GrammarPathToNode)paths.nextElement()) || changed;
    return changed;
  }

   // Return true iff the set contains a GrammarPathToNode with the same terminal GrammarNode as the argument GrammarPathToNode
  public boolean contains(GrammarPathToNode path) {
    return this.containsKey(path.terminus());
  }

   // Return true iff the argument set is a subset of the set
  public boolean containsAll(GrammarPathSet set) {
    boolean containsAll = true;
    Enumeration paths = set.iterator();
    while (containsAll && paths.hasMoreElements())
      containsAll = this.containsKey(((GrammarPathToNode)paths.nextElement()).terminus());
    return containsAll;
  }

   // Enumerate the paths in the set
  public Enumeration iterator() {
    return this.elements();
  }

   // Remove the argument path, returning true iff this changes the set
  public boolean remove(GrammarPathToNode path) {
    return (this.remove(path.terminus()) != null);
  }

   // Remove all the paths shared with the argument, returning true iff this changes the set
  public boolean removeAll(GrammarPathSet set) {
    boolean changed = false;
    Enumeration termini = set.keys();
    while (termini.hasMoreElements())
      changed = this.remove((GrammarNode)termini.nextElement()) != null || changed;
    return changed;
  }

   // Retain only the paths shared with the argument, returning true iff this changes the set
  public boolean retainAll(GrammarPathSet set) {
    GrammarPathSet unshared = new GrammarPathSet();
    Enumeration paths = this.elements();
    while (paths.hasMoreElements()) {
      GrammarPathToNode path = (GrammarPathToNode)paths.nextElement();
      if (!set.contains(path)) unshared.add(path);
    }
    return removeAll(unshared);
  }

   // Create an array of the GrammarPathToNodes in the set
  public GrammarPathToNode[] toArray() {
    int size = this.size();
    GrammarPathToNode[] array = new GrammarPathToNode[size];
    Enumeration paths = this.elements();
    for (int i = 0; i < size; i++)
      array[i] = (GrammarPathToNode)paths.nextElement();
    return array;
  }

   // Fill in an array with the elements in the set, creating a larger array if necessary and null-terminating it if possible
  public GrammarPathToNode[] toArray(GrammarPathToNode[] array) {
    int size = this.size();
    if (array.length < size) array = new GrammarPathToNode[size];
    Enumeration paths = this.elements();
    for (int i = 0; i < size; i++)
      array[i] = (GrammarPathToNode)paths.nextElement();
    if (array.length > size) array[size] = null;
    return array;
  }

   // Extensions

   // Return true iff the set contains a GrammarPathToNode with the given terminal GrammarNode
  public boolean contains(GrammarNode terminus) {
    return this.containsKey(terminus);
  }

   // Add a null path to the given GrammarNode to the set
  public boolean startPath(GrammarNode terminus) {
    GrammarPathToNode path = new GrammarPathToNode(terminus);
    return add(path);
  }

   // Append a GrammarPath to every GrammarPathToNode in the set
  public void append(GrammarPath path) {
    Enumeration paths = this.iterator();
    while (paths.hasMoreElements())
      ((GrammarPathToNode)paths.nextElement()).append(path);
  }

   // Add extensions of path via element to all GrammarNodes not already in this set
  public void addAll(GrammarPath path, GrammarPathElement element, ObjectSet terminalNodes) {
    Enumeration termini = terminalNodes.iterator();
    while (termini.hasMoreElements()) {
      GrammarNode terminus = (GrammarNode)termini.nextElement();
      if (!containsKey(terminus))
        put(terminus, new GrammarPathToNode(new GrammarPath(path, element), terminus));
    }
  }

   // Add extensions of path via element to all GrammarPathToNodes not already in this set
  public void addAll(GrammarPath path, GrammarPathElement element, GrammarPathSet pathsToNodes) {
    Enumeration paths = pathsToNodes.iterator();
    while (paths.hasMoreElements()) {
      GrammarPathToNode pathToNode = (GrammarPathToNode)paths.nextElement();
      if (!containsKey(pathToNode.terminus()))
        put(pathToNode.terminus(), ((GrammarPathToNode)pathToNode.clone()).append(new GrammarPath(path, element)));
    }
  }
}
