package voxware.grammar;

import java.io.Serializable;

public class GrammarTag implements GrammarPathElement, Serializable {

  public String text;

  public GrammarTag(String text) {
    this.text = text;
  }

  public String text() {
    return text;
  }

  public String toString() {
    return "{" + text + "}";
  }

  public String toReverseString() {
    return toString();
  }

  public void reverse() {
  }
}
