package voxware.grammar;

public interface GrammarPathElement {
  public String  toReverseString();
  public void    reverse();
}
