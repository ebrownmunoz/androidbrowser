package voxware.grammar;

public class XMLGrammarException extends Exception {

  private Throwable exception;

   // Constructors

  public XMLGrammarException(Throwable exception, String message) {
    super(message);
    this.exception = exception;
  }

  public XMLGrammarException(Throwable exception) {
    super();
    this.exception = exception;
  }

  public XMLGrammarException(String message) {
    super(message);
  }

  public XMLGrammarException() {
    super();
  }

   // Access Methods

  public Throwable exception() {
    return this.exception;
  }
}
