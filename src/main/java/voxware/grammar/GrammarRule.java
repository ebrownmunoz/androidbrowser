package voxware.grammar;

import java.io.Serializable;

import java.util.Enumeration;
import java.util.Vector;

import voxware.util.SysLog;
import voxware.util.ObjectSet;

public class GrammarRule implements Serializable {

  public  String        name;                 // The rule name
  private boolean       exportable = false;   // Whether the rule can be exported
  private Grammar       grammar;              // The Grammar that contains the rule
  private Vector        examples;             // Example Strings
  private GrammarNode   startNode;            // The starting GrammarNode
  private GrammarNode   endNode;              // The ending GrammarNode
  private ObjectSet     nodes;                // The rule's GrammarNodes
  private boolean       expanded = false;

  public GrammarRule(String name, boolean exportable, Grammar grammar) {
    this.name = name;
    this.exportable = exportable;
    this.grammar = grammar;
    nodes = new ObjectSet();
    startNode = new GrammarNode();
    endNode = new GrammarNode();
    nodes.add(startNode);
    nodes.add(endNode);
  }

  public GrammarRule(String name, boolean exportable) {
    this(name, exportable, null);
  }

  public GrammarRule(String name, Grammar grammar) {
    this(name, false, grammar);
  }

  public GrammarRule(String name) {
    this(name, false, null);
  }

  public String name() {
    return name;
  }

  public void exportable(boolean exportable) {
    this.exportable = exportable;
  }

  public boolean exportable() {
    return exportable;
  }

  public void grammar(Grammar grammar) {
    this.grammar = grammar;
  }

  public Grammar grammar() {
    return grammar;
  }

  public void addExample(String example) {
    if (examples == null) examples = new Vector();
    examples.addElement(example);
  }

  public Vector examples() {
    return examples;
  }

  public GrammarNode startNode() {
    return startNode;
  }

  public GrammarNode endNode() {
    return endNode;
  }

  public ObjectSet nodes() {
    return nodes;
  }

  public boolean addNode(GrammarNode node) {
    return nodes.add(node);
  }

  public boolean expanded() {
    return expanded;
  }

  public void expanded(boolean expanded) {
    this.expanded = expanded;
  }

   // Construct the closures of all the constituent GrammarNodes
  public void close() {
    Enumeration nodeEnum = nodes.iterator();
    while (nodeEnum.hasMoreElements()) ((GrammarNode)nodeEnum.nextElement()).closure();
  }

   // Return the parse of the given Vector of tokens, or null if none
  public String parse(Vector tokens) {
    String resultString = null;
    Enumeration tokenEnum = tokens.elements();
    GrammarRuleParser parser = new GrammarRuleParser(this);
    parser.startParse();
    try {
      while (tokenEnum.hasMoreElements())
        parser.parseToken((String)tokenEnum.nextElement());                 // Throws XMLGrammarException
      GrammarPath result = parser.parseResult();
      if (result != null)
        resultString = toString() + "[" + result.toReverseString() + "]";
    } catch (XMLGrammarException e) {
      SysLog.println("GrammarRule.parse: " + e.toString());
    }
    return resultString;
  }

  public String toString(String delimiter, String indentation) {
    StringBuffer ruleString = new StringBuffer(delimiter);
    ruleString.append("Rule \"").append(name).append("\"");
    if (name.equals(grammar.root().name())) ruleString.append(" (root)");
    delimiter += indentation;
    ruleString.append(":").append(delimiter);
    ruleString.append("Start: ").append(startNode.toString()).append(delimiter);
    ruleString.append("  End: ").append(endNode.toString()).append(delimiter);
    ruleString.append("Nodes:");
    delimiter += indentation;
    if (nodes.size() > 0) {
      Enumeration nodeEnum = nodes.iterator();
      while (nodeEnum.hasMoreElements())
        ruleString.append(((GrammarNode)nodeEnum.nextElement()).toString(delimiter, indentation));
    } else {
      ruleString.append(delimiter).append("<NONE>");
    }
    return ruleString.toString();
  }

  public String toString() {
    return "$" + name;
  }
}
