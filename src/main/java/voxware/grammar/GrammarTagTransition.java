package voxware.grammar;

import java.io.Serializable;
import voxware.util.ObjectSet;

public class GrammarTagTransition extends GrammarTransition implements Serializable {

  public GrammarTag tag;

  public GrammarTagTransition(GrammarTag tag, ObjectSet destinationNodes) {
    super(destinationNodes);
    this.tag = tag;
  }

  public GrammarTagTransition(GrammarTag tag) {
    super();
    this.tag = tag;
  }

  public GrammarTag tag() {
    return tag;
  }

  public String toString(String delimiter, String indentation) {
    StringBuffer transitionString = new StringBuffer(delimiter);
    transitionString.append("Transition on tag ").append(tag.toString()).append(" to:").append(super.toString(delimiter + indentation, indentation));
    return transitionString.toString();
  }
}
