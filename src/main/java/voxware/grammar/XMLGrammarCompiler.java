/**
 * A DOM-based compiler for XMLGrammars
 *
 * Based on the DOMWriter code from I.B.M.
 */

package voxware.grammar;

import java.util.*;
import java.io.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import voxware.browser.DOMParserWrapper;
import voxware.util.*;

 /** An XMLGrammarCompiler */
public class XMLGrammarCompiler {

  // Constants

  public static final String  DEFAULT_PARSER_NAME = "voxware.browser.DOMParser";
  public static final String  DEFAULT_RULE_DELIMITER = "::";

  // Data

  private static String  RuleNameDelimiter = DEFAULT_RULE_DELIMITER;
  private static String  DefaultRuleName = null;
  private static String  PRINTWRITER_ENCODING = "UTF8";

  private static String JAVA_SUPPORTED_ENCODINGS[] = 
  { "Default", "8859_1", "8859_2", "8859_3", "8859_4", "8859_5", "8859_6", 
     "8859_7", "8859_8", "8859_9", "Cp037", "Cp273", "Cp277", "Cp278",
     "Cp280", "Cp284", "Cp285", "Cp297", "Cp420", "Cp424", "Cp437",
     "Cp500", "Cp737", "Cp775", "Cp838", "Cp850", "Cp852", "Cp855", "Cp856",
     "Cp857", "Cp860", "Cp861",
     "Cp862", "Cp863", "Cp864", "Cp865", "Cp866", "Cp868", "Cp869", "Cp870",
     "Cp871", "Cp874", "Cp875",
     "Cp918", "Cp921", "Cp922", "Cp930", "Cp933", "Cp935", "Cp937", "Cp939",
     "Cp942", "Cp948", "Cp949",
     "Cp950", "Cp964", "Cp970", "Cp1006", "Cp1025", "Cp1026", "Cp1046", 
     "Cp1097", "Cp1098", "Cp1112",
     "Cp1122", "Cp1123", "Cp1124", "Cp1250", "Cp1251", "Cp1252", "Cp1253",
     "Cp1254", "Cp1255", "Cp1256",
     "Cp1257", "Cp1258", "Cp1381", "Cp1383", "Cp33722", "MS874",
     "DBCS_ASCII", "DBCS_EBCDIC", "EUC", "EUCJIS", "GB2312", 
      "GBK", "ISO2022CN_CNS", "ISO2022CN_GB",
     "JIS",
     "JIS0208", "KOI8_R", "KSC5601","MS874",
     "SJIS", "SingleByte", "Big5", "CNS11643",
     "MacArabic", "MacCentralEurope", "MacCroatian", "MacCyrillic",
     "MacDingbat", "MacGreek",
     "MacHebrew", "MacIceland", "MacRoman", "MacRomania", "MacSymbol",
     "MacThai", "MacTurkish",
     "MacUkraine", "SJIS", "Unicode", "UnicodeBig", "UnicodeLittle", "UTF8"};

  protected static boolean canonical;


  // Constructors

  private XMLGrammarCompiler(boolean canonical) throws UnsupportedEncodingException {
    this(getBrowserEncoding(), canonical);
  }

  public XMLGrammarCompiler(String encoding, boolean canonical) throws UnsupportedEncodingException {
    this.canonical = canonical;
  }


  // Methods

  public static String getBrowserEncoding() {
     return (PRINTWRITER_ENCODING);
  }

  public static void setBrowserEncoding(String encoding) {
     PRINTWRITER_ENCODING = encoding;
  }

  public static boolean isValidJavaEncoding(String encoding) {
    for (int i = 0; i < JAVA_SUPPORTED_ENCODINGS.length; i++)
       if (encoding.equals( JAVA_SUPPORTED_ENCODINGS[i]))
          return true;
    return false;
  }

  private static void printValidJavaEncoding() {
    SysLog.println("    Java Encoding one of (case sensitive):" );
    SysLog.print("   " );
    for (int i = 0; i < JAVA_SUPPORTED_ENCODINGS.length; i++) {
      SysLog.print(JAVA_SUPPORTED_ENCODINGS[i] + " " );
      if((i % 7 ) == 0) {
        SysLog.println();
        SysLog.print("   ");
      }
    }
  }


  // Main

   /** Main program entry point. */
  public static void main(String argv[]) {
    SysLog.initialize(System.out);
    SysLog.clearShowStamp();
    SysTimer.initialize(System.out);

     // Is there anything to do?
    if (argv.length == 0) {
      printUsage();
      System.exit(1);
    }

     // Variables
    String   parserName      = DEFAULT_PARSER_NAME;
    boolean  canonical       = false;
    String   encoding        = "UTF8";                 // Default encoding
    String   grammarFileName = null;
    String   outputFileName  = null;
    String   testFileName    = null;
    boolean  verify          = false;                  // Verify rules against embedded examples

     // Get the parameters from the command line
    for (int i = 0; i < argv.length; i++) {

      String arg = argv[i];

       // Options
      if (arg.startsWith("-")) {

        if (arg.equals("-c")) {
          canonical = true;
          continue;
        }

        if (arg.equals("-d")) {
          if (i == argv.length - 1) {
            SysLog.println("XMLGrammarCompiler.main: ERROR - missing rule name delimiter");
            System.exit(1);
          }
          RuleNameDelimiter = argv[++i];
          continue;
        }

        if (arg.equals("-e")) {
          if (i == argv.length - 1) {
            SysLog.println("XMLGrammarCompiler.main: ERROR - missing encoding name");
            printValidJavaEncoding();
            System.exit(1);
          } else {
            encoding = argv[++i];
            if (isValidJavaEncoding(encoding)) {
              setBrowserEncoding(encoding);
            } else {
              printValidJavaEncoding();
              System.exit(1);
            }
          }
          continue;
        }

        if (arg.equals("-h") || arg.equals("-?")) {
          printUsage();
          System.exit(1);
        }

        if (arg.equals("-l")) {
          int printLevel = Integer.parseInt(argv[++i]);
          SysLog.setPrintLevel(printLevel);
          continue;
        }

        if (arg.equals("-m")) {
          long printMask = Long.parseLong(argv[++i], 16);
          SysLog.setPrintMask(printMask);
          continue;
        }

        if (arg.equals("-o")) {
          if (i == argv.length - 1) {
            SysLog.println("XMLGrammarCompiler.main: ERROR - missing output file name");
            System.exit(1);
          }
          outputFileName = argv[++i];
          continue;
        }

        if (arg.equals("-p")) {
          if (i == argv.length - 1) {
            SysLog.println("XMLGrammarCompiler.main: ERROR - missing parser name");
            System.exit(1);
          }
          parserName = argv[++i];
          continue;
        }

        if (arg.equals("-r")) {
          if (i == argv.length - 1) {
            SysLog.println("XMLGrammarCompiler.main: ERROR - missing default rule name");
            System.exit(1);
          }
          DefaultRuleName = argv[++i];
          continue;
        }

        if (arg.equals("-t")) {
          if (i == argv.length - 1) {
            SysLog.println("XMLGrammarCompiler.main: ERROR - missing test file name");
            System.exit(1);
          }
          testFileName = argv[++i];
          continue;
        }

        if (arg.equals("-v")) {
          verify = true;
          continue;
        }

      }
      grammarFileName = arg;
    }

    if (grammarFileName == null) {
      printUsage();
      System.exit(1);
    }

    SysLog.println("XMLGrammarCompiler.main: Starting with grammar file \"" + grammarFileName + "\"");
    SysLog.println("XMLGrammarCompiler.main: Default file encoding is " + System.getProperty("file.encoding"));
    SysLog.println("XMLGrammarCompiler.main: System architecture is " + System.getProperty("os.arch"));
    SysLog.println("XMLGrammarCompiler.main: Operating system is " + System.getProperty("os.name") + " " + System.getProperty("os.version"));
    SysLog.println("XMLGrammarCompiler.main: VXML encoding is " + PRINTWRITER_ENCODING);

    try {
      DOMParserWrapper xmlParser = (DOMParserWrapper)Class.forName(parserName).newInstance();
      if (SysLog.printLevel > 0) SysLog.println("XMLGrammarCompiler.main: Starting XML parsing ...");
      long time = System.currentTimeMillis();
      Document page = xmlParser.parse(new InputSource(new FileInputStream(grammarFileName)));
      if (SysLog.printLevel > 0) SysLog.println("XMLGrammarCompiler.main: Starting XMLGrammar compilation ...");
      XMLGrammar grammar = compile(page);               // Throws DOMException, NumberFormatException, XMLGrammarException
      time = System.currentTimeMillis() - time;
      if (SysLog.printLevel > 0) SysLog.println("XMLGrammarCompiler.main: XMLGrammar compilation took " + time + " msec");
      if (SysLog.printLevel > 0) SysLog.print(grammar.toString());
      if (outputFileName != null) {
        if (SysLog.printLevel > 0) SysLog.println("XMLGrammarCompiler.main: writing XMLGrammar to file \"" + outputFileName + "\"");
        time = System.currentTimeMillis();
        writeGrammarFile(grammar, outputFileName);
        time = System.currentTimeMillis() - time;
        if (SysLog.printLevel > 0) SysLog.println("XMLGrammarCompiler.main: XMLGrammar serialization took " + time + " msec");
        time = System.currentTimeMillis();
        grammar = readGrammarFile(outputFileName);      // Throws ClassNotFoundException
        time = System.currentTimeMillis() - time;
        if (SysLog.printLevel > 0) SysLog.println("XMLGrammarCompiler.main: XMLGrammar deserialization took " + time + " msec");
      }
      if (grammar == null) throw new RuntimeException("XMLGrammarCompiler.main: grammar is null");
      if (verify) verify(grammar);
      if (testFileName != null) test(grammar, testFileName);
    } catch (Exception exc) {
      exc.printStackTrace(System.err);
      SysLog.println("XMLGrammarCompiler.main: caught " + exc.toString());
    }
  }

   // Print the usage
  private static void printUsage() {
    SysLog.println("usage: java voxware.grammar.XMLGrammarCompiler (options) grammarFileName");
    SysLog.println("");
    SysLog.println("options:");
    SysLog.println("  -c        Produce canonical XML output (default is not canonical)");
    SysLog.println("  -d string Rule name delimiter (default is \"" + DEFAULT_RULE_DELIMITER + "\")");
    SysLog.println("  -e name   Output Java Encoding (default is UTF8)");
    SysLog.println("  -h        Display this help screen");
    SysLog.println("  -l int    Log level (default is 0)");
    SysLog.println("  -m long   Hexadecimal bit mask to enable selective logging (default is 0)");
    SysLog.println("  -o file   Output file name (default is none)");
    SysLog.println("  -p name   Specify DOM parser wrapper by name (default is \"" + DEFAULT_PARSER_NAME + "\")");
    SysLog.println("  -r name   Default rule name (default is none)");
    SysLog.println("  -t file   File of test sequences to parse (default is none)");
    SysLog.println("  -v        Verify rules against embedded examples (default is not to verify)");
  }

   // Verify the rules against embedded examples
  private static void verify(XMLGrammar grammar) {
    int  trials = 0;
    int  successes = 0;
    SysLog.println("XMLGrammarCompiler.verify: starting");
    Enumeration rules = grammar.rules().elements();
    while (rules.hasMoreElements()) {
      GrammarRule rule = (GrammarRule)rules.nextElement();
      SysLog.print("  Rule " + rule.toString());
      Vector examples = rule.examples();
      int numExamples = examples == null ? 0 : examples.size();
      if (numExamples == 0) {
        SysLog.println(" has no examples");
      } else {
        if (numExamples == 1) SysLog.println(" has one example:");
        else                  SysLog.println(" has " + numExamples + " examples:");
        Enumeration exampleEnum = examples.elements();
        while (exampleEnum.hasMoreElements()) {
          String example = (String)exampleEnum.nextElement();
          if (parse(rule, example, "    ")) successes++;
          trials++;
        }
      }
    }
    SysLog.println("XMLGrammarCompiler.verify: " + trials + " trials; " + successes + " successes; " + (trials - successes) + " failures");
    SysLog.println("XMLGrammarCompiler.verify: complete");
  }

   // Parse the phrases in the test file
  private static void test(XMLGrammar grammar, String fileName) {
    int  trials = 0;
    int  successes = 0;
    long start = 0;
    try {
      FileReader testFile = new FileReader(fileName);
      BufferedReader testSequences = new BufferedReader(testFile);
      String sequence;
      SysLog.println("XMLGrammarCompiler.test: starting");
      start = System.currentTimeMillis();
      while (true) {
        sequence = testSequences.readLine();
        if (sequence == null) break;
        int endOfRuleName = sequence.indexOf(RuleNameDelimiter);
         // If no default rule name is provided on the command line, use the grammar's root rule
        GrammarRule rule = DefaultRuleName == null ? grammar.root() : grammar.rule(DefaultRuleName.trim());
        if (endOfRuleName > 0) {
          rule = grammar.rule(sequence.substring(0, endOfRuleName).trim());
          sequence = sequence.substring(endOfRuleName + RuleNameDelimiter.length());
        }
        if (parse(rule, sequence.trim(), "  ")) successes++;
        trials++;
      }
    } catch (IOException e) {
      SysLog.println("XMLGrammarCompiler.test: exception reading line from test file -- " + e.toString());
    }
    if (trials > 0)
      SysLog.println("XMLGrammarCompiler.test: mean time per trial = " + ((double)(System.currentTimeMillis() - start) / (double)trials) + " msec");
    SysLog.println("XMLGrammarCompiler.test: " + trials + " trials; " + successes + " successes; " + (trials - successes) + " failures");
  }

   // Parse the given phrase using the given GrammarRule and return success
  private static boolean parse(GrammarRule rule, String phrase, String indentation) {
    String result = rule.parse(XMLGrammar.tokens(phrase));
    if (result != null) {
      SysLog.println(indentation + "Success: \"" + phrase + "\"");
      SysLog.println(indentation + "  Parse: " + result);
    } else {
      SysLog.println(indentation + "FAILURE: \"" + phrase + "\"");
    }
    return (result != null);
  }

   // Serialize the grammar and write it to a file
  private static void writeGrammarFile(XMLGrammar grammar, String fileName) {
    try {
      ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
      out.writeObject(grammar);
      out.flush();
      out.close();
    } catch (IOException e) {
      SysLog.println("XMLGrammarCompiler.writeGrammarFile: exception writing the compiled grammar file -- " + e.toString());
    }
  }

   // Deserialize the file and reconstruct the grammar
  private static XMLGrammar readGrammarFile(String fileName) throws ClassNotFoundException {
    XMLGrammar grammar = null;
    try {
      ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
      grammar = (XMLGrammar)in.readObject();
      in.close();
    } catch (IOException e) {
      SysLog.println("XMLGrammarCompiler.readGrammarFile: exception reading the compiled grammar file -- " + e.toString());
    }
    return grammar;
  }

   // Compile the specified node
  private static XMLGrammar compile(Node node) throws DOMException, NumberFormatException, XMLGrammarException {

    XMLGrammar grammar = null;

    // Is there anything to do?
    if (node == null) return grammar;

    int type = node.getNodeType();
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK)) {
      SysLog.println("");
      SysLog.println("Node: " + node.getNodeName() + " = \"" + node.getNodeValue() + "\"");
    }

    switch (type) {
       // Compile the document
      case Node.DOCUMENT_NODE:
        if (!canonical && SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
          SysLog.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        grammar = compile(((Document)node).getDocumentElement());
        break;

       // Compile an element and its attributes
      case Node.ELEMENT_NODE:
        String name = node.getNodeName();
        Attr attrs[] = null;
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK)) {
          SysLog.print("<");
          SysLog.print(name);
          attrs = sortAttributes(node.getAttributes());
          for ( int i = 0; i < attrs.length; i++ ) {
            Attr attr = attrs[i];
            SysLog.print(" " + attr.getNodeName() + "=\"" + normalize(attr.getNodeValue()) + "\"");
          }
          SysLog.print(">");
        }

        if (SysLog.printLevel > 2 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
          SysLog.println("XMLGGrammar.compile: XMLGrammarElements.index(" + name + ")=" + XMLGrammarElements.index(name));
        switch (XMLGrammarElements.index(name)) {
          case XMLGrammarElements.GRAMMAR_INDEX:
             // Compile the grammar
            grammar = new XMLGrammar((Element)node);
            break;
          default:
            SysLog.print("XMLGrammarCompiler.compile: WARNING - ignoring invalid element <" + name + ">");
            break;
        }
        break;

       // Deal with entity reference nodes
      case Node.ENTITY_REFERENCE_NODE:
        if (canonical) {
          NodeList children = node.getChildNodes();
          if ( children != null ) {
            int len = children.getLength();
            for ( int i = 0; i < len; i++ )
              compile(children.item(i));
          }
        } else if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK)) {
          SysLog.print("&" + node.getNodeName() + ";");
        }
        break;

         // Compile cdata sections
      case Node.CDATA_SECTION_NODE:
        if (canonical) {
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
            SysLog.print(normalize(node.getNodeValue()));
        } else {
          if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
            SysLog.print("<![CDATA[" + node.getNodeValue() + "]]>");
        }
        break;

         // Compile text
      case Node.TEXT_NODE:
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
          SysLog.print(normalize(node.getNodeValue()));
        break;
    }

    if (type == Node.ELEMENT_NODE && SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.GRAMMAR_MASK))
      SysLog.println("</" + node.getNodeName() + ">");

    return grammar;
  }

  // Returns a sorted list of attributes
  private static Attr[] sortAttributes(NamedNodeMap attrs) {
    int len = (attrs != null) ? attrs.getLength() : 0;
    Attr array[] = new Attr[len];
    for ( int i = 0; i < len; i++ ) {
      array[i] = (Attr)attrs.item(i);
    }
    for (int i = 0; i < len - 1; i++) {
      String name  = array[i].getNodeName();
      int    index = i;
      for (int j = i + 1; j < len; j++) {
        String curName = array[j].getNodeName();
        if (curName.compareTo(name) < 0 ) {
          name  = curName;
          index = j;
        }
      }
      if ( index != i ) {
        Attr temp    = array[i];
        array[i]     = array[index];
        array[index] = temp;
      }
    }
    return (array);
  }

  // Normalizes the given string
  private static String normalize(String s) {
    StringBuffer str = new StringBuffer();
    int len = (s != null) ? s.length() : 0;
    for ( int i = 0; i < len; i++ ) {
      char ch = s.charAt(i);
      switch ( ch ) {
        case '<': {
            str.append("&lt;");
            break;
          }
        case '>': {
            str.append("&gt;");
            break;
          }
        case '&': {
            str.append("&amp;");
            break;
          }
        case '"': {
            str.append("&quot;");
            break;
          }
        case '\r':
        case '\n': {
            if ( canonical ) {
              str.append("&#");
              str.append(Integer.toString(ch));
              str.append(';');
              break;
            } // else, default append char
          }
        default: {
            str.append(ch);
          }
      }
    }
    return (str.toString());
  }

}
