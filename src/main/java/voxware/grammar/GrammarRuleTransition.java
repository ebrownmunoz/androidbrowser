package voxware.grammar;

import java.io.Serializable;

import voxware.util.ObjectSet;

public class GrammarRuleTransition extends GrammarTransition implements Serializable {

  public GrammarRule rule;

  public GrammarRuleTransition(GrammarRule rule, ObjectSet destinationNodes) {
    super(destinationNodes);
    this.rule = rule;
  }

  public GrammarRuleTransition(GrammarRule rule) {
    super();
    this.rule = rule;
  }

  public GrammarRule rule() {
    return rule;
  }

  public String toString(String delimiter, String indentation) {
    StringBuffer transitionString = new StringBuffer(delimiter);
    transitionString.append("Transition on rule ").append(rule.toString()).append(" to:").append(super.toString(delimiter + indentation, indentation));
    return transitionString.toString();
  }
}
