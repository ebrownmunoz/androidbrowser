package voxware.grammar;

import java.util.Hashtable;

public class XMLGrammarElements {

  // Constants

  public static final int     NULL_INDEX     = 0;

   // Names and indices of all the XML Grammar Elements
  public static final int     ALIAS_INDEX    = NULL_INDEX + 1;
  public static final String  ALIAS_NAME     = "alias";
  public static final int     EXAMPLE_INDEX  = ALIAS_INDEX + 1;
  public static final String  EXAMPLE_NAME   = "example";
  public static final int     GRAMMAR_INDEX  = EXAMPLE_INDEX + 1;
  public static final String  GRAMMAR_NAME   = "grammar";
  public static final int     ITEM_INDEX     = GRAMMAR_INDEX + 1;
  public static final String  ITEM_NAME      = "item";
  public static final int     LEXICON_INDEX  = ITEM_INDEX + 1;
  public static final String  LEXICON_NAME   = "lexicon";
  public static final int     META_INDEX     = LEXICON_INDEX + 1;
  public static final String  META_NAME      = "meta";
  public static final int     ONE_OF_INDEX   = META_INDEX + 1;
  public static final String  ONE_OF_NAME    = "one-of";
  public static final int     RULE_INDEX     = ONE_OF_INDEX + 1;
  public static final String  RULE_NAME      = "rule";
  public static final int     RULEREF_INDEX  = RULE_INDEX + 1;
  public static final String  RULEREF_NAME   = "ruleref";
  public static final int     TAG_INDEX      = RULEREF_INDEX + 1;
  public static final String  TAG_NAME       = "tag";
  public static final int     TOKEN_INDEX    = TAG_INDEX + 1;
  public static final String  TOKEN_NAME     = "token";

  public static final int     NUM_NODE_TYPES = TAG_INDEX;

  // Variables

  public static Hashtable  hash = new Hashtable(NUM_NODE_TYPES);

  // Static Initializer

  static {
     // Initialize XMLGElements.hash with all the XML Grammar Elements
    hash.put(ALIAS_NAME, new Integer(ALIAS_INDEX));
    hash.put(EXAMPLE_NAME, new Integer(EXAMPLE_INDEX));
    hash.put(GRAMMAR_NAME, new Integer(GRAMMAR_INDEX));
    hash.put(ITEM_NAME, new Integer(ITEM_INDEX));
    hash.put(LEXICON_NAME, new Integer(LEXICON_INDEX));
    hash.put(META_NAME, new Integer(META_INDEX));
    hash.put(ONE_OF_NAME, new Integer(ONE_OF_INDEX));
    hash.put(RULE_NAME, new Integer(RULE_INDEX));
    hash.put(RULEREF_NAME, new Integer(RULEREF_INDEX));
    hash.put(TAG_NAME, new Integer(TAG_INDEX));
    hash.put(TOKEN_NAME, new Integer(TOKEN_INDEX));
  }

   /* Get the index of an Element with the given name */
  public static int index(String name) {
    int index;
    Integer iindex = (Integer) hash.get(name);
    if (iindex == null) index = NULL_INDEX;
    else                index = iindex.intValue();
    return index;
  }
}
