package voxware.grammar;

import java.io.Serializable;

import voxware.util.ObjectSet;

public class GrammarTokenTransition extends GrammarTransition implements Serializable {

  public int tokenId;

  public GrammarTokenTransition(int tokenId, ObjectSet destinationNodes) {
    super(destinationNodes);
    this.tokenId = tokenId;
  }

  public GrammarTokenTransition(int tokenId) {
    super();
    this.tokenId = tokenId;
  }

  public int tokenId() {
    return tokenId;
  }

  public String toString(String delimiter, String indentation) {
    StringBuffer transitionString = new StringBuffer(delimiter);
    transitionString.append("Transition on token ").append(tokenId).append(" to:").append(super.toString(delimiter + indentation, indentation));
    return transitionString.toString();
  }
}
