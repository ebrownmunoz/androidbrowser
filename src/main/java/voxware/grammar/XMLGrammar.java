package voxware.grammar;

import java.io.Serializable;

import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import voxware.util.SysLog;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLGrammar extends Grammar implements Serializable {

  public  static final int SEPARATOR = '-';                             // Separator character for repeat specifications
  private static final int NO_LIMIT = Integer.MAX_VALUE;                // Infinite repeat count

  public static final String VOICE_MODE_NAME = "voice";
  public static final String DTMF_MODE_NAME = "dtmf";

  private transient Element grammar;
  private String  tagFormat;                                            // The format of the semantic tags
  private String  version = "1.0";
  private String  language = "en-US";
  private String  mode = VOICE_MODE_NAME;                               // "voice" or "dtmf"

   /** Construct an XMLGrammar from a NamedNodeMap of attribute Nodes */
  public XMLGrammar(NamedNodeMap nnm) throws DOMException {
    getAttributes(nnm);
  }

   /** Construct an XMLGrammar from a <grammar> element in the DOM tree */
  public XMLGrammar(Element grammar) throws DOMException, NumberFormatException, XMLGrammarException {
    this.grammar = grammar;
    getAttributes(((Node)grammar).getAttributes());                     // Throws DOMException
    compile();                                                          // Throws DOMException, NumberFormatException, XMLGrammarException
  }

  /** Get the attributes from a NamedNodeMap of attribute Nodes */
  protected void getAttributes(NamedNodeMap nnm) throws DOMException {
    if (nnm != null) {
      for (int i = 0; i < nnm.getLength(); i++) {
        Node attrNode = nnm.item(i);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("tag-format"))
          tagFormat = attrNode.getNodeValue();                          // Throws DOMException
        else if (nodeName.equalsIgnoreCase("version"))
          version = attrNode.getNodeValue();                            // Throws DOMException
        else if (nodeName.equalsIgnoreCase("xml:lang"))
          language = attrNode.getNodeValue();                           // Throws DOMException
        else if (nodeName.equalsIgnoreCase("root"))
          root = attrNode.getNodeValue();                               // Throws DOMException
        else if (nodeName.equalsIgnoreCase("mode"))
          mode = attrNode.getNodeValue();                               // Throws DOMException
        else if (SysLog.printLevel > 0)
          SysLog.println("XMLGrammar.compileAlias: WARNING - ignoring unexpected attribute \"" + nodeName + "\" of <grammar>");
      }
    }
  }

   // Compile this XMLGrammar from the children of <grammar>
  protected void compile() throws DOMException, NumberFormatException, XMLGrammarException {
    compile(grammar.getChildNodes());                                   // Throws DOMException, NumberFormatException, XMLGrammarException
  }

   // Compile this XMLGrammar from a NodeList of the children
  protected void compile(NodeList children) throws DOMException, NumberFormatException, XMLGrammarException {

    String   nodeName;
    for (int i = 0; i < children.getLength(); i++) {
      Node childNode = children.item(i);
      if (childNode.getNodeType() == Node.ELEMENT_NODE) {
        nodeName = childNode.getNodeName();
        switch (XMLGrammarElements.index(nodeName)) {
          case XMLGrammarElements.ALIAS_INDEX:
            compileAlias(childNode);                                    // Throws DOMException, XMLGrammarException
            break;
          case XMLGrammarElements.LEXICON_INDEX:
            compileLexicon(childNode);                                  // Throws DOMException, XMLGrammarException
            break;
          case XMLGrammarElements.META_INDEX:
            compileMeta(childNode);                                     // Throws DOMException, XMLGrammarException
            break;
          case XMLGrammarElements.RULE_INDEX:
            compileRule(childNode);                                     // Throws DOMException, NumberFormatException, XMLGrammarException
            break;
          default:
            if (SysLog.printLevel > 0)
              SysLog.println("XMLGrammar.compile: WARNING - ignoring unexpected child node \"" + nodeName + "\" in <grammar>");
            break;
        }
      }
    }

     // Check that rules exist
    if (rules == null)
      throw new XMLGrammarException("XMLGrammar.compile: the grammar contains no rules");

     // Check that the root rule exists
    if (root == null || rules.get(root) == null)
      SysLog.println("XMLGrammar.compile: WARNING - the root rule \"" + root + "\" is undefined");

     // Close all the GrammarNodes
    this.close();
  }

  public String mode() {
    return mode;
  }

  private void compileAlias(Node node) throws DOMException, XMLGrammarException {

    String uri  = null;                                                 // URI of underlying rule (REQUIRED)
    String type = null;                                                 // Mime type of underlying rule
    String name = null;                                                 // Alias (REQUIRED)
    
    NamedNodeMap nnm = node.getAttributes();
    if (nnm != null) {
      for (int i = 0; i < nnm.getLength(); i++) {
        Node attrNode = nnm.item(i);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("uri"))
          uri = attrNode.getNodeValue();                                // Throws DOMException
        else if (nodeName.equalsIgnoreCase("type"))
          type = attrNode.getNodeValue();                               // Throws DOMException
        else if (nodeName.equalsIgnoreCase("name"))
          name = attrNode.getNodeValue();                               // Throws DOMException
        else if (SysLog.printLevel > 0)
          SysLog.println("XMLGrammar.compileAlias: WARNING - ignoring unexpected attribute \"" + nodeName + "\" of <alias>");
      }
    }

    if (uri == null || name == null) {
      throw new XMLGrammarException("<alias> lacks either uri or name attribute");
    } else {
      SysLog.println("XMLGrammar.compileAlias: WARNING - <alias> not yet supported");
    }
  }

  private void compileLexicon(Node node) throws DOMException, XMLGrammarException {
    String uri  = null;                                                 // URI of lexicon (REQUIRED)
    NamedNodeMap nnm = node.getAttributes();
    if (nnm != null) {
      for (int i = 0; i < nnm.getLength(); i++) {
        Node attrNode = nnm.item(i);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("uri"))
          uri = attrNode.getNodeValue();                                // Throws DOMException
        else if (SysLog.printLevel > 0)
          SysLog.println("XMLGrammar.compileLexicon: WARNING - ignoring unexpected attribute \"" + nodeName + "\" of <lexicon>");
      }
    }

    if (uri == null) {
      throw new XMLGrammarException("<lexicon> lacks uri attribute");
    } else {
      SysLog.println("XMLGrammar.compileLexicon: WARNING - <lexicon> not yet supported");
    }
  }

  private void compileExample(Node node, GrammarRule rule) throws DOMException {
    StringBuffer exampleBuffer = new StringBuffer();
    NodeList children = node.getChildNodes();
    for (int i = 0; i < children.getLength(); i++) {
      Node childNode = children.item(i);
      short nodeType = childNode.getNodeType();
      if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE)
        exampleBuffer.append(childNode.getNodeValue());                 // Throws DOMException
      else if (nodeType != Node.COMMENT_NODE && SysLog.printLevel > 0)
        SysLog.println("XMLGrammar.compileExample: WARNING - ignoring unexpected type " + nodeType + " (non-text) child node in <example>");
    }
    rule.addExample(exampleBuffer.toString().trim());                   // Throws DOMException
  }

  private void compileMeta(Node node) throws DOMException, XMLGrammarException {

    String name  = null;
    String content = null;                                              // (REQUIRED)
    String httpEquiv = null;
    
    NamedNodeMap nnm = node.getAttributes();
    if (nnm != null) {
      for (int i = 0; i < nnm.getLength(); i++) {
        Node attrNode = nnm.item(i);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("name"))
          name = attrNode.getNodeValue();                               // Throws DOMException
        else if (nodeName.equalsIgnoreCase("content"))
          content = attrNode.getNodeValue();                            // Throws DOMException
        else if (nodeName.equalsIgnoreCase("http-equiv"))
          httpEquiv = attrNode.getNodeValue();                          // Throws DOMException
        else if (SysLog.printLevel > 0)
          SysLog.println("XMLGrammar.compileMeta: WARNING - ignoring unexpected attribute \"" + nodeName + "\" of <meta>");
      }
    }

    if (content == null) {
      throw new XMLGrammarException("<meta> lacks content attribute");
    } else {
      SysLog.println("XMLGrammar.compileMeta: WARNING - <meta> not yet supported");
    }
  }

  private void expandRule(Vector nodes, GrammarRule rule)
  throws DOMException, NumberFormatException, XMLGrammarException {
    rule.expanded(true);
    expandRule(nodes, rule, rule.startNode(), rule.endNode());          // Throws DOMException, NumberFormatException, XMLGrammarException
  }


  private void expandRule(Vector nodes, GrammarRule rule, GrammarNode startNode, GrammarNode endNode)
  throws DOMException, NumberFormatException, XMLGrammarException {
     // Walk backwards through the Vector of Nodes, expunging the Nodes that have no content
    int index = nodes.size();
    while (index-- > 0) {
      Node node = (Node)nodes.elementAt(index);
      short nodeType = node.getNodeType();
      if (nodeType == Node.TEXT_NODE && node.getNodeValue().trim().length() == 0 || nodeType == Node.COMMENT_NODE)
        nodes.removeElementAt(index);
    }
     // Step through the Vector of Nodes, adding transitions in sequence between the starting and ending GrammarNodes
    GrammarNode thisNode = startNode;
    GrammarNode nextNode;
    Enumeration nodeEnumeration = nodes.elements();
    while (nodeEnumeration.hasMoreElements()) {
      Node node = (Node)nodeEnumeration.nextElement();
       // If this isn't the last Node in the sequence, construct a GrammarNode to serve as destination
      if (nodeEnumeration.hasMoreElements()) {
        nextNode = new GrammarNode();
        rule.addNode(nextNode);
       // Otherwise, use the given endNode as destination
      } else {
        nextNode = endNode;
      }
       // Process the node
      String nodeName = node.getNodeName();
      switch (node.getNodeType()) {
        case Node.TEXT_NODE:
          compileTokens(node.getNodeValue(), rule, thisNode, nextNode);
          break;
        case Node.ELEMENT_NODE:
          switch (XMLGrammarElements.index(nodeName)) {
            case XMLGrammarElements.ITEM_INDEX:
              compileItem(node, rule, thisNode, nextNode);              // Throws DOMException, NumberFormatException, XMLGrammarException
              break;
            case XMLGrammarElements.ONE_OF_INDEX:
              compileOneOf(node, rule, thisNode, nextNode);             // Throws DOMException, NumberFormatException, XMLGrammarException
              break;
            case XMLGrammarElements.RULEREF_INDEX:
              compileRuleRef(node, rule, thisNode, nextNode);           // Throws DOMException, XMLGrammarException
              break;
            case XMLGrammarElements.TAG_INDEX:
              compileTag(node, thisNode, nextNode);
              break;
            case XMLGrammarElements.TOKEN_INDEX:
              compileToken(node, thisNode, nextNode);                   // Throws DOMException
              break;
            default:
              if (SysLog.printLevel > 0)
                SysLog.println("XMLGrammar.expandRule: WARNING - ignoring unexpected node \"" + nodeName + "\" in rule expansion");
              break;
          }
          break;
      }
      thisNode = nextNode;
    }
  }

  private void compileItem(Node node, GrammarRule rule, GrammarNode startNode, GrammarNode endNode)
  throws DOMException, NumberFormatException, XMLGrammarException {

    String  repeat = null;
    int     required = 1;
    int     optional = 0;
    float   repeatProb = (float)1.0;                                    // Not yet supported
    float   weight = (float)1.0;                                        // Not yet supported

    NamedNodeMap nnm = node.getAttributes();
    if (nnm != null) {
      for (int i = 0; i < nnm.getLength(); i++) {
        Node attrNode = nnm.item(i);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("repeat"))
          repeat = attrNode.getNodeValue().trim();                      // Throws DOMException
        else if (nodeName.equalsIgnoreCase("repeat-prob"))
          repeatProb = Float.valueOf(attrNode.getNodeValue()).floatValue();  // Throws DOMException, NumberFormatException
        else if (nodeName.equalsIgnoreCase("weight"))
          weight = Float.valueOf(attrNode.getNodeValue()).floatValue(); // Throws DOMException, NumberFormatException
        else if (SysLog.printLevel > 0)
          SysLog.println("XMLGrammar.compileItem: WARNING - ignoring unexpected attribute \"" + nodeName + "\" of <item>");
      }
    }

     // Parse the repeat specification, if any
    if (repeat != null) {
      int separator = repeat.indexOf(SEPARATOR);
      if (separator < 0) {
        required = Integer.parseInt(repeat);                            // Throws NumberFormatException
      } else {
        required = Integer.parseInt(repeat.substring(0, separator));    // Throws NumberFormatException
        separator++;
        optional = (separator < repeat.length()) ? Integer.parseInt(repeat.substring(separator)) - required : NO_LIMIT;  // Throws NumberFormatException
      }
      if (optional < 0)
        throw new XMLGrammarException("<item> has an invalid repeat attribute (optional = " + optional + ")");
    }

    if (required > 0 || optional > 0) {
       // Construct a Vector of the children
      NodeList children = node.getChildNodes();
      int length = children.getLength();
      Vector sequence = new Vector(length);
      for (int i = 0; i < length; i++) sequence.addElement(children.item(i));
       // Insert the required number of expansions of the Vector between the starting and ending nodes
      GrammarNode thisNode = null;
      GrammarNode nextNode = startNode;
       // If there will be a loop, make sure there is an isolated node to hold it
      int numTransitions = (optional == NO_LIMIT) ? ((required < 2) ? 2 : required) : required + optional;
      for (int i = 0; i < numTransitions; i++) {
        thisNode = nextNode;
         // Construct an intermediate GrammarNode to serve as destination
        if (i < numTransitions - 1) {
          nextNode = new GrammarNode();
          rule.addNode(nextNode);
        } else {
          nextNode = endNode;
        }
        if (i < required) {
          expandRule(sequence, rule, thisNode, nextNode);               // Throws DOMException, NumberFormatException
        } else if (optional != NO_LIMIT) {
          expandRule(sequence, rule, thisNode, nextNode);               // Throws DOMException, NumberFormatException
          thisNode.addTransition(GrammarToken.NULL_ID, endNode);
        } else {
          thisNode.addTransition(GrammarToken.NULL_ID, nextNode);
        }
      }
       // Put the loop, if any, on the last isolated node
      if (optional == NO_LIMIT)
        expandRule(sequence, rule, thisNode, thisNode);                 // Throws DOMException, NumberFormatException
    }
  }

  private void compileOneOf(Node node, GrammarRule rule, GrammarNode startNode, GrammarNode endNode)
  throws DOMException, NumberFormatException, XMLGrammarException {

    String langList = null;

    NamedNodeMap nnm = node.getAttributes();
    if (nnm != null) {
      for (int i = 0; i < nnm.getLength(); i++) {
        Node attrNode = nnm.item(i);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("lang-list"))
          langList = attrNode.getNodeValue();                           // Throws DOMException
        else if (SysLog.printLevel > 0)
          SysLog.println("XMLGrammar.compileOneOf: WARNING - ignoring unexpected attribute \"" + nodeName + "\" of <one-of>");
      }
    }

    NodeList children = node.getChildNodes();
    String   nodeName;
    for (int i = 0; i < children.getLength(); i++) {
      Node childNode = children.item(i);
      if (childNode.getNodeType() == Node.ELEMENT_NODE) {
        nodeName = childNode.getNodeName();
        switch (XMLGrammarElements.index(nodeName)) {
          case XMLGrammarElements.ITEM_INDEX:
            compileItem(childNode, rule, startNode, endNode);           // Throws DOMException, NumberFormatException, XMLGrammarException
            break;
          default:
            if (SysLog.printLevel > 0)
              SysLog.println("XMLGrammar.compileOneOf: WARNING - ignoring unexpected child node \"" + nodeName + "\" in <one-of>");
            break;
        }
      }
    }
  }

  private void compileRule(Node node) throws DOMException, NumberFormatException, XMLGrammarException {

    String       name = null;                                           // The rule id (REQUIRED)
    boolean      exportable = false;                                    // Whether the rule can be exported (DEFAULT is false)
    GrammarRule  rule = null;

    NamedNodeMap nnm = node.getAttributes();
    if (nnm != null) {
      for (int i = 0; i < nnm.getLength(); i++) {
        Node attrNode = nnm.item(i);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("id"))
          name = attrNode.getNodeValue();                               // Throws DOMException
        else if (nodeName.equalsIgnoreCase("scope"))
          exportable = attrNode.getNodeValue().equalsIgnoreCase("public");  // Throws DOMException
        else if (SysLog.printLevel > 0)
          SysLog.println("XMLGrammar.compileRule: WARNING - ignoring unexpected attribute \"" + nodeName + "\" of <rule>");
      }
    }

    if (name == null) {
      throw new XMLGrammarException("<rule> lacks name attribute");
    } else {
       // Get the rule, constructing it if necessary
      rule = this.rule(name);
      if (rule.expanded()) {
        SysLog.println("XMLGrammar.compileRule: ERROR - duplicate rule name \"" + name + "\" in <rule>");
        throw new XMLGrammarException("<rule> has duplicate name attribute \"" + name + "\"");
      } else {
        rule.exportable(exportable);
      }
      // The first rule compiled becomes the default root rule
      if (root == null)
        root = name;
    }

    NodeList children = node.getChildNodes();
    int length = children.getLength();
    Vector sequence = new Vector(length);
    for (int i = 0; i < length; i++) {
      Node childNode = children.item(i);
      switch (XMLGrammarElements.index(childNode.getNodeName())) {
        case XMLGrammarElements.EXAMPLE_INDEX:
          compileExample(childNode, rule);
          break;
        default:
          sequence.addElement(childNode);
          break;
      }
    }

    expandRule(sequence, rule);                                         // Throws DOMException, NumberFormatException, XMLGrammarException
  }

  private void compileRuleRef(Node node, GrammarRule rule, GrammarNode startNode, GrammarNode endNode) throws XMLGrammarException {

    String  uri = null;
    String  alias = null;
    String  type = null;                                                // The MIME type
    String  special = null;                                             // (NULL | VOID | GARBAGE)
    String  langList = null;

    NamedNodeMap nnm = node.getAttributes();
    if (nnm != null) {
      for (int i = 0; i < nnm.getLength(); i++) {
        Node attrNode = nnm.item(i);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("uri"))
          uri = attrNode.getNodeValue();                                // Throws DOMException
        else if (nodeName.equalsIgnoreCase("alias"))
          alias = attrNode.getNodeValue();                              // Throws DOMException
        else if (nodeName.equalsIgnoreCase("type"))
          type = attrNode.getNodeValue();                               // Throws DOMException
        else if (nodeName.equalsIgnoreCase("special"))
          special = attrNode.getNodeValue();                            // Throws DOMException
        else if (nodeName.equalsIgnoreCase("lang-list"))
          langList = attrNode.getNodeValue();                           // Throws DOMException
        else if (SysLog.printLevel > 0) 
          SysLog.println("XMLGrammar.compileRuleRef: WARNING - ignoring unexpected attribute \"" + nodeName + "\" of <ruleref>");
      }
    }

    if (uri != null && alias == null && special == null) {
      if (uri.indexOf('#') == 0) {
        String name = uri.substring(1);
         // Get the referenced rule, constructing it if necessary
        GrammarRule referencedRule = this.rule(name);
         // Add a GrammarRuleTransition via the referenced rule
        startNode.addTransition(referencedRule, endNode);
      } else {
        SysLog.println("XMLGrammar.compileRuleRef: ERROR - uri \"" + uri + "\" is not a pure reference");
        throw new XMLGrammarException("<ruleref> uri attribute \"" + uri + "\" is not a pure reference");
      }
    } else if (uri == null && alias != null && special == null) {
      SysLog.println("XMLGrammar.compileRuleRef: ERROR - alias is not supported");
      throw new XMLGrammarException("<ruleref> alias attribute is not yet supported");
    } else if (uri == null && alias == null && special != null) {
      if (special.equalsIgnoreCase("NULL")) {
        startNode.addTransition(GrammarToken.NULL_ID, endNode);
      } else if (special.equalsIgnoreCase("VOID")) {
        ;
      } else if (special.equalsIgnoreCase("GARBAGE")) {
         // Construct an intermediate GrammarNode to hold the garbage loop
        GrammarNode garbageNode = new GrammarNode();
        rule.addNode(garbageNode);
        garbageNode.addTransition(GrammarToken.GARBAGE_ID, garbageNode);
         // Attach it to the start and end nodes
        startNode.addTransition(GrammarToken.NULL_ID, garbageNode);
        garbageNode.addTransition(GrammarToken.NULL_ID, endNode);
      }
        
    } else {
      throw new XMLGrammarException("<ruleref> must have exactly one uri, alias or special attribute");
    }

  }

  private void compileTag(Node node, GrammarNode startNode, GrammarNode endNode) throws DOMException {

    StringBuffer tagBuffer = new StringBuffer();
    NodeList children = node.getChildNodes();
    for (int i = 0; i < children.getLength(); i++) {
      Node childNode = children.item(i);
      short nodeType = childNode.getNodeType();
      if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE)
        tagBuffer.append(childNode.getNodeValue().trim());              // Throws DOMException
      else if (nodeType != Node.COMMENT_NODE && SysLog.printLevel > 0)
        SysLog.println("XMLGrammar.compileTag: WARNING - ignoring unexpected type " + nodeType + " (non-text) child node in <tag>");
    }
    startNode.addTransition(new GrammarTag(tagBuffer.toString()), endNode);
  }

  private void compileToken(Node node, GrammarNode startNode, GrammarNode endNode) throws DOMException {

    String langList = null;

    NamedNodeMap nnm = node.getAttributes();
    if (nnm != null) {
      for (int i = 0; i < nnm.getLength(); i++) {
        Node attrNode = nnm.item(i);
        String nodeName = attrNode.getNodeName();
        if (nodeName.equalsIgnoreCase("lang-list"))
          langList = attrNode.getNodeValue();                           // Throws DOMException
        else if (SysLog.printLevel > 0)
          SysLog.println("XMLGrammar.compileToken: WARNING - ignoring unexpected attribute \"" + nodeName + "\" of <token>");
      }
    }

    short nodeType = node.getNodeType();
    switch (nodeType) {
      case Node.TEXT_NODE:
        compileToken(node.getNodeValue(), startNode, endNode);          // Throws DOMException
        break;
      case Node.COMMENT_NODE:
        break;
      default:
        if (SysLog.printLevel > 0)
          SysLog.println("XMLGrammar.compileToken: WARNING - ignoring unexpected type " + nodeType + " (non-text) child node in <token>");
        break;
    }
  }

   // Add a transition on the given token between the given starting and ending nodes, constructing the token and the transition if necessary
  private void compileToken(String token, GrammarNode startNode, GrammarNode endNode) {
    startNode.addTransition(this.token(token).id, endNode);
  }

   // Tokenize the given token string, and add tokens for the constituents in sequence between the given starting and ending nodes in the rule
  private void compileTokens(String tokenString, GrammarRule rule, GrammarNode startNode, GrammarNode endNode) {

     // Assemble a Vector of the token Strings
    Vector tokens = tokens(tokenString);

     // Step through the Vector, adding the tokens in sequence between the starting and ending nodes
    Enumeration tokenEnumeration = tokens.elements();
    GrammarNode thisNode = startNode;
    GrammarNode nextNode;
    while (tokenEnumeration.hasMoreElements()) {
      String name = (String)tokenEnumeration.nextElement();
       // If this isn't the last token in the sequence, construct a GrammarNode to serve as destination
      if (tokenEnumeration.hasMoreElements()) {
        nextNode = new GrammarNode();
        rule.addNode(nextNode);
       // Otherwise, use the given endNode as destination
      } else {
        nextNode = endNode;
      }
      compileToken(name, thisNode, nextNode);
      thisNode = nextNode;
    }
  }

   // Return a Vector of the tokens
  public static Vector tokens(String tokenString) {
     // Assemble a Vector of the token Strings
    Vector tokens = new Vector();
    tokenString = tokenString.trim();
     // The first substring is quoted iff the first character is a quote
    boolean quoted = tokenString.indexOf('\"') == 0;
    StringTokenizer quotedSubstrings = new StringTokenizer(tokenString, "\"");
    while (quotedSubstrings.hasMoreTokens()) {
      StringTokenizer subStrings = new StringTokenizer(quotedSubstrings.nextToken());
       // When not in quotes, each whitespace delimited substring is a distinct token
      if (quoted == false) {
        while (subStrings.hasMoreTokens())
          tokens.addElement(subStrings.nextToken());
       // Otherwise, the entire quoted substring, minus redundant whitespace, is one big token, unless it is all whitespace
      } else if (subStrings.hasMoreTokens()) {
        StringBuffer token = new StringBuffer(subStrings.nextToken());
        while (subStrings.hasMoreTokens())
          token.append(' ').append(subStrings.nextToken());
        tokens.addElement(token.toString());
       // Otherwise, the entire quoted substring is whitespace, an ERROR
      } else {
        SysLog.println("XMLGrammar.compileTokens: ERROR - the entire token is whitespace");
      }
      quoted = !quoted;
    }
    return tokens;
  }
}
