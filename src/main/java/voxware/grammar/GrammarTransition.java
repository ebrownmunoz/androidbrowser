package voxware.grammar;

import java.io.Serializable;
import java.util.Enumeration;

import voxware.util.ObjectSet;

 // A GrammarTransition is the set of all the GrammarNodes reached via a transition
public class GrammarTransition extends ObjectSet implements Serializable {

  public GrammarTransition() {
    super();
  }

  public GrammarTransition(ObjectSet destinationNodes) {
    super(destinationNodes);
  }

  public ObjectSet destinationNodes() {
    return this;
  }

  public boolean addDestinationNode(GrammarNode node) {
    return this.add(node);
  }

  public GrammarPathSet closure() {
    GrammarPathSet closure = new GrammarPathSet();
    Enumeration destinationNodes = this.iterator();
    while (destinationNodes.hasMoreElements()) {
      GrammarNode destinationNode = (GrammarNode)destinationNodes.nextElement();
      closure.add(new GrammarPathToNode(destinationNode));
      closure.addAll(destinationNode.closure());
    }
    return closure;
  }

  public String toString(String delimiter, String indentation) {
    StringBuffer destinationString = new StringBuffer();
    if (this.size() > 0) {
      Enumeration destinationEnum = this.iterator();
      while (destinationEnum.hasMoreElements())
        destinationString.append(delimiter).append(((GrammarNode)destinationEnum.nextElement()).toString());
    } else {
      destinationString.append("<NONE>");
    }
    return destinationString.toString();
  }
}
