package voxware.grammar;

import java.io.Serializable;

 // A GrammarPathToNode is a GrammarPath leading (back) from a GrammarNode
public class GrammarPathToNode implements Cloneable, Serializable {

  public GrammarPath path;
  public GrammarNode terminus;

  public GrammarPathToNode(GrammarNode terminus) {
    this.terminus = terminus;
  }

  public GrammarPathToNode(GrammarPath path, GrammarNode terminus) {
    this.path = path;
    this.terminus = terminus;
  }

  public GrammarPath path() {
    return path;
  }

  public void path(GrammarPath path) {
    this.path = path;
  }

  public GrammarNode terminus() {
    return terminus;
  }

  public void terminus(GrammarNode terminus) {
    this.terminus = terminus;
  }

  public GrammarPathToNode append(GrammarPath path) {
    if (path != null) this.path = this.path != null ? this.path.append(path) : path;
    return this;
  }

  public GrammarPathToNode prepend(GrammarPath path) {
    if (path != null) this.path = this.path != null ? this.path.prepend(path) : path;
    return this;
  }

  public GrammarPathToNode append(GrammarPathToNode path) {
    if (path != null) this.path = this.path != null ? this.path.append(path.path) : path.path;
    return this;
  }

  public GrammarPathToNode prepend(GrammarPathToNode path) {
    if (path != null) {
      this.path = this.path != null ? this.path.prepend(path.path) : path.path;
      terminus = path.terminus;
    }
    return this;
  }

  public Object clone() {
    try {
      GrammarPathToNode clone = (GrammarPathToNode)super.clone();
      if (path != null) clone.path((GrammarPath)path.clone());
      return clone;
    } catch (CloneNotSupportedException e) {
      throw new RuntimeException("GrammarPathToNode.clone: UNEXPECTED " + e.toString());
    }
  }

  public String toString() {
    String pathString = path != null ? path.toString() : null;
    StringBuffer stringBuffer = (new StringBuffer(terminus.toString())).append(" via ");
    return stringBuffer.append(pathString != null && pathString.length() > 0 ? pathString : "<null>").toString();
  }

  public String toReverseString() {
    String pathString = path != null ? path.toReverseString() : null;
    StringBuffer stringBuffer = (new StringBuffer(terminus.toString())).append(" via ");
    return stringBuffer.append(pathString != null && pathString.length() > 0 ? pathString : "<null>").toString();
  }
}
