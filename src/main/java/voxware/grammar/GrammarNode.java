package voxware.grammar;

import java.io.Serializable;

import java.util.Enumeration;

import voxware.util.ObjectSet;
import voxware.util.SysLog;

 // A GrammarNode is the set of all the labelled transitions leaving that node
public class GrammarNode extends ObjectSet implements Serializable {

  private static final GrammarPathSet emptySet = new GrammarPathSet(1);

  private GrammarPathSet closure;   // The set of all GrammarPathToNodes reachable from this via NULL or TAG transitions

   // Return the GrammarTransition for the given tokenId, or null if none
  public GrammarTokenTransition transition(int tokenId) {
    GrammarTransition transition = null;
    Enumeration transitions = iterator();
    while (transitions.hasMoreElements()) {
      transition = (GrammarTransition)transitions.nextElement();
      if (transition instanceof GrammarTokenTransition && ((GrammarTokenTransition)transition).tokenId == tokenId)
        break;
      else
        transition = null;
    }
    return (GrammarTokenTransition)transition;
  }

   // Add a GrammarTokenTransition to the given GrammarNode via the given tokenId
  public void addTransition(int tokenId, GrammarNode endNode) {
    GrammarTokenTransition transition = transition(tokenId);
    if (transition == null) {
      transition = new GrammarTokenTransition(tokenId);
      this.add(transition);
    }
    transition.addDestinationNode(endNode);
  }

   // Return the GrammarTransition for the given GrammarRule, or null if none
  public GrammarRuleTransition transition(GrammarRule rule) {
    GrammarTransition transition = null;
    Enumeration transitions = iterator();
    while (transitions.hasMoreElements()) {
      transition = (GrammarTransition)transitions.nextElement();
      if (transition instanceof GrammarRuleTransition && ((GrammarRuleTransition)transition).rule == rule)
        break;
      else
        transition = null;
    }
    return (GrammarRuleTransition)transition;
  }

   // Add a GrammarRuleTransition to the given GrammarNode via the given GrammarRule
  public void addTransition(GrammarRule rule, GrammarNode endNode) {
    GrammarRuleTransition transition = transition(rule);
    if (transition == null) {
      transition = new GrammarRuleTransition(rule);
      this.add(transition);
    }
    transition.addDestinationNode(endNode);
  }

   // Return the GrammarTransition for the given GrammarTag, or null if none
  public GrammarTagTransition transition(GrammarTag tag) {
    GrammarTransition transition = null;
    Enumeration transitions = iterator();
    while (transitions.hasMoreElements()) {
      transition = (GrammarTransition)transitions.nextElement();
      if (transition instanceof GrammarTagTransition && ((GrammarTagTransition)transition).tag == tag)
        break;
      else
        transition = null;
    }
    return (GrammarTagTransition)transition;
  }

   // Add a GrammarTagTransition to the given GrammarNode via the given GrammarTag
  public void addTransition(GrammarTag tag, GrammarNode endNode) {
    GrammarTagTransition transition = transition(tag);
    if (transition == null) {
      transition = new GrammarTagTransition(tag);
      this.add(transition);
    }
    transition.addDestinationNode(endNode);
  }

   // Return the node's closure, constructing it if necessary
  public GrammarPathSet closure() {
    if (closure == null) {
      Enumeration transitions = this.iterator();
      while (transitions.hasMoreElements()) {
        GrammarTransition transition = (GrammarTransition)transitions.nextElement();
         // If the transition is a GrammarRuleTransition and the rule contains a NULL/TAG path, include the closure of the transition via the rule
         // The transition path will be ((GrammarPathToNode)rule.startNode().closure().get(rule.endNode())).path()
        if (transition instanceof GrammarRuleTransition) {
          GrammarRule rule = ((GrammarRuleTransition)transition).rule;
          if (rule.startNode().closure().contains(rule.endNode())) {
            if (closure == null) closure = new GrammarPathSet();
            closure.addAll(null, new GrammarRuleResult(rule, ((GrammarPathToNode)rule.startNode().closure().get(rule.endNode())).path()), transition.closure());
          }
         // Else, if it is a GrammarTokenTransition, if the token is NULL, include the closure via null
        } else if (transition instanceof GrammarTokenTransition) {
          int tokenId = ((GrammarTokenTransition)transition).tokenId;
          if (tokenId == GrammarToken.NULL_ID) {
            if (closure == null) closure = new GrammarPathSet();
            closure.addAll(null, null, transition.closure());
          }
         // Otherwise, it must be a GrammarTagTransition, so include the closure via the tag
        } else {
          GrammarTag tag = ((GrammarTagTransition)transition).tag;
          if (closure == null) closure = new GrammarPathSet();
          closure.addAll(null, tag, transition.closure());
        }
      }
      if (closure == null) closure = emptySet;
      if (SysLog.printLevel > 2)
        SysLog.println("GrammarNode.closure: node " + this.toString() + " closure:" + closureToString("\n  ", "  "));
    }
    return closure;
  }

  public String closureToString(String delimiter, String indentation) {
    StringBuffer closureString = new StringBuffer();
    if (closure != null && closure.size() > 0) {
      closureString.append(delimiter).append("Closure:");
      delimiter += indentation;
      Enumeration closureEnum = closure.iterator();
      while (closureEnum.hasMoreElements()) {
        GrammarPathToNode pathToNode = (GrammarPathToNode)closureEnum.nextElement();
        closureString.append(delimiter).append(pathToNode.toReverseString());
      }
    }
    return closureString.toString();
  }

  public String toString(String delimiter, String indentation) {
    StringBuffer nodeString = new StringBuffer(delimiter);
    nodeString.append("Node (").append(toString()).append(") has ").append(this.size()).append(" transitions:");
    delimiter += indentation;
    Enumeration transitionEnum = this.iterator();
    while (transitionEnum.hasMoreElements())
      nodeString.append(((GrammarTransition)transitionEnum.nextElement()).toString(delimiter, indentation));
    nodeString.append(closureToString(delimiter, indentation));
    return nodeString.toString();
  }
}
