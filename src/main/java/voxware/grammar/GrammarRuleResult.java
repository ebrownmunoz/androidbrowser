package voxware.grammar;

import java.io.Serializable;

 // A GrammarRuleResult is a wrapper to hold a GrammarRule and a result GrammarPath
public class GrammarRuleResult implements GrammarPathElement, Serializable {

  private GrammarRule rule;
  private GrammarPath path;

  public GrammarRuleResult(GrammarRule rule, GrammarPath path) {
    this.rule = rule;
    this.path = path;
  }

  public GrammarRule rule() {
    return rule;
  }

  public GrammarPath path() {
    return path;
  }

  public String toString() {
    StringBuffer stringBuffer = new StringBuffer(rule.toString());
    stringBuffer.append('[');
    if (path != null) stringBuffer.append(path.toString());
    stringBuffer.append(']');
    return stringBuffer.toString();
  }

  public String toReverseString() {
    StringBuffer stringBuffer = new StringBuffer(rule.toString());
    stringBuffer.append('[');
    if (path != null) stringBuffer.append(path.toReverseString());
    stringBuffer.append(']');
    return stringBuffer.toString();
  }

  public void reverse() {
    if (path != null) path = path.reverse();
  }
}
