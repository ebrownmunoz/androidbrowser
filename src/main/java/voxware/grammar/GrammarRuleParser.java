package voxware.grammar;

import java.util.Enumeration;

import voxware.util.ObjectSet;
import voxware.util.SysLog;

public class GrammarRuleParser {

  private GrammarRule     rule;
  private Grammar         grammar;
  private GrammarPathSet  activePaths = new GrammarPathSet();       // The set of all active GrammarPathToNodes
  private ObjectSet       activeRules = new ObjectSet();            // The set of all active RuleTransitionInstances

  private class RuleTransitionInstance extends GrammarRuleParser {

    public GrammarPath path;
    public ObjectSet   destinationNodes;

    public RuleTransitionInstance(GrammarPath path, GrammarRuleTransition transition) {
      super(transition.rule());
      destinationNodes = transition;
      this.path = path;
    }
  }

  public GrammarRuleParser(GrammarRule rule) {
    this.rule = rule;
    grammar = rule.grammar();
  }

  public GrammarRule rule() {
    return rule;
  }

  public void startParse() {
    activePaths.clear();
    activePaths.add(new GrammarPathToNode(rule.startNode()));
    activeRules.clear();
    closeActivePathSet();
  }

  public boolean parseToken(String token) throws XMLGrammarException {
    return parseToken(rule.grammar().tokenId(token));
  }

  public boolean parseToken(int tokenId) {
    GrammarPathSet destinationNodes = new GrammarPathSet();
     // Process the paths
    Enumeration paths = activePaths.iterator();
    while (paths.hasMoreElements()) {
      GrammarPathToNode pathToNode = (GrammarPathToNode)paths.nextElement();
      Enumeration transitions = pathToNode.terminus().iterator();
       // Process the transitions out of the node
      while (transitions.hasMoreElements()) {
        GrammarTransition transition = (GrammarTransition)transitions.nextElement();
         // If the transition is a GrammarRuleTransition, instantiate the GrammarRuleParser and start the parse
        if (transition instanceof GrammarRuleTransition) {
          if (SysLog.printLevel > 2)
            SysLog.println("GrammarRuleParser.parseToken: adding transition on rule " + ((GrammarRuleTransition)transition).rule().toString());
          RuleTransitionInstance ruleTransition = new RuleTransitionInstance(pathToNode.path(), (GrammarRuleTransition)transition);
          ruleTransition.startParse();
          activeRules.add(ruleTransition);
         // Otherwise, if the transition is a GrammarTokenTransition, add a GrammarPath element for the token
        } else if (transition instanceof GrammarTokenTransition) {
          if (((GrammarTokenTransition)transition).tokenId == tokenId || ((GrammarTokenTransition)transition).tokenId == GrammarToken.GARBAGE_ID)
            destinationNodes.addAll(pathToNode.path(), grammar.token(tokenId), transition);
         // Otherwise, the transition is a GrammarTagTransition, which the closure mechanism will handle
        }
      }
    }
     // Process the rule transitions
    if (activeRules.size() > 0) {
      if (SysLog.printLevel > 2)
        SysLog.println("GrammarRuleParser.parseToken: there are " + activeRules.size() + " active RuleTransitions");
      ObjectSet detritus = null;
      Enumeration rules = activeRules.iterator();
      while (rules.hasMoreElements()) {
        RuleTransitionInstance transition = (RuleTransitionInstance)rules.nextElement();
        if (transition.parseToken(tokenId)) {
          GrammarPath rulePath = transition.parseResult();
          if (rulePath != null)
            destinationNodes.addAll(transition.path, new GrammarRuleResult(transition.rule(), rulePath), transition.destinationNodes);
        } else {
          if (detritus == null) detritus = new ObjectSet();
          detritus.add(transition);
        }
      }
      if (detritus != null) activeRules.removeAll(detritus);
    }
    activePaths = destinationNodes;
    closeActivePathSet();
    return (!activePaths.isEmpty() || !activeRules.isEmpty());
  }

  public boolean parseIsFinished() {
    return activePaths.containsKey(rule.endNode());
  }

  public GrammarPath parseResult() {
    GrammarPathToNode path = (GrammarPathToNode)activePaths.get(rule.endNode());
    return (path != null) ? path.path() : null;
  }

  private void closeActivePathSet() {
    GrammarPathSet closure = new GrammarPathSet();
    Enumeration activePathEnum = activePaths.iterator();
    while (activePathEnum.hasMoreElements()) {
      GrammarPathToNode activePathToNode = (GrammarPathToNode)activePathEnum.nextElement();
      GrammarPathSet closurePaths = activePathToNode.terminus().closure();
      Enumeration closurePathEnum = closurePaths.iterator();
      while (closurePathEnum.hasMoreElements()) {
        GrammarPathToNode closurePathToNode = (GrammarPathToNode)closurePathEnum.nextElement();
         // If there is no active path to the closure's terminal node, add one via the closure
        if (!activePaths.contains(closurePathToNode)) {
          closure.add(((GrammarPathToNode)closurePathToNode.clone()).append(activePathToNode));
        }
      }
    }
    activePaths.addAll(closure);
  }
}
