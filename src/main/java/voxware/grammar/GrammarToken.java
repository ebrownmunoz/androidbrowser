package voxware.grammar;

import java.io.Serializable;

public class GrammarToken implements GrammarPathElement, Serializable {

  public static final int VOID_ID    = -1;
  public static final int NULL_ID    =  0;
  public static final int GARBAGE_ID =  1;

  public  int     id = VOID_ID;
  public  String  name;
  private String  languages;

  public GrammarToken(String name, int id) {
    this.name = name;
    this.id = id;
  }

  public GrammarToken(String name) {
    this(name, VOID_ID);
  }

  public String name() {
    return name;
  }

  public void languages(String languages) {
    this.languages = languages;
  }

  public String languages() {
    return this.languages;
  }

  public String toString(String delimiter, String indentation) {
    StringBuffer tokenString = (new StringBuffer(delimiter)).append("    ");
    String idString = Integer.toString(id);
    tokenString.setLength(tokenString.length() - idString.length());
    return tokenString.append(idString).append(": ").append(name).toString();
  }

  public String toString() {
    return name;
  }

  public String toReverseString() {
    return toString();
  }

  public void reverse() {
  }
}
