package com.voxware.abrowser;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class MyListActivity extends Activity implements OnEditorActionListener {

	private static String TAG = "MyListActivity";
    /**
     * Called when the activity is first created.
     * @param savedInstanceState If the activity is being re-initialized after 
     * previously being shut down then this Bundle contains the data it most 
     * recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "I am in ListActivity");
        setContentView(R.layout.activity_main);
        ListView myList = (ListView) findViewById(R.id.listView1);
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(com.voxware.abrowser.R.menu.main, menu);
	Log.d(TAG, "Strartin menu option?");
	return true;
    }

	@Override
	public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
		
			TextView inputText = (TextView) findViewById(R.id.textView1);
			inputText.setText("Foo: " + textView.getText() + " - " + actionId);
		
			Intent intent = new Intent(getApplicationContext(), BrowserService.class);
			startService(intent);
		return true;
	}

}

