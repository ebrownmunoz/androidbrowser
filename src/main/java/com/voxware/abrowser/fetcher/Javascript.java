package com.voxware.abrowser.fetcher;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class Javascript {
	public Javascript() {
		
		
	}
	
	public String executeTest() {
		Context cx = Context.enter();
		
		// Needed to work with android (by default it optimizes JVM bytecode... doesn't work on Dalvik).
		cx.setOptimizationLevel(-1);
		Scriptable scopeA = cx.initStandardObjects();
		Scriptable scopeB = cx.initStandardObjects();
		scopeB.setParentScope(scopeA);
		
		Object result = cx.evaluateString(scopeB, "var foo = '1';", "foo", 1, null);
		cx.evaluateString(scopeA, "foo = '9';", "foo", 1, null);

		result = cx.evaluateString(scopeB, "(function() {return foo;})()", "bar", 1, null);
		
		return cx.toString(result);
	}
}
