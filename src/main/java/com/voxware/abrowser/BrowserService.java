/**
 * 
 */
package com.voxware.abrowser;

import com.voxware.abrowser.fetcher.Javascript;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * @author eric
 *
 */
public class BrowserService extends Service {
	
	private static String TAG = "BrowserService";

	/* (non-Javadoc)
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "starting service");
		
		Javascript js = new Javascript();
		String result = js.executeTest();
		
		Log.d(TAG, "result: " + result);
		return START_NOT_STICKY;
	}

}
